// import { BASE_ROUTE } from "@/config/constants";
// import { get } from "@/connections/fetch_wrapper";
// import { getCache, setCache } from "@/utils/redisService";
// import { createAsyncThunk } from "@reduxjs/toolkit";

// export const getAllInterfaceLanguages = createAsyncThunk(
//   "languages/getall",
//   async (_, { rejectWithValue }) => {
//     const cacheKey = 'allInterfaceLanguages'; // Set a cache key

//     // Attempt to get data from Redis cache
//     const cachedData = await new Promise((resolve) => {
//       getCache(cacheKey, (data: any ) => {
//         resolve(data);
//       });
//     });

//     if (cachedData) {
      
//       // If data is found in cache, return it
//       return cachedData;
//     } else {
//       try {
//         const response = await get(`${BASE_ROUTE}/interface-languages`);
//         if (response.code === 200) {
//           // Store the data in Redis cache with a TTL (e.g., 3600 seconds)
//           setCache(cacheKey, response, 3600);
//           return response;
//         } else {
//           return rejectWithValue(response.msg);
//         }
//       } catch (err:any) {
//         return rejectWithValue(err.message);
//       }
//     }
//   }
// );




import { BASE_ROUTE } from "@/config/constants";
import { get } from "@/connections/fetch_wrapper";
import { createAsyncThunk } from "@reduxjs/toolkit";


//---------------------------------------------------------------------------------------------------------------------------------------------------------//

//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous retrieval of particular Languages, and depending on the response, either returns the data or an error message.

export const getAllInterfaceLanguages = createAsyncThunk(
  "languages/getall",
  async (_, { rejectWithValue }) => {
    try {
      const response = await get(`${BASE_ROUTE}/interface-languages`);
      if (response.code === 200) {    
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);