
// actions.js
import { getActivePartnerCookie, getActiveServiceProviderCookie } from '@/utils/cookies';
import { createAsyncThunk } from '@reduxjs/toolkit';

export const fetchActiveCookies = createAsyncThunk(
  'get/Cookies',
  async (_, { rejectWithValue }) => {
    try {
      const activePartner = getActivePartnerCookie();
      const activeServiceProvider = getActiveServiceProviderCookie();

      return { activePartner, activeServiceProvider };
    } catch (error:any) {
      return rejectWithValue(error.message);
    }
  }
);



