// reducer.js
import { createReducer } from '@reduxjs/toolkit';
import { fetchActiveCookies } from './actions';
import { ResponseStatus } from '@/helpers/response_status';

interface IReducerInitialState {
  data: any;
  activePartner:string;
  activeServiceProvider:string;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}

const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: [],
  activePartner:"",
  activeServiceProvider:"",
};

const cookiesReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(fetchActiveCookies.pending, (state) => {
      state.status = ResponseStatus.LOADING;;
    })
    .addCase(fetchActiveCookies.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.activePartner = payload?.activePartner as any;
      state.activeServiceProvider = payload?.activeServiceProvider as any;
    })
    .addCase(fetchActiveCookies.rejected, (state, action) => {
      state.status = ResponseStatus.ERROR;
      state.error = action.error.message;
    });
});

export default cookiesReducer;
