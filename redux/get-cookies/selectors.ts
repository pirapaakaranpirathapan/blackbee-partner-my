import { RootState } from "../store/store";
import { createSelector } from '@reduxjs/toolkit';

export const cookieState = (state: RootState) => state.cookiesReducer;
export const cookieSelector = createSelector(cookieState, (state) => state);

// export const selectActiveServiceProvider = createSelector(cookieState, (state) => state);
