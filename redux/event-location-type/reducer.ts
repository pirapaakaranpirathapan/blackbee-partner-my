import { ResponseStatus } from '@/helpers/response_status';
import { createReducer } from '@reduxjs/toolkit';
import { getAllEventLocationTypes } from './actions';

interface IReducerInitialState {
  data: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: [],
};

export const eventLocationTypeReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(getAllEventLocationTypes.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      console.log("pending===>", state.status);
      
    })
    .addCase(getAllEventLocationTypes.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      console.log("payload===>", payload);
      
      state.data = payload?.data as any;
      
    })
    .addCase(getAllEventLocationTypes.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      console.log("error===>", error);
      

    });
});
export default eventLocationTypeReducer;
