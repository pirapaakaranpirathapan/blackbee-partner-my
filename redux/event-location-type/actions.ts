import { BASE_ROUTE } from "@/config/constants";
import { get } from "@/connections/fetch_wrapper";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const getAllEventLocationTypes = createAsyncThunk(
  "event-locations/getall",
  async (_, { rejectWithValue }) => {
    console.log("getAllEventLocationTypes===>");
    
    try {
      const response = await get(`${BASE_ROUTE}/event-location-types`);
      console.log('response===>', response);
      if (response.code === 200) {
        
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      console.log("err===>", err);
      
      return rejectWithValue(err.message);
    }
  }
);
