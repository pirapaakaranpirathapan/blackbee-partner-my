import { RootState } from "../store/store";
import { createSelector } from "@reduxjs/toolkit";

export const eventLocationTypeState = (state: RootState) =>
  state.eventLocationTypeReducer;

export const eventLocationTypesReducer = createSelector(
  eventLocationTypeState,
  (state) => state
);
