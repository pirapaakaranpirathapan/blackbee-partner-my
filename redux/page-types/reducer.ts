import { ResponseStatus } from '@/helpers/response_status';
import { INoticeTitles } from '@/interfaces/INoticeTitles';
import { createReducer } from '@reduxjs/toolkit';
import {  getAllPageTypes } from './actions';
import { IPageType } from '@/interfaces/memorial-page/IPageType';
import { IPageTypes } from '@/interfaces/IPageTypes';

interface IReducerInitialState {
  data: IPageTypes;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: {} as unknown as INoticeTitles,
};

export const pageTypeReducer = createReducer(initialState, (builder) => {

    builder
    .addCase(getAllPageTypes.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(getAllPageTypes.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
     
      
    })
    .addCase(getAllPageTypes.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });
});
export default pageTypeReducer;
