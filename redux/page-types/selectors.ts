import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const PageTypesState = (state: RootState) => state.pageTypeReducer;

export const PageTypesSelector = createSelector(
    PageTypesState, 
    state => state
)
