import { ResponseStatus } from '@/helpers/response_status';
import { createReducer } from '@reduxjs/toolkit';
import { DeletePageManagers, InviteManager, InviteManagerNotice, createPageManagers, deleteCustomerManagers, getAllCustomerManagers, getAllPageManagers } from './actions';
import { IPageManager } from '@/interfaces/IPageManager';

interface IReducerInitialState {
  data:IPageManager[];
  dataCustomer:any
  status: ResponseStatus;
  error: any;
  errorNotice:any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  errorNotice:{},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: [],
  dataCustomer:[],
};

// Reducer for handling the "get all Memorials" action
export const pageManagerReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(getAllPageManagers.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(getAllPageManagers.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
      // console.log("payload", payload?.data);
      
    })
    .addCase(getAllPageManagers.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });
//create page manager
    builder
    .addCase(createPageManagers.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(createPageManagers.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
      // console.log("payload", payload?.data);
      
    })
    .addCase(createPageManagers.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

//delete page manager

    builder
    .addCase(DeletePageManagers.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(DeletePageManagers.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
      // console.log("payload", payload?.data);
      
    })
    .addCase(DeletePageManagers.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

    builder
    .addCase(InviteManagerNotice.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(InviteManagerNotice.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;      
    })
    .addCase(InviteManagerNotice.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.errorNotice=error?.payload
    });

    builder
    .addCase(InviteManager.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(InviteManager.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;      
    })
    .addCase(InviteManager.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error=error?.payload
    });


    builder
    .addCase(getAllCustomerManagers.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(getAllCustomerManagers.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.dataCustomer = payload?.data as any;
    })
    .addCase(getAllCustomerManagers.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

    builder
    .addCase(deleteCustomerManagers.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(deleteCustomerManagers.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
      // console.log("payload", payload?.data);
      
    })
    .addCase(deleteCustomerManagers.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });
    
    
});




export default pageManagerReducer;
