import { BASE_ROUTE } from '@/config/constants';
import { deleteMethod, get, post } from '@/connections/fetch_wrapper';
import { createAsyncThunk } from '@reduxjs/toolkit';

//This action function handles the asynchronous retrieval of all Memorials, and depending on the response, either returns the data or an error message.
export const getAllPageManagers = createAsyncThunk(
  'GetAll/pageManagers',
  async ({ active_partner, active_service_provider,active_page }: {active_partner:string,active_service_provider:string,active_page:string}, {rejectWithValue}) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_page}/managers`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }

    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);

export const createPageManagers = createAsyncThunk("customer/create", async ( { activePartner, activeServiceProvider,updatedData }: {activePartner:string,activeServiceProvider:String,updatedData: { first_name: string;last_name:string;email:string;mobile:string;alternative_phone:String; password:string,password_confirmation:string }} ,{ rejectWithValue }) => {
  try {
    const response = await post(`${BASE_ROUTE}/${activePartner}/${activeServiceProvider}/clients`, updatedData);
    if(response.code ==200 ){
      return response;
    }else{
      return rejectWithValue(response)
    }
   
  } catch (err) {
    return rejectWithValue(err)
  }
  
});

//This action function handles the asynchronous retrieval of all Memorials, and depending on the response, either returns the data or an error message.
export const DeletePageManagers = createAsyncThunk(
  'Delete/pageManagers',
  async ({ active_partner, active_service_provider,active_page,active_page_manager }: {active_partner:string,active_service_provider:string,active_page:string,active_page_manager:string}, {rejectWithValue}) => {
    try {
      const response = await deleteMethod(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_page}/managers/${active_page_manager}`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }

    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);


export const InviteManager = createAsyncThunk('post/inviteManager', async ( { active_partner, active_service_provider,active_page,InviteData }: {active_partner:string, active_service_provider:String, active_page: string, InviteData: { email: string, manager_type_id:string }} ,{ rejectWithValue }) => {
  try {
    const response = await post(`${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_page}/managers/invitations`, InviteData);
    if(response.code ==200 ){
      return response;
    }else{
      return rejectWithValue(response)
    }
   
  } catch (err) {
    return rejectWithValue(err)
  }
  
});
export const InviteManagerNotice = createAsyncThunk('post/InviteManagerNotice', async ( { active_partner, active_service_provider, active_id,InviteData }: {active_partner:string, active_service_provider:String,  active_id: string, InviteData: { email: string, manager_type_id:string }} ,{ rejectWithValue }) => {
  try {
    const response = await post(`${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_id}/managers/invitations`, InviteData);
    if(response.code ==200 ){
      return response;
    }else{
      return rejectWithValue(response)
    }
   
  } catch (err) {
    return rejectWithValue(err)
  }
  
});


//This action function handles the asynchronous retrieval of all Memorials, and depending on the response, either returns the data or an error message.
export const getAllCustomerManagers = createAsyncThunk(
  'GetAll/getAllCustomerManagers',
  async ({ active_partner, active_service_provider,active_page }: {active_partner:string,active_service_provider:string,active_page:string}, {rejectWithValue}) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_page}/managers`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }

    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);

//This action function handles the asynchronous retrieval of all Memorials, and depending on the response, either returns the data or an error message.
export const deleteCustomerManagers = createAsyncThunk(
  'Delete/CustomerManagers',
  async ({ active_partner, active_service_provider,active_page,active_page_manager }: {active_partner:string,active_service_provider:string,active_page:string,active_page_manager:string}, {rejectWithValue}) => {
    try {
      const response = await deleteMethod(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_page}/managers/${active_page_manager}`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }

    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
