import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const pageManagerState = (state: RootState) => state.pageManagerReducer;

export const pageManagerSelector = createSelector(
    pageManagerState, 
    state => state
)
