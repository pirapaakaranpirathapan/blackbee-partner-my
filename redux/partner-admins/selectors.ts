import { RootState } from "../store/store";
import { createSelector } from "@reduxjs/toolkit";

export const partnerAdminState = (state: RootState) =>
  state.partnerAdminReducer;

export const partnerAdminSelector = createSelector(
  partnerAdminState,
  (state) => state
);
