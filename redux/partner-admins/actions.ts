import { BASE_ROUTE } from "@/config/constants";
import { deleteMethod, get, patch, post } from "@/connections/fetch_wrapper";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const getAllPartnerAdmins = createAsyncThunk(
  "partner-admins",
  async (
    {
      active_partner,
      page,
    }: {
      active_partner: string;
      page: number;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/${active_partner}/partner-admins?page=${page}`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
export const getPartnerAdmin = createAsyncThunk(
  "get/partner-admins",
  async (
    {
      active_partner,
      partner_admin_uuid,
    }: {
      active_partner: string;
      partner_admin_uuid: string;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/${active_partner}/partner-admins/${partner_admin_uuid}`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
export const createPartnerAdmin = createAsyncThunk(
  "partnerAdmin/create",
  async (
    {
      active_partner,
      updatedData,
    }: {
      active_partner: string;
      updatedData: {
        state_id: number;
        salutation_id?: number;
        first_name: string;
        last_name: string;
        email: string;
        password: string;
        password_confirmation: string;
        login_with_password: boolean;
        login_with_google: boolean;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await post(
        `${BASE_ROUTE}/${active_partner}/partner-admins`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
export const updatePartnerAdmin = createAsyncThunk(
  "partnerAdmin/update",
  async (
    {
      active_partner,
      partner_admin_uuid,
      updatedData,
    }: {
      active_partner: string;
      partner_admin_uuid: string;
      updatedData: {
        state_id?: number;
        salutation_id?: number;
        first_name: string;
        last_name: string;
        email: string;
        password: string;
        password_confirmation: string;
        login_with_password?: boolean;
        login_with_google?: boolean;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/partner-admins/${partner_admin_uuid}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const deletePartnerAdmin = createAsyncThunk(
  "partnerAdmin/update",
  async (
    {
      active_partner,
      partner_admin_uuid,
      uuid,
    }: {
      active_partner: string;
      partner_admin_uuid: string;
      uuid: string;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await deleteMethod(
        `${BASE_ROUTE}/${active_partner}/partner-admins/${uuid}`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
