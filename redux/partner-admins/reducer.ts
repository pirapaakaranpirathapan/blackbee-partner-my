import { ResponseStatus } from "@/helpers/response_status";
import { createReducer } from "@reduxjs/toolkit";
import {
  createPartnerAdmin,
  getAllPartnerAdmins,
  getPartnerAdmin,
  updatePartnerAdmin,
} from "./actions";

interface IReducerInitialState {
  data: any;
  singleData: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: [],
  singleData: [],
};

export const partnerAdminReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(getAllPartnerAdmins.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      console.log("pending===>", state.status);
    })
    .addCase(getAllPartnerAdmins.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      console.log("payload===>", payload);

      state.data = payload?.data as any;
    })
    .addCase(getAllPartnerAdmins.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      console.log("error===>", error);
    });

  builder
    .addCase(getPartnerAdmin.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      console.log("pending===>", state.status);
    })
    .addCase(getPartnerAdmin.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      console.log("payload===>", payload);

      state.singleData = payload?.data as any;
    })
    .addCase(getPartnerAdmin.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      console.log("error===>", error);
    });
  // Reducer for handling the "ccreate PartnerAdmin" action
  builder
    .addCase(createPartnerAdmin.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(createPartnerAdmin.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(createPartnerAdmin.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  // Reducer for handling the "update PartnerAdmin" action
  builder
    .addCase(updatePartnerAdmin.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(updatePartnerAdmin.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(updatePartnerAdmin.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });
});
export default partnerAdminReducer;
