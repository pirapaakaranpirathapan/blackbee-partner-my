import { ResponseStatus } from '@/helpers/response_status';
import { createReducer } from '@reduxjs/toolkit';
import { getAllGenders } from './actions';

interface IReducerInitialState {
  data: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: [],
};

export const gendersReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(getAllGenders.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(getAllGenders.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
      
    })
    .addCase(getAllGenders.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;

    });
});
export default gendersReducer;
