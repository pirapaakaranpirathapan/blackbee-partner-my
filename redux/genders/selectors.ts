import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const gendersState = (state: RootState) => state.gendersReducer;

export const gendersSelector = createSelector(
    gendersState, 
    state => state
)
