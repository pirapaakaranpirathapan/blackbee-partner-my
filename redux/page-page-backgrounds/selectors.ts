import { RootState } from "../store/store";
import { createSelector } from "@reduxjs/toolkit";

export const pagePageBackgroundsState = (state: RootState) => state.  pagePageBackgroundReducer;
export const  pagePageBackgroundsSelector = createSelector( pagePageBackgroundsState, (state) => state);
