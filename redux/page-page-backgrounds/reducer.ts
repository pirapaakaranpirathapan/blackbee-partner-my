import { ResponseStatus } from "@/helpers/response_status";
import { createReducer } from "@reduxjs/toolkit";
import {  getAllPagePageBackgrounds } from "./actions";
import { INoticePages } from "@/interfaces/INoticePages";

interface IReducerInitialState {
  data: any ;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: [],
};

export const pagePageBackgroundReducer = createReducer(initialState, (builder) => {
  // ------------Reducer for handling the "get All page templates" action------------//
  builder
    .addCase(getAllPagePageBackgrounds.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(getAllPagePageBackgrounds.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
       })
    .addCase(getAllPagePageBackgrounds.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

 
});
export default pagePageBackgroundReducer;
