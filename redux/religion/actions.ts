import { BASE_ROUTE } from "@/config/constants";
import { get } from "@/connections/fetch_wrapper";
import { createAsyncThunk } from "@reduxjs/toolkit";

//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous retrieval of religions, and depending on the response, either returns the data or an error message.

export const getAllReligions = createAsyncThunk(
  "religions/getall",
  async (_, { rejectWithValue }) => {
    try {
      const response = await get(`${BASE_ROUTE}/religions`);
      if (response.code === 200) {
        
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
