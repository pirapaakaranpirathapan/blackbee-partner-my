import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const religionState = (state: RootState) => state.religionReducer;

export const religionSelector = createSelector(
    religionState, 
    state => state
)
