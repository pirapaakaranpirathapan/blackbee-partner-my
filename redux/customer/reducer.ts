import { ResponseStatus } from "@/helpers/response_status";
import { createReducer } from "@reduxjs/toolkit";
import {
  createCustomer,
  getAllCustomer,
  getAllCustomerforpage,
  getOneCustomer,
  updateCustomer,
} from "./actions";
import { ICustomerPages } from "@/interfaces/ICustomerPages";

interface IReducerInitialState {
  // data: (ICustomerPages)[] ;
  data: any;
  dataOne: ICustomerPages[];
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}

const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  dataOne: [],
  data: [],
};

export const customerPageReducer = createReducer(initialState, (builder) => {
  // Reducer for handling the "get all customer" action
  builder
    .addCase(getAllCustomer.pending, (state) => {
      state.status = ResponseStatus.LOADING; // Set the status to loading
      state.data = [];
    })
    .addCase(getAllCustomer.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED; // Set the status to loaded
      state.data = payload?.data as any; // Update the data with the received payload
       console.log("getAllCustomerPayload",payload?.data);
    })
    .addCase(getAllCustomer.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR; // Set the status to error
    });
  // Reducer for handling the "get all customer for page" action
  builder
    .addCase(getAllCustomerforpage.pending, (state) => {
      state.status = ResponseStatus.LOADING; // Set the status to loading
      state.data = [];
    })
    .addCase(getAllCustomerforpage.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED; // Set the status to loaded
      state.data = payload?.data.data as any; // Update the data with the received payload
      // console.log("getAllCustomerPayload",payload?.data.data);
    })
    .addCase(getAllCustomerforpage.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR; // Set the status to error
    });

  // Reducer for handling the "get one customer" action
  builder
    .addCase(getOneCustomer.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.dataOne = [];
    })
    .addCase(getOneCustomer.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      // state.data = payload?.data as any;
      state.dataOne[0] = payload?.data as any;
      // state.data[0] = payload?.data as any;
      // console.log("getOneCustomerpayload",payload?.data);
    })
    .addCase(getOneCustomer.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

  // Reducer for handling the "create customer" action
  builder
    .addCase(createCustomer.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(createCustomer.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(createCustomer.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  // Reducer for handling the "update customer" action
  builder
    .addCase(updateCustomer.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(updateCustomer.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
      console.log("patch-response1payload", payload?.data);
    })
    .addCase(updateCustomer.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });
});
export default customerPageReducer;
