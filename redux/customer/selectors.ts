import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const customerPageState = (state: RootState) => state.customerPageReducer;

export const customerPageSelector = createSelector(
    customerPageState, 
    state => state
)
