import { BASE_ROUTE } from "@/config/constants";
import { get, post, patch } from "@/connections/fetch_wrapper";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { String } from "aws-sdk/clients/acm";
import { integer } from "aws-sdk/clients/cloudfront";

//This action function handles the asynchronous retrieval of all customers, and depending on the response, either returns the data or an error message.
export const getAllCustomer = createAsyncThunk(
  "customer/get-all",
  async (
    {
      active_partner,
      active_service_provider,
      page,
    }: {
      active_partner: string;
      active_service_provider: string;
      page: integer;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/clients?page=${page}`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
//This action function handles the asynchronous retrieval of all customers, and depending on the response, either returns the data or an error message.
export const getAllCustomerforpage = createAsyncThunk(
  "customer/get-allforcreatepage",
  async (
    {
      active_partner,
      active_service_provider,
    }: { active_partner: string; active_service_provider: string },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/clients`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
//This action function handles the asynchronous retrieval of a single customer based on the UUID and returns the customer data or an error message
export const getOneCustomer = createAsyncThunk(
  "customer/get-one",
  async (
    {
      active_partner,
      active_service_provider,
      client_id,
    }: {
      active_partner: string;
      active_service_provider: string;
      client_id: string;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/clients/${client_id}`
      );

      if (response.code == 200) {
        // console.log("response", response);
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
export const createCustomer = createAsyncThunk(
  "customer/create",
  async (
    {
      activePartner,
      activeServiceProvider,
      updatedData,
    }: {
      activePartner: string;
      activeServiceProvider: String;
      updatedData: {
        first_name: string;
        last_name: string;
        country_id: any;
        email: string;
        mobile: string;
        alternative_phone: String;
        gender_id: string;
        password: string;
        password_confirmation: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await post(
        `${BASE_ROUTE}/${activePartner}/${activeServiceProvider}/clients`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
//This action function handles the asynchronous update of an existing customer with the provided data and returns the response data or an error message.
export const updateCustomer = createAsyncThunk(
  "customer/update",
  async (
    {
      active_partner,
      active_service_provider,
      client_id,
      updatedData,
    }: {
      active_partner: string;
      active_service_provider: string;
      client_id: string;
      updatedData: {
        first_name: string;
        last_name: string;
        country_id: string;
        email: string;
        mobile: string;
        alternative_phone: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/clients/${client_id}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
//This action function handles the asynchronous update of an existing customer with the provided data and returns the response data or an error message.
export const GetCustomer = createAsyncThunk(
  "customer/update",
  async (
    {
      active_partner,
      active_service_provider,
      client_id,
      updatedData,
    }: {
      active_partner: string;
      active_service_provider: string;
      client_id: string;
      updatedData: {
        first_name: string;
        last_name: string;
        country_id: string;
        email: string;
        mobile: string;
        alternative_phone: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/clients/${client_id}`
      );
      if (response.code == 200) {
        console.log("patch-response1");

        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
