import { BASE_ROUTE } from "@/config/constants";
import { get } from "@/connections/fetch_wrapper";
import { createAsyncThunk } from "@reduxjs/toolkit";

//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous retrieval of relationships, and depending on the response, either returns the data or an error message.

export const getAllRelationships = createAsyncThunk(
  "relationships/getrelationships",
  async (_, { rejectWithValue }) => {
    try {
      const response = await get(`${BASE_ROUTE}/relationships`);
      if (response.code === 200) {
        
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
