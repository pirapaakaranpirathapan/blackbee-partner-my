import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const relationshipsState = (state: RootState) => state.relationshipsReducer;

export const relationsSelector = createSelector(
    relationshipsState, 
    state => state
)
