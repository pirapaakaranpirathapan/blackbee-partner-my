import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const CountriesState = (state: RootState) => state.CountriesReducer;

export const CountriesSelector = createSelector(
    CountriesState, 
    state => state
)
