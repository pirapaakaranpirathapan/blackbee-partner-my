import { RootState } from "../store/store";
import { createSelector } from "@reduxjs/toolkit";

export const pagetemplatesClipArtState = (state: RootState) => state. pageTemplatesClipArtReducer;
export const  pagetemplatesClipArtSelector = createSelector( pagetemplatesClipArtState, (state) => state);
