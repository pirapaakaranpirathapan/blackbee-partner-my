import { RootState } from "../store/store";
import { createSelector } from "@reduxjs/toolkit";

export const pagetemplatesBorderState = (state: RootState) => state. pageTemplatesBorderReducer;
export const  pagetemplatesBorderSelector = createSelector( pagetemplatesBorderState, (state) => state);
