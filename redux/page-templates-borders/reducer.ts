import { ResponseStatus } from "@/helpers/response_status";
import { createReducer } from "@reduxjs/toolkit";
import {  getAllPageTemplateBorders } from "./actions";
import { INoticePages } from "@/interfaces/INoticePages";

interface IReducerInitialState {
  data: any ;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: [],
};

export const pageTemplatesBorderReducer = createReducer(initialState, (builder) => {
  // ------------Reducer for handling the "get All page templates" action------------//
  builder
    .addCase(getAllPageTemplateBorders.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(getAllPageTemplateBorders.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
       })
    .addCase(getAllPageTemplateBorders.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

 
});
export default pageTemplatesBorderReducer;
