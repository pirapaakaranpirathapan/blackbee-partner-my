import { RootState } from "../store/store";
import { createSelector } from "@reduxjs/toolkit";

export const noticetemplatesClipArtState = (state: RootState) => state. noticeTemplatesClipArtReducer;
export const  noticetemplatesClipArtSelector = createSelector( noticetemplatesClipArtState, (state) => state);
