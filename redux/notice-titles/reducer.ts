import { ResponseStatus } from '@/helpers/response_status';
import { INoticeTitles } from '@/interfaces/INoticeTitles';
import { createReducer } from '@reduxjs/toolkit';
import {  searchNoticeTitle } from './actions';

interface IReducerInitialState {
  data: any;
  // data: INoticeTitles;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  // data: {} as unknown as INoticeTitles,
  data: {},
};

export const noticeTitleReducer = createReducer(initialState, (builder) => {

    builder
    .addCase(searchNoticeTitle.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(searchNoticeTitle.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(searchNoticeTitle.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });
});
export default noticeTitleReducer;
