import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const NoticeTitleState = (state: RootState) => state.noticeTitleReducer;

export const noticeTitleSelector = createSelector(
    NoticeTitleState, 
    state => state
)
