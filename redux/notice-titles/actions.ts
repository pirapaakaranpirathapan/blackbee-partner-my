import { BASE_ROUTE } from "@/config/constants";
import { get, post, search } from "@/connections/fetch_wrapper";
import { createAsyncThunk } from "@reduxjs/toolkit";

//---------------------------------------------------------------------------------------------------------------------------------------------------------//

//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous retrieval of NoticeTitles, and depending on the response, either returns the data or an error message.
export const searchNoticeTitle = createAsyncThunk(
  "NoticeTitles",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      keywordData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice:string;
     keywordData: {
        keyword: string;
     }
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await search(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_notice}/custom/notice-titles`,
        keywordData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);
//---------------------------------------------------------------------------------------------------------------------------------------------------------//

//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous retrieval of NoticeTitles, and depending on the response, either returns the data or an error message.
export const createNoticeTitle = createAsyncThunk(
  "NoticeTitles",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      postData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice:string;
    postData: {
        name: string;
         slug: string;
     }
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await post(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_notice}/custom/notice-titles`,
        postData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);
