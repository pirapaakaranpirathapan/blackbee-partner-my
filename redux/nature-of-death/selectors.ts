import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const natureOfDeathState = (state: RootState) => state.natureOfDeathReducer;

export const natureOfDeathSelector = createSelector(
    natureOfDeathState, 
    state => state
)
