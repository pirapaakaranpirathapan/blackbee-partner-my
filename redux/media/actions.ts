import { BASE_ROUTE } from "@/config/constants";
import { deleteMethod, get, patch, post } from "@/connections/fetch_wrapper";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { integer } from "aws-sdk/clients/cloudfront";

export const mediaUpload = createAsyncThunk(
  "media/mediaUpload",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      createData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
      createData: {
        url: string;
        media_type_id: any;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await post(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_notice}/media`,
        createData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);

export const mediaNoticeUpload = createAsyncThunk(
  "media/mediaUpload",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      createData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
      createData: {
        url: string;
        media_type_id: any;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await post(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_notice}/media`,
        createData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
export const mediaNoticeUpdate = createAsyncThunk(
  "media/mediaNoticeUpload",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      mediaUid,
      updateData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
      mediaUid: string;
      updateData: {
        url: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_notice}/media/${mediaUid}`,
        updateData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);

export const mediaNoticeGet = createAsyncThunk(
  "media/mediaNoticeget",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      page,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
      page: integer;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_notice}/media`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);

export const mediaDelete = createAsyncThunk(
  "media/mediaDelete",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      createData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
      createData: {
        url: string;
        media_type_id: any;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await post(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_notice}/media`,
        createData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);

// export const getMediaUpload = createAsyncThunk(
//   "media/getMediaUpload",
//   async (
//     {
//       active_partner,
//       active_service_provider,
//       active_notice,

//     }: {
//       active_partner: string;
//       active_service_provider: string;
//       active_notice: string;
//     },
//     { rejectWithValue }
//   ) => {
//     try {
//       const response = await post(
//         `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_notice}/media`,
//         createData
//       );
//       if (response.code == 200) {
//         return response;
//       } else {
//         return rejectWithValue(response.msg);
//       }
//     } catch (err: any) {
//       return rejectWithValue(err.message);
//     }
//   }
// );

export const getDeletedMedia = createAsyncThunk(
  "media/getDeletedMedia",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      page,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
      page: integer;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_notice}/media/deleted`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
export const postRestoreMedia = createAsyncThunk(
  "media/postRestoreMedia",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      image_id,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
      image_id: string;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_notice}/media/${image_id}/restore`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
export const getMedia = createAsyncThunk(
  "media/getMedia",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      page,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
      page: integer;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_notice}/media`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
export const deleteMedia = createAsyncThunk(
  "media/delete",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      uuid,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
      uuid: string;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await deleteMethod(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_notice}/media/${uuid}`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
export const deleteNoticeMedia = createAsyncThunk(
  "media/delete",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      uuid,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
      uuid: string;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await deleteMethod(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_notice}/media/${uuid}`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
export const updateCaptions = createAsyncThunk(
  "media/captions",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      uuid,
      updateData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
      uuid: string;
      updateData: {
        caption?: string;
        url?: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_notice}/media/${uuid}`,
        updateData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
export const updateNoticeCaptions = createAsyncThunk(
  "media/noticecaptions",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      uuid,
      updateData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
      uuid: string;
      updateData: {
        caption?: string;
        url?: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_notice}/media/${uuid}`,
        updateData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);