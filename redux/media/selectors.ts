import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const mediaState = (state: RootState) => state.mediaReducer;

export const mediaSelector = createSelector(
    mediaState, 
    state => state
)
