import { ResponseStatus } from "@/helpers/response_status";
import { createReducer } from "@reduxjs/toolkit";
import {
  getDeletedMedia,
  getMedia,
  mediaNoticeGet,
  mediaNoticeUpdate,
  mediaUpload,
  postRestoreMedia,
} from "./actions";

interface IReducerInitialState {
  data: any;
  deleteMedia: any;
  getMediaData: any;
  getNoticeMediaData: any;
  postData: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: [],
  postData: [],
  getMediaData: [],
  getNoticeMediaData: [],
  deleteMedia: [],
};

// Reducer for handling the "get all Memorials" action
export const mediaReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(mediaUpload.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(mediaUpload.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(mediaUpload.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  builder
    .addCase(mediaNoticeGet.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(mediaNoticeGet.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.getNoticeMediaData = payload?.data as any;
    })
    .addCase(mediaNoticeGet.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  builder
    .addCase(getDeletedMedia.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(getDeletedMedia.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.deleteMedia = payload?.data as any;
    })
    .addCase(getDeletedMedia.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  builder
    .addCase(getMedia.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.getMediaData = [];
    })
    .addCase(getMedia.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.getMediaData = payload?.data as any;
    })
    .addCase(getMedia.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });

  builder
    .addCase(postRestoreMedia.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(postRestoreMedia.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(postRestoreMedia.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });
  builder
    .addCase(mediaNoticeUpdate.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(mediaNoticeUpdate.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(mediaNoticeUpdate.rejected, (state, error) => {
      console.log("error", error);

      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });
});
export default mediaReducer;
