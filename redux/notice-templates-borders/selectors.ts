import { RootState } from "../store/store";
import { createSelector } from "@reduxjs/toolkit";

export const noticetemplatesBorderState = (state: RootState) => state. noticeTemplatesBorderReducer;
export const  noticetemplatesBorderSelector = createSelector( noticetemplatesBorderState, (state) => state);
