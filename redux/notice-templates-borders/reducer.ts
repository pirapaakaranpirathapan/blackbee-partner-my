import { ResponseStatus } from "@/helpers/response_status";
import { createReducer } from "@reduxjs/toolkit";
import {  getAllNoticeTemplateBorders } from "./actions";
import { INoticePages } from "@/interfaces/INoticePages";

interface IReducerInitialState {
  data: any ;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: [],
};

export const noticeTemplatesBorderReducer = createReducer(initialState, (builder) => {
  // ------------Reducer for handling the "get All notice templates" action------------//
  builder
    .addCase(getAllNoticeTemplateBorders.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(getAllNoticeTemplateBorders.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
       })
    .addCase(getAllNoticeTemplateBorders.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

 
});
export default noticeTemplatesBorderReducer;
