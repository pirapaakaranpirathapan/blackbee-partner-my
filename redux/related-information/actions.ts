import { BASE_ROUTE } from "@/config/constants";
import { get, post, patch } from "@/connections/fetch_wrapper";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { String } from "aws-sdk/clients/acm";


//This action function handles the asynchronous update of an existing customer with the provided data and returns the response data or an error message.
export const updateRelatedInformation = createAsyncThunk(
  "personal-information/update",
  async (
    {
      active_partner,
      active_service_provider,
      active_page,
      updatedData2,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_page: string;
      updatedData2: {
        // salutation_id: string;
        // name: string;
        // language_id: string;
        // lang_name: string;
        // nickname: string;
        // briefAbout: string;
        // quote: string;
        // gender_id: string;
        // dob: string;
        // dod: string;
        
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_page}`,
        updatedData2
      );
      if (response.code == 200) {  
        console.log("response",response);
              
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err) {
      console.log("response",err);

      return rejectWithValue(err);
    }
  }
);
