import { ResponseStatus } from '@/helpers/response_status';
import { createReducer } from '@reduxjs/toolkit';
import { updateRelatedInformation } from './actions';

interface IReducerInitialState {
  data: any;
  dataOne:any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}

const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  dataOne:[],
  data: [],
};

export const relatedInformationReducer = createReducer(initialState, (builder) => {

    // Reducer for handling the "update customer" action
     builder
     .addCase(updateRelatedInformation.pending, (state) => {
       state.status = ResponseStatus.LOADING;
     })
     .addCase(updateRelatedInformation.fulfilled, (state, { payload }) => {
       state.status = ResponseStatus.LOADED;
       state.data = payload?.data as any;
      // console.log("patch-response1payload" ,payload?.data); 

     })
     .addCase(updateRelatedInformation.rejected, (state, error) => {
       state.status = ResponseStatus.ERROR;
       state.error = error?.payload
      // console.log("patch-response1errorpayload" ,error?.payload); 

     });
});
export default relatedInformationReducer;
