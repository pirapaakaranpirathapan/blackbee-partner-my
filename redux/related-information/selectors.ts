import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const relatedInformationState = (state: RootState) => state.relatedInformationReducer;

export const relatedInformationSelector = createSelector(
    relatedInformationState, 
    state => state
)
