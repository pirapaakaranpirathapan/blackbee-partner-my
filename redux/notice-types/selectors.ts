import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const NoticeTypeState = (state: RootState) => state.noticeTypeReducer;

export const noticeTypeSelector = createSelector(
    NoticeTypeState, 
    state => state
)
