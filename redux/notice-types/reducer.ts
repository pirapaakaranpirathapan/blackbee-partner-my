import { ResponseStatus } from '@/helpers/response_status';
import { INoticeTitles } from '@/interfaces/INoticeTitles';
import { createReducer } from '@reduxjs/toolkit';
import {  getAllNoticeTypes } from './actions';
import { INoticeTypes } from '@/interfaces/INoticeTypes';

interface IReducerInitialState {
  data: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: [],
};

export const noticeTypeReducer = createReducer(initialState, (builder) => {

    builder
    .addCase(getAllNoticeTypes.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(getAllNoticeTypes.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(getAllNoticeTypes.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });
});
export default noticeTypeReducer;
