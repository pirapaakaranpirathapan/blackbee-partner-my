import { RootState } from "../store/store";
import { createSelector } from "@reduxjs/toolkit";

export const templatesState = (state: RootState) => state. templatesReducer;
export const  templatesSelector = createSelector( templatesState, (state) => state);
