import { Action, configureStore, ThunkAction } from "@reduxjs/toolkit";
import serviceReducer from "@/redux/customer-service/reducer";
import { authReducer } from "../auth";
import { languageReducer } from "../language";
import { memorialPageReducer } from "../memorial-pages";
import { customerPageReducer } from "../customer";
import { noticeReducer } from "../notices";
import { meReducer } from "../me";
import { serviceProviderReducer } from "../service-providers";
import { identityReducer } from "../identities";
import { templatesReducer } from "../notice-templates";
import { CountriesReducer } from "../countries";
import customerServiceReducer from "@/redux/customer-service/reducer";
import { noticeTypeReducer } from "../notice-types";
import { pageManagerReducer } from "../page-manager";
import { gendersReducer } from "../genders";
import { momorialContactReducer } from "../momorial-contact";
import { VisibilitiesReducer } from "../visibilities";
import { pageTypeReducer } from "../page-types";
import { personalLocationsReducer } from "../locations";
import { birthDeathTitlesReducer } from "../birth-death-titles";
import { noticeContactReducer } from "../notice-contact";
import { personalInformationReducer } from "../personal-information";
import { religionReducer } from "../religion";
import { natureOfDeathReducer } from "../nature-of-death";
import { relatedInformationReducer } from "../related-information";
import { noticeTitleReducer } from "../notice-titles";

import { pageTemplatesReducer } from "../page-templates";
import { pageTemplatesFrameReducer } from "../page-templates-frames";
import { pageTemplatesClipArtReducer } from "../page-templates-cliparts";
import { mediaReducer } from "../media";
import { relationshipsReducer } from "../relationships";
import { noticeTemplatesBorderReducer } from "../notice-templates-borders";
import { pageTemplatesBorderReducer } from "../page-templates-borders";
import { noticeTemplatesClipArtReducer } from "../notice-templates-cliparts";
import { noticeTemplatesFrameReducer } from "../notice-templates-frames";
import { noticePageBackgroundReducer } from "../notice-page-backgrounds";
import { pagePageBackgroundReducer } from "../page-page-backgrounds";
import { eventLocationTypeReducer } from "../event-location-type";
import { noticeEventReducer } from "../notices-event";
import { musicsReducer } from "../Music";
import { contactTypeReducer } from "../contact-type";
import { managerTypesReducer } from "../managerTypes";
import { partnerAdminReducer } from "../partner-admins";
import { getAllEventsTypesReducer } from "../event-types";
import cookiesReducer from "../get-cookies/reducer";

export const store = configureStore({
  reducer: {
    customerServiceReducer: customerServiceReducer,
    languageReducer: languageReducer,
    authReducer: authReducer,
    memorialPageReducer: memorialPageReducer,
    noticeReducer: noticeReducer,
    noticeEventReducer: noticeEventReducer,
    momorialContactReducer: momorialContactReducer,
    meReducer: meReducer,
    customerPageReducer: customerPageReducer,
    serviceProviderReducer: serviceProviderReducer,
    identityReducer: identityReducer,
    personalLocationsReducer: personalLocationsReducer,
    templatesReducer: templatesReducer,
    CountriesReducer: CountriesReducer,
    pageManagerReducer: pageManagerReducer,
    noticeTypeReducer: noticeTypeReducer,
    gendersReducer: gendersReducer,
    getAllEventsTypesReducer: getAllEventsTypesReducer,
    contactTypeReducer: contactTypeReducer,
    eventLocationTypeReducer: eventLocationTypeReducer,
    partnerAdminReducer: partnerAdminReducer,
    VisibilitiesReducer: VisibilitiesReducer,
    pageTypeReducer: pageTypeReducer,
    birthDeathTitlesReducer: birthDeathTitlesReducer,
    noticeContactReducer: noticeContactReducer,
    personalInformationReducer: personalInformationReducer,
    religionReducer: religionReducer,
    natureOfDeathReducer: natureOfDeathReducer,
    relatedInformationReducer: relatedInformationReducer,
    noticeTitleReducer: noticeTitleReducer,
    pageTemplatesReducer: pageTemplatesReducer,
    pageTemplatesBorderReducer: pageTemplatesBorderReducer,
    pageTemplatesFrameReducer: pageTemplatesFrameReducer,
    pageTemplatesClipArtReducer: pageTemplatesClipArtReducer,
    mediaReducer: mediaReducer,
    relationshipsReducer: relationshipsReducer,
    noticeTemplatesBorderReducer: noticeTemplatesBorderReducer,
    noticeTemplatesClipArtReducer: noticeTemplatesClipArtReducer,
    noticeTemplatesFrameReducer: noticeTemplatesFrameReducer,
    noticePageBackgroundReducer: noticePageBackgroundReducer,
    pagePageBackgroundReducer: pagePageBackgroundReducer,
    musicsReducer: musicsReducer,
    managerTypesReducer: managerTypesReducer,
    cookiesReducer:cookiesReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
