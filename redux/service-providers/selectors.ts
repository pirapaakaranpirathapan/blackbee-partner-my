import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const serviceProviderState = (state: RootState) => state.serviceProviderReducer;

export const serviceProviderSelector = createSelector(
    serviceProviderState, 
    state => state
)
