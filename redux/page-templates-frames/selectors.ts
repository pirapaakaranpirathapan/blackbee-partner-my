import { RootState } from "../store/store";
import { createSelector } from "@reduxjs/toolkit";

export const pagetemplatesFrameState = (state: RootState) => state. pageTemplatesFrameReducer;
export const  pagetemplatesFrameSelector = createSelector( pagetemplatesFrameState, (state) => state);
