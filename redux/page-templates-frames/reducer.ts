import { ResponseStatus } from "@/helpers/response_status";
import { createReducer } from "@reduxjs/toolkit";
import {  getAllPageTemplateFrames } from "./actions";
import { INoticePages } from "@/interfaces/INoticePages";

interface IReducerInitialState {
  data: any ;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: [],
};

export const pageTemplatesFrameReducer = createReducer(initialState, (builder) => {
  // ------------Reducer for handling the "get All page templates" action------------//
  builder
    .addCase(getAllPageTemplateFrames.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(getAllPageTemplateFrames.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
       })
    .addCase(getAllPageTemplateFrames.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

 
});
export default pageTemplatesFrameReducer;
