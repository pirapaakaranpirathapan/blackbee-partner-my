import { ResponseStatus } from '@/helpers/response_status';
import { IAuth } from '@/interfaces/IAuth';
import { IApiResponse } from '@/interfaces/manage/IApiResponse';
import { createReducer } from '@reduxjs/toolkit';
import { login, logout } from './actions';
import { getCookie, removeCookies, setCookie } from 'cookies-next';
import router, { useRouter } from 'next/router'
import { encryptToken } from '@/utils/encryptionAndDecryptionUtils';


interface IReducerInitialState {
  data: IAuth ;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: [] as unknown as String[],
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: {} as unknown as IAuth,
};

//  The `authReducer` manages the state related to authentication.
//  It handles login and logout actions, updating the state accordingly
export const authReducer = createReducer(initialState, (builder) => {

  //login
  builder
    .addCase(login.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(login.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      let data = payload as IApiResponse
      state.data = data.data;
      const accessToken =data.data.data?.["access_token"];
      // setCookie('token', data.data.data?.["access_token"]);
      const encryptedToken = encryptToken(accessToken);
      // setCookie('token', encryptedToken, { secure: true, httpOnly: true });
      setCookie('token', encryptedToken);

      //  window.location.href = '/dashboard'
    })
    .addCase(login.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;      
    });

    //logout
    builder
    .addCase(logout.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.data = initialState.data;
    })
    .addCase(logout.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = initialState.data;
      removeCookies('token');
      // removeCookies('activepartner');
      // removeCookies('activeserviceprovider');
      // removeCookies('serviceprovidername')
      router.push('/');
    })
    .addCase(logout.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });


});
export default authReducer;
