import { BASE_ROUTE } from "@/config/constants";
import { get } from "@/connections/fetch_wrapper";
import { createAsyncThunk } from "@reduxjs/toolkit";

//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous retrieval of genders, and depending on the response, either returns the data or an error message.

export const getAllContactTypes = createAsyncThunk(
  "contact-methods/getall",
  async (_, { rejectWithValue }) => {
    try {
      const response = await get(`${BASE_ROUTE}/contact-methods`);
      if (response.code === 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
