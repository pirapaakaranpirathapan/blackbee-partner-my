import { ResponseStatus } from "@/helpers/response_status";
import { createReducer } from "@reduxjs/toolkit";
import { getAllContactTypes } from "./actions";

interface IReducerInitialState {
  data: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: [],
};

export const contactTypeReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(getAllContactTypes.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(getAllContactTypes.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(getAllContactTypes.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });
});
export default contactTypeReducer;
