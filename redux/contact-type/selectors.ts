import { RootState } from "../store/store";
import { createSelector } from "@reduxjs/toolkit";

export const contactTypeState = (state: RootState) => state.contactTypeReducer;

export const contactTypeSelector = createSelector(
  contactTypeState,
  (state) => state
);
