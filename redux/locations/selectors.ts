import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const personalLocationsState = (state: RootState) => state.personalLocationsReducer;

export const personalLocationsSelector = createSelector(
    personalLocationsState, 
    state => state
)
