import { BASE_ROUTE } from "@/config/constants";
import { patch } from "@/connections/fetch_wrapper";
import { createAsyncThunk } from "@reduxjs/toolkit";


//This action function handles the asynchronous update of an existing customer with the provided data and returns the response data or an error message.
export const updatePersonalLocations = createAsyncThunk(
  "locations/update",
  async (
    {
      active_partner,
      active_service_provider,
      active_page,
      updatedData1,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_page: string;
      updatedData1: {
        death_location: string,
        // lived_locations: string[],
        // countryOrigin: countryOrigin,
        hometown_location: string,
        country_id: string,
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_page}`,
        updatedData1
      );
      if (response.code == 200) {  
        console.log("responselocations",response);
              
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err) {
      console.log("responselocations",err);

      return rejectWithValue(err);
    }
  }
);
