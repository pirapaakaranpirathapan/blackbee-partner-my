import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const VisibilitiesState = (state: RootState) => state.VisibilitiesReducer;

export const VisibilitiesSelector = createSelector(
    VisibilitiesState, 
    state => state
)
