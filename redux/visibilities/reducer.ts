import { ResponseStatus } from '@/helpers/response_status';
import { ILanguages } from '@/interfaces/ILanguages';
import { createReducer } from '@reduxjs/toolkit';
import {  getAllVisibilities} from './actions';

interface IReducerInitialState {
  data: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: [],
};

export const VisibilitiesReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(getAllVisibilities.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(getAllVisibilities.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(getAllVisibilities.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });
});
export default VisibilitiesReducer;
