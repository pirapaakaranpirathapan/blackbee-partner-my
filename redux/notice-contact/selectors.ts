import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const noticeContactState = (state: RootState) => state.noticeContactReducer;

export const noticeContactSelector = createSelector(
    noticeContactState, 
    state => state
)
