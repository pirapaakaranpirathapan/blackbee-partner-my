import { BASE_ROUTE } from "@/config/constants";
import { deleteMethod, get, patch, post } from "@/connections/fetch_wrapper";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { integer } from "aws-sdk/clients/cloudfront";

//This action function handles the asynchronous retrieval of all notice-contact, and depending on the response, either returns the data or an error message.
export const getAllNoticesContact = createAsyncThunk(
  "notices-contact/getall",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      page,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: String;
      page: integer;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_notice}/contacts`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);

//This action function handles the asynchronous creation of a new notice-contact with the provided data and returns the notice-contact data or an error message.
export const createNoticeContact = createAsyncThunk(
  "notice-contact/create",
  async (
    {
      activePartner,
      activeServiceProvider,
      activeNotice,
      createData,
    }: {
      activePartner: string;
      activeServiceProvider: String;
      activeNotice: String;
      createData: {
        name: string;
        country_id: integer;
        relationship_id: integer;
        contact_details: object[];
        // additional_text: string;
        status: string;
        auto_hide: boolean;
        visibility_id: integer;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await post(
        `${BASE_ROUTE}/${activePartner}/${activeServiceProvider}/products/notices/${activeNotice}/contacts`,
        createData
      );

      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const deleteNoticeContact = createAsyncThunk(
  "momorial-contact/delete",
  async (
    {
      activePartner,
      activeServiceProvider,
      activePageId,
      contactId,
    }: {
      activePartner: string;
      activeServiceProvider: String;
      activePageId: String;
      contactId: string;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await deleteMethod(
        `${BASE_ROUTE}/${activePartner}/${activeServiceProvider}/products/notices/${activePageId}/contacts/${contactId}`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

//This action function handles the asynchronous update of a new notice-contact with the provided data and returns the notice-contact data or an error message.
export const updateNoticeContact = createAsyncThunk(
  "notice-contact/update",
  async (
    {
      activePartner,
      activeServiceProvider,
      activeNotice,
      updatedData,
      uuid,
    }: {
      activePartner: string;
      activeServiceProvider: String;
      activeNotice: String;
      uuid: string;
      updatedData: {
        name: string;
        country_id: integer;
        relationship_id: integer;
        contact_details: object[];
        // additional_text: string;
        auto_hide: string;
        visibility_id: integer;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${activePartner}/${activeServiceProvider}/products/notices/${activeNotice}/contacts/${uuid}`,
        updatedData
      );

      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);