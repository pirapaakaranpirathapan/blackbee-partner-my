import { ResponseStatus } from "@/helpers/response_status";
import { createReducer } from "@reduxjs/toolkit";
import {
  createNoticeContact,
  getAllNoticesContact,
  updateNoticeContact,
} from "./actions";

interface IReducerInitialState {
  data: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: [],
};

// Reducer for handling the "get all Notice contacts" action
export const noticeContactReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(getAllNoticesContact.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(getAllNoticesContact.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(getAllNoticesContact.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

  // Reducer for handling the "create notice-contact" action
  builder
    .addCase(createNoticeContact.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(createNoticeContact.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(createNoticeContact.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });
  builder
    .addCase(updateNoticeContact.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(updateNoticeContact.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(updateNoticeContact.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
    });
});

export default noticeContactReducer;
