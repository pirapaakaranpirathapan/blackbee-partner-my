import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const personalInformationState = (state: RootState) => state.personalInformationReducer;

export const personalInformationSelector = createSelector(
    personalInformationState, 
    state => state
)
