import { ResponseStatus } from '@/helpers/response_status';
import { createReducer } from '@reduxjs/toolkit';
import { updatePersonalInformation } from './actions';

interface IReducerInitialState {
  data: any;
  dataOne:any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}

const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  dataOne:[],
  data: [],
};

export const personalInformationReducer = createReducer(initialState, (builder) => {

    // Reducer for handling the "update customer" action
     builder
     .addCase(updatePersonalInformation.pending, (state) => {
       state.status = ResponseStatus.LOADING;
     })
     .addCase(updatePersonalInformation.fulfilled, (state, { payload }) => {
       state.status = ResponseStatus.LOADED;
       state.data = payload?.data as any;
      // console.log("patch-response1payload" ,payload?.data); 

     })
     .addCase(updatePersonalInformation.rejected, (state, error) => {
       state.status = ResponseStatus.ERROR;
       state.error = error?.payload

     });
});
export default personalInformationReducer;
