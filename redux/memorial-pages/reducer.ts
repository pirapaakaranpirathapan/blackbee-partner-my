import {ResponseStatus} from '@/helpers/response_status'
import {IMemorialPage} from '@/interfaces/IMemorialPages'
import {createReducer} from '@reduxjs/toolkit'
import {
  createMemorialPage,
  getAllMemorialPages,
  globalSearchDeathPerson,
  patchGenralSettings,
  patchMemorialClient,
  patchPageDesign,
  patchPageTemplatesBorder,
  patchPageTemplatesClipArt,
  patchPageTemplatesframe,
  patchPageTemplatess,
  searchDeathPerson,
  showPage,
} from './actions'

interface IReducerInitialState {
  data: any
  dataOne: any
  postData: any
  searchData: any
  status: ResponseStatus
  error: any
  added: boolean
  updated: boolean
  deleted: boolean
  pending: boolean
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: [],
  dataOne: [],
  postData: [],
  searchData: [],
}

// Reducer for handling the "get all Memorials" action
export const memorialPageReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(getAllMemorialPages.pending, (state) => {
      state.status = ResponseStatus.LOADING
      state.data = []
    })
    .addCase(getAllMemorialPages.fulfilled, (state, {payload}) => {
      state.status = ResponseStatus.LOADED
      state.data = payload?.data as any
      // console.log("getAll>>>>",payload?.data.data);
    })
    .addCase(getAllMemorialPages.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR
    })
  builder
    .addCase(showPage.pending, (state) => {
      state.status = ResponseStatus.LOADING
      state.data = []
    })
    .addCase(showPage.fulfilled, (state, {payload}) => {
      state.status = ResponseStatus.LOADED
      // state.data = [payload?.data] as IMemorialPage[];
      state.dataOne = payload.data as any
    })
    .addCase(showPage.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR
    })
  // ------------Reducer for handling the "patch Notice" action------------//
  builder
    .addCase(patchGenralSettings.pending, (state) => {
      state.status = ResponseStatus.LOADING
    })
    .addCase(patchGenralSettings.fulfilled, (state, {payload}) => {
      state.status = ResponseStatus.LOADED
      state.data = payload?.data as any
    })
    .addCase(patchGenralSettings.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR
      state.error = error?.payload
    })
  // ------------Reducer for handling the "patch template" action------------//
  builder
    .addCase(patchPageTemplatess.pending, (state) => {
      state.status = ResponseStatus.LOADING
    })
    .addCase(patchPageTemplatess.fulfilled, (state, {payload}) => {
      state.status = ResponseStatus.LOADED
      state.data = payload?.data as any
    })
    .addCase(patchPageTemplatess.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR
      state.error = error?.payload
    })
  // ------------Reducer for handling the "patch template border" action------------//
  builder
    .addCase(patchPageTemplatesBorder.pending, (state) => {
      state.status = ResponseStatus.LOADING
    })
    .addCase(patchPageTemplatesBorder.fulfilled, (state, {payload}) => {
      state.status = ResponseStatus.LOADED
      state.data = payload?.data as any
    })
    .addCase(patchPageTemplatesBorder.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR
      state.error = error?.payload
    })

  // ------------Reducer for handling the "patch template frame" action------------//
  builder
    .addCase(patchPageTemplatesframe.pending, (state) => {
      state.status = ResponseStatus.LOADING
    })
    .addCase(patchPageTemplatesframe.fulfilled, (state, {payload}) => {
      state.status = ResponseStatus.LOADED
      state.data = payload?.data as any
    })
    .addCase(patchPageTemplatesframe.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR
      state.error = error?.payload
    })

  // ------------Reducer for handling the "patch template clipart" action------------//
  builder
    .addCase(patchPageTemplatesClipArt.pending, (state) => {
      state.status = ResponseStatus.LOADING
    })
    .addCase(patchPageTemplatesClipArt.fulfilled, (state, {payload}) => {
      state.status = ResponseStatus.LOADED
      state.data = payload?.data as any
    })
    .addCase(patchPageTemplatesClipArt.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR
      state.error = error?.payload
    })

  // Reducer for handling the "create customer" action
  builder
    .addCase(createMemorialPage.pending, (state) => {
      state.status = ResponseStatus.LOADING
    })
    .addCase(createMemorialPage.fulfilled, (state, {payload}) => {
      state.status = ResponseStatus.LOADED
      state.postData = payload?.data as any
      // console.log("payload?.datasss",payload?.data.data);
    })
    .addCase(createMemorialPage.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR
      state.error = error?.payload
    })

  builder
    .addCase(patchMemorialClient.pending, (state) => {
      state.status = ResponseStatus.LOADING
    })
    .addCase(patchMemorialClient.fulfilled, (state, {payload}) => {
      state.status = ResponseStatus.LOADED
      state.postData = payload?.data as any
      // console.log("payload?.datasss",payload?.data.data);
    })
    .addCase(patchMemorialClient.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR
      state.error = error?.payload
    })

  //now this is a sample api for search because we dont have the actual search api  (dummy)
  builder
    .addCase(searchDeathPerson.pending, (state) => {
      state.status = ResponseStatus.LOADING
    })
    .addCase(searchDeathPerson.fulfilled, (state, {payload}) => {
      state.status = ResponseStatus.LOADED
      state.searchData = payload?.data as any
    })
    .addCase(searchDeathPerson.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR
    })

  //now this is a sample api for search because we dont have the actual search api  (dummy)
  builder
    .addCase(globalSearchDeathPerson.pending, (state) => {
      state.status = ResponseStatus.LOADING
    })
    .addCase(globalSearchDeathPerson.fulfilled, (state, {payload}) => {
      state.status = ResponseStatus.LOADED
      state.searchData = payload?.data as any
    })
    .addCase(globalSearchDeathPerson.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR
    })

  builder
    .addCase(patchPageDesign.pending, (state) => {
      state.status = ResponseStatus.LOADING
    })
    .addCase(patchPageDesign.fulfilled, (state, {payload}) => {
      state.status = ResponseStatus.LOADED
      state.data = payload?.data as any
    })
    .addCase(patchPageDesign.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR
      state.error = error?.payload
    })
})
export default memorialPageReducer
