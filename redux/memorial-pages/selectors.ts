import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const memorialPageState = (state: RootState) => state.memorialPageReducer;

export const memorialPageSelector = createSelector(
    memorialPageState, 
    state => state
)
