import { BASE_ROUTE } from "@/config/constants";
import { get, patch, post, search } from "@/connections/fetch_wrapper";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { integer } from "aws-sdk/clients/cloudfront";
import { getCookie } from "cookies-next";

//This action function handles the asynchronous retrieval of all Memorials, and depending on the response, either returns the data or an error message.
export const getAllMemorialPages = createAsyncThunk(
  "GetAll/Memorials",
  async (
    {
      active_partner,
      active_service_provider,
      page,
    }: {
      active_partner: string;
      active_service_provider: string;
      page: integer;
    },
    { rejectWithValue }
  ) => {
    try {
      const activePartner = getCookie("activepartner") as string;
      const activeServiceProvider = getCookie(
        "activeserviceprovider"
      ) as string;
      const response = await get(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages?page=${page}`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous retrieval of particular page, and depending on the response, either returns the data or an error message.
export const showPage = createAsyncThunk(
  "pages/showpage",
  async (
    {
      active_partner,
      active_service_provider,
      active_page,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_page: string;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_page}`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
//---------------------------------------------------------------------------------------------------------------------------------------------------------//

//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous update for  particular GenralSettings , and depending on the response, either returns the data or an error message.
export const patchGenralSettings = createAsyncThunk(
  "notices/patchnotice",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      updatedData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
      updatedData: {
        visibility_id: string;
        page_type_id: string;
        relationship_id: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_notice}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);

// export const mediaUpload = createAsyncThunk(
//   "media/mediaUpload",
//   async (
//     {
//       active_partner,
//       active_service_provider,
//       active_notice,
//       createData,
//     }: {
//       active_partner: string;
//       active_service_provider: string;
//       active_notice: string;
//       createData: {
//         url:string;
//         media_type_id:any;
//       };
//     },
//     { rejectWithValue }
//   ) => {
//     try {
//       const response = await post(
//         `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_notice}/media`,
//         createData
//       );
//       if (response.code == 200) {
//         return response;
//       } else {
//         return rejectWithValue(response.msg);
//       }
//     } catch (err: any) {
//       return rejectWithValue(err.message);
//     }
//   }
// );

// export const mediaDelete = createAsyncThunk(
//   "media/mediaDelete",
//   async (
//     {
//       active_partner,
//       active_service_provider,
//       active_notice,
//       createData,
//     }: {
//       active_partner: string;
//       active_service_provider: string;
//       active_notice: string;
//       createData: {
//         url:string;
//         media_type_id:any;
//       };
//     },
//     { rejectWithValue }
//   ) => {
//     try {
//       const response = await post(
//         `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_notice}/media`,
//         createData
//       );
//       if (response.code == 200) {
//         return response;
//       } else {
//         return rejectWithValue(response.msg);
//       }
//     } catch (err: any) {
//       return rejectWithValue(err.message);
//     }
//   }
// );

export const createMemorialPage = createAsyncThunk(
  "memorial/create",
  async (
    {
      activePartner,
      activeServiceProvider,
      CreateData,
    }: {
      activePartner: string;
      activeServiceProvider: String;
      CreateData: {
        name: any;
        client_id: any;
        country_id: any;
        page_type_id: string;
        salutation_id: integer;
        relationship_id: any;
        dod: any;
        dob: any;
        death_location: any;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await post(
        `${BASE_ROUTE}/${activePartner}/${activeServiceProvider}/products/pages`,
        CreateData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

// export const getMediaUpload = createAsyncThunk(
//   "media/getMediaUpload",
//   async (
//     {
//       active_partner,
//       active_service_provider,
//       active_notice,

//     }: {
//       active_partner: string;
//       active_service_provider: string;
//       active_notice: string;
//     },
//     { rejectWithValue }
//   ) => {
//     try {
//       const response = await post(
//         `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_notice}/media`,
//         createData
//       );
//       if (response.code == 200) {
//         return response;
//       } else {
//         return rejectWithValue(response.msg);
//       }
//     } catch (err: any) {
//       return rejectWithValue(err.message);
//     }
//   }
// );
//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous update for  particular page templates , and depending on the response, either returns the data or an error message.
export const patchPageTemplatess = createAsyncThunk(
  "pages/patchrtemplates",
  async (
    {
      active_partner,
      active_service_provider,
      active_page,
      updatedData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_page: string;
      updatedData: {
        page_template_id: number;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_page}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);

//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous update for  particular page templates , and depending on the response, either returns the data or an error message.
export const patchPageTemplatesBorder = createAsyncThunk(
  "pages/patchborders",
  async (
    {
      active_partner,
      active_service_provider,
      active_page,
      updatedData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_page: string;
      updatedData: {
        border_id: number;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_page}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);

//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous update for  particular page templates clipartr , and depending on the response, either returns the data or an error message.
export const patchPageTemplatesClipArt = createAsyncThunk(
  "pages/patchcliparts",
  async (
    {
      active_partner,
      active_service_provider,
      active_page,
      updatedData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_page: string;
      updatedData: {
        clip_art_id: number;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_page}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);

//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous update for  particular page templates frame , and depending on the response, either returns the data or an error message.
export const patchPageTemplatesframe = createAsyncThunk(
  "pages/patchfarmes",
  async (
    {
      active_partner,
      active_service_provider,
      active_page,
      updatedData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_page: string;
      updatedData: {
        frame_id: number;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_page}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);
//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous update for  particular page_background_id , and depending on the response, either returns the data or an error message.
export const patchPagePageBackground = createAsyncThunk(
  "pages/patchh",
  async (
    {
      active_partner,
      active_service_provider,
      active_page,
      updatedData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_page: string;
      updatedData: {
        page_background_id: number;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_page}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);

// export const getDeletedMedia = createAsyncThunk(
//   "media/getDeletedMedia",
//   async (
//     {
//       active_partner,
//       active_service_provider,
//       active_notice
//     }: {
//       active_partner: string;
//       active_service_provider: string;
//       active_notice: string;
//     },
//     { rejectWithValue }
//   ) => {
//     try {
//       const response = await get(
//         `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_notice}/media/deleted`
//       );
//       if (response.code == 200) {
//         return response;
//       } else {
//         return rejectWithValue(response.msg);
//       }
//     } catch (err: any) {
//       return rejectWithValue(err.message);
//     }
//   }
// );
// export const postRestoreMedia = createAsyncThunk(
//   "media/postRestoreMedia",
//   async (
//     {
//       active_partner,
//       active_service_provider,
//       active_notice,
//       image_id
//     }: {
//       active_partner: string;
//       active_service_provider: string;
//       active_notice: string;
//       image_id:string;
//     },
//     { rejectWithValue }
//   ) => {
//     try {
//       const response = await get(
//         `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_notice}/media/${image_id}/restore`
//       );
//       if (response.code == 200) {
//         return response;
//       } else {
//         return rejectWithValue(response.msg);
//       }
//     } catch (err: any) {
//       return rejectWithValue(err.message);
//     }
//   }
// );

export const pageUpdate = createAsyncThunk(
  "pages/patchfarmes",
  async (
    {
      active_partner,
      active_service_provider,
      active_page,
      updatedData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_page: string;
      updatedData: {
        header_background: {
          header_background_url?: string;
        };

        page_background_url?: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_page}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);

//This action function handles the asynchronous update for  particular page templates , and depending on the response, either returns the data or an error message.
export const patchMemorialClient = createAsyncThunk(
  "pages/patchMemorialClient",
  async (
    {
      active_partner,
      active_service_provider,
      active_id,
      updatedData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_id: string;
      updatedData: {
        client_id: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_id}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);

//now this is a sample api for search because we dont have the actual search api  (dummy)
export const searchDeathPerson = createAsyncThunk(
  "DeathPerson/search",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      keywordData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
      keywordData: {
        keyword: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await search(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_notice}/custom/notice-titles`,
        keywordData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);

//now this is a sample api for search because we dont have the actual search api  (dummy) //global
export const globalSearchDeathPerson = createAsyncThunk(
  "DeathPerson/globalsearch",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      keywordData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
      keywordData: {
        keyword: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await search(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_notice}/custom/notice-titles?type="global"`,
        keywordData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);

//This action function handles the asynchronous update for  particular page templates frame , and depending on the response, either returns the data or an error message.
export const patchPageDesign = createAsyncThunk(
  "pages/patchDesign",
  async (
    {
      active_partner,
      active_service_provider,
      active_page,
      updatedData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_page: string;
      updatedData: {
        clip_art_id?: number;
        border_id?: number;
        page_template_id?: number;
        frame_id: number;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_page}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);
