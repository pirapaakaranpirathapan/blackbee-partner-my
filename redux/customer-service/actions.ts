import { createAction, createAsyncThunk } from '@reduxjs/toolkit';
import { get } from '../../connections/fetch_wrapper';
import { BASE_ROUTE } from '@/config/constants';

// GET ALL FUNCTION ==========================================================================================
export const getAllCustomerServices= createAsyncThunk(
  "customer/get-all-services",
  async (
    {
      active_partner,
      active_service_provider,
      client_id,
    }: { active_partner: string; active_service_provider: string;client_id:string },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/clients/${client_id}/products`
        // `${BASE_ROUTE}/${active_partner}/${active_service_provider}/clients/${client_id}/products/notices`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);

export const getAllCustomerNotices= createAsyncThunk(
  "customer/get-all-notice-services",
  async (
    {
      active_partner,
      active_service_provider,
      client_id,
    }: { active_partner: string; active_service_provider: string;client_id:string },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/clients/${client_id}/products/notices`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
