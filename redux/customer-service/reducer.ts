// import { IServiceList,IService } from '../../interfaces/IService';
import { getAllCustomerNotices, getAllCustomerServices } from './actions';
import { createReducer } from '@reduxjs/toolkit';
import { ResponseStatus } from '../../helpers/response_status';

interface IReducerInitialState {
  // services: IServiceList ,
  data: any ,
  noticesData:any,
  service: any,
  // service: IService,
  status : ResponseStatus,
  error: any,
  added: boolean,
  updated: boolean,
  deleted : boolean,
  pending: boolean,
};

const initialState: IReducerInitialState = {
  data:[],
  noticesData:[],
  // services: [] as unknown as IServiceList,
  // service: {} as unknown as IService,
  service: {} ,
  error: { } ,
  status : ResponseStatus.INITIAL,
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  
};

const customerServiceReducer = createReducer(initialState, builder => {
  builder
    .addCase(getAllCustomerServices.pending, state => {
      state.status = ResponseStatus.LOADING;
      state.data = [];
    })
    .addCase(getAllCustomerServices.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data =  payload?.data as any;
      // console.log("payload",payload?.data); 
    })
    .addCase(getAllCustomerServices.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error =  error as any;

    })

    builder
    .addCase(getAllCustomerNotices.pending, state => {
      state.status = ResponseStatus.LOADING;
      state.noticesData =  []
    })
    .addCase(getAllCustomerNotices.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.noticesData =  payload?.data as any;
      // console.log("payload",payload);
      //  console.log("ClientNoticeServices>>>>payload",payload?.data);
    })
    .addCase(getAllCustomerNotices.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error =  error as any;
    })
});
export default customerServiceReducer;


