import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const customerServiceState = (state: RootState) => state.customerServiceReducer;

export const customerServiceSelector = createSelector(customerServiceState, (state) => state);
