import { ResponseStatus } from '@/helpers/response_status';
import { ILanguages } from '@/interfaces/ILanguages';
import { createReducer } from '@reduxjs/toolkit';
import { getAllManagerTypes } from './actions';

interface IReducerInitialState {
  data: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: [],
};

export const managerTypesReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(getAllManagerTypes.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(getAllManagerTypes.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(getAllManagerTypes.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });
});
export default managerTypesReducer;
