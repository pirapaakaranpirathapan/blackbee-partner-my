import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const managerTypesState = (state: RootState) => state.managerTypesReducer;

export const managerTypesSelector = createSelector(
    managerTypesState, 
    state => state
)
