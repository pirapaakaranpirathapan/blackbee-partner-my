import { BASE_ROUTE } from "@/config/constants";
import { get } from "@/connections/fetch_wrapper";
import { createAsyncThunk } from "@reduxjs/toolkit";


//---------------------------------------------------------------------------------------------------------------------------------------------------------//

//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous retrieval of BirthAndDeathTitles , and depending on the response, either returns the data or an error message.
export const getAllBirthAndDeathTitles = createAsyncThunk(
  "birthdeathtitles",
  async () => {
    try {
      const response = await get(`${BASE_ROUTE}/birth-and-death-titles`);

      if (response.code == 200) {
        return response;
      } else {
        rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
function rejectWithValue(msg: string): any {
  throw new Error("Function not implemented.");
}


export const getBirthAndDeathTitle = createAsyncThunk(
  "BirthAndDeathTitle ",
  async (
    {
      active_page,
    }: {
      active_page: string;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/birth-and-death-titles/${active_page}`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);