import { ResponseStatus } from '@/helpers/response_status';
import { ILanguages } from '@/interfaces/ILanguages';
import { createReducer } from '@reduxjs/toolkit';
import {  getAllBirthAndDeathTitles } from './actions';
import { IBirthDeathTitles } from '@/interfaces/IBirthDeathTitles';

interface IReducerInitialState {
  data: IBirthDeathTitles;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: {} as unknown as IBirthDeathTitles,
};

export const birthDeathTitlesReducer = createReducer(initialState, (builder) => {

    builder
    .addCase(getAllBirthAndDeathTitles.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(getAllBirthAndDeathTitles.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(getAllBirthAndDeathTitles.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });
});
export default birthDeathTitlesReducer;
