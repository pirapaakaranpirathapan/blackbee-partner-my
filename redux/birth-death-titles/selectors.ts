import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const birthDeathTitlesState = (state: RootState) => state.birthDeathTitlesReducer;

export const birthDeathTitlesSelector = createSelector(
    birthDeathTitlesState, 
    state => state
)
