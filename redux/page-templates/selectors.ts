import { RootState } from "../store/store";
import { createSelector } from "@reduxjs/toolkit";

export const pagetemplatesState = (state: RootState) => state. pageTemplatesReducer;
export const  pagetemplatesSelector = createSelector( pagetemplatesState, (state) => state);
