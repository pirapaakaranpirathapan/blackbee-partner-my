import { BASE_ROUTE } from "@/config/constants";
import { get, patch, post } from "@/connections/fetch_wrapper";
import { createAsyncThunk } from "@reduxjs/toolkit";
//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous retrieval of all PageTemplates, and depending on the response, either returns the data or an error message.
export const getAllPageTemplates = createAsyncThunk(
  "pages/getall",
  async (
    {
      active_partner,
    
    }: {
      active_partner: string;
     
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/${active_partner}/page-templates`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);

