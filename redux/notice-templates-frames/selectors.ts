import { RootState } from "../store/store";
import { createSelector } from "@reduxjs/toolkit";

export const noticetemplatesFrameState = (state: RootState) => state. noticeTemplatesFrameReducer;
export const  noticetemplatesFrameSelector = createSelector( noticetemplatesFrameState, (state) => state);
