import { BASE_ROUTE } from "@/config/constants";
import { get, patch, post } from "@/connections/fetch_wrapper";
import { createAsyncThunk } from "@reduxjs/toolkit";
//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous retrieval of all Notices, and depending on the response, either returns the data or an error message.
export const getAllSalutations = createAsyncThunk(
  "salutations/getall",
  async () => {
    try {
      const response = await get(`${BASE_ROUTE}/salutations`);
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
//---------------------------------------------------------------------------------------------------------------------------------------------------------//

//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous retrieval of particular Notice, and depending on the response, either returns the data or an error message.
export const getSalutation = createAsyncThunk(
  "salutations/showsalutation",
  async (
    {
      active_notice,
    }: {
     
      active_notice: string;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/salutations/${active_notice}`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
function rejectWithValue(msg: string): any {
  throw new Error("Function not implemented.");
}

