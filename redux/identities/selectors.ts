import { RootState } from "../store/store";
import { createSelector } from "@reduxjs/toolkit";

export const identityState = (state: RootState) => state. identityReducer;
export const  identitySelector = createSelector( identityState, (state) => state);
