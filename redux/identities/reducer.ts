import { ResponseStatus } from "@/helpers/response_status";
import { createReducer } from "@reduxjs/toolkit";
import { getAllSalutations } from "./actions";
import { INoticePages } from "@/interfaces/INoticePages";

interface IReducerInitialState {
  data: any ;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: [],
};

export const identityReducer = createReducer(initialState, (builder) => {
  // ------------Reducer for handling the "get All Salutations" action------------//
  builder
    .addCase(getAllSalutations.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(getAllSalutations.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
       })
    .addCase(getAllSalutations.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

 
});
export default identityReducer;
