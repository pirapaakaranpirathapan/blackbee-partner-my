import { RootState } from "../store/store";
import { createSelector } from "@reduxjs/toolkit";

export const noticeState = (state: RootState) => state.noticeReducer;
export const noticeSelector = createSelector(noticeState, (state) => state);