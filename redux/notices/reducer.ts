import { ResponseStatus } from "@/helpers/response_status";
import { createReducer } from "@reduxjs/toolkit";
import { createEvent, getAllNotices, patcNoticePageBackground, patcNoticeSettings, patcNoticeTemplatesframe, patchLiveStreamVideo, patchNickName, patchNotice, patchNoticeClient, patchNoticeGroupPage, patchNoticeSettingDesign, patchNoticeTemplatesBorder, patchNoticeTemplatesClipArt, patchNoticeTemplatess, postNotice, showNotice } from "./actions";
import { INoticePages } from "@/interfaces/INoticePages";

interface IReducerInitialState {
  data: any ;
  dataOne:any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: [],
  dataOne:[],
};

export const noticeReducer = createReducer(initialState, (builder) => {
  // ------------Reducer for handling the "get all Notices" action------------//
  builder
    .addCase(getAllNotices.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(getAllNotices.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(getAllNotices.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });

  // ------------Reducer for handling the "show Notice" action------------//
  builder
    .addCase(showNotice.pending, (state) => {
      state.status = ResponseStatus.LOADING;
      state.dataOne =  []
    })
    .addCase(showNotice.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.dataOne = payload?.data as any;
      // console.log("getoneNoticedata",payload?.data);
      
    })
    .addCase(showNotice.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      // console.log("getoneNoticedata",error);

    });

     // ------------Reducer for handling the "post Notice" action------------//
  builder
  .addCase(postNotice.pending, (state) => {
    state.status = ResponseStatus.LOADING;
  })
  .addCase(postNotice.fulfilled, (state, { payload }) => {
    state.status = ResponseStatus.LOADED;
    state.data = payload?.data as any;
  })
  .addCase(postNotice.rejected, (state, error) => {
    state.status = ResponseStatus.ERROR;
     state.error = error?.payload;
  });
     // ------------Reducer for handling the "patchNoticeGroupPage NoticeGroup page add" action------------
     builder
     .addCase(patchNoticeGroupPage.pending, (state) => {
       state.status = ResponseStatus.LOADING;
     })
     .addCase(patchNoticeGroupPage.fulfilled, (state, { payload }) => {
       state.status = ResponseStatus.LOADED;
       state.data = payload?.data as any;
       
     })
     .addCase(patchNoticeGroupPage.rejected, (state, error) => {
       state.status = ResponseStatus.ERROR;
       state.error = error?.payload

      });


   // ------------Reducer for handling the "patch Notice" action------------//patchNickName
   builder
   .addCase(patchNotice.pending, (state) => {
     state.status = ResponseStatus.LOADING;
   })
   .addCase(patchNotice.fulfilled, (state, { payload }) => {
     state.status = ResponseStatus.LOADED;
     state.data = payload?.data as any;
   })
   .addCase(patchNotice.rejected, (state, error) => {
     state.status = ResponseStatus.ERROR;
     state.error = error?.payload
     
     
   });
   builder
   .addCase(patchNickName.pending, (state) => {
     state.status = ResponseStatus.LOADING;
   })
   .addCase(patchNickName.fulfilled, (state, { payload }) => {
     state.status = ResponseStatus.LOADED;
     state.data = payload?.data as any;
   })
   .addCase(patchNickName.rejected, (state, error) => {
     state.status = ResponseStatus.ERROR;
     state.error = error?.payload 
   });
   builder
   .addCase(createEvent.pending, (state) => {
     state.status = ResponseStatus.LOADING;
   })
   .addCase(createEvent.fulfilled, (state, { payload }) => {
     state.status = ResponseStatus.LOADED;
     state.data = payload?.data as any;
   })
   .addCase(createEvent.rejected, (state, error) => {
     state.status = ResponseStatus.ERROR;
     state.error = error?.payload
   });
   // ------------Reducer for handling the "patch template" action------------//
  builder
  .addCase(patchNoticeTemplatess.pending, (state) => {
    state.status = ResponseStatus.LOADING;
  })
  .addCase(patchNoticeTemplatess.fulfilled, (state, { payload }) => {
    state.status = ResponseStatus.LOADED;
    state.data = payload?.data as any;
  })
  .addCase(patchNoticeTemplatess.rejected, (state, error) => {
    state.status = ResponseStatus.ERROR;
    state.error = error?.payload;
  });
// ------------Reducer for handling the "patch template border" action------------//
builder
  .addCase(patchNoticeTemplatesBorder.pending, (state) => {
    state.status = ResponseStatus.LOADING;
  })
  .addCase(patchNoticeTemplatesBorder.fulfilled, (state, { payload }) => {
    state.status = ResponseStatus.LOADED;
    state.data = payload?.data as any;
  })
  .addCase(patchNoticeTemplatesBorder.rejected, (state, error) => {
    state.status = ResponseStatus.ERROR;
    state.error = error?.payload;
  });

// ------------Reducer for handling the "patch template frame" action------------//
builder
  .addCase(patcNoticeTemplatesframe.pending, (state) => {
    state.status = ResponseStatus.LOADING;
  })
  .addCase(patcNoticeTemplatesframe.fulfilled, (state, { payload }) => {
    state.status = ResponseStatus.LOADED;
    state.data = payload?.data as any;
  })
  .addCase(patcNoticeTemplatesframe.rejected, (state, error) => {
    state.status = ResponseStatus.ERROR;
    state.error = error?.payload;
  });

// ------------Reducer for handling the "patch template clipart" action------------//
builder
  .addCase(patchNoticeTemplatesClipArt.pending, (state) => {
    state.status = ResponseStatus.LOADING;
  })
  .addCase(patchNoticeTemplatesClipArt.fulfilled, (state, { payload }) => {
    state.status = ResponseStatus.LOADED;
    state.data = payload?.data as any;
  })
  .addCase(patchNoticeTemplatesClipArt.rejected, (state, error) => {
    state.status = ResponseStatus.ERROR;
    state.error = error?.payload;
  });
  // ------------Reducer for handling the "patcNoticePageBackground" action------------//
builder
.addCase(patcNoticePageBackground.pending, (state) => {
  state.status = ResponseStatus.LOADING;
})
.addCase(patcNoticePageBackground.fulfilled, (state, { payload }) => {
  state.status = ResponseStatus.LOADED;
  state.data = payload?.data as any;
})
.addCase(patcNoticePageBackground.rejected, (state, error) => {
  state.status = ResponseStatus.ERROR;
  state.error = error?.payload;
});
 // ------------Reducer for handling the "patcNoticeSettingd" action------------//
 builder
 .addCase(patcNoticeSettings.pending, (state) => {
   state.status = ResponseStatus.LOADING;
 })
 .addCase(patcNoticeSettings.fulfilled, (state, { payload }) => {
   state.status = ResponseStatus.LOADED;
   state.data = payload?.data as any;
 })
 .addCase(patcNoticeSettings.rejected, (state, error) => {
   state.status = ResponseStatus.ERROR;
   state.error = error?.payload;
 });
  // ------------Reducer for handling the "patchLiveStreamVideo" action------------//
  builder
  .addCase(patchLiveStreamVideo.pending, (state) => {
    state.status = ResponseStatus.LOADING;
  })
  .addCase(patchLiveStreamVideo.fulfilled, (state, { payload }) => {
    state.status = ResponseStatus.LOADED;
    state.data = payload?.data as any;
  })
  .addCase(patchLiveStreamVideo.rejected, (state, error) => {
    state.status = ResponseStatus.ERROR;
    state.error = error?.payload;
  });



builder
.addCase(patchNoticeClient.pending, (state) => {
  state.status = ResponseStatus.LOADING;
})
.addCase(patchNoticeClient.fulfilled, (state, { payload }) => {
  state.status = ResponseStatus.LOADED;
  state.data = payload?.data as any;
})
.addCase(patchNoticeClient.rejected, (state, error) => {
  state.status = ResponseStatus.ERROR;
  state.error = error?.payload;
});

builder
.addCase(patchNoticeSettingDesign.pending, (state) => {
  state.status = ResponseStatus.LOADING;
})
.addCase(patchNoticeSettingDesign.fulfilled, (state, { payload }) => {
  state.status = ResponseStatus.LOADED;
  state.data = payload?.data as any;
})
.addCase(patchNoticeSettingDesign.rejected, (state, error) => {
  state.status = ResponseStatus.ERROR;
  state.error = error?.payload;
});
});


export default noticeReducer;
