import { BASE_ROUTE } from "@/config/constants";
import {
  deleteMethod,
  get,
  patch,
  post,
  put,
} from "@/connections/fetch_wrapper";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { integer } from "aws-sdk/clients/cloudfront";
//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous retrieval of all Notices, and depending on the response, either returns the data or an error message.
export const getAllNotices = createAsyncThunk(
  "notices/getall",
  async (
    {
      active_partner,
      active_service_provider,
      page,
    }: {
      active_partner: string;
      active_service_provider: string;
      page: integer;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices?page=${page}`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
//---------------------------------------------------------------------------------------------------------------------------------------------------------//

//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous retrieval of particular Notice, and depending on the response, either returns the data or an error message.
export const showNotice = createAsyncThunk(
  "notices/shownotice",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_notice}`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
//---------------------------------------------------------------------------------------------------------------------------------------------------------//

//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous post for  particular Notice, and depending on the response, either returns the data or an error message.
export const postNotice = createAsyncThunk(
  "notices/postnotice",
  async (
    {
      active_partner,
      active_service_provider,
      createdData,
    }: {
      active_partner: string;
      active_service_provider: string;
      createdData: {
        pages: string[];
        language_id: integer;
        relationship_id: integer;
        client_id: string;
        notice_type_id: string;
        country_id: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await post(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices`,
        createdData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);
//---------------------------------------------------------------------------------------------------------------------------------------------------------//
export const patchNoticeGroupPage = createAsyncThunk(
  "notices/patchNoticeGroupPage",
  async (
    {
      active_partner,
      active_service_provider,
      active_id,
      updatedData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_id: string;
      updatedData: {
        pages: string[];
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_id}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);
//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous update for  particular Notice, and depending on the response, either returns the data or an error message.
export const patchNotice = createAsyncThunk(
  "notices/patchnotice",
  async (
    {
      active_partner,
      active_service_provider,
      active_id,
      updatedData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_id: string;
      updatedData: {
        name?: string;
        nickname?: string;
        dod?: string;
        dob?: string;
        death_location?: string;
        salutation_id?: string;
        birth_location?: string;
        photo_url?: string;
        frame_id?: integer;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_id}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);
//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------------------------------------------------------------//

//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous update for  particular Notice, and depending on the response, either returns the data or an error message.
export const patchNickName = createAsyncThunk(
  "notices/patchNickName",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      updatedData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
      updatedData: {
        nickname: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/pages/${active_notice}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);
//---------------------------------------------------------------------------------------------------------------------------------------------------------//

//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous update for  particular Notice, and depending on the response, either returns the data or an error message.
export const patchNoticeDetails = createAsyncThunk(
  "notices/patchnotice",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      updatedData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
      updatedData: {
        language_id: number;
        lang_name?: string;
        notice_title_id: number;
        notice_template_id: number;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_notice}/custom/details`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);

export const createEvent = createAsyncThunk(
  "notices/event/create",
  async (
    {
      active_partner,
      active_service_provider,
      notice_uuid,
      createdData,
    }: {
      active_partner: string;
      active_service_provider: string;
      notice_uuid: string;
      createdData: {
        state_id: integer;
        virtual_event: string;
        address: string;
        is_delivery_possible: boolean;
        instructions: string;
        event_location_type_id: string;
        event_instructions: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await post(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${notice_uuid}/events`,
        createdData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);
export const updateEvent = createAsyncThunk(
  "notices/event/update",
  async (
    {
      active_partner,
      active_service_provider,
      notice_uuid,
      updateData,
      event_id,
    }: {
      active_partner: string;
      active_service_provider: string;
      notice_uuid: string;
      event_id: string;
      updateData: {
        state_id?: integer;
        virtual_event?: string;
        address?: string;
        is_delivery_possible?: boolean;
        instructions?: string;
        event_location_type_id?: string;
        event_instructions?: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${notice_uuid}/events/${event_id}`,
        updateData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);
export const deleteEvent = createAsyncThunk(
  "notices/event/deleted",
  async (
    {
      active_partner,
      active_service_provider,
      notice_uuid,
      event_id,
    }: {
      active_partner: string;
      active_service_provider: string;
      notice_uuid: string;
      event_id: string;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await deleteMethod(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${notice_uuid}/events/${event_id}`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);

export const patchNoticeProfile = createAsyncThunk(
  "notices/patchnotice",
  async (
    {
      active_partner,
      active_service_provider,
      active_id,
      updatedData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_id: string;
      updatedData: {
        profile_photo_url?: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_id}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);

//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous update for  particular notice templates , and depending on the response, either returns the data or an error message.
export const patchNoticeTemplatess = createAsyncThunk(
  "notices/patchrtemplates",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      updatedData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
      updatedData: {
        notice_template_id: number;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_notice}/custom/details`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);

//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous update for  particular notice templates , and depending on the response, either returns the data or an error message.
export const patchNoticeTemplatesBorder = createAsyncThunk(
  "notices/patchborders",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      updatedData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
      updatedData: {
        border_id: number;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_notice}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);

//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous update for  particular notice templates clipartr , and depending on the response, either returns the data or an error message.
export const patchNoticeTemplatesClipArt = createAsyncThunk(
  "notices/patchcliparts",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      updatedData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
      updatedData: {
        clip_art_id: number;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_notice}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);

//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous update for  particular notice templates frame , and depending on the response, either returns the data or an error message.
export const patcNoticeTemplatesframe = createAsyncThunk(
  "notices/patchfarmes",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      updatedData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
      updatedData: {
        frame_id: number;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_notice}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);
//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous update for  particular page_background_id , and depending on the response, either returns the data or an error message.
export const patcNoticePageBackground = createAsyncThunk(
  "notices/patchh",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      updatedData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
      updatedData: {
        page_background_id?: number;
        page_background_url?: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_notice}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);
//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous update for  particular page_background_id , and depending on the response, either returns the data or an error message.
export const patcNoticeSettings = createAsyncThunk(
  "notices/patchhsett",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      updatedData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
      updatedData: {
        clip_art_id?:number;
        border_id?:number;
        notice_template_id?:number;
        frame_id?: number;
        page_background_id?:number;
        family_address?: string;
        custom_family_address?: string;
        birth_and_death_title_id?: number;
        background_music_url?: string;
        visibility_id?: string;
        auto_hide?: boolean;
        hide_dob_and_dod?: boolean;
        hide_at?: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_notice}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);
//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous update for  particular LiveStreamVideo and depending on the response, either returns the data or an error message.
export const patchLiveStreamVideo = createAsyncThunk(
  "notices/patchLiveStreamVideo",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      updatedData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
      updatedData: {
        live_video_url?: string;
        background_video_url?: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_notice}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);

//This action function handles the asynchronous update for  particular Notice, and depending on the response, either returns the data or an error message.
export const patchNoticeClient = createAsyncThunk(
  "notices/patchNoticeClient",
  async (
    {
      active_partner,
      active_service_provider,
      active_id,
      updatedData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_id: string;
      updatedData: {
        client_id?: string;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_id}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);


//This action function handles the asynchronous update for  particular page templates frame , and depending on the response, either returns the data or an error message.
export const patchNoticeSettingDesign = createAsyncThunk(
  "pages/patchNoticeSettingDesign",
  async (
    {
      active_partner,
      active_service_provider,
      active_notice,
      updatedData,
    }: {
      active_partner: string;
      active_service_provider: string;
      active_notice: string;
      updatedData: {
        clip_art_id:number;
        border_id:number;
        notice_template_id:number;
        frame_id: number;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${active_notice}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);