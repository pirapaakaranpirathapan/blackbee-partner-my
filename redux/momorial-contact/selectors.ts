import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const momorialContactState = (state: RootState) => state.momorialContactReducer;

export const memorialContactSelector = createSelector(
    momorialContactState, 
    state => state
)
