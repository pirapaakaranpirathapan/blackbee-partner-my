import { BASE_ROUTE } from "@/config/constants";
import {
  deleteMethod,
  get,
  patch,
  post,
  put,
} from "@/connections/fetch_wrapper";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { integer } from "aws-sdk/clients/cloudfront";

//This action function handles the asynchronous retrieval of all notice-contact, and depending on the response, either returns the data or an error message.
export const getAllMemorialContact = createAsyncThunk(
  "momorial-contact/getAllContacts",
  async (
    {
      activePartner,
      activeServiceProvider,
      activePageId,
      page,
    }: {
      activePartner: string;
      activeServiceProvider: string;
      activePageId: string;
      page: integer;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/${activePartner}/${activeServiceProvider}/products/pages/${activePageId}/contacts`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);

export const createMemorialContact = createAsyncThunk(
  "momorial-contact/create",
  async (
    {
      activePartner,
      activeServiceProvider,
      activePageId,
      updatedData,
    }: {
      activePartner: string;
      activeServiceProvider: String;
      activePageId: String;
      updatedData: {
        name: string;
        country_id: integer;
        relationship_id: integer;
        contact_details: object[];
        // additional_text: string;
        auto_hide: string;
        visibility_id: integer;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await post(
        `${BASE_ROUTE}/${activePartner}/${activeServiceProvider}/products/pages/${activePageId}/contacts`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
export const updateMemorialContact = createAsyncThunk(
  "momorial-contact/update",
  async (
    {
      activePartner,
      activeServiceProvider,
      activePageId,
      uuid,
      updatedData,
    }: {
      activePartner: string;
      activeServiceProvider: String;
      activePageId: String;
      uuid: string;
      updatedData: {
        name: string;
        country_id: integer;
        relationship_id: integer;
        contact_details: object[];
        // additional_text: string;
        auto_hide: string;
        visibility_id: integer;
      };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${activePartner}/${activeServiceProvider}/products/pages/${activePageId}/contacts/${uuid}`,
        updatedData
      );
      if (response.code == 200) {
        return response;

        
      } else {
        return rejectWithValue(response);
      }
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
export const updateFamilyAddress = createAsyncThunk(
  "update-family-address/create",
  async (
    {
      activePartner,
      activeServiceProvider,
      activePageId,
      updatedData,
    }: {
      activePartner: string;
      activeServiceProvider: String;
      activePageId: String;
      updatedData: { family_address: string };
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await patch(
        `${BASE_ROUTE}/${activePartner}/${activeServiceProvider}/products/pages/${activePageId}`,
        updatedData
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
export const deleteMemorialContact = createAsyncThunk(
  "momorial-contact/delete",
  async (
    {
      activePartner,
      activeServiceProvider,
      activePageId,
      contactId,
    }: {
      activePartner: string;
      activeServiceProvider: String;
      activePageId: String;
      contactId: string;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await deleteMethod(
        `${BASE_ROUTE}/${activePartner}/${activeServiceProvider}/products/pages/${activePageId}/contacts/${contactId}`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
