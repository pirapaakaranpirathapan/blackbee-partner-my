import { ResponseStatus } from "@/helpers/response_status";
import { createReducer } from "@reduxjs/toolkit";
import {
  createMemorialContact,
  deleteMemorialContact,
  getAllMemorialContact,
  updateMemorialContact,
} from "./actions";

interface IReducerInitialState {
  data: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: [],
};

// Reducer for handling the "get all Notice contacts" action
export const momorialContactReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(getAllMemorialContact.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(getAllMemorialContact.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(getAllMemorialContact.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });
  builder
    .addCase(deleteMemorialContact.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(deleteMemorialContact.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      // state.data = payload?.data.data as any;
    })
    .addCase(deleteMemorialContact.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
    });
  builder
    .addCase(createMemorialContact.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(createMemorialContact.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(createMemorialContact.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
      console.log("errorredu", error?.payload);
    });
  builder
    .addCase(updateMemorialContact.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(updateMemorialContact.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
    })
    .addCase(updateMemorialContact.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;
      state.error = error?.payload;
      console.log("errorredu", error?.payload);
    });
});
export default momorialContactReducer;
