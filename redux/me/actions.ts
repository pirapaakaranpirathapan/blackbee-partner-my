import { BASE_ROUTE } from '@/config/constants';
import { get } from '@/connections/fetch_wrapper';
import { createAsyncThunk } from '@reduxjs/toolkit';

export const getMe = createAsyncThunk(
  'me',
  async (_, thunkAPI) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/auth/me`
      );

      if (response.code == 200) {
        return response;
      } else {
        thunkAPI.rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return thunkAPI.rejectWithValue(err.message);
    }
  }
  
  );