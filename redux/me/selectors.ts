import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const meState = (state: RootState) => state.meReducer;

export const meSelector = createSelector(
    meState, 
    state => state
)
