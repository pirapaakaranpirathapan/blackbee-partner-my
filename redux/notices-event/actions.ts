import { createAsyncThunk } from "@reduxjs/toolkit";
import { get, patch, post } from "@/connections/fetch_wrapper";
import { BASE_ROUTE } from "@/config/constants";
import { integer } from "aws-sdk/clients/cloudfront";
export const getEvent = createAsyncThunk(
  "notices/event/get",
  async (
    {
      active_partner,
      active_service_provider,
      notice_uuid,
      page,
    }: {
      active_partner: string;
      active_service_provider: string;
      notice_uuid: string;
      page: integer;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${notice_uuid}/events`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);

export const getOneEvent = createAsyncThunk(
  "notices/event/getOne",
  async (
    {
      active_partner,
      active_service_provider,
      notice_uuid,
      uuid,
    }: {
      active_partner: string;
      active_service_provider: string;
      notice_uuid: string;
      uuid: string;
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/${active_partner}/${active_service_provider}/products/notices/${notice_uuid}/events/${uuid}`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response);
      }
    } catch (err: any) {
      return rejectWithValue(err);
    }
  }
);
