import { ResponseStatus } from "@/helpers/response_status";
import { createReducer } from "@reduxjs/toolkit";
import { getEvent, getOneEvent } from "./actions";

interface IReducerInitialState {
  data: any ;
  dataOne:any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: [],
  dataOne:[],
};

export const noticeEventReducer = createReducer(initialState, (builder) => {
 
  builder
  .addCase(getEvent.pending, (state) => {
    state.status = ResponseStatus.LOADING;
  })
  .addCase(getEvent.fulfilled, (state, { payload }) => {
    state.status = ResponseStatus.LOADED;
    state.data = payload?.data as any;
  })
  .addCase(getEvent.rejected, (state, error) => {
    state.status = ResponseStatus.ERROR;
     state.error = error;
  });
  builder
  .addCase(getOneEvent.pending, (state) => {
    state.status = ResponseStatus.LOADING;
  })
  .addCase(getOneEvent.fulfilled, (state, { payload }) => {
    state.status = ResponseStatus.LOADED;
    state.dataOne = payload?.data as any;
  })
  .addCase(getOneEvent.rejected, (state, error) => {
    state.status = ResponseStatus.ERROR;
     state.error = error;
  });

});

export default noticeEventReducer;
