import { RootState } from "../store/store";
import { createSelector } from "@reduxjs/toolkit";

export const noticeEventState = (state: RootState) => state.noticeEventReducer;
export const noticeEventSelector = createSelector(noticeEventState, (state) => state);