import { BASE_ROUTE } from "@/config/constants";
import { get, patch, post } from "@/connections/fetch_wrapper";
import { createAsyncThunk } from "@reduxjs/toolkit";
//---------------------------------------------------------------------------------------------------------------------------------------------------------//
//This action function handles the asynchronous retrieval of all Notice PageBackgrounds Frames, and depending on the response, either returns the data or an error message.
export const getAllNoticePageBackgrounds = createAsyncThunk(
  "notices/getallPageBackgrounds",
  async (
    {
      active_partner,
      active_notice_template
    
    }: {
      active_partner: string;
      active_notice_template:string;
     
    },
    { rejectWithValue }
  ) => {
    try {
      const response = await get(
        `${BASE_ROUTE}/${active_partner}/notice-templates/${active_notice_template}/page-backgrounds`
      );
      if (response.code == 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);

