import { RootState } from "../store/store";
import { createSelector } from "@reduxjs/toolkit";

export const noticePageBackgroundsState = (state: RootState) => state.  noticePageBackgroundReducer;
export const  noticePageBackgroundsSelector = createSelector( noticePageBackgroundsState, (state) => state);
