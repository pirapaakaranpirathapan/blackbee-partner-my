export const UPDATE_ACTIVE_PARTNER = 'UPDATE_ACTIVE_PARTNER';
export const UPDATE_ACTIVE_SERVICE_PROVIDER = 'UPDATE_ACTIVE_SERVICE_PROVIDER';

export interface UpdateActivePartnerAction {
  type: typeof UPDATE_ACTIVE_PARTNER;
  payload: string;
}

export interface UpdateActiveServiceProviderAction {
  type: typeof UPDATE_ACTIVE_SERVICE_PROVIDER;
  payload: string;
}

export const updateActivePartner = (newActivePartner: string): UpdateActivePartnerAction => ({
  type: UPDATE_ACTIVE_PARTNER,
  payload: newActivePartner,
});

export const updateActiveServiceProvider = (
  newActiveServiceProvider: string
): UpdateActiveServiceProviderAction => ({
  type: UPDATE_ACTIVE_SERVICE_PROVIDER,
  payload: newActiveServiceProvider,
});
