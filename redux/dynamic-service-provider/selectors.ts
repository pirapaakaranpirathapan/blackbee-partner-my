import { createSelector } from 'reselect';
import { AppState } from './reducer';

const selectActivePartner = (state: AppState) => state.activePartner;

export const getActivePartner = createSelector([selectActivePartner], (activePartner) => activePartner);
