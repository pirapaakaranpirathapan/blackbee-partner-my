import { Reducer } from 'redux';
import {
  UPDATE_ACTIVE_PARTNER,
  UPDATE_ACTIVE_SERVICE_PROVIDER,
  UpdateActivePartnerAction,
  UpdateActiveServiceProviderAction,
} from './actions';

export interface AppState {
  activePartner: string;
  activeServiceProvider: string;
}

const initialState: AppState = {
  activePartner: '95857916-5b03-486c-8a4e-ef92146db906',
  activeServiceProvider: '6bddc5e6-d129-4304-bfe5-b3cc5bc3c826',
};

type Actions = UpdateActivePartnerAction | UpdateActiveServiceProviderAction;

const activeServiceProviderReducer: Reducer<AppState, Actions> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case UPDATE_ACTIVE_PARTNER:
      return {
        ...state,
        activePartner: (action as UpdateActivePartnerAction).payload,
      };
    case UPDATE_ACTIVE_SERVICE_PROVIDER:
      return {
        ...state,
        activeServiceProvider: (action as UpdateActiveServiceProviderAction).payload,
      };
    default:
      return state;
  }
};

export default activeServiceProviderReducer;
