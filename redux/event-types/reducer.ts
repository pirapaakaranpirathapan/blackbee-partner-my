import { ResponseStatus } from "@/helpers/response_status";
import { createReducer } from "@reduxjs/toolkit";
import { getAllEventsTypes } from "./actions";

interface IReducerInitialState {
  data: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: [],
};

export const getAllEventsTypesReducer = createReducer(
  initialState,
  (builder) => {
    builder
      .addCase(getAllEventsTypes.pending, (state) => {
        state.status = ResponseStatus.LOADING;
      })
      .addCase(getAllEventsTypes.fulfilled, (state, { payload }) => {
        state.status = ResponseStatus.LOADED;
        state.data = payload?.data as any;
      })
      .addCase(getAllEventsTypes.rejected, (state, error) => {
        state.status = ResponseStatus.ERROR;
      });
  }
);
export default getAllEventsTypesReducer;
