import { RootState } from "../store/store";
import { createSelector } from "@reduxjs/toolkit";

export const getAllEventsTypesState = (state: RootState) =>
  state.getAllEventsTypesReducer;

export const EventTypeSelector = createSelector(
  getAllEventsTypesState,
  (state) => state
);
