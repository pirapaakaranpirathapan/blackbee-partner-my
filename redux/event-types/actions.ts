import { BASE_ROUTE } from "@/config/constants";
import { get } from "@/connections/fetch_wrapper";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const getAllEventsTypes = createAsyncThunk(
  "event-types/getall",
  async (_, { rejectWithValue }) => {
    try {
      const response = await get(`${BASE_ROUTE}/event-types`);
      if (response.code === 200) {
        return response;
      } else {
        return rejectWithValue(response.msg);
      }
    } catch (err: any) {
      return rejectWithValue(err.message);
    }
  }
);
