import { RootState } from '../store/store';
import { createSelector } from '@reduxjs/toolkit';

export const musicsState = (state: RootState) => state.musicsReducer;

export const musicsSelector = createSelector(
    musicsState, 
    state => state
)
