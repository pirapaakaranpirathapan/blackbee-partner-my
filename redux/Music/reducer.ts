import { ResponseStatus } from '@/helpers/response_status';
import { createReducer } from '@reduxjs/toolkit';
import { getAllMusics } from './actions';


interface IReducerInitialState {
  data: any;
  status: ResponseStatus;
  error: any;
  added: boolean;
  updated: boolean;
  deleted: boolean;
  pending: boolean;
}
const initialState: IReducerInitialState = {
  status: ResponseStatus.INITIAL,
  error: {},
  added: false,
  updated: false,
  deleted: false,
  pending: false,
  data: [],
};

export const musicsReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(getAllMusics.pending, (state) => {
      state.status = ResponseStatus.LOADING;
    })
    .addCase(getAllMusics.fulfilled, (state, { payload }) => {
      state.status = ResponseStatus.LOADED;
      state.data = payload?.data as any;
      
    })
    .addCase(getAllMusics.rejected, (state, error) => {
      state.status = ResponseStatus.ERROR;

    });
});
export default musicsReducer;
