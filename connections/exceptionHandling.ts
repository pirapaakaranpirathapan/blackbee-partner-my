export function exceptionHandling(
  rejectedResponse:
    | {
        data: { msg: any; error: any } | null | undefined;
        status: number | null | undefined;
      }
    | null
    | undefined
) {
  if (rejectedResponse === undefined || rejectedResponse === null) {
    return "Unable process data";
  } else if (
    rejectedResponse.data === undefined ||
    rejectedResponse.data === null
  ) {
    return "Unable process data";
  } else if (
    rejectedResponse.status === undefined ||
    rejectedResponse.status === null
  ) {
    return "Unable process data";
  } else if (rejectedResponse.status === 417) {
    return rejectedResponse.data.msg;
  } else if (rejectedResponse.status === 400) {
    return {
      msg: rejectedResponse.data.msg,
      error: rejectedResponse.data.error,
    };
  } else if (rejectedResponse.status === 404) {
    return rejectedResponse.data.msg;
  } else if (rejectedResponse.status === 401) {
    return rejectedResponse.data.msg;
  } else if (rejectedResponse.status === 403) {
    return rejectedResponse.data.msg;
  } else if (rejectedResponse.status === 429) {
    return (
      rejectedResponse.data.msg || "Too many requests, please try again later"
    );
  } else if (rejectedResponse.status === 500) {
    return "Internal server error";
  }

  return "Unhandled error occurred";
}
