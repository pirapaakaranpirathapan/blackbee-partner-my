export interface IPageManager {
    alternative_phone: null | string;
    country_id: number;
    created_at: string;
    deleted_at: null;
    email: string;
    first_name: string;
    gender_id: number;
    id: number;
    last_login: null;
    last_name: string;
    last_password_reset: null;
    mobile: string;
    owner_id: number;
    owner_type: string;
    pivot: {
      page_id: number;
      client_id: number;
    };
    service_provider_id: number;
    state_id: number;
    updated_at: string;
    uuid: string;
  }
  