export interface IMemorialPage {
      id: number;
      uuid: string;
      state: {
          id: number;
          uuid: string;
          state: null | any; // This property can be null or of any type
          owner: null | any; // This property can be null or of any type
          created_at: string;
          updated_at: string;
          name: string;
          slug: string;
          is_enabled: boolean;
      };
      owner: {
          id: number;
          first_name: string;
          last_name: string;
          internal_job_title: string;
          email: string;
          login_with_password: number;
          login_with_google: number;
          force_password_change: number;
          salutation_id: number;
          last_accessed_service_provider_id: null | number; // This property can be null or a number
          default_language_id: number;
          state_id: number;
          owner_id: number;
          owner_type: string;
          created_at: string;
          deleted_at: null | string; // This property can be null or a string
          updated_at: string;
          uuid: string;
      };
      photo_url: string;
      name: string;
      lang_name: string;
      nickname: string;
      job_title: null | string; // This property can be null or a string
      quote: null | string; // This property can be null or a string
      use_notice_photo: number;
      family_display_address: null | string; // This property can be null or a string
      display_locations: null | any; // This property can be null or of any type
      accept_donations: number;
      auto_hide_contacts: number;
      hide_dates: number;
      client: null | any; // This property can be null or of any type
      gender: null | any; // This property can be null or of any type
      language: null | any; // This property can be null or of any type
      nature_of_death: null | any; // This property can be null or of any type
      page_category: null | any; // This property can be null or of any type
      page_type: {
          id: number;
          uuid: string;
          state: {
              id: number;
              uuid: string;
              name: string;
              slug: string;
          };
          owner: {
              id: number;
              first_name: string;
              last_name: string;
              alias: string;
              email: string;
              email_verified_at: null | string; // This property can be null or a string
              state_id: number;
              owner_id: number;
              owner_type: string;
              created_at: string;
              deleted_at: null | string; // This property can be null or a string
              updated_at: string;
              uuid: string;
          };
          created_at: string;
          updated_at: string;
          name: string;
          slug: string;
      };
      partner: null | any; // This property can be null or of any type
      relationship: null | any; // This property can be null or of any type
      religion: null | any; // This property can be null or of any type
      salutation: {
          id: number;
          uuid: string;
          state: {
              id: number;
              uuid: string;
              name: string;
              slug: string;
          };
          owner: {
              id: number;
              first_name: string;
              last_name: string;
              alias: string;
              email: string;
              email_verified_at: null | string; // This property can be null or a string
              state_id: number;
              owner_id: number;
              owner_type: string;
              created_at: string;
              deleted_at: null | string; // This property can be null or a string
              updated_at: string;
              uuid: string;
          };
          created_at: string;
          updated_at: string;
          name: string;
          slug: string;
      };
      education: null | any; // This property can be null or of any type
      is_verified: boolean;
      verification_reason: null | string; // This property can be null or a string
      trust_level: null | any; // This property can be null or of any type
      donations: {
          total_donations: number;
          total_fundraised: number;
      };
      is_active: boolean;
      is_deleted: boolean;
      slug: string;
      theme_color: string;
      created_at: string;
      updated_at: string;
  };


