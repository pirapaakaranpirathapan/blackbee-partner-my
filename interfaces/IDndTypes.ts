export interface Item {
    id: string;
    name: string;
  }
  
  export interface DragItem {
    item: Item;
    name: string,
    index: number;
  }
  
  export interface DropResult {
    fromIndex: number;
    toIndex: number;
  }
  