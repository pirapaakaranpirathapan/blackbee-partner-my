import { IOwner } from "../manage/IOwner";
import { IPagination } from "../manage/IPagination";
import { IState } from "../manage/IState";

export interface IPageType {
    id: number;
    uuid: string;
    name: string;
    slug: string;
    created_at: string;
    updated_at: string;

    state: IState;
    owner: IOwner;
  }

  export interface IPageTypeList {
    data: (IPageType)[],
    pagination: IPagination | null
  }
  