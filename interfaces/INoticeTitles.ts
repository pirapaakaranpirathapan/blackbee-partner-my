export interface INoticeTitles {
  find(arg0: (option: { id: number;name:string }) => boolean): unknown;
  id: number;
  name: String;
  notice_title: string;
  data:[]
}

