export interface INoticeTypes {
  find(arg0: (x: any) => boolean): unknown;
  length: number;
  id: number;
  name: String;
}

