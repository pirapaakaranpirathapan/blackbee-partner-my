export interface ICustomerPages {
  alternative_phone: null | string;
  country: {
    id: number;
    iso_2: string;
    iso_3: string;
    name: string;
    calling_code: string;
    // Other properties
  };
  created_at: string;
  deleted_at: null;
  email: string;
  first_name: string;
  gender: {
    id: number;
    name: string;
    slug: string;
    state_id: number;
    owner_id: number;
    // Other properties
  };
  id: number;
  last_name: string;
  mobile: string;
  owner_id: {
    id: number;
    first_name: string;
    last_name: string;
    alias: string;
    email: string;
    // Other properties
  };
  owner_type: string;
  service_provider: {
    id: number;
    uuid: string;
    name: string;
    // Other properties
  };
  state: {
    id: number;
    uuid: string;
    name: string;
    // Other properties
  };
  updated_at: string;
  uuid: string;
}

