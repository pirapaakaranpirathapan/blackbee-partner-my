import { IFrame } from "./manage/IFrame";
import { IOwner } from "./manage/IOwner";
import { IState } from "./manage/IState";
//import { IPageType } from "./memorial-page/IPageType";

export interface INoticePages {
  id: any;
  uuid: string;
  photo_url: string;
  name: string;
  lang_name: string;
  nickname: string;
  job_title: string;
  quote: string;
  family_display_address: string;
  display_locations: string;
  accept_donations: boolean;
  auto_hide_contacts: boolean;
  hide_dates: boolean;
  client: string;
  gender: string;
  language: string;
  nature_of_death: string;
  page_category: string;
  partner: string;
  relationship: string;
  religion: string;
  salutation: string;
  visibility: boolean;
  legacies: string;
  tributes: string;
  dob: Date;
  dod: Date;
  border: Date;
  clip_art: Date;
  template: string;
  header_background_url: string;
  page_background_url: string;
  birth_location: string;
  death_location: string;
  family_address: string;
  hometown_location: string;
  lived_locations: string;

 // page_type: IPageType;
  frame: IFrame;
  state: IState;
  owner: IOwner;
}

