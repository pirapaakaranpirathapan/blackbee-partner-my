export interface IApiResponse {
  // client_id: null
  token?: any
  language?: any
  msg: string
  code: number
  data: any
  error: any
  success: boolean
  count?:number
  cartTotal?:string
}