import { IPagination } from "./IPagination";

export interface IOwner {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
  }

  export interface IOwnerList {
    data: (IOwner)[],
    pagination: IPagination | null
  }
  