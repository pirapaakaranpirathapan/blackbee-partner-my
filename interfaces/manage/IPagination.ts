export interface IPagination {
    current_page: number,
    last_page : number | null,
    total? : number
}