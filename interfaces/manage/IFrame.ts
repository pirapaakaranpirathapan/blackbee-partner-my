import { IOwner } from "./IOwner";
import { IPagination } from "./IPagination";
import { IState } from "./IState";

export interface IFrame {
    id: number;
    uuid: string;
    name: string;
    slug: string;
    description: string;
    thumb_url: string;
    frame_url: string;
    frame_print_url: string;
    frame_width: string;
    frame_height: string;
    photo_width: string;
    photo_height: string;
    photo_left: string;
    photo_top: string;
    created_at: string;
    updated_at: string;

    state: IState;
    owner: IOwner;
  }

  export interface IFrameList {
    data: (IFrame)[],
    pagination: IPagination | null
  }
  