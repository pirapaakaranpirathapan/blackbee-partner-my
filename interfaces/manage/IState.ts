import { IPagination } from "./IPagination";

export interface IState {
    id: number;
    uuid: string;
    state: string;
    owner: string;
    name: string;
    slug: string;
    is_enabled: boolean;
    created_at: string;
    updated_at: string;
  }

  export interface IStateList {
    data: (IState)[],
    pagination: IPagination | null
  }
  