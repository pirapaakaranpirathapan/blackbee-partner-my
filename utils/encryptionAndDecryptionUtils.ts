import CryptoJS from 'crypto-js';

// Replace 'your-secret-key' with your actual secret key
const secretKey = 'rose-secret-1234';

export const encryptToken = (token: string): string => {
  return CryptoJS.AES.encrypt(token, secretKey).toString();
};

export const decryptToken = (encryptedToken: string): string => {
  try {
    if (!encryptedToken) {
      throw new Error('Invalid encrypted token'); // Handle empty or invalid input
    }

    const bytes = CryptoJS.AES.decrypt(encryptedToken, secretKey);

    if (!bytes) {
      throw new Error('Decryption failed'); // Handle decryption failure
    }

    return bytes.toString(CryptoJS.enc.Utf8);
  } catch (error) {
    console.error('Decryption error:', error);
    return ''; // Return a default value or handle the error as needed
  }
};



// import CryptoJS from 'crypto-js';

// // Replace 'your-secret-key' with your actual secret key
// const secretKey = 'your-secret-key';

// export const encryptToken = (token: string): string => {
//   return CryptoJS.AES.encrypt(token, secretKey).toString();
// };

// export const decryptToken = (encryptedToken: string): string => {
//   const bytes = CryptoJS.AES.decrypt(encryptedToken, secretKey);
//   return bytes.toString(CryptoJS.enc.Utf8);
// };
