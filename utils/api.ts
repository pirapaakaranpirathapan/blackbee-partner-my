import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER, BASE_ROUTE} from '@/config/constants';
import { getCookie } from 'cookies-next';
import { decryptToken } from './encryptionAndDecryptionUtils';

export async function fetchCountriesData(req: any, res: any) {
    // const apiUrl = `http://dev.zoftline.com/api/v1/aBZu7ui0/admin/countries`;
    const apiUrl = `${process.env.BASE_URL}${BASE_ROUTE}/countries`;
    
    const rexs = await fetch(apiUrl, {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
        'Accept': 'application/json, text/plain',
        'Content-Type': 'application/json; charset=utf-8',
        'Authorization': `Bearer ${getCookie('token', { req, res })}`
      },
    });
  
    return rexs.json();
  }
  export async function fetchNoticeTypeData(req: any, res: any) {
    const apiUrl = `${process.env.BASE_URL}${BASE_ROUTE}/${ACTIVE_PARTNER}/notice-types`;
    const dtoken =getCookie('token', { req, res })
    const token =decryptToken(String(dtoken))
    // console.log("dtoken",dtoken);
    // console.log("token",token);
    
    const rexs = await fetch(apiUrl, {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
        'Accept': 'application/json, text/plain',
        'Content-Type': 'application/json; charset=utf-8',
        'Authorization': `Bearer ${token}`
      },
    });
  
    return rexs.json();
  }
  
  export async function fetchCommunityTypeData(req: any, res: any) {
    const apiUrl = `${process.env.BASE_URL}${BASE_ROUTE}/${ACTIVE_PARTNER}/notice-types`;
    const dtoken =getCookie('token', { req, res })
    const token =decryptToken(String(dtoken))

    const rexs = await fetch(apiUrl, {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
        'Accept': 'application/json, text/plain',
        'Content-Type': 'application/json; charset=utf-8',
        'Authorization': `Bearer ${getCookie('token', { req, res })}`
      },
    });
  
    return rexs.json();
  }


  export async function fetchMeData(req: any, res: any) {
    const apiUrl = `${process.env.BASE_URL}${BASE_ROUTE}/me`;
    const dtoken =getCookie('token', { req, res })
    const token =decryptToken(String(dtoken))
    const rexs = await fetch(apiUrl, {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
        'Accept': 'application/json, text/plain',
        'Content-Type': 'application/json; charset=utf-8',
        'Authorization': `Bearer ${token}`
      },
    });
  
    return rexs.json();
  }

  export async function fetchGetMeData() {
    try {
      const response = await fetch(`${process.env.BASE_URL}${BASE_ROUTE}/me`); // Replace with your actual API endpoint
      const data = await response.json();
      return data;
    } catch (error) {
      console.error('Error fetching meData:', error);
      return null;
    }
  }
  // export async function fetchCustomerNotices() {
  //   try {
  //     const response = await fetch(`${process.env.BASE_URL}${BASE_ROUTE}/${ACTIVE_PARTNER}/${ACTIVE_SERVICE_PROVIDER}/clients/${clientuuid}/products/notices`); // Replace with your actual API endpoint
  //     const data = await response.json();
  //     return data;
  //   } catch (error) {
  //     console.error('Error fetching meData:', error);
  //     return null;
  //   }
  // }

  