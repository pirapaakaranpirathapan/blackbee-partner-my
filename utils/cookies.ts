// cookieUtils.ts
import { getCookie } from 'cookies-next';

export function getActivePartnerCookie(){
  return getCookie('activepartner') ;
}

export function getActiveServiceProviderCookie(){
  return getCookie ('activeserviceprovider');
}
