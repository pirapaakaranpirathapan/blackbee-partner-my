// utils/sendEmail.ts
import nodemailer, { Transporter } from "nodemailer";

// Load environment variables from .env file
require("dotenv").config();

// Function to send email
async function sendEmail(
  to: string,
  subject: string,
  text: string
): Promise<void> {
  const transporter: Transporter = nodemailer.createTransport({
    // host: process.env.SMTP_HOST,
    // port: parseInt(process.env.SMTP_PORT || "", 10),
    // auth: {
    //   user: process.env.SMTP_USER,
    //   pass: process.env.SMTP_PASS,
    // },
    host: process.env.MAIL_HOST,
    port: parseInt(process.env.MAIL_PORT || "", 10),
    auth: {
      user: process.env.MAIL_USERNAME,
      pass: process.env.MAIL_PASSWORD,
    },
  });
  // Verify SMTP configuration
  transporter.verify(function (error, success) {
    if (error) {
      console.log("SMTP connection error:", error);
    } else {
      console.log("SMTP connection is ready to send emails");
    }
  });

  const mailOptions = {
    // from: '"Blackbee" <blackbee@gmail.com>',
    from: `"Blackbee" <${process.env.MAIL_FROM_ADDRESS}>`,
    to, // List of recipients separated by commas
    subject, // Subject line
    text, // Plain text body
  };

  try {
    await transporter.sendMail(mailOptions);
    console.log("Email sent successfully!");
  } catch (error) {
    console.error("Error sending email:", error);
    throw error;
  }
}

export default sendEmail;
