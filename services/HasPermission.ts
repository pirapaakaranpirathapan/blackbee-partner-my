export function hashPermission(searchPermission: string[]): boolean {
  const ISSERVER: boolean = typeof window === "undefined";

  if (!ISSERVER) {
    const permissions: string[] = JSON.parse(
      localStorage.getItem("permissions") || "[]"
    );
    return searchPermission.some((element) => permissions.includes(element));
  }

  return false;
}
