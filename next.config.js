const { i18n } = require('./next-i18next.config')
// const  withLess  = require('@arco-design/web-react/next');
module.exports = {
  transpilePackages: ["@pqina/pintura", "@pqina/react-pintura"],
  poweredByHeader: false,
  generateEtags: false,
  reactStrictMode: false,
  i18n,
  env: {
    BASE_URL: process.env.BASE_URL,
    PROXY_PREFIX: process.env.PROXY_PREFIX,
  },
  images: {
    domains: ["unsplash.com", "domain.tld", "blackbeeglobal.s3.amazonaws.com", "some.domain", "www.nationalia.info", "direct-upload-s3-bucket-testing.s3.amazonaw.com"],
    remotePatterns: [
      {
        protocol: process.env.IMAGE_PROTOCOL,
        hostname: process.env.IMAGE_URL,
      },
    ],
    deviceSizes: [640, 750, 828, 1080, 1200, 1920, 2048, 3840],
  },
  trailingSlash: false,
  async headers() {
    return [
      {
        source: '/(.*)',
        headers: [
          {
            key: 'Strict-Transport-Security',
            value: 'max-age=63072000; includeSubDomains; preload',
          },
          {
            key: 'X-Frame-Options',
            value: 'DENY',
          },
          {
            key: 'Cache-Control',
            value: 'public, max-age=900, s-maxage=900, stale-while-revalidate=1200, stale-if-error=2400',
          },
        ],
      },
    ]
  },
  async rewrites() {
    return {
      beforeFiles: [
        {
          source: process.env.PROXY_PREFIX + '/:path(.*)',
          destination: process.env.BASE_URL + '/:path',
        },
      ],
    }
  },
}


// module.exports = withLess({
//   lessLoaderOptions: {
//     javascriptEnabled: true,
//   },
//   webpack: (config, { isServer }) => {
//     if (!isServer) {
//       config.node = {
//         fs: 'empty',
//       };
//     }
//     return config;
//   },
// });
