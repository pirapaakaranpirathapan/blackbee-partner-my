export const SINHALA = {
    // language in sinhala
    config: {
        
        //login
        signInTo:"Sign in to",
        signInToBlackBeesPartner:"BlackBee හි සහනාගාරයට ඇතුළු වන්න",
        signin: "පුරන්න",
        email: "Email",
        password: "Password",
        forgotPassword: "Forgot Password",
        joinAsPartner: "Join as a partner",
        contactUs: "Contact us",
        termsofService: "Terms of Service",
        support: "Support",
        companyName: "Company Name",

        //dashboard
        dashboard:"Dashboard",
        customers:"Customers",
        memorialPages:"Memorial Pages",
        notices:"Notices",
        manage:"MANAGE",
        billings:"Billings",
        reports:"Reports",
        settings:"Settings",
        usersAccess:"Users Access",

        switchProvider:"Switch Provider",

        option:"Option",
        search:"Search",

        createAnOrder:"Create an order",
        CustomerWillReceiveAUnique:"Customer will receive a unique link to claim this order",
        memorialPage:"Memorial Page",
        dasMemorialDes:"Create a page for your loved one.",
        deathNotice:"Death Notice",
        dasDeathNoticeDes:"Publish a notice for death person.",
        otherNotices:"Other Notices",
        dasOtherNoticesDes:"Publish a notice for death person.",
        remembrance:"Remembrance",
        dasRemembranceDes:"Publish a announcement for their remembrance day.",
        tributes:"Tributes",
        dasTributesDes:"Tribute a post to someone who you lost.",
        birthMemorial:"Birth Memorial",
        dasbirthMemorialDes:"Create a page for your loved one.",
        prayerNotice:"Prayer Notice",
        dasPrayerNoticeDes:"Publish a notice for death person.",
        otherServices:"Other Services",
        dasOtherServicesDes:"You can choose from our list of services.",
    },
}
