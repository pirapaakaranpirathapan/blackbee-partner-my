export const ARABIC = {
    // language in arabic
    config: {
        
        //login
        signInTo:"تسجيل الدخول إلى",
        signInToBlackBeesPartner: "قم بتسجيل الدخول إلى شريك BlackBee",
        signin: "تسجيل الدخول",
        email: "بريد إلكتروني",
        password: "كلمة المرور",
        forgotPassword: "هل نسيت كلمة السر",
        joinAsPartner: "انضم كشريك",
        contactUs: "اتصل بنا",
        termsofService: "شروط الخدمة",
        support: "يدعم",
        companyName: "اسم الشركة",

        //dashboard
        dashboard:"Dashboard",
        customers:"Customers",
        memorialPages:"تسجيل الدخو",
        notices:"Notices",
        manage:"MANAGE",
        billings:"Billings",
        reports:"Reports",
        settings:"Settings",
        usersAccess:"Users Access",

        switchProvider:"Switch Provider",

        option:"Option",
        search:"Search",

        createAnOrder:"Create an order",
        CustomerWillReceiveAUnique:"Customer will receive a unique link to claim this order",
        memorialPage:"Memorial Page",
        dasMemorialDes:"Create a page for your loved one.",
        deathNotice:"Death Notice",
        dasDeathNoticeDes:"Publish a notice for death person.",
        otherNotices:"Other Notices",
        dasOtherNoticesDes:"Publish a notice for death person.",
        remembrance:"Remembrance",
        dasRemembranceDes:"Publish a announcement for their remembrance day.",
        tributes:"Tributes",
        dasTributesDes:"Tribute a post to someone who you lost.",
        birthMemorial:"Birth Memorial",
        dasbirthMemorialDes:"Create a page for your loved one.",
        prayerNotice:"Prayer Notice",
        dasPrayerNoticeDes:"Publish a notice for death person.",
        otherServices:"Other Services",
        dasOtherServicesDes:"You can choose from our list of services.",
      
    },
}
