export const ENGLISH = {
  //language in english
  config: {
    //login
    signInTo: "Sign in to",
    signInToBlackBeesPartner: "Sign in to BlackBee’s Partner",
    signin: "Sign In",
    email: "Email",
    password: "Password",
    forgotPassword: "Forgot Password?",
    joinAsPartner: "Join as a partner",
    contactUs: "Contact us",
    termsofService: "Terms of Service",
    support: "Support",
    companyName: "Company Name",

    //sidebar
    dashboard: "Dashboard",
    customers: "Customers",
    memorialPages: "Memorial Pages",
    notices: "Notices",
    manage: "MANAGE",
    billings: "Billings",
    reports: "Reports",
    settings: "Settings",
    usersAccess: "Users Access",
    switchProvider: "Switch Provider",

    //header
    option: "Option",
    showOptions: "Show Options",
    otherDocuments: "Other documents",
    includeDocuments: "Include documents",
    search: "Search",
    myAccountSettings: "My Account Settings",
    partnerSettings: "Partner Settings",
    userAccess: "User Access",
    logout: "Logout",

    // ==================================================================================
    //dashboard
    createAnOrder: "Create an order",
    CustomerWillReceiveAUnique:
      "Customer will receive a unique link to claim this order",

    //sample dasboard testing
    // memorialPage:"Memorial Page",
    dasMemorialDes: "Create a page for your loved one.",
    deathNotice: "Death Notice",
    dasDeathNoticeDes: "Publish a notice for death person.",
    otherNotices: "Other Notices",
    dasOtherNoticesDes: "Publish a notice for death person.",
    remembrance: "Remembrance",
    dasRemembranceDes: "Publish a announcement for their remembrance day.",
    tributes: "Tributes",
    dasTributesDes: "Tribute a post to someone who you lost.",
    birthMemorial: "Birth Memorial",
    dasbirthMemorialDes: "Create a page for your loved one.",
    prayerNotice: "Prayer Notice",
    dasPrayerNoticeDes: "Publish a notice for death person.",
    otherServices: "Other Services",
    dasOtherServicesDes: "You can choose from our list of services.",

    // ==================================================================================
    //pagination
    totalRecords: "Total records",
    next: "Next",
    previous: "Previous",

    //Customer All page
    allCustomers: "All Customers",
    allCustomersDescription:
      "Manage your customer who already bought services through your company",
    create: "Create",
    view: "View",

    //Customer Create Page
    addNewCustomer: "Add a new customer",
    addNewCustomerDescription: "Create new Customer for a page",

    //Customer form
    firstName: "First Name",
    lastName: "Last Name",
    gender: "Gender",
    selectAGender: "Select a Gender",
    country: "Country",
    selectACountry: "Select a Country",
    confirmPassword: "Confirm Password",
    mobile: "Mobile",
    alternativeNumber: "Alternative Number",
    save: "Save",
    selectATitle: "Select a Title",
    selectAStatus: "Select a Status",

    //Customer Toast
    // faildCreateCustomer:"Faild to create customer",
    // customerCreatedSuccess:"Customer has been created successfully",

    //Customer single page
    customer: "Customer",
    addNotice: "Add Notice",

    //choose noticetype modal
    chooseNoticeType: "Choose a notice type",

    // customer sidebar menu
    customerDetails: "Customer Details",
    services: "Services",

    //customer update form
    update: "Update",
    customerBottomDescription:
      "You can not modified customer’s information once customer claimed their account. Only empty field can be modified. Customer still able to update their details through their login.",

    //customer update toast
    customerUpdatedFailed: "Customer updated failed",
    customerUpdateSuccess: "Customer has been Updated successfully",

    // ==================================================================================
    //All Memorial Pages
    allMemorialPages: "All Memorial Pages",
    allMemorialPagesDescription: "All the memorial pages in your account",

    daysAgo: "days ago",
    ago: "ago",
    justNow: "Just now",
    year: "year",
    month: "month",
    hour: "hour",
    minute: "minute",
    second: "second",

    deathDate: "Death Date",
    status: "Status",

    //Create Memorial Modal
    createMemorialHeading: "Add a death person’s detail",
    createMemorialHeadingDescription:
      "You can edit this information later as well",

    // Create Memorial form
    personName: "Person Name",
    personNamePlaceholder: "Full Name in English",
    customerPlaceholder:
      "Select from your customer database or create as new customer",
    pageType: "Page Type",
    selectPageType: "Select a Page Type",
    relationship: "What is your relationship",
    relationshipLabel: "Relationship",
    selectRelationship: "Select a relationship",
    dateOfDeath: "Date of Death",
    dateOfBirth: "Date of Birth",
    deathPlace: "Death Place",
    deathPlacePlaceholder: "Search city of Death",
    selectBirthandDeathTitle: "Select a Birth and Death Title",
    //toast memorial create
    faildCreateMemorial: "Faild to create Memorial",
    successCreateMemorial: "Memorial has been created successfully",

    //memorial SIngle Page
    memorialPage: "Memorial Page",
    preview: "Preview",
    pageFor: "Page for",
    visibility: "Visibility",
    pageManager: "Page Manager",
    pageSettings: "Page Settings",
    recordHistory: "Record History",
    clearCache: "Clear Cache",
    editInfo: "Edit Info",
    addNickName: "Add nick name",
    addDeathDate: "Add death date",
    addDeathLocation: "Add death Location",

    //upload profile drpdown and modal
    uploadNew: "Upload new",
    setPhotoFrame: "Set photo frame",
    delete: "Delete",

    uploadYourProfileHere: "Upload your Profile here",
    chooseFile: "Choose File",
    noFileChoosen: "No File Choosen",
    submit: "Submit",

    //toast upload profile in header
    successUploadProfile: "Updated successfully",
    failedUploadProfile: "Update failed",

    updateProfilePhoto: "Update Profile Photo",
    selectFrame: "Select a Frame",

    //toast frame set
    successframeSet: "Successfully Set Frame",
    frameFailed: "Failed",

    //toast delete
    successDelete: "Successfully deleted",
    faildDelete: "Failed Delete",

    // editinfo modal header
    editPage: "Edit Page",
    preferredName: "Preferred name (Nickname)",
    knowAs: "Know as",
    countryOfOrigin: "Country of origin/native",
    tags: "Tags",
    tagsPlaceholder: "Tag locations,topics,etc",

    //editinfo toast
    updatedSuccessEditInfo: "Updated successfully",
    updatedFailedEditInfo: "Updated Failed",

    //memorial header tabmenu
    home: "Home",
    about: "About",

    //memorial Home
    edit: "Edit",
    createdBy: "Created by",

    //memorial About

    //memorial About Sidebar
    personalInformation: "Personal Information",
    contactDetails: "Contact Details",
    photosVideos: "Photos & Videos",
    designs: "Designs",

    //Personal Information
    profileDisplayLanguage: "Profile display language",
    profileDisplayLanguageDescription:
      "The page element will change based on this language.",
    DOBDescription:
      "Optional. You can give year only if are not sure about date and month",
    personNameIn: "Person Name in",
    optional: "Optional",
    fullNameIn: "Full Name in",
    briefAboutThe: "Brief about the",
    briefAboutDescriptionOne: "It can be",
    briefAboutDescriptionTwo: "’s job title, How he knowns for others?",
    quote: "Quote",

    //toast common
    updatedSuccessfully: "Updated successfully",
    updateFailed: "Update failed",

    //Locations
    locations: "Locations",
    locationDescriptionOne: "Enter all related locations for",
    locationDescriptionTwo:
      "This will help our system to notify to the relevant users.",
    livedPlaces: "Lived places",
    select: "Select",
    origin: "origin",
    addMultipleCities: "Add multiple cities",
    hometownNativePlace: "Hometown or Native place",
    hometownNativePlacePlaceholder: "’s hometown or native place",
    birthPlace: "Birth place",
    birthPlaceSingle: "birth place",
    add: "Add",
    birthPlacePlaceholder: "’s birth place",

    //Related Information
    relatedInformation: "Related Information",
    relatedInformationDescription: "Tag any related information to the person",
    religion: "Religion",
    religions: "’s Religion",
    selectAreligion: "Select a religion",
    reasonsForDeath: "Reasons for death",
    selectReasonsForDeath: "Select a reasons for death",
    selectReasonsForDeaths: "’s Reasons for death",
    schoolAndColleges: "School and Colleges",
    hashTag: "Hash Tag",
    searchAndSelect: "Search and Select",
    schoolAndCollegesPlaceholder: "’s School and Colleges",
    mentionUserOrProfile: "@mention user or profile",
    hashTagPlaceholder: "Tag locations, topic, @mention user or profile.",
    contactInformation: "Contact Information",
    contactInformationDescription:
      "This contact details viewable by user. You can control the privacy. This will be visible for the memorial page only.",
    addContact: "Add Contact",
    addNewContact: "Add new contact",
    familyAddress: "Family address",
    familyAddressDescription: "This is where you can visit to the family.",

    areYouWantToDelete: "Do you want to delete it?",

    // toast family Addres
    successFamilyAddressUpdate: "Family Address Save Successfully",
    faildFamilyAddressUpdate: "Family Address Save Failed",

    //add new contact modal
    addNewContactModalHeadingDescription:
      "This will be visible in the memorial page",
    contactType: "Contact Type",
    enterContactDetail: "Enter Contact Detail",
    additionalContact: "additional contact",
    hideContactContent: "Hide contact after 30 days?",
    yes: "Yes",
    no: "No",
    YesDeleteIt: "Yes, Delete it",
    DeleteTheContact: "Delete the contact",

    showContact: "Show contact",
    showContactDescription: "This will be  visible in the memorial page",

    //contact toast
    faildCreateContact: "Faild to create contact",
    successCreateContact: "Contact has been created successfully",

    //profile photos
    profilePhotos: "Profile Photos",
    uploadPhoto: "Upload Photo",
    uploadVideo: "Upload Video",
    uploads: "Uploads",
    upload: "Upload",
    photo: "Photo",

    // uploadprofilemodal
    here: "here",

    uploadprofilemodalHeading: "Upload your profile here",
    makeItDefault: "Make it default",

    isUploading: " is uploading....",
    profile: "Profile",
    uploadProfile: "Upload your profile here",
    uploadYour: "Upload your",
    uploadYourPhoto: "Upload your photo here",

    //toast
    successSeDefault: "Set as default successfully",
    faildSeDefault: "",

    //Upload your edited photo here modal

    uploadYourEditedPhotoHere: "Upload your edited photo here",
    displayCaption: "Display caption here for reference",
    displayInTheMemorialPage: "Display in the memorial page",

    // toastcaption
    errorUpdatingCaption: "Error updating caption",

    noticePhotos: "Notice photos",
    noticePhotosDescription: "Photos and videos that uploaded in the notices",
    deletedPhotos: "Deleted Photos",
    restore: "Restore",

    //design setting
    designSettings: "Design Settings",
    birthAndDeathTitle: "Birth and Death Title",
    birthDeathTitle: "Birth & Death Title",
    decoration: "Decoration",
    changeTemplate: "Change Template",
    browseTemplate: "Browse Template",
    changeFrame: "Change Frame",
    browseFrame: "Browse Frame",
    changeClipArt: "Change Clipart",
    browseClipArt: "Browse Clipart",
    changeBorder: "Change Border",
    browseBorder: "Browse Border",
    changeHeaderBackground: "Change Header background",
    changePageBackground: "Change Page background",

    headerBackground: "Header Background",
    pageBackground: "Page Background",
    pageBackgrounds: "Page Backgrounds",

    //Templates modal
    templates: "Templates",
    template: "Template",
    templatesDescription: "This will be visible in the memorial page",
    hindu: "Hindu",
    classic: "Classic",
    general: "General",

    //toast template
    alreadySelectedTemplate: "Already selected Template",

    frames: "Frames",
    clipArts: "Clip Arts",
    borders: "Borders",

    //pagemanagers

    pageManagers: "Page Managers",
    client: "Client",
    clientDescription:
      "The person who created memorial page. This user has the admin access for the page .",
    change: "Change",
    pageMangerDescription:
      "These user can modify and change settings of the notice by login to the user portal",
    inviteManagerCarddescription: "Invitation sent to this email",
    remove: "Remove",
    inviteNewManagers: "Invite new managers",

    //modal invite manager
    addNewManager: "Add new manager",
    addNewManagerDescription: "Invite new user to manage this page",
    managerType: "Manager Type",
    selectManagerType: "Select a Manager Type",
    invite: "Invite",

    //Change customer
    changeCustomer: "Change customer",
    changeCustomerDescription: "This will be visible in the memorial page",

    // General Settings
    generalSettings: "General Settings",

    pagePassword: "Page Password",
    recordInformation: "Record Information",
    memorialPageID: "Memorial Page ID",
    createdAt: "Created at",
    lastModifiedAt: "Last modified at",

    // =====================================================================================
    //All Notices
    allNotices: "All Notices",
    allNoticesDescription: "All the notices in your account",

    obituaryForGroupOfPeople: "Obituary for A group of people",
    others: "others",
    other: "other",

    //create Notice
    createNotice: "Create a Memorial Mass Notice",
    createNoticeDescription: "Create and manage new notice for a page",
    notice: "Notice",
    createA: " Create a",
    deathPersonDetails: "Death Person’s Details",
    deathPerson: "Death Person",
    deathPersonInputDescription: "Select from your page database or ",
    or: "or",
    createAsNewPage: "create as new page",
    deathPersonInputPlaceholder:
      "Search or Create death person’s profile page(Multiple)",
    selectNoticeType: "Select Notice Type",
    selectOutputLanguage: "Select Output Language",
    selectALanguage: "Select a Language",

    whoIsPaying: "Who is paying for the service?",
    relationshipCreateNoticeInputDescription:
      "What is relationship for the customer?. This is notice for",
    langCreateNotice: "Notice language going to be publish in what language?",

    customerInputDescription: "Select from your customer database or ",
    createNewCustomer: "create as new customer",

    createNoticeCheckboxLabel: "Stop sending invitation to the customer",

    //create customer modal
    addNewCustomerModalDescription:
      "They can manage this notice by login to their user portal",
    enterEmail: "Enter Email",
    enterFirstName: "Enter First Name",
    enterLastName: "Enter Last Name",
    enterPassword: "Enter Password",
    useThisCustomer: "Use this customer",
    continueWithNewEmail: "Continue with new email",
    weHaveFoundCustomer: "We have found a customer with this email.",
    weHaveFoundCustomerMobile: "We have found a customer with this mobile.",

    continue: "Continue",

    //notice single page

    noticePage: "Notice Page",
    manageSmall: "Manage",
    addMorePages: "Add more Pages",
    obituaryNotice: "Obituary Notice",
    previewNotice: "Preview Notice",
    completeOrder: "Complete Order",

    //Notice Home sidebar Menu
    noticeDetails: "Notice Details",
    noticeSettings: "Notice Settings",
    orderDetails: "Order Details",
    customerManager: "Customer & Manager",

    //notice details
    fillAllRequiredAlert: "Fill all Required information!",
    fillAllRequiredAlertDescription:
      "Please update all of the required information before you activate page.",

    noticePageLanguage: "Notice Page Language",
    noticePageLanguageSelectDescription:
      "Output page elements will change based on this language",

    nameIn: "name in",
    noticeTitle: "Notice Title",
    searchOrAddNew: "Search or add a new",

    selectATemplate: "Select Template",

    //Notice Contents
    noticeContents: "Notice Contents",
    viewSample: "View Sample",

    addCustomInformation: "Add a custom information",
    addPreNameInformation: "Add pre name information",
    mainContent: "Main Content",
    switchToSimpleMode: "Switch to simple mode",
    switchToCustomParagraphMode: "Switch to custom paragraph mode",

    addFamilyMembersAndRelatives: "Add family members and relatives",
    addFamilyMembersAndRelativesDescription:
      "Make it simple death notice content by adding relationship names of the person.",

    //modal Add family and relatives
    addFamilyAndRelatives: "Add family and relatives",
    addFamilyAndRelativesDescription: "This will be visible in the notice page",
    additionalInformation: "Additional Information",
    additionalInformationPlaceholder:
      "Relationship/Job title/Education title/Location",
    passedAway: "Passed away?",

    //Informant Title
    informantTitle: "Informant Title",
    suggestion: "Suggestion:",

    //Informant
    informant: "Informant",

    // Add a informant modal
    addInformant: "Add a informant",
    addInformantDescription: "This will be visible in the notice page",
    additionalText: "Additional Text",

    additionalTextDescription:
      "Status of the person/Relationship/Job title/Phone No.",
    groupByRelationship: "Group by relationship",

    //Events
    events: "Events",
    eventsDescription:
      "Posting an event is the easiest way to let visitors to the story know about funeral services being hosted.",
    addAnEvent: "Add an event",

    showEvent: "Show Event",
    showEventDescription: "This will be visible in the notice page",

    //modal Add new event
    addNewEvent: "Add new event",
    addNewEventDescription: "Here you can add all the event services",
    eventType: "Event type",
    selectAEvent: "Select a Event",
    date: "Date",
    startTime: "Start Time",
    endTime: "End Time",
    timeDescription:
      "For 24 hours event (whole day) select 00.00 in both fields",
    eventLocation: "Event Location",
    venue: "Venue",
    virtualMeeting: "Virtual Meeting",
    eventPlaceSearch: "Event Place Search",
    specialInstruction: "Special Instruction",
    flowerDeliveryToThisPlace: "Flower Delivery to this place?",
    eventInstruction: "Event Instruction",
    eventInstructionPlaceholder: "virtual Event information",

    deleteTheEvent: "Delete the event",

    // /Contacts
    contacts: "Contacts",
    contactDescription:
      "Posting a contact is the easiest way to let visitors to contact the family members.",

    //Photos
    photos: "Photos",
    photosDescription: "Add or select photos for highlights in the notice",

    showImage: "Show Image",

    // modal photo
    uploadYourPhotosHere: "Upload your photos here",
    addPhotosFromProfile: "Add photos from profile",

    addPhotosFromProfilemodal:
      "All the information you add here will be visible in the memorial page",
    addPhotosFromProfilemodalDescription:
      "This will be visible in the notice page",

    //Live Streaming
    liveStreaming: "Live Streaming",
    liveStreamingDescription:
      "Add or select photos for highlights in the notice",
    liveStreamingDescriptionDescription: "Use our own LIVE streaming solution",
    liveStreamingInformation: "Live streaming information",
    liveStreamingInformationDescription:
      "You can live stream using our broadcasting option",
    liveStreamingURL: "Live streaming URL",
    liveStreamingURLDescription:
      "Our system will support to embed from Vimeo and YouTube only. Other url will be open in a new window.",
    liveStreamingURLPlaceholder:
      "Paste a link to embed content from another site and press Enter",

    //Videos
    videos: "Videos",
    videosDescription: "Embed your video in this box",
    videoURL: "Video URL",
    videoDescription:
      "Try using a URL instead of an embed code if the widget is from Instagram, Facebook, Twitter, Youtube, Vimeo and Dailymotion. External url will be open in a new window.",

    lastUpdatedBy: "Last updated by",

    //Notice settings
    noticeDecoration: "Notice Decoration",
    // birthDeathTitle: "Birth & Death Title",
    backgroundMusic: "Background Music",
    backgroundMusicUploading: "Background Music is Uploading",
    customMusic: "Upload your music here",
    customMusicUploading: "Music is Uploading",
    changeCustomMusic: "Change",
    uploadCustomMusic: "Upload custom music",
    backgroundMusicdescription:
      "We support only mp3 and wave audio format. Maximum 10mb allowed",
    notSupportAudioTag: "Your browser does not support the audio tag.",

    //Background Music
    backgroundMusicModalDescription:
      "We support only mp3 and wave audio format. Maximum 10mb allowed",

    whatIsCustomerRelationship: "What is customer’s relationship?",
    familyAddressDescriptionNoticeSettings:
      "Add family address if they want to publish within the notice for people to visit their home",
    overrideFamilyAddressForDisplay: "Override Family address for display",
    overrideFamilyAddressForDisplayDescription:
      "Override family address if you want to add extra information",

    additionalSettings: "Additional Settings",
    noticeVisibility: "Notice Visibility",
    optionalSettings: "Optional Settings",
    hide: "Hide",
    noticeCap: "NOTICE",
    contactCap: "CONTACT",
    hideContactsToggleLabel: "after the 3 days of the funeral date",
    hideNoticeToggleLabel: "after 7 days of the funeral date",
    hideDate: "Hide date of birth and death date",

    customerInformation: "Customer Information",
    owner: "Owner",
    ownerDescription: "The person who created memorial page.",

    whoCanManageNoticePCustomerMAnager: "Who can manage this notice?",
    CustomerMAnagerdescription:
      "These user can modify and change settings of the notice by login to the user portal",

    live: "Live",
    goToLink: "Go to Link",

    //My Account setting
    myAccountSetting: "My Account setting",
    editYourPersonalInformation: "Edit your personal information",

    securityInformation: "Security Information",

    confirm: "Confirm",
    //Partner Admins
    partnerAdmins: "Partner Admins",
    partnerAdminsDescription: "Manage your admin users",

    // Add a partner admin modal
    addPartnerAdmin: "Add a partner admin",
    showPartnerAdmin: "Show Partner Admin",
    addPartnerAdminDescription: "You can edit this information later as well",
    loginWithPassword: "Login with password",
    loginWithGoogle: "Login with Google",

    assignServiceProviders: " Assign Service Providers",

    contactWith: "Contact with",
    name: "Name",
    phoneNumber: "Phone Number",
    message: "Message",
    // notice create
    SelectNoticeType: "Select Notice Type",
    uploadmusic: "Upload Custom Music",
    changemusic: "Change Custom Music",
    created: "Created",
    convertToGroupNotice: "Convert to Group Notice",

    addPerson: "Add a person",
  },
};
