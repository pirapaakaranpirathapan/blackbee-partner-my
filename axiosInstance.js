import axios from 'axios'

const baseURL = 'http://dev.zoftline.com'
const username = 'myUsername'
const password = 'myPassword'

const axiosInstance = axios.create({
  baseURL: baseURL,
  auth: {
    username: username,
    password: password,
  },
})

export default axiosInstance
