import Filter from "@/components/Filter/Filter";
import React, { useEffect, useRef, useState } from "react";
import Image from "next/image";
import Avatar from "../public/avatar.jpg";
import { PaginationComponent } from "@/components/Elements/Pagination";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import {
  deletePartnerAdmin,
  getAllPartnerAdmins,
  partnerAdminSelector,
} from "@/redux/partner-admins";
import { ACTIVE_PARTNER } from "@/config/constants";
import FullScreenModal from "@/components/Elements/FullScreenModal";
import PartnerAdminModal, {
  ChildModalRef,
} from "@/components/Modal/createPartnerAdminModal";
import { Button } from "react-bootstrap";
import CustomerSkelton from "@/components/Skelton/CustomerSkelton";
import { getLabel } from "@/utils/lang-manager";
import { integer } from "aws-sdk/clients/cloudfront";
import TableCardCustomerSkelton from "@/components/Skelton/TableCardCustomerSkelton";
import TableCardOne from "@/components/Cards/TableCardOne";
import router, { Router, useRouter } from "next/router";
import TableCardTwo from "@/components/Cards/TableCardTwo";
import { getCookie } from "cookies-next";
import AlertModal from "@/components/Elements/AlertModal";
import { Notification } from "@arco-design/web-react";
import MainPagination from "@/components/Paginations";
import ViewPartnerAdminModal, {
  ChildModalRefUpdate,
} from "@/components/Modal/viewPartnerAdminModal";
import CustomImage from "@/components/Elements/CustomImage";

const UserAccess = ({ initialLanguage }: any) => {
  const dispatch = useAppDispatch();
  const { data: partnerAdminsData } = useAppSelector(partnerAdminSelector);
  const getAdmin = (page: integer) => {
    setPartnerAdminsSkeleton(true);
    dispatch(
      getAllPartnerAdmins({
        active_partner: ACTIVE_PARTNER,
        page: page,
      })
    ).then(() => {
      setPartnerAdminsSkeleton(false);
    });
  };

  const actionBtnRef = useRef<HTMLButtonElement>(null);
  // useEffect(() => {
  //   getAdmin(
  //     localStorage.getItem("partner_admins")
  //       ? parseInt(localStorage.getItem("partner_admins") as string, 10)
  //       : 1
  //   );
  // }, []);
  const [page, setPage] = useState(1);
  const [showInfo, setShowInfo] = useState(false);
  const [ShowProfileBanner, setShowProfileBanner] = useState(false);
  const childModalRef = useRef<ChildModalRef>(null);
  const childModalRefUpdate = useRef<ChildModalRefUpdate>(null);
  const [partnerAdminsSkeleton, setPartnerAdminsSkeleton] = useState(true);
  //page no query -----------------------------------------------------//
  const router = useRouter();
  const { query } = router;
  const handlePagination = (page: number) => {
    setPage(page);
    getAdmin(page);
    // localStorage.setItem("partner_admins", page.toString());
    if (page !== null) {
      router.push(
        { pathname: router.pathname, query: { ...query, p: page } },
        undefined,
        { shallow: true }
      );
    }
    setPartnerAdminsSkeleton(true);
  };
  const pageNo = query && query.p !== undefined ? parseInt(query?.p[0]) : 1;
  const [showData, setShowData] = useState({
    status: "",
    first_name: "",
    last_name: "",
    email: "",
    // id: "",
  });
  useEffect(() => {
    handlePagination(pageNo);
    getAdmin(pageNo);
  }, []);
  const handleClick = async () => {
    setCreateUserAccessButton(true);
    if (childModalRef.current) {
      childModalRef.current.callModalFunction(handleChildResponse);
    }
    fewSecWait();
  };

  const fewSecWait = () => {
    setTimeout(() => {
      setCreateUserAccessButton(false);
    }, 1500);
  };

  const handleChildResponse = (response: any) => {
    console.log("fffff", response);

    getAdmin(1);
    setShowProfileBanner(false);
    setCreateUserAccessButton(false);
  };
  const handleClickUpdate = async () => {
    if (childModalRefUpdate.current) {
      console.log("childModalRefUpdate", childModalRefUpdate.current);

      childModalRefUpdate.current.callModalFunctionUpdate(handleChildResponse);
    }
  };
  function singleView(uuid: any) {
    setShowInfo(true);
  }
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const deleteAdmins = (uuid: string) => {
    dispatch(
      deletePartnerAdmin({
        active_partner: ACTIVE_PARTNER,
        partner_admin_uuid: partnerAdminsData?.data[0]?.uuid,
        uuid: uuid,
      })
    ).then((res: any) => {
      if (res?.payload?.success === true) {
        setDeleteUser(false);
        Notification.success({
          title: "Deleted successfully",
          duration: 3000,
          content: undefined,
        });
        dispatch(
          getAllPartnerAdmins({
            active_partner: ACTIVE_PARTNER,
            page: 1,
          })
        );
        setShowDeleteModal(false);
      } else {
        setDeleteUser(false);
        Notification.error({
          title: "Something went wrong",
          duration: 3000,
          content: undefined,
        });
      }
    });
  };
  const [createUserAccessButton, setCreateUserAccessButton] = useState(false);
  const [deleteUser, setDeleteUser] = useState(false);
  return (
    <>
      <AlertModal
        show={showDeleteModal}
        isSmallModal={true}
        position="centered"
        title="Confirmation"
        onClose={() => {
          setShowDeleteModal(false);
        }}
      >
        <div className="mb-4 d-flex">
          Do you want to delete
          <span
            className="text-truncate ps-1 font-semibold"
            style={{
              maxWidth: "100px",
            }}
          >
            {/* {partnerAdminsData && partnerAdminsData?.data[0]?.first_name} */}
          </span>
          ?
        </div>

        {/* <p className="mb-4 text-truncate">Do you want to delete <span className="text-truncate" style={{ maxWidth: "30px" }}>{props?.data?.name}</span>?</p> */}
        <div className="col-auto">
          <button
            className="btn btn-system-primary me-3"
            onClick={() => setShowDeleteModal(false)}
          >
            No
          </button>
          <span
            className="text-danger pointer"
            onClick={() => {
              setDeleteUser(true);
              deleteUser
                ? null
                : deleteAdmins(partnerAdminsData?.data[0]?.uuid);
            }}
          >
            Yes, Delete it
          </span>
        </div>
      </AlertModal>

      <FullScreenModal
        show={ShowProfileBanner}
        onClose={() => {
          setShowProfileBanner(false);
        }}
        title={`${getLabel("addPartnerAdmin", initialLanguage)}`}
        description={`${getLabel(
          "addPartnerAdminDescription",
          initialLanguage
        )}`}
        footer={
          <Button
            className="btn-system-primary pointer"
            disabled={createUserAccessButton}
            onClick={handleClick}
          >
            {getLabel("create", initialLanguage)}
          </Button>
        }
        deleteLabel={
          <a
            onClick={() => {
              setShowProfileBanner(false);
            }}
          >
            {getLabel("close", initialLanguage)}
          </a>
        }
      >
        {/* <CustomerDetailsModal ref={childModalRef} /> */}
        <PartnerAdminModal ref={childModalRef} initialLang={initialLanguage} />
      </FullScreenModal>
      <div className="d-flex justify-content-between align-items-center page-heading-container">
        <div className="page-header">
          <h1 className="page-heading">
            {getLabel("partnerAdmins", initialLanguage)}
          </h1>
          <p className="page-heading-sub text-muted">
            {getLabel("partnerAdminsDescription", initialLanguage)}
          </p>
        </div>
        {!partnerAdminsSkeleton ? (
          <button
            onClick={() => {
              setShowProfileBanner(true);
            }}
            type="button"
            className="btn btn-system-primary mt-n10 border-0"
            // onClick={() => {
            //   setNoticeCreate(true);
            // }}
          >
            <strong>+ </strong>
            {getLabel("create", initialLanguage)}
          </button>
        ) : (
          <button
            type="button"
            className=" d-none d-sm-flex me-3 shimmer-button22 border-0 shimmer-effect"
          ></button>
        )}
      </div>
      <hr className="page-heading-divider" />
      <div className="mb-0">
        <Filter />
      </div>
      <div className="">
        <>
          {partnerAdminsSkeleton ? (
            <>
              <CustomerSkelton loopTimes={9} />
            </>
          ) : (
            <>
              <div className="d-none d-sm-block">
                <table className="table mt-3 ">
                  <tbody className=" ">
                    {Array.isArray(partnerAdminsData?.data) &&
                      partnerAdminsData?.data?.map((any: any) => {
                        return (
                          <tr className=" align-middle ">
                            <td
                              className="col-3 ps-0"
                              style={{ maxWidth: "200px" }}
                            >
                              <div className="d-flex align-items-center  gap-3">
                                <div className="ms-md-3 ms-3 table-img text-muted font-140 system-text-dark-light d-flex align-items-center justify-content-center rounded-circle">
                                  {/* <>
                                      <Image
                                        // src={data?.header_background.image_url}
                                        src={Avatar}
                                        alt=""
                                        className="image rounded-circle"
                                        width={400}
                                        height={400}
                                      />{" "}
                                    </> */}
                                  {any && any.photo_url ? (
                                    <CustomImage
                                      src={any.photo_url}
                                      alt=""
                                      className="image rounded-circle"
                                      width={200}
                                      height={200}
                                      divClass="table-img rounded-circle"
                                    />
                                  ) : any && any.first_name ? (
                                    <div className="rounded-circle">
                                      {/* {`${any.first_name[0]}${
                                          any.last_name ? any.last_name[0] : ""
                                        }`} */}
                                      {/* {any.first_name[0]}
                                      {`${any.first_name[0]}${
                                        any.last_name ? any.last_name[0] : ""
                                      }`} */}
                                      {any.last_name[0]}
                                    </div>
                                  ) : (
                                    ""
                                  )}
                                </div>{" "}
                                <div style={{ maxWidth: "50%" }}>
                                  <div>
                                    <p className="p-0 m-0 text-dark font-110  text-truncate font-semibold">
                                      {any.first_name} {any.last_name}
                                    </p>
                                  </div>

                                  <div className="text-dark font-90  text-truncate">
                                    {any.state.name}
                                  </div>
                                </div>
                              </div>
                            </td>
                            <td
                              className="col-3 p-0"
                              style={{ maxWidth: "90px" }}
                            >
                              <>
                                <div className="text-secondary font-110 d-flex align-items-center justify-content-start ">
                                  <p className="p-0 m-0">{any.email}</p>
                                </div>
                                <div
                                  className="text-dark font-90 d-flex align-items-center  justify-content-start "
                                  style={{ maxWidth: "80%" }}
                                ></div>
                              </>
                            </td>

                            <td className="col-1 p-0 ">
                              <div className="p-0 m-0 d-flex align-items-center justify-content-end">
                                <button
                                  type="button"
                                  className="btn btn-system-light me-3"
                                  onClick={() => {
                                    setShowInfo(true);
                                    setShowData({
                                      status: any.state.name,
                                      first_name: any.first_name,
                                      last_name: any.last_name,
                                      email: any.email,
                                      // id: any.id,
                                    });
                                    localStorage.setItem(
                                      "edit_partner_id",
                                      any.uuid
                                    );
                                  }}
                                >
                                  {getLabel("view", initialLanguage)}
                                </button>
                              </div>
                            </td>
                          </tr>
                        );
                      })}
                  </tbody>
                </table>
              </div>{" "}
            </>
          )}
          <div className="d-block d-sm-none">
            {partnerAdminsSkeleton ? (
              <TableCardCustomerSkelton loopTimes={9} />
            ) : (
              Array.isArray(partnerAdminsData?.data) &&
              partnerAdminsData?.data?.map((data: any) => {
                return (
                  <>
                    {
                      <div className="card border bg-white mb-3">
                        <div className="card-body">
                          <div className="row m-0">
                            <div className="col-2  p-0">
                              <div className=" d-flex justify-content-center">
                                <div className=" table-img text-muted font-140 system-text-dark-light d-flex align-items-center justify-content-center rounded-circle">
                                  {/* {data && data.profile ? (
                                          <Image
                                            src={Avatar}
                                            alt=""
                                            className="image rounded-circle"
                                          />
                                        ) : data && data?.["first_name"] ? (
                                          data?.["first_name"][0]
                                        ) : data && data.name ? (
                                          data?.name[0]
                                        ) : (
                                          ""
                                        )} */}
                                  {data && data.photo_url ? (
                                    <CustomImage
                                      src={data.photo_url}
                                      alt=""
                                      className="image rounded-circle"
                                      width={200}
                                      height={200}
                                      divClass="table-img rounded-circle"
                                    />
                                  ) : data && data.first_name ? (
                                    <div className="rounded-circle">
                                      {`${data.first_name[0]}${
                                        data.last_name ? data.last_name[0] : ""
                                      }`}
                                    </div>
                                  ) : (
                                    ""
                                  )}
                                </div>
                              </div>
                            </div>
                            <div className="col-10 ps-3">
                              <div className="mb-2">
                                <div className=" p-0 font-110 font-semibold ">
                                  <div className="d-flex">
                                    <p className="text-truncate p-0 m-0">
                                      {data?.["first_name"] &&
                                      data?.["first_name"]
                                        ? data?.["first_name"]
                                        : ""}{" "}
                                      {data?.["last_name"] &&
                                      data?.["last_name"]
                                        ? data?.["last_name"]
                                        : "unknown"}
                                    </p>
                                  </div>
                                </div>

                                <div className=" p-0 text-dark font-90  text-truncate font-110">
                                  {data?.email ? data?.email : ""}
                                </div>
                              </div>
                              <div>
                                <button
                                  onClick={() => {
                                    singleView(data.uuid);
                                    setShowData({
                                      status: data.state.name,
                                      first_name: data.first_name,
                                      last_name: data.last_name,
                                      email: data.email,
                                      // id: data.id,
                                    });
                                    setShowInfo(true);
                                  }}
                                  type="button"
                                  className="btn btn-system-light col-12 pointer mt-2"
                                >
                                  {getLabel("view", initialLanguage)}
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    }
                  </>
                );
              })
            )}
          </div>

          <hr className="page-heading-divider" />
          <div className="row align-items-center m-0 mt-4">
            <div className="col-lg-6 col-12 p-0 font-110 text-center text-lg-start">
              {getLabel("totalRecords", initialLanguage)}:
              <span> {partnerAdminsData?.total}</span>{" "}
            </div>

            <div className="col-lg-6 col-12 p-0 d-flex justify-content-center justify-content-lg-end  mt-2 mt-lg-0">
              {partnerAdminsData?.total > 15 && (
                // <PaginationComponent
                //   page={
                //     // localStorage.getItem("partner_admins")
                //     //   ? parseInt(
                //     //       localStorage.getItem("partner_admins") as string,
                //     //       10
                //     //     )
                //     //   : 1
                //     pageNo
                //   }
                //   totalPages={Math.ceil(partnerAdminsData.total / 15)}
                //   handlePagination={handlePagination}
                // />
                <MainPagination
                  page={pageNo}
                  totalPages={partnerAdminsData.total}
                  handlePagination={handlePagination}
                />
              )}
            </div>
          </div>
        </>
      </div>
      {/* <FullScreenModal
        show={false}
        onClose={() => {
          setShowInfo(false);
        }}
        title="Show partner admin"
        footer={
          <a
            onClick={() => {
              setShowDeleteModal(true);
              setShowInfo(false);
            }}
          >
            <div className="text-danger pointer">Delete the contact</div>
          </a>
        }
      >
        <div className="row align-items-center m-0 ">
          <div className="row m-0 p-0 mb-3">
            <div className="col-md-12 col-12 p-0">
              <div className="font-semibold">Status</div>
            </div>
            <div className="col-md-12 col-12  p-0">
              <input
                type="text"
                className="form-control border border-2 p-2 system-control "
                placeholder=""
                disabled
                value={showData.status}
              />
            </div>
          </div>
          <div className="row m-0 p-0 mb-3">
            <div className="col-md-12 col-12 p-0">
              <div className="font-semibold">First Name</div>
            </div>
            <div className="col-md-12 col-12  p-0">
              <input
                type="text"
                className="form-control border border-2 p-2 system-control "
                placeholder=""
                disabled
                value={showData.first_name}
              />
            </div>
          </div>
          <div className="row m-0 p-0 mb-3">
            <div className="col-md-12 col-12 p-0">
              <div className="font-semibold">Last Name</div>
            </div>
            <div className="col-md-12 col-12  p-0">
              <input
                type="text"
                className="form-control border border-2 p-2 system-control "
                placeholder=""
                disabled
                value={showData.last_name}
              />
            </div>
          </div>
          <div className="row m-0 p-0 mb-3">
            <div className="col-md-12 col-12 p-0">
              <div className="font-semibold">Email</div>
            </div>
            <div className="col-md-12 col-12  p-0">
              <input
                type="text"
                className="form-control border border-2 p-2 system-control "
                placeholder=""
                disabled
                value={showData.email}
              />
            </div>
          </div>
        </div>
      </FullScreenModal> */}

      <FullScreenModal
        show={showInfo}
        onClose={() => {
          setShowInfo(false);
        }}
        title="View partner admin"
        footer={
          <div style={{ display: "flex", gap: 2 }}>
            <Button
              className="btn-system-primary pointer"
              // disabled={createUserAccessButton}
              onClick={handleClickUpdate}
            >
              Update
            </Button>
            <a
              onClick={() => {
                setShowDeleteModal(true);
                setShowInfo(false);
              }}
              style={{ marginLeft: "10px", marginTop: "7px" }}
            >
              <div className="text-danger pointer">Deactivate </div>
            </a>
          </div>
        }
      >
        <ViewPartnerAdminModal ref={childModalRefUpdate} />
      </FullScreenModal>
    </>
  );
};
export async function getServerSideProps({ req, res, query }: any) {
  // const idQuery = query.id || "";
  const pageQuery = query && query.p !== undefined ? parseInt(query?.p[0]) : 1;
  // const pageQuery=(query && Array.isArray(query?.p) && query.p !== undefined)? parseInt(query?.p[0]) : 1;
  const initialLanguage = getCookie("language", { req, res });

  const cookies = getCookie("token", { req, res });
  if (cookies == undefined) {
    res.writeHead(307, { Location: "/" });
    res.end();
  }

  return {
    props: {
      pageQuery,
      initialLanguage,
    },
  };
}
export default UserAccess;
