import React, { useEffect, useState } from 'react'
import router, { useRouter } from 'next/router'
import { Form, InputGroup } from 'react-bootstrap'
import Image from 'next/image'
import Link from 'next/link'
import Noticevj from "../../public/Noticevj.svg"
import { useAppDispatch, useAppSelector } from '@/redux/store/hooks'
import { authSelector, login } from '@/redux/auth'
import { getCookie, setCookie, setCookies } from 'cookies-next'
import Loader from '@/components/Loader/Loader'
import back1 from '../../public/back1.svg';
import { Notification } from "@arco-design/web-react";
import { getLabel } from '@/utils/lang-manager'



const Contact: React.FC = ({ initialLanguage }: any) => {

  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [subject, setSubject] = useState('Inquiry from Blackbee Website'); // Default subject value
  const [phone, setPhone] = useState('');
  const [message, setMessage] = useState('');

  // Validation states
  const [nameError, setNameError] = useState('');
  const [emailError, setEmailError] = useState('');
  const [phoneError, setPhoneError] = useState('');
  const [messageError, setMessageError] = useState('');
  const router = useRouter();


  const handleBack = () => {
    router.push("/");
  }

  const handleSendEmail = async () => {

    // Validation before sending email
    let isValid = true;

    if (!name.trim()) {
      setNameError('Name is required');
      isValid = false;
    } else {
      setNameError('');
    }

    if (!email.trim()) {
      setEmailError('Email is required');
      isValid = false;
    } else if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)) {
      setEmailError('Invalid email format');
      isValid = false;
    } else {
      setEmailError('');
    }

    if (!phone.trim()) {
      setPhoneError('Phone number is required');
      isValid = false;
    } else if (!/^\d{10}$/g.test(phone)) {
      setPhoneError('Phone number must be 10 digits');
      isValid = false;
    } else {
      setPhoneError('');
    }

    if (!message.trim()) {
      setMessageError('Message is required');
      isValid = false;
    } else {
      setMessageError('');
    }

    if (!isValid) {
      return;
    }
    try {
      const text = `
        Hi,

        My name is ${name}.
        My phone number is ${phone}.
        
        Message:
        ${message}
        
        Best regards,
        ${name}
      `;

      const response = await fetch('/api/sendEmail', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          to: email,
          subject,
          text: text,
        }),
      });

      if (response.ok) {
        Notification.success({
          title: "Email has been Sent Successfully",
          duration: 3000,
          content: undefined,
        });
        console.log('Email sent successfully!');
      } else {
        Notification.error({
          title: "Error sending email failed",
          duration: 3000,
          content: undefined,
        });
        console.error('Error sending email:', await response.text());
      }
    } catch (error) {
      Notification.error({
        title: "Error sending email",
        duration: 3000,
        content: undefined,
      });
    }
  };


  return (
    <>
      <div className="mb-5">
        <nav className="navbar position-sticky top-0 w-100 ps-2 pe-2 pe-md-0 ">
          <div className="row">
            <div className="col-auto  login-profile-box mt-2 ms-3 rounded">
              <Image src={Noticevj} alt="" className="image rounded" />
            </div>
            <div className="col-auto pointer">
              <h6 className="fw-semibold col-12 mt-2 mb-0">BlackBee</h6>
              <p className="font-90 fw-semibold">Partner</p>
            </div>
          </div>
        </nav>
      </div>
      {/* <div className='bg-warning' style={{ maxWidth: "450px", margin: "0 auto " }}>
        <div className="w-100 " >
          <Image src={back1} alt='' width={26} height={26} onClick={handleBack} className='pointer' />
        </div>
      </div > */}

      <div className="d-flex justify-content-center w-100">
        <div className="col-9 col-sm-6 col-md-4 col-lg-4 col-xl-3 login-container">
          <div className='d-flex gap-2 mb-5'>
            <div className="">
              <Image src={back1} alt='' width={26} height={26} onClick={handleBack} className='pointer' />
            </div>
            <h5 className="fw-semibold d-flex justify-content-center">
              {getLabel("contactWith", initialLanguage)} BlackBee’s Partner
            </h5>
          </div>

          <Form.Group controlId="formEmail">
            <Form.Label className="m-0 mb-1">{getLabel("name", initialLanguage)}</Form.Label>
            <Form.Control
              value={name}
              onChange={(e) => setName(e.target.value)}
              placeholder="Sam"
              className="form-control inner-shadow-default p-2 "
            />
            <div className='mb-2'>
              {nameError && <p className="text-danger">{nameError}</p>}
            </div>

          </Form.Group>

          <Form.Group controlId="formEmail">
            <Form.Label className="m-0 mb-1">{getLabel("email", initialLanguage)}</Form.Label>
            <Form.Control
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              placeholder="sam@gmail.com"
              className="form-control inner-shadow-default p-2"
            />
            <div className='mb-2'>
              {emailError && <p className="text-danger">{emailError}</p>}
            </div>


          </Form.Group>

          <Form.Group controlId="formEmail">
            <Form.Label className="m-0 mb-1">{getLabel("phoneNumber", initialLanguage)}</Form.Label>
            <Form.Control
              value={phone}
              onChange={(e) => setPhone(e.target.value)}
              placeholder="+94"
              className="form-control inner-shadow-default p-2 "
            />
            <div className='mb-2'>
              {phoneError && <p className="text-danger">{phoneError}</p>}
            </div>

          </Form.Group>
          <Form.Group controlId="formEmail">
            <Form.Label className="m-0 mb-1">{getLabel("message", initialLanguage)}</Form.Label>
            <Form.Control as="textarea" rows={3}
              value={message}
              onChange={(e) => setMessage(e.target.value)}
              placeholder=""
              className="form-control inner-shadow-default p-2 "
            />
            <div className='mb-2'>
              {messageError && <p className="text-danger">{messageError}</p>}

            </div>
          </Form.Group>
          <div className='d-flex gap-2 m-0 align-items-center mt-4'>
            {/* <div className="p-1 rounded btn btn-dark" onClick={handleBack}>
              <Image src={back} alt='' width={26} height={26} />
            </div> */}
            {/* <div className="rounded btn rounded border-dark w-50" onClick={handleBack}>
               
                Cancel
              </div> */}
            <div className='w-100'>
              <button className="w-100 btn btn-dark p-2" onClick={handleSendEmail}>
                {getLabel("submit", initialLanguage)}
              </button>
            </div>
          </div>
        </div>
      </div>



    </>
  )
}
export default Contact

export async function getServerSideProps({ req, res, query }: any) {
  const initialLanguage = getCookie('language', { req, res }) || "";
  return {
    props: {
      initialLanguage,
    },
  };
}

