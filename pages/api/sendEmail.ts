// pages/api/sendEmail.ts
import type { NextApiRequest, NextApiResponse } from "next/types";
import sendEmail from '../../utils/sendEmail';

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
    if (req.method !== 'POST') {
        return res.status(405).end(); // Method Not Allowed
    }


    const { to, subject, text } = req.body;

    if (!to || !subject || !text) {
        return res.status(400).json({ error: 'Missing required parameters.' });
    }

    try {
        await sendEmail(to, subject, text);
        return res.status(200).json({ message: 'Email sent successfully!' });
    } catch (error) {
        console.error('Error sending email:', error);
        return res.status(500).json({ error: 'An error occurred while sending the email.' });
    }
}
