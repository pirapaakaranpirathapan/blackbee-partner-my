import type { AppLayoutProps } from "next/app";
import "bootstrap/dist/css/bootstrap.min.css";
import DefaultLayout from "../components/Layout/Layout/DefalutLayout";
import { SSRProvider } from "react-bootstrap";
import "../styles/style.css";
import "intl-tel-input/build/css/intlTelInput.css";
import Head from "next/head";
import LoginLayout from "@/components/Layout/Layout/LoginLayout";
import HomePage from ".";
import LoginPage from "./login";
import { Provider } from "react-redux";
import { store } from "@/redux/store/store";
import { getCookie } from "cookies-next";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import "@arco-design/web-react/dist/css/arco.css";
import Contact from "./contact";
import ContactLayout from "@/components/Layout/Layout/ContactLayout";
import ConactUsPage from "./contact-index";
// import { DEFAULT_LANGUAGE } from "@/config/constants";
import { ConfigProvider } from "@arco-design/web-react";

import enUS from "../config/dayjs/locale/en-US";
interface PlaceAutocompleteProps {
  onPlaceSelect: (place: google.maps.places.PlaceResult) => void;
  className?: string;
}

const MyApp = ({ Component, pageProps }: AppLayoutProps) => {
  const [languageInitial, setLanguageInitial] = useState("")

  const isLoginPage = Component === HomePage || Component === LoginPage;
  const GetLayout = isLoginPage ? LoginLayout : DefaultLayout;

  const isContactPage = Component === ConactUsPage || Component === Contact;
  const GetContactLayout = isContactPage ? ContactLayout : DefaultLayout;
  const router = useRouter();

  useEffect(() => {
    const googleMapsScript = document.createElement("script");
    googleMapsScript.src = `https://maps.googleapis.com/maps/api/js?key=AIzaSyBMocC7IbWoCOCC_wqCi5_gF9M7wd89UhY&libraries=places`;
    googleMapsScript.async = true;
    document.head.appendChild(googleMapsScript);
  }, []);

  useEffect(() => {
    const initialLanguage: string = getCookie('language') as string || "";
    setLanguageInitial(initialLanguage);
  }, []);

  const getFontFamily = () => {
    let font = "";
    let fontUrl = "";
    let lang = languageInitial;

    if (lang == "Tamil") {
      font = "'Noto Sans Tamil',";
      fontUrl =
        "https://fonts.googleapis.com/css2?family=Noto+Sans+Tamil:wght@200;300;400;500;600;700;800;900&display=swap";
    } else if (lang == "Sinhala, Sinhalese") {
      font = "'Noto Sans Sinhala',";
      fontUrl =
        "https://fonts.googleapis.com/css2?family=Noto+Sans+Sinhala:wght@200;300;400;500;600;700;800;900&display=swap";
    } else if (lang == "English") {
      font = "'Work Sans', sans-serif,";
      fontUrl =
        "https://fonts.googleapis.com/css2?family=Work+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap";
    } else font = "'Work Sans', sans-serif,";
    fontUrl =
      "https://fonts.googleapis.com/css2?family=Work+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap";

    font += "Verdana, 'sans - serif' !important";

    return { family: font, url: fontUrl };
  };
  return (
    <>
      <Head>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <title>BlackBee</title>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0"
        />
        <meta name="description" content="BlackBee Partner Portal Dashboard" />
        <link rel="icon" href="/favicon.ico" />
        <link href={getFontFamily().url} rel="stylesheet" />

        <style>
          {`
                        body {
                            font-family: ${getFontFamily().family} ;
                        }`}
        </style>
      </Head>
      <Provider store={store}>
        <SSRProvider>
          <ConfigProvider locale={enUS}>
            {isContactPage ? (
              <GetContactLayout>
                <Component {...pageProps} />
              </GetContactLayout>
            ) : (
              <GetLayout>
                <Component {...pageProps} />
              </GetLayout>
            )}
          </ConfigProvider>
        </SSRProvider>
      </Provider>
    </>
  );
};

export default MyApp;

export async function getServerSideProps({ req, res, query }: any) {
  const initialLanguage = getCookie('language', { req, res }) || "";
  return {
    props: {
      initialLanguage,
    },
  };
}
