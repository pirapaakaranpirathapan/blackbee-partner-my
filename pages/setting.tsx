import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import { useRouter } from "next/router";
import React, { useEffect, useRef, useState } from "react";
import { Notification } from "@arco-design/web-react";
import { getCookie } from "cookies-next";
import { useApiErrorHandling } from "@/components/Others/useApiErrorHandling";
import { getAllSalutations, identitySelector } from "@/redux/identities";
import Image from "next/image";
import {
  getPartnerAdmin,
  partnerAdminSelector,
  updatePartnerAdmin,
} from "@/redux/partner-admins";
import { getMe, meSelector } from "@/redux/me";
import SettingsSkelton from "@/components/Skelton/settingSkelton";
import { getLabel } from "@/utils/lang-manager";
import { logout } from "@/redux/auth";
import CustomSelect from "@/components/Arco/CustomSelect";
const create = ({ noticetypes, idQuery, initialLanguage }: any) => {
  const [response, setResponse] = useState("");
  const { error: apiError } = useAppSelector(partnerAdminSelector);
  const router = useRouter();
  const dispatch = useAppDispatch();
  const handleUpdate = () => {
    setIsLoading(true);
    const updatedData = {
      first_name: firstName,
      last_name: lastName,
      email: email,
      password: password,
      password_confirmation: confirmPassword,
      state_id: status,
      salutation_id: salutationId,
    };
    dispatch(
      updatePartnerAdmin({
        active_partner: ACTIVE_PARTNER,
        partner_admin_uuid: meData?.uuid,
        updatedData,
      })
    ).then((response: any) => {
      if (response.payload.success == true) {
        loadStaticData();
        // logoutAction();
        dispatch(getMe());
        Notification.success({
          title: "Partner admin has been Updated Successfully",
          duration: 3000,
          content: undefined,
        });

        setResponse(response);
      } else {
        setIsLoading(false);
        (response.payload.code != 422) &&    Notification.error({
          title: "Partner admin update Failed",
          duration: 3000,
          content: undefined,
        });
      }
    });
  };
  const [meData, setMeData] = useState({}) as unknown as any;
  const { data: getMeData } = useAppSelector(meSelector);
  useEffect(() => {
    setMeData(getMeData);
  }, [getMeData]);
  console.log(meData);
  const handleSalutationChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedValue = e.target.value;
    selectedValue;
    setSalutationID(parseInt(selectedValue, 10));
  };
  const error = useApiErrorHandling(apiError, response);
  const loadStaticData = async () => {
    dispatch(getAllSalutations());
    dispatch(
      getPartnerAdmin({
        active_partner: ACTIVE_PARTNER,
        partner_admin_uuid: meData?.uuid,
      })
    ).then((response: any) => {
      setIsLoading(false);
    });
  };
  useEffect(() => {
    loadStaticData();
  }, [meData]);
  const { data: showSalutationsData } = useAppSelector(identitySelector);
  // const { data: showPartnerAdminData } = useAppSelector(partnerAdminSelector);
  const showPartnerAdminData = meData;
  const [firstName, setFirstName] = useState(showPartnerAdminData?.first_name);
  const [lastName, setLastName] = useState(showPartnerAdminData?.last_name);
  const [email, setEmail] = useState(showPartnerAdminData?.email);
  const [password, setpassword] = useState(showPartnerAdminData?.password);
  const [confirmPassword, setConfirmPassword] = useState(
    showPartnerAdminData?.password_confirmation
  );
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [status, setStatus] = useState(showPartnerAdminData?.state_id);
  const [salutationId, setSalutationID] = useState(
    showPartnerAdminData?.salutation?.id
  );
  const [salutationName, setSalutationName] = useState(
    showPartnerAdminData?.salutation?.name
  );
  useEffect(() => {
    setFirstName(showPartnerAdminData?.first_name);
    setLastName(showPartnerAdminData?.last_name);
    setEmail(showPartnerAdminData?.email);
    setpassword(showPartnerAdminData?.password);
    setConfirmPassword(showPartnerAdminData?.password_confirmation);
    setShowPassword(false);
    setShowConfirmPassword(false);
    setStatus(showPartnerAdminData?.state_id);
    setSalutationID(showPartnerAdminData?.salutation?.id);
    setSalutationName(showPartnerAdminData?.salutation?.name);
  }, [showPartnerAdminData]);
  console.log(showPartnerAdminData);

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };
  const toggleConfirmPasswordVisibility = () => {
    setShowConfirmPassword(!showConfirmPassword);
  };

  const logoutAction = () => {
    dispatch(logout());
  };
  return (
    <>
      {" "}
      {isLoading ? (
        <SettingsSkelton />
      ) : (
        <div>
          <div className="row">
            <div className="page-header">
              <h1 className="font-210 font-bold p-0 m-0">
                {getLabel("myAccountSetting", initialLanguage)}
              </h1>
              <p className="page-heading-sub text-muted">
                {getLabel("editYourPersonalInformation", initialLanguage)}
              </p>
            </div>
          </div>
          <hr className="page-heading-divider" />
          <div>
            <div className="row m-0 align-items-center">
              <p className="p-0 mb-4 mt-3 font-bold ">
                {getLabel("personalInformation", initialLanguage)}
              </p>
            </div>
            <div className="row align-items-center m-0 mb-4 align-items-center">
              <div className="col-md-3 ps-md-5 col-12 p-0 font-semibold ncr-maxwidth ">
                {getLabel("firstName", initialLanguage)}
                {/* <span className="text-danger">*</span>  */}
              </div>
              <div className="col-md-5 ms-0 col-12  p-0  row m-0">
                {/* <div className="col-md-3 col-3 p-0 pe-2 ">
                  <CustomSelect
                    data={showSalutationsData}
                    placeholderValue={getLabel("selectATitle",initialLanguage)}
                    onChange={(value: any) => setSalutationID(value)}
                    is_search={true}
                    defaultValue={ showPartnerAdminData?.salutation?.id}
                  />
                </div> */}
                <div className="col-md-12 col-12 p-0">
                  <input
                    type="text"
                    className="form-control border border-2 p-2 system-control "
                    // placeholder="Full Name in English"
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                    name="text"
                    autoComplete="new-text"
                  />
                  {error.first_name && (
                    <div className="text-danger">{error.first_name}</div>
                  )}
                </div>
                <div className="row align-items-center m-0 col-12 p-0 ">
                  <div className="col-md-4 col-3 p-0 pe-2 "></div>
                  {/* <div className="col-md-8 col-9 p-0">
                {error.first_name && (
                  <div className="text-danger">{error.first_name}</div>
                )}
              </div> */}
                </div>
              </div>
            </div>
            {/* <div className="row align-items-center m-0">
              <div className="col-md-3 ps-md-5 col-12 p-0 font-semibold ncr-maxwidth"></div>
              <div className="col-md-5 ms-0 col-12  p-0 mb-3 row m-0">
                <div className="col-md-4 col-3 p-0 pe-2 "></div>
                <div className="col-md-8 col-9 p-0"></div>
                <div className="row align-items-center m-0 col-12 p-0">
                  <div className="col-md-3 col-3 p-0 "></div>
                  <div className="col-md-9 col-9 p-0">
                    {error.first_name && (
                      <div className="text-danger">{error.first_name}</div>
                    )}
                  </div>
                </div>
              </div>
            </div> */}
            <div className="row m-0 mb-4 align-items-center">
              <div className="col-md-3 ps-md-5 col-12 p-0 font-semibold ncr-maxwidth">
                {getLabel("lastName", initialLanguage)}
                {/* <span className="text-danger">*</span> */}
              </div>
              <div className="col-md-5 ms-0 col-12  p-0">
                <input
                  type="text"
                  className="form-control border border-2 p-2 system-control "
                  // placeholder="Enter Last Name"
                  value={lastName}
                  onChange={(e) => setLastName(e.target.value)}
                  name="text"
                // autoComplete="new-text"
                />
                {error.last_name && (
                  <div className="text-danger">{error.last_name}</div>
                )}
              </div>
            </div>
            <div className="row m-0 mb-4 align-items-center">
              <div className="col-md-3 ps-md-5 col-12 p-0 font-semibold ncr-maxwidth">
                {getLabel("email", initialLanguage)}
                {/* <span className="text-danger">*</span> */}
              </div>
              <div className="col-md-5 ms-0 col-12  p-0">
                <input
                  type="text"
                  className="form-control border border-2 p-2 system-control "
                  // placeholder="Enter Email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  name="email"
                  autoComplete="new-email"
                />
                {error.email && (
                  <div className="text-danger">{error.email}</div>
                )}
              </div>
            </div>
          </div>
          <div className="">
            <div className="row m-0 align-items-center">
              <p className="p-0 mb-3 font-bold mt-3 ">
                {getLabel("securityInformation", initialLanguage)}
              </p>
            </div>
            <div className="row m-0 align-items-center  mb-4  ">
              <div className="col-md-3 ps-md-5 col-12 mb-0 p-0 font-semibold ncr-maxwidth">
                {getLabel("password", initialLanguage)}
                {/* <span className="text-danger"> *</span> */}
              </div>
              <div className="col-md-5 ms-0 col-12  p-0   position-relative  ">
                <input
                  type={showPassword ? "text" : "password"}
                  className="form-control border border-2 p-2 system-control"
                  // placeholder="Enter new password"
                  value={password}
                  onChange={(e) => setpassword(e.target.value)}
                  name="password"
                  autoComplete="new-password"
                />
                {password && (
                  <div className="">
                    <Image
                      onClick={togglePasswordVisibility}
                      src={showPassword ? "/openeye.svg" : "/closeeye.svg"}
                      alt={showPassword ? "eyeIcon icon" : "closeeye icon"}
                      width={16}
                      height={16}
                      className="pswd-group-eye"
                    />
                  </div>
                )}
                {error.password && (
                  <div className="text-danger">{error.password}</div>
                )}
              </div>
            </div>
            <div className="row m-0 mb-4 align-items-center">
              <div className="col-md-3 ps-md-5 col-12 p-0 font-semibold ncr-maxwidth">
                {getLabel("confirmPassword", initialLanguage)}
                {/* <span className="text-danger"> *</span> */}
              </div>
              <div className="col-md-5 ms-0 col-12  p-0 view-t position-relative">
                <input
                  type={showConfirmPassword ? "text" : "password"}
                  className="form-control border border-2 p-2 system-control"
                  // placeholder="Confirm new Password"
                  value={confirmPassword}
                  onChange={(e) => setConfirmPassword(e.target.value)}
                  name="password"
                  autoComplete="new-password"
                />
                {confirmPassword && (
                  <div className="">
                    <Image
                      onClick={toggleConfirmPasswordVisibility}
                      src={
                        showConfirmPassword ? "/openeye.svg" : "/closeeye.svg"
                      }
                      alt={
                        showConfirmPassword ? "eyeIcon icon" : "closeeye icon"
                      }
                      width={16}
                      height={16}
                      className="pswd-group-eye"
                    />
                  </div>
                )}
                {error.password_confirmation && (
                  <div className="text-danger">
                    {error.password_confirmation}
                  </div>
                )}
              </div>
            </div>
          </div>
          <div className="row m-0 mb-3">
            <div className="col-lg-3 col-12 d-none d-sm-flex ncr-maxwidth"></div>
            <div className="col-lg-8 col-12 p-0">
              <button
                type="button"
                className="btn btn-system-primary"
                onClick={handleUpdate}
              >
                {getLabel("confirm", initialLanguage)}
              </button>
            </div>
          </div>
        </div>
      )}
    </>
  );
};
export async function getServerSideProps({ req, res, query }: any) {
  const idQuery = query.id || "";
  const cookies = getCookie("token", { req, res });
  if (cookies == undefined) {
    res.writeHead(307, { Location: "/" });
    res.end();
  }
  const initialLanguage = getCookie('language', { req, res }) || "";
  return {
    props: {
      idQuery,
      initialLanguage
    },
  };
}
export default create;
