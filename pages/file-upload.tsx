import { useState, useEffect } from "react";
import { S3Client, GetObjectCommand } from "@aws-sdk/client-s3";

type Props = {
  bucketName: string;
  objectKey: string;
};

const S3Image = ({ bucketName, objectKey }: Props) => {
  const [imageUrl, setImageUrl] = useState("");
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const s3Client = new S3Client({
      region: "us-west-2", // replace with your bucket's region
      credentials: {
        accessKeyId: "AKIAUV6MFPRFBJSIDQTB", // replace with your AWS access key ID
        secretAccessKey: "FsJPZBCM1RK2FvW5m+s90L3w0iOoloJWYPfoFiqc", // replace with your AWS secret access key
      },
    });

    const getObjectCommand = new GetObjectCommand({
      Bucket: "direct-upload-s3-bucket-testing",
      Key: "first-image",
    });

    s3Client
      .send(getObjectCommand)
      .then((data) => {
        const blob = new Blob([data.Body as BlobPart], {
          type: data.ContentType,
        });
        const url = URL.createObjectURL(blob);
        setImageUrl(url);
        console.log("data->", url);
      })
      .catch((error) => console.error(error))
      .finally(() => setIsLoading(false));
  }, [bucketName, objectKey]);

  if (isLoading) {
    return <div>Loading...</div>;
  }

  return <img src={imageUrl ? imageUrl : ""} />;
};

export default S3Image;
