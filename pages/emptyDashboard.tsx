import React from 'react';

const EmptyDashboard = () => {
  return (
    <div>
      <div className="row justify-content-center align-items-center mx-auto vh-100">
        <div className="col-md-6 col-sm-7">
          <div className="row justify-content-center text-center">
            <div className="col-lg-1 col-md-2 col-2 mb-4"></div>
            <div>
              <h6 className="fw-normal">No notice added</h6>
              <div className="container">
                <div className="row justify-content-md-center">
                  <p className="col-lg-9 col-12 text-muted fw-light fs-6">
                    Use My Drafts to store your unfinished or private documents.
                    Only you can see documents that you keep in My Drafts —
                    unless you choose to share them.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EmptyDashboard;
