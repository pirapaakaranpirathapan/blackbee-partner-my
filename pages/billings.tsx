import React, { useState } from 'react';
import { parse } from 'cookie';
import { GetServerSidePropsContext } from 'next/types';
import { DatePicker } from '@arco-design/web-react';
import CustomDatePicker from '@/components/Accessories/CustomDatePicker';
import PickerDateSingle from '@/components/Accessories/PickerDateSingle';
const { YearPicker, MonthPicker } = DatePicker;


const Billings = () => {
  const [selectedDate, setSelectedDate] = useState<string>('');

  const handleDateChange = (newDate: string) => {
    setSelectedDate(newDate);
  };

  console.log("selectedDate", selectedDate);






  // const [dob, setDob] = useState("");
  // const [dod, setDod] = useState("");
  // function onSelect(dateString: any, date: any) {
  //   console.log('onSelect', dateString, date);
  // }

  // function onChange(dateString: any, date: any) {
  //   console.log('onChange: ', dateString, date);
  // }

  return (
    <div className='vh-100 h-100'>
      <div className='mb-5'>Billings</div>
      {/* //monthpicker */}
      {/* <div className="mb-5">
        <MonthPicker defaultValue='2019-06' onSelect={onSelect} onChange={onChange} />
      </div> */}


      {/* //yearpicker */}
      {/* <div className="mb-5">
        <YearPicker defaultValue='2019' onSelect={onSelect} onChange={onChange} />
      </div> */}

      <div className='row m-0 my-5 '>
        <div className=" col-7  p-0">
          <CustomDatePicker defaultDate="2023-10-19" onDateChange={handleDateChange} />
        </div>

      </div>
      <div className='row m-0 my-5 '>
        <div className=" col-12  p-0">
          <PickerDateSingle defaultDate="2023-10-19" onDateChange={handleDateChange} />
        </div>
      </div>
    </div>

  );
};

export async function getServerSideProps({ req, res, query }: GetServerSidePropsContext) {
  const cookies = parse(req.headers.cookie || '');
  const token = cookies.token;

  if (!token) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    };
  }

  return {
    props: {},
  };
}

export default Billings;


// const [day, setDay] = useState<string>('');
// const [month, setMonth] = useState<string>('');
// const [year, setYear] = useState<string>('');
// const [finalBirthDate, setFinalBirthDate] = useState<string>('');
// console.log("finalBirthDate", finalBirthDate);

// // const today = new Date().toISOString().split("T")[0];
// const today = new Date().toISOString().split("T")[0];


// // const handleDayChange = (day: string) => {
// //   setDay(day);
// //   updateFinalDate();
// // };
// const handleDayChange = (date: string) => {
//   const dateObj = new Date(date);
//   const newDay = dateObj.getDate().toString();
//   setDay(newDay);
//   updateFinalDate(newDay, month, year);
// };

// const handleMonthChange = (date: string) => {
//   const dateObj = new Date(date);
//   const newMonth = (dateObj.getMonth() + 1).toString(); // Month is zero-indexed, so add 1
//   setMonth(newMonth);
//   updateFinalDate(day, newMonth, year);
// };

// const handleYearChange = (date: string) => {
//   const dateObj = new Date(date);
//   const newYear = dateObj.getFullYear().toString();
//   setYear(newYear);
//   updateFinalDate(day, month, newYear);
// };

// const updateFinalDate = (newDay: string, newMonth: string, newYear: string) => {
//   const BirthDate = `${newYear}-${newMonth}-${newDay}`;
//   setFinalBirthDate(BirthDate);
// };

