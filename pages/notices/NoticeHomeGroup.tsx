import CustomerAndManager from '@/components/Notices/NoticeHome/CustomerAndManager'
import NoticeSettings from '@/components/Notices/NoticeHome/NoticeSettings'
import OrderDetails from '@/components/Notices/NoticeHome/OrderDetails'
import Image from 'next/image'
import React, { useState } from 'react'
import NoticeDetailsGroup from './NoticeDetailsGroup'
import groupprofile from '../../public/groupprofile.svg'
import ellipsissvg from '../../public/ellipsissvg.svg'

const NoticeHomeGroup = () => {
  const tab = 'Notice Details'
  const [activeTab, setActiveTab] = useState(tab)

  const changeTab = (val: any) => {
    setActiveTab(val)
  }
  return (
    <div className=" px-md-4 mx-1 my-2 bg-light">
      {/* ---------------Uma Navaratnasamy------ */}
      <div className=" my-4 row m-0 p-0 ">
        <div className="col-12 col-sm-6 col-md-4 col-lg-3 py-2 text-center d-flex flex-column justify-content-center align-items-center">
          <div className="group-add-container system-danger-light-1 rounded-circle">
            <Image
              src={groupprofile}
              alt="memorialprofile"
              className="image rounded-circle"
            />
          </div>
          <div className=" mt-1">
            <h3 className="m-0 p-0 py-1 font-80 font-bold">
              Uma Navaratnasamy
            </h3>
            <p className="m-0 p-0 font-80 font-normal pb-1">18.09.1995</p>
            <button
              type="button"
              className="btn btn-sm system-outline-primary p-1 px-2 bg-white font-normal mb-1">
              Manage
            </button>
          </div>
        </div>
        <div className="col-12 col-sm-6 col-md-4 col-lg-3 py-2 text-center d-flex flex-column justify-content-center align-items-center">
          <div className="group-add-container system-danger-light-1 rounded-circle">
            <Image
              src={groupprofile}
              alt="memorialprofile"
              className="image rounded-circle"
            />
          </div>
          <div className=" mt-1">
            <h3 className="m-0 p-0 py-1 font-80 font-bold">
              Uma Navaratnasamy
            </h3>
            <p className="m-0 p-0 font-80 font-normal pb-1">18.09.1995</p>
            <button
              type="button"
              className="btn btn-sm system-outline-primary p-1 px-2 bg-white font-normal mb-1">
              Manage
            </button>
          </div>
        </div>
        <div className="col-12 col-sm-6 col-md-4 col-lg-3 py-2 text-center d-flex flex-column justify-content-center align-items-center">
          <div className="group-add-container system-danger-light-1 rounded-circle">
            <Image
              src={groupprofile}
              alt="memorialprofile"
              className="image rounded-circle"
            />
          </div>
          <div className=" mt-1">
            <h3 className="m-0 p-0 py-1 font-80 font-bold">
              Uma Navaratnasamy
            </h3>
            <p className="m-0 p-0 font-80 font-normal pb-1">18.09.1995</p>
            <button
              type="button"
              className="btn btn-sm system-outline-primary p-1 px-2 bg-white font-normal mb-1">
              Manage
            </button>
          </div>
        </div>
        <div className="col-12 col-sm-6 col-md-4 col-lg-3 py-2 text-center d-flex flex-column justify-content-start align-items-center">
          <div className="group-add-container system-danger-light-1 rounded-circle d-flex align-items-center justify-content-center pointer">
            <div className="px-3 d-flex align-items-center justify-content-center fw-normal font-80 system-text-primary">
              Add more Pages
            </div>
          </div>
        </div>
      </div>
      {/* ---------------Uma Navaratnasamy------ */}
      <div className="bg-white border  p-2 rounded-top mb-2">
        <div className="row align-items-center m-0 p-0 ">
          <div className="col-md-3  col-12 p-0 mb-2 mb-md-0 ">
            <div className=" font-150 font-bold m-0 p-0">Obituary Notice</div>
            <div className="font-90 fw-normal system-icon-secondary ">
              1009282817172
            </div>
          </div>
          <div className="col-md-9 col-12 p-0 d-flex justify-content-md-end justify-content-start">
            <div className=" d-flex gap-1 gap-sm-3">
              <div className="  ">
                <button
                  type="button"
                  className="btn px-1 px-sm-4 btn-md border shadow-sm font-110 text-dark system-secondary-light">
                  Preview Notice
                </button>
              </div>
              <div className="  ">
                <button
                  type="button"
                  className="btn px-1 px-sm-4 btn-md border shadow-sm font-110 text-dark system-secondary-light ">
                  Complete Order
                </button>
              </div>
              <div className=" ">
                <button
                  type="button"
                  className="btn px-2 px-sm-4 btn-md border shadow-sm font-110 text-dark system-secondary-light">
                  <Image src={ellipsissvg} alt='' />
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* -----------------------notice sidebar--------------------- */}
      <div className="row rounded mx-md-1 my-3 " id="filter-panel">
        <div className="col-md-3 col-12 m-0 p-0  ">
          <div className=" p-3  inpage-sidebar">
            <div className="scrollmenu d-flex align-items-center d-md-block">
              <div className="">
                <a
                  href="#"
                  onClick={() => changeTab('Notice Details')}
                  className="">
                  <div
                    className={
                      activeTab === 'Notice Details'
                        ? `system-text-primary shadow-md sidelink active`
                        : `rounded shadow-md sidelink`
                    }>
                    Notice Details
                  </div>
                </a>
              </div>
              <div className="">
                <a
                  href="#"
                  onClick={() => changeTab('Notice Settings')}
                  className="">
                  <div
                    className={
                      activeTab === 'Notice Settings'
                        ? `system-text-primary shadow-md sidelink active`
                        : `rounded shadow-md sidelink `
                    }>
                    Notice Settings
                  </div>
                </a>
              </div>
              <div className="">
                <a
                  href="#"
                  onClick={() => changeTab('Order Details')}
                  className="">
                  <div
                    className={
                      activeTab === 'Order Details'
                        ? `system-text-primary shadow-md sidelink  active`
                        : `rounded shadow-md sidelink `
                    }>
                    Order Details
                  </div>
                </a>
              </div>
              <div className="">
                <a href="#" onClick={() => changeTab('Customer & Manager')}>
                  <div
                    className={
                      activeTab === 'Customer & Manager'
                        ? `system-text-primary shadow-md sidelink  active`
                        : `rounded shadow-md sidelink `
                    }>
                    Customer & Manager
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-9 col-12 data-body  ">
          <div className="row ">
            <div className="col-12 m-0 p-0">
              {activeTab === 'Notice Details' && (
                <>
                  <NoticeDetailsGroup />
                </>
              )}
              {activeTab === 'Notice Settings' && (
                <>
                  <NoticeSettings />
                </>
              )}
              {activeTab === 'Order Details' && (
                <>
                  <OrderDetails />
                </>
              )}
              {activeTab === 'Customer & Manager' && (
                <>
                  <CustomerAndManager />
                </>
              )}
            </div>
          </div>
        </div>
      </div>
      {/* -------------------------------------------- */}
    </div>
  )
}

export default NoticeHomeGroup
