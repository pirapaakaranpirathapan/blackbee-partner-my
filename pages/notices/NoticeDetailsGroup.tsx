import Image from "next/image";
import React, { useState } from "react";
import templates from "../../public/templates.svg";
import vjjnotice from "../../public/vjjnotice.svg";
import cam from "../../public/cam.svg";
import question from "../../public/question.svg";
import drag from "../../public/drag.svg";
import AddInformant from "../../components/Modal/AddInformant";
import FullScreenModal from "@/components/Elements/FullScreenModal";
import { Button } from "react-bootstrap";

const NoticeDetailsGroup = () => {
  const [showAddInformant, SetShowAddInformant] = useState(false);

  const defaultValue = "Tamil";
  return (
    <>
      <FullScreenModal
        show={showAddInformant}
        onClose={() => {
          SetShowAddInformant(false);
        }}
        title="Add a informant"
        description="This will be visible in the notice page"
        footer={
          <Button
            onClick={() => {
              alert("Add a informant working");
            }}
          >
            Submit
          </Button>
        }
        deleteLabel={
          <a
            onClick={() => {
              alert("Delete is working");
            }}
          >
            Delete
          </a>
        }
      >
        <AddInformant />
      </FullScreenModal>

      <div className="border bg-white px-4 py-3 mb-3 ">
        <div className="">
          <div className="mb-3  p-2 system-danger-light rounded">
            <p className="p-0 m-0 font-90 font-bold">
              Required information are missing!
            </p>
            <p className="p-0 m-0 mt-1 system-text-dark-80 font-90 font-normal ">
              Please update all of the required information before you activate
              page.
            </p>
          </div>
          <div className="row  align-items-center p-0 m-0">
            <div className="col-md-3 col-12  font-bold p-0 text-md-end pe-0 pe-md-4">
              Notice Page Language
            </div>
            <div className="col-md-4 col-12   ps-0 ps-md-2 mt-1 mt-sm-0 ">
              <select
                className="form-select border border-2 bg-white  system-control font-bold  p-2"
                id="dropdownMenuLink"
              >
                <option defaultValue={defaultValue}>Tamil </option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
              </select>
            </div>
            <div className="row mb-3 m-0 p-0">
              <div className="col-md-3  col-12 d-none d-sm-flex p-0 m-0 "></div>
              <div className="col-md-9  col-12 font-80 font-normal system-muted-5  ps-0 ps-md-2 mt-1 mt-sm-0">
                Outpur page elements will change based on this language
              </div>
            </div>
          </div>
          <div className="row  align-items-center p-0 m-0 ">
            <div className="col-md-3 col-12 p-0 text-md-end  font-bold pe-0 pe-md-4">
              Notice Title
              <div className="font-80 font-normal system-icon-secondary  p-0 text-md-end ">
                Optional
              </div>
            </div>
            <div className="col-md-9 col-12   ps-0 ps-md-2 mt-1 mt-sm-0 ">
              <input
                type="text"
                className="form-control border border-2 notice-input system-control "
                placeholder="Search or add a new"
              />
            </div>
          </div>
          <div className="row  m-0 p-0 mb-3">
            <div className="col-lg-3  col-12 d-none d-sm-flex ps-0 "></div>
            <div className="col-lg-9  col-12 font-bold   ps-0 ps-md-2 mt-1 mt-sm-0">
              <span className="font-90 font-semibold text-dark">
                Suggestion:{" "}
              </span>
              <span className="font-90 font-normal ms-2 me-3 system-text-brown ">
                மரண அறிவித்தல்
              </span>
              <span className="font-90 font-normal system-text-brown">
                அகால மரணம்
              </span>
            </div>
          </div>
          <div className="row  align-items-center p-0 m-0 mb-3">
            <div className="col-md-3 col-12 p-0 text-md-end  font-bold pe-0 pe-md-4">
              Template
            </div>
            <div className="col-md-9 col-12 m-0  ps-0 ps-md-2 mt-1 mt-sm-0 ">
              <button
                type="button"
                className="btn px-2 px-sm-4 border border-2  font-bold text-dark py-3"
              >
                Select Template
              </button>
              <span>
                <Image
                  src={templates}
                  alt="memorialprofile"
                  className="mx-3 img-fluid pointer"
                />{" "}
              </span>
              <span className="font-normal system-text-primary pointer">
                Change Template
              </span>
            </div>
          </div>
          <div className="row mb-4 m-0 p-0 ">
            <div className="col-lg-3  col-12 d-none d-sm-flex ps-0 "></div>
            <div className="col-lg-9  col-12  ps-0 ps-md-2 mt-1 mt-sm-0 ">
              <button
                type="button"
                className=" btn btn-md border system-primary text-white  px-4 mt-4 font-110 font-bold"
              >
                Save
              </button>
            </div>
          </div>
        </div>
        <hr className="m-0 p-0" />
        <div className="row m-0 p-0 align-items-center my-4">
          <div className="p-0 m-0 col-12 col-sm-6 d-flex justify-content-center justify-content-sm-start mb-2">
            <strong className="font-150 font-bold ">Notice Contents</strong>
          </div>
          <div className="col-12 col-sm-6  d-flex justify-content-center justify-content-sm-end p-0 m-0">
            <button type="button" className="btn system-success text-white">
              View Sample
            </button>
          </div>
        </div>

        <div className="system-warning-light mb-5 pb-3 rounded ">
          <div className="px-4 pb-4 ">
            <h6 className="m-0 p-0 font-bold system-text-brown  font-160 py-3 text-center text-sm-start mb-sm-0">
              மரண அறிவித்தல்
            </h6>
            <div className="">
              <div className="row m-0 p-0 mb-4  justify-content-center  text-center justify-content-sm-start  text-sm-start ">
                <div className="col-sm-4 col-12 p-0 me-sm-3 group-notice-image-container position-relative  ">
                  <Image src={vjjnotice} alt="img" className="image rounded" />
                  <Image
                    src={cam}
                    alt="img"
                    className="position-absolute end-0 top-0 m-1 pointer"
                  />
                </div>

                <div className=" col-sm-8 col-12 p-0  pt-4 pt-sm-0">
                  <p className="p-0 m-0 font-140 font-bold text-dark mb-2">
                    திரு பேரின்பநாதன் பொன்னையா
                  </p>
                  <p className="p-0 m-0 font-bold font-80 mb-2">
                    இளைப்பாறிய பொது சுகாதார பரிசோதகர்
                  </p>
                  <p className="p-0 m-0 font-80 fw-normal system-text-primary">
                    Add Nick Name
                  </p>
                </div>
              </div>
              <div className="row m-0 p-0 mb-4  justify-content-center  text-center justify-content-sm-start  text-sm-start">
                <div className="col-sm-4 col-12 p-0 me-sm-3 group-notice-image-container position-relative  ">
                  <Image src={vjjnotice} alt="img" className="image rounded" />
                  <Image
                    src={cam}
                    alt="img"
                    className="position-absolute end-0 top-0 m-1"
                  />
                </div>

                <div className=" col-sm-8 col-12 p-0  pt-4 pt-sm-0   ">
                  <p className="p-0 m-0 font-140 font-bold text-dark mb-2">
                    திரு பேரின்பநாதன் பொன்னையா
                  </p>
                  <p className="p-0 m-0 font-bold font-80 mb-2">
                    இளைப்பாறிய பொது சுகாதார பரிசோதகர்
                  </p>
                  <p className="p-0 m-0 font-80 fw-normal system-text-primary">
                    Add Nick Name
                  </p>
                </div>
              </div>
              <div className="row m-0 p-0   justify-content-center  text-center justify-content-sm-start  text-sm-start">
                <div className="col-sm-4 col-12 p-0 me-sm-3 group-notice-image-container position-relative  ">
                  <Image src={vjjnotice} alt="img" className="image rounded" />
                  <Image
                    src={cam}
                    alt="img"
                    className="position-absolute end-0 top-0 m-1 pointer"
                  />
                </div>

                <div className=" col-sm-8 col-12 p-0  pt-4 pt-sm-0  ">
                  <p className="p-0 m-0 font-140 font-bold text-dark mb-2">
                    திரு பேரின்பநாதன் பொன்னையா
                  </p>
                  <p className="p-0 m-0 font-bold font-80 mb-2">
                    இளைப்பாறிய பொது சுகாதார பரிசோதகர்
                  </p>
                  <p className="p-0 m-0 font-80 fw-normal system-text-primary">
                    Add Nick Name
                  </p>
                </div>
              </div>
            </div>
          </div>

          <hr className="mb-4 " />

          <div className="px-2 px-md-5 pb-3 ">
            <div className="d-flex align-items-center">
              <div className="p-0 m-0 me-1 font-bold">Informant Title</div>
              <Image src={question} alt="memorialprofile" className="" />
            </div>
            <div className="row p-0 m-0 ">
              <div className=" col-12 col-md-10 m-0 px-0  mt-2 ">
                <input
                  type="text"
                  className="form-control border border-2 p-2 system-control "
                />
              </div>
            </div>
            <div className="row m-0 p-0 ">
              <div className="col-12 fw-normal text-muted  px-0">
                <span className="fw-semibold text-muted-5 ">Suggestion:</span>
                <span className="font-70 ms-2 me-3 text-muted-5 ">
                  உங்கள் நினைவில் வாழும்
                </span>
                <span className="font-70 ps-2 text-muted-5 ">
                  என்றும் உங்கள் நினைவில் வாழும்
                </span>
              </div>
            </div>
          </div>
          <hr className="mb-4 " />
          <div className="px-2 px-md-5 mb-4">
            <div className="d-flex align-items-center mb-3">
              <div className="p-0 m-0 me-1 font-bold">Informant</div>
              <Image src={question} alt="memorialprofile" className="" />
            </div>

            <div className="row">
              <div className="col-12  ">
                <div className="row gap-2 p-0 m-0 ">
                  <div className="col-md-10 col-12 p-0 m-0 border border-2 px-2 p-1 bg-white d-flex  rounded">
                    <div>
                      {" "}
                      <Image src={drag} alt="drag" className="move" />
                    </div>
                    <div
                      className="text-muted ps-2"
                      style={{ maxWidth: "100%" }}
                    >
                      <p className="p-o m-0 font-bold text-truncate">
                        Saravanan
                      </p>
                      <p className="p-o m-0 font-80 font-normal font-80 ">
                        Teacher
                      </p>
                    </div>
                  </div>
                  <div className="system-text-primary m-0 p-0 font-80 font-normal">
                    Group by Sons
                  </div>
                  <div className="col-md-10 col-12 p-0 m-0 border border-2 px-2 p-1 bg-white d-flex rounded">
                    <div>
                      {" "}
                      <Image src={drag} alt="drag" className="move" />
                    </div>
                    <div
                      className="text-muted ps-2"
                      style={{ maxWidth: "100%" }}
                    >
                      <p className="p-o m-0 font-bold text-truncate">
                        Rajmohan
                      </p>
                      <p className="p-o m-0 font-80 font-normal">
                        Australia, MBBS
                      </p>
                    </div>
                  </div>
                  <div className="col-md-10 col-12 p-0 m-0 border border-2 px-2 p-1 bg-white d-flex  rounded">
                    <div>
                      {" "}
                      <Image src={drag} alt="drag" className="move" />
                    </div>
                    <div
                      className="text-muted ps-2"
                      style={{ maxWidth: "100%" }}
                    >
                      <p className="p-o m-0 font-bold text-truncate">
                        Kishonmaran
                      </p>
                      <p className="p-o m-0 font-80 font-normal ">
                        Former principal of Jaffna Hindu College, Sri Lanka
                      </p>
                    </div>
                  </div>
                  <div className="system-text-primary m-0 p-0 font-80 font-normal">
                    and
                  </div>
                  <div className="mb-2 col-md-10 col-12 p-0 m-0 border border-2 px-2 p-1 bg-white d-flex  rounded">
                    <div>
                      {" "}
                      <Image src={drag} alt="drag" className="move" />
                    </div>
                    <div className="text-muted ps-2">
                      <p className="p-o m-0 font-bold">Relatives</p>
                    </div>
                  </div>
                  <div className="mb-3 mb-sm-0 m-0 p-0">
                    <button
                      onClick={() => {
                        SetShowAddInformant(true);
                      }}
                      type="button"
                      className="btn btn-sm system-outline-primary p-1 px-2 bg-white  font-normal"
                    >
                      + Add
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr className="mb-5" />
      </div>
    </>
  );
};

export default NoticeDetailsGroup;
