import React, { useState, useEffect } from "react";
import Head from "next/head";
import Image from "next/image";
import { useRouter } from "next/router";
import { PaginationComponent } from "@/components/Elements/Pagination";
import Filter from "@/components/Filter/Filter";
import TableCardMemorial from "@/components/Cards/TableCardMemorial";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import { getAllNotices, noticeSelector } from "@/redux/notices";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";

import extractDate from "../../components/TimeConverter/dateExtracter";
import { integer } from "aws-sdk/clients/cloudfront";
import FullScreenModalOne from "@/components/Elements/FullScreenModalOne";
import NoticeTypeCreate from "@/components/Modal/NoticeTypeCreate";
import { getCookie } from "cookies-next";
import { fetchCommunityTypeData, fetchNoticeTypeData } from "@/utils/api";
import NoticeSkelton from "@/components/Skelton/NoticeSkelton";
import TableCardNotice from "@/components/Cards/TableCardNotice";
import TableCardSkelton from "@/components/Skelton/TableCardSkelton";
import { timeagoformatter } from "@/components/TimeConverter/timeAgoFormatter";
import { MonthDate } from "@/components/TimeConverter/MonthFormat";
import CustomImage from "@/components/Elements/CustomImage";
import { getLabel } from "@/utils/lang-manager";
import { parse } from "cookie";
import { GetServerSidePropsContext } from "next/types";
import MainPagination from "@/components/Paginations";
import { noticeTypeSelector } from "@/redux/notice-types";

const Notice = ({ noticetypes, noticeQuery, initialLanguage }: any) => {
  const [noticeCreate, setNoticeCreate] = useState(false);
  const router = useRouter();
  const dispatch = useAppDispatch();
  const { data: noticeData } = useAppSelector(noticeSelector);
  const { data: noticeTypeData } = useAppSelector(noticeTypeSelector);

  const [loading, setLoading] = useState(true);
  const loadData = (page: integer) => {
    setLoading(true);
    dispatch(
      getAllNotices({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        page: page,
      })
    ).then(() => {
      setLoading(false);
    });
  };
  const subtab = "Notice Details";
  // =============================================================================
  //This function is used to navigate to a specific notice view based on the provided UUID.
  function singleView(uuid: any, isSingle: any) {
    router.push({
      pathname: `/notices/${uuid}`,
      query: { g: isSingle ? "s" : "g", t: "home", s: subtab },
    });
  }
  //================================================================================
  //pagination
  const { query } = router;
  const [page, setPage] = useState(1);
  const [activeNotice, setActiveNotice] = useState(1);
  const handlePagination = (page: number) => {
    setPage(page);
    loadData(page);
    setActiveNotice(page);
    if (page !== null) {
      router.push(
        { pathname: router.pathname, query: { ...query, p: page } },
        undefined,
        { shallow: true }
      );
    }
  };
  useEffect(() => {
    setPage(noticeQuery);
    loadData(noticeQuery);
  }, [noticeQuery]);

  return (
    <>
      <Head>
        <title>Blackbee | Notices</title>
      </Head>
      <FullScreenModalOne
        show={noticeCreate}
        onClose={() => {
          setNoticeCreate(false);
        }}
        title={`${getLabel("chooseNoticeType", initialLanguage)}`}
        description=""
        // footer={<Button className="btn-system-primary">Save</Button>}
      >
        <NoticeTypeCreate noticetypesDatas={noticeTypeData} />
      </FullScreenModalOne>
      <div>
        <div className="d-flex justify-content-between align-items-center page-heading-container">
          <div className="page-header">
            <h1 className="page-heading">
              {getLabel("allNotices", initialLanguage)}
            </h1>
            <p className="page-heading-sub text-muted">
              {getLabel("allNoticesDescription", initialLanguage)}
            </p>
          </div>
          {!loading ? (
            <button
              type="button"
              className="btn btn-system-primary mt-n10 border-0"
              onClick={() => {
                setNoticeCreate(true);
              }}
              // onClick={() => router.push("notices/create")}
            >
              <strong>+ </strong>
              {getLabel("create", initialLanguage)}
            </button>
          ) : (
            <button
              type="button"
              className=" d-none d-sm-flex me-3 shimmer-button22 border-0 shimmer-effect"
            ></button>
          )}
        </div>
        <hr className="page-heading-divider" />
        {/* filter */}
        <div className="mb-4 mb-md-0">
          <Filter />
        </div>
        {/* Table */}
        {loading ? (
          <NoticeSkelton loopTimes={9} />
        ) : (
          <div className="d-none d-sm-block">
            {noticeData.data.length > 0 ? (
              <>
                <table className="table mt-3 ">
                  <tbody className=" ">
                    {noticeData?.data?.map((data: any) => {
                      return (
                        <tr
                          key={data.id}
                          className=" align-middle "
                          onClick={() => singleView(data.uuid, data.is_single)}
                        >
                          <td
                            className="col-5 col-lg-5 col-xl-3 ps-0"
                            style={{ maxWidth: "200px" }}
                          >
                            <div className="d-flex align-items-center gap-3 pe-3">
                              <div className="ms-md-3 ms-3 table-img text-muted font-140 system-text-dark-light d-flex align-items-center justify-content-center rounded-circle position-relative">
                                {data && data?.pages[0]?.photo_url ? (
                                  <>
                                    <CustomImage
                                      src={data?.pages[0]?.photo_url}
                                      alt=""
                                      className="image rounded-circle"
                                      width={400}
                                      height={400}
                                      divClass="table-img"
                                    />
                                  </>
                                ) : data?.pages.length > 0 ? (
                                  data?.pages[0].name[0]
                                ) : (
                                  ""
                                )}
                                {Array.isArray(data.pages) &&
                                data.pages.length > 1 ? (
                                  <div className="position-absolute translate-middle  no-circle d-flex align-items-center justify-content-center font-80 ">
                                    +{data.pages.length - 1}
                                  </div>
                                ) : (
                                  <></>
                                )}
                              </div>
                              <div style={{ maxWidth: "70%" }} className="pe-2">
                                <div>
                                  <p className="p-0 m-0 text-dark font-90  text-truncate">
                                    {Array.isArray(data.pages) &&
                                    data.pages.length > 1
                                      ? `${getLabel(
                                          "obituaryForGroupOfPeople",
                                          initialLanguage
                                        )}`
                                      : ""}
                                  </p>
                                </div>
                                <div className="font-110 font-semibold d-flex">
                                  <div className="text-truncate">
                                    {data?.pages.length > 0
                                      ? data?.pages[0].name
                                      : ""}
                                    {/* {data.pages[0]} */}
                                  </div>
                                  {Array.isArray(data.pages) &&
                                  data.pages.length > 1 ? (
                                    <>
                                      <span className="px-1">&</span>
                                      <span className="px-1">
                                        {data.pages.length - 1}
                                      </span>
                                      {data.pages.length - 1 > 1
                                        ? `${getLabel(
                                            "others",
                                            initialLanguage
                                          )}`
                                        : `${getLabel(
                                            "other",
                                            initialLanguage
                                          )}`}
                                    </>
                                  ) : (
                                    ""
                                  )}
                                </div>
                                <div className="text-dark font-90 font-thin text-truncate">
                                  {data
                                    ? timeagoformatter(data?.created_at)
                                    : ""}
                                </div>
                              </div>
                            </div>
                          </td>
                          <td
                            className="col-2 col-lg-2 col-xl-2 p-0"
                            style={{ maxWidth: "90px" }}
                          >
                            {data && (
                              <>
                                <div className="text-secondary font-80 d-flex align-items-center justify-content-start ">
                                  <p className="p-0 m-0">
                                    {getLabel("country", initialLanguage)}
                                  </p>
                                </div>
                                <div
                                  className="text-dark font-90 d-flex align-items-center  justify-content-start "
                                  style={{ maxWidth: "80%" }}
                                >
                                  <p className="text-truncate p-0 m-0">
                                    {data ? data.pages[0]?.country?.name : "-"}
                                  </p>
                                </div>
                              </>
                            )}
                          </td>
                          <td className="col-2 col-lg-2 col-xl-2 p-0 pe-sm-3">
                            {data?.pages && (
                              <>
                                <div className="text-secondary font-80 font-thin d-none d-sm-flex text-truncate">
                                  {getLabel("deathDate", initialLanguage)}
                                </div>
                                <div className="text-dark font-90 d-none d-sm-flex text-truncate">
                                  {data?.pages
                                    ? MonthDate(
                                        extractDate(data?.pages[0]?.dod)
                                      )
                                    : ""}
                                </div>
                              </>
                            )}
                          </td>
                          <td
                            className="col-2 col-lg-2 col-xl-4 p-0"
                            style={{ maxWidth: "60px" }}
                          >
                            {data?.state && (
                              <>
                                <div className="text-secondary font-80 font-thin">
                                  {getLabel("status", initialLanguage)}
                                </div>
                                <div className="">
                                  <p
                                    className="text-dark font-90 text-truncate p-0 m-0"
                                    style={{ maxWidth: "90%" }}
                                  >
                                    {data?.state.name == "Enabled" ? (
                                      <span className="text-success text-truncate ">
                                        {data?.state.name}
                                      </span>
                                    ) : (
                                      <span className="text-danger text-truncate">
                                        {data?.state.name}
                                      </span>
                                    )}
                                  </p>
                                </div>
                              </>
                            )}
                          </td>
                          <td className="col-1 col-lg-1 col-xl-1 p-0 ">
                            <div className="p-0 m-0 d-flex align-items-center justify-content-end">
                              <button
                                type="button"
                                className="btn btn-system-light me-3"
                              >
                                {getLabel("view", initialLanguage)}
                              </button>
                            </div>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </>
            ) : (
              <>
                <div className="w-100 d-flex align-items-center justify-content-center grey-600 py-4 .font-120">
                  No Notices found.
                </div>
              </>
            )}
          </div>
        )}
        <div className="d-block d-sm-none">
          {loading ? (
            <TableCardSkelton loopTimes={9} />
          ) : (
            noticeData.data?.map((item: any) => {
              return (
                <>
                  <TableCardNotice
                    data={item}
                    singleView={singleView}
                    initialLanguage={initialLanguage}
                  />
                </>
              );
            })
          )}
        </div>
        {/* -------------------pagination-------------------------- */}
        {loading
          ? ""
          : noticeData?.data?.length > 0 && (
              <>
                <hr />
                <div className="row align-items-center m-0 mt-4 mb-4 mb-md-0">
                  <div className="col-xl-3 col-12 p-0 font-110 text-center text-xl-start">
                    {getLabel("totalRecords", initialLanguage)}:
                    <span> {noticeData?.total}</span>{" "}
                  </div>
                  <div className="col-xl-9 col-12 p-0 d-flex justify-content-center justify-content-xl-end  mt-2 mt-lg-0 mb-4 mb-0">
                    {/* <PaginationComponent
                    page={page}
                    totalPages={Math.ceil(noticeData.total / 15)}
                    handlePagination={handlePagination}
                  /> */}
                    <MainPagination
                      page={parseInt(query?.p as string) || 1}
                      totalPages={noticeData.total}
                      handlePagination={handlePagination}
                    />
                  </div>
                </div>
              </>
            )}
      </div>
    </>
  );
};

export async function getServerSideProps({
  req,
  res,
  query,
}: GetServerSidePropsContext) {
  const noticeQuery = query && query.p !== undefined ? query?.p : 1;

  const cookies = parse(req.headers.cookie || "");
  const token = cookies.token;
  const initialLanguage = getCookie("language", { req, res });

  const noticetypes = await fetchNoticeTypeData(req, res);

  if (!token) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }

  return {
    props: {
      noticetypes,
      noticeQuery,
      initialLanguage,
    },
  };
}
export default Notice;
