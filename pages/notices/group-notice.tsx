
import Image from 'next/image'
import Link from 'next/link'
import React, { useState } from 'react'
import memorialprofile from '../../public/memorialprofile.png'
import { Tab, Tabs } from 'react-bootstrap'
import NoticeHome from '@/components/Notices/NoticeHome'
import NoticeAbout from '@/components/Notices/NoticeAbout'
import NoticeHomeGroup from './NoticeHomeGroup'
import ProfileMeta from '@/components/Profile/ProfileMeta'
import { getCookie } from 'cookies-next'

function GroupNotice() {
  const [activeKey, setActiveKey] = useState('home')

  return (
    <div>
      <div className="ow align-items-center mb-4">
        <div className="col-6">
          <h6 className="page-heading font-bold m-0">Notice Page</h6>
          <p className="m-0 text-muted font-80">1232233454657</p>
        </div>
      </div>
      <hr className="page-heading-divider" />
      {/* -----------------------profile components------------------ */}
      <div className="row m-0 bg-light profile-container">
        <div>
          <ProfileMeta />
        </div>

        {/* ----------------------memorial profile------------------------ */}
        <div className=" m-0 p-0">
          <div className=" py-3 px-4 system-text-dark-light">
            <h1 className="font-170 font-bold system-text-blue text-truncate">
              Uma & Palani’s page
            </h1>
            <p className=" font-normal p-0 system-text-blue m-0">
              ஆளி பேரலை சுனாமி அனர்த்தத்தில் எம்மை விட்டு பிரிந்த எங்கள் அண்ணா
              மற்றும் அக்கா{' '}
            </p>
          </div>
        </div>

        {/* ----------------------memorial Tabs------------------------ */}
        <div>
          <div className="row font-semibold">
            <Tabs
              activeKey={activeKey}
              onSelect={(k: any) => setActiveKey(k)}
              className="system-thin-black ps-md-2">
              <Tab title={'Home'} eventKey={'home'}>
                <NoticeHomeGroup />
              </Tab>
              <Tab title={'About'} eventKey={'about'}>
                notice about group
              </Tab>
              <Tab title={'|'}></Tab>
              <Tab
                title={'Notices & Other Services'}
                eventKey={'notices-and-services'}>
                <>Notices & Other Services</>
              </Tab>

              <Tab title={'More'} eventKey={'more'}>
                <>More</>
              </Tab>
            </Tabs>
          </div>
        </div>
        {/* ----------------------memorial add profile component------------------------ */}
      </div>
    </div>
  )
}
export async function getServerSideProps({ req, res, query }: any) {
  // const idQuery = query.id || "";
  // console.log("idQuery", idQuery);

  const cookies = getCookie('token', { req, res });
  if (cookies == undefined) {
    res.writeHead(307, { Location: "/" });
    res.end();
  }

  return {
    props: {
      // idQuery,
    },
  };
}

export default GroupNotice
