import React, { useEffect, useRef, useState } from "react";
import FullScreenModal from "@/components/Elements/FullScreenModal";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import { customerPageSelector, getAllCustomerforpage } from "@/redux/customer";
import { getAllInterfaceLanguages, languageSelector } from "@/redux/language";
import {
  createMemorialPage,
  getAllMemorialPages,
  memorialPageSelector,
} from "@/redux/memorial-pages";
import { getAllNoticeTypes, noticeTypeSelector } from "@/redux/notice-types";
import { noticeSelector, postNotice } from "@/redux/notices";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import { useRouter } from "next/router";
import { Button } from "react-bootstrap";
import CustomerDetailsModal, { ChildModalRef } from "../../components/Modal/createCustomerModal";
import MemorialCreate from "@/components/Modal/MemorialCreate";
import { Notification } from "@arco-design/web-react";
import { getAllRelationships, relationsSelector } from "@/redux/relationships";
import CreateNoticeSkelton from "@/components/Skelton/CreateNoticeSkelton";
import { useApiErrorHandling } from "@/components/Others/useApiErrorHandling";
import { getLabel } from "@/utils/lang-manager";
import { parse } from "cookie";
import { GetServerSidePropsContext } from "next/types";
import CustomSelect from "@/components/Arco/CustomSelect";
import ArcoSelectAndCreate from "@/components/Arco/ArcoSelectAndCreate";
import {
  LabeledValue,
  OptionInfo,
} from "@arco-design/web-react/es/Select/interface";
import ArcoSelectAndCreateSingle from "@/components/Arco/ArcoSelectAndCreateSingle";
import { integer } from "aws-sdk/clients/cloudfront";
import { getCookie } from "cookies-next";

const create = ({ idQuery, routerMemorialUuid, initialLanguage }: any) => {
  const [language, setLanguage] = useState(40);
  const [relationship, setRelationship] = useState(4);
  const [noticetype, setNoticetype] = useState<string>(idQuery);
  const [profileId, setProfileId] = useState<string[]>([]);
  const [deselectUuid, setDeselectUuid] = useState<
    string | number | LabeledValue
  >();
  const [customerId, setCustomerId] = useState<string>("");
  const [noticeTypeTitle, setNoticeTypeTitle] = useState<string>("");
  const [customer, setCustomer] = useState<string>("");
  const [customerErr, setCustomerErr] = useState<string>("");
  const [pageErr, setPageErr] = useState<string>("");
  console.log("pageErr", pageErr);

  const [ShowProfileBanner, setShowProfileBanner] = useState(false);
  const childModalRef = useRef<ChildModalRef>(null);
  const [memorialCreate, setMemorialCreate] = useState(false);
  const [customerDatas, setCustomerDatas] = useState<string[]>([]);
  const [response, setResponse] = useState("");
  const [memorialId, setMemorialId] = useState<string[]>([]);
  const [countryId, setCountryId] = useState("130");
  const [isLoading, setIsLoading] = useState(true);
  const [response1, setResponse1] = useState("");
  const [selectedValues, setSelectedValues] = useState<string[]>([]);
  const [defaultNames, setDefaultNames] = useState<string[]>([]);
  const [defaultCustomerNames, setDefaultCustomerNames] = useState<
    string | undefined
  >();
  const [addSearchValue, setAddSearchValue] = useState({
    inputValue: "",
    searchType: "",
  });
  const [addCusSearchValue, setAddCusSearchValue] = useState({
    inputValue: "",
    searchType: "",
  });
  const [newError, setNewError] = useState({});
  const [
    submitButtonAddDethPersonEnabled,
    setsubmitButtonAddDethPersonEnabled,
  ] = useState(false);
  const [submitButtonAddCustomerEnabled, setsubmitButtonAddCustomerEnabled] =
    useState(false);
  const [isSendingInvitation, setIsSendingInvitation] = useState(false);
  const dispatch = useAppDispatch();
  const { data: memorialData } = useAppSelector(memorialPageSelector);
  const memorialAllData = memorialData.data;
  const { data: showLanguagessData } = useAppSelector(languageSelector);
  const { data: allCustomerData } = useAppSelector(customerPageSelector);
  const { data: showRelationshipsData } = useAppSelector(relationsSelector);
  const { error: apiError } = useAppSelector(noticeSelector);
  const { data: noticeTypesData } = useAppSelector(noticeTypeSelector);
  const actionBtnRef = useRef<HTMLButtonElement>(null);
  const router = useRouter();
  const { c_uuid } = router.query;
  const CreateErr = useApiErrorHandling(apiError, response1);

  const loadData = async () => {
    try {
      dispatch(getAllMemorialPages({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        page: 1,
      }));
      dispatch(getAllInterfaceLanguages());
      dispatch(getAllNoticeTypes({
        active_partner: ACTIVE_PARTNER,
      }));
      dispatch(getAllCustomerforpage({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
      }));
      dispatch(getAllRelationships());
      await new Promise(resolve => setTimeout(resolve, 1000));
      setIsLoading(false);
    } catch (error) {
      console.error("Error loading data:", error);
      setIsLoading(false);
    }
  };

  useEffect(() => {
    loadData();
  }, []);


  useEffect(() => {
    if (noticeTypesData && noticeTypesData.length > 0 && noticetype) {
      const noticeTypeName = noticeTypesData.find(
        (type: any) => type.id.toString() === idQuery.toString()
      );
      if (noticeTypeName) {
        const noticetitlename = noticeTypeName?.name;
        setNoticeTypeTitle(noticetitlename || "");
      }
    }
  }, [noticeTypesData, idQuery]);

  useEffect(() => {
    if (memorialAllData) {
      const RouterId = Array.isArray(routerMemorialUuid)
        ? routerMemorialUuid[0]
        : routerMemorialUuid;
      const matchingData = memorialAllData.find(
        (data: { uuid: string }) => data.uuid === RouterId
      );
      setDefaultNames(
        matchingData && matchingData.uuid !== deselectUuid
          ? [matchingData.name]
          : []
      );
    }
    const routerIdArray = Array.isArray(routerMemorialUuid)
      ? routerMemorialUuid.filter((value) => value !== undefined)
      : [routerMemorialUuid].filter((value) => value !== undefined);
    const memorialIdArray = Array.isArray(memorialId)
      ? memorialId.filter((value) => value !== undefined)
      : [memorialId].filter((value) => value !== undefined);
    const profileIdWithoutEmptyStrings = profileId.filter(
      (value) => value !== ""
    );
    const filteredRouterIdArray = routerIdArray.filter(
      (value) => value !== deselectUuid
    );
    const filteredMemorialIdArray = memorialIdArray.filter(
      (value) => value !== deselectUuid
    );
    const uniqueProfileIds = new Set([
      ...profileIdWithoutEmptyStrings,
      ...filteredRouterIdArray,
      ...filteredMemorialIdArray,
      ...selectedValues,
    ]);
    const updatedProfileIdArray = Array.from(uniqueProfileIds).filter(
      (value) => value !== ""
    );
    setProfileId(Array.from(updatedProfileIdArray) as string[]);
  }, [
    routerMemorialUuid,
    memorialAllData,
    selectedValues,
    memorialId,
    deselectUuid,
  ]);

  useEffect(() => {
    if (c_uuid) {
      let cus =
        Array.isArray(allCustomerData) &&
        allCustomerData.find((x: any) => x.uuid.toString() == c_uuid);
      cus && setCustomerId(cus.id);
      setDefaultCustomerNames(cus?.first_name);
      if (cus === false) {
      } else {
        setCustomerDatas([cus]);
      }
    }
  }, [c_uuid, allCustomerData]);

  useEffect(() => {
    setCustomerDatas(allCustomerData);
  }, [allCustomerData])

  useEffect(() => {
    if (!memorialCreate) {
      setNewError({});
    }
  }, [memorialCreate]);

  useEffect(() => {
    setCustomerErr("");
    setPageErr("")
  }, []);

  useEffect(() => {
    if (ShowProfileBanner === true) {
      setCustomerId("");
    }
  }, [ShowProfileBanner]);

  useEffect(() => {
    const handleBeforeUnload = () => {
      localStorage.removeItem('memorialData');
    };

    window.addEventListener('beforeunload', handleBeforeUnload);

    return () => {
      window.removeEventListener('beforeunload', handleBeforeUnload);
    };
  }, []);

  const handleClick = async () => {
    setSubmitButtonDisabled(true);
    setsubmitButtonAddCustomerEnabled(true);
    if (childModalRef.current) {
      childModalRef.current.callModalFunction(handleChildResponse);
    }
    waiting();
  };

  const waiting = () => {
    setTimeout(() => {
      setSubmitButtonDisabled(false);
    }, 1000);
  };

  const handleChildResponse = (response: any) => {
    const customerResponseData = response;
    setCustomerDatas([customerResponseData]);
    setCustomerId(response?.id);
    setCustomer(response?.["first_name"]);
    setDefaultCustomerNames(response?.first_name);
    setShowProfileBanner(false);
  };

  const onCallCustomerContinue = (value: any) => {
    setCustomerId(value);
    setShowProfileBanner(false);
  };

  const selectedOptionName = showRelationshipsData.find(
    (option: any) => option.id === relationship
  )?.name;

  const handleCheckboxChange = (event: any) => {
    setIsSendingInvitation(event.target.checked);
  };

  const handleOptionChange = (selectedValues: string[]) => {
    const updatedSelectedValues = [...selectedValues];
    setSelectedValues(updatedSelectedValues);
  };

  const handleClear = (visible: any) => {
    if (visible) {
      setSelectedValues([]);
      setProfileId([]);
    }
  };

  const handleDeselect = (
    value: string | number | LabeledValue,
    option: OptionInfo
  ) => {
    setDeselectUuid(value);
    setProfileId((prevProfileId) =>
      prevProfileId.filter((uuid) => uuid !== value)
    );
    setSelectedValues((prevSelectedValues) =>
      prevSelectedValues.filter((uuid) => uuid !== value)
    );
  };

  const handleDeselectValue = (
    value: string | number | LabeledValue,
    option: OptionInfo
  ) => {
    const storedMemorialData = localStorage.getItem("memorialData");
    if (storedMemorialData) {
      let memorialDataArray = JSON.parse(storedMemorialData);
      memorialDataArray = memorialDataArray.filter(
        (item: { name: string }) => item.name !== value
      );
      localStorage.setItem("memorialData", JSON.stringify(memorialDataArray));
    }
    setDefaultNames((prevNames) => prevNames.filter((name) => name !== value));
  };

  const handleSelectCustomerChange = (selectedValues: string) => {
    const selectCustomerId = selectedValues;
    setCustomerId(selectCustomerId);
  };

  const checkErrors = (
    personName: string,
    dob: string,
    dod: string,
    pageTypeId: string
  ) => {
    const error = {
      name: ["The name field is required."],
      client_id: [
        "The client id must be an integer.",
        "The client id field is required.",
      ],
      dob: [
        "The dob does not match the format Y-m-d.",
        "The dob must be a date before or equal to today.",
      ],
      dod: [
        "The dod does not match the format Y-m-d.",
        "The dod must be a date before or equal to today.",
      ],
      pagetype_id: ["The page type id field is required."],
      message: "The page type id field is required. (and 6 more errors)",
    };
    const localError = {} as any;
    if (!personName) {
      localError.name = error.name;
    }
    if (!dob || dob === "-") {
      localError.dob = error.dob;
    }
    if (!dod) {
      localError.dod = error.dod;
    }
    if (!pageTypeId) {
      localError.pageTypeId = error.pagetype_id;
    }
    return localError;
  };

  const handleCreate = (
    personName: string,
    salutation: integer,
    countryId: string,
    pageTypeId: string,
    relationship: string,
    dod: string,
    dob: string,
    deathPlace: string
  ) => {
    setsubmitButtonAddDethPersonEnabled(false);
    const localError = checkErrors(personName, dob, dod, pageTypeId);
    setNewError(localError);
    if (Object.keys(localError).length === 0) {
      createPage(
        personName,
        salutation,
        countryId,
        pageTypeId,
        relationship,
        dod,
        dob,
        deathPlace
      );
      setNewError({});
    } else {
      setNewError(localError);
    }
  };

  const createPage = (
    personName: string,
    salutation: integer,
    countryId: string,
    pageTypeId: string,
    relationship: string,
    dod: string,
    dob: string,
    deathPlace: string
  ) => {
    const CreateData = {
      name: personName,
      page_type_id: pageTypeId,
      salutation_id: salutation,
      relationship_id: relationship,
      dod: dod,
      dob: dob,
      death_location: deathPlace,
      client_id: customerId,
      country_id: countryId,
    };
    const storedMemorialData = localStorage.getItem("memorialData");
    let memorialDataArray = [];
    if (storedMemorialData) {
      memorialDataArray = JSON.parse(storedMemorialData);
    }
    memorialDataArray.push(CreateData);
    try {
      const memorialDataString = JSON.stringify(memorialDataArray);
      localStorage.setItem("memorialData", memorialDataString);
      setsubmitButtonAddDethPersonEnabled(false);
      const updatedDefaultNames = [personName, ...defaultNames];
      setDefaultNames(updatedDefaultNames);
      setMemorialCreate(false);
      setNewError({});
    } catch (error) {
      console.error("Error while setting data in local storage:", error);
    }
  };

  const createMemorialsAndGetUUIDs = async (
    memorialDataArray: any,
    customerId: any
  ) => {
    const newMemorialUuids = [];
    for (const CreateData of memorialDataArray) {
      CreateData.client_id = customerId;
      const response: { payload: any } = await dispatch(
        createMemorialPage({
          activePartner: ACTIVE_PARTNER,
          activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
          CreateData: CreateData,
        })
      );
      if (response.payload.success === true) {
        const newMemorialUuid = response.payload?.data?.data?.uuid;
        newMemorialUuids.push(newMemorialUuid);
        Notification.success({
          title: "Memorial has been created successfully",
          duration: 3000,
          content: undefined,
          style: {
            zIndex: 1000,
          },
        });
        memorialDataArray = memorialDataArray.filter(
          (item: any) => item !== CreateData
        );
      } else {
        // Notification.error({
        //   title: "Faild to create Memorial",
        //   duration: 3000,
        //   content: undefined,
        // });
      }
    }
    return newMemorialUuids;
  };

  const handleUpdate = async () => {
    try {
      const storedMemorialData = localStorage.getItem("memorialData");

      if (storedMemorialData) {
        const memorialDataArray = JSON.parse(storedMemorialData);
        const newMemorialUuids = await createMemorialsAndGetUUIDs(
          memorialDataArray,
          customerId
        );
        const updatedProfileId = [...profileId, ...newMemorialUuids];

        if (!customerId && updatedProfileId.length === 0) {
          setCustomerErr("The client id field is required.");
          setPageErr("The pages field is required.");
          return;
        } else {
          setCustomerErr("");
          setPageErr("");
        }
        if (!customerId) {
          setCustomerErr("The client id field is required.");
          return;
        } else {
          setCustomerErr("");
        }

        if (updatedProfileId.length === 0) {
          setPageErr("The pages field is required.");
          return;
        } else {
          setPageErr("");
        }
        const createdData = {
          pages: updatedProfileId,
          language_id: language,
          relationship_id: relationship,
          client_id: customerId,
          notice_type_id: noticetype,
          country_id: countryId,
        };
        dispatch(
          postNotice({
            active_partner: ACTIVE_PARTNER,
            active_service_provider: ACTIVE_SERVICE_PROVIDER,
            createdData,
          })
        ).then((response: any) => {
          if (response.payload.success === true) {
            localStorage.removeItem("memorialData");
            const updateId = response.payload?.data?.data?.uuid;
            setResponse1(response);
            const isSingle = response.payload?.data?.data?.is_single
              ? "s"
              : "g";
            router.push(
              `/notices/${updateId}?g=${isSingle}&t=home&s=Notice+Details`
            );
            Notification.success({
              title: "Notice has been created successfully",
              duration: 3000,
              content: undefined,
            });
          } else {
            setIsLoading(false);
            // Notification.error({
            //   title: "Failed to create notice",
            //   duration: 3000,
            //   content: undefined,
            // });
          }
        });
      } else {
        if (!customerId && profileId.length === 0) {
          setCustomerErr("The client id field is required.");
          setPageErr("The pages field is required.");
          // Notification.error({
          //   title: "Failed to create notice",
          //   duration: 3000,
          //   content: undefined,
          // });
          return;
        } else {
          setCustomerErr("");
          setPageErr("");
        }
        if (profileId.length === 0) {
          setPageErr("The pages field is required.");
          // Notification.error({
          //   title: "Failed to create notice",
          //   duration: 3000,
          //   content: undefined,
          // });
          return;
        } else {
          setPageErr("");
        }
        if (!customerId) {
          setCustomerErr("The client id field is required.");
          // Notification.error({
          //   title: "Failed to create notice",
          //   duration: 3000,
          //   content: undefined,
          // });
          return;
        } else {
          setCustomerErr("");
        }
        const createdData = {
          pages: profileId,
          language_id: language,
          relationship_id: relationship,
          client_id: customerId,
          notice_type_id: noticetype,
          country_id: countryId,
        };
        dispatch(
          postNotice({
            active_partner: ACTIVE_PARTNER,
            active_service_provider: ACTIVE_SERVICE_PROVIDER,
            createdData,
          })
        ).then((response: any) => {
          if (response.payload.success == true) {
            const updateId = response.payload?.data?.data?.uuid;
            const isSingle = response.payload?.data?.data?.is_single ? "s" : "g";
            setResponse1(response);
            router.push(
              `/notices/${updateId}?g=${isSingle}&t=home&s=Notice+Details`
            );
            Notification.success({
              title: "Notice has been created successfully",
              duration: 3000,
              content: undefined,
            });
          } else {
            setIsLoading(false);
            // Notification.error({
            //   title: "Failed to create notice",
            //   duration: 3000,
            //   content: undefined,
            // });
          }
        });
      }
    } catch (error) {
      console.error("Error:", error);
    }
  };

  const updateAddSearchValue = (value: any) => {
    let searchType = "Name";
    setAddSearchValue({ inputValue: value, searchType: searchType });
  };

  const updateAddCusSearchValue = (value: any) => {
    let searchType = "Name";
    setAddCusSearchValue({ inputValue: value, searchType: searchType });
  };
  const [submitButtonDisabled, setSubmitButtonDisabled] = useState(false);
  return (
    <>
      {isLoading ? (
        <CreateNoticeSkelton />
      ) : (
        <div>
          <FullScreenModal
            show={ShowProfileBanner}
            onClose={() => {
              setShowProfileBanner(false);
            }}
            title={`${getLabel("addNewCustomer", initialLanguage)} `}
            description={`${getLabel(
              "addNewCustomerModalDescription",
              initialLanguage
            )}`}
            footer={
              <Button
                className="btn-system-primary pointer"
                onClick={handleClick}
                disabled={submitButtonDisabled}
              >
                {getLabel("create", initialLanguage)}
              </Button>
            }
            deleteLabel={
              <a
                onClick={() => {
                  setShowProfileBanner(false);
                }}
              >
                {getLabel("close", initialLanguage)}
              </a>
            }
          >
            <CustomerDetailsModal
              ref={childModalRef}
              onCallCustomerContinue={onCallCustomerContinue}
              addCusSearchValue={addCusSearchValue}
              setModalVisible={setShowProfileBanner}
            />
          </FullScreenModal>
          <FullScreenModal
            show={memorialCreate}
            onClose={() => {
              setMemorialCreate(false);
            }}
            title={`${getLabel("createMemorialHeading", initialLanguage)}`}
            cusSize={"custom-size-3"}
            description={`${getLabel(
              "createMemorialHeadingDescription",
              initialLanguage
            )}`}
            footer={
              <Button
                ref={actionBtnRef}
                className="btn-system-primary"
                disabled={submitButtonAddDethPersonEnabled}
              >
                {getLabel("create", initialLanguage)}
              </Button>
            }
          >
            <MemorialCreate
              response={response}
              actionBtnRef={actionBtnRef}
              handleCreate={handleCreate}
              addSearchValue={addSearchValue}
              errors={newError}
            />
          </FullScreenModal>
          <div className="row">
            <div className="page-header">
              <h1 className="font-210 font-bold p-0 m-0">
                {getLabel("createA", initialLanguage)} {noticeTypeTitle} {getLabel("notice", initialLanguage)}
              </h1>
              <p className="page-heading-sub text-muted">
                {getLabel("createNoticeDescription", initialLanguage)}
              </p>
            </div>
          </div>
          <hr className="page-heading-divider" />
          <div>
            <div className="row m-0 align-items-center">
              <p className="p-0 mb-4 mt-3 font-bold ">
                {getLabel("deathPersonDetails", initialLanguage)}
              </p>
            </div>
            <div className="row align-items-center m-0">
              <div className="col-md-3 ps-md-5 col-12 p-0 font-semibold ncr-maxwidth">
                {getLabel("deathPerson", initialLanguage)}
                <p className="m-0 p-0 system-icon-secondary font-80">
                  Optional
                </p>
              </div>
              <div className="col-xl-5 col-md-7 ms-0 col-12  p-0 ">
                <ArcoSelectAndCreate
                  data={memorialData}
                  placeholderValue={"Search or Create death person’s profile page(Multiple)"}
                  onOptionChange={handleOptionChange}
                  is_search={true}
                  setIsModalVisible={setMemorialCreate}
                  isVisible={memorialCreate}
                  updateAddSearchValue={updateAddSearchValue}
                  onClear={handleClear}
                  onDeselect={handleDeselect}
                  onDeselectValue={handleDeselectValue}
                  defaultValue={defaultNames}
                />
              </div>
            </div>
            <div className="row m-0 mb-3 align-items-center">
              <div className="col-md-3 ps-md-5 col-12 p-0 font-semibold ncr-maxwidth"></div>
              <div className="col-xl-5 col-md-7 ms-0 col-12  p-0">
                {pageErr && (
                  <div className="text-danger font-normal">
                    {pageErr}
                  </div>
                )}
                {/* {CreateErr && (
                  <div className="text-danger font-normal">
                    {CreateErr.pages}
                  </div>
                )} */}
                <p className="p-0 m-0 mb-1 system-muted-5 font-normal">
                  {getLabel("deathPersonInputDescription", initialLanguage)}
                  <span
                    className="system-text-primary pointer"
                    onClick={() => {
                      setMemorialCreate(true);
                    }}
                  >
                    {getLabel("createAsNewPage", initialLanguage)}
                  </span>
                </p>
              </div>
            </div>
            <div className="row m-0 align-items-center">
              <div className="col-md-3 ps-md-5 col-12 p-0 pe-1 font-semibold ncr-maxwidth">
                {getLabel("selectOutputLanguage", initialLanguage)}
              </div>
              <div className="col-xl-5 col-md-7 ms-0 col-12  p-0 ">
                <CustomSelect
                  data={showLanguagessData}
                  placeholderValue={getLabel("selectALanguage", initialLanguage)}
                  onChange={(value: any) => setLanguage(value)}
                  is_search={true}
                  defaultValue={language}
                />
              </div>
            </div>
            <div className="row m-0 align-items-center">
              <div className="col-md-3 ps-md-5 col-12 p-0 pe-1 font-semibold ncr-maxwidth"></div>
              <div className="col-xl-5 col-md-7 ms-0 col-12  p-0">
                <p className="p-0 m-0 mb-1 system-muted-5 font-normal">
                  {getLabel("langCreateNotice", initialLanguage)}
                </p>
              </div>
            </div>
          </div>
          <div>
            <div className="row m-0 mt-5 align-items-center">
              <p className="p-0 mb-4 font-bold">
                {getLabel("whoIsPaying", initialLanguage)}
              </p>
            </div>
            <div className="row m-0 align-items-center">
              <div className="col-md-3 ps-md-5 col-12 mb-0 p-0 font-semibold ncr-maxwidth">
                {getLabel("relationshipLabel", initialLanguage)}
              </div>
              <div className="col-xl-5 col-md-7 ms-0 col-12  p-0">
                <CustomSelect
                  data={showRelationshipsData}
                  placeholderValue={getLabel(
                    "selectRelationship",
                    initialLanguage
                  )}
                  onChange={(value: any) => setRelationship(value)}
                  is_search={true}
                  defaultValue={relationship}
                />
              </div>
            </div>
            <div className="row m-0 p-0 align-items-center mb-3">
              <div className="col-md-3 col-12 p-0 font-semibold ncr-maxwidth"></div>
              <div className="col-xl-5 col-md-7 ms-0 col-12  p-0">
                <p className="p-0 m-0 mb-1 system-muted-5 font-normal">
                  {getLabel(
                    "relationshipCreateNoticeInputDescription",
                    initialLanguage
                  )}
                  {selectedOptionName && (
                    <span className="font-bold ps-1">{`"${selectedOptionName}"`}</span>
                  )}
                </p>
              </div>
            </div>
            <div className="row m-0 align-items-center">
              <div className="col-md-3 ps-md-5 col-12 p-0 font-semibold ncr-maxwidth">
                {getLabel("customer", initialLanguage)}
              </div>

              <div className="col-xl-5 col-md-7 ms-0 col-12  p-0 view-t">
                <ArcoSelectAndCreateSingle
                  data={customerDatas}
                  placeholderValue={"Search with name, email or telephone  +9199902929202"}
                  onOptionChange={handleSelectCustomerChange}
                  is_search={true}
                  setIsModalVisible={setShowProfileBanner}
                  isVisible={ShowProfileBanner}
                  defaultValue={defaultCustomerNames}
                  updateAddSearchValue={updateAddCusSearchValue}
                />
                <p className="p-0 m-0 mb-1 text-danger font-normal ">
                  {customerErr}
                </p>
              </div>
              <div className="row m-0 p-0 align-items-center">
                <div className="col-md-3 col-12 p-0 font-semibold ncr-maxwidth"></div>
                <div className="col-xl-5 col-md-7 ms-0 col-12  p-0">
                  <div className="d-flex align-items-center mt-1">
                    <input
                      type="checkbox"
                      className="me-1 check-button"
                      checked={isSendingInvitation}
                      onChange={handleCheckboxChange}
                    />
                    <span>
                      {getLabel("createNoticeCheckboxLabel", initialLanguage)}
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div className="row m-0 mb-4 align-items-center">
              <div className="col-md-3 ps-md-5 col-12 p-0 font-semibold ncr-maxwidth"></div>
              <div className="col-xl-5 col-md-7 ms-0 col-12  p-0">
                {CreateErr && (
                  <div className="text-danger font-normal">
                    {CreateErr.client_id}
                  </div>
                )}
              </div>
            </div>
          </div>

          <div className="row m-0 mb-3">
            <div className="col-lg-3 col-md-3 col-12 d-none d-sm-flex ncr-maxwidth"></div>
            <div className="col-lg-5 col-md-5 col-12 p-0">
              <button
                type="button"
                className="btn btn-system-primary"
                onClick={handleUpdate}
              >
                {getLabel("continue", initialLanguage)}
              </button>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export async function getServerSideProps({
  req,
  res,
  query,
}: GetServerSidePropsContext) {
  const idQuery = query.id || "";
  const routerMemorialUuid = query.uuid || "";
  const noticeQuery =
    query && query.p !== undefined ? parseInt(query?.p[0]) : 1;
  const cookies = parse(req.headers.cookie || "");
  const token = cookies.token;
  const initialLanguage = getCookie("language", { req, res }) || "";
  if (!token) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }
  return {
    props: {
      idQuery,
      noticeQuery,
      routerMemorialUuid,
      initialLanguage,
    },
  };
}

export default create;
