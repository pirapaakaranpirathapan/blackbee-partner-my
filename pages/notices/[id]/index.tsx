import React, { useEffect, useState } from "react";
import { Tab, Tabs } from "react-bootstrap";
import NoticeHome from "@/components/Notices/NoticeHome";
import NoticeAbout from "@/components/Notices/NoticeAbout";
import ProfileMeta from "@/components/Profile/ProfileMeta";
import ProfileBanner from "@/components/Profile/ProfileBanner";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import { noticeSelector, showNotice } from "@/redux/notices";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import { getAllSalutations, identitySelector } from "@/redux/identities";
import { getAllInterfaceLanguages, languageSelector } from "@/redux/language";
import {
  getAllNoticeTemplates,
  templatesSelector,
} from "@/redux/notice-templates";
import { CountriesSelector } from "@/redux/countries";
import { getAllNoticeTypes, noticeTypeSelector } from "@/redux/notice-types";
import { NoticeDataProvider } from "./NoticeDataContext";
import { getCookie } from "cookies-next";
import { useRouter } from "next/router";
import GroupBanner from "@/components/Profile/GroupBanner";
import { getDeletedMedia, getMedia } from "@/redux/media";
import {
  birthDeathTitlesSelector,
  getAllBirthAndDeathTitles,
} from "@/redux/birth-death-titles";
import { getAllVisibilities, VisibilitiesSelector } from "@/redux/visibilities";
import { getAllCustomerforpage } from "@/redux/customer";
import { getLabel } from "@/utils/lang-manager";
import { parse } from "cookie";
import { GetServerSidePropsContext } from "next/types";
import { getAllMemorialPages } from "@/redux/memorial-pages";
import CustomAlert from "@/components/Alert";
import NoDataBanner from "@/components/Profile/NoDataBanner";
function ShowNotice({ idQuery, isSingle, initialLanguage }: any) {
  const [activeKey, setActiveKey] = useState("");
  // const [isGroup, setIsGroup] = useState(true);
  const [isLoading, setIsLoading] = useState(true);
  const [isERR, setERR] = useState(false);
  const router = useRouter();
  const { query } = router;
  const dispatch = useAppDispatch();

  //-----------Edit info Data----------------------------------------------------------------------------//
  const loadEditData = async () => {
    setIsLoading(true);
    await dispatch(
      showNotice({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_notice: idQuery as unknown as string,
      })
    ).then((response: any) => {
      if (response?.payload == "Too Many Attempts.") {
        setIsLoading(true);
        setERR(true);
      } else if (response?.error?.message == "Rejected") {
        router.push("/404");
      } else {
        setIsLoading(false);
      }
    });
  };
  //-----------Arrays of Salutation,Interface Languages,Notice Templates,Countries------------------------//
  const loadStaticData = async () => {
    dispatch(getAllNoticeTypes({ active_partner: ACTIVE_PARTNER }));
    dispatch(getAllSalutations());
    dispatch(getAllInterfaceLanguages());
    dispatch(getAllNoticeTemplates({ active_partner: ACTIVE_PARTNER }));
    dispatch(getAllBirthAndDeathTitles());
    dispatch(getAllVisibilities());
    dispatch(
      getAllCustomerforpage({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
      })
    );
    dispatch(
      getAllMemorialPages({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        page: 1,
      })
    );
  };
  const { dataOne: showtNoticeData } = useAppSelector(noticeSelector);
  const { data: showtNoticeTypesData } = useAppSelector(noticeTypeSelector);
  const { data: showSalutationsData } = useAppSelector(identitySelector);
  const { data: showLanguagessData } = useAppSelector(languageSelector);
  const { data: showTemplatessData } = useAppSelector(templatesSelector);
  const { data: showCountriesData } = useAppSelector(CountriesSelector);
  const { data: birthDeathTitlesData } = useAppSelector(
    birthDeathTitlesSelector
  );
  // console.log("showtNoticeData", showtNoticeData?.pages[0]?.page_type?.name);

  // useEffect(() => {
  //   if (isERR && !showtNoticeData?.id) {
  //     setIsLoading(true);
  //     router.push("/404");
  //   }
  // });
  const { data: VisibilitiesData } = useAppSelector(VisibilitiesSelector);
  useEffect(() => {
    const handleSetActiveKey = () => {
      if (Array.isArray(query?.t)) {
        setActiveKey(query?.t[0] || "home");
      } else {
        setActiveKey(query?.t || "home");
      }
    };
    // Call the function when the component mounts or when the query changes.
    handleSetActiveKey();
  }, [query?.t]);
  // useEffect(() => {
  //   const handleSetActiveKey = () => {
  //     if (Array.isArray(query?.g)) {
  //       setIsGroup(query?.g[0] == "g" ? true : false);
  //     } else {
  //       setIsGroup(query?.g == "g" ? true : false);
  //     }
  //   };
  //   // Call the function when the component mounts or when the query changes.
  //   handleSetActiveKey();
  // }, [query?.g]);
  useEffect(() => {
    // setIsGroup(!showtNoticeData?.is_single)
  }, [showtNoticeData]);
  //---To reload the loadEditData from EditInfo----------------------------------------------------------------//
  const handleSave = () => {
    idQuery && loadEditData();
  };
  //------------------All datas need to be send to components -----------------------------------------------//
  const NoticeData = {
    uuid: idQuery,
    active_page: showtNoticeData?.pages?.[0]?.uuid,
    id: showtNoticeData?.id || "-",
    Status: showtNoticeData?.state?.["name"] || "-",
    Page_for: showtNoticeData?.is_single
      ? showtNoticeData?.pages[0]?.page_type?.name || "-"
      : "A group of people",
    // Page_for: showtNoticeData?.page_type?.name || "A group of people",
    // Page_for: showtNoticeData?.is_single === true ? showtNoticeData?.pages[0]?.page_type?.name ? showtNoticeData?.pages[0]?.page_type?.name : "-" : "A group of people",
    Country: showtNoticeData?.country?.name || "-",
    Country_id: showtNoticeData?.pages?.[0]?.country?.id,
    visibility_name: showtNoticeData?.visibility?.name || "-",
    visibility_id: showtNoticeData?.visibility?.id,
    visibilities: VisibilitiesData,
    // page_manager: showtNoticeData?.owner?.["first_name"] + " " + showtNoticeData?.owner?.["last_name"],
    photo: showtNoticeData?.profile_photo_url,
    photoUrl: showtNoticeData?.pages?.[0]?.photo_url,
    abbreviation: showtNoticeData?.pages?.[0]?.salutation?.name,
    abbreviation_id: showtNoticeData?.pages?.[0]?.salutation?.id || "1",
    full_name: showtNoticeData?.pages?.[0]?.name,
    full_lang_name: showtNoticeData?.pages?.[0]?.["lang_name"],
    nick_name: showtNoticeData?.pages?.[0]?.nickname,
    dob: showtNoticeData?.pages?.[0]?.dob,
    dod: showtNoticeData?.pages?.[0]?.dod,
    birth_location: showtNoticeData?.pages?.[0]?.birth_location || "",
    death_location: showtNoticeData?.pages?.[0]?.death_location,
    family_address: showtNoticeData?.custom_family_address,
    notice_title: showtNoticeData?.notice_title?.name || "",
    notice_title_id: showtNoticeData?.["notice_title"]?.id,
    salutations: showSalutationsData,
    languages: showLanguagessData,
    templates: showTemplatessData,
    countries: showCountriesData,
    noticetitles: showtNoticeTypesData,
    language_id: showtNoticeData?.language?.id,
    notice_template: showtNoticeData?.["notice_template"]?.["thumb_url"],
    notice_template_alt: showtNoticeData?.["notice_template"]?.name,
    notice_template_id: showtNoticeData?.["notice_template"]?.id,
    clipart_url: showtNoticeData?.["clip_art"]?.["thumb_url"],
    frame_url: showtNoticeData?.frame?.["thumb_url"],
    border_url: showtNoticeData?.border?.["thumb_url"],
    template_url: showtNoticeData?.["page_template"]?.["thumb_url"],
    clipart_id: showtNoticeData?.["clip_art"]?.id,
    frame_id: showtNoticeData?.frame?.id,
    border_id: showtNoticeData?.border?.id,
    template_id: showtNoticeData?.["page_template"]?.id,
    notice_template_uuid: showtNoticeData?.["notice_template"]?.uuid,
    is_single: showtNoticeData?.is_single,
    pages: showtNoticeData?.pages,
    loading: !isLoading || showtNoticeData?.id,
    created_at: showtNoticeData?.["created_at"],
    last_modified_at: showtNoticeData?.["updated_at"],
    created_by:
      showtNoticeData?.owner?.["first_name"] +
      " " +
      showtNoticeData?.owner?.["last_name"],
    page_manager: showtNoticeData?.client?.["first_name"]
      ? showtNoticeData?.client?.["first_name"] +
      " " +
      showtNoticeData?.client?.["last_name"]
      : "-",
    client_country: showtNoticeData?.client?.country?.name,
    // page_manager:
    //   showtNoticeData?.owner?.["first_name"] +
    //   " " +
    //   showtNoticeData?.owner?.["last_name"],
    PageBackground_id: showtNoticeData?.["page_background"]?.id,
    birthDeathTitles: birthDeathTitlesData,
    birthDeathTitle_id: showtNoticeData?.["birth_and_death_title"]?.id,
    background_music_url: showtNoticeData?.["background_music_url"],
    family_location: showtNoticeData?.family_address,
    custom_family_location: showtNoticeData?.custom_family_address,
    hide_at: showtNoticeData?.hide_at,
    hide_dob_and_dod: showtNoticeData?.hide_dob_and_dod,
    auto_hide: showtNoticeData?.auto_hide,
    live_video: showtNoticeData?.live_video_url,
    video_url: showtNoticeData?.background_video_url,
    client: showtNoticeData?.client,
    clipart_alt: showtNoticeData?.["clip_art"]?.name,
    frame_alt: showtNoticeData?.frame?.name,
    border_alt: showtNoticeData?.border?.name,
    template_alt: showtNoticeData?.["page_template"]?.name,
    PageBackground_alt: showtNoticeData?.["page_background"]?.name,
    PageBackground_url: showtNoticeData?.["page_background_url"],
    notice_type_name: showtNoticeData?.["notice_type"]?.title,
    initialLanguage: initialLanguage,
  };
  // console.log("dddd===>", showtNoticeData?.["birth_and_death_title"]?.id);
  const momorialId = NoticeData.pages?.[0]?.uuid;
  //background_music_url,custom_family_address,family_address,hide_at,hide_dob_and_dod,visibility live_video_url  background_video_url
  const getAllMedia = () => {
    dispatch(
      getMedia({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_notice: NoticeData.pages?.[0]?.uuid,
        page: 1,
      })
    );
  };
  const getDeletedMediaData = () => {
    dispatch(
      getDeletedMedia({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_notice: NoticeData.pages?.[0]?.uuid,
        page: 1,
      })
    );
  };
  useEffect(() => {
    if (NoticeData.pages?.[0]?.uuid) {
      getAllMedia();
      getDeletedMediaData();
    }

  }, [momorialId]);
  //-----------------------------------------Notice page query setups--------------------------------------------------//
  const subtab = "Notice Details";
  const handleTabSelect = (key: string) => {
    setActiveKey(key);
    if (key !== null) {
      if (key === "home") {
        router.push(
          { pathname: router.pathname, query: { ...query, t: key, s: subtab } },
          undefined,
          { shallow: true }
        );
      } else {
        const { s, ...updatedQuery } = query;
        updatedQuery.t = key;
        router.push(
          { pathname: router.pathname, query: updatedQuery },
          undefined,
          { shallow: true }
        );
      }
    }
  };
  useEffect(() => {
    idQuery && loadStaticData();
    idQuery && loadEditData();
  }, [idQuery]);
  return (
    <NoticeDataProvider handleSave={handleSave} NoticeData={NoticeData}>
      {isERR ? (
        <>
          <div className={`custom-alertwrong  rounded active-link m-2`}>
            <div className="">
              <div className="custom-alert-title ">
                Something went wrong ..
              </div>
            </div>
          </div>
        </>
      ) : (
        <div>
          {!isLoading ? (
            <>
              <div className="row align-items-center mb-4">
                <div className="col-10">
                  <h6 className="page-heading font-bold m-0">
                    {getLabel("noticePage", initialLanguage)}
                  </h6>
                  <p className="m-0 text-muted font-80">{idQuery}</p>
                </div>
              </div>
            </>
          ) : (
            <>
              <div className="row align-items-center mb-4">
                <div>
                  <div className="col-2 ">
                    <h6 className="page-heading font-bold m-0 shimmer-text mb-1"></h6>
                  </div>
                  <div className="col-3 ">
                    <p className="m-0 text-muted font-80 shimmer-text"></p>
                  </div>
                </div>
              </div>
            </>
          )}
          <hr className="page-heading-divider" />
          <div className="row m-0 bg-light profile-container ">
            <div>
              <ProfileMeta />
            </div>
            <div>
              {/* {isGroup ? <GroupBanner /> : <ProfileBanner />} */}
              {showtNoticeData?.is_single === false ? (
                <GroupBanner />
              ) : (
                <ProfileBanner />
              )}
              {/* <NoDataBanner /> */}
            </div>
            <div>
              <div className="row">
                <Tabs
                  activeKey={activeKey}
                  onSelect={(k: any) => handleTabSelect(k)}
                  className="system-thin-black ps-md-4"
                >
                  <Tab
                    title={`${getLabel("home", initialLanguage)}`}
                    eventKey={"home"}
                    className="nav-link-swapped"
                  >
                    <NoticeHome />
                  </Tab>
                  <Tab
                    title={`${getLabel("about", initialLanguage)}`}
                    eventKey={"about"}
                    className="nav-link-swapped"
                  >
                    <NoticeAbout />
                  </Tab>
                  <Tab title={"|"} id="vertical-divider"></Tab>

                  <Tab
                    title={"Notices & Other Services"}
                    eventKey={"notices-and-services"}
                    className="nav-link-swapped"
                  >
                    <>Notices & Other Services</>
                  </Tab>
                  <Tab
                    title={"Tributes"}
                    eventKey={"tributes"}
                    className="nav-link-swapped"
                  >
                    <>Tributes</>
                  </Tab>
                  <Tab
                    title={"Audiences"}
                    eventKey={"audiences"}
                    className="nav-link-swapped"
                  >
                    <>Audiences</>
                  </Tab>
                  <Tab
                    title={"More"}
                    eventKey={"more"}
                    className="nav-link-swapped"
                  >
                    <>More</>
                  </Tab>
                </Tabs>
              </div>
            </div>
          </div>
        </div>
      )}
    </NoticeDataProvider>
  );
}
// export async function getServerSideProps({ req, res, query }: any) {
//   const idQuery = query.id || "";

//   const cookies = getCookie("token", { req, res });
//   if (cookies == undefined) {
//     res.writeHead(307, { Location: "/" });
//     res.end();
//   }
//   return {
//     props: {
//       idQuery,
//     },
//   };
// }
export async function getServerSideProps({
  req,
  res,
  query,
}: GetServerSidePropsContext) {
  const idQuery = query.id || "";
  const isSingle = query.g || false;
  const cookies = parse(req.headers.cookie || "");
  const token = cookies.token;
  const initialLanguage = getCookie("language", { req, res }) || "";
  if (!token) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }
  return {
    props: {
      idQuery,
      isSingle,
      initialLanguage,
    },
  };
}
export default ShowNotice;
