import React, { createContext, ReactNode, useContext } from 'react';
interface NoticeDataContextProps {
  handleSave: () => void;
  NoticeData: any; 
  children?: ReactNode;
}
const defaultValue: NoticeDataContextProps = {
  handleSave: () => {},
  NoticeData: null,
};
export const NoticeDataContext = createContext<NoticeDataContextProps>(defaultValue);

export const NoticeDataProvider: React.FC<NoticeDataContextProps> = ({ handleSave, NoticeData, children }) => {
  return (
    <NoticeDataContext.Provider value={{ handleSave, NoticeData }}>
      {children}
    </NoticeDataContext.Provider>
  );
};
export default NoticeDataProvider;