import React, { useEffect, useRef } from "react";
import { useRouter } from "next/router";
import { useState } from "react";
import Filter from "@/components/Filter/Filter";
import TableCardMemorial from "@/components/Cards/TableCardMemorial";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import {
  createMemorialPage,
  getAllMemorialPages,
  memorialPageSelector,
} from "@/redux/memorial-pages";
import Head from "next/head";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import FullScreenModal from "@/components/Elements/FullScreenModal";
import { Button } from "react-bootstrap";
import { getCookie } from "cookies-next";
import { integer } from "aws-sdk/clients/cloudfront";
import MemorialSkelton from "@/components/Skelton/MemorialSkelton";
import { Notification } from "@arco-design/web-react";
import Link from "next/link";
import TableCardSkelton from "@/components/Skelton/TableCardSkelton";
import { timeagoformatter } from "@/components/TimeConverter/timeAgoFormatter";
import { MonthDate } from "@/components/TimeConverter/MonthFormat";
import CustomImage from "@/components/Elements/CustomImage";
import { getLabel } from "@/utils/lang-manager";
import { parse } from "cookie";
import { GetServerSidePropsContext } from "next/types";
import MainPagination from "@/components/Paginations";
import MemorialCreatePage from "@/components/Modal/MemorialCreatePage";

const Memorial = ({ pageQuery, initialLanguage }: any) => {
  const router = useRouter();
  const [page, setPage] = useState(1);
  const [memorialCreate, setMemorialCreate] = useState(false);
  const [response, setResponse] = useState("");
  const [addSearchValue, setAddSearchValue] = useState("");
  const actionBtnRef = useRef<HTMLButtonElement>(null);
  const handleCreate = (
    personName: string,
    // salutationId: string,
    salutation: integer,
    clientId: string,
    countryId: string,
    pageTypeId: string,
    relationship: string,
    dod: string,
    dob: string,
    deathPlace: string
  ) => {
    createPage(
      personName,
      // salutationId,
      salutation,
      clientId,
      countryId,
      pageTypeId,
      relationship,
      dod,
      dob,
      deathPlace
    );
  };
  const createPage = (
    personName: any,
    // salutationId: any,
    salutation: integer,
    clientId: any,
    countryId: any,
    pageTypeId: string,
    relationship: any,
    dod: any,
    dob: any,
    deathPlace: any
  ) => {
    setAddDeathPersonButtonEnabled(true);
    const CreateData = {
      name: personName,
      page_type_id: pageTypeId,
      salutation_id: salutation,
      relationship_id: relationship,
      dod: dod,
      dob: dob,
      death_location: deathPlace,
      client_id: clientId,
      country_id: countryId,
    };

    dispatch(
      createMemorialPage({
        activePartner: ACTIVE_PARTNER,
        activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
        CreateData,
      })
    ).then((response: any) => {
      if (response.payload.success == true) {
        setResponse(response);
        loadData(page);
        setAddDeathPersonButtonEnabled(false);
        Notification.success({
          title: "Memorial has been created successfully",
          duration: 3000,
          content: undefined,
        });
        // setMemorialCreate(false);
        const createId = response.payload?.data?.data?.uuid;
        router.push(`/memorials/${createId}`);
      } else {
        setAddDeathPersonButtonEnabled(false);
        response.payload.code != 422 &&
          Notification.error({
            title: "Faild to create Memorial",
            duration: 3000,
            content: undefined,
          });
      }
    });
  };
  // =============================================================================
  //This code sets up a Redux workflow to fetch all memorials data
  const dispatch = useAppDispatch();
  const [loading, setLoading] = useState(true);
  const loadData = (page: integer) => {
    setLoading(true);
    dispatch(
      getAllMemorialPages({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        page: page,
      })
    ).then(() => {
      setLoading(false);
    });
  };
  // useEffect(() => {
  //   loadData(pageQuery);
  // }, []);
  // const childTab = "personal-information";
  const { data: memorialData } = useAppSelector(memorialPageSelector);
  // =============================================================================
  //This function is used to navigate to a specific memorial view based on the provided UUID.
  function singleView(uuid: any) {
    // router.push({pathname:`memorials/${uuid}`, query: { t: "home" ,photo_url:url}})
    router.push({ pathname: `memorials/${uuid}`, query: { t: "home" } });
  }
  const imgRef = useRef<HTMLImageElement>(null);
  const aspectRatio = 1;
  const [activePage, setActivePage] = useState(1);
  const { query } = router;
  useEffect(() => {
    const img = imgRef.current;
    if (img) {
      img.style.width = "100%";
      img.style.height = "auto";
      img.style.paddingBottom = `${100 / aspectRatio}%`;
    }
  }, [query]);
  //---To set Pages and set queries----------------------------------------------------------------//
  const handlePagination = (page: number) => {
    // const currentPage = parseInt(query.p) || 1;
    setPage(page);
    loadData(page);
    setActivePage(page);
    if (page !== null) {
      router.push(
        { pathname: router.pathname, query: { ...query, p: page } },
        undefined,
        { shallow: true }
      );
    }
  };
  // const pageNo = query && query.p !== undefined ? query?.p : 1;
  // useEffect(() => {
  //   // if (query && Array.isArray(query?.p) && query.p !== undefined) {
  //   //   setActivePage(4);
  //   //   setActivePage(parseInt(query.p[0]));
  //   //   console.log(activePage);
  //   // }
  //   // setPage(pageQuery);
  //   // loadData(pageQuery);
  //   // setActivePage(pageQuery);
  //   handlePagination(pageNo);
  //   loadData(pageNo);
  // }, []);
  useEffect(() => {
    setPage(pageQuery);
    loadData(pageQuery);
  }, [pageQuery]);
  const [addDeathPersonButtonEnabled, setAddDeathPersonButtonEnabled] =
    useState(false);

  return (
    <>
      <Head>
        <title>Blackbee | Memorials</title>
      </Head>
      <FullScreenModal
        show={memorialCreate}
        // size="lg"
        onClose={() => {
          setMemorialCreate(false);
        }}
        cusSize={"custom-size-3"}
        title={`${getLabel("createMemorialHeading", initialLanguage)}`}
        description={`${getLabel(
          "createMemorialHeadingDescription",
          initialLanguage
        )}`}
        footer={
          <Button
            ref={actionBtnRef}
            className="btn-system-primary"
            disabled={addDeathPersonButtonEnabled}
          >
            {`${getLabel("create", initialLanguage)}`}
          </Button>
        }
      >
        <MemorialCreatePage
          response={response}
          actionBtnRef={actionBtnRef}
          handleCreate={handleCreate}
          addSearchValue={addSearchValue}
        />
      </FullScreenModal>
      <div className=" ">
        <div className="d-flex justify-content-between align-items-center page-heading-container">
          <div className="page-header">
            <h1 className="page-heading">
              {getLabel("allMemorialPages", initialLanguage)}
            </h1>
            <p className="page-heading-sub text-muted">
              {getLabel("allMemorialPagesDescription", initialLanguage)}
            </p>
          </div>
          {!loading ? (
            <button
              type="button"
              className="btn btn-system-primary mt-n10 border-0 outline-none"
              onClick={() => {
                setMemorialCreate(true);
              }}
            >
              <strong>+ </strong>
              {getLabel("create", initialLanguage)}
            </button>
          ) : (
            <button
              type="button"
              className=" d-none d-sm-flex me-3 shimmer-button22 shimmer-effect"
            ></button>
          )}
        </div>

        <hr className="page-heading-divider" />

        {/* filter */}
        <div className="mb-4 mb-md-0">
          <Filter />
        </div>

        {/* Table */}
        <div className="d-none d-sm-block ">
          {loading ? (
            <MemorialSkelton loopTimes={9} />
          ) : memorialData.data.length > 0 ? (
            <table className="table mt-3">
              <tbody className=" ">
                {memorialData.data?.map((data: any) => {
                  return (
                    <tr
                      key={data.id}
                      className=" align-middle "
                      onClick={() => {
                        singleView(data.uuid);
                      }}
                    >
                      <td
                        className="col-5 col-lg-5 col-xl-3 ps-0"
                        style={{ maxWidth: "200px" }}
                      >
                        <div className="d-flex align-items-center gap-3">
                          <div className="ms-md-3 ms-3 table-img text-muted font-140 system-text-dark-light d-flex align-items-center justify-content-center rounded-circle">
                            {data && data.photo_url ? (
                              <CustomImage
                                src={data?.photo_url}
                                alt=""
                                className="image rounded-circle"
                                ref={imgRef}
                                width={200}
                                height={200}
                                divClass="table-img"
                              />
                            ) : data && data.name ? (
                              data?.name[0]
                            ) : (
                              ""
                            )}
                          </div>
                          <div style={{ maxWidth: "50%" }}>
                            <div className="font-110 font-semibold d-flex">
                              <div className="text-truncate">
                                {data?.name ? data?.name : ""}
                              </div>
                            </div>
                            <div className="text-dark font-90 text-truncate">
                              {data?.updated_at
                                ? timeagoformatter(data?.created_at)
                                : ""}
                            </div>
                          </div>
                        </div>
                      </td>
                      <td
                        className="col-2 col-lg-2 col-xl-2 p-0 "
                        style={{ maxWidth: "90px" }}
                      >
                        <>
                          <div className="text-secondary font-80 d-flex align-items-center  justify-content-start ">
                            <p className="p-0 m-0">
                              {getLabel("country", initialLanguage)}
                            </p>
                          </div>

                          <div className="text-dark font-90 d-flex align-items-center  justify-content-start">
                            <p
                              className="p-0 m-0 text-truncate"
                              style={{ maxWidth: "80%" }}
                            >
                              {data?.death_location
                                ? data?.death_location
                                : "Unknown"}
                            </p>
                          </div>
                        </>
                      </td>
                      <td className="col-2 col-lg-2 col-xl-2 p-0 pe-sm-3">
                        {/* {data?.dod && ( */}
                        <>
                          <div className="text-secondary font-80 font-thin d-none d-sm-flex text-truncate">
                            {getLabel("deathDate", initialLanguage)}
                          </div>
                          <div className="text-dark font-90 d-none d-sm-flex text-truncate">
                            {data?.dod ? MonthDate(data?.dod) : "Unknown"}
                          </div>
                        </>
                        {/* )} */}
                      </td>
                      <td
                        className="col-2 col-lg-2 col-xl-4 p-0"
                        style={{ maxWidth: "60px" }}
                      >
                        {/* {data?.visibility && ( */}
                        <>
                          <div className="text-secondary font-80 font-thin">
                            {getLabel("status", initialLanguage)}
                          </div>
                          <div>
                            <p
                              className="text-dark font-90 text-truncate p-0 m-0"
                              style={{ maxWidth: "90%" }}
                            >
                              {/* {data?.state.name || "-"} */}
                              {data?.state.name == "Enabled" ? (
                                <span className="text-success text-truncate">
                                  {data?.state.name}
                                </span>
                              ) : (
                                <span className="text-danger text-truncate">
                                  {data?.state.name}
                                </span>
                              )}
                            </p>
                          </div>
                        </>
                        {/* )} */}
                      </td>
                      <td className="col-1 col-lg-1 col-xl-1 p-0">
                        <div className="p-0 m-0 d-flex align-items-center justify-content-end">
                          <Link href={`/memorials/${data.uuid}`}>
                            <button
                              type="button"
                              className="btn btn-system-light me-3"
                            >
                              {getLabel("view", initialLanguage)}
                            </button>
                          </Link>
                        </div>
                      </td>
                    </tr>
                  );
                })}{" "}
              </tbody>
            </table>
          ) : (
            <div className="w-100 d-flex align-items-center justify-content-center grey-600 py-4 .font-120">
              No Memorial Found
            </div>
          )}
        </div>
        <div className="d-block d-sm-none">
          {loading ? (
            <TableCardSkelton loopTimes={9} />
          ) : (
            memorialData?.data?.map((_: any, item: any) => {
              return (
                <>
                  <TableCardMemorial
                    data={memorialData?.data[item]}
                    singleView={singleView}
                    initialLanguage={initialLanguage}
                  />
                </>
              );
            })
          )}
        </div>

        {/* -------------------pagination-------------------------- */}
        {memorialData && memorialData.data?.length == 0
          ? ""
          : memorialData.data?.length > 0 && (
              <>
                <hr className="" />
                <div className="row align-items-center m-0 mt-4 mb-4 mb-md-0">
                  <div className="col-xl-3 col-12 p-0 font-110 text-center text-xl-start">
                    {getLabel("totalRecords", initialLanguage)}:
                    <span> {memorialData?.total}</span>{" "}
                  </div>
                  <div className="col-xl-9 col-12 p-0 d-flex justify-content-center justify-content-xl-end  mt-2 mt-lg-0 mb-4 mb-0">
                    {/* <PaginationComponent
                  page={page}
                  totalPages={Math.ceil(memorialData.total / 15)}
                  handlePagination={handlePagination}
                /> */}
                    <MainPagination
                      page={parseInt(query?.p as string) || 1}
                      totalPages={memorialData.total}
                      handlePagination={handlePagination}
                    />
                  </div>
                </div>
              </>
            )}
      </div>
    </>
  );
};

export async function getServerSideProps({
  req,
  res,
  query,
}: GetServerSidePropsContext) {
  const pageQuery = query && query.p !== undefined ? query?.p : 1;
  const cookies = parse(req.headers.cookie || "");
  const token = cookies.token;
  const initialLanguage = getCookie("language", { req, res }) || "";

  if (!token) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }
  return {
    props: {
      pageQuery,
      initialLanguage,
    },
  };
}
export default Memorial;
