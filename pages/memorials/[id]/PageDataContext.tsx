import React, { createContext, ReactNode, useContext } from 'react';

interface PageDataContextProps {
  handleSave: () => void;
  PageData: any;
  children?: ReactNode;
}

const defaultValue: PageDataContextProps = {
  handleSave: () => { },
  PageData: null,
};

export const PageDataContext = createContext<PageDataContextProps>(defaultValue);

export const PageDataProvider: React.FC<PageDataContextProps> = ({ handleSave, PageData, children }) => {
  return (
    <PageDataContext.Provider value={{ handleSave, PageData }}>
      {children}
    </PageDataContext.Provider>
  );
};
export default PageDataProvider;