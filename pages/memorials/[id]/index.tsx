import MemorialAbout from "@/components/Memorials/MemorialAbout";
import MemorialHome from "@/components/Memorials/MemorialHome";
import Image from "next/image";
import React, { useContext, useEffect, useState } from "react";
import eye from "../../../public/eye.svg";
import { Tab, Tabs } from "react-bootstrap";
import ProfileBanner from "@/components/Profile/ProfileBanner";
import ProfileMeta from "@/components/Profile/ProfileMeta";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import { CountriesSelector } from "@/redux/countries";
import { getAllSalutations, identitySelector } from "@/redux/identities";
import { getAllInterfaceLanguages, languageSelector } from "@/redux/language";
import { getAllNoticeTypes, noticeTypeSelector } from "@/redux/notice-types";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import {
  getAllNoticeTemplates,
  templatesSelector,
} from "@/redux/notice-templates";
import { memorialPageSelector, showPage } from "@/redux/memorial-pages";
import { PageDataProvider } from "./PageDataContext";
import { VisibilitiesSelector, getAllVisibilities } from "@/redux/visibilities";
import { PageTypesSelector, getAllPageTypes } from "@/redux/page-types";
import {
  birthDeathTitlesSelector,
  getAllBirthAndDeathTitles,
} from "@/redux/birth-death-titles";
import { getCookie } from "cookies-next";
import { getAllPageTemplates } from "@/redux/page-templates";
import { useRouter } from "next/router";
import { getAllRelationships, relationsSelector } from "@/redux/relationships";
import { getAllCustomerforpage } from "@/redux/customer";
import { getLabel } from "@/utils/lang-manager";
import { parse } from "cookie";
import { GetServerSidePropsContext } from "next/types";
import CustomAlert from "@/components/Alert";

function ShowMemorial({ idQuery, tabQuery, initialLanguage }: any) {
  const [activeKey, setActiveKey] = useState("");

  const [isERR, setERR] = useState(false);
  const dispatch = useAppDispatch();
  const [isLoading, setIsLoading] = useState(true);
  //-----------Edit info Data----------------------------------------------------------------------------//
  const loadEditData = async () => {
    setIsLoading(true);
    await dispatch(
      showPage({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_page: idQuery as unknown as string,
      })

    ).then((response: any) => {
      if (response?.payload == "Too Many Attempts.") {
        setIsLoading(true);
        setERR(true);
      } else if (response?.error?.message == 'Rejected') {
        router.push('/404');
      } else {
        setIsLoading(false);
      }
    });
  };
  //-----------Arrays of Salutation,Interface Languages,Notice Templates,Countries------------------------//
  const loadStaticData = async () => {
    dispatch(getAllSalutations());
    dispatch(getAllInterfaceLanguages());
    dispatch(getAllNoticeTemplates({ active_partner: ACTIVE_PARTNER }));
    dispatch(getAllPageTemplates({ active_partner: ACTIVE_PARTNER }));
    dispatch(getAllPageTypes({ active_partner: ACTIVE_PARTNER }));
    dispatch(getAllVisibilities());
    dispatch(getAllBirthAndDeathTitles());
    dispatch(getAllRelationships());
    dispatch(
      getAllCustomerforpage({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
      })
    );
  };
  const { dataOne: showtPageData } = useAppSelector(memorialPageSelector);
  const { data: showCountriesData } = useAppSelector(CountriesSelector);
  const { data: showtNoticeTypeData } = useAppSelector(noticeTypeSelector);
  const { data: showSalutationsData } = useAppSelector(identitySelector);
  const { data: showRelationshipsData } = useAppSelector(relationsSelector);
  const { data: showLanguagessData } = useAppSelector(languageSelector);
  const { data: showTemplatessData } = useAppSelector(templatesSelector);
  const { data: VisibilitiesData } = useAppSelector(VisibilitiesSelector);
  const { data: showPageTypesData } = useAppSelector(PageTypesSelector);
  const { data: birthDeathTitlesData } = useAppSelector(
    birthDeathTitlesSelector
  );

  useEffect(() => {
    idQuery && loadStaticData();
    idQuery && loadEditData();
  }, [idQuery]);

  const handleSave = () => {
    idQuery && loadEditData();
  };
  const router = useRouter();
  const { query } = router;

  // useEffect(() => {
  //   if (isERR && !showtPageData?.id) {
  //     setIsLoading(true);
  //     router.push("/404");
  //   }
  // });

  //------------------All datas need to be send to components -----------------------------------------------//
  const PageData = {
    active_page: idQuery,
    abbreviation: showtPageData?.salutation?.name,
    abbreviation_id: showtPageData?.salutation?.id || "1",
    full_name: showtPageData?.name,
    full_lang_name: showtPageData?.["lang_name"],
    nick_name: showtPageData?.nickname,
    dob: showtPageData?.dob || "-",
    dod: showtPageData?.dod || "00 xxxx 0000",
    birth_location: showtPageData?.birth_location,
    death_location: showtPageData?.death_location,
    Status: showtPageData?.state?.["name"],
    Page_for: showtPageData?.page_type?.name || "-",
    visibility: showtPageData?.visibility?.id,
    visibility_name: showtPageData?.visibility?.name || "-",
    salutations: showSalutationsData,
    password: "123456,",
    languages: showLanguagessData,
    templates: showTemplatessData,
    visibilities: VisibilitiesData,
    pagetype_id: showtPageData?.["page_type"]?.id,
    pagetemplate_uuid: showtPageData?.["page_template"]?.uuid,
    relationship: showtPageData?.relationship?.id,
    page_types: showPageTypesData,
    countries: showCountriesData,
    noticetypes: showtNoticeTypeData,
    Country: showtPageData?.country?.name || "-",
    Country_id: showtPageData?.country?.id || "-",
    memorial_page_id: showtPageData?.id,
    created_at: showtPageData?.["created_at"],
    last_modified_at: showtPageData?.["updated_at"],
    created_by:
      showtPageData?.owner?.["first_name"] +
      " " +
      showtPageData?.owner?.["last_name"],
    page_manager: showtPageData?.client?.["first_name"]
      ? showtPageData?.client?.["first_name"] +
      " " +
      showtPageData?.client?.["last_name"]
      : "-",
    client_country: showtPageData?.client?.country?.name,
    // +
    // " " +
    // showtPageData?.owner?.["last_name"],
    relationships: showRelationshipsData,
    birthDeathTitles: birthDeathTitlesData,
    birthDeathTitle: "3",
    clipart_url: showtPageData?.["clip_art"]?.["thumb_url"],
    frame_url: showtPageData?.frame?.["thumb_url"],
    border_url: showtPageData?.border?.["thumb_url"],
    template_url: showtPageData?.["page_template"]?.["thumb_url"],
    clipart_id: showtPageData?.["clip_art"]?.id,
    frame_id: showtPageData?.frame?.id,
    border_id: showtPageData?.border?.id,
    template_id: showtPageData?.["page_template"]?.id,
    clipart_alt: showtPageData?.["clip_art"]?.name,
    frame_alt: showtPageData?.frame?.name,
    border_alt: showtPageData?.border?.name,
    template_alt: showtPageData?.["page_template"]?.name,
    client: showtPageData?.client,
    id: showtPageData?.id,
    photoUrl: showtPageData?.["photo_url"],
    loading: !showtPageData?.id ? false : true,
    language_id: showtPageData?.language?.id,
    brief_about: showtPageData?.briefAbout || "",
    quote: showtPageData?.quote || "",
    hometown_location: showtPageData?.hometown_location || "",
    gender_id: showtPageData?.gender?.id,
    religion_id: showtPageData?.religion?.id,
    nature_of_death_id: showtPageData?.nature_of_death?.id,
    initialLanguage: initialLanguage,
  };

  const subtab = "personal-information";

  //---To set tabs--------------------------------------------------------------------------------------------//
  const handleTabSelect = (key: string) => {
    setActiveKey(key);
    if (key !== null) {
      if (key === "about") {
        router.push(
          { pathname: router.pathname, query: { ...query, t: key, s: subtab } },
          undefined,
          { shallow: true }
        );
      } else {
        const { s, ...updatedQuery } = query;
        updatedQuery.t = key;
        router.push(
          { pathname: router.pathname, query: updatedQuery },
          undefined,
          { shallow: true }
        );
      }
    }
  };
  useEffect(() => {
    const handleSetActiveKey = () => {
      if (Array.isArray(query?.t)) {
        setActiveKey(query?.t[0] || "home");
      } else {
        setActiveKey(query?.t || "home");
      }
    };
    // Call the function when the component mounts or when the query changes.
    handleSetActiveKey();
  }, [query?.t]);
  //--- tabs----------------------------------------------------------------------------------------------------------//

  console.log("loading", PageData?.loading);

  return (
    <PageDataProvider handleSave={handleSave} PageData={PageData}>
      {isERR ? (
        <>
          <div className={`custom-alertwrong  rounded active-link m-2`}>
            <div className="">
              <div className="custom-alert-title ">
                Something went wrong..
              </div>
            </div>
          </div>
        </>
      ) : (
        <div>
          {PageData.id ? (
            <>
              <div className="row align-items-center mb-4 mx-wi">
                <div className="col-10 ">
                  <h6 className="page-heading font-bold m-0">
                    {getLabel("memorialPage", initialLanguage)}
                  </h6>
                  <p className="m-0 text-muted font-80">{idQuery}</p>
                </div>
                <div className="col-6  text-end ps-5">
                  {activeKey !== "about" && (
                    <div className="d-none">
                      <Image src={eye} alt="eye icon" className="me-2" />
                      <span className="system-text-primary">
                        {getLabel("preview", initialLanguage)}
                      </span>
                    </div>
                  )}
                </div>
              </div>
            </>
          ) : (
            <>
              {" "}
              <div className="row align-items-center mb-4 mx-wi">
                <div>
                  <div className="col-2 ">
                    <h6 className="page-heading font-bold m-0 shimmer-text mb-1"></h6>
                  </div>
                  <div className="col-3 ">
                    <p className="m-0 text-muted font-80 shimmer-text"></p>
                  </div>
                </div>
                <div className="col-6  text-end ps-5">
                  {/* <Image src={eye} alt="" className="me-2" />
                  <span className="system-text-primary">Preview</span> */}
                </div>
              </div>
            </>
          )}
          {/* </div> */}
          <hr className="page-heading-divider" />
          {/* -----------------------profile components------------------ */}
          <div className="row m-0 bg-light pb-3 profile-container">
            <div>
              <ProfileMeta />
            </div>
            {/* ----------------------memorial profile------------------------ */}
            <div>
              <ProfileBanner />
            </div>
            {/* ----------------------memorial Tabs------------------------ */}
            <div>
              <div className="row">
                <Tabs
                  activeKey={activeKey}
                  onSelect={(k: any) => handleTabSelect(k)}
                  className="system-thin-black ps-md-4 "
                >
                  <Tab
                    title={`${getLabel("home", initialLanguage)}`}
                    eventKey={"home"}
                  >
                    <MemorialHome />
                  </Tab>
                  <Tab
                    title={`${getLabel("about", initialLanguage)}`}
                    eventKey={"about"}
                  >
                    <MemorialAbout />
                  </Tab>
                  <Tab title={"|"}></Tab>
                  <Tab
                    title={"Notices & Other Services"}
                    eventKey={"notices-and-services"}
                  >
                    <>Notices & Other Services</>
                  </Tab>
                  <Tab title={"More"} eventKey={"more"} className="hide-xs ">
                    <>More</>
                  </Tab>
                </Tabs>
              </div>
            </div>
          </div>
        </div>
      )}
    </PageDataProvider>
  );
}

export async function getServerSideProps({
  req,
  res,
  query,
}: GetServerSidePropsContext) {
  const idQuery = query.id || "";
  const tabQuery = query;

  const cookies = parse(req.headers.cookie || "");
  const token = cookies.token;
  const initialLanguage = getCookie("language", { req, res }) || "";

  if (!token) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }

  return {
    props: {
      idQuery,
      tabQuery,
      initialLanguage,
    },
  };
}
export default ShowMemorial;
