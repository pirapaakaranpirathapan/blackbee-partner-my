import CustomerHome from "@/components/Customer/CustomerHome";
import { customerPageSelector, getOneCustomer } from "@/redux/customer";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import { getCookie } from "cookies-next";
import Loader from "@/components/Loader/Loader";
import { CountriesSelector, getAllCountries } from "@/redux/countries";
import { fetchCountriesData, fetchNoticeTypeData } from "@/utils/api";
import FullScreenModal from "@/components/Elements/FullScreenModal";
import NoticeTypeCreate from "@/components/Modal/NoticeTypeCreate";
import { getAllGenders } from "@/redux/genders";
import { getLabel } from "@/utils/lang-manager";
import NoticeTypeCreateOne from "@/components/Modal/NoticeTypeCreateOne";
import { parse } from "cookie";
import { GetServerSidePropsContext } from "next/types";
import { noticeTypeSelector } from "@/redux/notice-types";
import CustomerDataProvider, {
  CustomerDataContext,
} from "./CustomerDataContext";
import CustomerNameSkelton from "@/components/Skelton/CustomerNameSkelton";

const ShowCustomer = ({ noticetypes, initialLanguage }: any) => {
  const [noticeCreate, setNoticeCreate] = useState(false);
  const router = useRouter();
  const dispatch = useAppDispatch();
  const { dataOne: customerSingleData } = useAppSelector(customerPageSelector);

  const { data: noticeTypeData } = useAppSelector(noticeTypeSelector);
  const [isLoading, setIsLoading] = useState(true);
  const uuid = router.query?.id;

  const loadData = (uuid: string | string[] | undefined) => {
    if (uuid) {
      setIsLoading(true);
      dispatch(
        getOneCustomer({
          active_partner: ACTIVE_PARTNER,
          active_service_provider: ACTIVE_SERVICE_PROVIDER,
          client_id: uuid as string,
        })
      ).then((response: any) => {
        if (response?.payload?.success) {
          setIsLoading(false);
        } else {
          console.log("Dispatch action failed", response.payload);
          setIsLoading(false);
        }
        setTimeout(() => {
          setIsLoading(false);
        }, 1000);
      });
    }
  };

  const handleSave = () => {
    uuid && loadData(uuid);
  };

  useEffect(() => {
    loadData(uuid);
    dispatch(getAllGenders());
  }, [uuid]);

  //===add all data need to be send to components through props===========//
  const CustomerData = {
    uuid: customerSingleData[0]?.uuid,
    first_name: customerSingleData[0]?.first_name,
    first_name_index: customerSingleData[0]?.first_name[0],
    last_name: customerSingleData[0]?.last_name,
    mobile: customerSingleData[0]?.mobile,
    alternative_phone: customerSingleData[0]?.alternative_phone,
    country: customerSingleData[0]?.country,
    gender: customerSingleData[0]?.gender,
    countryId: customerSingleData[0]?.country?.id,
    genderId: customerSingleData[0]?.gender?.id,
    email: customerSingleData[0]?.email,
    isLoading: isLoading,
    initialLanguage: initialLanguage,
  };
  const isDataEmpty = Object.keys(CustomerData).length === 0;
  const { data: showCountriresData } = useAppSelector(CountriesSelector);

  return (
    <CustomerDataProvider handleSave={handleSave} CustomerData={CustomerData}>
      <div>
        {isLoading && <div>{/* <Loader /> */}</div>}
        <FullScreenModal
          show={noticeCreate}
          onClose={() => {
            setNoticeCreate(false);
          }}
          title={`${getLabel("chooseNoticeType", initialLanguage)}`}
          description=""
        // footer={<Button className="btn-system-primary">Save</Button>}
        >
          <NoticeTypeCreateOne noticetypesDatas={noticeTypeData} />
        </FullScreenModal>

        {isLoading ? (
          <CustomerNameSkelton />
        ) : (
          <div className="d-flex justify-content-between align-items-center page-heading-container">
            <div
              className="d-flex gap-3 align-items-center page-header"
              style={{ maxWidth: "65%" }}
            >
              <div className="table-img system-text-dark-light rounded-circle ms-0 ms-sm-3 d-flex align-items-center justify-content-center fs-6 fw-semibold text-muted">
                {CustomerData && CustomerData
                  ? CustomerData.first_name_index
                  : ""}
              </div>
              <div className="" style={{ maxWidth: "65%" }}>
                <h6 className="page-heading font-bold m-0 text-truncate">
                  {/* {CustomerData && CustomerData.length > 0 && CustomerData ? CustomerData?.["first_name"] : ""} */}
                  {CustomerData && CustomerData ? CustomerData.first_name : ""}
                  <span className="ms-1">
                    {CustomerData && CustomerData ? CustomerData.last_name : ""}
                  </span>
                </h6>
                <p className="m-0 text-muted font-80  text-truncate">
                  {getLabel("customer", initialLanguage)}
                </p>
              </div>
            </div>
            <div className="ms-a20">
              <button
                type="button"
                className="btn btn-system-primary mt-n10 px-2 px-sm-4"
                // onClick={update}
                onClick={() => {
                  setNoticeCreate(true);
                }}
              >
                <strong>+ </strong>
                {getLabel("addNotice", initialLanguage)}
              </button>
            </div>
          </div>
        )}
        <hr className="page-heading-divider" />
        <div>
          <CustomerHome />
        </div>
      </div>
    </CustomerDataProvider>
  );
};

export async function getServerSideProps({
  req,
  res,
  query,
}: GetServerSidePropsContext) {
  const idQuery = query.id || "";

  const cookies = parse(req.headers.cookie || "");
  const token = cookies.token;
  const initialLanguage = getCookie('language', { req, res }) || "";
  if (!token) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }
  const noticetypes = await fetchNoticeTypeData(req, res);
  return {
    props: {
      idQuery,
      noticetypes,
      initialLanguage,
    },
  };
}
export default ShowCustomer;
