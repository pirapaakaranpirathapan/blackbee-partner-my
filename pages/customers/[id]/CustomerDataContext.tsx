import React, { createContext, ReactNode, useContext } from 'react';

interface CustomerDataContextProps {
  handleSave: () => void;
  CustomerData: any;
  children?: ReactNode;
}

const defaultValue: CustomerDataContextProps = {
  handleSave: () => { },
  CustomerData: null,
};

export const CustomerDataContext = createContext<CustomerDataContextProps>(defaultValue);

export const CustomerDataProvider: React.FC<CustomerDataContextProps> = ({ handleSave, CustomerData, children }) => {
  return (
    <CustomerDataContext.Provider value={{ handleSave, CustomerData }}>
      {children}
    </CustomerDataContext.Provider>
  );
};
export default CustomerDataProvider;