import React, { useEffect, useState } from "react";
import Image from "next/image";
import { useRouter } from "next/router";
import { PaginationComponent } from "@/components/Elements/Pagination";
import Filter from "@/components/Filter/Filter";
import Head from "next/head";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import { customerPageSelector, getAllCustomer } from "@/redux/customer";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import Loader from "@/components/Loader/Loader";
import Link from "next/dist/client/link";
import { integer } from "aws-sdk/clients/cloudfront";
import { getCookie } from "cookies-next";
import TableCardOne from "@/components/Cards/TableCardOne";
import CustomerSkelton from "@/components/Skelton/CustomerSkelton";
import dynamic from "next/dynamic";
import TableCardCustomerSkelton from "@/components/Skelton/TableCardCustomerSkelton";
import { getLabel } from "@/utils/lang-manager";
import { parse } from "cookie";
import { GetServerSidePropsContext } from "next/types";
import MainPagination from "@/components/Paginations";
import CustomImage from "@/components/Elements/CustomImage";
import TestPage from "@/components/Paginations";
import NewOnePagination from "@/components/Paginations/newPaginations";

const Customer = ({ customerQuery, initialLanguage }: any) => {
  const router = useRouter();
  // =============================================================================
  //This code sets up a Redux workflow to fetch all customer data
  const dispatch = useAppDispatch();
  const [loading, setLoading] = useState(true);
  const { data: allCustomerData } = useAppSelector(customerPageSelector);
  const loadData = (page: integer) => {
    setLoading(true);
    dispatch(
      getAllCustomer({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        page: page,
      })
    ).then(() => {
      setLoading(false);
    });
  };
  // useEffect(() => {
  //   loadData(1);
  // }, []);

  // =============================================================================
  //This function is used to navigate to a specific customer view based on the provided UUID.
  function singleView(uuid: any) {
    router.push({
      pathname: `/customers/${uuid}`,
      query: { t: "Customer Details" },
    });
  }

  // =============================================================================
  //pagination
  const { query } = router;
  const [page, setPage] = useState(1);
  const [activeCustomer, setActiveCustomer] = useState(1);
  const handlePagination = (page: number) => {
    setPage(page);
    loadData(page);
    setActiveCustomer(page);
    if (page !== null) {
      router.push(
        { pathname: router.pathname, query: { ...query, p: page } },

        undefined,
        { shallow: true }
      );
    }
  };
  useEffect(() => {
    console.log("customerQuery", customerQuery);

    setPage(customerQuery);
    loadData(customerQuery);
  }, [customerQuery]);
  return (
    <>
      <Head>
        <title>Blackbee | Customer</title>
      </Head>
      <div>
        <div className="d-flex justify-content-between align-items-center page-heading-container">
          <div className="page-header">
            <h1 className="page-heading">
              {getLabel("allCustomers", initialLanguage)}
            </h1>
            <p className="page-heading-sub text-muted">
              {getLabel("allCustomersDescription", initialLanguage)}
            </p>
          </div>
          <Link
            href={"customers/create"}
            className="text-decoration-none text-white lh-lg font-normal"
          >
            {!loading ? (
              <button
                type="button"
                className="btn btn-system-primary mt-n10 border-0"
              >
                <strong>+ </strong>
                {getLabel("create", initialLanguage)}
              </button>
            ) : (
              <button
                type="button"
                className=" d-none d-sm-flex me-3 shimmer-button22 border-0 shimmer-effect"
              ></button>
            )}
          </Link>
        </div>
        <hr className="page-heading-divider" />
        <div className="mb-0">
          <Filter />
        </div>

        <div className="d-none d-sm-block">
          {loading ? (
            <CustomerSkelton loopTimes={9} />
          ) : (
            allCustomerData && (
              <>
                {allCustomerData?.data?.length > 0 ? (
                  <>
                    <table className="table mt-3">
                      <tbody className="">
                        {allCustomerData?.data?.map((data: any) => {
                          return (
                            <tr
                              key={data.id}
                              className="align-middle ps-0"
                              onClick={() => singleView(data.uuid)}
                            >
                              <td
                                className="col-3 ps-0"
                                style={{ maxWidth: "150px" }}
                              >
                                <div className="d-flex align-items-center gap-3">
                                  <div className="ms-md-3 ms-0 table-img text-muted font-140 system-text-dark-light d-flex align-items-center justify-content-center rounded-circle">
                                    {/* {data && data.profile ? (
                                <Image
                                  src={data?.profile}
                                  alt=""
                                  className="image rounded-circle"
                                />
                              ) : data && data?.["first_name"] ? (
                                data?.["first_name"][0]
                              ) : (
                                ""
                              )} */}

                                    {data && data.photo_url ? (
                                      <CustomImage
                                        src={data.photo_url}
                                        alt=""
                                        className="image rounded-circle"
                                        width={200}
                                        height={200}
                                        divClass="image rounded-circle"
                                      />
                                    ) : data && data.first_name ? (
                                      <div className="rounded-circle">
                                        {`${data.first_name[0]}${
                                          data.last_name
                                            ? data.last_name[0]
                                            : ""
                                        }`}
                                      </div>
                                    ) : (
                                      ""
                                    )}
                                  </div>
                                  <div className="" style={{ maxWidth: "50%" }}>
                                    <div className="font-110 font-semibold text-truncate">
                                      {data?.["first_name"] &&
                                      data?.["first_name"]
                                        ? data?.["first_name"]
                                        : ""}{" "}
                                      {data?.["last_name"] &&
                                      data?.["last_name"]
                                        ? data?.["last_name"]
                                        : ""}
                                    </div>
                                    <div className="text-dark font-90 font-thin text-truncate">
                                      {data?.country.name
                                        ? data?.country.name
                                        : ""}
                                    </div>
                                  </div>
                                </div>
                              </td>
                              <td
                                className="col-7 p-0"
                                style={{ maxWidth: "150px" }}
                              >
                                <div className="text-dark font-110 d-flex align-items-center justify-content-end justify-content-sm-start">
                                  <p className="p-0 m-0 text-truncate">
                                    {data?.email ? data?.email : ""}
                                  </p>
                                </div>
                              </td>
                              <td className=" col-2  p-0  ">
                                <div className="p-0 m-0 d-none d-sm-flex align-items-center justify-content-end">
                                  <button
                                    type="button"
                                    className="btn btn-system-light d-none d-sm-flex me-3"
                                  >
                                    {getLabel("view", initialLanguage)}
                                  </button>
                                </div>
                              </td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  </>
                ) : (
                  <div className="w-100 d-flex align-items-center justify-content-center grey-600 py-4 .font-120">
                    No customers found.
                  </div>
                )}
              </>
            )
          )}
        </div>
        <div className="d-block d-sm-none">
          {loading ? (
            <TableCardCustomerSkelton loopTimes={9} />
          ) : (
            allCustomerData?.data?.length > 0 &&
            allCustomerData?.data?.map((_: any, item: any) => {
              return (
                <>
                  <TableCardOne
                    key={item}
                    data={allCustomerData?.data[item]}
                    singleView={singleView}
                    initialLanguage={initialLanguage}
                  />
                </>
              );
            })
          )}
        </div>
        {/* -------------------pagination-------------------,------- */}
        {allCustomerData && allCustomerData.data?.length == 0
          ? ""
          : allCustomerData.data?.length > 0 && (
              <>
                <hr className="" />
                <div className="row align-items-center m-0 mt-4 mb-4 mb-md-0">
                  <div className="col-xl-3 col-12 p-0 font-110 text-center text-xl-start">
                    {getLabel("totalRecords", initialLanguage)}:
                    <span> {allCustomerData?.total}</span>{" "}
                  </div>
                  <div className="col-xl-9 col-12 p-0 d-flex justify-content-center justify-content-xl-end">
                    {/* <PaginationComponent
                  page={page}
                  totalPages={Math.ceil(memorialData.total / 15)}
                  handlePagination={handlePagination}
                /> */}
                    {/* <MainPagination
                      page={page}
                      totalPages={256}
                      handlePagination={handlePagination}
                    /> */}
                    <TestPage
                      page={parseInt(query?.p as string) || 1}
                      totalPages={allCustomerData?.total}
                      handlePagination={handlePagination}
                    />
                    {/* <NewOnePagination
                      current={page}
                      total={allCustomerData?.total}
                      onChange={handlePagination}
                      showMore={false}
                      bufferSize={5}
                      sizeOptions={[10, 20]}
                    /> */}
                  </div>
                </div>
              </>
            )}
      </div>
    </>
  );
};
export async function getServerSideProps({
  req,
  res,
  query,
}: GetServerSidePropsContext) {
  const customerQuery = query && query.p !== undefined ? query?.p : 1;

  const initialLanguage = getCookie("language", { req, res });
  const cookies = parse(req.headers.cookie || "");
  const token = cookies.token;

  if (!token) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }

  return {
    props: { customerQuery, initialLanguage },
  };
}

// export async function getServerSideProps({ req, res, query }: any) {
//   const cookies = getCookie("token", { req, res });
//   const customerQuery =
//     query && query.p !== undefined ? parseInt(query?.p[0]) : 1;
//   if (cookies == undefined) {
//     res.writeHead(307, { Location: "/" });
//     res.end();
//   }

//   return {
//     props: { customerQuery },
//   };
// }

export default Customer;
