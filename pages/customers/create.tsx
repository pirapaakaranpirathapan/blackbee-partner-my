import Loader from "@/components/Loader/Loader";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import { CountriesSelector, getAllCountries } from "@/redux/countries";
import { customerPageSelector, createCustomer } from "@/redux/customer";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import { getCookie } from "cookies-next";
import router, { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { Notification } from "@arco-design/web-react";
import CustomerCreateSkelton from "@/components/Skelton/CustomerCreateSkelton";
import { useApiErrorHandling } from "@/components/Others/useApiErrorHandling";
import PhoneNumberInput from "@/components/Others/PhoneNumberInput";
import { gendersSelector, getAllGenders } from "@/redux/genders";
import { getLabel } from "@/utils/lang-manager";
import { parse } from "cookie";
import { GetServerSidePropsContext } from "next/types";
import CustomSelect from "@/components/Arco/CustomSelect";

const CustomerDetails = ({ data, initialLanguage }: any) => {
  const tab = "Customer Details";
  const [response, setResponse] = useState("");
  const [activeTab, setActiveTab] = useState(tab);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [countryId, setCountryId] = useState("");
  const [genderId, setGenderId] = useState("");
  const [email, setEmail] = useState("");
  const [password, setpassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [mobile, setMobile] = useState("");
  const [alternativeNumber, setAlternativeNumber] = useState("");
  const [loading, setLoading] = useState(true);
  const dispatch = useAppDispatch();
  const { error: apiError } = useAppSelector(customerPageSelector);
  const { data: showCountriresData } = useAppSelector(CountriesSelector);
  const { data: gendersData } = useAppSelector(gendersSelector);
  const errors = useApiErrorHandling(apiError, response);

  useEffect(() => {
    const loadData = async () => {
      await dispatch(getAllGenders());
      await dispatch(getAllCountries());
    };
    loadData()
      .then(() => setLoading(false))
      .catch((error) => {
        setLoading(false);
      });
  }, []);

  const handleGenderChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedValue = e.target.value;
    setGenderId(selectedValue);
  };

  const handleCreate = () => {
    setLoading(true);
    const updatedData = {
      first_name: firstName,
      last_name: lastName,
      country_id: countryId,
      email: email,
      mobile: mobile,
      alternative_phone: "",
      gender_id: "3",
      password: "12345678",
      password_confirmation: "12345678",
    };
    if (alternativeNumber.length > 5) {
      updatedData.alternative_phone = alternativeNumber;
    } else {
      const alternativeProtectFlag = "";
      updatedData.alternative_phone = alternativeProtectFlag;
    }
    dispatch(
      createCustomer({
        activePartner: ACTIVE_PARTNER,
        activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
        updatedData,
      })
    ).then((response: any) => {
      // setFirstName("");
      // setLastName("");
      // setCountryId("");
      // setEmail("");
      // setMobile("");
      // setAlternativeNumber("");

      setLoading(false);
      if (response.payload.success == true) {
        Notification.success({
          title: "Customer has been created successfully",
          duration: 3000,
          content: undefined,
        });
        setLoading(true);
        // router.push("/customers");
        const createId = response.payload?.data?.data?.uuid;
        router.push(`/customers/${createId}?t=Customer+Details`);

        setResponse(response);
      } else {
        response.payload.code != 422 &&
          Notification.error({
            title: "Faild to create customer",
            duration: 3000,
            content: undefined,
          });
      }
    });
  };

  const changeTab = (val: any) => {
    setActiveTab(val);
  };

  return (
    <div>
      <div className="row">
        <div className="page-header">
          <h1 className="font-210 font-bold p-0 m-0">
            {getLabel("addNewCustomer", initialLanguage)}
          </h1>
          <p className="page-heading-sub text-muted">
            {getLabel("addNewCustomerDescription", initialLanguage)}
          </p>
        </div>
      </div>
      <hr className="page-heading-divider" />
      {loading || (showCountriresData && showCountriresData.length == 0) ? (
        <CustomerCreateSkelton />
      ) : (
        <div className=" page-heading-container">
          <div className="row m-0 rounded mx-md-1 my-4">
            <div className=" col-xl-10 col-lg-9 col-md-9 col-12 bg-body pt-3">
              <div className="row ">
                <div className="col-12 m-0 p-0 px-3 mb-3">
                  {activeTab === "Customer Details" && (
                    <>
                      <div className="px-md-5 ">
                        <div className="mb-4">
                          <div className="row  align-items-center m-0">
                            <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0   font-bold  text-start">
                              {getLabel("firstName", initialLanguage)}
                              <span className="ps-1">*</span>
                              {/* <span className="text-danger">*</span> */}
                            </div>
                            <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0">
                              <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                placeholder={data?.first_name}
                                value={firstName}
                                onChange={(e) => setFirstName(e.target.value)}
                              />
                            </div>
                          </div>
                          {errors.first_name && (
                            <div className="row  align-items-center m-0">
                              <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0"></div>
                              <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 text-danger">
                                {errors.first_name}
                              </div>
                            </div>
                          )}
                        </div>

                        <div className="mb-4">
                          <div className="row align-items-center m-0">
                            <div className=" col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0   font-bold  text-start">
                              {getLabel("lastName", initialLanguage)}
                              {/* <span className="text-danger">*</span> */}
                            </div>
                            <div className=" col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0">
                              <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                value={lastName}
                                onChange={(e) => setLastName(e.target.value)}
                              />
                            </div>
                          </div>
                          {errors.last_name && (
                            <div className="row align-items-center m-0">
                              <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0"></div>
                              <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 text-danger">
                                {errors.last_name}
                              </div>
                            </div>
                          )}
                        </div>
                        {/* <div className="mb-4">
                          <div className="row  align-items-center m-0">
                            <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0   font-bold  text-start">
                              {getLabel("gender",initialLanguage)}
                              <span className="text-danger">*</span>
                            </div>
                            <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 ">

                              <CustomSelect
                                data={gendersData}
                                placeholderValue={getLabel("selectAGender",initialLanguage)}
                                onChange={(value: any) => setGenderId(value)}
                                is_search={false}
                                defaultValue={genderId}
                              />
                            </div>
                          </div>
                          {errors.gender_id && (
                            <div className="row  align-items-center m-0">
                              <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0"></div>
                              <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 text-danger">
                                {errors.gender_id}
                              </div>
                            </div>
                          )}
                        </div> */}
                        <div className="mb-4">
                          <div className="row  align-items-center m-0">
                            <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0   font-bold  text-start">
                              {getLabel("country", initialLanguage)}
                              <span className="ps-1">*</span>
                              {/* <span className="">*</span> */}
                            </div>
                            <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 page-arco">
                              <CustomSelect
                                data={showCountriresData}
                                placeholderValue={getLabel(
                                  "selectACountry",
                                  initialLanguage
                                )}
                                onChange={(value: any) => setCountryId(value)}
                                is_search={true}
                                defaultValue={countryId}
                              />
                            </div>
                          </div>
                          {errors.country_id && (
                            <div className="row  align-items-center m-0">
                              <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0"></div>
                              <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 text-danger">
                                {errors.country_id}
                              </div>
                            </div>
                          )}
                        </div>

                        <div className="mb-4">
                          <div className="row align-items-center m-0">
                            <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0 font-bold text-start">
                              {getLabel("email", initialLanguage)}
                              <span className="ps-1">*</span>
                              {/* <span className="">*</span> */}
                            </div>
                            <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0">
                              <input
                                type="text"
                                className="form-control border border-2 p-2 system-control "
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                                name="email"
                                autoComplete="new-email"
                              />
                            </div>
                          </div>
                          {errors.email && (
                            <div className="row  align-items-center m-0">
                              <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0"></div>
                              <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 text-danger">
                                {errors.email}
                              </div>
                            </div>
                          )}
                        </div>
                        {/* <div className="mb-4">
                          <div className="row align-items-center m-0">
                            <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0 font-bold text-start">
                              {getLabel("password",initialLanguage)}
                              <span className="">*</span>
                            </div>
                            <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0">
                              <input
                                type="password"
                                className="form-control border border-2 p-2 system-control font-bold"
                                value={password}
                                onChange={(e) => setpassword(e.target.value)}
                                name="password"
                                autoComplete="new-password"
                              />
                            </div>
                          </div>
                          {errors.password && (
                            <div className="row  align-items-center m-0">
                              <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0"></div>
                              <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 text-danger">
                                {errors.password}
                              </div>
                            </div>
                          )}
                        </div>
                        <div className="mb-4">
                          <div className="row align-items-center m-0">
                            <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0 font-bold text-start">
                              {getLabel("confirmPassword",initialLanguage)}
                              <span className="">*</span>
                            </div>
                            <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0">
                              <input
                                type="password"
                                className="form-control border border-2 p-2 system-control "
                                value={confirmPassword}
                                onChange={(e) =>
                                  setConfirmPassword(e.target.value)
                                }
                              />
                            </div>
                          </div>
                          {errors.password && (
                            <div className="row  align-items-center m-0">
                              <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0"></div>
                              <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 text-danger">
                                {errors.password}
                              </div>
                            </div>
                          )}
                        </div> */}

                        <div className="mb-4">
                          <div className="row  align-items-center m-0">
                            <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0   font-bold  text-startd">
                              {getLabel("mobile", initialLanguage)}
                              <span className="ps-1">*</span>
                              {/* <span className="text-danger">*</span> */}
                            </div>
                            <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0">
                              <PhoneNumberInput
                                id="phone"
                                setMobile={setMobile}
                                mobile={mobile}
                                defaultCountryCode="+94"
                              />
                            </div>
                          </div>
                          {errors.mobile && (
                            <div className="row  align-items-center m-0 ">
                              <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0"></div>
                              <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 text-danger">
                                {errors.mobile}
                              </div>
                            </div>
                          )}
                        </div>

                        <div className="mb-4">
                          <div className="row  align-items-center m-0">
                            <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0   font-bold  text-start">
                              {getLabel("alternativeNumber", initialLanguage)}
                            </div>
                            <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 ">
                              <PhoneNumberInput
                                id="alternativePhone"
                                setMobile={setAlternativeNumber}
                                mobile={alternativeNumber}
                              />
                            </div>
                          </div>
                          {errors.alternative_phone && (
                            <div className="row  align-items-center m-0">
                              <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0"></div>
                              <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 text-danger">
                                {errors.alternative_phone}
                              </div>
                            </div>
                          )}
                        </div>
                        <div className="row  align-items-center m-0 mb-3">
                          <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0   font-bold  text-start"></div>
                          <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0">
                            <div>
                              <button
                                className="btn btn-dark rounded font-110"
                                onClick={handleCreate}
                              >
                                {/* {getLabel("save",initialLanguage)} */}{" "}
                                create
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export async function getServerSideProps({
  req,
  res,
  query,
}: GetServerSidePropsContext) {
  const customerQuery =
    query && query.p !== undefined ? parseInt(query?.p[0]) : 1;
  const cookies = parse(req.headers.cookie || "");
  const token = cookies.token;
  const initialLanguage = getCookie("language", { req, res }) || "";
  if (!token) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }

  return {
    props: { customerQuery, initialLanguage },
  };
}

export default CustomerDetails;
