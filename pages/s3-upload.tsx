import { S3Client, PutObjectCommand } from "@aws-sdk/client-s3";
import React, { useState } from "react";

const s3Client = new S3Client({
  region: "us-west-2", // replace with your bucket's region
  credentials: {
    accessKeyId: "AKIAUV6MFPRFBJSIDQTB", // replace with your AWS access key ID
    secretAccessKey: "FsJPZBCM1RK2FvW5m+s90L3w0iOoloJWYPfoFiqc", // replace with your AWS secret access key
  },
});

const UploadImage = () => {
  const [selectedFile, setSelectedFile] = useState(null) as unknown as any;

  const handleFileChange = (event: any) => {
    setSelectedFile(event.target.files[0]);
  };

  const handleUpload = () => {
    const bucketName = "direct-upload-s3-bucket-testing";
    const objectKey = "first-image";

    const putObjectCommand = new PutObjectCommand({
      Bucket: bucketName,
      Key: objectKey,
      Body: selectedFile,
      ContentType: selectedFile.type,
    });

    s3Client
      .send(putObjectCommand)
      .then((data) => {
        console.log("Image uploaded successfully", data);
      })
      .catch((error) => {
        console.error("Error uploading image", error);
      });
  };

  return (
    <div>
      <input type="file" onChange={handleFileChange} />
      <button onClick={handleUpload}>Upload</button>
    </div>
  );
};

export default UploadImage;
