import NotFound from '@/components/Others/NotFound';
import { getCookie } from 'cookies-next';
import React from 'react';


const NotFoundPage: React.FC = () => {
    return (
        <>
            <NotFound />
        </>
    );
};



export default NotFoundPage;
