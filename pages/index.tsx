import React, { useEffect, useState } from 'react';
import LoginPage from './login';
import { getCookie } from 'cookies-next';
import { parse } from 'cookie';
import { GetServerSidePropsContext } from 'next/types'
import { decryptToken } from '@/utils/encryptionAndDecryptionUtils';

const HomePage = ({ initialLanguage }: any) => {

  return (
    <LoginPage />
  );
};

export async function getServerSideProps({
  req,
  res,
  query,
}: GetServerSidePropsContext) {
  const cookies = parse(req.headers.cookie || "");
  const encryptedToken = cookies.token;
  const initialLanguage = getCookie('language', { req, res }) || "";
  // Decrypt the token using the decryption utility
  const decryptedToken = decryptToken(encryptedToken);
  // console.log("decryptedToken", decryptedToken);
  if (decryptedToken) {
    return {
      redirect: {
        destination: "/dashboard",
        permanent: false,
      },
    };
  }

  return {
    props: {
      initialLanguage,
    },
  };
}

export default HomePage;