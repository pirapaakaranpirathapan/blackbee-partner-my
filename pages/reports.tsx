import React, { useRef, useState } from "react";
import { parse } from "cookie";
import { GetServerSidePropsContext } from "next/types";
import SearchArcoSelect from "@/components/Arco/SearchArcoSelect";
import FullScreenModal from "@/components/Elements/FullScreenModal";
import { getLabel } from "@/utils/lang-manager";
import { Button } from "react-bootstrap";
import MemorialCreate from "@/components/Modal/MemorialCreate";
import { createMemorialPage } from "@/redux/memorial-pages";
import { useAppDispatch } from "@/redux/store/hooks";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import { Notification } from "@arco-design/web-react";
import { integer } from "aws-sdk/clients/cloudfront";
import CustomerDetailsModal, {
  ChildModalRef,
} from "@/components/Modal/createCustomerModal";
import { getCookie } from "cookies-next";

const reports = ({ initialLanguage }: any) => {
  const [showCreateModal, setShowCreateModal] = useState(false);
  const [showCustomerModal, setShowCustomerModal] = useState(false);

  const [selectedValue, setSelectedValue] = useState<string | null>(null);
  const [response, setResponse] = useState("");
  const [newError, setNewError] = useState({});
  const [createSearchValue, setCreateSearchValue] = useState({
    inputValue: "",
    searchType: "",
  });

  const childModalRef = useRef<ChildModalRef>(null);

  const actionBtnRef = useRef<HTMLButtonElement>(null);
  const dispatch = useAppDispatch();

  const checkErrors = (
    personName: string,
    dob: string,
    dod: string,
    pageTypeId: string
  ) => {
    const error = {
      name: ["The name field is required."],
      client_id: [
        "The client id must be an integer.",
        "The client id field is required.",
      ],
      dob: [
        "The dob does not match the format Y-m-d.",
        "The dob must be a date before or equal to today.",
      ],
      dod: [
        "The dod does not match the format Y-m-d.",
        "The dod must be a date before or equal to today.",
      ],
      pagetype_id: ["The page type id field is required."],
      message: "The page type id field is required. (and 6 more errors)",
    };

    const localError = {} as any;

    if (!personName) {
      localError.name = error.name;
    }
    if (!dob || dob === "-") {
      localError.dob = error.dob;
    }
    if (!dod) {
      localError.dod = error.dod;
    }
    if (!pageTypeId) {
      localError.pageTypeId = error.pagetype_id;
    }

    return localError;
  };

  const handleCreate = (
    personName: string,
    // salutationId: string,
    salutation: integer,
    // clientId: string,
    countryId: string,
    pageTypeId: string,
    relationship: string,
    dod: string,
    dob: string,
    deathPlace: string
  ) => {
    const localError = checkErrors(personName, dob, dod, pageTypeId);
    setNewError(localError);

    if (Object.keys(localError).length === 0) {
      // If there are no errors, create and set local storage
      createPage(
        personName,
        salutation,
        // clientId,
        countryId,
        pageTypeId,
        relationship,
        dod,
        dob,
        deathPlace
      );

      // Clear localError because there are no errors
      setNewError({});
    } else {
      // If there are errors, set the errors
      setNewError(localError);
    }
  };
  const createPage = (
    personName: string,
    salutation: integer,
    countryId: string,
    pageTypeId: string,
    relationship: string,
    dod: string,
    dob: string,
    deathPlace: string
  ) => {
    // Create a new data object
    const CreateData = {
      name: personName,
      page_type_id: pageTypeId,
      salutation_id: salutation,
      relationship_id: relationship,
      dod: dod,
      dob: dob,
      death_location: deathPlace,
      client_id: 1004,
      country_id: countryId,
    };

    // Retrieve any existing data from local storage
    const storedMemorialData = localStorage.getItem("memorialData");
    let memorialDataArray = [];

    if (storedMemorialData) {
      // Parse and update the existing data array
      memorialDataArray = JSON.parse(storedMemorialData);
    }

    // Add the new data object to the array
    memorialDataArray.push(CreateData);

    try {
      // Convert the updated array to a JSON string
      const memorialDataString = JSON.stringify(memorialDataArray);
      // Set the updated data in local storage
      localStorage.setItem("memorialData", memorialDataString);
      // const updatedDefaultNames = [personName, ...defaultNames];
      // setDefaultNames(updatedDefaultNames);
      setShowCreateModal(false);
      setNewError({});
    } catch (error) {
      console.error("Error while setting data in local storage:", error);
    }
  };

  const updateCreateSearchValue = (inputValue: any, searchType: any) => {
    setCreateSearchValue({ inputValue, searchType });
  };

  //memorial
  const handleOptionChange = (selectedValues: string) => {
    console.log("selectedValues", selectedValues);

    // setSelectedValues(updatedSelectedValues);
  };

  //customermodal
  const handleClick = async () => {
    if (childModalRef.current) {
      childModalRef.current.callModalFunction(handleChildResponse);
    }
  };

  const handleChildResponse = (response: any) => {
    const customerResponseData = response;

    setShowCustomerModal(false);
  };

  //Create customer
  const onCallCustomerContinue = (value: any) => {
    // setCustomerId(value);
    setShowCustomerModal(false);
  };
  return (
    <>
      <FullScreenModal
        show={showCreateModal}
        onClose={() => {
          setShowCreateModal(false);
        }}
        cusSize={"custom-size-3"}
        title={`${getLabel("createMemorialHeading", initialLanguage)}`}
        description={`${getLabel(
          "createMemorialHeadingDescription",
          initialLanguage
        )}`}
        footer={
          <Button ref={actionBtnRef} className="btn-system-primary">
            {getLabel("create", initialLanguage)}
          </Button>
        }
      >
        <MemorialCreate
          response={response}
          actionBtnRef={actionBtnRef}
          handleCreate={handleCreate}
          addSearchValue={createSearchValue}
          errors={newError}
        />
      </FullScreenModal>
      <FullScreenModal
        show={showCustomerModal}
        onClose={() => {
          setShowCustomerModal(false);
        }}
        title={`${getLabel("addNewCustomer", initialLanguage)}`}
        description={`${getLabel(
          "addNewCustomerModalDescription",
          initialLanguage
        )}`}
        footer={
          <Button className="btn-system-primary pointer" onClick={handleClick}>
            {getLabel("create", initialLanguage)}
          </Button>
        }
        deleteLabel={
          <a
            onClick={() => {
              setShowCustomerModal(false);
            }}
          >
            {getLabel("close", initialLanguage)}
          </a>
        }
      >
        <CustomerDetailsModal
          ref={childModalRef}
          onCallCustomerContinue={onCallCustomerContinue}
          addCusSearchValue={createSearchValue}
          setModalVisible={setShowCustomerModal}
        />
      </FullScreenModal>

      <div className="mb-5">{/* reports */}</div>
      <div className="mb-5 mt-5 row align-items-center m-0">
        <div className="col-2 p-0">Death person</div>
        <div className="col-7 p-0">
          <SearchArcoSelect
            useDefaultSearch={false}
            placeholderValue={
              "Search or Create death person’s profile page(Multiple)"
            }
            setIsModalVisible={setShowCreateModal}
            isVisible={showCreateModal}
            updateCreateSearchValue={updateCreateSearchValue}
            onOptionChange={handleOptionChange}
          />
        </div>
      </div>

      <div className="mb-5  row align-items-center m-0">
        <div className="col-2 p-0">Customer</div>
        <div className="col-7  p-0">
          <SearchArcoSelect
            useDefaultSearch={true}
            placeholderValue={
              "Search with name, email or telephone  +9199902929202"
            }
            setIsModalVisible={setShowCustomerModal}
            isVisible={showCustomerModal}
            updateCreateSearchValue={updateCreateSearchValue}
            onOptionChange={handleOptionChange}
          />
        </div>
      </div>
    </>
  );
};

export async function getServerSideProps({
  req,
  res,
  query,
}: GetServerSidePropsContext) {
  const cookies = parse(req.headers.cookie || "");
  const token = cookies.token;
  const initialLanguage = getCookie("language", { req, res }) || "";

  if (!token) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }

  return {
    props: {
      initialLanguage,
    },
  };
}

export default reports;
