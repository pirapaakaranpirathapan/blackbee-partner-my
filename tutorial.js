

function sumEvenNumbers(numbers) {
    return numbers.reduce((sum, number) =>
        number % 2 === 0 ? sum + number : sum, 0);
}



function getMessage(status) {
    let message;
    if (status === 'success') {
        message = 'Operation successful';
    } else if (status === 'error') {
        message = 'Operation failed';
    } else {
        message = 'Operation pending';
    }
    return message;
}


const getMessage = (status) =>
    status === 'success' ? 'Operation successful' :
        status === 'error' ? 'Operation failed' : 'Operation pending';


function getDiscount(price, isMember) {
    let discount;
    if (isMember) {
        if (price >= 500) {
            discount = 50;
        } else if (price >= 200) {
            discount = 20;
        } else {
            discount = 10;
        }
    } else {
        if (price >= 500) {
            discount = 25;
        } else if (price >= 200) {
            discount = 10;
        } else {
            discount = 5;
        }
    }
    return discount;
}


const getDiscount = (price, isMember) =>
    isMember ? (price >= 500 ? 50 : price >= 200 ? 20 : 10)
        : (price >= 500 ? 25 : price >= 200 ? 10 : 5);
