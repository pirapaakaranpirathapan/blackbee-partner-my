// CustomPagination.js

import React from "react";
import { Pagination } from "@arco-design/web-react";

const CustomPagination = ({
  current,
  total,
  onChange,
  showMore,
  bufferSize,
  sizeOptions,
}: {
  current: number;
  total: number;
  onChange: Function;
  showMore: boolean;
  bufferSize: number;
  sizeOptions: Array<number>;
}) => {
  const pageSize = sizeOptions ? sizeOptions[0] : 10;

  const handleChange = (pageNum: number) => {
    if (pageNum > total) {
      onChange(total, pageSize);
      return;
    }
    onChange(pageNum, pageSize);
  };

  return (
    <Pagination
      current={current}
      total={total}
      onChange={handleChange}
      showMore={showMore}
      bufferSize={bufferSize}
      sizeOptions={sizeOptions}
    />
  );
};

export default CustomPagination;
