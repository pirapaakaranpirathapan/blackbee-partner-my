import { useEffect, useState } from "react";

export const useApiErrorHandling = (apiError: any, response: any) => {
  const [errors, setErrors] = useState({
    msg: "",
    name: "",
    first_name: "",
    last_name: "",
    mobile: "",
    email: "",
    alternative_phone: "",
    password: "",
    password_confirmation: "",
    pageTypeId: "",
    dod: "",
    dob: "",
    status: "",
    client_id: "",
    pages: "",
    country_id: "",
    notice_type_id: "",
    event_type_id: "",
    gender_id: "",
    manager_type_id: "",
    contact_type: "",
    relationship: "",
  });
  console.log("123456789", apiError);

  useEffect(() => {
    if (apiError) {
      setErrors({
        msg: apiError.error ? apiError.error.message : "",

        name: apiError.error?.name
          ? apiError.error.name.length > 1
            ? apiError.error.name[1]
            : apiError.error.name[0]
          : "",

        first_name: apiError.error?.first_name
          ? apiError.error.first_name.length > 1
            ? apiError.error.first_name[1]
            : apiError.error.first_name[0]
          : "",

        last_name: apiError.error?.last_name
          ? apiError.error.last_name.length > 1
            ? apiError.error.last_name[1]
            : apiError.error.last_name[0]
          : "",

        mobile: apiError.error?.mobile
          ? apiError.error.mobile.length > 1
            ? apiError.error.mobile[1]
            : apiError.error.mobile[0]
          : "",

        email: apiError.error?.email
          ? apiError.error.email.length > 1
            ? apiError.error.email[1]
            : apiError.error.email[0]
          : "",
        alternative_phone: apiError.error?.alternative_phone
          ? apiError.error.alternative_phone.length > 1
            ? apiError.error.alternative_phone[1]
            : apiError.error.alternative_phone[0]
          : "",

        password: apiError.error?.password
          ? apiError.error.password.length > 1
            ? apiError.error.password[1]
            : apiError.error.password[0]
          : "",

        password_confirmation: apiError.error?.password_confirmation
          ? apiError.error.password_confirmation.length > 1
            ? apiError.error.password_confirmation[1]
            : apiError.error.password_confirmation[0]
          : "",

        pageTypeId: apiError.error?.page_type_id
          ? apiError.error.page_type_id.length > 1
            ? apiError.error.page_type_id[1]
            : apiError.error.page_type_id[0]
          : "",

        dod: apiError.error?.dod
          ? apiError.error.dod.length > 1
            ? apiError.error.dod[1]
            : apiError.error.dod[0]
          : "",

        dob: apiError.error?.dob
          ? apiError.error.dob.length > 1
            ? apiError.error.dob[1]
            : apiError.error.dob[0]
          : "",
        status: apiError.error?.state_id
          ? apiError.error.state_id.length > 1
            ? apiError.error.state_id[1]
            : apiError.error.state_id[0]
          : "",
        client_id: apiError.error?.client_id
          ? apiError.error.client_id.length > 1
            ? apiError.error.client_id[1]
            : apiError.error.client_id[0]
          : "",
        pages: apiError.error?.pages
          ? apiError.error.pages.length > 1
            ? apiError.error.pages[1]
            : apiError.error.pages[0]
          : "",
        country_id: apiError.error?.country_id
          ? apiError.error.country_id.length > 1
            ? apiError.error.country_id[1]
            : apiError.error.country_id[0]
          : "",
        notice_type_id: apiError.error?.notice_type_id
          ? apiError.error.notice_type_id.length > 1
            ? apiError.error.notice_type_id[1]
            : apiError.error.notice_type_id[0]
          : "",
        event_type_id: apiError.error?.event_type_id
          ? apiError.error.event_type_id.length > 1
            ? apiError.error.event_type_id[0]
            : apiError.error.event_type_id[1]
          : "",
        gender_id: apiError.error?.gender_id
          ? apiError.error.gender_id.length > 1
            ? apiError.error.gender_id[1]
            : apiError.error.gender_id[0]
          : "",
        manager_type_id: apiError.error?.manager_type_id
          ? apiError.error.manager_type_id.length > 1
            ? apiError.error.manager_type_id[1]
            : apiError.error.manager_type_id[0]
          : "",
        contact_type: "The contact_details.0.id_or_number field is required.",
        relationship: apiError.error?.relationship_id
          ? apiError.error.relationship_id.length > 1
            ? apiError.error.relationship_id[1]
            : apiError.error.relationship_id[0]
          : "",
      });
    }
  }, [apiError]);
  useEffect(() => {
    setErrors({
      msg: "",
      name: "",
      first_name: "",
      last_name: "",
      mobile: "",
      email: "",
      alternative_phone: "",
      password: "",
      password_confirmation: "",
      pageTypeId: "",
      dod: "",
      dob: "",
      status: "",
      client_id: "",
      pages: "",
      country_id: "",
      notice_type_id: "",
      event_type_id: "",
      gender_id: "",
      manager_type_id: "",
      contact_type: "",
      relationship: "",
    });
  }, []);
  useEffect(() => {
    if (response?.payload?.success) {
      clearErrorFields();
    }
  }, [response]);

  const clearErrorFields = () => {
    setErrors({
      msg: "",
      name: "",
      first_name: "",
      last_name: "",
      mobile: "",
      email: "",
      alternative_phone: "",
      password: "",
      password_confirmation: "",
      pageTypeId: "",
      dod: "",
      dob: "",
      status: "",
      client_id: "",
      pages: "",
      country_id: "",
      notice_type_id: "",
      event_type_id: "",
      gender_id: "",
      manager_type_id: "",
      contact_type: "",
      relationship: "",
    });
  };

  return errors;
};
