import React from "react";

const NotFound = () => {
  return (
    <div className="error-container">
      <h1 className="error-code">404</h1>
      <h1 className="error-title">Page Not Found</h1>
      <p className="error-description p-3 text-center">
        The page you are looking for might have been removed, had its name
        changed, or is temporarily unavailable.
      </p>
    </div>
  );
};

export default NotFound;
