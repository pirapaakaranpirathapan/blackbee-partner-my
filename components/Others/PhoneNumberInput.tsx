import React, { useEffect, useRef, useState } from "react";
import intlTelInput from "intl-tel-input";

const PhoneNumberInput = ({ id, mobile, setMobile, data, handleBlur }: any) => {
  const [iti, setIti] = useState<any | null>(null);
  const inputRef = useRef<HTMLInputElement | null>(null);


  useEffect(() => {
    if (inputRef.current) {
      const newIti = intlTelInput(inputRef.current, {
        utilsScript: "/intl-tel-input/js/utils.js?1690975972744",
        geoIpLookup: function (callback) {
          // Fetch user's geo location to determine the initial country
          fetch("https://ipapi.co/json")
            .then((res) => res.json())
            .then((geoData) => {
              // Use the provided country code or default to the user's country code
              const countryCode = data?.country_code || geoData.country_code;
              callback(countryCode);
            })
            .catch(() => callback("us"));
        },
      });

      // Set the intlTelInput instance in state
      setIti(newIti);

      // newIti?.setCountry("LK"); // Set initial country if needed



      // inputRef.current.addEventListener("countrychange", function () {
      //   const selectedCountryData = newIti.getSelectedCountryData();
      //   setMobile("+" + selectedCountryData.dialCode);
      // });
    }
  }, []);

  useEffect(() => {
    if (iti) {
      if (!mobile) {
        iti.setCountry("LK");
        iti.setNumber(`+94`);
        setMobile(`+94`);
      } else if (mobile) {
        iti.setNumber(mobile);
      }
    }
  }, [iti, mobile, data]);

  useEffect(() => {
    const inputElement = inputRef.current;
    if (inputElement) {
      inputElement.addEventListener("countrychange", function () {
        const selectedCountryData = iti?.getSelectedCountryData();
        const dialCode = selectedCountryData?.dialCode;
        if (dialCode) {
          iti.setNumber(`+${dialCode}`);
        }
      });
    }
  }, [iti]);

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newMobile = e.target.value;

    // Ensure that the plus sign is maintained
    if (!newMobile.startsWith("+")) {
      setMobile(`+${newMobile}`);
    } else {
      setMobile(newMobile);
    }

    if (iti) {
      iti.setNumber(newMobile);
    }

  };

  return (
    <>
      <input
        ref={inputRef}
        id={id}
        type="tel"
        className="form-control border border-2 py-2 system-control "
        value={mobile}
        onChange={handleInputChange}
        onBlur={handleBlur}
      />
    </>
  );
};

export default PhoneNumberInput;

// import React, { useEffect, useRef } from 'react';
// import intlTelInput from 'intl-tel-input';

// const PhoneNumberInput = ({ id, mobile, setMobile, data }: any) => {
//   const inputRef = useRef<HTMLInputElement | null>(null);

//   useEffect(() => {
//     const input = inputRef.current;
//     if (input) {
//       const iti = intlTelInput(input, {
//         utilsScript: "/intl-tel-input/js/utils.js?1690975972744",
//         geoIpLookup: function (callback) {
//           fetch("https://ipapi.co/json")
//             .then((res) => res.json())
//             .then((geoData) => {
//               const countryCode = data?.country_code || geoData.country_code;
//               callback(countryCode);
//             })
//             .catch(() => callback("us"));
//         },
//       });

//       input.addEventListener("countrychange", function () {
//         const selectedCountryData = iti.getSelectedCountryData();
//         const phoneNumber = "+" + selectedCountryData.dialCode;
//         input.value = phoneNumber;
//         setMobile(phoneNumber);
//       });

//       if (!mobile && !data?.alternative_phone) {
//         iti.setCountry('us');
//         iti.setNumber(``);
//       } else if (mobile) {
//         iti.setNumber(`${mobile}`);
//       } else if (data?.country_code) {
//         iti.setCountry(data.country_code);
//       }
//     }
//   }, [data, mobile]);

//   return (
//     <input
//       ref={inputRef}
//       id={id}
//       type="tel"
//       className="form-control border border-2 py-2 system-control font-bold"
//       value={mobile}
//       onChange={(e) => setMobile(e.target.value)}
//     />
//   );
// }

// export default PhoneNumberInput;

// import React, { useEffect, useRef, useState } from 'react';
// import intlTelInput from 'intl-tel-input';

// const PhoneNumberInput = ({ id, mobile, setMobile, data }: any) => {
//   const inputRef = useRef<HTMLInputElement | null>(null);
//   const [initialized, setInitialized] = useState(false);

//   useEffect(() => {
//     const input = inputRef.current;
//     if (input && !initialized) {
//       const iti = intlTelInput(input, {
//         utilsScript: "/intl-tel-input/js/utils.js?1690975972744",
//         geoIpLookup: function (callback) {
//           fetch("https://ipapi.co/json")
//             .then((res) => res.json())
//             .then((geoData) => {
//               const countryCode = data?.country_code || geoData.country_code;
//               callback(countryCode);
//             })
//             .catch(() => callback("us"));
//         },
//       });

//       input.addEventListener("countrychange", function () {
//         const selectedCountryData = iti.getSelectedCountryData();
//         const phoneNumber = "+" + selectedCountryData.dialCode;
//         input.value = phoneNumber;
//         setMobile(phoneNumber); // Update the mobile state when the country changes
//       });

//       if (!mobile && !data?.alternative_phone) {
//         iti.setCountry('us');
//         iti.setNumber(``);
//       } else if (mobile) {
//         iti.setNumber(`${mobile}`);
//       } else if (data?.country_code) {
//         iti.setCountry(data.country_code);
//       }

//       setInitialized(true);
//     }
//   }, [data, mobile, initialized]);

//   return (
//     <input
//       ref={inputRef}
//       id={id}
//       type="tel"
//       className="form-control border border-2 py-2 system-control font-bold"
//       value={mobile}
//       onChange={(e) => setMobile(e.target.value)}
//     />
//   );
// }

// export default PhoneNumberInput;

// import React, { useEffect } from 'react';
// import intlTelInput from 'intl-tel-input';

// const PhoneNumberInput = ({ id, mobile, setMobile, data }: any) => {

//   useEffect(() => {
//     // console.log("data", data);
//     // setMobile("");

//     console.log("mobile", mobile);

//     const input = document.querySelector(`#${id}`) as HTMLInputElement;
//     console.log("input", input);

//     if (input) {
//       const iti = intlTelInput(input, {
//         utilsScript: "/intl-tel-input/js/utils.js?1690975972744",
//         geoIpLookup: function (callback) {
//           fetch("https://ipapi.co/json")
//             .then((res) => res.json())
//             .then((geoData) => {
//               const countryCode = data?.country_code || geoData.country_code;
//               callback(countryCode);
//             })
//             .catch(() => callback("us"));
//         },
//       });
//       console.log("input iti", iti);

//       if (data?.country_code) {
//         const selectedCountryData = iti.getSelectedCountryData();
//         const phoneNumber = "+" + selectedCountryData.dialCode;
//         input.value = phoneNumber;

//         const event = new Event("countrychange", { bubbles: true });
//         input.dispatchEvent(event);
//       }

//       input.addEventListener("countrychange", function () {
//         const selectedCountryData = iti.getSelectedCountryData();
//         const phoneNumber = "+" + selectedCountryData.dialCode;
//         input.value = phoneNumber;
//       });
//     }
//   }, [data, id]);

//   return (
//     <input
//       id={id}
//       type="tel"
//       className="form-control border border-2 py-2 system-control font-bold"
//       value={mobile}
//       onChange={(e) => setMobile(e.target.value)}
//     />
//   );
// }

// export default PhoneNumberInput;

// import React, { useEffect } from 'react';
// import intlTelInput from 'intl-tel-input';

// const PhoneNumberInput = ({ id, mobile, setMobile, data }: any) => {

//   useEffect(() => {
//     const input = document.querySelector(`#${id}`) as HTMLInputElement; // Cast input as HTMLInputElement
//     if (input) {
//       const iti = intlTelInput(input, {
//         utilsScript: "/intl-tel-input/js/utils.js?1690975972744",
//         geoIpLookup: function (callback) {
//           fetch("https://ipapi.co/json")
//             .then((res) => res.json())
//             .then((data) => callback(data.country_code))
//             .catch(() => callback("us"));
//         },
//       });
//       input.addEventListener("countrychange", function () {
//         const selectedCountryData = iti.getSelectedCountryData();
//         const phoneNumber = "+" + selectedCountryData.dialCode;
//         input.value = phoneNumber;
//       });
//     }
//   }, [id, data]);

//   return (
//     <input
//       id={id}
//       type="tel"
//       className="form-control border border-2 py-2 system-control font-bold"
//       value={mobile}
//       onChange={(e) => setMobile(e.target.value)}
//     />
//   );
// }

// export default PhoneNumberInput;

// import React, { useEffect, useRef } from 'react';
// import intlTelInput from 'intl-tel-input';

// const PhoneNumberInput = ({ id, mobile, setMobile, data }: any) => {
//   const inputRef = useRef<HTMLInputElement | null>(null);

//   useEffect(() => {
//     console.log("mobile", mobile);
//     console.log("mobile", data);

//     const input = inputRef.current;
//     if (input) {
//       const iti = intlTelInput(input, {
//         utilsScript: "/intl-tel-input/js/utils.js?1690975972744",
//         geoIpLookup: function (callback) {
//           fetch("https://ipapi.co/json")
//             .then((res) => res.json())
//             .then((geoData) => {
//               const countryCode = data?.country_code || geoData.country_code;
//               callback(countryCode);
//             })
//             .catch(() => callback("us"));
//         },
//       });

//       // If mobile number is provided, set it and trigger country flag update
//       if (mobile) {
//         iti.setNumber(`${mobile}`);
//       } else if (data?.country_code) {
//         // If no mobile number but country code is available, set country code
//         iti.setCountry(data.country_code);
//       }

//       // input.addEventListener("countrychange", function () {
//       //   const selectedCountryData = iti.getSelectedCountryData();
//       //   const phoneNumber = "+" + selectedCountryData.dialCode;
//       //   input.value = phoneNumber;
//       // });
//     }
//   }, [data, mobile]);
// return (
//   <input
//     ref={inputRef}
//     id={id}
//     type="tel"
//     className="form-control border border-2 py-2 system-control font-bold"
//     value={mobile}
//     onChange={(e) => setMobile(e.target.value)}
//   />
// );
// }

// export default PhoneNumberInput;
