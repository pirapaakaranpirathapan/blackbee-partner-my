import Image from 'next/image'
import React, { useState } from 'react'
import Button from 'react-bootstrap/Button'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import Toast from 'react-bootstrap/Toast'
import greentick from '../../public/greentick.svg'

function ToastAlert() {
  const [show, setShow] = useState(false)

  return (
    <>
      <Toast
        onClose={() => setShow(false)}
        show={show}
        delay={3000}
        autohide
        className="bg-white"
        style={{
          width: '40%',
          position: 'absolute',
          right: '10px',
          border: 'none',
        }}>
        <Toast.Header>
          <Image
            src={greentick}
            alt=""
            className="me-2"
            width={20}
            height={20}
          />
          <strong className="me-auto font-semibold text-dark">
            Blackbee (ablackbee@gmail.com) was changed to Editor.
          </strong>
        </Toast.Header>
        {/* <Toast.Body>Woohoo, you're reading this text in a Toast!</Toast.Body> */}
      </Toast>

      <Button onClick={() => setShow(true)}>Show Toast</Button>
    </>
  )
}

export default ToastAlert
