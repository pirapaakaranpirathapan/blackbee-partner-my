import React, { useEffect, useRef, useState } from "react";
import { Select, Space, Tag } from "@arco-design/web-react";

interface PlaceMulticompleteProps {
  className?: string;
  placeholder: string;
  dvalue?: string[];
  onSuggestionClick: (description: string) => void;
  onRemoveItem: (index: number) => void;
}

const PlaceMulticomplete: React.FC<PlaceMulticompleteProps> = ({
  className,
  placeholder,
  onSuggestionClick,
  dvalue = [],
  onRemoveItem,
}) => {
  // const inputRef = useRef<HTMLInputElement | null>(null);
  const suggestionsRef = useRef<HTMLUListElement | null>(null);
  const autocompleteServiceRef =
    useRef<google.maps.places.AutocompleteService | null>(null);

  const [suggestions, setSuggestions] = useState<
    google.maps.places.AutocompletePrediction[]
  >([]);
  const [createSuggestions, setCreateSuggestions] = useState<
    google.maps.places.AutocompletePrediction[]
  >([]);
  const [dropdownOpen, setDropdownOpen] = useState(false);

  useEffect(() => {
    if ( typeof google !== "undefined") {
      const newAutocompleteService =
        new google.maps.places.AutocompleteService();
      autocompleteServiceRef.current = newAutocompleteService;
    }
  }, []);

  const [typeInput, setTypeInput] = useState("");

  const handleInputChange = (value: any) => {
    setTypeInput(value); // Set the input value
    !value && setSuggestions([]);
    setAddLocation(value);
    value && setClearSuggetion(true);
    // If you need to fetch suggestions based on the input value
    if (autocompleteServiceRef.current) {
      const options = {
        input: value,
        types: ["(cities)"],
      };
      autocompleteServiceRef.current.getPlacePredictions(
        options,
        (predictions, status) => {
          if (status === google.maps.places.PlacesServiceStatus.OK) {
            setSuggestions(predictions);
            setDropdownOpen(true);
          } else {
            setSuggestions([]);
            setDropdownOpen(false);
          }
        }
      );
    }
  };

  const handleSuggestionClick = async (
    prediction: google.maps.places.AutocompletePrediction
  ) => {
    prediction && setClearSuggetion(true);
    onSuggestionClick(prediction?.description);
    // setDefaultLocation(prediction?.description);
    // setLocation(prediction?.description);
  };
  //Clear selected option in input box----------------------------------------------------------------//
  const clearSelection = () => {
    onSuggestionClick("");
    setSuggestions([]);
    setAddLocation("");
    setCreateSuggestions([]);
  };
  const [clearSuggetion, setClearSuggetion] = useState(false);
  useEffect(() => {
    if (clearSuggetion) {
      setSuggestions([]);
      setCreateSuggestions([]);
    }
  }, [clearSuggetion]);
  const Option = Select.Option;

  //Add New Item other than API REsult----------------------------------------------------------------//
  const addItem = (location: any) => {
    setClearSuggetion(false);
    if (location) {
      const customPrediction: google.maps.places.AutocompletePrediction = {
        description: location,
        place_id: "custom_place_id",
        reference: "custom_reference",
        structured_formatting: {
          main_text: location,
          main_text_matched_substrings: [
            { length: location.length, offset: 0 },
          ],
          secondary_text: "custom_secondary_text",
        },
        terms: [{ offset: 0, value: location }],
        types: ["locality", "political", "geocode"],
        matched_substrings: [{ length: location.length, offset: 0 }],
      };
      // setSuggestions([...suggestions, customPrediction]);
      setCreateSuggestions([customPrediction]);
      handleSuggestionClick(customPrediction);
    }
  };
  const [addLocation, setAddLocation] = useState("");
  const addNew = (location: any) => {
    addItem(location);
    setAddLocation("");
  };
  return (
    <div >
 
      <Select
        mode="multiple"
        placeholder={placeholder}
        notFoundContent={null}
        showSearch
        defaultValue={dvalue}
        allowClear
        onClear={clearSelection}
        onSearch={handleInputChange}
        filterOption={false}
        getPopupContainer={(node) => {
          const container = node.parentElement;
          if (container) {
            return container;
          }
          // Return a default element if needed
          // For example, you can return document.body as the fallback
          return document.body;
        }}
      >
        {createSuggestions.map((prediction) => (
          <Option
            key={prediction.place_id}
            value={prediction.description}
            onClick={() => handleSuggestionClick(prediction)}
         
          >
               &nbsp;  {prediction.description}
          </Option>
        ))}
        {suggestions.map((prediction) => (
          <Option
            key={prediction.place_id}
            value={prediction.description}
            onClick={() => handleSuggestionClick(prediction)}
        
          >
          &nbsp; {prediction.description}
          </Option>
        ))}
                {addLocation ? (
          <li
            role="option"
            aria-selected="false"
            className="arco-select-option-wrapper"
            onClick={() => addNew(addLocation)}
          >
            <span className="arco-select-option">
              <div className="d-flex gap-1 align-items-center d-flex gap-1 align-items-center  font-bold  px-2 system-text-primary ">
                <span className="font-normal">Create</span>
                {`"${addLocation}"`}
              </div>
            </span>
          </li>
        ) : null}
      </Select>
    </div>
  );
};

export default PlaceMulticomplete;
