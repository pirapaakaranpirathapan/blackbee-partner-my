import Image from "next/image";
import React, { useContext, useEffect, useRef, useState } from "react";
import ellipsissvg from "../../public/ellipsissvg.svg";
import { getLabel } from "@/utils/lang-manager";
import { useRouter } from "next/router";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";
import AlertModal from "./AlertModal";
import FullScreenModal from "./FullScreenModal";
import MemorialCreatePage from "../Modal/MemorialCreatePage";
import { Button } from "react-bootstrap";
import ArcoSelectAndCreate from "../Arco/ArcoSelectAndCreate";
import { integer } from "aws-sdk/clients/lightsail";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import {
  createMemorialPage,
  getAllMemorialPages,
  memorialPageSelector,
} from "@/redux/memorial-pages";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import { Notification } from "@arco-design/web-react";
import { noticeSelector, patchNoticeGroupPage } from "@/redux/notices";
import {
  LabeledValue,
  OptionInfo,
} from "@arco-design/web-react/es/Select/interface";
import { useApiErrorHandling } from "../Others/useApiErrorHandling";
import GroupAddPages from "../Cards/GroupAddPages";

const ProfileMoreDropdown = () => {
  const [showProfileMore, setShowProfileMore] = useState(false);
  const [addPages, setAddPages] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [selectedValues, setSelectedValues] = useState<string[]>([]);
  const [defaultNames, setDefaultNames] = useState<string[]>([]);
  const [response, setResponse] = useState("");
  const [groupId, setGroupId] = useState<string[]>([]);
  const [addSearchValue, setAddSearchValue] = useState("");
  const [updateError, setUpdateError] = useState("");

  const popupRef = useRef<HTMLDivElement>(null);
  const router = useRouter();
  const { query } = router;
  const path = router.pathname;
  const subtab = router.query.s;
  const allowedPaths = ["/notices/[id]"];
  const { handleSave, NoticeData } = useContext(NoticeDataContext);
  const { PageData } = useContext(PageDataContext);
  const Data = NoticeData || PageData;
  const initialLanguage = NoticeData?.initialLanguage || PageData?.initialLanguage
  const dataType = (NoticeData && "notices") || (PageData && "memorials");
  const actionBtnRef = useRef<HTMLButtonElement>(null);
  const dispatch = useAppDispatch();
  const [response1, setResponse1] = useState("");

  const { data: memorialData } = useAppSelector(memorialPageSelector);

  useEffect(() => {
    if (addPages) {
      setSelectedValues([])
    }
  }, [addPages]);


  useEffect(() => {
    function handleClickOutside(event: MouseEvent) {
      if (
        popupRef.current &&
        !popupRef.current.contains(event.target as Node)
      ) {
        setShowProfileMore(false);
      }
    }
    function handlePopstate() {
      setShowProfileMore(false);
    }
    window.addEventListener('popstate', handlePopstate);
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      window.removeEventListener('popstate', handlePopstate);
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [popupRef]);

  const getHref = () => {
    if (dataType === "memorials") {
      return `/${dataType}/${query?.id}?t=about&s=page-manager#`;
    } else if (dataType === "notices") {
      return `/${dataType}/${query?.id}?t=home&s=Customer+%26+Manager#`;
    } else {
      return "";
    }
  };
  const getHrefSetting = () => {
    if (dataType === "memorials") {
      return `/${dataType}/${query?.id}?t=about&s=settings`;
    } else if (dataType === "notices") {
      return `/${dataType}/${query?.id}?g=s&t=home&s=Notice+Settings`;
    } else {
      return "";
    }
  };


  useEffect(() => {
    const uuidsArray = Array.isArray(Data?.pages) && Data?.pages.map((item: { uuid: any }) => item.uuid);
    if (uuidsArray) {
      setGroupId(uuidsArray);
    }
  }, [Data]);

  const handlePerson = () => {
    setAddPages(true);
  };

  // create memorial
  const handleCreate = (
    personName: string,
    salutation: integer,
    clientId: string,
    countryId: string,
    pageTypeId: string,
    relationship: string,
    dod: string,
    dob: string,
    deathPlace: string
  ) => {
    // setsubmitButtonAddDethPersonEnabled(true);
    createPage(
      personName,
      // salutationId,
      salutation,
      clientId,
      countryId,
      pageTypeId,
      relationship,
      dod,
      dob,
      deathPlace
    );
  };

  const createPage = (
    personName: any,
    // salutationId: any,
    salutation: integer,
    clientId: any,
    countryId: any,
    pageTypeId: string,
    relationship: any,
    dod: any,
    dob: any,
    deathPlace: any
  ) => {
    const CreateData = {
      name: personName,
      page_type_id: pageTypeId,
      salutation_id: salutation,
      client_id: clientId,
      relationship_id: relationship,
      // salutation: salutation,
      dod: dod,
      dob: dob,
      death_location: deathPlace,
      country_id: countryId,
    };
    dispatch(
      createMemorialPage({
        activePartner: ACTIVE_PARTNER,
        activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
        CreateData,
      })
    ).then((response: any) => {
      dispatch(
        getAllMemorialPages({
          active_partner: ACTIVE_PARTNER,
          active_service_provider: ACTIVE_SERVICE_PROVIDER,
          page: 1,
        })
      );
      setResponse(response);

      // setMemorialId(newMemorialUuid);
      const memorialResponseData = response?.payload?.data?.data;
      // setMemorialDatasCreate(memorialResponseData);
      if (response.payload.success === true) {
        const newMemorialUuid = response.payload?.data?.data?.uuid;
        const createuuid = [newMemorialUuid, ...selectedValues];
        const createUuidUniqueArray = [...createuuid, ...groupId];
        setSelectedValues(createUuidUniqueArray);
        // setMemorialId(newMemorialUuid);
        const newName = response.payload?.data?.data?.name;
        const updatedDefaultNames = [newName, ...defaultNames];

        setDefaultNames(updatedDefaultNames);

        setIsModalVisible(false);

        // setMemorialCreate(false);
        Notification.success({
          title: "Memorial has been created successfully",
          duration: 3000,
          content: undefined,
          style: {
            zIndex: 1000,
          },
        });
      } else {
        // setsubmitButtonAddDethPersonEnabled(false);
        Notification.error({
          title: "Failed to  create memorial",
          duration: 3000,
          content: undefined,
          style: {
            zIndex: 1000,
          },
        });
      }
    });
  };
  const active_id = NoticeData?.uuid;
  const handleUpdatePage = () => {
    const updateData = {
      pages: selectedValues,
    };
    dispatch(
      patchNoticeGroupPage({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_id: active_id,
        updatedData: updateData,
      })
    ).then(async (response: any) => {
      setResponse1(response);
      if (response.payload.success == true) {
        handleSave();
        dispatch(
          getAllMemorialPages({
            active_partner: ACTIVE_PARTNER,
            active_service_provider: ACTIVE_SERVICE_PROVIDER,
            page: 1,
          })
        );
        Notification.success({
          title: "Successfully updated",
          duration: 3000,
          content: undefined,
        });
        setAddPages(false);
      } else {
        (response.payload.code != 422) && Notification.error({
          title: "Failed",
          duration: 3000,
          content: undefined,
        });
      }
    });
  };

  const handleOptionChange = (selectedValues: string[]) => {
    if (selectedValues.length === 0) {
      setSelectedValues([]);
      return;
    }
    const updatedSelectedValues = [...selectedValues, ...groupId];
    setSelectedValues(updatedSelectedValues);
  };

  const updateAddSearchValue = (value: React.SetStateAction<string>) => {
    setAddSearchValue(value);
  };

  const handleDeselectValue = (
    value: string | number | LabeledValue,
    option: OptionInfo
  ) => {
    const storedMemorialData = localStorage.getItem("memorialData");

    if (storedMemorialData) {
      let memorialDataArray = JSON.parse(storedMemorialData);

      // Filter out the item with the matching name property
      memorialDataArray = memorialDataArray.filter(
        (item: { name: string }) => item.name !== value
      );

      // Update the local storage with the modified data
      localStorage.setItem("memorialData", JSON.stringify(memorialDataArray));
    }

    setDefaultNames((prevNames) => prevNames.filter((name) => name !== value));
  };

  function handleClear(visible: boolean): void {
    if (visible) {
      setSelectedValues([]);
    }
  }

  function handleDeselect(value: string | number | LabeledValue, option: OptionInfo): void {
    setSelectedValues((prevSelectedValues) => {
      const updatedSelectedValues = prevSelectedValues.filter((uuid) => uuid !== value);
      const updatedWithGroupIds = [...updatedSelectedValues, ...groupId];
      const uniqueValues = Array.from(new Set(updatedWithGroupIds));
      if (uniqueValues.every(uuid => groupId.includes(uuid))) {
        return [];
      }
      return uniqueValues;
    });
  }

  return (
    <>
      <FullScreenModal
        show={isModalVisible}
        onClose={() => {
          setIsModalVisible(false);
        }}
        title={`${getLabel("createMemorialHeading", initialLanguage)}`}
        description={`${getLabel("createMemorialHeadingDescription", initialLanguage)}`}
        footer={
          <Button ref={actionBtnRef} className="btn-system-primary">
            {getLabel("create", initialLanguage)}
          </Button>
        }
      >
        <MemorialCreatePage
          response={response}
          actionBtnRef={actionBtnRef}
          handleCreate={handleCreate}
          addSearchValue={addSearchValue}
        />
      </FullScreenModal>
      <AlertModal
        show={addPages}
        onClose={() => {
          setAddPages(false);
        }}
        title="Add memorial page with this notice"
        description="You can edit this information later as well"
        position="centered"
      >
        <>
          <GroupAddPages
            memorialData={memorialData}
            handleOptionChange={handleOptionChange}
            setIsModalVisible={setIsModalVisible}
            isModalVisible={isModalVisible}
            defaultNames={defaultNames}
            updateAddSearchValue={updateAddSearchValue}
            handleUpdatePage={handleUpdatePage}
            handleClear={handleClear}
            handleDeselect={handleDeselect}
            handleDeselectValue={handleDeselectValue}
            response1={response1} />
        </>
      </AlertModal>
      <div className="arco-dropdown">
        <div className="profile-More-dropdown" ref={popupRef}>
          <div
            className="rounded-circle d-flex justify-content-center align-items-center system-filter-secondary pointer"
            style={{ width: "34px", height: "34px" }}
            onClick={() => {
              setShowProfileMore(!showProfileMore);
            }}
          >
            <Image src={ellipsissvg} alt="" />
          </div>
          {showProfileMore && (
            <div className={`${allowedPaths.includes(path) ? "profile-More-content" : "profile-More-content-normal"}`}>
              {allowedPaths.includes(path) ? (
                <li className="rounded font-110 font-normal">
                  <a
                    href={getHrefSetting()}
                    className={`text-decoration-none  ${subtab === "Notice Settings"
                      ? "active-system font-bold"
                      : "text-dark "
                      }`}
                  >
                    {getLabel("noticeSettings", initialLanguage)}
                  </a>
                </li>
              ) : (
                <li className="rounded font-110 font-normal">
                  <a
                    href={getHrefSetting()}
                    className={`text-decoration-none  ${subtab === "settings"
                      ? "active-system font-bold"
                      : "text-dark "
                      }`}
                  >
                    {getLabel("pageSettings", initialLanguage)}
                  </a>
                </li>
              )}
              {allowedPaths.includes(path) ? (
                <li className="rounded font-110 font-normal">
                  <a
                    href={getHref()}
                    className={`text-decoration-none  ${subtab === "Customer & Manager"
                      ? "active-system font-bold"
                      : "text-dark "
                      }`}
                  >
                    {getLabel("pageManagers", initialLanguage)}
                  </a>
                </li>
              ) : (
                <></>
              )}
              <li className="rounded font-110 font-normal">
                {getLabel("recordHistory", initialLanguage)}
              </li>
              <li className="rounded font-110 font-normal ">
                {getLabel("clearCache", initialLanguage)}
              </li>
              {allowedPaths.includes(path) && Data.is_single === true ? (
                <li
                  className="rounded font-110 font-normal "
                  onClick={handlePerson}
                >
                  {getLabel("convertToGroupNotice", initialLanguage)}
                </li>
              ) : (
                <></>
              )}
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default ProfileMoreDropdown;
