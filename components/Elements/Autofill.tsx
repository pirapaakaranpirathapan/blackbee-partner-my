import React, { useEffect, useState } from "react";

interface Option {
  id: string;
  name: string;
}

interface AutocompleteProps {
  options: any[];
  defaultValue: string;
  onChange: (value: string) => void;
  placeholder: string;
  nameValue: string;
  defaultValue1: string;
}

const Autocomplete: React.FC<AutocompleteProps> = ({
  options,
  defaultValue,
  onChange,
  placeholder,
  nameValue,
  defaultValue1,
}) => {


  const [value, setValue] = useState("");

  const [filteredOptions, setFilteredOptions] = useState<Option[]>([]);
  const [showOptions, setShowOptions] = useState(false); // State variable to control whether to show the suggestion options
  useEffect(() => {
    if (options && options.length > 0) {
      let option = options.find((x) => x.id === defaultValue);

      // defaultValue

      if (option) {
        handleOptionClick(option);
      }
    }
  }, [defaultValue, options]);

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const inputValue = event.target.value;
    setValue(inputValue);
    // Filter the options based on the input value and nameValue parameter
    const filtered = options?.filter((option: any) =>
      option[nameValue]?.toLowerCase().includes(inputValue.toLowerCase())
    );
    setFilteredOptions(filtered);
    setShowOptions(true); // Show the suggestion options when the input value changes
  };
  // useEffect(() => {

  //   console.log('defaultValue', defaultValue);
  //   setValue(defaultValue);
  // }, [defaultValue]);
  useEffect(() => {
    setValue(defaultValue);
    if (options && options.length > 0) {
      let option = options.find((x) => x.id === defaultValue);
      if (option) {
        setValue(option[nameValue]);
        setValue(defaultValue);
      }
    }
  }, [defaultValue, options]);

  const handleOptionClick = (option: any) => {

    setValue(option[nameValue]);
    onChange(option.id);
    setShowOptions(false);
  };

  return (
    <div className="suggestion-autocomplete">
      <input
        className="form-control"
        type="text"
        placeholder={placeholder}
        value={value}
        onChange={handleInputChange}
      />
      <div className="suggestion-list">
        <ul className="suggestion-options">
          {value && filteredOptions && filteredOptions.length > 0 && showOptions && (
            <>
              {filteredOptions.map((option: any) => (
                <li
                  key={option.id}
                  className="suggestion-option"
                  onClick={() => handleOptionClick(option)}
                >
                  {option[nameValue]}
                </li>
              ))}
            </>
          )}
        </ul>
      </div>
    </div>
  );
};

export default Autocomplete;
