import { useEffect, useRef, useState } from 'react';
import { Select, Spin, Avatar } from '@arco-design/web-react';
import { useAppDispatch, useAppSelector } from '@/redux/store/hooks';
import { getAllMemorialPages, memorialPageSelector } from '@/redux/memorial-pages';
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from '@/config/constants';

function MultiSelectTool() {
    const [fetching, setFetching] = useState(false);
    const refCanTriggerLoadMore = useRef(true);
    const dispatch = useAppDispatch();

    const [options, setOptions] = useState([]);
    const [searchTerm, setSearchTerm] = useState('');
    const { data: memorialData } = useAppSelector(memorialPageSelector);
    const Data = memorialData.data
    console.log("memorialData", memorialData);


    const loadData = () => {
        dispatch(
            getAllMemorialPages({
                active_partner: ACTIVE_PARTNER,
                active_service_provider: ACTIVE_SERVICE_PROVIDER,
                page: 1,
            })
        ).then(() => {
            // setLoading(false);
        });
    };

    useEffect(() => {
        loadData();
    }, []);


    const filterOptions = (inputValue: string) => {
        const filteredData = Data?.filter((item: any) =>
            `${item.name}`.toLowerCase().includes(inputValue.toLowerCase())
        );

        setOptions(
            filteredData.map((user: any) => ({
                label: (
                    <div className='multitool-dropdown'>
                        {/* <Avatar size={24}>
                            <img alt='avatar' src={user?.photo_url} />
                        </Avatar> */}
                        {`${user?.name}`}
                    </div>
                ),
                // value: user.email,
            }))
        );
    };

    const handleSearch = (inputValue: string) => {
        setSearchTerm(inputValue);
        if (inputValue.trim() === '') {
            setOptions([]); // Clear options when search term is empty
            return;
        }
        filterOptions(inputValue);
    };

    const fetchMoreData = () => {
        // Your logic to fetch more data from Redux goes here
        // Update the 'options' state with the new data
    };

    // const popupScrollHandler = (element: { scrollTop: any; scrollHeight: any; clientHeight: any }) => {
    //     const { scrollTop, scrollHeight, clientHeight } = element;
    //     const scrollBottom = scrollHeight - (scrollTop + clientHeight);

    //     if (scrollBottom < 10) {
    //         if (!fetching && refCanTriggerLoadMore.current) {
    //             fetchMoreData();
    //             refCanTriggerLoadMore.current = false;
    //         }
    //     } else {
    //         refCanTriggerLoadMore.current = true;
    //     }
    // };

    return (
        <div className=''>
            <Select
                // style={{
                //     border: "1px solid red", borderRadius: "4px", outline: "none", boxShadow: "none", fontSize: '13px',
                // }}
                className="select-multi-tool"
                mode='multiple'
                options={options}
                placeholder='Search or Create death person’s profile page'
                filterOption={false}
                notFoundContent={
                    fetching ? (
                        <div
                            style={{
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}
                        >
                            <Spin style={{ margin: 12 }} />
                        </div>
                    ) : null
                }
                onSearch={handleSearch}
            // onPopupScroll={popupScrollHandler}
            />
        </div>

    );
}

export default MultiSelectTool;
