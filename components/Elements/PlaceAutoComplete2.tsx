import React, { useEffect, useRef, useState } from "react";
import arrowdown from "./../../public/arrowdownI.svg";
import search from "./../../public/searchI.svg";
import Image from "next/image";
interface PlaceAutocompleteProps {
  //onPlaceSelect: (place: google.maps.places.PlaceResult) => void;
  className?: string;
  placeholder: string;
  dvalue?: string;
  onSuggestionClick: (description: string) => void;
}
const PlaceAutocomplete2: React.FC<PlaceAutocompleteProps> = ({
  className,
  placeholder,
  onSuggestionClick,
  dvalue,
}) => {
  //onPlaceSelect,
  const inputRef = useRef<HTMLInputElement | null>(null);
  const suggestionsRef = useRef<HTMLUListElement | null>(null);
  const autocompleteServiceRef =
    useRef<google.maps.places.AutocompleteService | null>(null);

  const [suggestions, setSuggestions] = useState<
    google.maps.places.AutocompletePrediction[]
  >([]);
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [defaultLocation, setDefaultLocation] = useState(dvalue);
  useEffect(() => {
    setDefaultLocation(dvalue);
    if (inputRef.current && typeof google !== "undefined") {
      const newAutocompleteService =
        new google.maps.places.AutocompleteService();
      autocompleteServiceRef.current = newAutocompleteService;
    }
  }, []);
  useEffect(() => {
    setDefaultLocation(dvalue);
  }, [dvalue]);

  useEffect(() => {
    const handleOutsideClick = (event: MouseEvent) => {
      setClick(false);
      if (
        suggestionsRef.current &&
        !suggestionsRef.current.contains(event.target as Node)
      ) {
        setDropdownOpen(false);
      }
    };
    document.addEventListener("mousedown", handleOutsideClick);
    return () => {
      document.removeEventListener("mousedown", handleOutsideClick);
    };
  }, []);
  const [value, setValue] = useState("");
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSelectedSuggestionIndex(0);
    const newValue = event.target.value;
    setValue(newValue);
    onSuggestionClick(newValue);
    setDefaultLocation(newValue);
    setDefaultLocation(newValue);
    if (autocompleteServiceRef.current && inputRef.current) {
      const input = inputRef.current;
      const options = {
        input: input.value,
      };
      autocompleteServiceRef.current.getPlacePredictions(
        options,
        (predictions, status) => {
          if (status === google.maps.places.PlacesServiceStatus.OK) {
            setSuggestions(predictions);
            setDropdownOpen(true);
          } else {
            setSuggestions([]);
            setDropdownOpen(false);
          }
        }
      );
    }
  };
  const handleSuggestionClick = async (
    prediction: google.maps.places.AutocompletePrediction
  ) => {
    if (autocompleteServiceRef.current) {
      const placeResult = await new Promise<google.maps.places.PlaceResult>(
        (resolve) => {
          const request = {
            placeId: prediction.place_id,
            fields: ["geometry", "formatted_address"],
          };
          const service = new google.maps.places.PlacesService(
            inputRef.current!
          );
          service.getDetails(request, (place, status) => {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
              resolve(place);
            } else {
              resolve({} as google.maps.places.PlaceResult);
            }
          });
        }
      );
      //  onPlaceSelect(placeResult);
      inputRef.current!.value = prediction.description;
      setSuggestions([]);
      setDropdownOpen(false);
    }
    // onSuggestionClick(prediction?.['structured_formatting']?. ['secondary_text']);
    // setDefaultLocation(prediction?.['structured_formatting']?. ['secondary_text']);
    onSuggestionClick(
      prediction?.description.split(",").slice(-2).join(",").trim()
    );
    setDefaultLocation(
      prediction?.description.split(",").slice(-2).join(",").trim()
    );
  };

  useEffect(() => {
    if (suggestionsRef.current && inputRef.current) {
      suggestionsRef.current.style.width = inputRef.current.offsetWidth + "px";
    }
  }, [suggestions]);
  const [selectedSuggestionIndex, setSelectedSuggestionIndex] = useState(0);
  const handleMouseEnter = (index: number) => {
    setSelectedSuggestionIndex(index);
  };

  const handleMouseLeave = () => {
    setSelectedSuggestionIndex(-1);
  };

  const handleArrowKey = (event: React.KeyboardEvent) => {
    if (suggestions.length === 0) return;

    if (event.key === "ArrowUp") {
      event.preventDefault();
      setSelectedSuggestionIndex((prevIndex) =>
        prevIndex > 0 ? prevIndex - 1 : suggestions.length - 1
      );
    } else if (event.key === "ArrowDown") {
      event.preventDefault();
      setSelectedSuggestionIndex((prevIndex) =>
        prevIndex < suggestions.length - 1 ? prevIndex + 1 : 0
      );
    } else if (event.key === "Enter" && selectedSuggestionIndex !== -1) {
      event.preventDefault();
      handleSuggestionClick(suggestions[selectedSuggestionIndex]);
    }
  };
  const [click, setClick] = useState(false);
  return (
    <div style={{ position: "relative" }}>
      <input
        ref={inputRef}
        type="text"
        placeholder={placeholder}
        className={`ofl ${className}`}
        onChange={handleInputChange}
        value={defaultLocation}
        onKeyDown={handleArrowKey}
        onClick={() => setClick(true)}
      />
      {dropdownOpen && suggestions.length > 0 && value && (
        <ul
          ref={suggestionsRef}
          className="suggestions"
          style={{
            position: "absolute",
            zIndex: 1,
            listStyle: "none",
            padding: 0,
          }}
        >
          {suggestions.map((prediction, index) => (
            <li
              key={prediction.place_id}
              onClick={() => handleSuggestionClick(prediction)}
              style={{
                cursor: "pointer",
                padding: "8px",
                background:
                  selectedSuggestionIndex === index ? "#E8E8E8" : "transparent",
              }}
              onMouseEnter={() => handleMouseEnter(index)}
              onMouseLeave={handleMouseLeave}
            >
              {prediction.description}
            </li>
          ))}
        </ul>
      )}
      <div className="place-group-eye">
        {/* <Image
        src={click?search:arrowdown}
        width={click?16:11}
        height={<div className="arco-select-arrow-icon"><svg fill="none" stroke="currentColor" stroke-width="4" viewBox="0 0 48 48" aria-hidden="true" focusable="false" className="arco-icon arco-icon-down"><path d="M39.6 17.443 24.043 33 8.487 17.443"></path></svg></div>16:11}
        className=""
        alt={""}
      /> */}

        {!click ? (
          <div className="arco-select-arrow-icon">
            <svg
              fill="none"
              stroke="currentColor"
              stroke-width="4"
              viewBox="0 0 48 48"
              aria-hidden="true"
              focusable="false"
              className="arco-icon arco-icon-down"
            >
              <path d="M39.6 17.443 24.043 33 8.487 17.443"></path>
            </svg>
          </div>
        ) : (
          <div className="arco-select-search-icon">
            <svg
              fill="none"
              stroke="currentColor"
              stroke-width="4"
              viewBox="0 0 48 48"
              aria-hidden="true"
              focusable="false"
              className="arco-icon arco-icon-search"
            >
              <path d="M33.072 33.071c6.248-6.248 6.248-16.379 0-22.627-6.249-6.249-16.38-6.249-22.628 0-6.248 6.248-6.248 16.379 0 22.627 6.248 6.248 16.38 6.248 22.628 0Zm0 0 8.485 8.485"></path>
            </svg>
          </div>
        )}
      </div>
    </div>
  );
};
export default PlaceAutocomplete2;
