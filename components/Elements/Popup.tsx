import React, { useState, useRef, useEffect } from 'react';

interface PopupProps {
  children: React.ReactNode;
}

function Popup({ children }: PopupProps) {
  const [isOpen, setIsOpen] = useState(false);
  const popupRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    function handleClickOutside(event: MouseEvent) {
      if (popupRef.current && !popupRef.current.contains(event.target as Node)) {
        setIsOpen(false);
      }
    }

    document.addEventListener('mousedown', handleClickOutside);

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [popupRef]);

  return (
    <>
      {isOpen && (
        <div className="popup-container" ref={popupRef}>
          {children}
        </div>
      )}
    </>
  );
}

export default Popup;
