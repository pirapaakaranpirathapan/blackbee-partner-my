import Image from 'next/image';
import maleo from '../../public/maleo.jpg';
import { Select } from '@arco-design/web-react';
import { useEffect, useState } from 'react';
import { useAppDispatch } from '@/redux/store/hooks';
import { getAllMemorialPages } from '@/redux/memorial-pages';
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from '@/config/constants';

const Option = Select.Option;

function ArcoSelectSingle({ Data, onSelectChange, defaultValue }: any) {
    console.log("Data", Data);

    // const [defaultData, setDefaultData] = useState<string[]>([]);
    // console.log("profiledefaultData", defaultData);

    const originalOptions = Data?.data;
    // useEffect(() => {
    //     const defaultNames = defaultValue.map((item: { name: any }) => item?.name);
    //     setDefaultData(defaultNames);
    // }, [defaultValue]);

    const handleSelectChange = (selectedValues: string[]) => {
        const selectedUuids = selectedValues.map(selectedName => {
            const matchingOption = Data.find((option: any) => option.first_name === selectedName);
            return matchingOption ? matchingOption.id : null;
        });
        // setDefaultData(selectedValues);
        onSelectChange(selectedUuids);
    };

    return (
        <div>
            <Select
                placeholder="It's a testing arcoselect"
                style={{ marginRight: 20 }}
                mode='multiple'
                showSearch
                allowClear
                // defaultValue={[]}
                // value={defaultData}
                onChange={handleSelectChange}
            >
                {Data?.map((item: any, index: any) => (
                    <Option value={item.first_name} key={index} extra={item.uuid}>
                        <div className="d-flex gap-2">
                            <div className='avatar-container'>
                                <Image
                                    src={item?.photo_url ? item?.photo_url : maleo}
                                    alt=""
                                    className="image rounded-circle"
                                    width={200}
                                    height={200}
                                />
                            </div>
                            {item.first_name}
                        </div>
                    </Option>
                ))}
            </Select>
        </div>
    );
}

export default ArcoSelectSingle;


