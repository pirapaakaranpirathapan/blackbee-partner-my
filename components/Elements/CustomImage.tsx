import React, { useState, useEffect, useRef } from "react";
import BG from "./../../public/BG.svg";
import lgrey from "./../../public/lgrey.jpg";
import brokeimg from "./../../public/broken-file.svg";
import NextImage from "next/image";

interface CustomImageProps {
  className: string;
  alt: string;
  src: string;
  onClick?: () => void;
  width?: number;
  height?: number;
  divClass?: string;
  ref?: React.Ref<HTMLImageElement>;
}

const CustomImage: React.FC<CustomImageProps> = ({
  className,
  alt,
  src,
  onClick,
  width,
  height,
  divClass,
  ref,
}: CustomImageProps) => {
  const [showSingleImage, setShowSingleImage] = useState(false);
  const [isValidImage, setIsValidImage] = useState(true);
  const imageRef = useRef<HTMLImageElement | null>(null);
  useEffect(() => {
    const img = new Image();
    img.src = src;

    img.onload = () => {
      setShowSingleImage(true);
      setIsValidImage(true);
    };

    img.onerror = () => {
      setIsValidImage(false);
    };
  }, [src]);

  return (
    <>
      {isValidImage ? (
        <div className={divClass}>
          <NextImage
            src={showSingleImage ? src : lgrey}
            alt={alt}
            className={className}
            onClick={onClick}
            width={width}
            height={height}
            ref={imageRef}
          />
        </div>
      ) : (
        <div className={divClass}>
          {/* <NextImage
            src={lgrey}
            alt="Fallback Image"
            className={className}
            onClick={onClick}
            width={width}
            height={height}
            ref={imageRef}
          /> */}
          <div
            className="position-absolute top-50 start-50 translate-middle ps-1 z-1 text-dark text-truncate d-flex align-items-center"
            style={{ maxWidth: width }}
          >
            {/* <NextImage
              src={brokeimg}
              alt=""
              width={15}
              height={15}
              className=""
            /> */}
            {/* <div className="ps-1">{alt}</div> */}
          </div>
        </div>
      )}
    </>
  );
};

export default CustomImage;






// import React, { useState, useEffect, useRef } from "react";
// import BG from "./../../public/BG.svg";
// import brokeimg from "./../../public/broken-file.svg";
// import NextImage from "next/image";

// interface CustomImageProps {
//   className: string;
//   alt: string;
//   src: string;
//   onClick?: () => void;
//   width?: number;
//   height?: number;
//   divClass?: string;
//   ref?: React.Ref<HTMLImageElement>;
// }

// const CustomImage: React.FC<CustomImageProps> = ({
//   className,
//   alt,
//   src,
//   onClick,
//   width,
//   height,
//   divClass,
//   ref,
// }: CustomImageProps) => {
//   const [showSingleImage, setShowSingleImage] = useState(false);
//   const [isValidImage, setIsValidImage] = useState(true);
//   const imageRef = useRef<HTMLImageElement | null>(null);
//   useEffect(() => {
//     const img = new Image();
//     img.src = src;

//     img.onload = () => {
//       setShowSingleImage(true);
//       setIsValidImage(true);
//     };

//     img.onerror = () => {
//       setIsValidImage(false);
//     };
//   }, [src]);

//   return (
//     <>
//       {!isValidImage ? (
//         <div className={divClass}>
//           <NextImage
//             // src={showSingleImage ? src : BG}
//             src={showSingleImage ? src : ""}
//             alt={alt}
//             className={className}
//             onClick={onClick}
//             width={width}
//             height={height}
//             ref={imageRef}
//           />
//         </div>
//       ) : (
//         <div className={divClass}>
//           <NextImage
//             // src={showSingleImage ? src : BG}
//             src={showSingleImage ? src : ""}
//             alt={alt}
//             className={className}
//             onClick={onClick}
//             width={width}
//             height={height}
//             ref={imageRef}
//           />
//           <div
//             className="position-absolute top-50 start-50 translate-middle ps-1 z-1 text-dark text-truncate d-flex align-items-center"
//             style={{ maxWidth: width }}
//           >
//             {/* <NextImage
//               src={brokeimg}
//               alt=""
//               width={15}
//               height={15}
//               className=""
//             /> */}
//             {/* <div className="ps-1">{alt}</div> */}
//           </div>
//         </div>
//       )}
//     </>
//   );
// };

// export default CustomImage;

























// import React, { useState, useEffect, useRef } from "react";
// import BG from "./../../public/BG.svg";
// import brokeimg from "./../../public/broken-file.svg";
// import NextImage from "next/image";

// interface CustomImageProps {
//   className: string;
//   alt: string;
//   src: string;
//   onClick?: () => void;
//   width?: number;
//   height?: number;
//   divClass?: string;
//   ref?: React.Ref<HTMLImageElement>;
// }

// const CustomImage: React.FC<CustomImageProps> = ({
//   className,
//   alt,
//   src,
//   onClick,
//   width,
//   height,
//   divClass,
//   ref,
// }: CustomImageProps) => {
//   const [showSingleImage, setShowSingleImage] = useState(false);
//   const [isValidImage, setIsValidImage] = useState(true);
//   const imageRef = useRef<HTMLImageElement | null>(null);
//   useEffect(() => {
//     const img = new Image();
//     img.src = src;

//     img.onload = () => {
//       setShowSingleImage(true);
//       setIsValidImage(true);
//     };

//     img.onerror = () => {
//       setIsValidImage(false);
//     };
//   }, [src]);

//   return (
//     <>
//       {isValidImage ? (
//         <div className={divClass}>
//           <NextImage
//             src={showSingleImage ? src : BG}
//             alt={alt}
//             className={className}
//             onClick={onClick}
//             width={width}
//             height={height}
//             ref={imageRef}
//           />
//         </div>
//       ) : (
//         <div className={divClass}>
//           <NextImage
//             src={BG}
//             alt="Fallback Image"
//             className={className}
//             onClick={onClick}
//             width={width}
//             height={height}
//             ref={imageRef}
//           />
//           <div
//             className="position-absolute top-50 start-50 translate-middle ps-1 z-1 text-dark text-truncate d-flex align-items-center"
//             style={{ maxWidth: width }}
//           >
//             {/* <NextImage
//               src={brokeimg}
//               alt=""
//               width={15}
//               height={15}
//               className=""
//             /> */}
//             <div className="ps-1">{alt}</div>
//           </div>
//         </div>
//       )}
//     </>
//   );
// };

// export default CustomImage;
