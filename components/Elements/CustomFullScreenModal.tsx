import Image from "next/image";
import React, { useEffect, useState } from "react";
import { Button, CloseButton, Modal } from "react-bootstrap";
import cross from "../../public/cross.svg";

type Props = {
  show: boolean;
  onClose: Function;
  size?: "sm" | "lg" | "xl";
  position?:
    | "centered"
    | "full-screen"
    | "left"
    | "right"
    | "bottom"
    | "top"
    | "left-bottom"
    | "right-bottom"
    | "left-top"
    | "right-top"
    | "center";
  title?: string;
  description?: string;
  deleteLabelText?: string;
  deleteLabel?: any;
  showCloseButton?: boolean;
  footerButtonText?: any;
  footer?: any;
  footerText?: any;
  children?: any;
  footerCenter?: boolean;
  customSize?: any;
};

const CustomFullScreenModal: React.FC<Props> = ({
  show,
  onClose,
  size,
  position,
  title,
  description,
  deleteLabelText,
  children,
  showCloseButton,
  footer,
  footerCenter,
  footerText,
  footerButtonText,
  deleteLabel,
  customSize,
}) => {
  const [showModal, setShowModal] = useState(show);
  const [positionModal, setPositionModal] = useState(position);

  const handleClose = () => {
    onClose();
    setShowModal(false);
  };

  useEffect(() => {
    setShowModal(show);
  }, [show]);

  useEffect(() => {
    setPositionModal(position);
  }, [position]);

  return (
    <Modal
      dialogClassName={`modal-dialog-centered ${customSize}`}
      show={showModal}
      onHide={handleClose}

      size={size}
      fullscreen={positionModal == "full-screen" ? true : undefined}
      aria-labelledby="contained-modal-title-vcenter"
    >
      {title != undefined && (
        <div className="">
          <div>
            <Modal.Header className="px-4 py-3 d-flex align-items-center">
              {title != undefined && (
                <Modal.Title id="contained-modal-title-vcenter ">
                  <div className=" ">
                    <h5 className="mb-0 font-220 font-bold ">{title}</h5>
                    {description && (
                      <p className="font-100 font-normal text-secondary mb-0 c-modal-title " >
                        {description}
                      </p>
                    )}
                  </div>
                </Modal.Title>
              )}
            </Modal.Header>
          </div>
        </div>
      )}
      <Modal.Body className="px-4">{children}</Modal.Body>
      {footer !== undefined && (
        <Modal.Footer
          className={
            !footerCenter
              ? "my-modal-footer-c px-4 p-0 d-flex"
              : "d-flex  align-items-center justify-content-center mb-3 "
          }
        >
          <div className="">{footer}</div>
          <button
            type="button"
            className="btn px-1 px-sm-4 btn-md border shadow-sm font-110 text-dark system-secondary-light btn-system-border  "
            onClick={handleClose}
          >
            Close
          </button>
        </Modal.Footer>
      )}
      {footerText !== undefined && (
        <Modal.Footer className="my-modal-footer-c px-4 pb-3 p-0 mb-3 mt-2">
          <div>{footerText}</div>
        </Modal.Footer>
      )}
    </Modal>
  );
};
export default CustomFullScreenModal;
