import Image from 'next/image';
import React, { useEffect, useState } from 'react';
import { Button, CloseButton, Modal } from 'react-bootstrap';
import cross from '../../public/cross.svg'


type Props = {
  show: boolean;
  onClose: Function;
  size?: 'sm' | 'lg' | 'xl';
  position?:
  | 'centered'
  | 'full-screen'
  | 'left'
  | 'right'
  | 'bottom'
  | 'top'
  | 'left-bottom'
  | 'right-bottom'
  | 'left-top'
  | 'right-top'
  | 'center';
  title?: string;
  description?: string;
  deleteLabelText?: string;
  deleteLabel?: any;
  showCloseButton?: boolean;
  footerButtonText?: any;
  footer?: any;
  children?: any;
  isSmallModal?: boolean
};

const AlertModalOne: React.FC<Props> = ({
  show,
  onClose,
  size,
  position,
  title,
  description,
  deleteLabelText,
  children,
  showCloseButton,
  footer,
  footerButtonText,
  deleteLabel,
  isSmallModal
}) => {
  const [showModal, setShowModal] = useState(show);
  const [positionModal, setPositionModal] = useState(position);

  const handleClose = () => {
    onClose();
    setShowModal(false);
  };

  useEffect(() => {
    setShowModal(show);
  }, [show]);

  useEffect(() => {
    setPositionModal(position);
  }, [position]);

  return (
    <Modal
      dialogClassName={`modal-dialog-${position}`}
      show={showModal}
      size={size}
      className={isSmallModal == true ? 'small-modal' : ''}
      onHide={handleClose}
      fullscreen={positionModal == 'full-screen' ? true : undefined}
      aria-labelledby="contained-modal-title-vcenter"
    >
      <div className="">
        <div>
          <Modal.Header className="px-4 py-3 d-flex align-items-center">
            {title != undefined && (
              <Modal.Title id="contained-modal-title-vcenter ">
                <div className="">
                  <h5 className="mb-0 font-150 font-bold ">{title}</h5>
                  {description && (
                    <p className="font-100 font-normal text-secondary mb-0">
                      {description}
                    </p>
                  )}
                </div>
              </Modal.Title>
            )}
            <div
              className="pointer custom-close-button d-flex align-items-center justify-content-center"
              onClick={handleClose}>
              <Image src={cross} alt="world-icon" className="" />
            </div>
          </Modal.Header>
        </div>
      </div>
      <Modal.Body className="p-0 pt-3">
        {/* {title != undefined && (
          <Modal.Title id="contained-modal-title-vcenter" className="px-4">
            <div className="">
              <h1 className="mb-0 font-bold font-150">{title}</h1>
              {description && (
                <p className="font-90 text-secondary mb-3">{description}</p>
              )}
            </div>
          </Modal.Title>
        )} */}
        {/* <hr /> */}
        <div className="px-4 py-0">{children}</div>
      </Modal.Body>
      {footer !== undefined && (
        <Modal.Footer className="my-modal-footer px-4 pb-4 pt-0">
          <div>{footer}</div>
          <div className="text-danger pointer">{deleteLabel}</div>
        </Modal.Footer>
      )}
    </Modal>
  );
};

export default AlertModalOne;
