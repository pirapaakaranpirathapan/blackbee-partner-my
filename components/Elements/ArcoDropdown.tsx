import React, { useEffect, useState } from "react";
import { Select, Space } from "@arco-design/web-react";
import Image from "next/image";
import maleo from "../../public/maleo.jpg";

const Option = Select.Option;

const ArcoDropdown = ({ Data, onSelectChange, defaultValue }: any) => {
  const [defaultData, setDefaultData] = useState<string[]>([]);
  const [select, setSelect] = useState<string[]>([]);
  // console.log("defaultData", defaultData);

  const originalOptions = Data;

  useEffect(() => {
    const defaultNames = defaultValue.map(
      (item: { first_name: any }) => item?.first_name
    );
    const defaultNamesString = defaultNames.join(", ");
    setDefaultData(defaultNamesString);
  }, [defaultValue]);

  const handleSelectChange = (selectedValues: string[]) => {
    const selectValueName = Array.isArray(selectedValues)
      ? selectedValues
      : [selectedValues];
    const selectedUuids = selectValueName?.map((selectedName) => {
      const matchingOption = originalOptions.find(
        (option: any) => option.first_name === selectedName
      );
      return matchingOption ? matchingOption.id : null;
    });

    setDefaultData(selectedValues);
    onSelectChange(selectedUuids);
  };

  return (
    <div>
      <Select
        placeholder="Select from your customer database or create as new customer"
        style={{ width: "100%" }}
        showSearch={{
          retainInputValue: true,
        }}
        defaultValue={[]}
        // value={defaultData}
        value={defaultData.length > 0 ? defaultData : undefined}
        onChange={handleSelectChange}
        getPopupContainer={(node) => {
          const container = node.parentElement;
          if (container) {
            return container;
          }
          // Return a default element if needed
          // For example, you can return document.body as the fallback
          return document.body;
        }}
      >
        {Array.isArray(Data) &&
          Data?.map((item: any, index: any) => (
            <Option value={item.first_name} key={index} extra={item.uuid}>
              <div className="d-flex gap-2 align-items-center">
                <div className="avatar-container">
                  <Image
                    src={item?.photo_url ? item?.photo_url : maleo}
                    alt=""
                    className="image rounded-circle"
                    width={200}
                    height={200}
                  />
                </div>
                {item.first_name}
              </div>
            </Option>
          ))}
      </Select>
    </div>
  );
};

export default ArcoDropdown;
