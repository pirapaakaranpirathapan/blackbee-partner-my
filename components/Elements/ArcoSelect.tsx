import Image from 'next/image';
import maleo from '../../public/maleo.jpg';
import { Select } from '@arco-design/web-react';
import { useEffect, useState } from 'react';
import { useAppDispatch } from '@/redux/store/hooks';
import { getAllMemorialPages } from '@/redux/memorial-pages';
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from '@/config/constants';

const Option = Select.Option;

function ArcoSelect({ memorialData, onSelectChange, defaultValue }: any) {
    const originalOptions = memorialData?.data;

    // Initialize defaultData with default values, if provided
    const [defaultData, setDefaultData] = useState<string[]>(() => {
        const defaultNames = defaultValue?.map((item: { name: any }) => item?.name);
        return Array.from(new Set(defaultNames)); // Convert Set to an array
    });

    console.log("defaultData", defaultData);


    // Handle changes in the defaultValue prop
    useEffect(() => {
        if (defaultValue && defaultValue.length > 0) {
            const newDefaultNames = defaultValue.map((item: { name: any }) => item?.name);
            setDefaultData((prevDefaultData) => {
                const combinedData = [...prevDefaultData, ...newDefaultNames];
                return Array.from(new Set(combinedData));
            });
        }
    }, [defaultValue]);

    const handleSelectChange = (selectedValues: string[]) => {
        setDefaultData(selectedValues); // Update defaultData with selected values
        const selectedUuids = selectedValues.map(selectedName => {
            const matchingOption = originalOptions.find((option: any) => option.name === selectedName);
            return matchingOption ? matchingOption.uuid : null;
        });
        onSelectChange(selectedUuids);
    };

    return (
        <div>
            <Select
                placeholder="Search or Create death person’s profile page(Multiple)"
                style={{ marginRight: 20 }}
                mode='multiple'
                showSearch
                allowClear
                value={defaultData}
                onChange={handleSelectChange}
                getPopupContainer={(node) => {
                    const container = node.parentElement;
                    if (container) {
                        return container;
                    }
                    // Return a default element if needed
                    // For example, you can return document.body as the fallback
                    return document.body;
                }}

            >
                {originalOptions?.map((item: any, index: any) => (
                    <Option value={item.name} key={index} extra={item.uuid}>
                        <div className="d-flex gap-2 align-items-center">
                            <div className='avatar-container'>
                                <Image
                                    src={item?.photo_url ? item?.photo_url : maleo}
                                    alt=""
                                    className="image rounded-circle"
                                    width={200}
                                    height={200}
                                />
                            </div>

                            {item.name}

                        </div>
                    </Option>
                ))}
            </Select>
        </div>
    );
}

export default ArcoSelect;




























// import Image from 'next/image';
// import maleo from '../../public/maleo.jpg';
// import { Select } from '@arco-design/web-react';
// import { useEffect, useState } from 'react';
// import { useAppDispatch } from '@/redux/store/hooks';
// import { getAllMemorialPages } from '@/redux/memorial-pages';
// import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from '@/config/constants';

// const Option = Select.Option;

// function ArcoSelect({ memorialData, onSelectChange, defaultValue, profileUuid }: any) {
//     const originalOptions = memorialData?.data;
//     // Initialize defaultData with default values, if provided
//     const [defaultData, setDefaultData] = useState<string[]>();
//     console.log("defaultData", defaultData);

//     useEffect(() => {
//         if (Array.isArray(originalOptions) && Array.isArray(profileUuid)) {
//             const names = profileUuid.map((uuid) => {
//                 const matchedOption = originalOptions.find((option) => option.uuid === uuid);
//                 return matchedOption ? matchedOption.name : '';
//             });
//             setDefaultData(names);
//         }
//     }, [originalOptions, profileUuid]);

//     const handleSelectChange = (selectedValues: string[]) => {
//         // setDefaultData(selectedValues);
//         const selectedUuids = selectedValues.map(selectedName => {
//             const matchingOption = originalOptions.find((option: any) => option.name === selectedName);
//             return matchingOption ? matchingOption.uuid : null;
//         });
//         onSelectChange(selectedUuids);
//     };

//     return (
//         <div>
//             <Select
//                 placeholder="Search or Create death person’s profile page(Multiple)"
//                 style={{ marginRight: 20 }}
//                 mode='multiple'
//                 showSearch
//                 allowClear
//                 value={defaultData}
//                 onChange={handleSelectChange}
//             >
//                 {originalOptions?.map((item: any, index: any) => (
//                     <Option value={item.name} key={index} extra={item.uuid}>
//                         <div className="d-flex gap-2 align-items-center">
//                             <div className='avatar-container'>
//                                 <Image
//                                     src={item?.photo_url ? item?.photo_url : maleo}
//                                     alt=""
//                                     className="image rounded-circle"
//                                     width={200}
//                                     height={200}
//                                 />
//                             </div>

//                             {item.name}

//                         </div>
//                     </Option>
//                 ))}
//             </Select>
//         </div>
//     );
// }

// export default ArcoSelect;



































// import Image from 'next/image';
// import maleo from '../../public/maleo.jpg';
// import { Select } from '@arco-design/web-react';
// import { useEffect, useState } from 'react';
// import { useAppDispatch } from '@/redux/store/hooks';
// import { getAllMemorialPages } from '@/redux/memorial-pages';
// import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from '@/config/constants';

// const Option = Select.Option;

// function ArcoSelect({ memorialData, onSelectChange, defaultValue }: any) {

//     const originalOptions = memorialData?.data;

//     // Initialize defaultData with default values, if provided
//     const [defaultData, setDefaultData] = useState<string[]>(() => {
//         const defaultNames = defaultValue?.map((item: { name: any }) => item?.name);
//         return Array.from(new Set(defaultNames)); // Convert Set to an array
//     });

//     // Handle changes in the defaultValue prop
//     useEffect(() => {
//         if (defaultValue && defaultValue.length > 0) {
//             const newDefaultNames = defaultValue.map((item: { name: any }) => item?.name);
//             setDefaultData((prevDefaultData) => {
//                 const combinedData = [...prevDefaultData, ...newDefaultNames];
//                 return Array.from(new Set(combinedData));
//             });
//         }
//     }, [defaultValue]);

//     const handleSelectChange = (selectedValues: string[]) => {
//         setDefaultData((prevDefaultData) => Array.from(new Set([...prevDefaultData, ...selectedValues])));

//         const selectedUuids = selectedValues.map(selectedName => {
//             const matchingOption = originalOptions.find((option: any) => option.name === selectedName);
//             return matchingOption ? matchingOption.uuid : null;
//         });
//         onSelectChange(selectedUuids);
//     };

//     return (
//         <div>
//             <Select
//                 placeholder="Search or Create death person’s profile page(Multiple)"
//                 style={{ marginRight: 20 }}
//                 mode='multiple'
//                 showSearch
//                 allowClear
//                 defaultValue={[]}
//                 value={defaultData}
//                 onChange={handleSelectChange}
//             >
//                 {originalOptions?.map((item: any, index: any) => (
//                     <Option value={item.name} key={index} extra={item.uuid}>
//                         <div className="d-flex gap-2">
//                             <div className='avatar-container'>
//                                 <Image
//                                     src={item?.photo_url ? item?.photo_url : maleo}
//                                     alt=""
//                                     className="image rounded-circle"
//                                     width={200}
//                                     height={200}
//                                 />
//                             </div>
//                             {item.name}
//                         </div>
//                     </Option>
//                 ))}
//             </Select>
//         </div>
//     );
// }

// export default ArcoSelect;






// const [defaultData, setDefaultData] = useState<string[]>([]);
// console.log("defaultValuememorial", defaultValue);

// const originalOptions = memorialData?.data;

// useEffect(() => {
//     const defaultNames = defaultValue?.map((item: { name: any }) => item?.name);
//     setDefaultData(defaultNames);
//     // setDefaultData((prevdefaultData) => [
//     //     ...prevdefaultData,
//     //     defaultNames,
//     // ]);
// }, [defaultValue]);

// const handleSelectChange = (selectedValues: string[]) => {
//     const selectedUuids = selectedValues.map(selectedName => {
//         const matchingOption = originalOptions.find((option: any) => option.name === selectedName);
//         return matchingOption ? matchingOption.uuid : null;
//     });
//     // setDefaultData(selectedValues);
//     setDefaultData(selectedValues);
//     onSelectChange(selectedUuids);
// };
