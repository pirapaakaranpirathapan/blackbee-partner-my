import React from "react";
import { Placeholder } from "react-bootstrap";

interface CustomTextBoxProps {
  value: string;
  placeholder?: string;
  maxLength: number;
  description?: string;
  onChange: (e: React.ChangeEvent<HTMLTextAreaElement>, field: string) => void;
}
const CustomTextBox: React.FC<CustomTextBoxProps> = ({
  value,
  maxLength,
  placeholder,
  description,
  onChange,
}: CustomTextBoxProps) => {
  return (
    <>
      <div className="form-control border border-2 system-control resize-none p-0">
        <textarea
          className="resize-none ps-2 pt-2"
          placeholder={placeholder||""}
          rows={2}
          maxLength={maxLength}
          aria-label="With textarea"
          defaultValue={value}
          onChange={(e) =>onChange(e, description||"")}
        ></textarea>
        <div className="status-bar pe-1 ">
          <div id="message-count" className="m-0 system-muted-2">
            {value?.length}/{maxLength}
          </div>
        </div>
      </div>
    </>
  );
};
export default CustomTextBox;
