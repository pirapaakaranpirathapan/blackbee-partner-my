import React, { useEffect, useRef, useState } from "react";
import { Select } from "@arco-design/web-react";
interface PlaceAutocompleteProps {
  className?: string;
  placeholder: string;
  dvalue?: string;
  onSuggestionClick: (description: string) => void;
}
const PlaceAutocomplete: React.FC<PlaceAutocompleteProps> = ({
  className,
  placeholder,
  onSuggestionClick,
  dvalue,
}) => {
  const autocompleteServiceRef =
    useRef<google.maps.places.AutocompleteService | null>(null);
  const [suggestions, setSuggestions] = useState<
    google.maps.places.AutocompletePrediction[]
  >([]);
  const [defaultLocation, setDefaultLocation] = useState(
    (dvalue && dvalue.trim() === "") || "" ? undefined : dvalue
  );
  const [addLocation, setAddLocation] = useState("");
  const [selectedlocation, setLocation] = useState("");
  useEffect(() => {
    setDefaultLocation(dvalue);
    if (typeof google !== "undefined") {
      const newAutocompleteService =
        new google.maps.places.AutocompleteService();
      autocompleteServiceRef.current = newAutocompleteService;
    }
  }, []);
  useEffect(() => {
    setDefaultLocation(dvalue);
  }, [dvalue]);
  //Search with input in google auto place complete api------------------------------//
  const handleInputChange = (value: any) => {
    const newValue = value;
    setDefaultLocation(newValue);
    setAddLocation(newValue);
    !value && setSuggestions([]);
    newValue && setClearSuggetion(true);
    if (autocompleteServiceRef.current) {
      const options = {
        input: value,
        types: ["(cities)"], //types: ["(regions)"]
      };
      autocompleteServiceRef.current.getPlacePredictions(
        options,
        (predictions, status) => {
          if (status === google.maps.places.PlacesServiceStatus.OK) {
            setSuggestions(predictions);
          } else {
            setSuggestions([]);
          }
        }
      );
    }
  };
  //Handle option click---------------------------------------------------------------------//
  const handleSuggestionClick = async (
    prediction: google.maps.places.AutocompletePrediction
  ) => {
    prediction && setClearSuggetion(true);
    onSuggestionClick(prediction?.description);
    setDefaultLocation(prediction?.description);
    setLocation(prediction?.description);
  };
  const [clearSuggetion, setClearSuggetion] = useState(false);
  useEffect(() => {
    if (clearSuggetion) {
      setSuggestions([]);
    }
  }, [clearSuggetion]);
  useEffect(() => {
    if (!addLocation && suggestions.length === 0) {
      addItem(dvalue);
    }
  }, [suggestions]);
  const addNew = (location: any) => {
    addItem(location);
    setAddLocation("");
  };
  //Add New Item other than API REsult----------------------------------------------------------------//
  const addItem = (location: any) => {
    setClearSuggetion(false);
    if (location) {
      const customPrediction: google.maps.places.AutocompletePrediction = {
        description: location,
        place_id: "custom_place_id",
        reference: "custom_reference",
        structured_formatting: {
          main_text: location,
          main_text_matched_substrings: [
            { length: location.length, offset: 0 },
          ],
          secondary_text: "custom_secondary_text",
        },
        terms: [{ offset: 0, value: location }],
        types: ["locality", "political", "geocode"],
        matched_substrings: [{ length: location.length, offset: 0 }],
      };
      // setSuggestions([...suggestions, customPrediction]);
      setSuggestions([customPrediction]);
      handleSuggestionClick(customPrediction);
    }
  };
  //On click action on Select input box----------------------------------------------------------//
  const Option = Select.Option;
  const onClick = (value: any) => {
    // !selectedlocation && !dvalue && setSuggestions([]);
    setSuggestions([]);
    // customLocation ? handleInputChange(value) : handleInputChange(dvalue);
    handleInputChange(value);
    setAddLocation("");
  };
  //Clear selected option in input box----------------------------------------------------------------//
  const clearSelection = () => {
    onSuggestionClick("");
    setSuggestions([]);
    setLocation("");
    setDefaultLocation("");
    setPlace("");
  };
  const [place, setPlace] = useState("");

  useEffect(() => {
    setClearSuggetion(true);
    onSuggestionClick(place);
    setDefaultLocation(place);
    setLocation(place);
  }, [place]);
  return (
    <div style={{ position: "relative" }}>
      <Select
        placeholder={placeholder}
        showSearch
        value={defaultLocation || undefined}
        onSearch={handleInputChange}
        onClick={() => onClick(dvalue)}
        filterOption={false}
        onClear={clearSelection}
        allowClear={true}
        notFoundContent={null}
        onChange={(e) => setPlace(e)}
        getPopupContainer={(node) => {
          const container = node.parentElement;
          if (container) {
            return container;
          }
          // Return a default element if needed
          // For example, you can return document.body as the fallback
          return document.body;
        }}
      >
        {/* {addLocation && (
          <Option onClick={() => addNew(addLocation)} value={addLocation} >
            <span style={{ color: "#F3804E" }}>Create as a new place </span>
            {addLocation}
          </Option>
        )} */}
        {suggestions.map((prediction) => (
          <Option
            key={prediction.place_id}
            onClick={() => handleSuggestionClick(prediction)}
            value={prediction?.description}
          >
            {prediction?.description}
          </Option>
        ))}
               {addLocation ? (
          <li
            role="option"
            aria-selected="false"
            className="arco-select-option-wrapper"
            onClick={() => addNew(addLocation)}
          >
            <span className="arco-select-option">
              <div className="d-flex gap-1 align-items-center d-flex gap-1 align-items-center  font-bold  px-2 system-text-primary ">
                <span className="font-normal">Create</span>
                {`"${addLocation}"`}
              </div>
            </span>
          </li>
        ) : null}
      </Select>
    </div>
  );
};
export default PlaceAutocomplete;
