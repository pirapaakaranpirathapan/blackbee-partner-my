import React from 'react';

interface ButtonGroupProps {
  name: string;
  onClick: () => void;
 
}

const ButtonGroup: React.FC<ButtonGroupProps> = ({ name, onClick }) => {
  const handleClick = async () => {
    await onClick(); // Wait for the onClick function to complete
  };
  return (
    <div className="d-flex align-items-center">
      <div>
        <button
          type="button"
          className="btn-system-primary pointer btn btn-primary"
          onClick={handleClick}
        >
          {name}
        </button>
      </div>
      {/* <div className="text-danger pointer ps-4" onClick={}>
        <a>close</a>
      </div> */}
    </div>
  );
};

export default ButtonGroup;
