import { getLabel } from "@/utils/lang-manager";
import React from "react";

export interface Props {
  page: number;
  totalPages: number;
  handlePagination: (page: number) => void;
  initialLanguage: string
}

export const PaginationComponent: React.FC<Props> = ({
  page,
  totalPages,
  handlePagination,
  initialLanguage
}) => {
  return (
    <div className="pagination-cont">
      <div className="text-center">
        {page !== 1 && (
          <>
            <button
              onClick={() => handlePagination(page - 1)}
              type="button"
              className="btn px-2 px-sm-3 py-1 py-sm-2 px-md-3  border pagination-button system-control "
            >
              {`${getLabel("previous", initialLanguage)}`}
            </button>
            &nbsp;
          </>
        )}

        {totalPages !== 1 && (
          <>
            <button
              onClick={() => handlePagination(1)}
              type="button"
              className={
                page == 1
                  ? "btn px-2 px-sm-3 py-1 py-sm-2  system-filter-secondary"
                  : "btn px-2 px-sm-3 py-1 py-sm-2 "
              }
            >
              {1}
            </button>
            &nbsp;
          </>
        )}

        {page > 3 && <span>... &nbsp;</span>}

        {page === totalPages && totalPages > 3 && (
          <>
            <button
              onClick={() => handlePagination(page - 2)}
              type="button"
              className="btn px-2 px-sm-3 py-1 py-sm-2 "
            >
              {page - 2}
            </button>
            &nbsp;
          </>
        )}
        {page > 2 && (
          <>
            <button
              onClick={() => handlePagination(page - 1)}
              type="button"
              className="btn px-2 px-sm-3 py-1 py-sm-2 "
            >
              {page - 1}
            </button>
            &nbsp;
          </>
        )}
        {page !== 1 && page !== totalPages && (
          <>
            <button
              onClick={() => handlePagination(page)}
              type="button"
              className="btn px-2 px-sm-3 py-1 py-sm-2  system-filter-secondary"
            >
              {page}
            </button>
            &nbsp;
          </>
        )}
        {page < totalPages - 1 && (
          <>
            <button
              onClick={() => handlePagination(page + 1)}
              type="button"
              className="btn px-2 px-sm-3 py-1 py-sm-2 "
            >
              {page + 1}
            </button>
            &nbsp;
          </>
        )}
        {page === 1 && totalPages > 3 && (
          <>
            <button
              onClick={() => handlePagination(page + 2)}
              type="button"
              className="btn px-2 px-sm-3 py-1 py-sm-2 "
            >
              {page + 2}
            </button>
            &nbsp;
          </>
        )}
        {page < totalPages - 2 && <span>... &nbsp;</span>}
        {totalPages !== 1 && (
          <>
            <button
              onClick={() => handlePagination(totalPages)}
              type="button"
              className={
                page == totalPages
                  ? "btn px-2 px-sm-3 py-1 py-sm-2  system-filter-secondary"
                  : "btn px-2 px-sm-3 py-1 py-sm-2  "
              }
            >
              {totalPages}
            </button>
            &nbsp;
          </>
        )}
        {page !== totalPages && totalPages !== 1 && (
          <>
            <button
              onClick={() => handlePagination(page + 1)}
              type="button"
              className="btn px-2 px-sm-3 py-1 py-sm-2 px-md-3  border  pagination-button system-control "
            >
              {`${getLabel("next", initialLanguage)}`}
            </button>
          </>
        )}
      </div>
    </div>
  );
};

export const Pagination = PaginationComponent;
