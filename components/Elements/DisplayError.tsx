import React from 'react';

type Props = {
    error: string;
    customClass?:string
  };
const DisplayError: React.FC<Props> = ({error,customClass}) => {
    return (
      <span className={`display-error ${customClass}`} >
        {error}
      </span>
    )
}
export default DisplayError




