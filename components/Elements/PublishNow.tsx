import { DatePicker } from "@arco-design/web-react";
import { useEffect, useState } from "react";
interface PublishProps {
  response: any;
  actionBtnRef: any;
  handlePublish: (updateData: any) => void;
}
const PublishNow = ({
  handlePublish,
  actionBtnRef,
  response,
}: PublishProps) => {
  const [isPublishNow, setIsPublishNow] = useState(false);
  const [isPublisheLater, setIsPublisheLater] = useState(true);
  const [clickstatus, setClickStatus] = useState(false);
  useEffect(() => {
    actionBtnRef.current.onclick = () => {
      setClickStatus(true);
    };
    if (clickstatus) {
        handlePublish(isPublishNow);
      setClickStatus(false);
    }
  }, [clickstatus]);
  return (
    <div className="mt-3 ">
      <>
        <div className="c-font-publish mb-3 ">When you want to publish?</div>
        <div className="d-flex mb-4 align-items-center">
          <div className="col-6 d-flex ">
            <div className="d-flex me-2  justify-content-start black-radio">
              <input
                type="radio"
                name="radio"
                className="form-radio-input pointer "
                checked={isPublishNow}
                onClick={() => {
                  setIsPublishNow(!isPublishNow);
                  setIsPublisheLater(false);
                }}
              />
            </div>
            <div className="font-normal">Publish Now</div>
          </div>
          <div className="col-6 d-flex ">
            <div className="d-flex  me-2 justify-content-start black-radio  ">
              <input
                type="radio"
                name="radio"
                id="test1"
                // name="radio-group"
                className="form-radio-input pointer"
                checked={isPublisheLater}
                onClick={() => {
                  setIsPublisheLater(!isPublisheLater);
                  setIsPublishNow(false);
                }}
              />
            </div>

            <div className="font-normal">Publish Later</div>
          </div>
        </div>

        <div className="c-font-publish mb-2">Schedule the date and time</div>
        <div className="col-8 dateOfDeath">
          <DatePicker
            className="form-control border border-2 system-control py-3 mb-3 "
            defaultValue={""}
            format="DD-MMM-YYYY"
            allowClear={true}
            placeholder={" "}
            // onChange={(date: string) => handleInputDateChange(date, "dateOfDeath")}
            // onClear={()=>setDod("")}
            // allowClear={false}
                       getPopupContainer={(node) => {
              const container = node.parentElement;
              if (container) {
                return container;
              }
              return document.body;
            }}
          />
        </div>
      </>
    </div>
  );
};
export default PublishNow;
