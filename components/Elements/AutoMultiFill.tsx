import React, { useEffect, useState } from "react";

interface AutocompleteProps {
  defaultValue: any[];
  options: any[];
  onChange: (values: string[]) => void;
  placeholder: string;
  nameValue: string;
}

const AutoMulticomplete: React.FC<AutocompleteProps> = ({
  options,
  defaultValue,
  onChange,
  placeholder,
  nameValue,
}) => {


  const [selectedOptions, setSelectedOptions] = useState(defaultValue);
  const [inputValue, setInputValue] = useState("");
  const handleOptionClick = (option: any) => {
    // console.log("optionclick",option);

    if (!selectedOptions.some((selectedOption) => selectedOption.id === option.id)) {
      const updatedOptions = [...selectedOptions, option];
      setSelectedOptions(updatedOptions);
      onChange(updatedOptions.map((opt) => opt.id));
    }
    setInputValue("");
  };

  const handleRemoveOption = (optionId: string) => {
    const updatedOptions = selectedOptions.filter((option) => option.id !== optionId);
    setSelectedOptions(updatedOptions);
    onChange(updatedOptions.map((opt) => opt.id));
  };

  const filteredOptions =
    Array.isArray(options) &&
    options &&
    options.filter((option: any) =>
      option[nameValue]?.toLowerCase().includes(inputValue.toLowerCase())
    );


  // Update selectedOptions when defaultValue changes from the parent
  useEffect(() => {
    setSelectedOptions((prevSelectedOptions) => {
      const updatedOptions = [...prevSelectedOptions];
      if (Array.isArray(defaultValue)) {
        defaultValue.forEach((defaultOption) => {

          if (!updatedOptions.some((selectedOption) => selectedOption.id === defaultOption.id)) {
            updatedOptions.push(defaultOption);
          }
        });
      }
      return updatedOptions;
    });
  }, [defaultValue]);



  return (
    <div className="suggestion-autocomplete">
      <div className="input-container form-control">

        <div className="selected-options">
          {selectedOptions.map((option) => (
            <div key={option.id} className="selected-option">
              {option.name}
              <button
                className="remove-option"
                onClick={() => handleRemoveOption(option.id)}
              >
                &times;
              </button>
            </div>
          ))}
        </div>
        <input
          className="form-control-multi"
          type="text"
          placeholder={placeholder}
          value={inputValue}
          onChange={(event) => setInputValue(event.target.value)}
        />
      </div>
      <div className="suggestion-list">
        <ul className="suggestion-options">
          {inputValue &&
            Array.isArray(filteredOptions) &&
            filteredOptions.length > 0 &&
            filteredOptions.map((option) => (
              <li
                key={option.id}
                className={`suggestion-option ${selectedOptions.some((selectedOption) => selectedOption.id === option.id)
                  ? "selected"
                  : ""
                  }`}
                onClick={() => handleOptionClick(option)}
              >
                {option.name}
              </li>
            ))}
        </ul>
      </div>
    </div>
  );
};

export default AutoMulticomplete;
