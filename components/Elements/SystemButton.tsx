import React from 'react';
import { Button } from 'react-bootstrap';

type Props = {
  btnColor: any
  status?: any
  content?: any
};
const SystemButton: React.FC<Props> = ({ status, content, btnColor }) => {
  let btnType = 'btn-success';

  if (btnColor.toUpperCase() === "PRIMARY") { btnType = "btn-system-primary" }

  return (
    <Button className={`btn ${btnType}`}>
      {content ? content : status}
    </Button>
  )
}
export default SystemButton
