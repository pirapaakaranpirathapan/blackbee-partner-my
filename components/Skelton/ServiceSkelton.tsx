import React from "react";
import ShimmerEffect from "./ShimmerEffect";
const ServiceSkelton = ({ loopTimes }: any) => {
  return (
    <>
      <table className="table "  style={{ cursor: "default", pointerEvents: "none"  }} >
        <tbody className="">
          {Array.from({ length: loopTimes }).map((_, index) => {
            return (
              <tr className="align-middle ">
                <td className="col-4 text-start" style={{ maxWidth: "100px" }}>
                  <div
                    className=" font-bold text-truncate  mb-1 "
                    style={{ maxWidth: "100%" }}
                  >
                    <ShimmerEffect width="80%" height="15px" />
                  </div>
                  <div className="font-80  system-text-success  ">
                    <ShimmerEffect width="60%" height="15px" />
                  </div>
                </td>
                <td className="w-100">
                  <div className="text-dark font-80 text-truncate  mb-1 mx-w1 col-4 col-md-5 col-lg-3 ">
                    <ShimmerEffect width="" height="15px" />
                  </div>
                  <div className="text-dark font-80 text-truncate  mx-w1 col-3 col-md-4 col-lg-2 ">
                    <ShimmerEffect width="" height="15px" />
                  </div>
                </td>
                <td className=" text-end pe-0 pe-md-3">
                  <button
                    type="button"
                    className=" shimmer-button1 shimmer-effect"
                  ></button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
};
export default ServiceSkelton;
