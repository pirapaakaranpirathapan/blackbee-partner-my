import Image from "next/image";
import nextIcon from "../../public/nextIcon.svg";
import ShimmerEffectText from "./ShimmerEffectText";
const RouteSkelton = () => {
  return (
    <div className=" skelton-disable row  ">
      <div className="col-10 m-0 p-0 d-flex">
        <ShimmerEffectText width="50px" height="8px" /><div className="me-1"/>
        <Image src={nextIcon} alt="" className="" /><div className="me-1"/>
        <ShimmerEffectText width="60px" height="8px" /><div className="me-1"/>
        <Image src={nextIcon} alt="" className=" m-0 p-0" /><div className="me-1"/>
        <ShimmerEffectText width="200px" height="8px" />
      </div>
    </div>
  );
};

export default RouteSkelton;
