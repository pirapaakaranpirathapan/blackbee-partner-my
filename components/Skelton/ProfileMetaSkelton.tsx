import React from "react";
import ProfileMoreDropdown from "../Elements/ProfileMoreDropdown";
const ProfileMetaSkelton = () => {
  return (
    <>
      <div
        className="row  py-3  m-0 "
        style={{ cursor: "default", pointerEvents: "none" }}
      >
        <div className="col-md-2 col-2">
          <p className="m-0 font-90 system-text-blue-light shimmer-text mb-1">
            {" "}
            &nbsp;
          </p>
          <p
            className={`m-0 font-semibold shimmer-text
              }`}
          >
            &nbsp;
          </p>
        </div>
        <div className="col-md-2 col-4 ps-md-4">
          <p className="m-0 font-90 system-text-blue-light text-truncate shimmer-text mb-1">
            &nbsp;
          </p>
          <p className="m-0 text-truncate system-text-blue font-semibold shimmer-text ">
            &nbsp;
          </p>
        </div>
        <div className="col-md-2 col-4 p-0">
          <p className="m-0 font-90 system-text-blue-light text-truncate shimmer-text mb-1">
            &nbsp;
          </p>
          <p className="m-0 text-truncate system-text-blue font-semibold shimmer-text ">
            &nbsp;
          </p>
        </div>
        <div className="col-md-2 p-0 d-none d-md-block ps-2">
          <p className="m-0 font-90 system-text-blue-light text-truncate shimmer-text mb-1">
            &nbsp;
          </p>
          <p className="m-0 text-truncate system-text-blue font-semibold shimmer-text">
            &nbsp;
          </p>
        </div>
        <div className="col-md-2 d-none d-md-block">
          <p className="m-0 font-90 system-text-blue-light text-truncate shimmer-text mb-1">
            &nbsp;
          </p>
          <p className="m-0 text-truncate font-semibold shimmer-text">&nbsp;</p>
        </div>{" "}
        <div className="col-md-2 d-none d-md-flex justify-content-end">
          <div
            className="rounded-circle d-flex justify-content-end align-items-center shimmer-text"
            style={{ width: '34px', height: '34px' }}>

          </div>
        </div>{" "}
        {/* <div className="col-md-2 col-2 d-flex justify-content-end ">
          <ProfileMoreDropdown />
        </div> */}
      </div>
    </>
  );
};
export default ProfileMetaSkelton;
