import PhotosAndVideosSkelton from "./PhotosVideosSkelton"
import ShimmerEffectText from "./ShimmerEffectText"

const MemorialPhotoSkelton = () => {
  return (<>
    <div className="mb-4 m-0 page-heading font-bold col-lg-3 col-7 col-md-4 col-sm-1">
      <ShimmerEffectText width="" height="20px" />
    </div>
    <div className="d-flex align-items-center  gap-2 gap-sm-3 flex-wrap mb-3">
      <div className=" font-bold m-0">
        {" "}
        <ShimmerEffectText width="100px" height="20px" />
      </div>
      <button
        type="button"
        className=" d-none d-sm-flex me-3 shimmer-button13 shimmer-effect"
      ></button>
      <button
        type="button"
        className=" d-flex d-sm-none me-3 shimmer-button13-2 shimmer-effect"
      ></button>
    </div>{" "}
    <div className="d-flex flex-wrap gap-2 gap-sm-3 mb-4 justify-content-start">
      <PhotosAndVideosSkelton
        loopTimes={8}
        className={"profile-photo-container"}
      />
    </div>
    {/* skell start */}
    {/* ***********************profile image model start*/}
    {/* ***********************profile image model end */}
    <div className=" d-flex flex-wrap gap-2 gap-sm-3 align-items-center mb-4">
      {/* <div className=" m-0 font-bold ">Uploads </div> */}
      <div className=" m-0 font-bold ">
        {" "}
        <ShimmerEffectText width="100px" height="20px" />{" "}
      </div>
      <div className="d-flex gap-1 gap-sm-1  ">
        <button
          type="button"
          className=" d-none d-sm-flex me-3 shimmer-button14 shimmer-effect"
        ></button>
        <button
          type="button"
          className=" d-none d-sm-flex me-3 shimmer-button14 shimmer-effect"
        ></button>
        <button
          type="button"
          className=" d-flex d-sm-none me-3 shimmer-button14-2 shimmer-effect"
        ></button>
        <button
          type="button"
          className=" d-flex d-sm-none me-3 shimmer-button14-2 shimmer-effect"
        ></button>
      </div>
    </div>
    {/* --------------upload photos-------------- */}
    <div className="d-flex flex-wrap gap-2 gap-sm-3 mb-4 justify-content-start">
      <PhotosAndVideosSkelton
        loopTimes={8}
        className={"upload-image-container"}
      />
    </div>
    <div className="mb-1">
      <div className="m-0 p-0 font-bold mb-1 col-lg-4 col-7 col-md-4 ">
        {" "}
        <ShimmerEffectText width="" height="20px" />
      </div>
      <p className="system-muted1 p-0 m-0 col-lg-6 col-7 col-md-4">
        <ShimmerEffectText width="" height="20px" />
      </p>
    </div>
    <div className=" form-check form-switch mb-2 font-110">
      {/* <input
        className="form-check-input border border-2 system-control pointer"
        type="checkbox"
        id="flexSwitchCheckDefault"
       /> */}
      <label
        className="form-check-label font-90 font-normal ps-1"
        htmlFor="flexSwitchCheckDefault"
      >
        <ShimmerEffectText width="" height="20px" />
      </label>
    </div>
    <div className="d-flex flex-wrap gap-2 gap-sm-3 mb-5 justify-content-start">
      {" "}
      <PhotosAndVideosSkelton
        loopTimes={8}
        className={"upload-image-container"}
      />
    </div>
    {(
      <div className="mb-5">
        <div className="mb-3">
          <div className="m-0 p-0 font-bold col-lg-3 col-7 col-md-4">
            <ShimmerEffectText width="" height="20px" />
          </div>
        </div>
        <div className="d-flex flex-column gap-2">
          <div className="d-flex gap-2">
            <PhotosAndVideosSkelton
              loopTimes={1}
              className={
                "delete-photo-container system-filter-secondary rounded d-flex column gap-2"
              }
            />
          </div>

          {/* {showDeletedData?.data.length >= 15 && (
            <PaginationComponent
              page={page}
              totalPages={Math.ceil(showDeletedData.total / 15)}
              handlePagination={handlePagination}
            />
          )} */}
        </div>
      </div>
    )}
    {/* skelll end */}
  </>)
}

export default MemorialPhotoSkelton;