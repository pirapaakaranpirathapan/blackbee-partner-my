import React from "react";
import ShimmerEffectText from "./ShimmerEffectText";
import draggable from "../../public/draggable.svg";
import Image from "next/image";
import ShimmerEffect from "./ShimmerEffect";

const ContactSkeleton = ({ loopTimes }:any) => {
  return (
    <>
      {Array.from({ length: loopTimes }).map((_, index) => (
        <div className="card border mb-3" key={index}>
          <div className="card-body mx-3 my-2 ps-2 p-0 skelton-disable"  style={{ cursor: "default", pointerEvents: "none"  }}>
            <div className="row">
              <div className="col-7 m-0 p-0">
                <div className="d-flex align-items-start gap-2">
                  <div className="">
                    <Image src={draggable} alt="memorialprofile" />
                  </div>
                  <div className="m-0 text-muted-dark text-truncate col-lg-6 col-10 col-md-10">
                    <ShimmerEffectText width="" height="12px" />
                    <div className="font-normal text-truncate col-lg-12 col-12 col-md-12 mb-2">
                    </div>
                    <span className="font-normal text-truncate col-lg-9 col-8 col-md-9">
                      <ShimmerEffectText width="" height="12px" />
                    </span>
                  </div>
                </div>
              </div>
              <div className="col-5 d-flex justify-content-end align-items-center pe-2">
                <button
                  type="button"
                  className="py-1 px-4 font-90 font-semibold shimmer-effect shimmer-button20"
                ></button>
              </div>
            </div>
          </div>
        </div>
      ))}
    </>
  );
};

export default ContactSkeleton;
