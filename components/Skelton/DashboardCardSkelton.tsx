import Image from "next/image";
import folder from "../../public/folder.png";
import folder2 from "../../public/folder2.png";
import React from "react";
import Link from "next/link";
import ShimmerEffectText from "./ShimmerEffectText";

type Props = {
  content: any;
  // amountsymbol?: string;
  // title?: string;
  // description?: string;
};

const DashboardCardSkelton = ({ loopTimes }: any) => {
  return (
    <div className="d-flex gap-3 flex-wrap dash-child">
      {Array.from({ length: loopTimes }).map((_, index) => (
        <div
          className="skelton-disable"
          style={{ cursor: "default", pointerEvents: "none" }}
        >
          <div className="card border dashboard-card mb-4">
            <div className="card-body">
              <div className="row mb-3 align-items-end">
                <div className="col-3 h-36">
                  <ShimmerEffectText width="40px" height="40px" />
                </div>
                <div className="col-9 d-flex flex-row-reverse card-title  pe-3 text-truncate a-tag-disable1">
                  <ShimmerEffectText width="40px" height="30px" />
                </div>
              </div>
              <h6 className="card-title text-truncate a-tag-disable1 mb-2">
                {" "}
                <ShimmerEffectText width="" height="20px" />
              </h6>
              <div className="">
                <div className="fw-normal system-icon-secondary avoid-overflow-two-lines lh-sm">
                  <div className="mb-1">
                    <ShimmerEffectText width="" height="10px" />
                  </div>
                  <div className="col-md-7 col-lg-7 d-none d-md-block d-lg-block ">
                    {" "}
                    <ShimmerEffectText width="" height="10px" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default DashboardCardSkelton;
