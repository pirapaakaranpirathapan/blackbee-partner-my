import ShimmerEffectText from "./ShimmerEffectText";

const DesignSkelton = () => {
  return (
    <>
      <div className="pe-md-5">
        <div className="mb-4 page-heading font-bold m-0 col-lg-3 col-7 col-md-4 ">
          {" "}
          <ShimmerEffectText width="" height="20px" />
        </div>
        <div className="row  align-items-center  m-0 mb-3 ">
          <div className="col-md-3 col-12  p-0 m-0 font-bold  pb-2 pb-md-0 pe-2  ">
            <ShimmerEffectText width="" height="20px" />
          </div>
          <div className="col-md-9 col-12  p-0 m-0 ">
            <select
              className="form-select border border-2 bg-white p-2 font-normal system-control shimmer-effect"
              id="dropdownMenuLink"
              disabled
            ></select>
          </div>
        </div>
        <div className="row mb-5 m-0">
          <div className="col-lg-3  col-12 d-none d-sm-flex p-0 m-0 "></div>
          <div className="col-lg-9 col-12  p-0">
            <button className=" d-none d-sm-flex me-3 shimmer-button3 shimmer-effect"></button>
            {/*------------------------------------------------------------------------------*/}
          </div>
        </div>
        <div className="row  align-items-start  m-0 mb-5">
          <div className="col-md-3 col-12 p-0 font-bold pb-sm-2 pb-2 pb-md-0 pe-2 ">
            <ShimmerEffectText width="" height="20px" />
          </div>
          <div className="col-md-9 p-0">
            <div className="d-flex gap-3 justify-content-start  text-center flex-wrap ">
              <div className="d-flex flex-column justify-content-center">
                <div className="decoration-img-container rounded d-flex align-items-center justify-content-center system-filter-secondary shimmer-effect  "></div>
                <div className="font-normal font-90 system-text-primary pt-1   ">
                  <ShimmerEffectText width="" height="20px" />
                </div>
              </div>
              <div className="d-flex flex-column justify-content-center">
                <div className="decoration-img-container rounded d-flex align-items-center justify-content-center system-filter-secondary  shimmer-effect"></div>
                <div className="font-normal font-90 system-text-primary pt-1   ">
                  <ShimmerEffectText width="" height="20px" />
                </div>
              </div>
              <div className="d-flex flex-column justify-content-center">
                <div className="decoration-img-container rounded d-flex align-items-center justify-content-center system-filter-secondary shimmer-effect  "></div>
                <div className="font-normal font-90 system-text-primary pt-1   ">
                  <ShimmerEffectText width="" height="20px" />
                </div>
              </div>
              <div className="d-flex flex-column justify-content-center">
                <div className="decoration-img-container rounded d-flex align-items-center justify-content-center system-filter-secondary shimmer-effect "></div>
                <div className="font-normal font-90 system-text-primary pt-1   ">
                  <ShimmerEffectText width="" height="20px" />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row  align-items-start  m-0 mb-5">
          <div className="col-md-3 col-12 p-0 font-bold pb-sm-2 pb-2 pb-md-0 pe-2 ">
            <ShimmerEffectText width="" height="20px" />
          </div>
          <div className="col-md-9 p-0 ">
            <div className="system-text-primary">
              <div className="header-background-container system-filter-secondary rounded shimmer-effect"></div>
              <div className="mt-1 gap-3">
                <div className="font-90 font-normal system-text-primary pointer ">
                  <ShimmerEffectText width="" height="20px" />
                </div>
                {/* <div className="font-90 font-normal system-text-primary pointer pe-2 ">
                    <ShimmerEffectText width="" height="20px" />
                  </div> */}
              </div>
            </div>
          </div>
        </div>
        <div className="row  align-items-start  m-0 mb-5">
          <div className="col-md-3 col-12 p-0 font-bold pb-sm-2 pb-2 pb-md-0 pe-2 ">
            <ShimmerEffectText width="" height="20px" />
          </div>
          <div className="col-md-9 p-0">
            <div className="system-text-primary">
              <div className="page-background-container system-filter-secondary shimmer-effect"></div>
              <div className="mt-1  gap-3">
                <div className="font-90 font-normal system-text-primary pointer ">
                  <ShimmerEffectText width="" height="20px" />
                </div>
                {/* <div className="font-90 font-normal system-text-primary pointer pe-2 ">
                    <ShimmerEffectText width="" height="20px" />
                  </div> */}
              </div>
            </div>
          </div>
        </div>
        <div className="row mb-5 m-0">
          <div className="col-lg-3  col-12 d-none d-sm-flex p-0 m-0 "></div>
          <div className="col-lg-9 col-12  p-0">
            <button
              type="button"
              className="btn  my-1 shimmer-button3 shimmer-effect"
            ></button>
          </div>
        </div>
      </div>
    </>
  );
};

export default DesignSkelton;
