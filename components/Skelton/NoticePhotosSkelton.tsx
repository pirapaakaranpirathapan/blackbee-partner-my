import React from "react";
import PhotosAndVideosSkelton from "./PhotosVideosSkelton";
import NoticeHeadingSkelton from "./NoticeHeadingSkelton";
const NoticePhotosSkelton = () => {
  return (
    <>
      <div className="border bg-white px-4 py-3 pb-4 mb-3">
      < NoticeHeadingSkelton x={3} y={1}/>
        <div className="row my-3 gap-2 gap-sm-0">
          <div className="col-auto">
            <button
              type="button"
              className=" d-none d-sm-flex me-1 shimmer-button13 shimmer-effect"
            ></button>
          </div>
          <div className="col-auto">
            <button
              type="button"
              className=" d-none d-sm-flex me-3 shimmer-button16 shimmer-effect"
            ></button>
          </div>
        </div>

        <div className="d-flex flex-wrap gap-2 gap-sm-3 mb-3 justify-content-start">
          <PhotosAndVideosSkelton
            loopTimes={12}
            className={"upload-image-container"}
          />
        </div>
      </div>
    </>
  );
};
export default NoticePhotosSkelton;
