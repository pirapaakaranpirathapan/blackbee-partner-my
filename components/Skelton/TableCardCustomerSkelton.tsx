import React, { useEffect, useState } from "react";

import ShimmerEffect from "./ShimmerEffect";

const TableCardCustomerSkelton = ({ loopTimes }: any) => {
  return (
    <>
      {Array.from({ length: loopTimes }).map((_, index) => {
        return (
          <div className="card border bg-white mb-3 d-block d-sm-none">
            <div className="card-body">
              <div className="row m-0">
                <div className="col-2  p-0">
                  <div className=" d-flex justify-content-center">
                    <div className=" table-img text-muted font-140 system-text-dark-light d-flex align-items-center justify-content-center rounded-circle shimmer-circle shimmer-effect"></div>
                  </div>
                </div>
                <div className="col-10 ps-3">
                  <div className="mb-2">
                    <div className=" p-0 font-110 font-semibold ">
                      <div className="">
                        <div className="text-truncate p-0 m-0 mb-1">
                          <ShimmerEffect width="" height="20px" />
                        </div>
                      </div>
                    </div>
                    <div className=" p-0 text-dark font-90  text-truncate mb-3">
                      <ShimmerEffect width="" height="15px" />
                    </div>
                  </div>

                  <div className="row m-0">
                    <div className="col-6 p-0 text-secondary font-80 font-thin  align-items-center  justify-content-start mb-1 pe-1">
                      <div className="p-0 m-0">
                        <ShimmerEffect width="" height="15px" />
                      </div>
                    </div>
                    <div className="col-6 ps-1 p-0 text-dark font-90 align-items-center  justify-content-start ">
                      <div className="text-truncate p-0 m-0">
                        <ShimmerEffect width="" height="15px" />
                      </div>
                    </div>
                  </div>

                  {/* <div className="row m-0">
                    <div className="col-6 p-0 text-secondary font-80 font-thin  mb-1 pe-1">
                      <ShimmerEffect width="" height="15px" />
                    </div>
                    <div className=" col-6 p-0 text-dark font-90 text-truncate ps-1">
                      <ShimmerEffect width="" height="15px" />
                    </div>
                  </div>

                  <div className="row m-0 ">
                    <div className="col-6 p-0 text-secondary font-80 font-thin  mb-3 pe-1 ">
                      <ShimmerEffect width="" height="15px" />
                    </div>
                    <div className="col-6 p-0  text-dark font-90 text-truncate ps-1">
                      <ShimmerEffect width="" height="15px" />
                    </div>
                  </div> */}

                  <div>
                    <button
                      type="button"
                      className=" shimmer-button-m shimmer-effect "
                    ></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      })}
    </>
  );
};

export default TableCardCustomerSkelton;
