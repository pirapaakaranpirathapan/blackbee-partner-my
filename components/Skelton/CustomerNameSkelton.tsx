import ShimmerEffect from "./ShimmerEffect";

const CustomerNameSkelton = () => {
  return (
    <>
      <div className="d-flex justify-content-between align-items-center page-heading-container">
        <div
          className="d-flex gap-3 align-items-center page-header"
          style={{ maxWidth: "65%" }}
        >
          <div className="table-img system-text-dark-light rounded-circle ms-0 ms-sm-3 d-flex align-items-center justify-content-center fs-6 fw-semibold text-muted shimmer-effect"></div>
          <div className="" style={{ maxWidth: "65%" }}>
            <h6 className="page-heading font-bold m-0 text-truncate me-3 mb-1">
              <ShimmerEffect width="200px" height="13px" />
            </h6>
            <div className="m-0 text-muted font-80  text-truncate me-3">
              <ShimmerEffect width="100px" height="10px" />
            </div>
          </div>
        </div>
        <div className="ms-a20">
          <button
            type="button"
            className="btn  shimmer-button24 shimmer-effect"
          >
          </button>
        </div>
      </div>
    </>
  );
};
export default CustomerNameSkelton;
