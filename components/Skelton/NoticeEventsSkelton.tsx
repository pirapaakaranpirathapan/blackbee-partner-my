import React from "react";
import ShimmerEffectText from "./ShimmerEffectText";
import NoticeHeadingSkelton from "./NoticeHeadingSkelton";
const NoticeEventsSkelton = () => {
  return (
    <>
      <div className="border bg-white px-4 py-3 mb-3 skelton-disable" style={{ cursor: "default", pointerEvents: "none"  }}>
      < NoticeHeadingSkelton x={8} y={2}/>
        <div className="row mb-3 ">
          <div className="col-12">
            <button
              type="button"
              className=" d-none d-sm-flex me-3 shimmer-button17 shimmer-effect"
            ></button>
          </div>
        </div>
        <div className="row gap-2 p-0 m-0 mb-4">
        {Array.from({ length: 20 }).map((_, index) => (
            <div className="col-auto p-0 m-0 border border-2 px-3 p-1 bg-white d-flex gap-2 rounded pointer">
              <div></div>
              <div className="text-muted py-1">
                <div className="p-o m-0 font-semibold text-truncate mb-1">
                  <ShimmerEffectText width="100px" height="15px" />
                </div>
                <div className="p-o m-0 font-11 font-normal ">
                  <ShimmerEffectText width="80px" height="12px" />
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  );
};
export default NoticeEventsSkelton;
