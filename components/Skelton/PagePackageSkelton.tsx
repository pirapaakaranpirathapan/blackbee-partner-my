import React from "react";
import ShimmerEffect from "./ShimmerEffect";
import Link from "next/link";
import ShimmerEffectText from "./ShimmerEffectText";
const PagePackageSkelton = ({ loopTimes }: any) => {
  return (
    <>
      {Array.from({ length: loopTimes }).map((_, index) => {
        return (
          <>
            <div className="col-lg-7 col-md-8 col-sm-11 col-12 skelton-disable" style={{ cursor: "default", pointerEvents: "none"  }}>
              <div className="card border bg-white mb-2 ">
                <div className="card-body px-md-4 pt-3">
                  <div className="row m-0  ">
                    <div className="col-sm-12 col-md-6 col-lg-6 p-0">
                      <div className="card-title m-0 p-0 mb-1 ">
                        {" "}
                        <ShimmerEffectText width="" height="20px" />
                      </div>
                    </div>
                    <div className="col-sm-12 col-md-6 col-lg-6 p-0">
                      <div className="card-text system-danger d-flex justify-content-md-end font-110 font-normal ">
                        <ShimmerEffectText width="180px" height="20px" />
                      </div>
                    </div>
                  </div>
                  <div className="mb-2 mb-md-3 mt-1 font-normal ">
                    <div
                      className="card-text text-black-50  text-truncate "
                      style={{ maxWidth: "100%" }}
                    >
                      <ShimmerEffectText width="" height="20px" />
                      <span className="card-text text-black-50 "> </span>
                      <span className="card-text text-black-50 "></span>
                      <span className="card-text ms-1">
                        <a
                          href=""
                          className="text-decoration-none text-black-50 text-decoration-underline"
                        ></a>
                      </span>
                    </div>
                  </div>
                  <div className="row align-items-center">
                    <div className="col-md-8 col-9 ">
                      <button
                       
                        className="  px-3 px-sm-4    me-2 shimmer-button6  shimmer-effect"
                      ></button>
                      <button className=" px-3 px-sm-4   shimmer-button7  shimmer-effect "></button>
                    </div>
                    <div className="col-md-4 col-3 d-flex justify-content-end  ">
                      <div className="font-bold font-110">
                        <span></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        );
      })}
    </>
  );
};
export default PagePackageSkelton;
