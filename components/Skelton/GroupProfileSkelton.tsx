import React from "react";
import ShimmerEffect from "./ShimmerEffect";

const GroupProfileSkelton = ({ loopTimes }: any) => {
  return (
    <>
      {Array.from({ length: loopTimes }).map((_, index) => (
        <div
          key={index} // Add a unique key for each element in the map
          className="col-12 col-sm-6 col-md-4 col-lg-3 py-2 text-center d-flex flex-column justify-content-center align-items-center skelton-disable"
          style={{ cursor: "default", pointerEvents: "none"  }}  >
          <div className="group-add-container system-danger-light-1 rounded-circle shimmer-effect"></div>
          <div className="mt-1">
            <h3 className="m-0 p-0 py-1 font-80 font-bold">
              <ShimmerEffect width="" height="10px" />
            </h3>
            <p className="m-0 p-0 font-80 font-normal pb-1">
              <ShimmerEffect width="" height="10px" />
            </p>
            <div className="d-flex column">
            <button
              type="button"
              className="btn btn-sm system-outline-primary p-1 px-2 bg-white font-normal mb-1 shimmer-button19 shimmer-effect"
            ></button>
             <button
              type="button"
              className="btn btn-sm system-outline-primary p-1 px-2 bg-white font-normal mb-1 shimmer-buttonsm shimmer-effect"
            ></button></div>
          </div>
        </div>
      ))}
    </>
  );
};

export default GroupProfileSkelton;

