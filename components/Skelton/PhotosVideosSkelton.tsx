import React from "react";
const PhotosAndVideosSkelton = ({ loopTimes,className }: any) => {
  return (
    <>
      <div className="d-flex flex-wrap gap-2 gap-sm-3 mb-4 justify-content-start skelton-disable">
        {Array.from({ length: loopTimes }).map((_, index) => {
          return (
            <>
              <div className="flex-column">
                <div
                 className={`mb-2 skelton-disable position-relative ${className} system-filter-secondary shimmer-effect`}
                  style={{ borderRadius: "12px" }}
                ></div>
              </div>
            </>
          );
        })}
      </div>
    </>
  );
};
export default PhotosAndVideosSkelton;
