import React from "react";
import ShimmerEffectText from "./ShimmerEffectText";
const CustomerCreateSkelton = () => {
  return (
    <>
      <div className="page-heading-container skelton-disable" style={{ cursor: "default", pointerEvents: "none" }}>
        <div className="row m-0 rounded mx-md-1 my-4">
          <div className=" col-xl-10 col-lg-9 col-md-9 col-12 bg-body pt-3">
            <div className="row ">
              <div className="col-12 m-0 p-0 px-3 mb-3">
                <div className="px-md-5 ">
                  <div className="mb-4">
                    <div className="row  align-items-center m-0">
                      <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-6 mb-1 p-0   font-bold  text-start  pe-2  ">
                        <ShimmerEffectText width="" height="20px" />
                      </div>
                      <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0">
                        <input
                          type="text"
                          disabled
                          className="form-control border border-2 p-2 system-control font-bold  shimmer-effect"
                        />
                      </div>
                    </div>
                  </div>

                  <div className="mb-4">
                    <div className="row align-items-center m-0">
                      <div className=" col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-6 mb-1 p-0   font-bold  text-start  pe-2 ">
                        <ShimmerEffectText width="" height="20px" />
                      </div>
                      <div className=" col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0">
                        <input
                          type="text"
                          disabled
                          className="form-control border border-2 p-2 system-control font-bold  shimmer-effect"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="row  align-items-center m-0 mb-4">
                    <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-6 mb-1 p-0   font-bold  text-start  pe-2 ">
                      <ShimmerEffectText width="" height="20px" />
                    </div>
                    <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 ">
                      <select
                        className="form-select border border-2 bg-white font-bold p-2 system-control  shimmer-effect"
                        id="dropdownMenuLink"
                        disabled
                      ></select>
                    </div>
                  </div>

                  <div className="mb-4">
                    <div className="row align-items-center m-0">
                      <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-6 mb-1 p-0 font-bold text-start  pe-2 ">
                        <ShimmerEffectText width="" height="20px" />
                      </div>
                      <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0">
                        <input
                          type="text"
                          disabled
                          className="form-control border border-2 p-2 system-control font-bold  shimmer-effect"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="mb-4">
                    <div className="row align-items-center m-0">
                      <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-6 mb-1 p-0 font-bold text-start  pe-2 ">
                        <ShimmerEffectText width="" height="20px" />
                      </div>
                      <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0">
                        <input
                          type="password"
                          disabled
                          className="form-control border border-2 p-2 system-control font-bold  shimmer-effect"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="mb-4">
                    <div className="row align-items-center m-0">
                      <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-6 mb-1 p-0 font-bold text-start  pe-2 ">
                        <ShimmerEffectText width="" height="20px" />
                      </div>
                      <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0">
                        <input
                          type="password"
                          disabled
                          className="form-control border border-2 p-2 system-control font-bold  shimmer-effect"
                        />
                      </div>
                    </div>
                  </div>

                  {/* <div className="mb-4">
                    <div className="row  align-items-center m-0">
                      <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0   font-bold  text-startd  pe-2 ">
                        <ShimmerEffectText width="" height="20px" />
                      </div>
                      <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0">
                        <input
                          type="text"
                          disabled
                          className="form-control border border-2 p-2 system-control font-bold  shimmer-effect"
                        />
                      </div>
                    </div>
                  </div>

                  <div className="mb-4">
                    <div className="row  align-items-center m-0">
                      <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0   font-bold  text-start  pe-2 ">
                        <ShimmerEffectText width="" height="20px" />
                      </div>
                      <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 ">
                        <input
                          type="text"
                          disabled
                          className="form-control border border-2 p-2 system-control font-bold  shimmer-effect"
                        />
                      </div>
                    </div>
                  </div> */}
                  <div className="row  align-items-center m-0 mb-3">
                    <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0   font-bold  text-start"></div>
                    <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0">
                      <div>
                        <button className=" shimmer-button3 shimmer-effect">

                        </button>
                      </div>
                    </div>
                  </div>
                  <div className="row  align-items-center m-0 mb-4">
                    <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0   font-bold  text-start"></div>
                    <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 ">
                      {/* <p className="text-dark font-90 fw-normal lh-sm">
                        You can not modified customer’s information once
                        customer claimed their account. Only empty field can be
                        modified. Customer still able to update their details
                        through their login.
                      </p> */}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default CustomerCreateSkelton;
