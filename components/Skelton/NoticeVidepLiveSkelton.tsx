import React from "react";
import ShimmerEffectText from "./ShimmerEffectText";
import ShimmerEffect from "./ShimmerEffect";
import NoticeHeadingSkelton from "./NoticeHeadingSkelton";
const NoticeVidepLiveSkelton = () => {
  return (
    <>
      {/* --------------Live Streaming ----------- */}
      <div
        className="border bg-white px-4 py-3 pb-4 mb-3 skelton-disable "
        style={{ cursor: "default", pointerEvents: "none" }}
      >
        <div className="d-flex justify-content-between align-items-center ">
          <div className="system-muted-5 font-90 font-normal m-0 p-0 mb-1 col-lg-3 col-md-4 col-4">
            <ShimmerEffect width="" height="15px" />
          </div>
          <div className=" form-check form-switch">
            <input className="  system-control m-0 p-0 pointer shimmer-effectb " />
          </div>
        </div>
        <div className="system-muted-5 font-90 font-normal m-0 p-0 mb-3 col-lg-4 col-md-6 col-6">
          <ShimmerEffect width="" height="10px" />
        </div>
        <div className="m-0 p-0 mb-1 font-semibold col-lg-4 col-md-6 col-6">
          <ShimmerEffect width="" height="10px" />
        </div>
        {/* <div className="m-0 p-0 mb-1 font-semibold col-lg-4 col-md-6 col-6">
          <ShimmerEffect width="" height="10px" />
        </div> */}
        <div className="mb-4 align-items-center">
          <ShimmerEffectText width="" height="55px" />
        </div>

        <div className="m-0 p-0 mb-1 font-semibold col-lg-3 col-md-5 col-5">
          <ShimmerEffect width="" height="10px" />
        </div>
        <input
          type="text"
          className="form-control border border-2 system-control shimmer-effect"
          disabled
        />
        <div className="row m-0 p-0 pt-1 pb-1">
          <div className=" col-12 p-0 m-0   font-90 font-normal lh-1 system-muted-5 mb-1">
            <ShimmerEffect width="" height="10px" />
          </div>
          <div className=" col-lg-10 col-md-10 col-12 p-0 m-0   font-90 font-normal lh-1 system-muted-5">
            <ShimmerEffect width="" height="10px" />
          </div>
        </div>
      </div>
      {/* --------------Videos----------- */}
      <div className="border px-4 py-3 mb-3 bg-white">
     < NoticeHeadingSkelton x={2} y={2}/>
        <div className="m-0 p-0 mb-1 font-semibold col-lg-3 col-md-5 col-5">
          <ShimmerEffect width="" height="10px" />
        </div>
        <input
          type="text"
          className="form-control border border-2 system-control shimmer-effect"
          disabled
        />
        <div className="row m-0 p-0 pt-1 pb-1 mb-4">
          <div className=" col-12 p-0 m-0   font-90 font-normal lh-1 system-muted-5 mb-1">
            <ShimmerEffect width="" height="10px" />
          </div>
          <div className=" col-lg-10 col-md-10 col-12 p-0 m-0   font-90 font-normal lh-1 system-muted-5">
            <ShimmerEffect width="" height="10px" />
          </div>
        </div>
        <button
          type="button"
          className=" d-none d-sm-flex me-3 shimmer-button3 shimmer-effect"
        ></button>
      </div>
      {/* ------------------------- */}
      <div className="mb-1 d-flex justify-content-end">
        <div className="text-end  font-80 fw-normal col-lg-4 col-md-6 col-6  justify-content-end">
          <ShimmerEffect width="" height="10px" />
        </div>
      </div>
      <div className="mb-4 d-flex justify-content-end">
        <div className="text-end  font-80 fw-normal col-lg-2 col-md-3 col-3  justify-content-end">
          <ShimmerEffect width="" height="10px" />
        </div>
      </div>
    </>
  );
};
export default NoticeVidepLiveSkelton;
