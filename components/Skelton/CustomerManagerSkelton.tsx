import React, { useEffect, useState } from "react";
import ShimmerEffect from "./ShimmerEffect";
const CustomerManagerSkelton= () => {
  return (
    <div className="mb-2">
        <div className="card border-white system-secondary-light-thin">
          <div className="card-body">
            {/* ---------------------------------------- */}
            <div className="row m-0">
              <div className="col-12  col-sm-9 p-0 d-sm-flex align-items-center justify-content-center justify-content-sm-start gap-3">
                <div className="d-flex justify-content-center justify-content-sm-start">
                  <div>
                    <div className="page-manager-container text-muted font-140 system-text-dark-light d-flex align-items-center justify-content-center rounded-circle shimmer-effect">

                    </div>
                  </div>
                </div>
                {/* ------------------ */}
                <div
                  className="font-90 font-bold d-flex d-sm-block justify-content-center justify-content-sm-start mt-1 mt-sm-0"
                  style={{ maxWidth: '100%' }}>
                  <div className="w-100" style={{ maxWidth: '100%' }}>
                    <div className="font-90 font-bold text-center text-sm-start text-truncate pe-0 pe-sm-5 mb-1">
                    <ShimmerEffect width="400px" height="15px" />
                    </div>
                    <div className="font-80 font-normal text-secondary text-center text-sm-start text-truncate mb-1">
                    <ShimmerEffect width="400px" height="15px" />
                    </div>
                    <div className="d-sm-flex  font-90 fw-normal  text-truncate">
                      <div className="text-truncate text-center text-sm-start mb-1">
                      <ShimmerEffect width="400px" height="15px" />
                      </div>
                      <div className="d-none d-sm-flex px-1">  </div>
                      {/* <div className="text-center text-sm-start">
                      <ShimmerEffect width="400px" height="15px" />
                      </div> */}
                    </div>
                  </div>
                </div>
              </div>
              {/* ------------------ */}
              <div className="col-12 col-sm-3 p-0 mt-1 mt-sm-0 d-flex gap-2 justify-content-center justify-content-sm-end">
                <div className="text-secondary font-80 font-normal pointer">
                <ShimmerEffect width="40px" height="15px" />
                </div>
                <div
                  className="text-secondary font-80 font-normal pointer"
              >
                  <ShimmerEffect width="40px" height="15px" />
                </div>
              </div>
            </div>
            {/* ---------------------------------------- */}
          </div>
        </div>
      </div>
  );
};

export default CustomerManagerSkelton;
