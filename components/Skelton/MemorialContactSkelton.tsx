import React from "react";
import ShimmerEffectText from "./ShimmerEffectText";
import draggable from "../../public/draggable.svg";
import Image from "next/image";
import ShimmerEffect from "./ShimmerEffect";
import ContactSkelton from "./ContactSkelton ";

const MemorialContactSkeleton = () => {
  return (
    <>
      <div style={{ cursor: "default", pointerEvents: "none"  }}>
        <div className="page-heading font-bold mb-1">
          <ShimmerEffectText width="180px" height="20px" />
        </div>
        <div className="system-muted-2 font-90 mb-4 ">
          <ShimmerEffectText width="" height="12px" />
        </div>
        <div className="row mb-0">
          <div className="col-12">
            <button
              type="button"
              className="btn system-outline-primary mb-3 font-bold shimmer-effect shimmer-button21"
            ></button>
          </div>
        </div>
        <div className="mb-5">
          <ContactSkelton loopTimes={5} />
        </div>
        <div className="">
          <div className="page-heading font-bold mb-1 ">
            <ShimmerEffectText width="180px" height="20px" />
          </div>
          <div className="system-muted-2 font-90 mb-3">
            <ShimmerEffectText width="" height="12px" />
          </div>
          <div className="row  align-items-center  m-0 mb-3">
            <div className="col-12  p-0">
              <textarea className="form-control border border-2 system-control resize-none shimmer-effect "></textarea>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="  col-12 ">
            <button
              type="button"
              className="btn  my-1 shimmer-button3 shimmer-effect"
            ></button>
          </div>
        </div>
      </div>
    </>
  );
};

export default MemorialContactSkeleton;
