import Image from "next/image";
import ellipsissvg from "../../public/ellipsissvg.svg";
import ShimmerEffectText from "./ShimmerEffectText";
const NoticeTypeSkelton = () => {
  return (
    <>
      <div className="bg-white border  p-2 rounded-top mb-2 skelton-disable">
        <div className="row align-items-center m-0 p-0 ">
          <div className="col-md-7 col-12 p-0 mb-2 mb-md-0 ps-2">
            <div className="font-150 font-bold m-0 p-0 mb-2 col-md-5 col-6 col-lg-3 mt-1">
              <ShimmerEffectText width="" height="20px" />
            </div>
            <div className="font-90 system-icon-secondary d-flex flex-column m-0 p-0 ">
              <div className="m-0 p-0 col-7 col-md-8 mb-1">
                <ShimmerEffectText width="" height="10px" />
              </div>
              <div className={`m-0 p-0 font-normal font-100 col-5  col-sm-0 d-sm-none d-block mb-1 d-md-block  d-xl-none`}>
                <ShimmerEffectText width="" height="10px" />
              </div>
              <div className={`m-0 p-0 font-normal font-100 col-2  d-md-none d-block d-lg-none`}>
                <ShimmerEffectText width="" height="10px" />
              </div>
            </div>
          </div>
          <div className="col-md-5 col-12 p-0 d-flex justify-content-md-end justify-content-start ps-2 ps-md-0 d-none d-md-flex">
            <div className=" d-flex gap-1 gap-sm-3 ">
              <div className="  ">
                <button className="btn shimmer-button25 ">
                </button>
              </div>
              <div className=" ">
                <button
                  type="button"
                  className="btn shimmer-button26"
                >
                </button>
              </div>
            </div>
          </div>

          <div className="col-md-5 col-12 p-0 d-flex justify-content-md-end justify-content-start ps-2 ps-md-0 d-flex d-md-none">
            <div className=" d-flex gap-1 gap-sm-3 ">
              <div className="  ">
                <button className="btn shimmer-button25-2 ">
                </button>
              </div>
              <div className=" ">
                <button
                  type="button"
                  className="btn shimmer-button26-2"
                >
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default NoticeTypeSkelton;
