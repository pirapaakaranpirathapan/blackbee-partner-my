import React from "react";
import ShimmerEffectText from "./ShimmerEffectText";
import draggable from "../../public/draggable.svg";
import playIcon from "../../public/play-grey.svg";
import Image from "next/image";
import ShimmerEffect from "./ShimmerEffect";

const MusicSkeleton = ({ loopTimes }: any) => {
  return (
    <>
      {Array.from({ length: loopTimes }).map((_, index) => (
        <div>
          <div className="">
            <div
              className={`d-flex justify-content-between align-items-center  w-100 mu-bt`}
            >
              <div className="d-flex gap-3 align-items-center font-normal-over-flow-f">
                <div className="d-flex align-items-center justify-content-center pointer ">
                  <Image
                    src={playIcon}
                    alt={""}
                    className="sound-container "
                    width={16}
                    height={16}
                  />
                  <div></div>
                </div>
                <div className=" font-110   m-0 p-0 text-truncate me-2 ">
                <ShimmerEffectText width="610px" height="12px" />
                </div>
              </div>
              <button type="button" className="btn shimmer-effect shimmer-button23">
                
              </button>
            </div>
          </div>
        </div>
      ))}
    </>
  );
};

export default MusicSkeleton;
