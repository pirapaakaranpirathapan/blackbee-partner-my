import ShimmerEffect from "./ShimmerEffect";

const NoticeHeadingSkelton = ({ x ,y}: any) => {
  const lgCol = x + 1;
  const mdCol = x + 2;
  const smCol = x + 2;
  const lgCol1 =y + 1;
  const mdCol1 =y + 2;
  const smCol1 =y + 2;
  return (
    <>
      <div className={`d-flex justify-content-between align-items-center`}>
        <div
          className={`system-muted-5 font-90 font-normal m-0 p-0 mb-1 col-lg-${lgCol1} col-md-${mdCol1} col-${smCol1}`}
        >
          <ShimmerEffect width="" height="15px" />
        </div>
        <div className="form-check form-switch">
          <input className="system-control m-0 p-0 pointer shimmer-effectb" />
        </div>
      </div>
      <div
        className={`system-muted-5 font-90 font-normal m-0 p-0 mb-3 col-lg-${lgCol+1} col-md-${mdCol+2} col-${smCol+2}`}
      >
        <ShimmerEffect width="" height="10px" />
      </div>
    </>
  );
};

export default NoticeHeadingSkelton;
