import React from "react";
import ShimmerEffect from "./ShimmerEffect";
const NoticeSkelton = ({ loopTimes }: any) => {
  return (
    <>
      <div className="d-none d-sm-block skelton-disable" style={{ cursor: "default", pointerEvents: "none" }}>
        <table className="table mt-3 ">
          <tbody className="">
            {Array.from({ length: loopTimes }).map((_, index) => {
              return (
                <tr className="align-middle ps-0">
                  <td className="col-5 col-lg-5 col-xl-3 ps-0" style={{ maxWidth: "200px" }}>
                    <div className="d-flex align-items-center gap-3">
                      <div className="ms-md-3 ms-0 table-img text-muted font-140 system-text-dark-light d-flex align-items-center justify-content-center rounded-circle shimmer-circle shimmer-effect"></div>

                      <div style={{ maxWidth: "50%" }}>
                        <div>
                          <div className="p-0 m-0 text-dark font-90 text-truncate mb-1">
                            <ShimmerEffect width="140px" height="15px" />
                          </div>
                        </div>
                        <div className="font-110 font-semibold d-flex mb-1">
                          <div className="text-truncate">
                            <ShimmerEffect width="20px" height="15px" />
                          </div>
                          <ShimmerEffect width="80px" height="15px" />
                        </div>
                        <div className="text-dark font-90  text-truncate">
                          <ShimmerEffect width="60px" height="15px" />
                        </div>
                      </div>
                    </div>
                  </td>
                  <td className="col-2 col-lg-2 col-xl-2 p-0 " style={{ maxWidth: "90px" }}>
                    <>
                      <div className="text-secondary font-80 d-flex align-items-center  justify-content-start mb-1 text-truncate me-2 ">
                        <div className="p-0 m-0">
                          {" "}
                          <ShimmerEffect width="140px" height="15px" />
                        </div>
                      </div>
                      <div className="text-dark font-90 d-flex align-items-center  justify-content-start text-truncate">
                        <div
                          className="p-0 m-0 text-truncate"
                          style={{ maxWidth: "80%" }}
                        >
                          <ShimmerEffect width="140px" height="15px" />
                        </div>
                      </div>
                    </>
                  </td>
                  <td className="col-2 col-lg-2 col-xl-2 p-0 pe-sm-3">
                    <>
                      <div className="text-secondary font-80 font-thin d-none d-sm-flex text-truncate mb-1">
                        <ShimmerEffect width="100px" height="15px" />
                      </div>
                      <div className="text-dark font-90 d-none d-sm-flex text-truncate">
                        <ShimmerEffect width="100px" height="15px" />
                      </div>
                    </>
                  </td>
                  <td className="col-2 col-lg-2 col-xl-4 p-0" style={{ maxWidth: "60px" }}>
                    <>
                      <div className="text-secondary font-80 font-thin text-truncate  mb-1 " style={{ maxWidth: "90%" }}>
                        <ShimmerEffect width="100px" height="15px" />
                      </div>
                      <div>
                        <div
                          className="text-dark font-90 text-truncate p-0 m-0"
                          style={{ maxWidth: "90%" }}
                        >
                          <ShimmerEffect width="100px" height="15px" />
                        </div>
                      </div>
                    </>
                  </td>
                  <td className="col-1 col-lg-1 col-xl-1 p-0">
                    <div className="p-0 m-0 d-none d-sm-flex align-items-center justify-content-end">
                      <button
                        type="button"
                        className=" d-none d-sm-flex me-3 shimmer-button shimmer-effect"
                      ></button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
};
export default NoticeSkelton;
