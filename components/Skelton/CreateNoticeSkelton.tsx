import React, { useEffect, useRef, useState } from "react";
import ShimmerEffectText from "./ShimmerEffectText";

const CreateNoticeSkelton = () => {
  return (
    <div>
      <div className="row skelton-disable" style={{ cursor: "default", pointerEvents: "none" }}>
        <div className="page-header">
          <div className="font-210 font-bold p-0 m-0 mb-1">
            {" "}
            <ShimmerEffectText width="400px" height="30px" />
          </div>
          <div className="page-heading-sub text-muted">
            <ShimmerEffectText width="300px" height="20px" />
          </div>
        </div>
      </div>

      <hr className="page-heading-divider" />

      <div>
        <div className="row m-0 align-items-center">
          <div className="p-0 mb-4 mt-3 font-bold ">
            <ShimmerEffectText width="400px" height="20px" />
          </div>
        </div>
        {/* === */}

        {/* ---------------------------sample arco design deathperson============================ */}
        <div className="row align-items-center m-0">
          <div className="col-md-3 ps-md-5 col-12 p-0 font-semibold ncr-maxwidth pe-1">
            <ShimmerEffectText width="" height="20px" />
          </div>
          <div className="col-md-5 ms-0 col-12  p-0 mb-1">
            <select
              className="form-select border border-2 bg-white mb-0 system-control fw-normal p-2 shimmer-effect"
              id="dropdownMenuLink"
            ></select>
          </div>
        </div>
        <div className="row m-0 mb-3 align-items-center">
          <div className="col-md-3 ps-md-5 col-12 p-0 font-semibold ncr-maxwidth"></div>
          <div className="col-md-5 ms-0 col-12  p-0">
            <div className="p-0 m-0 mb-1 system-muted-5 font-normal">
              <ShimmerEffectText width="" height="15px" />
            </div>
          </div>
        </div>
        {/* ============================================================================= */}

        {/*----------------------------------UI change/ required field/need to verify--------------------------------------------------------------- */}
        {/* <div className="row m-0 mb-4 align-items-center">
          <div className="col-md-3 ps-md-5 col-12 p-0 font-semibold ncr-maxwidth pe-1">
            <ShimmerEffectText width="" height="20px" />

          </div>
          <div className="col-md-5 ms-0 col-12  p-0">
            <select
              className="form-select border border-2 bg-white system-control p-2 shimmer-effect"
              id="dropdownMenuLink"
            >
            </select>
          </div>
        </div> */}

        {/* ==============country================================= */}
        {/* <div className="row m-0 mb-4 align-items-center">
          <div className="col-md-3 ps-md-5 col-12 p-0 font-semibold ncr-maxwidth pe-1">
            <ShimmerEffectText width="" height="20px" />
          </div>
          <div className="col-md-5 ms-0 col-12  p-0">
            <select
              className="form-select border border-2 bg-white system-control p-2 shimmer-effect"
              id="dropdownMenuLink"
            ></select>
          </div>
        </div> */}

        {/*----------------------------------UI change--------------------------------------------------------------- */}
        <div className="row m-0 align-items-center mb-1">
          <div className="col-md-3 ps-md-5 col-12 p-0 pe-1 font-semibold ncr-maxwidth pe-1">
            <ShimmerEffectText width="" height="20px" />
          </div>
          <div className="col-md-5 ms-0 col-12  p-0 ">
            <select
              className="form-select border border-2 bg-white mb-0 system-control fw-normal p-2 shimmer-effect"
              id="dropdownMenuLink"
            >
            </select>
          </div>
        </div>
        <div className="row m-0 mb-3 align-items-center">
          <div className="col-md-3 ps-md-5 col-12 p-0 font-semibold ncr-maxwidth"></div>
          <div className="col-md-5 ms-0 col-12  p-0">
            <div className="p-0 m-0 mb-1 system-muted-5 font-normal">
              <ShimmerEffectText width="" height="15px" />
            </div>
          </div>
        </div>
      </div>

      <div className="">
        <div className="row m-0 mt-5 align-items-center">
          <div className="p-0 mb-4 font-bold">
            <ShimmerEffectText width="450px" height="20px" />
          </div>
        </div>
        <div className="row m-0 align-items-center">
          <div className="col-md-3 ps-md-5 col-12 mb-0 p-0 font-semibold ncr-maxwidth pe-1">
            <ShimmerEffectText width="" height="20px" />
          </div>
          <div className="col-md-5 ms-0 col-12  p-0 mb-1">
            <select
              className="form-select border border-2 bg-white system-control p-2 shimmer-effect"
              id="dropdownMenuLink"
            >


            </select>
          </div>
        </div>

        <div className="row m-0 p-0 align-items-center mb-3">
          <div className="col-md-3 col-12 p-0 font-semibold ncr-maxwidth"></div>
          <div className="col-md-5 ms-0 col-12  p-0">
            <div className="p-0 m-0 mb-1 system-muted-5 font-normal">
              <ShimmerEffectText width="" height="15px" />
            </div>
          </div>
        </div>

        <div className="row m-0 mb-4 align-items-center">
          <div className="col-md-3 ps-md-5 col-12 p-0 font-semibold ncr-maxwidth pe-1">
            <ShimmerEffectText width="" height="20px" />

          </div>
          <div className="col-md-5 ms-0 col-12  p-0 view-t mb-1">
            <select
              className="form-select border border-2 bg-white mb-0 system-control fw-normal p-2 shimmer-effect"
              id="dropdownMenuLink"
            >
            </select>
          </div>
          <div className="row m-0 p-0 align-items-center">
            <div className="col-md-3 col-12 p-0 font-semibold ncr-maxwidth"></div>
            <div className="col-md-5 ms-0 col-12  p-0">
              <div className="p-0 m-0 mb-1 system-muted-5 font-normal">
                <ShimmerEffectText width="" height="15px" />
              </div>
              {/* <div className="align-items-center mt-1">

                <ShimmerEffectText width="" height="15px" />
              </div> */}
            </div>
          </div>
        </div>
      </div>
      <div className="row m-0 mb-3">
        <div className="col-lg-3 col-12 d-none d-sm-flex ncr-maxwidth"></div>
        <div className="col-lg-8 col-12 p-0">
          <button
            type="button"
            className=" d-none d-sm-flex me-3 shimmer-button13 shimmer-effect"
          ></button>
        </div>
      </div>
    </div>
  );
};

export default CreateNoticeSkelton;
