import React from "react";
import ShimmerEffectText from "./ShimmerEffectText";
const NoticeSettingSkelton = () => {
  return (
    <>
    <div className="pe-md-5 border  bg-white p-4 ps-sm-4 pe-md-5 mb-3">
      <div className="row  align-items-start  m-0 mb-5">
        <div className="col-md-3 col-12 p-0 font-bold pb-sm-2 pb-2 pb-md-0 pe-2 ">
          <ShimmerEffectText width="" height="20px" />
        </div>
        <div className="col-md-9 p-0">
          <div className="d-flex gap-3 justify-content-start  text-center flex-wrap ">
            <div className="d-flex flex-column justify-content-center">
              <div className="decoration-img-container rounded d-flex align-items-center justify-content-center system-filter-secondary shimmer-effect  "></div>
              <div className="font-normal font-90 system-text-primary pt-1   ">
                <ShimmerEffectText width="" height="12px" />
              </div>
            </div>
            <div className="d-flex flex-column justify-content-center">
              <div className="decoration-img-container rounded d-flex align-items-center justify-content-center system-filter-secondary  shimmer-effect"></div>
              <div className="font-normal font-90 system-text-primary pt-1   ">
                <ShimmerEffectText width="" height="12px" />
              </div>
            </div>
            <div className="d-flex flex-column justify-content-center">
              <div className="decoration-img-container rounded d-flex align-items-center justify-content-center system-filter-secondary shimmer-effect  "></div>
              <div className="font-normal font-90 system-text-primary pt-1   ">
                <ShimmerEffectText width="" height="12px" />
              </div>
            </div>
            <div className="d-flex flex-column justify-content-center">
              <div className="decoration-img-container rounded d-flex align-items-center justify-content-center system-filter-secondary shimmer-effect "></div>
              <div className="font-normal font-90 system-text-primary pt-1  ">
                <ShimmerEffectText width="" height="12px" />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row  align-items-start  m-0 mb-5">
        <div className="col-md-3 col-12 p-0 font-bold pb-sm-2 pb-2 pb-md-0 pe-2 ">
          <ShimmerEffectText width="" height="20px" />
        </div>
        <div className="col-md-9 p-0">
          <div className="system-text-primary">
            <div className="page-background-container system-filter-secondary shimmer-effect"></div>
            <div className="mt-1  gap-3">
              <div className="font-90 font-normal system-text-primary pointer mb-1  col-md-3 col-6 col-lg-3">
                <ShimmerEffectText width="" height="12px" />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row  align-items-start  m-0 mb-5">
        <div className="row  align-items-center  m-0 mb-3 ">
          <div className="col-md-3 col-12  p-0 m-0 font-bold  pb-2 pb-md-0 pe-2  ">
            <ShimmerEffectText width="" height="20px" />
          </div>
          <div className="col-md-9 col-12  p-0 m-0 ">
            <select
              className="form-select border border-2 bg-white p-2 font-normal system-control shimmer-effect"
              id="dropdownMenuLink"
              disabled
            ></select>
          </div>
        </div>
        <div className="row  align-items-center  m-0 mb-3 ">
          <div className="col-md-3 col-12  p-0 m-0 font-bold  pb-2 pb-md-0 pe-2  ">
            <ShimmerEffectText width="" height="20px" />
          </div>
          <div className="col-md-9 col-12  p-0 m-0 ">
            <div className="col-md-9 col-12 ps-0  mt-1 mt-sm-0 d-flex font-normal ">
              <div className="pe-2">
                <ShimmerEffectText width="84px" height="40px" />
              </div>
              {/* <div className="pe-2">
                <ShimmerEffectText width="172px" height="40px" />
              </div> */}
            </div>
          </div>
          <div className="row mb-3 m-0 p-0 mt-1">
        <div className="col-md-3 pe-2  col-12 d-none d-sm-flex p-0 m-0 "></div>
        <div className="col-md-9  col-12 font-80 font-normal text-muted-5  ps-0  mt-1 mt-sm-0 ">
          <ShimmerEffectText width="" height="12px" />
        </div>
      </div>
        </div>
      </div>

      <hr className="mb-5 hr-width-form2" />
      <div className="row  align-items-start p-0 m-0 mb-4">
        <div className="col-md-3 pe-2 col-12 font-bold  p-0 text-sm-start"></div>
        <div className="col-md-9 col-12 ps-0  mt-1 mt-sm-0   ">
          <div className=" font-150 font-bold col-lg-6 col-7 col-md-6">
            <ShimmerEffectText width="" height="20px" />
          </div>
        </div>
      </div>
      <div className="row  align-items-start p-0 m-0 mb-4">
        <div className="col-md-3 pe-2 col-12 font-bold p-0 text-sm-start mb-1">
          <ShimmerEffectText width="" height="20px" />
        </div>
        <div className="col-md-9 col-12   ps-0  mt-1 mt-sm-0 ">
          <select
            className="form-select border border-2 bg-white  system-control  p-2 shimmer-effect"
            disabled
          ></select>
        </div>
      </div>
      <div className="row  align-items-start p-0 m-0 ">
        <div className="col-md-3 pe-2 col-12 font-bold   p-0 text-sm-start mb-1">
          <ShimmerEffectText width="" height="20px" />
        </div>
        <div className="col-md-9 col-12   ps-0  mt-1 mt-sm-0 ">
          <input
            type="text"
            className="form-control border border-2 system-control shimmer-effect"
          />
        </div>
      </div>
      <div className="row mb-3 m-0 p-0 mt-1">
        <div className="col-md-3 pe-2  col-12 d-none d-sm-flex p-0 m-0 "></div>
        <div className="col-md-9  col-12 font-80 font-normal text-muted-5  ps-0  mt-1 mt-sm-0 ">
          <ShimmerEffectText width="" height="12px" />
        </div>
      </div>
      <div className="row  align-items-start p-0 m-0 mb-1">
        <div className="col-md-3 pe-2 col-12 font-bold p-0 text-sm-start mb-1">
          <ShimmerEffectText width="" height="20px" />
        </div>

        <div className="col-md-9 col-12   ps-0  mt-1 mt-sm-0 ">
          <input
            className="form-control border border-2 system-control shimmer-effect "
            disabled
          />
        </div>
      </div>
      <div className="row mb-4 m-0 p-0">
        <div className="col-md-3 pe-2  col-12 d-none d-sm-flex p-0 m-0 "></div>
        <div className="col-md-9  col-12 font-80 font-normal  text-muted-5  ps-0  mt-1 mt-sm-0">
          <ShimmerEffectText width="" height="12px" />
        </div>
      </div>

      <hr className="mb-5 hr-width-form2" />
      <div className="row  align-items-start p-0 m-0 mb-4">
        <div className="col-md-3 pe-2 col-12 fw-bold  p-0 text-sm-start"></div>
        <div className="col-md-9 col-12 ps-0  mt-1 mt-sm-0    ">
          <div className="font-150 font-bold col-lg-6 col-7 col-md-6">
            <ShimmerEffectText width="" height="20px" />
          </div>
        </div>
      </div>
      <div className="row  align-items-start p-0 m-0 mb-4">
        <div className="col-md-3 pe-2 col-12 font-bold p-0 text-sm-start mb-1">
          <ShimmerEffectText width="" height="20px" />
        </div>
        <div className="col-md-9 col-12   ps-0  mt-1 mt-sm-0 ">
          <select
            className="form-select border border-2 bg-white  system-control p-2 shimmer-effect"
            id="dropdownMenuLink"
            disabled
          ></select>
        </div>
      </div>
      <div className="row  align-items-start p-0 m-0 mb-4  ">
        <div className="col-md-3 pe-2 col-12 font-bold   p-0 text-sm-start mb-1">
          <ShimmerEffectText width="" height="20px" />
        </div>
        <div className="col-md-9 col-12 ps-0  p-0  ">
          <div className="form-check form-switch  m-0 p-0 d-flex align-items-center mb-2 ">
            <input className="  system-control m-0 p-0 pointer shimmer-effectb " />
            <div className="form-check-label font-normal  ms-sm-3 ms-2 col-md-3 col-9 col-lg-8">
              <ShimmerEffectText width="" height="20px" />
            </div>
          </div>
          <div className="form-check form-switch m-0 p-0 d-flex align-items-center mb-2  ">
            <input className="  system-control m-0 p-0 pointer shimmer-effectb " />
            <div className="form-check-label ms-sm-3 ms-2 font-normal col-md-3 col-9 col-lg-8">
              <ShimmerEffectText width="" height="20px" />
            </div>
          </div>
          <div className="form-check form-switch m-0 p-0 d-flex align-items-center">
            <input className="  system-control m-0 p-0 pointer shimmer-effectb " />
            <div className="form-check-label ms-sm-3 ms-2 font-normal col-md-3 col-9 col-lg-8">
              <ShimmerEffectText width="" height="20px" />
            </div>
          </div>
        </div>
      </div>
      
    </div><div className="  mt-4 mb-3">
        <button type="button" className=" shimmer-button4"></button>
      </div>
  </>
  );
};
export default NoticeSettingSkelton;
