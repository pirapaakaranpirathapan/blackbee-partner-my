import React from "react";
import ShimmerEffectText from "./ShimmerEffectText";
const GenralSettingSkelton = () => {
  return (
    <>
      <div className="m-0 page-heading font-bold mb-4 skelton-disable col-lg-3 col-7 col-md-4"  style={{ cursor: "default", pointerEvents: "none"  }}>
        <ShimmerEffectText width="" height="20px" />
      </div>

      <div className="row m-0 mb-3 align-items-center">
        <div className="col-md-3 col-9 p-0 pe-2 mb-1">
          <ShimmerEffectText width="" height="20px" />
        </div>
        <div className="col-md-8 col-12  p-0">
          <select
            className="form-select border border-2 bg-white p-2 system-control shimmer-effect"
            id="dropdownMenuLink"
            disabled
          ></select>
        </div>
      </div>
      <div className="row m-0 mb-3 align-items-center">
        <div className="col-md-3 col-9 p-0 pe-2 mb-1">
          <ShimmerEffectText width="" height="20px" />
        </div>
        <div className="col-md-8 col-12  p-0">
          <select
            className="form-select border border-2 bg-white  p-2 system-control shimmer-effect"
            id="dropdownMenuLink"
            disabled
            defaultValue=""
          ></select>
        </div>
      </div>
      <div className="row m-0 mb-3 align-items-center">
        <div className="col-md-3 col-9 p-0 pe-2 mb-1">
          <ShimmerEffectText width="" height="20px" />
        </div>
        <div className="col-md-8 col-12  p-0">
          <select
            className="form-select border border-2 bg-white p-2 system-control shimmer-effect"
            id="dropdownMenuLink"
            disabled
          ></select>
        </div>
      </div>
      <div className="row m-0 mb-3 align-items-center">
        <div className="col-md-3 col-9 p-0 pe-2 mb-1">
          <ShimmerEffectText width="" height="20px" />
        </div>
        <div className="col-md-8 col-12  p-0 ">
          <input
            type="text"
            className="form-control border border-2 p-2 system-control shimmer-effect  "
            disabled
          />
        </div>
      </div>

      <div className="row mb-3 m-0">
        <div className="col-lg-3  col-12 d-none d-sm-flex "></div>
        <div className="col-lg-8  col-12 p-0">
          <button
            type="button"
            className="  d-sm-flex me-3 shimmer-button3 shimmer-effect"
          ></button>
        </div>
      </div>



      <hr className="mt-5  hr-width-form3" />
     
      <div className="mb-4 page-heading font-bold col-lg-3 col-7 col-md-4"> <ShimmerEffectText width="" height="20px" /></div>
      <div className="row m-0 mb-3 align-items-center fw-semibold">
        <div className="col-md-3 col-9 p-0 pe-2 mb-1"> <ShimmerEffectText width="" height="20px" /></div>
        <div className="col-md-3 col-6 p-0 font-normal pe-2">
        <ShimmerEffectText width="" height="20px" />
        </div>
      </div>
      <div className="row m-0 mb-3 align-items-center fw-semibold">
        <div className="col-md-3 col-9 p-0 pe-2 mb-1"> <ShimmerEffectText width="" height="20px" /></div>
        <div className="col-md-3 col-6 font-normal p-0 pe-2">
        <ShimmerEffectText width="" height="20px " />
        </div>
      </div>
      <div className="row m-0 mb-3 align-items-center fw-semibold">
        <div className="col-md-3 col-9 p-0 pe-2 mb-1"> <ShimmerEffectText width="" height="20px" /></div>
        <div className="col-md-3 col-6 font-normal p-0 pe-2">
        <ShimmerEffectText width="" height="20px" />
        </div>
      </div>
      <div className="row m-0 mb-3 align-items-center fw-semibold">
        <div className="col-md-3 col-9 p-0 pe-2 mb-1"> <ShimmerEffectText width="" height="20px" /></div>
        <div className="col-md-3 col-6 font-normal p-0 text-truncate pe-2" >
        <ShimmerEffectText width="" height="20px" />
        </div>
      </div>
    </>
  );
};
export default GenralSettingSkelton;
