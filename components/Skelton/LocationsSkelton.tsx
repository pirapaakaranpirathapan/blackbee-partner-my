import React from "react";
import ShimmerEffectText from "./ShimmerEffectText";
const LocationsSkelton = () => {
  return (
    <>
      <div className="m-0 page-heading font-bold mb-1 skelton-disable"  style={{ cursor: "default", pointerEvents: "none"  }}>
        {" "}
        <ShimmerEffectText width="180px" height="20px" />
      </div>
      <p className="system-muted mb-3 col-lg-11 col-12 col-md-8">
        {" "}
        <ShimmerEffectText width="" height="12px" />
      </p>
      <div className="row m-0 mb-3 align-items-center">
        <div className="col-md-3 p-0 pe-2 mb-1 col-9 ">
          {" "}
          <ShimmerEffectText width="" height="20px" />
        </div>
        <div className="col-md-8 col-12  p-0">
          <select
            className="form-select border border-2 bg-white p-2 system-control shimmer-effect"
            id="dropdownMenuLink"
            disabled
          ></select>
        </div>
      </div>
      <div className="row m-0 mb-3 align-items-center">
        <div className="col-md-3 col-9 p-0 pe-2 mb-1">
          <ShimmerEffectText width="" height="20px" />
        </div>
        <div className="col-md-8 col-12  p-0">
          <select
            className="form-select border border-2 bg-white  p-2 system-control shimmer-effect"
            id="dropdownMenuLink"
            disabled
            defaultValue=""
          ></select>
        </div>
      </div>
      <div className="row m-0 mb-3 align-items-center">
        <div className="col-md-3 col-9 p-0 pe-2 mb-1">
          <ShimmerEffectText width="" height="20px" />
        </div>
        <div className="col-md-8 col-12  p-0">
          <select
            className="form-select border border-2 bg-white p-2 system-control shimmer-effect"
            id="dropdownMenuLink"
            disabled
          ></select>
        </div>
      </div>
      <div className="row m-0 mb-3 align-items-center">
        <div className="col-md-3 col-9 p-0 pe-2 mb-1">
          <ShimmerEffectText width="" height="20px" />
        </div>
        <div className="col-md-8 col-12  p-0">
          <input
            type="text"
            className="form-control border border-2 p-2 system-control shimmer-effect "
            disabled
          />
        </div>
      </div>
      <div className="row m-0 mb-3 align-items-center">
        <div className="col-md-3 col-9 p-0 pe-2 mb-1">
          <ShimmerEffectText width="" height="20px" />
        </div>
        <div className="col-md-8 col-12  p-0">
          <input
            type="text"
            className="form-control border border-2 p-2 system-control shimmer-effect "
            disabled
          />
        </div>
      </div>
      <div className="row mb-3 m-0">
        <div className="col-lg-3  col-12 d-none d-sm-flex col-md-3"></div>
        <div className="col-lg-8  col-12 p-0 col-md-9">
          <button
            type="button"
            className=" d-sm-flex me-3 shimmer-button3 shimmer-effect"
          ></button>
        </div>
      </div>
    </>
  );
};
export default LocationsSkelton;
