import React from "react";
import ContactSkelton from "./ContactSkelton ";
import NoticeHeadingSkelton from "./NoticeHeadingSkelton";
const NoticeContactsSkelton = () => {
  return (
    <>
      <div className="border bg-white px-4 py-3 pb-4 mb-3 skelton-disable"style={{ cursor: "default", pointerEvents: "none"  }}>
      < NoticeHeadingSkelton x={4} y={2}/>
          <div className="row my-3">
            <div className="col-12">
            <button
              type="button"
              className=" d-none d-sm-flex me-3 shimmer-button18 shimmer-effect"
            ></button>
            </div>
          </div>
          <div>
          <ContactSkelton loopTimes={4} />
          </div>
        
        </div>
    </>
  );
};
export default NoticeContactsSkelton;
