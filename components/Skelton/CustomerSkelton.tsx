import React from "react";
import ShimmerEffect from "./ShimmerEffect";
const CustomerSkelton = ({ loopTimes }: any) => {
  return (
    <>
      <div className="d-none d-sm-block skelton-disable"  style={{ cursor: "default", pointerEvents: "none"  }}>
        <table className="table mt-3 ">
          <tbody className="">
            {Array.from({ length: loopTimes }).map((_, index) => {
              return (
                <tr className="align-middle ps-0">
                  <td className="col-3 ps-0" style={{ maxWidth: "150px" }}>
                    <div className="d-flex align-items-center gap-3">
                      <div className="ms-md-3 ms-0 table-img text-muted font-140 system-text-dark-light d-flex align-items-center justify-content-center rounded-circle shimmer-circle shimmer-effect">
                      
                      </div>
                      <div className="" style={{ maxWidth: "50%" }}>
                        <div className="font-110 font-semibold text-truncate mb-1">
                            <ShimmerEffect width="150px" height="20px" />
                        </div>
                        <div className="text-dark font-90 font-thin text-truncate">
                        <ShimmerEffect width="100px" height="15px" />
                        </div>
                      </div>
                    </div>
                  </td>
                  <td className="col-7 p-0" style={{ maxWidth: "150px" }}>
                    <div className="text-dark font-110 d-flex align-items-center justify-content-end justify-content-sm-start me-3">
                      <div className="p-0 m-0 text-truncate "> <ShimmerEffect width="350px" height="15px" /></div>
                    </div>
                  </td>
                  <td className=" col-2  p-0  ">
                    <div className="p-0 m-0 d-none d-sm-flex align-items-center justify-content-end">
                      <button
                        type="button"
                        className=" d-none d-sm-flex me-3 shimmer-button shimmer-effect"
                      >
                      </button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
};
export default CustomerSkelton;