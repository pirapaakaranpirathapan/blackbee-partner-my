import React from "react";
import ShimmerEffect from "./ShimmerEffect";
import ShimmerEffectText from "./ShimmerEffectText";
const PersonalInformationSkelton = () => {
  return (
    <>
      <div className="mb-3 page-heading font-bold skelton-disable">
        <ShimmerEffectText width="180px" height="20px" />
      </div>
      <div className="row m-0 align-items-center">
        <div className="col-md-3 col-9 p-0 pe-2 mb-1">
          <ShimmerEffectText width="" height="20px" />
        </div>
        <div className="col-md-2 col-4 p-0">
          <select
            className="form-select border border-2 bg-white p-2 system-control shimmer-effect "
            id="dropdownMenuLink"
            disabled
          ></select>
        </div>
        <div className="col-md-6 col-8 pe-0">
          <input
            type="text"
            className="form-control border  system-control p-2  shimmer-effect "
            disabled
          />
        </div>
      </div>{" "}
      <div className=" page-heading font-bold"></div>
      <div className="row mb-3 m-0 align-items-center">
        <div className="col-md-3 col-12 p-0 pe-2"></div>
        <div className="col-md-2 col-4 p-0 "></div>
        <div className="col-md-6 col-8 pe-0   "></div>
      </div>
      <div className="row m-0 align-items-center">
        <div className="col-md-3 col-9 p-0 pe-2 mb-1">
          <ShimmerEffectText width="" height="20px" />
        </div>
        <div className="col-md-8 col-12  p-0">
          <select
            className="form-select border border-2 bg-white p-2 system-control shimmer-effect"
            id="dropdownMenuLink"
            disabled
          ></select>
        </div>
      </div>
      <div className="row m-0 mb-3">
        <div className="col-lg-3 col-12 d-none d-sm-flex  col-md-3"></div>
        <div className="col-lg-8 col-12 system-muted font-normal p-0 mt-1 mb-1 col-md-8">
          <ShimmerEffectText width="" height="12px" />
        </div>
      </div>
      <div className="row m-0 align-items-center">
        <div className="col-md-3 col-9 p-0 pe-2">
          <ShimmerEffectText width="" height="20px" />
          <div className="system-muted font-normal mt-1 mb-1">
            {/* <ShimmerEffectText width="" height="20px" /> */}
          </div>
        </div>
        <div className="col-md-8 col-12  p-0">
          <input
            type="text"
            className="form-control border border-2 system-control mb-3 p-2 shimmer-effect"
            disabled
          />
        </div>
      </div>
      <div className="row m-0 mb-3 align-items-center">
        <div className="col-md-3 col-9 p-0 pe-2 mb-1">
          <ShimmerEffectText width="" height="20px" />
        </div>
        <div className="col-md-8 col-12 p-0">
          <input
            type="text"
            disabled
            className="form-control border border-2 p-2 system-control shimmer-effect "
          />
        </div>
      </div>
      <div className="row m-0 align-items-center">
        <div className="col-md-3 col-9 p-0 pe-2 mb-1">
          <ShimmerEffectText width="" height="20px" />
        </div>
        <div className="col-md-8 col-12  p-0">
          <div className=" position-relative">
            <textarea
              className="form-control border border-2 system-control resize-none shimmer-effect"
              rows={2}
              disabled
              aria-label="With textarea"
            ></textarea>
            <div className="system-muted-2 font-70 position-absolute bottom-0 end-0 mx-1 mb-1"></div>
          </div>
        </div>
      </div>
      <div className="row m-0 mb-3">
        <div className="col-lg-3 col-12 d-none d-sm-flex col-md-3"></div>
        <div className="col-lg-8 col-12 system-muted font-normal p-0 mt-1 mb-1 col-md-8">
          <ShimmerEffectText width="" height="12px" />
        </div>
      </div>
      <div className="row m-0 mb-3 align-items-center">
        <div className="col-md-3 col-9 p-0 pe-2 mb-1">
          {" "}
          <ShimmerEffectText width="" height="20px" />
        </div>
        <div className="col-md-8 col-12  p-0">
          <div className=" position-relative">
            <textarea
              className="form-control border border-2 system-control resize-none shimmer-effect"
              rows={2}
              disabled
              aria-label="With textarea"
            ></textarea>
            <div className="system-muted-2 position-absolute font-70  bottom-0 end-0 mx-1 mb-1"></div>
          </div>
        </div>
      </div>
      <div className="row m-0 mb-3 align-items-center">
        <div className="col-md-3 col-9 p-0 pe-2 mb-1">
          {" "}
          <ShimmerEffectText width="" height="20px" />
        </div>
        <div className="col-md-3 col-12  p-0">
          <select
            className="form-select border border-2 bg-white p-2 system-control shimmer-effect"
            id="dropdownMenuLink"
            disabled
          ></select>
        </div>
      </div>
      <div className="row m-0 align-items-center">
        <div className="col-md-3 col-9 p-0 pe-2 mb-1">
          {" "}
          <ShimmerEffectText width="" height="20px" />
        </div>
        <div className="col-md-4 col-12  p-0">
          <input
            disabled
            className="form-control border border-2 system-control  p-2 shimmer-effect"
          />
        </div>
      </div>
      <div className="row mb-3 m-0">
        <div className="col-lg-3 col-12 d-none d-sm-flex"></div>
        <div className="col-lg-9 col-12 system-muted font-normal p-0"></div>
      </div>
      <div className="row m-0 align-items-center ">
        <div className="col-md-3 col-9 p-0 pe-2 mb-1 ">
          <ShimmerEffectText width="" height="20px" />
        </div>
        {/* <div className="col-lg-6 col-md-7 col-sm-12 col-12 p-0">
          <DatePicker defaultDate={dob} onDateChange={handleDateChange} />
        </div> */}
        <div className="col-md-4 col-12  p-0">
          <input
            disabled
            className="form-control border border-2 system-control  p-2 shimmer-effect"
          />
        </div>
      </div>
      <div className="row  m-0">
        <div className="col-lg-3 col-12 d-none d-sm-flex"></div>
        <div className="col-lg-9 col-12 system-muted font-normal p-0"></div>
      </div>
      <div className="row mb-3 m-0">
        <div className="col-lg-3 col-12 d-none d-sm-flex col-md-3"></div>
        <div className="col-lg-8 col-12 system-muted font-normal p-0 mt-1 mb-1 col-md-8">
          <ShimmerEffectText width="" height="12px" />
        </div>
      </div>
      <div className="row m-0 mb-3">
        <div className="col-lg-3  col-12 d-none d-sm-flex col-md-3 "></div>
        <div className="col-lg-9  col-12 p-0 col-md-9">

          <button
            type="button"
            className="  d-sm-flex me-3 shimmer-button3 shimmer-effect"
          ></button>
        </div>
      </div>
    </>
  );
};
export default PersonalInformationSkelton;
