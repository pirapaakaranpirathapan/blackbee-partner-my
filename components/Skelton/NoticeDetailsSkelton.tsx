import React from "react";
import ShimmerEffect from "./ShimmerEffect";
import ShimmerEffectText from "./ShimmerEffectText";
const NoticeDetailsSkelton = () => {
  return (
    <>
      <div className="skelton-disable">
        {/* <ShimmerEffectText width="" height="65px" /> */}
        {/* <div className="mb-3 p-3 system-danger-light rounded ">  
          <p className="p-0 m-0 font-90 font-bold">
        
          </p>
          <p className="p-0 m-0 system-text-dark-80 font-90 font-normal ">
          
          </p>
        </div> */}
        <div className="row  align-items-center p-0 m-0  mt-4">
          <div className="col-md-3 col-12  font-bold p-0 text-md-end pe-0 pe-md-4 mb-1 ">
            <div className="m-0 p-0"> <ShimmerEffectText width="" height="20px" /></div>

          </div>
          <div className="col-md-4 col-12   ps-0 ps-md-2 mt-1 mt-sm-0 ">
            <select
              className="form-select border border-2 bg-white system-control font-bold p-2 shimmer-effect"
              id="dropdownMenuLink"
              disabled
            >

            </select>
          </div>
          <div className="row mb-3 m-0 p-0">
            <div className="col-md-3  col-12 d-none d-sm-flex p-0 m-0 "></div>
            <div className="col-md-9  col-12 font-80 font-normal system-muted-5  ps-0 ps-md-2 mt-1 mt-sm-0">
              {/* <ShimmerEffectText width="" height="20px" /> */}
            </div>
          </div>
        </div>
        <div className="row  align-items-center p-0 m-0 mb-3">
          <div className="col-md-3 col-12 p-0 text-md-end  font-bold pe-0 pe-md-4 lh-sm mb-1">
            <ShimmerEffectText width="" height="20px" />
            {/* <div className="font-80 font-normal system-icon-secondary p-0 text-md-end lh-sm">
            <ShimmerEffectText width="" height="20px" />
            </div> */}
          </div>
          <div className="col-md-9 col-12 ps-0 ps-md-2 mt-1 mt-sm-0">
            <input
              type="text"
              className="form-control border border-2 notice-input system-control shimmer-effect mb-1 "
              disabled
            />
          </div>
        </div>
        <div className="row  align-items-center p-0 m-0 ">
          <div className="col-md-3 col-12 p-0 text-md-end  font-bold pe-0 pe-md-4 mb-1">
            <ShimmerEffectText width="" height="20px" />
            {/* <div className="font-80 font-normal system-icon-secondary  p-0 text-md-end ">
            <ShimmerEffectText width="" height="20px" />
            </div> */}
          </div>
          <div className="col-md-9 col-12   ps-0 ps-md-2 mt-1 mt-sm-0 ">
            <input
              type="text"
              className="form-control border border-2 notice-input system-control shimmer-effect "
              disabled
            />
          </div>{" "}
          <div className="row  m-0 p-0 mb-3">
            <div className="col-md-3 col-12 d-none d-sm-flex ps-0 "></div>
            <div className="col-md-9  col-12 font-bold   ps-0 ps-md-2 mt-1 mt-sm-0 ">
              {/* <span className="font-90 font-semibold text-dark">
                Suggestion:{" "}
              </span> */}
            </div>
          </div>
        </div>
        <div className="row  align-items-center p-0 m-0">
          <div className="col-md-3 col-12 p-0 text-md-end  font-bold pe-0 pe-md-4 ">
            <ShimmerEffectText width="" height="20px" />
          </div>
          <div className="col-md-9 col-12 m-0  ps-0 ps-md-2 mt-1 mt-sm-0 mb-1">
            {/* <button
                  type="button"
                  className="btn px-3 px-sm-4 me-3 border border-2 font-bold text-dark py-3"
                  onClick={() => setAddTemplate(true)}
                >
                  Select Template
                </button> */}
            {/* {!selectedOptionId && (
                    <button
                      type="button"
                      className="btn px-3 px-sm-4 me-3 border border-2 font-bold text-dark py-3"
                      
                    >
                      Select Template
                    </button>
                  )} */}
            <div
              className={`mb-2 skelton-disable position-relative template-container-small  shimmer-effect mt-1`}
              style={{ borderRadius: "12px" }}
            ></div>
            {/* {selectedOptionId && (
                    <span
                      className="font-normal system-text-primary pointer"
                      onClick={() => setAddTemplate(true)}
                    >
                      Change Template
                    </span>
                  )} */}
            {/* <span className="font-normal system-text-primary pointer">
                  Change Template
                </span> */}
          </div>
        </div>
        <div className="row mb-5 m-0 p-0">
          <div className="col-lg-3 col-12 d-none d-sm-flex ps-0 "></div>
          <div className="col-lg-9 col-12  ps-0 ps-md-2 mt-1 mt-sm-0 ">
            <button
              type="button"
              className=" d-none d-sm-flex me-3 shimmer-button3 shimmer-effect"
            ></button>
          </div>
        </div>
      </div>
    </>
  );
};
export default NoticeDetailsSkelton;
