import React from "react";
import ShimmerEffect from "./ShimmerEffect";
import ShimmerEffectText from "./ShimmerEffectText";
const CustomerDetailsSkelton = () => {
  return (
    <>
      <div className="px-md-5  skelton-disable"  style={{ cursor: "default", pointerEvents: "none"  }}>
        <div className="mb-4">
          <div className="row  align-items-center m-0">
            <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-5  p-0  font-bold  mb-1   ">
              <ShimmerEffectText width="90%" height="20px" />
            </div>
            <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0">
              <input
                type="text"
                className="form-control border border-2 p-2 system-control font-bold shimmer-effect"
                disabled
              />
            </div>
          </div>
        </div>
        <div className="mb-4">
          <div className="row align-items-center m-0">
            <div className=" col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-5 p-0   font-bold  text-start mb-1">
              <ShimmerEffectText width="90%" height="20px" />
            </div>
            <div className=" col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0">
              <input
                type="text"
                className="form-control border border-2 p-2 system-control font-bold shimmer-effect"
                disabled
              />
            </div>
          </div>
        </div>
        <div className="row  align-items-center m-0 mb-4">
          <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-5 p-0   font-bold  text-start mb-1">
            <ShimmerEffectText width="90%" height="20px" />
          </div>
          <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 ">
            <select
              className="form-select border border-2 bg-white font-bold p-2 system-control shimmer-effect"
              id="dropdownMenuLink"
              disabled
            ></select>
          </div>
        </div>
        <div className="mb-4">
          <div className="row align-items-center m-0">
            <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-5 p-0 font-bold text-start mb-1">
              <ShimmerEffectText width="90%" height="20px" />
            </div>
            <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0">
              <input
                type="text"
                className="form-control border border-2 p-2 system-control font-bold shimmer-effect"
                disabled
              />
            </div>
          </div>
        </div>
        <div className="mb-4">
          <div className="row  align-items-center m-0">
            <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-5 p-0   font-bold  text-startd mb-1">
              <ShimmerEffectText width="90%" height="20px" />
            </div>
            <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0">
              <input
                type="text"
                className="form-control border border-2 p-2 system-control font-bold shimmer-effect"
                disabled
              />
            </div>
          </div>
        </div>
        <div className="mb-4">
          <div className="row  align-items-center m-0">
            <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-5 p-0   font-bold  text-start mb-1">
              <ShimmerEffectText width="90%" height="20px" />
            </div>
            <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 ">
              <input
                type="text"
                className="form-control border border-2 p-2 system-control font-bold shimmer-effect"
                disabled
              />
            </div>
          </div>
        </div>
        <div className="row  align-items-center m-0 mb-3">
          <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0   font-bold  text-start"></div>
          <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0">
            <div>
              <button className="btn rounded font-110 shimmer-button4 shimmer-effect"></button>
            </div>
          </div>
        </div>
        <div className="row  align-items-center m-0 mb-4">
          <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0   font-bold  text-start"></div>
          <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 ">
            <p className="text-dark font-90 fw-normal lh-sm"></p>
          </div>
        </div>
      </div>
    </>
  );
};
export default CustomerDetailsSkelton;
