
import ShimmerEffectText from "./ShimmerEffectText";

const SettingsSkelton = ({ noticetypes, idQuery }: any) => {
  return (
    <div
      className="skelton-disable"
      style={{ cursor: "default", pointerEvents: "none" }}
    >
      <div className="row">
        <div className="page-header">
          <div className="font-210 font-bold p-0 m-0 mb-1">
            {" "}
            {/* <ShimmerEffectText width="400px" height="30px" /> */}
            My Account setting
          </div>
          <div className="page-heading-sub text-muted">
            {/* <ShimmerEffectText width="300px" height="20px" /> */}
            Edit your personal information
          </div>
        </div>
      </div>
      <hr className="page-heading-divider" />
      <div>
        <div className="row m-0 align-items-center">
          <div className="p-0 mb-4 mt-3 font-bold  col-md-3 col-7 col-lg-2">
            <ShimmerEffectText width="" height="20px" />
          </div>
        </div>
        <div className="row align-items-center m-0">
          <div className="col-md-3 ps-md-5 col-6 p-0 font-semibold ncr-maxwidth mb-1 pe-1">
            <ShimmerEffectText width="" height="20px" />
          </div>
          <div className="col-md-5 ms-0 col-12  p-0 mb-3 row m-0">
            {/* <div className="col-md-3 col-3 p-0 pe-2 ">
              <select
                className="form-select border border-2 bg-white fw-normal p-2 system-control shimmer-effect"
                id="dropdownMenuLink"
              ></select>
            </div> */}
            <div className="col-md-12 col-12 p-0">
              <input
                type="text"
                className="form-control border border-2 p-2 system-control shimmer-effect "
              />
            </div>
            <div className="row align-items-center m-0 col-12 p-0 ">
              <div className="col-md-4 col-3 p-0 pe-2 "></div>
              <div className="col-md-8 col-9 p-0"></div>
            </div>
          </div>
        </div>
        <div className="row m-0 mb-4 align-items-center">
          <div className="col-md-3 ps-md-5 col-6 p-0 font-semibold ncr-maxwidth pe-1 mb-1 ">
            <ShimmerEffectText width="" height="20px" />
          </div>
          <div className="col-md-5 ms-0 col-12  p-0">
            <input
              type="text"
              className="form-control border border-2 p-2 system-control font-bold shimmer-effect"
              name="text"
              autoComplete="new-text"
            />
          </div>
        </div>
        <div className="row m-0 mb-4 align-items-center">
          <div className="col-md-3 ps-md-5 col-6 p-0 font-semibold ncr-maxwidth pe-1 mb-1">
            <ShimmerEffectText width="" height="20px" />
          </div>
          <div className="col-md-5 ms-0 col-12  p-0">
            <input
              type="text"
              className="form-control border border-2 p-2 system-control font-bold shimmer-effect"
            />
          </div>
        </div>
      </div>
      <div className="">
        <div className="row m-0 mt-5 align-items-center">
          <div className="p-0 mb-3 font-bold col-md-3 col-7 col-lg-2">
            {" "}
            <ShimmerEffectText width="" height="20px" />
          </div>
        </div>
        <div className="row m-0 align-items-center  ">
          <div className="col-md-3 ps-md-5 col-6 mb-0 p-0 font-semibold ncr-maxwidth pe-1 mb-1">
            <ShimmerEffectText width="" height="20px" />
          </div>
          <div className="col-md-5 ms-0 col-12  p-0  mb-3 position-relative ">
            <input className="form-control border border-2 p-2 system-control font-bold shimmer-effect" />
            <div className=""></div>
          </div>
        </div>
        <div className="row m-0 mb-4 align-items-center">
          <div className="col-md-3 ps-md-5 col-6 p-0 font-semibold ncr-maxwidth pe-1 mb-1">
            <ShimmerEffectText width="" height="20px" />
          </div>
          <div className="col-md-5 ms-0 col-12  p-0 view-t position-relative">
            <input className="form-control border border-2 p-2 system-control font-bold shimmer-effect" />
            <div className=""></div>
          </div>
        </div>
      </div>
      <div className="row m-0 mb-3">
        <div className="col-lg-3 col-12 d-none d-sm-flex ncr-maxwidth"></div>
        <div className="col-lg-8 col-12 p-0">
          <button
            type="button"
            className="  d-sm-flex me-3 shimmer-button13 shimmer-effect"
          ></button>
        </div>
      </div>
    </div>
  );
};

export default SettingsSkelton;
