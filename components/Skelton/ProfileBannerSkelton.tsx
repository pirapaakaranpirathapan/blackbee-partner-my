import React from "react";
import ProfileMoreDropdown from "../Elements/ProfileMoreDropdown";
import ShimmerEffect from "./ShimmerEffect";
const ProfileBannerSkelton = () => {
  return (
    <>
      <div
        className="ps-md-3 d-flex align-items-center justify-content-center "
        style={{ cursor: "default", pointerEvents: "none" }}
      >
        <div className=" main-profile-container position-relative   border  rounded-circle  shimmer-effect">
          <div className="bg-white rounded-circle position-absolute end-0 top-50 shimmer-effect"></div>
        </div>
      </div>
      <div
        style={{ maxWidth: "100%" }}
        className="px-md-3 w-100 d-flex align-items-center justify-content-md-start justify-content-center text-center text-md-start mb-3 mb-md-0 "
      >
        <div className="w-100">
          <h5 className="m-0 system-text-blue font-170 font-bold text-truncate-mobile pe-0 me-0   line-height-2  mb-1 shimmer-effect">
            <ShimmerEffect width="" height="20px" />
          </h5>
          <div className="m-0 font-90 system-text-blue text-truncate-mobile line-height-2  mb-1">
            <ShimmerEffect width="" height="20px" />
          </div>
          <div className="m-0 font-90 system-text-blue line-height-2  mb-1">
            <ShimmerEffect width="" height="20px" />
          </div>
          <div className="m-0 font-90 system-text-blue text-truncate line-height-2 ">
            <ShimmerEffect width="" height="20px" />
          </div>
        </div>
      </div>
    </>
  );
};
export default ProfileBannerSkelton;
