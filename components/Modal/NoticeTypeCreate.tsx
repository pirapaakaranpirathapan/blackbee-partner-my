import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
import { getLabel } from "@/utils/lang-manager";
import { useRouter } from "next/router";
import React, { useContext, useState } from "react";

const NoticeTypeCreate = ({ noticetypesDatas }: any) => {
  const [visibleItems, setVisibleItems] = useState(10);
  const [selectedItemId, setSelectedItemId] = useState(null);
  const router = useRouter();
  const Customeruuid = router.query?.id;
  const { NoticeData } = useContext(NoticeDataContext);
  const { PageData } = useContext(PageDataContext);
  const initialLanguage = NoticeData?.initialLanguage || PageData?.initialLanguage

  const handleClick = () => {
    if (selectedItemId) {
      router.push({
        pathname: "/notices/create",
        query: {
          id: selectedItemId,
          uuid: Customeruuid,
        },
      });
    }
  };

  const handleClickLoadMore = () => {
    setVisibleItems((prevVisibleItems) => prevVisibleItems + 4);
  };

  const handleRadioChange = (id: any) => {
    setSelectedItemId(id);
  };

  const renderedItems = noticetypesDatas && noticetypesDatas.slice(0, visibleItems).map((data: any, index: any) => (
    <li key={index} className="list-style-none mb-2 cursor-default">
      <div className="d-flex align-items-center gap-2 cursor-default">
        <div className="form-check m-0 p-0 b-line mt-tpp">
          <input
            className="check-button pointer"
            type="radio"
            name="exampleRadios"
            id={`exampleRadios${index}`}
            value={data.id}
            checked={data.id === selectedItemId}
            onChange={() => handleRadioChange(data.id)}
          />
        </div>
        <div className="font-normal">
          <label htmlFor={`exampleRadios${index}`}>
            <div className="system-text-primary font-140 lh-1 pointer">{data.name}</div>
            <div className="font-90 system-muted1">
              {data.description ? data.description : ""}
            </div>
          </label>
        </div>
      </div>
    </li>
  ));

  return (
    <>
      <div className="notice-type">
        <div className="row  align-items-center m-0">
          <div className="col-12 p-0 mb-4 notice-list-content">
            {renderedItems}
          </div>
          {noticetypesDatas?.length > visibleItems && (
            <div
              className="font-normal text-center font-120 pointer"
              onClick={handleClickLoadMore}
            >
              Load more
            </div>
          )}
        </div>
        <button
          type="button"
          className="btn btn-system-primary"
          onClick={handleClick}
          disabled={!selectedItemId}
        >
          {getLabel("next", initialLanguage)}
        </button>
      </div>
    </>
  );
};

export default NoticeTypeCreate;
