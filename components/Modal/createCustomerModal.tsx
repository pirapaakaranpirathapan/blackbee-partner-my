import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import { CountriesSelector, getAllCountries } from "@/redux/countries";
import {
  customerPageSelector,
  createCustomer,
  getAllCustomer,
  getAllCustomerforpage,
} from "@/redux/customer";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import React, {
  useEffect,
  useState,
  forwardRef,
  useContext,
  useImperativeHandle,
  useRef,
} from "react";
import { Button } from "react-bootstrap";
import { Notification } from "@arco-design/web-react";
import { useApiErrorHandling } from "../Others/useApiErrorHandling";
import PhoneNumberInput from "../Others/PhoneNumberInput";
import { gendersSelector, getAllGenders } from "@/redux/genders";
import { getLabel } from "@/utils/lang-manager";
import CustomSelect from "../Arco/CustomSelect";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";
type CustomerData = {
  id: number;
  name: string;
  mobile: string;
  email: string;
  country: string;
};
export interface ChildModalRef {
  callModalFunction: (callback: (response: any) => void) => void;
}
type CustomerContinueProps = {
  onCallCustomerContinue: (response: any) => void; // Callback function from parent
  addCusSearchValue: {
    inputValue: string;
    searchType: string;
  };
  setModalVisible: (visible: boolean) => void;

};

export interface ApiResponse {
  msg: string;
  code: number;
  data: any;
  error: null;
  success: boolean;
  // Add any other properties if available in the actual response
}
const CustomerDetailsModal = forwardRef<ChildModalRef, CustomerContinueProps>(
  ({ onCallCustomerContinue, addCusSearchValue, setModalVisible }, ref) => {
    const { NoticeData } = useContext(NoticeDataContext);
    const { PageData } = useContext(PageDataContext);
    const initialLanguage = NoticeData?.initialLanguage || PageData?.initialLanguage
    const { inputValue, searchType } = addCusSearchValue;
    const tab = "Customer Details";
    const [activeTab, setActiveTab] = useState(tab);
    const [selectedCustomer, setselectedCustomer] = useState(0);
    const dispatch = useAppDispatch();
    const { error: apiError } = useAppSelector(customerPageSelector);
    const [isModalFunctionCalled, setIsModalFunctionCalled] = useState(false);
    const [firstName, setFirstName] = useState(searchType === 'Name' ? inputValue : '');
    const [lastName, setLastName] = useState("");
    const [countryId, setCountryId] = useState(0);
    const [email, setEmail] = useState(searchType === 'Email' ? inputValue : '');
    const [password, setpassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [mobile, setMobile] = useState('+94');
    const [genderId, setGenderId] = useState("");
    const [alternativeNumber, setAlternativeNumber] = useState("");
    const [response, setResponse] = useState("");
    const [customer, setcustomer] = useState({ id: "108", first_name: "Albinn" });

    const [customerDatas, setCustomerDatas] = useState<CustomerData[]>([]);

    useEffect(() => {
      dispatch(getAllGenders());
      setMobile(searchType === 'Mobile' ? inputValue : '+94');
      setAlternativeNumber("+94");
    }, []);

    const callContinueFunction = () => {
      onCallCustomerContinue(customer);
    };

    const callModalFunction = (callback: any) => {
      // Declare updatedData with an optional alternative_phone property
      let updatedData: {
        first_name: string;
        last_name: string;
        country_id: number;
        email: string;
        mobile: string;
        gender_id: string;
        password: string;
        password_confirmation: string;
        alternative_phone: string;
      } = {
        first_name: firstName,
        last_name: lastName,
        country_id: countryId,
        email: email,
        mobile: mobile,
        gender_id: "3",
        password: "12345678",
        password_confirmation: "12345678",
        alternative_phone: "",
      };

      if (alternativeNumber.length > 5) {
        // Conditionally assign the alternative_phone property
        updatedData.alternative_phone = alternativeNumber;
      } else {
        // Assuming alternativeProtectFlag is a variable containing the default value
        const alternativeProtectFlag = "";
        updatedData.alternative_phone = alternativeProtectFlag;
      }

      setIsModalFunctionCalled(true);
      dispatch(
        createCustomer({
          activePartner: ACTIVE_PARTNER,
          activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
          updatedData,
        })
      ).then((response: any) => {
        dispatch(
          getAllCustomerforpage({
            active_partner: ACTIVE_PARTNER,
            active_service_provider: ACTIVE_SERVICE_PROVIDER
          })
        );
        if (response.payload.success == true) {
          Notification.success({
            title: "Customer Create Successfully",
            duration: 3000,
            content: undefined,
          });
          setselectedCustomer(response?.payload?.data?.data?.id);
          callback(response?.payload?.data?.data);
          setResponse(response);
        } else {
          (response.payload.code != 422) &&    Notification.error({
            title: "Customer Create Failed",
            duration: 3000,
            content: undefined,
          });
        }
      });
    };
    useImperativeHandle(ref, () => ({
      callModalFunction,
      selectedCustomerId: selectedCustomer,
    }));

    useEffect(() => {
      loadStaticData();
    }, [apiError]);

    // ===========================rathan code=========================================================
    // useEffect(() => {
    //   continuewithmail();
    // }, []);

    const changeTab = (val: any) => {
      setActiveTab(val);
    };
    const handleCountryChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
      const selectedValue = e.target.value;
      setCountryId(parseInt(selectedValue));
    };
    const handleGenderChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
      const selectedValue = e.target.value;
      setGenderId(selectedValue);
    };
    const loadStaticData = async () => {
      dispatch(getAllCountries());
    };
    const { data: showCountriresData } = useAppSelector(CountriesSelector);
    const countries = showCountriresData;

    const { data: gendersData } = useAppSelector(gendersSelector);

    const maskString = (inputString: string, visibleCount: number) =>
      inputString.length <= visibleCount
        ? inputString
        : inputString.slice(0, visibleCount) +
        "*".repeat(inputString.length - visibleCount);

    const error = useApiErrorHandling(apiError, response);
    console.log("error", error);

    //customer email find api
    const [errorShow, setErrorShow] = useState(false);
    const [errorShowMobile, setErrorShowMobilew] = useState(false);
    const [Err, setErr] = useState("");


    const continuewithmail = () => {
      // setNames("")
      setModalVisible(false);
    };

    useEffect(() => {
      setEmail("");
      setErr("");
      // setErrorShow(false);
      // setErrorShowMobilew(false)
    }, [])




    const handleBlur = async () => {
      try {
        const customerData = await checkCustomerExists(email);

        if (email) {
          if (customerData && customerData.length > 0) {
            // Customer exists
            setCustomerDatas(customerData);
            setErrorShow(true);
            setErrorShowMobilew(false)
            // setMobile("+94");

          } else {
            // Customer does not exist
            setCustomerDatas([]);
            // setErr('Customer not found. Create a new customer?');
            setErrorShow(false);
          }
        }

        // if (mobile.length > 3) {
        //   if (customerData && customerData.length > 0) {
        //     // Customer exists
        //     setCustomerDatas(customerData);
        //     setErrorShowMobilew(true)
        //     setErrorShow(false);

        //   } else {
        //     // Customer does not exist
        //     setCustomerDatas([]);
        //     setErrorShowMobilew(false)
        //   }
        // }
      } catch (error) {
        console.error('Error checking customer:', error);
        // setErr('Error checking customer. Please try again.');
        setErrorShow(false);
        setErrorShowMobilew(false)

      }
    };


    const handleBlurMobile = async () => {
      try {
        const customerData = await checkCustomerExistsMobile(mobile);

        // if (email) {
        //   if (customerData && customerData.length > 0) {
        //     // Customer exists
        //     setCustomerDatas(customerData);
        //     setErrorShow(true);
        //     setErrorShowMobilew(false)
        //     // setMobile("+94");

        //   } else {
        //     // Customer does not exist
        //     setCustomerDatas([]);
        //     // setErr('Customer not found. Create a new customer?');
        //     setErrorShow(false);
        //   }
        // }

        if (mobile.length > 3) {
          if (customerData && customerData.length > 0) {
            // Customer exists
            setCustomerDatas(customerData);
            setErrorShowMobilew(true)
            setErrorShow(false);

          } else {
            // Customer does not exist
            setCustomerDatas([]);
            setErrorShowMobilew(false)
          }
        }
      } catch (error) {
        console.error('Error checking customer:', error);
        // setErr('Error checking customer. Please try again.');
        setErrorShow(false);
        setErrorShowMobilew(false)

      }
    };

    // const checkCustomerExists = (email: string, mobile: string) => {
    const checkCustomerExists = (email: string) => {
      if (email.toLowerCase() === 'albin.9118@yahoo.com') {
        const dummyData: CustomerData[] = [
          {
            id: 1,
            name: 'Albin',
            mobile: '+15167671180',
            email: 'albin.9118@yahoo.com',
            country: 'Srilanka',
          },
        ];
        return dummyData;
      } else {
      }
      // if (mobile === '+15167671180') {
      //   const dummyData: CustomerData[] = [
      //     {
      //       id: 1,
      //       name: 'Albin',
      //       mobile: '+15167671180',
      //       email: 'albin.9118@yahoo.com',
      //       country: 'Srilanka',
      //     },
      //   ];
      //   return dummyData;
      // } else {
      // }
    };

    // const checkCustomerExistsMobile = (email: string, mobile: string) => {
    const checkCustomerExistsMobile = (mobile: string) => {
      // if (email.toLowerCase() === 'albin.9118@yahoo.com') {
      //   const dummyData: CustomerData[] = [
      //     {
      //       id: 1,
      //       name: 'Albin',
      //       mobile: '+15167671180',
      //       email: 'albin.9118@yahoo.com',
      //       country: 'Srilanka',
      //     },
      //   ];
      //   return dummyData;
      // } else {
      // }
      if (mobile === '+15167671180') {
        const dummyData: CustomerData[] = [
          {
            id: 1,
            name: 'Albin',
            mobile: '+15167671180',
            email: 'albin.9118@yahoo.com',
            country: 'Srilanka',
          },
        ];
        return dummyData;
      } else {
      }
    };

    // const checkCustomerExists = async (email: string) => {
    //   const response = await fetch(`your_api_endpoint/${email}`);
    //   const data = await response.json();

    //   return data;
    // };


    return (
      <div className="upper-modal">
        <div className="mb-3 ">
          <div className="row m-0">
            <div className="col-12 p-0 font-semibold">
              {getLabel("email", initialLanguage)}
              {/* <span className="text-danger">*</span> */}
            </div>
            <div className="col-12 p-0">
              <input
                type="text"
                className="form-control border border-2 p-2 system-control"
                placeholder={`${getLabel("enterEmail", initialLanguage)}`}
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                onBlur={handleBlur}
                name="email"
                autoComplete="new-email"
              />
              {errorShow ? (
                <>
                  <div className="text-danger my-1">
                    {getLabel("weHaveFoundCustomer", initialLanguage)}
                  </div>
                  <div className="rounded system-red-light-500">
                    <div className="row m-0 text-truncate">
                      <div className="col-12 col-sm-7 my-2">
                        <p className="m-0 p-0 font-bold line-height-3 font-130 text-truncate">Mr. Sanj*** Nav***</p>
                        <p className="m-0 p-0 text-truncate">sanjay@gmail.com,<span className="m-0 p-0 ps-1">+9477721*****</span></p>
                        <p className="m-0 p-0 text-truncate">Sri Lanka</p>
                      </div>
                      <div className="col-12 col-sm-5 text-start text-sm-end mb-3 mt-0 mt-sm-2 pt-0 pt-sm-1">
                        <button
                          className="btn btn-system-primary pointer py-0 px-2 rounded-2"
                          onClick={continuewithmail}
                        >
                          Use this customer
                        </button>
                      </div>
                    </div>

                    {/* ===========================rathan code========================================================= */}
                    {/* <div className="row m-0 bg-danger">
                      <div className="col-3 p-0 font-semibold">
                        <p
                          style={{
                            maxWidth: "100px",
                            overflow: "hidden",
                            whiteSpace: "nowrap",
                            justifyContent: "space-between",
                          }}
                        >
                          <span>{maskString(email.split("@")[0], 9)}</span>
                        </p>
                      </div>
                      <div className="col-9 p-0 font-semibold">
                        <a
                          style={{
                            textDecoration: "underline",
                            cursor: "pointer",
                          }}
                          onClick={callContinueFunction}
                        >
                          {getLabel("useThisCustomer",initialLanguage)}
                        </a>
                      </div>
                    </div>
                    <div className="row m-0 align-items-center bg-warning">
                      <div className="col-3 p-0 font-semibold">
                        <Button
                          className="btn-system-primary pointer"
                          onClick={continuewithmail}
                        >
                          {getLabel("continue",initialLanguage)}
                        </Button>
                      </div>
                      <div className="col-9 p-0 font-semibold">
                        <p className="p-0 m-0">
                          <a
                            style={{
                              cursor: "pointer",
                            }}
                            onClick={continuewithmail}
                          >
                            {getLabel("continueWithNewEmail",initialLanguage)}
                          </a>
                        </p>
                      </div>
                    </div> */}
                  </div>
                </>
              ) : error?.email ? (
                <div className="text-danger">{error?.email}</div>
              ) : null}
            </div>
          </div>
        </div>
        <div className="mb-3">
          <div className="row m-0">
            <div className="col-12 p-0 font-semibold">
              {getLabel("firstName", initialLanguage)}<span className="ps-1">*</span>
              {/* <span className="text-danger">*</span> */}
            </div>
            <div className="col-12 p-0 ">
              <input
                type="text"
                className="form-control border border-2 p-2 system-control "
                placeholder={`${getLabel("enterFirstName", initialLanguage)}`}
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                name="text"
                autoComplete="new-text"
              />
              {error.first_name && (
                <div className="text-danger">{error.first_name}</div>
              )}
            </div>
          </div>
        </div>

        <div className="mb-3">
          <div className="row m-0">
            <div className="col-12 p-0 font-semibold">
              {getLabel("lastName", initialLanguage)}
              {/* <span className="text-danger">*</span> */}
            </div>
            <div className="col-12 p-0">
              <input
                type="text"
                className="form-control border border-2 p-2 system-control "
                placeholder={`${getLabel("enterLastName", initialLanguage)}`}
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                name="text"
                autoComplete="new-text"
              />
              {error.last_name && (
                <div className="text-danger">{error.last_name}</div>
              )}
            </div>
          </div>
        </div>
        {/* <div className="mb-3">
          <div className="row m-0">
            <div className="col-12 p-0 font-semibold">
              {getLabel("gender",initialLanguage)}
              <span className="text-danger">*</span>
            </div>
            <div className="col-12 p-0 ">
              <select
                className="form-select border border-2 bg-white fw-normal p-2 system-control"
                id="dropdownMenuLink"
                onChange={handleGenderChange}
              >
                <option value="" disabled selected>
                  {getLabel("selectAGender",initialLanguage)}
                </option>
                {Array.isArray(gendersData) &&
                  gendersData.map((option: any) => (
                    <option key={option.id} value={option.id}>
                      {option.name}
                    </option>
                  ))}
              </select>
              {error.gender_id && (
                <div className="text-danger">{error.gender_id}</div>
              )}
            </div>
          </div>
        </div> */}

        <div className="mb-3">
          <div className="row m-0">
            <div className="col-12 p-0 font-semibold">
              {getLabel("country", initialLanguage)}<span className="ps-1">*</span>
              {/* <span className="text-danger">*</span> */}
            </div>
            <div className="col-12 p-0 ">
              <CustomSelect
                data={showCountriresData}
                placeholderValue={getLabel("selectACountry", initialLanguage)}
                onChange={(value: any) => setCountryId(value)}
                is_search={true}
                defaultValue={countryId}
              />
              {error.country_id && (
                <div className="text-danger">{error.country_id}</div>
              )}
            </div>
          </div>
        </div>

        {/* <div className="mb-3">
          <div className="row m-0">
            <div className="col-12 p-0 font-semibold">
              {getLabel("password",initialLanguage)}
              <span className="text-danger">*</span>
            </div>
            <div className="col-12 p-0 ">
              <input
                type="password"
                className="form-control border border-2 p-2 system-control font-bold"
                placeholder={`${getLabel("enterPassword",initialLanguage)}`}
                value={password}
                onChange={(e) => setpassword(e.target.value)}
                name="password"
                autoComplete="new-password"
              />
              {error.password && (
                <div className="text-danger">{error.password}</div>
              )}
            </div>
          </div>
        </div> */}

        {/* <div className="mb-3">
          <div className="row m-0">
            <div className="col-12 p-0 font-semibold">
              {getLabel("confirmPassword",initialLanguage)}
              <span className="text-danger">*</span>
            </div>
            <div className="col-12 p-0 ">
              <input
                type="password"
                className="form-control border border-2 p-2 system-control font-bold"
                placeholder={`${getLabel("confirmPassword",initialLanguage)}`}
                value={confirmPassword}
                onChange={(e) => setConfirmPassword(e.target.value)}
                name="password"
                autoComplete="new-password"
              />
              {error.password_confirmation && (
                <div className="text-danger">{error.password_confirmation}</div>
              )}
            </div>
          </div>
        </div> */}

        <div className="mb-3">
          <div className="row m-0">
            <div className="col-12 p-0 font-semibold">
              {getLabel("mobile", initialLanguage)}<span className="ps-1">*</span>
              {/* <span className="text-danger">*</span> */}
            </div>
            <div className="col-12 p-0">
              <PhoneNumberInput
                id="phone"
                setMobile={setMobile}
                mobile={mobile}
                defaultCountryCode="+94"
                handleBlur={handleBlurMobile}
              />
              {/* {error.mobile && (
                <div className="text-danger">{error.mobile}</div>
              )} */}
              {errorShowMobile ? (
                <>
                  <div className="text-danger my-1">
                    {getLabel("weHaveFoundCustomerMobile", initialLanguage)}
                  </div>
                  <div className="rounded system-red-light-500">
                    <div className="row m-0 text-truncate">
                      <div className="col-12 col-sm-6 my-2">
                        <p className="m-0 p-0 font-bold line-height-3 font-130 text-truncate">Mr. Sanj*** Nav***</p>
                        <p className="m-0 p-0 text-truncate">sanjay@gmail.com,<span className="m-0 p-0 ps-1">+9477721*****</span></p>
                        <p className="m-0 p-0 text-truncate">Sri Lanka</p>
                      </div>
                      <div className="col-12 col-sm-6 text-start text-sm-end mb-3 mt-0 mt-sm-2 pt-0 pt-sm-1">
                        <button
                          className="btn btn-system-primary pointer py-0"
                          onClick={continuewithmail}
                        >
                          Use this customer
                        </button>
                      </div>
                    </div>
                  </div>
                </>
              ) : error.mobile ? (
                <div className="text-danger">{error.mobile}</div>
              ) : null}
            </div>
          </div>
        </div>


        <div className="mb-3">
          <div className="row m-0">
            <div className="col-12 p-0 font-semibold">
              {getLabel("alternativeNumber", initialLanguage)}
            </div>
            <div className="col-12 p-0">
              <PhoneNumberInput
                id="alternativePhone"
                setMobile={setAlternativeNumber}
                mobile={alternativeNumber}
              />
              {error.alternative_phone && (
                <div className="text-danger">{error.alternative_phone}</div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
);

export default CustomerDetailsModal;
