import { ACTIVE_PARTNER } from "@/config/constants";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import React, {
  useEffect,
  useState,
  forwardRef,
  useImperativeHandle,
  useContext,
} from "react";
// import { Notification } from "@arco-design/web-react";
import { useApiErrorHandling } from "../Others/useApiErrorHandling";
import Image from "next/image";
import { getAllSalutations, identitySelector } from "@/redux/identities";
import {
  createPartnerAdmin,
  getAllPartnerAdmins,
  getPartnerAdmin,
  partnerAdminSelector,
  updatePartnerAdmin,
} from "@/redux/partner-admins";
import { getLabel } from "@/utils/lang-manager";
import CustomSelect from "../Arco/CustomSelect";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";
export interface ChildModalRefUpdate {
  callModalFunctionUpdate: (callback: (response: any) => void) => void;
}
export interface ApiResponse {
  msg: string;
  code: number;
  data: any;
  error: null;
  success: boolean;
  status: any;
}

const ViewPartnerAdminModal = forwardRef<ChildModalRefUpdate>((props, ref1) => {
  const { NoticeData } = useContext(NoticeDataContext);
  const { PageData } = useContext(PageDataContext);
  const initialLanguage =
    NoticeData?.initialLanguage || PageData?.initialLanguage;
  const [selectedCustomer, setselectedCustomer] = useState(0);
  const dispatch = useAppDispatch();
  const { error: apiError } = useAppSelector(partnerAdminSelector);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setpassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [status, setStatus] = useState(1);
  const [response, setResponse] = useState("");
  const [loginPswd, setLoginPswd] = useState(false);
  const [loginGoogle, setLoginGoogle] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };
  const toggleConfirmPasswordVisibility = () => {
    setShowConfirmPassword(!showConfirmPassword);
  };

  const callModalFunctionUpdate = (callback: any) => {
    const updatedData = {
      first_name: firstName,
      last_name: lastName,
      email: email,
      mobile: status,
      password: password,
      password_confirmation: confirmPassword,
      state_id: status,
      salutation_id: salutationId,
      login_with_password: loginPswd,
      login_with_google: loginGoogle,
    };
    console.log("updatedData", updatedData);

    dispatch(
      updatePartnerAdmin({
        active_partner: ACTIVE_PARTNER,
        partner_admin_uuid: partnerSingleAdminsData.uuid,
        updatedData,
      })
    ).then((response: any) => {
      if (response.payload.success == true) {
        //   Notification.success({
        //     title: "Partner admin has been updated Successfully",
        //     duration: 3000,
        //     content: undefined,
        //   });
        //   console.log(response.payload.data.data.id);
        //   setselectedCustomer(response?.payload?.data?.data?.id);
        //   callback(response?.payload?.data?.data);
        //   setResponse(response);
        // } else {
        //   Notification.error({
        //     title: "Partner admin updated Failed",
        //     duration: 3000,
        //     content: undefined,
        //   });
      }
    });
  };
  useImperativeHandle(ref1, () => ({
    callModalFunctionUpdate,
  }));
  useEffect(() => {}, [apiError]);
  useEffect(() => {
    loadStaticData();
  }, []);
  const loadStaticData = async () => {
    dispatch(getAllSalutations());
  };
  const { data: showSalutationsData } = useAppSelector(identitySelector);
  const [salutationId, setSalutationID] = useState(1);
  const handleSalutationChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedValue = e.target.value;
    selectedValue;
    setSalutationID(parseInt(selectedValue, 10));
  };
  const error = useApiErrorHandling(apiError, response);
  const statusOptions = [
    { id: 1, name: "Enable" },
    { id: 2, name: "Disable" },
  ];
  // const getSinglePartnerAdmin = async (id: number) => {
  //     dispatch(
  //         getPartnerAdmin({
  //             active_partner: ACTIVE_PARTNER,
  //             partner_admin_uuid: id,
  //         })
  //     );
  // };
  // const getAdmin = () => {
  //   dispatch(
  //     getAllPartnerAdmins({
  //       active_partner: ACTIVE_PARTNER,
  //       page: 1,
  //     })
  //   ).then(() => {});
  // };
  useEffect(() => {
    dispatch(
      getPartnerAdmin({
        active_partner: ACTIVE_PARTNER,
        partner_admin_uuid: localStorage.getItem("edit_partner_id") as string,
      })
    );
  }, []);
  const { singleData: partnerSingleAdminsData } =
    useAppSelector(partnerAdminSelector);
  console.log("85", partnerSingleAdminsData);
  useEffect(() => {
    setFirstName(partnerSingleAdminsData?.first_name);
    setLastName(partnerSingleAdminsData?.last_name);
    setEmail(partnerSingleAdminsData?.email);
    setStatus(partnerSingleAdminsData?.state?.id);
    setpassword(partnerSingleAdminsData?.password);
    setConfirmPassword(partnerSingleAdminsData?.password_confirmation);
    setLoginPswd(partnerSingleAdminsData?.login_with_password);
    setLoginGoogle(partnerSingleAdminsData?.login_with_google);
    setSalutationID(partnerSingleAdminsData?.salutation?.id);
  }, [partnerSingleAdminsData]);
  return (
    <div>
      {/* {partnerSingleAdminsData.map((item: any) => ( */}
      <div>
        <div className="mb-3">
          <div className="row m-0">
            <div className="col-12 p-0 font-semibold">
              {getLabel("status", initialLanguage)}
              <span className="ps-1">*</span>
              {/* <span className="text-danger">*</span> */}
            </div>
            <div className="col-12 p-0">
              {/* <select
              className="form-select border border-2 p-2 system-control font-bold"
              value={status}
              onChange={(e) => setStatus(e.target.value)}
            >
              {statusOptions.map((option) => (
                <option key={option.id} value={option.id}>
                  {option.label}
                </option>
              ))}
            </select> */}
              <CustomSelect
                data={statusOptions}
                placeholderValue={getLabel("selectAStatus", initialLanguage)}
                onChange={(value: any) => setStatus(value)}
                is_search={false}
                defaultValue={
                  // partnerSingleAdminsData
                  //   ? partnerSingleAdminsData?.state?.id
                  //   : status
                  status ? partnerSingleAdminsData?.state?.id : status
                }
              />
              {error.status && (
                <div className="text-danger">{error.status}</div>
              )}
            </div>
          </div>
        </div>
        <div className="mb-3">
          <div className="row m-0">
            <div className="col-12 p-0 font-semibold">
              {getLabel("firstName", initialLanguage)}
              <span className="ps-1">*</span>
              {/* <span className="text-danger">*</span> */}
            </div>
            <div className="col-12 p-0">
              <input
                type="text"
                className="form-control border border-2 p-2 system-control "
                // placeholder="Full Name in English"
                value={
                  partnerSingleAdminsData?.first_name == ""
                    ? partnerSingleAdminsData?.first_name
                    : firstName
                }
                onChange={(e) => setFirstName(e.target.value)}
                name="text"
                autoComplete="new-text"
              />
              {error.first_name && (
                <div className="text-danger">{error.first_name}</div>
              )}
            </div>
            {/* <div className="row align-items-center m-0 col-12 p-0 ">
            <div className="col-md-3 col-3 p-0 pe-2 ">
              <select
                className="form-select border border-2 bg-white fw-normal p-2 system-control"
                id="dropdownMenuLink"
                defaultValue={salutationId}
                onChange={handleSalutationChange}
              >
                {Array.isArray(showSalutationsData) &&
                  showSalutationsData.map((option: any) => (
                    <option key={option.id} value={option.id}>
                      {option.name}
                    </option>
                  ))}
              </select>
                 <CustomSelect
                    data={showSalutationsData}
                    placeholderValue={getLabel("selectATitle")}
                    onChange={(value: any) => setSalutationID(value)}
                    is_search={true}
                    defaultValue={salutationId}
                  />
            </div>
            <div className="col-md-9 col-9 p-0">
              <input
                type="text"
                className="form-control border border-2 p-2 system-control "
                placeholder="Full Name in English"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                name="text"
                autoComplete="new-text"
              />
            </div>
          </div> */}
            {/* <div className="row align-items-center m-0 col-12 p-0 ">
            <div className="col-md-3 col-3 p-0 pe-2 "></div>
            <div className="col-md-9 col-9 p-0">
              {error.first_name && (
                <div className="text-danger">{error.first_name}</div>
              )}
            </div>
          </div> */}
          </div>
        </div>
        <div className="mb-3">
          <div className="row m-0">
            <div className="col-12 p-0 font-semibold">
              {getLabel("lastName", initialLanguage)}
              {/* <span className="text-danger">*</span> */}
            </div>
            <div className="col-12 p-0">
              <input
                type="text"
                className="form-control border border-2 p-2 system-control "
                // placeholder="Enter Last Name"
                value={
                  // partnerSingleAdminsData
                  //   ? partnerSingleAdminsData.last_name
                  //   : lastName
                  partnerSingleAdminsData?.last_name == ""
                    ? partnerSingleAdminsData?.last_name
                    : lastName
                }
                onChange={(e) => setLastName(e.target.value)}
                name="text"
                autoComplete="new-text"
              />
              {error.last_name && (
                <div className="text-danger">{error.last_name}</div>
              )}
            </div>
          </div>
        </div>
        <div className="mb-3">
          <div className="row m-0">
            <div className="col-12 p-0 font-semibold">
              {getLabel("email", initialLanguage)}
              <span className="ps-1">*</span>
              {/* <span className="text-danger">*</span> */}
            </div>
            <div className="col-12 p-0 ">
              <input
                type="text"
                className="form-control border border-2 p-2 system-control "
                // placeholder="Enter Email"
                value={
                  // partnerSingleAdminsData
                  //   ? partnerSingleAdminsData.email
                  //   : email
                  partnerSingleAdminsData?.email == ""
                    ? partnerSingleAdminsData?.email
                    : email
                }
                onChange={(e) => setEmail(e.target.value)}
                name="email"
                autoComplete="new-email"
              />
              {error.email && <div className="text-danger">{error.email}</div>}
            </div>
          </div>
        </div>
        <div className="mb-3">
          <div className="row m-0">
            <div className="col-12 p-0 font-semibold">
              {getLabel("password", initialLanguage)}
              <span className="ps-1">*</span>
              {/* <span className="text-danger">*</span> */}
            </div>
            <div className="col-12 p-0 position-relative ">
              <input
                type={showPassword ? "text" : "password"}
                className="form-control border border-2 p-2 system-control "
                // placeholder="Enter Password"
                value={password}
                onChange={(e) => setpassword(e.target.value)}
                name="password"
                autoComplete="new-password"
              />
              <div className="">
                <Image
                  onClick={togglePasswordVisibility}
                  src={showPassword ? "/openeye.svg" : "/closeeye.svg"}
                  alt={showPassword ? "eyeIcon icon" : "closeeye icon"}
                  width={16}
                  height={16}
                  className="input-group-eye"
                />
              </div>
              {error.password && (
                <div className="text-danger">{error.password}</div>
              )}
            </div>
          </div>
        </div>
        <div className="mb-3">
          <div className="row m-0">
            <div className="col-12 p-0 font-semibold">
              {getLabel("confirmPassword", initialLanguage)}
              <span className="ps-1">*</span>
              {/* <span className="text-danger">*</span> */}
            </div>
            <div className="col-12 p-0 position-relative">
              <input
                type={showConfirmPassword ? "text" : "password"}
                className="form-control border border-2 p-2 system-control "
                // placeholder="Confirm Password"
                value={confirmPassword}
                onChange={(e) => setConfirmPassword(e.target.value)}
                name="password"
                autoComplete="new-password"
              />
              <div className="">
                <Image
                  onClick={toggleConfirmPasswordVisibility}
                  src={showConfirmPassword ? "/openeye.svg" : "/closeeye.svg"}
                  alt={showConfirmPassword ? "eyeIcon icon" : "closeeye icon"}
                  width={16}
                  height={16}
                  className="input-group-eye"
                />
              </div>
              {error.password_confirmation && (
                <div className="text-danger">{error.password_confirmation}</div>
              )}
            </div>
          </div>
        </div>

        <div className="row m-0 mb-3">
          <div className="col-12 col-md-6 d-flex align-items-center gap-3 mb-2 p-0">
            <input
              type="checkbox"
              className=" check-button2"
              // placeholder=""
              checked={loginPswd}
              onChange={() => setLoginPswd(!loginPswd)}
            />
            <div className="col-md-10 col-10 p-0 font-semibold">
              {getLabel("loginWithPassword", initialLanguage)}
            </div>
          </div>
          <div className="col-12 col-md-6 d-flex align-items-center gap-3  mb-2 p-0">
            <input
              type="checkbox"
              className=" check-button2 "
              // placeholder=""
              checked={loginGoogle}
              onChange={() => setLoginGoogle(!loginGoogle)}
            />

            <div className=" col-md-10 col-10 p-0 font-semibold ">
              {getLabel("loginWithGoogle", initialLanguage)}
            </div>
          </div>
        </div>

        <div className="mb-3">
          <div className="row m-0">
            <div className="col-12 p-0 font-bold mb-3">
              {getLabel("assignServiceProviders", initialLanguage)}
              <span className="ps-1">*</span>
            </div>
          </div>
          <div className="d-flex align-items-center gap-3 mb-2">
            <input type="checkbox" className=" check-button2" placeholder="" />
            <div className="font-semibold">Service Provider A</div>
          </div>
          <div className="d-flex align-items-center gap-3">
            <input type="checkbox" className=" check-button2" placeholder="" />
            <div className="font-semibold">Service Provider B</div>
          </div>
        </div>
      </div>
      {/* ))} */}
    </div>
  );
});
export default ViewPartnerAdminModal;
