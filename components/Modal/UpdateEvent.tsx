import { getLabel } from "@/utils/lang-manager";
import { DatePicker, Radio, TimePicker } from "@arco-design/web-react";
import CustomSelect from "../Arco/CustomSelect";
import PlaceAutocomplete from "../Elements/PlaceAutoComplete";
import Image from "next/image";
import { useContext, useEffect, useState } from "react";
import addMore from "../../public/add-more.svg";
import trash from "../../public/trash.svg";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import { getOneEvent, noticeEventSelector } from "@/redux/notices-event";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import { useRouter } from "next/router";
import {
  getAllEventLocationTypes,
  eventLocationTypesReducer,
} from "@/redux/event-location-type";
import { EventTypeSelector, getAllEventsTypes } from "@/redux/event-types";
import CustomTextBox from "../Elements/CustomTextBox";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";
import CustomSelect1 from "../Arco/CustomSelect1";
interface UpdateEventProps {
  response: any;
  actionBtnRef: any;
  handleEventUpdate: (updateData: any) => void;
  data: any;
}
const showEvent = ({
  handleEventUpdate,
  actionBtnRef,
  response,
  data,
}: UpdateEventProps) => {
  const { NoticeData } = useContext(NoticeDataContext);
  const { PageData } = useContext(PageDataContext);
  const initialLanguage =
    NoticeData?.initialLanguage || PageData?.initialLanguage;
  const statusOptions = [
    { id: 1, name: "Enable" },
    { id: 2, name: "Disable" },
  ];
  const FlowerDelivery = [
    { id: 1, name: "Yes" },
    { id: 2, name: "No" },
  ];
  const [isDeliveryPossible, setIsDeliveryPossible] = useState(true);
  const [status, setStatus] = useState(1);
  const [eventTypeId, seteventTypeId] = useState(1);
  const [eventDate, setEventDate] = useState("");
  const tab = "Venue";
  const [activeTab, setActiveTab] = useState(tab);
  const router = useRouter();
  const dispatch = useAppDispatch();
  const uuid = router.query?.id;
  const { dataOne: oneEventData } = useAppSelector(noticeEventSelector);
  const { data: eventLocationType } = useAppSelector(eventLocationTypesReducer);
  const { data: eventTypeData } = useAppSelector(EventTypeSelector);
  // const OneEvent = (id: string) => {
  //   dispatch(
  //     getOneEvent({
  //       active_partner: ACTIVE_PARTNER,
  //       active_service_provider: ACTIVE_SERVICE_PROVIDER,
  //       notice_uuid: uuid as unknown as string,
  //       uuid: id,
  //     })
  //   ).then((res: any) => {
  //     changeTab(res?.payload?.data?.event_location_type?.name);
  //   });
  // };
  // useEffect(() => {
  //   OneEvent(response?.data?.id);
  // }, []);
  //------------------------Venue-----------------------------------------------------------------------//
  const [specialVenueInstructions, setSpecialVenueInstructions] = useState("");
  const [specialVenueInstructionsLength, setSpecialVenueInstructionsLength] =
    useState(0);
  const [addressVenue, setAddressVenue] = useState("");
  //--------------------------Home------------------------------------------------------------------------//
  const [specialHomeInstructions, setSpecialHomeInstructions] = useState("");
  const [specialHomeInstructionsLength, setSpecialHomeInstructionsLength] =
    useState(0);
  const [addressHome, setAddressHome] = useState("");
  //--------------------------VirtualEvent----------------------------------------------------------------//
  const [
    specialVirtualMeetingInstructions,
    setSpecialvirtualMeetingInstruction,
  ] = useState("");
  const [virtualMeetingEventInstructions, setVirtualMeetingEventInstructions] =
    useState("");
  const [
    specialVirtualMeetingInstructionsLength,
    setSpecialVirtualMeetingInstructionsLength,
  ] = useState(0);
  const [
    virtualMeetingEventInstructionsLength,
    setvirtualMeetingEventInstructionsLength,
  ] = useState(0);
  const [eventLocationTypeId, setEventLocationTypeId] = useState(1);
  useEffect(() => {
    dispatch(getAllEventsTypes());
    dispatch(getAllEventLocationTypes());
    dispatch(getAllEventsTypes());
    dispatch(getAllEventLocationTypes());
    setActiveTab("Venue");
  }, []);
  useEffect(() => {
    switch (oneEventData?.event_location_type?.id) {
      case 1:
        setSpecialVenueInstructions(oneEventData?.instructions);
        setSpecialVenueInstructionsLength(
          oneEventData?.instructions?.length || 0
        );
        setAddressVenue(oneEventData?.address);
        setActiveTab("Venue");
        break;
      case 2:
        setSpecialHomeInstructions(oneEventData?.instructions);
        setSpecialHomeInstructionsLength(
          oneEventData?.instructions?.length || 0
        );
        setAddressHome(oneEventData?.address);
        setActiveTab("Home");
        break;
      case 3:
        setSpecialvirtualMeetingInstruction(oneEventData?.instructions);
        setVirtualMeetingEventInstructions(oneEventData?.event_instructions);
        setvirtualMeetingEventInstructionsLength(
          oneEventData?.event_instructions?.length || 0
        );
        setSpecialVirtualMeetingInstructionsLength(
          oneEventData?.instructions?.length || 0
        );
        setActiveTab("Virtual Meeting");
        break;
      default:
    }
    setEventLocationTypeId(oneEventData?.event_location_type?.id);
    seteventTypeId(oneEventData?.event_type?.id);
    setStatus(oneEventData?.state?.id);
    setIsDeliveryPossible(
      oneEventData?.is_delivery_possible === 1 ? true : false
    );
  }, [oneEventData]);
  //-----------Event time selection---------------------------------------------------------------------------------//
  const defaultValue = "1";
  const [timeFrames, setTimeFrames] = useState([
    { id: defaultValue, startTime: "", endTime: "" },
  ]);
  const [disable, setDisable] = useState(false);
  const [defaultVal, setDefaultVal] = useState("default");
  const [isSingleTimeFrameWithValues, setIsSingleTimeFrameWithValues] =
    useState(true);
  const checkSingleTime = () => {
    setIsSingleTimeFrameWithValues(
      timeFrames.length === 1 &&
        (timeFrames[0].startTime?.trim() !== "" ||
          timeFrames[0].endTime?.trim() !== "")
    );
  };
  useEffect(() => {
    setIsSingleTimeFrameWithValues(
      timeFrames.length === 1 &&
        (timeFrames[0].startTime || timeFrames[0].endTime ? true : false)
    );
  }, [timeFrames]);
  const handleAddTimeFrame = () => {
    setTimeFrames([
      ...timeFrames,
      { id: Date.now().toString(), startTime: "", endTime: "" },
    ]);
  };
  const handleRemoveTimeFrame = (id: string) => {
    // if (id === defaultValue) {
    //   return; // Prevent removing the default field
    // }
    setTimeFrames(timeFrames.filter((timeFrame) => timeFrame.id !== id));
  };
  // Updating startTime and endTime for the first object in timeFrames
  const updateStartTimeAndEndTime = () => {
    setTimeFrames((prevTimeFrames) => [
      {
        ...prevTimeFrames[0],
        startTime: "",
        endTime: "",
      },
      ...prevTimeFrames.slice(1),
    ]);
  };
  const setToZeroAndRemoveOthers = () => {
    setTimeFrames((prevTimeFrames) => [
      {
        ...prevTimeFrames[0],
        // startTime: "00:00 AM",
        // endTime: "00:00 AM",
        startTime: "",
        endTime: "",
      },
    ]);
  };
  const handleStartTimeChange = (index: number, value: any) => {
    const newTimeFrames = [...timeFrames];
    newTimeFrames[index].startTime = value;
    setTimeFrames(newTimeFrames);
  };
  const handleEndTimeChange = (index: number, value: any) => {
    const newTimeFrames = [...timeFrames];
    newTimeFrames[index].endTime = value;
    setTimeFrames(newTimeFrames);
  };
  const setToMidnight = () => {
    setToZeroAndRemoveOthers();
    setDisable(true);
    setDefaultVal("custom");
  };
  const setToDefault = () => {
    updateStartTimeAndEndTime();
    setDisable(false);
    setDefaultVal("default");
  };
  const changeTab = (val: any) => {
    setActiveTab(val);
  };
  const updateData = {
    state_id: status,
    address: addressHome || addressVenue,
    event_instructions: virtualMeetingEventInstructions,
    instructions:
      specialHomeInstructions ||
      specialVenueInstructions ||
      specialVirtualMeetingInstructions,
    is_delivery_possible: isDeliveryPossible,
    event_location_type_id: eventLocationTypeId,
    event_type_id: eventTypeId,
  };
  const [clickstatus, setClickStatus] = useState(false);
  useEffect(() => {
    actionBtnRef.current.onclick = () => {
      setClickStatus(true);
    };
    if (clickstatus) {
      handleEventUpdate(updateData);
      setClickStatus(false);
    }
  }, [clickstatus]);
  return (
    <>
      <div className="col-12 p-0 mb-3">
        <div className=" font-semibold">
          {getLabel("eventType", initialLanguage)}
          <span className="ps-1">*</span>
        </div>
        <CustomSelect
          data={eventTypeData}
          placeholderValue={getLabel("selectAEvent", initialLanguage)}
          onChange={(value: any) => {
            seteventTypeId(value);
          }}
          is_search={true}
          defaultValue={data.event_type.id}
        />
      </div>
      <div className="row m-0">
        <div className="col-md-6 col-12 p-0 mb-3 mb-md-0 pe-md-2">
          <div className=" font-semibold mb-1">
            {getLabel("date", initialLanguage)}
            <span className="ps-1">*</span>
          </div>
          <DatePicker
            className="form-control border border-2 system-control py-3"
            value={eventDate}
            format={"DD-MMM-YYYY"}
            onChange={(date: string) => setEventDate(date)}
            getPopupContainer={(node) => {
              const container = node.parentElement;
              if (container) {
                return container;
              }
              return document.body;
            }}
          />
        </div>
        <div className="col-md-6 col-12 p-0">
          <div className="mb-md-4 bg-danger"></div>
          <div className="ps-md-2 event-time-radio ">
            <Radio.Group
              mode="fill"
              type="button"
              defaultValue={defaultVal}
              className="font-semibold"
            >
              <Radio
                value="default"
                className={`pe-1 ${defaultVal == "default" ? "b-border" : ""}`}
                onClick={defaultVal == "custom" ? setToDefault : undefined}
              >
                Custom Hours
              </Radio>
              <span className="vertical-separator"></span>
              <Radio
                value="custom"
                className={`ps-1 ${defaultVal == "custom" ? "b-border" : ""}`}
                onClick={setToMidnight}
              >
                Full day event
              </Radio>
            </Radio.Group>
          </div>
          <>
            <div className="row  m-0 mb-1 ps-md-1">
              <label className="font-90 font-bold col-5 p-0 px-2 ">
                {getLabel("startTime", initialLanguage)}
              </label>
              <label className="font-90 font-bold col-5 p-0 px-2">
                {getLabel("endTime", initialLanguage)}
              </label>
            </div>
            {timeFrames.map((timeFrame, index) => (
              <div
                key={timeFrame.id}
                className={`row m-0 mb-2 ps-md-2   ${
                  disable ? "event-disabled" : ""
                }`}
              >
                <div className="col-5 p-0 pe-1 evnt">
                  <TimePicker
                    use12Hours
                    format="h:mm A"
                    value={timeFrame.startTime}
                    placeholder={disable ? "00:00" : "Start Time"}
                    className="form-control border system-control p-2 "
                    disabled={disable}
                    onChange={(value) => handleStartTimeChange(index, value)}
                    onClear={() => checkSingleTime()}
                    getPopupContainer={(node) => {
                      const container = node.parentElement;
                      if (container) {
                        return container;
                      }
                      // Return a default element if needed
                      // For example, you can return document.body as the fallback
                      return document.body;
                    }}
                  />
                </div>
                <div className="col-5  p-0 ps-1 evnt">
                  <TimePicker
                    use12Hours
                    format="h:mm A"
                    value={timeFrame.endTime}
                    placeholder={disable ? "00:00" : " End Time"}
                    className="form-control border system-control p-2 "
                    disabled={disable}
                    onChange={(value) => handleEndTimeChange(index, value)}
                    onClear={() => checkSingleTime()}
                    getPopupContainer={(node) => {
                      const container = node.parentElement;
                      if (container) {
                        return container;
                      }
                      // Return a default element if needed
                      // For example, you can return document.body as the fallback
                      return document.body;
                    }}
                  />
                </div>
                {timeFrames.length == 1 &&
                  index == 0 &&
                  !disable &&
                  !isSingleTimeFrameWithValues && (
                    <div className="col-2 p-0 pe-1 d-flex align-items-center justify-content-end">
                      <Image
                        src={addMore}
                        alt="add-more"
                        onClick={handleAddTimeFrame}
                        className="pointer"
                      />
                    </div>
                  )}
                {(index !== 0 ||
                  (timeFrames.length > 1 && index == 0) ||
                  isSingleTimeFrameWithValues) &&
                  !disable && (
                    <div className="col-2 p-0 pe-1 d-flex align-items-center justify-content-end">
                      <Image
                        src={trash}
                        alt="trash"
                        onClick={() => {
                          if (timeFrames.length !== 1) {
                            handleRemoveTimeFrame(timeFrame.id);
                          } else {
                            updateStartTimeAndEndTime();
                          }
                        }}
                        className="pointer"
                      />
                    </div>
                  )}
              </div>
            ))}
            {(isSingleTimeFrameWithValues || timeFrames.length > 1) &&
              !disable && (
                <div className="row  mb-1 ps-md-2 ">
                  <p className="font-semibold m-0 ">
                    <span
                      className="highlighted-text font-bold system-text-primary pointer text-decoration-underline"
                      onClick={handleAddTimeFrame}
                    >
                      Add
                    </span>
                    &nbsp;additional time
                  </p>
                </div>
              )}
          </>
        </div>
      </div>
      <div className="row m-0 ">
        <div className="col-12 p-0  mb-3 ">
          <div className=" font-semibold">
            {getLabel("eventLocation", initialLanguage)}
          </div>
          <div className="row m-0 " id="filter-panel ">
            <div className="col-12 p-0  ">
              <div className="inpage-sidebar ">
                <div className="row  m-0">
                  {eventLocationType.map((item: any) => (
                    <div
                      className={
                        activeTab === item.name
                          ? `py-2 system-text-primary shadow-md font-semibold active font-100 system-warning-light col-4 d-flex align-items-center border border-2  system-border-warning ${
                              item.name === "Venue"
                                ? "rounded-start border-end-0"
                                : ""
                            } ${
                              item.name === "Virtual Meeting"
                                ? "rounded-end"
                                : ""
                            } ${item.name === "Home" ? "border-end-0" : ""}`
                          : `py-2 rounded-end-0 shadow-md font-semibold font-100 col-4 d-flex align-items-center border border-2 system-border-warning ${
                              item.name === "Venue"
                                ? "rounded-start border-end-0"
                                : ""
                            } ${
                              item.name === "Virtual Meeting"
                                ? "rounded-end "
                                : ""
                            } ${item.name === "Home" ? "border-end-0" : ""}`
                      }
                      onClick={() => {
                        changeTab(item.name);
                        setEventLocationTypeId(item.id);
                      }}
                    >
                      {item.name}
                    </div>
                  ))}
                </div>
              </div>
            </div>
            <div className="col-12 p-0 pt-3">
              <div className="">
                {activeTab === "Venue" && (
                  <>
                    {" "}
                    <div className="row m-0 ">
                      <div className="col-12 p-0 mb-3">
                        <div className=" font-semibold">
                          {getLabel("eventPlaceSearch", initialLanguage)}
                        </div>
                        <PlaceAutocomplete
                          className="form-control border border-2 p-2 system-control"
                          placeholder={"Search city of Death"}
                          onSuggestionClick={(value) => {
                            if (value) {
                              setSpecialHomeInstructions("");
                              setSpecialHomeInstructionsLength(0);
                              setVirtualMeetingEventInstructions("");
                              setvirtualMeetingEventInstructionsLength(0);
                              setSpecialVirtualMeetingInstructionsLength(0);
                              setSpecialvirtualMeetingInstruction("");
                              setAddressHome("");
                              setAddressVenue(value);
                            }
                          }}
                          dvalue={addressVenue}
                        />
                      </div>
                    </div>
                    <div className="row m-0">
                      <div className=" col-12 p-0 mb-3">
                        <div className=" font-semibold">
                          {getLabel("specialInstruction", initialLanguage)}
                        </div>
                        <div className=" position-relative">
                          <CustomTextBox
                            value={specialVenueInstructions}
                            placeholder={`${getLabel(
                              "specialInstruction",
                              initialLanguage
                            )}`}
                            onChange={(e) => {
                              setSpecialVenueInstructions(e.target.value);
                              setSpecialVenueInstructionsLength(
                                e.target.value.length
                              );
                              if (e.target.value) {
                                setSpecialHomeInstructions("");
                                setSpecialHomeInstructionsLength(0);
                                setVirtualMeetingEventInstructions("");
                                setvirtualMeetingEventInstructionsLength(0);
                                setSpecialVirtualMeetingInstructionsLength(0);
                                setSpecialvirtualMeetingInstruction("");
                                setAddressHome("");
                              }
                            }}
                            maxLength={400}
                          />
                        </div>
                      </div>
                    </div>
                  </>
                )}
                {activeTab === "Virtual Meeting" && (
                  <>
                    <div className="row m-0">
                      <div className=" col-12 p-0 mb-3">
                        <div className=" font-semibold">
                          {getLabel("eventInstruction", initialLanguage)}
                        </div>
                        <div className=" position-relative">
                          <CustomTextBox
                            value={virtualMeetingEventInstructions}
                            placeholder={`${getLabel(
                              "eventInstructionPlaceholder",
                              initialLanguage
                            )}`}
                            onChange={(e) => {
                              setVirtualMeetingEventInstructions(
                                e.target.value
                              );
                              setvirtualMeetingEventInstructionsLength(
                                e.target.value.length
                              );
                              if (e.target.value) {
                                setSpecialVenueInstructions("");
                                setSpecialVenueInstructionsLength(0);
                                setSpecialHomeInstructions("");
                                setSpecialHomeInstructionsLength(0);
                                setAddressHome("");
                                setAddressVenue("");
                              }
                            }}
                            maxLength={400}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="row m-0">
                      <div className=" col-12 p-0 mb-3">
                        <div className=" font-semibold">
                          {getLabel("specialInstruction", initialLanguage)}
                        </div>
                        <div className=" position-relative">
                          <CustomTextBox
                            value={specialVirtualMeetingInstructions}
                            placeholder={`${getLabel(
                              "specialInstruction",
                              initialLanguage
                            )}`}
                            onChange={(e) => {
                              if (e.target.value) {
                                setSpecialVenueInstructions("");
                                setSpecialVenueInstructionsLength(0);
                                setSpecialHomeInstructions("");
                                setSpecialHomeInstructionsLength(0);
                                setAddressHome("");
                                setAddressVenue("");
                              }

                              setSpecialVirtualMeetingInstructionsLength(
                                e.target.value.length
                              );
                              setSpecialvirtualMeetingInstruction(
                                e.target.value
                              );
                            }}
                            maxLength={400}
                          />
                        </div>
                      </div>
                    </div>
                  </>
                )}
                {activeTab === "Home" && (
                  <>
                    {" "}
                    <div className="row m-0 ">
                      <div className="col-12 p-0 mb-3">
                        <div className=" font-semibold">
                          {getLabel("eventPlaceSearch", initialLanguage)}
                        </div>

                        <PlaceAutocomplete
                          className="form-control border border-2 p-2 system-control"
                          placeholder={`${getLabel(
                            "deathPlacePlaceholder",
                            initialLanguage
                          )}`}
                          onSuggestionClick={(value) => {
                            if (value) {
                              setVirtualMeetingEventInstructions("");
                              setvirtualMeetingEventInstructionsLength(0);
                              setSpecialVirtualMeetingInstructionsLength(0);
                              setSpecialvirtualMeetingInstruction("");
                              setSpecialVenueInstructions("");
                              setSpecialVenueInstructionsLength(0);
                              setAddressVenue("");
                              setAddressHome(value);
                            }
                          }}
                          dvalue={addressHome}
                        />
                      </div>
                    </div>
                    <div className="row m-0">
                      <div className=" col-12 p-0 mb-3">
                        <div className=" font-semibold">
                          {getLabel("specialInstruction", initialLanguage)}
                        </div>
                        <div className=" position-relative">
                          <CustomTextBox
                            value={specialHomeInstructions}
                            placeholder={`${getLabel(
                              "specialInstruction",
                              initialLanguage
                            )}`}
                            onChange={(e) => {
                              setSpecialHomeInstructions(e.target.value);
                              setSpecialHomeInstructionsLength(
                                e.target.value.length
                              );
                              if (e.target.value) {
                                setSpecialVenueInstructions("");
                                setSpecialVenueInstructionsLength(0);
                                setVirtualMeetingEventInstructions("");
                                setvirtualMeetingEventInstructionsLength(0);
                                setSpecialVirtualMeetingInstructionsLength(0);
                                setSpecialvirtualMeetingInstruction("");
                                setAddressVenue("");
                              }
                            }}
                            maxLength={400}
                          />
                        </div>
                      </div>
                    </div>
                  </>
                )}
              </div>
            </div>
          </div>
          {/* tab */}
        </div>
      </div>

      <hr className="mb-3 mt-0 hr-width-form" />
      <div className="row m-0">
        <div className="col-md-4 col-12 p-0 mb-3">
          <div className=" font-semibold">
            {getLabel("status", initialLanguage)}
          </div>
          <CustomSelect1
            data={statusOptions}
            placeholderValue={getLabel("status", initialLanguage)}
            onChange={(value: any) => setStatus(value)}
            is_search={false}
            defaultValue={status}
          />
        </div>
        <div className="col-md-8 col-12 ps-md-2 p-0 mb-3">
          <div className=" font-semibold">
            {getLabel("flowerDeliveryToThisPlace", initialLanguage)}
          </div>
          <CustomSelect
            data={FlowerDelivery}
            placeholderValue={getLabel(
              "flowerDeliveryToThisPlace",
              initialLanguage
            )}
            onChange={(value: any) =>
              setIsDeliveryPossible(value === 1 ? true : false)
            }
            is_search={false}
            defaultValue={isDeliveryPossible ? 1 : 2}
          />
        </div>
      </div>
    </>
  );
};
export default showEvent;
