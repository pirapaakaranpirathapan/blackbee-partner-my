import { getAllRelationships, relationsSelector } from "@/redux/relationships";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import { getLabel } from "@/utils/lang-manager";
import React, { useContext, useEffect, useState } from "react";
import ToggleButton from "react-bootstrap/ToggleButton";
import CustomSelect from "../Arco/CustomSelect";
import { CountriesSelector } from "@/redux/countries";
import CustomSelectIndex from "../Arco/CustomSelectIndex";
import Image from "next/image";
import trash from "../../public/trash.svg";
import { contactTypeSelector, getAllContactTypes } from "@/redux/contact-type";
import { VisibilitiesSelector, getAllVisibilities } from "@/redux/visibilities";
import CustomSelectContact from "../Arco/CustomSelectContact";
import addMore from "../../public/add-more.svg";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";
function ViewContact({
  name,
  country,
  text,
  hideAfterFuneral,
  visibility,
  contactFrame,
  relationship,
  relationshipId,
}: any) {
  const [visibilityId, setVisibilityId] = useState(1);
  const [hideAfterFuneraldata, setHideAfterFuneralData] = useState(false);
  const [disabled, setDisabled] = useState(false);
  const { data: showRelationshipsData } = useAppSelector(relationsSelector);
  console.log("nnnn", contactFrame);
  const { NoticeData } = useContext(NoticeDataContext);
  const { PageData } = useContext(PageDataContext);
  const initialLanguage = NoticeData?.initialLanguage || PageData?.initialLanguage
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(getAllRelationships());
  }, []);
  const { data: showCountriresData } = useAppSelector(CountriesSelector);
  const { data: visibilityData } = useAppSelector(VisibilitiesSelector);
  const [countryId, setCountryId] = useState("1");
  const { data: contactType } = useAppSelector(contactTypeSelector);
  const [contactFrames, setContactFrames] = useState([
    { select: 1, value: "" },
  ]);
  const [contactTypes, setContactTypes] = useState([
    {
      id_or_number: "",
      contact_method_id: "",
    },
  ]);

  useEffect(() => {
    setContactTypes(
      contactFrames.map((item: any) => {
        return {
          id_or_number: item.value,
          contact_method_id: item.select,
        };
      })
    );
  }, [contactFrames]);
  useEffect(() => {
    setContactFrames(contactFrame.map((contact: any) => ({
      select: contact.contact_method.id,
      value: contact.id_or_number,
    })))
  }, [contactFrame]);
  const handleSelectChange = (index: any, selectValue: any) => {
    console.log("index", index);
    console.log("indexselectValue", selectValue);

    const newContactFrames = [...contactFrames];
    console.log("newContactFrames444", newContactFrames);
    const updatedDval = [...dval];
    updatedDval[index] = selectValue;
    setdval(updatedDval);
    newContactFrames[index].select = selectValue;
    setContactFrames(newContactFrames);
  };
  const handleInputChange = (index: any, event: any) => {
    const newContactFrames = [...contactFrames];
    newContactFrames[index].value = event.target.value;
    setContactFrames(newContactFrames);
  };
  const handleRemoveContactFrame = (index: any) => {
    const newContactFrames = contactFrames.filter((_, i) => i !== index);
    setContactFrames(newContactFrames);
    setdval((prevDval) => {
      const updatedDval = [...prevDval];
      updatedDval.splice(index, 1);
      return updatedDval;
    });
  };
  const [error, setError] = useState({
    msg: "",
    name: "",
    first_name: "",
    last_name: "",
    mobile: "",
    email: "",
    alternative_phone: "",
    password: "",
    password_confirmation: "",
    pageTypeId: "",
    gender_id: "",
    contact_type: "",
  });
  const handleAddContactFrame = () => {
    setContactFrames([...contactFrames, { select: 1, value: "" }]);
    setdval((prevDval) => {
      const index = contactFrames.length;
      const updatedDval = [...prevDval];
      updatedDval[index] = 1;
      return updatedDval;
    });
  };
  useEffect(() => {
    dispatch(getAllContactTypes());
    dispatch(getAllVisibilities());
    dispatch(getAllRelationships());
  }, []);
  const [isSingleTimeFrameWithValues, setIsSingleTimeFrameWithValues] =
    useState(false);
  const [dval, setdval] = useState(Array(contactFrames.length).fill(1));

  useEffect(() => {
    setIsSingleTimeFrameWithValues(
      contactFrames.length === 1 && (contactFrames[0].value ? true : false)
    );
  }, [contactFrames]);

  const updateStartTimeAndEndTime = () => {
    const initialDval = Array(contactFrames.length).fill(1);
    setdval(initialDval);
    setContactFrames((prevContactFrames) => [
      {
        ...prevContactFrames[0],
        value: "",
        select: 1,
      },
      ...prevContactFrames.slice(1),
    ]);
  };
  console.log("contactFrame", contactFrame);

  return (
    <div>
      <div>
        <div className="row  align-items-center m-0">
          <div className="col-md-12 col-12 p-0 font-semibold ">
            {getLabel("country", initialLanguage)}
          </div>
          <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-12 p-0 mb-3">
            {/* <select
              className="form-select border border-2 bg-white p-2 system-control"
              id="dropdownMenuLink"
              disabled
            >
              <option value={country}>{country}</option>
            </select> */}

            <CustomSelect
              data={showCountriresData}
              placeholderValue={"Select a country"}
              onChange={(value: any) => {
                setCountryId(value);
              }}
              is_search={true}
              defaultValue={country}
            />
          </div>
          <div className="col-md-12 col-12 p-0 mb-3">
            <div className="font-semibold">{getLabel("personName", initialLanguage)}</div>
            <input
              type="text"
              className="form-control border border-2 p-2 system-control text-truncate"
              placeholder={`${getLabel("personName", initialLanguage)}`}
              value={name}
              disabled={disabled}
            // onChange={(e) => setPersonName(e.target.value)}
            />
          </div>

          <div className="col-md-12 col-12 p-0 mb-3">
            <div className=" font-semibold">
              {getLabel("relationship", initialLanguage)}
              <span className="text-danger">*</span>
            </div>

            <CustomSelect
              data={showRelationshipsData}
              placeholderValue={getLabel("selectRelationship", initialLanguage)}
              onChange={(value: any) => {
                // setRelationship(value);
              }}
              is_search={true}
              defaultValue={relationshipId}
            />
          </div>
        </div>
        {/* ----------------- */}
        <div className="mb-2">
          <div className="row m-0 ">
            <div className="col-4 pe-1 p-0">
              <div className=" font-semibold">
                {getLabel("contactType", initialLanguage)}
                <span className="text-danger ps-1">*</span>
              </div>
            </div>
            <div className="col-7 ps-1 p-0">
              <label className="col-10 p-0  font-semibold ">
                {getLabel("enterContactDetail", initialLanguage)}
              </label>
            </div>
          </div>

          {/* {contactFrames.map((contactFrame, index) => (
            <div key={index} className="row m-0 p-0 mb-2">
              <div className="col-4 pe-1 p-0">
                <CustomSelectIndex
                  key={index}
                  data={contactType}
                  placeholderValue="Select an option"
                  defaultValue={contactFrame.select}
                  is_search={false}
                  handleSelectChange={handleSelectChange}
                  index={index}
                />
              </div>

              <div className="col-7 ps-1 p-0">
                {contactFrame.select === 1 && (
                  <input
                    type="email"
                    pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"
                    value={contactFrame.value}
                    onChange={(event) => handleInputChange(index, event)}
                    className="form-control border border-2 p-2 system-control"
                    placeholder="name@domain.com"
                  />
                )}
                {contactFrame.select === 2 && (
                  <input
                    type="tel"
                    pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"
                    value={contactFrame.value}
                    onChange={(event) => handleInputChange(index, event)}
                    className="form-control border border-2 p-2 system-control"
                    placeholder="+44"
                  />
                )}
                {contactFrame.select === 3 && (
                  <input
                    type="tel"
                    pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"
                    value={contactFrame.value}
                    onChange={(event) => handleInputChange(index, event)}
                    className="form-control border border-2 p-2 system-control"
                    placeholder="+44"
                  />
                )}
                {contactFrame.select === 4 && (
                  <input
                    type="tel"
                    pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"
                    value={contactFrame.value}
                    onChange={(event) => handleInputChange(index, event)}
                    className="form-control border border-2 p-2 system-control"
                    placeholder="+44 , name@domain.com"
                  />
                )}
                {contactFrame.select === 5 && (
                  <input
                    type="tel"
                    pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"
                    value={contactFrame.value}
                    onChange={(event) => handleInputChange(index, event)}
                    className="form-control border border-2 p-2 system-control"
                    placeholder="+44"
                  />
                )}
                {contactFrame.select === 6 && (
                  <input
                    type="email"
                    value={contactFrame.value}
                    onChange={(event) => handleInputChange(index, event)}
                    className="form-control border border-2 p-2 system-control"
                    placeholder="+44"
                  />
                )}
                {contactFrame.select === 7 && (
                  <input
                    type="text"
                    value={contactFrame.value}
                    onChange={(event) => handleInputChange(index, event)}
                    className="form-control border border-2 p-2 system-control"
                    placeholder="+44"
                  />
                )}
              </div>
              <div className="col-1 p-0  d-flex align-items-center justify-content-end ">
                {index !== 0 && (
                  <Image
                    src={trash}
                    alt="trash"
                    onClick={() => {
                      console.log("index", index);

                      handleRemoveContactFrame(index);
                    }}
                  />
                )}
              </div>
            </div>
          ))} */}

          {contactFrames.map((contactFrame: any, index: any) => (
            <div key={index} className="row m-0 p-0 mb-2">
              <div className="col-4 pe-1 p-0">
                <CustomSelectContact
                  key={index}
                  data={contactType}
                  placeholderValue="Select an option"
                  defaultValue={contactFrame.select}
                  is_search={false}
                  handleSelectChange={handleSelectChange}
                  index={index}
                />
              </div>

              <div className="col-7 ps-1 p-0">
                {contactFrame.select === 1 && (
                  <input
                    type="email"
                    pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"
                    value={contactFrame.value ? contactFrame.value : ""}
                    onChange={(event) => handleInputChange(index, event)}
                    className="form-control border border-2 p-2 system-control"
                    placeholder="name@domain.com"
                  />
                )}
                {contactFrame.select === 2 && (
                  <input
                    type="tel"
                    pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"
                    value={contactFrame.value}
                    onChange={(event) => handleInputChange(index, event)}
                    className="form-control border border-2 p-2 system-control"
                    placeholder="+44"
                  />
                )}
                {contactFrame.select === 3 && (
                  <input
                    type="tel"
                    pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"
                    value={contactFrame.value}
                    onChange={(event) => handleInputChange(index, event)}
                    className="form-control border border-2 p-2 system-control"
                    placeholder="+44"
                  />
                )}
                {contactFrame.select === 4 && (
                  <input
                    type="tel"
                    pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"
                    value={contactFrame.value}
                    onChange={(event) => handleInputChange(index, event)}
                    className="form-control border border-2 p-2 system-control"
                    placeholder="+44 , name@domain.com"
                  />
                )}
                {contactFrame.select === 5 && (
                  <input
                    type="tel"
                    pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"
                    value={contactFrame.value}
                    onChange={(event) => handleInputChange(index, event)}
                    className="form-control border border-2 p-2 system-control"
                    placeholder="+44"
                  />
                )}
                {contactFrame.select === 6 && (
                  <input
                    type="email"
                    value={contactFrame.value}
                    onChange={(event) => handleInputChange(index, event)}
                    className="form-control border border-2 p-2 system-control"
                    placeholder="+44"
                  />
                )}
                {contactFrame.select === 7 && (
                  <input
                    type="text"
                    value={contactFrame.value}
                    onChange={(event) => handleInputChange(index, event)}
                    className="form-control border border-2 p-2 system-control"
                    placeholder="+44"
                  />
                )}
              </div>
              <div className="col-1 p-0  d-flex align-items-center justify-content-end ">
                {contactFrames.length == 1 &&
                  index == 0 &&
                  !isSingleTimeFrameWithValues && (
                    <Image
                      src={addMore}
                      alt="add-more"
                      onClick={handleAddContactFrame}
                      className="pointer"
                    />
                  )}
                {(index !== 0 ||
                  (contactFrames.length > 1 && index == 0) ||
                  isSingleTimeFrameWithValues) && (
                    <Image
                      src={trash}
                      alt="trash"
                      onClick={() => {
                        if (contactFrames.length !== 1) {
                          handleRemoveContactFrame(index);
                        } else {
                          updateStartTimeAndEndTime();
                        }
                      }}
                      className="pointer"
                    />
                  )}
              </div>
            </div>
          ))}
          {error.contact_type && (
            <div className="row align-items-center" style={{ marginLeft: 4 }}>
              <div className="col-md-9 col-12 p-0 text-danger">
                {error.contact_type}
              </div>
            </div>
          )}
        </div>
        {(isSingleTimeFrameWithValues || contactFrames.length > 1) ? (
          <div className="row m-0 ">
            <div className="col-md-12 col-12 p-0 m-0  ps-0  d-flex">
              <p
                className="p-0 system-text-primary font-semibold pointer"
                onClick={handleAddContactFrame}
              >
                <u>{getLabel("add", initialLanguage)}</u>
              </p>
              <span className=" ps-1">{getLabel("additionalContact", initialLanguage)} </span>
            </div>
          </div>
        ) : (<div className="pb-2"></div>)}
        <hr className="mb-3 mt-0 hr-width-form" />
        <div className="row m-0">
          <div className="col-md-6 col-12 p-0 mb-3 ">
            <div className=" font-semibold">{getLabel("visibility", initialLanguage)}</div>

            <CustomSelect
              data={visibilityData}
              placeholderValue={"Select a visibility"}
              onChange={(value: any) => {
                setVisibilityId(value);
              }}
              is_search={false}
              defaultValue={visibility}
            />
          </div>
          <div className="col-md-6 col-12 p-0 mb-3 ps-md-2 ps-0">
            <div className=" font-semibold">
              {getLabel("hideContactContent", initialLanguage)}?
            </div>

            <CustomSelect
              data={[
                { id: 1, name: "Yes" },
                { id: 0, name: "No" },
              ]}
              placeholderValue={"Select a Hide After Funeral"}
              onChange={(value: any) => {
                setHideAfterFuneralData(value === "1" ? true : false);
              }}
              is_search={false}
              defaultValue={hideAfterFuneral}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default ViewContact;
