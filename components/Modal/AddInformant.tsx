import React from "react";

const AddInformant = () => {
  const defaultValue = "1";
  return (
    <>
      <div>
        <div className="row  align-items-center  m-0 ">
          <div className="col-md-12 col-12 p-0 mb-3">
            <div className="font-semibold">
              Name <span className="font-semibold text-danger">*</span>
            </div>
            <input
              type="text"
              className="form-control border border-2 p-2 system-control "
              placeholder="Person Name"
            />
          </div>
          <div className="col-md-12 col-12 p-0 mb-3">
            <div className="font-semibold">Additional Text</div>
            <textarea
              rows={2}
              className="form-control border border-2 system-control resize-none"
              placeholder="Status of the person/Relationship/Job title/Phone No."
            />
            {/* <input
              type="text"
              className="form-control border border-2 p-2 system-control "
              placeholder="Status of the person/Relationship/Job title/Phone No."
            /> */}
          </div>

          <div className="col-md-4 col-12 p-0 mb-3">
            <div className="font-semibold">Group by relationship</div>
            <select
              className="form-select border border-2 bg-white font-semibold p-2 system-control"
              id="dropdownMenuLink"
            >
              <option defaultValue={defaultValue}>Sons </option>
              <option value="1" className="">
                Something
              </option>
            </select>
          </div>
        </div>
      </div>
    </>
  );
};

export default AddInformant;
