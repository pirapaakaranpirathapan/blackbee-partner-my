import { PageDataContext } from '@/pages/memorials/[id]/PageDataContext';
import { NoticeDataContext } from '@/pages/notices/[id]/NoticeDataContext';
import { getLabel } from '@/utils/lang-manager';
import { useRouter } from 'next/router';
import React, { useContext, useState } from 'react';

const NoticeTypeCreateOne = ({ noticetypesDatas }: any) => {
  const [visibleItems, setVisibleItems] = useState(10);
  const [selectedItemId, setSelectedItemId] = useState(null);
  const handleClickLoadMore = () => { setVisibleItems((prevVisibleItems) => prevVisibleItems + 4) };
  const router = useRouter();
  const Customeruuid = router.query?.id;
  const { NoticeData } = useContext(NoticeDataContext);
  const { PageData } = useContext(PageDataContext);
  const initialLanguage = NoticeData?.initialLanguage || PageData?.initialLanguage

  const handleClick = () => {
    if (selectedItemId) {
      router.push({
        pathname: '/notices/create',
        query: {
          id: selectedItemId,
          c_uuid: Customeruuid
        },
      });
    }
  };

  const handleRadioChange = (id: any) => {
    setSelectedItemId(id);
  };

  const renderedItems = noticetypesDatas && noticetypesDatas.slice(0, visibleItems).map((data: any, index: any) => (
    <li key={index} className="list-style-none mb-2">
      <label
        htmlFor={`exampleRadios${index}`}
        className="d-flex align-items-center gap-2 pointer"
      >
        <div className="form-check m-0 p-0 b-line mt-tpp ">
          <input
            className="check-button"
            type="radio"
            name="exampleRadios"
            id={`exampleRadios${index}`}
            value={data.id}
            checked={data.id === selectedItemId}
            onChange={() => handleRadioChange(data.id)}
          />
        </div>
        <div className="font-normal ">
          <div className="system-text-primary font-140 lh-1">{data.name}</div>
          <div className="font-90 system-muted1">
            {data.description
              ? data.description
              : ""}
          </div>
        </div>
      </label>
    </li>

  ));

  return (
    <>
      <div className='notice-type'>
        <div className="row  align-items-center m-0">
          <div className="col-12 p-0 mb-4 notice-list-content">
            {renderedItems}
          </div>
          {noticetypesDatas?.length > visibleItems && (
            <div className="font-normal text-center font-120 pointer" onClick={handleClickLoadMore}>
              Load more
            </div>
          )}
        </div>
        <button
          type="button"
          className="btn btn-system-primary"
          onClick={handleClick}
        >
          {getLabel("next", initialLanguage)}
        </button>
      </div>
    </>
  )
}

export default NoticeTypeCreateOne;














// import { useRouter } from 'next/router';
// import React, { useState } from 'react'

// const NoticeTypeCreate = ({ noticetypesDatas }: any) => {
//   const [visibleItems, setVisibleItems] = useState(4);

//   const handleClickLoadMore = () => {
//     setVisibleItems((prevVisibleItems) => prevVisibleItems + 4);
//   };
//   const router = useRouter();

//   const handleClick = (id: any) => {
//     router.push({
//       pathname: '/notices/create',
//       query: { id },
//     });
//   };



//   return (
//     <>

//       <div className='notice-type'>
//         <div className="row align-items-center m-0 ">
//           <div className="col-12 col-md-10 p-0 mb-4">
//             <div className="font-semibold font-100">
//               Ethnicity / Community
//             </div>
//             <select
//               className="form-select border border-2  font-bold p-2 system-control system-dark-2"
//               id="dropdownMenuLink"
//             >
//               <option value="1">Sri Lankan Tamil</option>
//               <option value="2">Two</option>
//               <option value="3">Three</option>
//             </select>
//           </div>
//         </div>
//         <div className="row  align-items-center m-0">
//           <div className="col-12 p-0 mb-4 notice-list-content">
//             {noticetypesDatas &&
//               noticetypesDatas.slice(0, visibleItems).map((data: any, index: any) => (
//                 <li key={index} className="list-style-none mb-2">
//                   <div className="d-flex align-items-center gap-2">
//                     <div className="form-check m-0 p-0 b-line mt-tpp">
//                       <input
//                         className="check-button pointer"
//                         type="radio"
//                         name="exampleRadios"
//                         id={`exampleRadios${index}`}
//                         value={data.id}
//                         checked={data.checked}
//                       />
//                     </div>
//                     <div className="font-normal" onClick={() => handleClick(data.id)}>
//                       <div className="system-text-primary font-140 lh-1">{data.name}</div>
//                       <div className="font-90 system-muted1">
//                         {data.description
//                           ? data.description
//                           : "no data here so dummy data for description- Publishing 31 days remembrance and acknowledge to the people"}
//                       </div>
//                     </div>
//                   </div>
//                 </li>
//               ))}
//           </div>
//           {noticetypesDatas.length > visibleItems && (
//             <div className="font-normal text-center font-120 pointer" onClick={handleClickLoadMore}>
//               Load more
//             </div>
//           )}
//         </div>

//         <button
//           type="button"
//           className="btn btn-system-primary"
//            onClick={() => handleClick(data.id)}
//         >
//           Next
//         </button>
//       </div>



//     </>
//   )
// }

// export default NoticeTypeCreate













{/* <div className="row align-items-center m-0 ">
          <div className="col-12 col-md-10 p-0 mb-4">
            <div className="font-semibold font-100">
              Ethnicity / Community
            </div>
            <select
              className="form-select border border-2  font-bold p-2 system-control system-dark-2"
              id="dropdownMenuLink"
            >
              <option value="1">Sri Lankan Tamil</option>
              <option value="2">Two</option>
              <option value="3">Three</option>
            </select>
          </div>
        </div> */}
{/* <li className="list-style-none mb-2 ">
              <div className="d-flex align-items-center gap-2">
                <div className="form-check m-0 p-0 b-line ">
                  <input className="check-button pointer" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked />
                </div>
                <div className="font-normal">
                  <div className='system-text-primary font-140 lh-1'>31st Day Invitation and Acknowledgement</div>
                  <div className='font-90 system-muted1'>Publishing 31 days remembrance and acknowledge to the people</div>
                </div>
              </div>
            </li> */}
{/* <li className="list-style-none mb-2">
              <div className="d-flex align-items-center gap-2">
                <div className="form-check m-0 p-0 b-line">
                  <input className="check-button pointer" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked />
                </div>
                <div className="font-normal ">
                  <div className='system-text-primary font-140 lh-1'>Punniya Thithi</div>
                  <div className='font-90 system-muted1'>Publishing 31 days remembrance and acknowledge to the people</div>
                </div>
              </div>
            </li>
            <li className="list-style-none mb-2">
              <div className="d-flex align-items-center gap-2">
                <div className="form-check m-0 p-0 b-line">
                  <input className="check-button pointer" type="radio" name="exampleRadios" id="exampleRadios1" value="option3" checked />
                </div>
                <div className="font-normal ">
                  <div className='system-text-primary font-140 lh-1'>Wedding Anniversary Remembrance</div>
                  <div className='font-90 system-muted1'>Publishing 31 days remembrance and acknowledge to the people</div>
                </div>
              </div>
            </li> */}
{/* <div className='col-12 p-0 mb-4 font-140 font-semibold'>Other Types</div>

          <div className='col-12 p-0 mb-3'>
            <li className="list-style-none mb-2">
              <div className="d-flex align-items-center gap-2">
                <div className="form-check m-0 p-0 b-line">
                  <input className="check-button pointer" type="radio" name="exampleRadios" id="exampleRadios1" value="option3" />
                </div>
                <div className="font-normal">
                  <div className='system-text-primary font-140 lh-1'>Mothers Day in Memoriam</div>
                  <div className='font-90 system-muted1'>Publishing 31 days remembrance and acknowledge to the people</div>
                </div>
              </div>
            </li>
          </div> */}