import React from 'react';

const AddRelationship = () => {
  const defaultValue = '1';
  return (
    <>
      <div>
        <div className="row  align-items-center  m-0 ">
          <div className="col-md-4 col-12 p-0 mb-3">
            <div className="font-semibold">Relationship</div>
            <select
              className="form-select border border-2 bg-white fw-normal p-2 system-control"
              id="dropdownMenuLink"
            >
              <option defaultValue={defaultValue}>Sons </option>
              <option value="1" className="">
                Something
              </option>
            </select>
          </div>
          <div className="col-md-12 col-12 p-0 mb-3">
            <div className="font-semibold">
              Name <span className="text-danger">*</span>
            </div>
            <input
              type="text"
              className="form-control border border-2 p-2 system-control "
              placeholder="Person Name"
            />
          </div>
          <div className="col-md-12 col-12 p-0 mb-3">
            <div className="font-semibold ">
            Additional Information
              <span className="text-secondary font-80 font-thin ps-1">Optional</span>
            </div>
            <textarea
              rows={2}
              className="form-control border border-2 p-2 system-control resize-none"
              placeholder="Relationship/Job title/Education title/Location"
              
            />
          </div>
          <div className="col-md-4 col-12 p-0 mb-3">
            <div className="font-semibold">Passed away?</div>
            <select
              className="form-select border border-2 bg-white p-2 system-control"
              id="dropdownMenuLink"
            >
              <option defaultValue={defaultValue}>Yes </option>
              <option value="1">No</option>
            </select>
          </div>
        </div>
      </div>
    </>
  );
};

export default AddRelationship;
