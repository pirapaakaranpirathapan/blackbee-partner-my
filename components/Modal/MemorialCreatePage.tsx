import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import { getAllSalutations, identitySelector } from "@/redux/identities";
import { memorialPageSelector } from "@/redux/memorial-pages";
import { PageTypesSelector, getAllPageTypes } from "@/redux/page-types";
import { getAllRelationships, relationsSelector } from "@/redux/relationships";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import error from "next/error";

import React, { useContext, useEffect, useRef, useState } from "react";
import Loader from "../Loader/Loader";
import PlaceAutocomplete from "../Elements/PlaceAutoComplete";
import { useApiErrorHandling } from "../Others/useApiErrorHandling";
import { CountriesSelector } from "@/redux/countries";
import { customerPageSelector, getAllCustomerforpage } from "@/redux/customer";
import ArcoSelect from "../Elements/ArcoSelect";
import ArcoSelectSingle from "../Elements/ArcoSelectSingle";
import { integer } from "aws-sdk/clients/cloudfront";
import ArcoDropdown from "../Elements/ArcoDropdown";
import { getLabel } from "@/utils/lang-manager";
import CustomSelect from "../Arco/CustomSelect";
import { DatePicker } from "@arco-design/web-react";
import CustomDatePicker from "../Accessories/CustomDatePicker";
import CustomDropDownDatepicker from "../Accessories/CustomDropDownDatepicker";
import ArcoSelectAndCreateSingle from "../Arco/ArcoSelectAndCreateSingle";
import FullScreenModal from "../Elements/FullScreenModal";
import CustomerDetailsModal, { ChildModalRef } from "./createCustomerModal";
import { Button } from "react-bootstrap";
import getYearFromDate from "../Utils/YearExtracter";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";
interface AddNewPageProps {
  response: any;
  actionBtnRef: any;
  handleCreate: (
    // salutationId: string,
    personName: string,
    salutation: integer,
    clientId: string,
    countryId: string,
    pageTypeId: string,
    relationship: string,
    dod: string,
    dob: string,
    deathPlace: string
  ) => void;
  addSearchValue: string;
}
const MemorialCreatePage = ({
  handleCreate,
  actionBtnRef,
  response,
  addSearchValue,
}: AddNewPageProps) => {
  // const [salutation, setSalutation] = useState("");
  const [salutation, setSalutation] = useState(2);
  // const [salutationId, setSalutationID] = useState("1");
  const [pageTypeId, setPageTypeId] = useState("");
  const [clientId, setClientId] = useState("");
  const [countryId, setCountryId] = useState("130");
  const [relationship, setRelationship] = useState("");
  const [clickstatus, setClickStatus] = useState(false);
  const [personName, setPersonName] = useState(addSearchValue);
  const [deathPlace, setDeathPlace] = useState("");
  // const [dod, setDod] = useState("");
  // const [dob, setDob] = useState("");
  const [dob, setDob] = useState<string>("-");
  const [dod, setDod] = useState<string>("");
  const [customerDatas, setCustomerDatas] = useState<string[]>([]);
  const [ShowProfileBanner, setShowProfileBanner] = useState(false);
  const [defaultNames, setDefaultNames] = useState<string[]>([]);
  const [defaultCustomerNames, setDefaultCustomerNames] = useState<string | undefined>();
  const [backgroundOpacity, setBackgroundOpacity] = useState(1);
  const { NoticeData } = useContext(NoticeDataContext);
  const { PageData } = useContext(PageDataContext);
  const initialLanguage = NoticeData?.initialLanguage || PageData?.initialLanguage

  // const [addCusSearchValue, setAddCusSearchValue] = useState('');
  const [addCusSearchValue, setAddCusSearchValue] = useState({
    inputValue: "",
    searchType: "",
  });

  // const today = new Date().toISOString().split("T")[0];
  const today = new Date().toISOString().split("T")[0];
  const loadData = () => {
    dispatch(
      getAllCustomerforpage({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
      })
    );
  };

  useEffect(() => {
    loadData();
  }, []);

  const childModalRef = useRef<ChildModalRef>(null);
  const { data: showCountriesData } = useAppSelector(CountriesSelector);
  const { data: allCustomerData } = useAppSelector(customerPageSelector);

  //customer

  // useEffect(() => {
  //   if (changeCustomer) {
  //     setCustomerId("")
  //     setCustomerErr("");
  //     setDefaultCustomerNames(undefined)
  //   }

  // }, [changeCustomer])

  useEffect(() => {
    setCustomerDatas(allCustomerData);
  }, [allCustomerData])

  const handleSelectCustomerChange = (selectedValues: string) => {
    const selectCustomerId = selectedValues;
    // const selectCustomerId = selectedValues ? selectedValues[0] : "";
    setClientId(selectCustomerId);
  };

  const handleSuggestionClick = (description: string) => {
    setDeathPlace(description);
  };

  useEffect(() => {
    actionBtnRef.current.onclick = () => {
      setClickStatus(true);
    };
    if (clickstatus) {
      handleCreate(
        personName,
        // salutationId,
        salutation,
        clientId,
        countryId,
        pageTypeId,
        relationship,
        dod,
        dob,
        deathPlace
      );
      setClickStatus(false);
    }
  }, [clickstatus]);

  const dispatch = useAppDispatch();
  const loadStaticData = async () => {
    dispatch(getAllSalutations());
    dispatch(getAllPageTypes({ active_partner: ACTIVE_PARTNER }));
    dispatch(getAllRelationships());
  };
  const { data: showSalutationsData } = useAppSelector(identitySelector);
  const { data: showPageTypesData } = useAppSelector(PageTypesSelector);
  const { data: showRelationshipsData } = useAppSelector(relationsSelector);

  useEffect(() => {
    loadStaticData();
  }, []);
  const { error: apiError } = useAppSelector(memorialPageSelector);

  const err = useApiErrorHandling(apiError, response);

  const handleRelationshipsChange = (
    e: React.ChangeEvent<HTMLSelectElement>
  ) => {
    const selectedValue = e.target.value;
    setRelationship(selectedValue);
  };
  const handleInputChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    field: string
  ) => {
    const value = e.target.value;
    switch (field) {
      case "personName":
        setPersonName(value);
        // clearError();
        break;
      case "deathPlace":
        setDeathPlace(value);
        break;
      // case "dob":
      //   if (!dod || (value <= dod && value <= today)) {
      //     value <= today ? setDob(value) : setDob("");
      //     // clearError();
      //   } else {
      //     setDob("");
      //   }
      //   break;
      // case "dod":
      //   if (!dod || value <= today) {
      //     setDod(value);
      //     // clearError();
      //     if (dob <= dod && dob <= today) {
      //       dob <= today ? setDob(dob) : setDob("");
      //       // clearError();
      //     } else {
      //       setDob("");
      //     }
      //   } else {
      //     setDod("");
      //   }
      //   break;
      default:
        break;
    }
  };
  const [value, setValue] = useState<string>("");

  useEffect(() => {
    // Date of Birth validation
    if (dob) {
      if (dob > today) {
        // setDob("");
      } else if (dod && dob > dod) {
        // setDob('');
      }
    }

    // Date of Death validation
    if (dod) {
      if (dod > today) {
        // setDod("");
      } else if (dob && dod < dob) {
        // setDob('');
      }
    }
  }, [dob, dod]);

  const handleInputDateChange = (date: any, field: any) => {
    if (date === undefined) {
      date = "";
    }
    switch (field) {
      case "dateOfBirth":
        if (getYearFromDate(date) > getYearFromDate(dod)) {
          getYearFromDate(date)!=0&&   setDod("");
          setDob(date);
        } else {
          setDob(date);
        }
        if (!dod || (date <= dod && date <= today)) {
          setDob(date);
          if (dod && dob > dod) {
            // setDod('');
          }
        } else {
          // setDob('');
        }
        break;

      case "dateOfDeath":
        if (getYearFromDate(date) < getYearFromDate(dob)) {
          getYearFromDate(date)!=0&&   setDob("");
          setDod(date);
        } else {
          setDod(date);
        }
        if (!dob || (date <= today && date >= dob)) {
          setDod(date);
          if (dob >= date) {
            // setDob('');
          }
        } else {
          // setDod('');
        }
        break;

      default:
        break;
    }
  };

  const onCallCustomerContinue = (value: any) => {
    // setCustomerId(value);
    setShowProfileBanner(false);
    // const selectedCustomer =allCustomerData?allCustomerData.find(
    //   (option: { id: number }) => option.id === parseInt(value)
    // ) :"not found";
    // console.log(selectedCustomer,allCustomerData);

    // const selectedUuid = selectedCustomer?.uuid || "";
  };
  const handleClick = async () => {
    // setsubmitButtonAddCustomerEnabled(true);
    if (childModalRef.current) {
      childModalRef.current.callModalFunction(handleChildResponse);
    }
  };

  const handleChildResponse = (response: any) => {
    const customerResponseData = response;
    setDefaultCustomerNames(response?.first_name);
    setCustomerDatas([customerResponseData]);
    setClientId(response?.id);
    // setCustomer(response?.["first_name"]);
    setShowProfileBanner(false);
  };
  const updateAddSearchValue = (value: any) => {
    // setAddCusSearchValue(value);
    let searchType = "Name";
    setAddCusSearchValue({ inputValue: value, searchType: searchType });
  };

  return (
    <>
      <FullScreenModal
        show={ShowProfileBanner}
        onClose={() => {
          setShowProfileBanner(false);
          setClientId("")
        }}
        title={`${getLabel("addNewCustomer", initialLanguage)}`}
        description={`${getLabel("addNewCustomerModalDescription", initialLanguage)}`}
        // className={`opacity-50`}
        footer={
          <Button
            className="btn-system-primary pointer"
            onClick={handleClick}
          // disabled={submitButtonAddCustomerEnabled}
          >
            {getLabel("create", initialLanguage)}
          </Button>
        }
        deleteLabel={
          <a
            onClick={() => {
              setShowProfileBanner(false);
            }}
          >
            {getLabel("close", initialLanguage)}
          </a>
        }
      >
        <CustomerDetailsModal
          ref={childModalRef}
          onCallCustomerContinue={onCallCustomerContinue}
          addCusSearchValue={addCusSearchValue}
          setModalVisible={setShowProfileBanner}
        />
      </FullScreenModal>
      <div className="row align-items-center m-0 ">
        <div className="row m-0 p-0  ">
          <div className="col-md-12 col-12 p-0 ">
            <div className="font-semibold ">
              {getLabel("personName", initialLanguage)}
              <span className="ps-1">*</span>
            </div>
          </div>
          <div className="col-md-3 col-3 p-0 ">
            <CustomSelect
              data={showSalutationsData}
              placeholderValue={"Select a Salutation"}
              onChange={(value: any) => setSalutation(value)}
              is_search={true}
              defaultValue={salutation}
            />
          </div>

          <div className="col-md-9 col-9  pe-0">
            <input
              type="text"
              className="form-control border border-2 p-2 system-control "
              placeholder="Full Name in English *"
              value={personName}
              onChange={(e) => handleInputChange(e, "personName")}
            />
          </div>
        </div>
        <div className="row m-0 p-0 mb-3">
          <div className="col-md-3 col-3 p-0"></div>
          <div className="col-md-9 col-9  pe-0  ">
            {err && <div className="text-danger">{err.name}</div>}
          </div>
        </div>

        {/* ================ */}
        {/* <div className="row m-0 p-0 mb-3 ">
          <div className="col-md-12 col-12 p-0">
            <div className="font-semibold">
              {getLabel("country")}
              <span className="text-danger">*</span>
            </div>
          </div>
          <div className="col-md-12 col-12  p-0">
            <CustomSelect
              data={showCountriesData}
              placeholderValue={"Select a Country"}
              onChange={(value: any) => setCountryId(value)}
              is_search={true}
              defaultValue={value}
            />
          </div>
          <div className="col-md-12 col-12 p-0">
            {err && <div className="text-danger">{err.country_id}</div>}
          </div>
        </div> */}

        <div className="row m-0 p-0 mb-3">
          <div className="col-md-12 col-12 p-0">
            <div className="font-semibold">
              {getLabel("customer", initialLanguage)}
              <span className="ps-1">*</span>
            </div>
          </div>
          <div className="col-md-12 col-12  p-0">
            {/* <ArcoDropdown
              Data={allCustomerData}
              defaultValue={customerDatas}
              onSelectChange={handleSelectCustomerChange}
            /> */}
            <ArcoSelectAndCreateSingle
              data={customerDatas}
              placeholderValue={"Search a Customer"}
              onOptionChange={handleSelectCustomerChange}
              is_search={true}
              setIsModalVisible={setShowProfileBanner}
              isVisible={ShowProfileBanner}
              // onClear={handleClear}
              // onDeselect={handleDeselect}
              defaultValue={defaultCustomerNames}
              updateAddSearchValue={updateAddSearchValue}
            />
          </div>
          <div className="col-md-12 col-12 p-0">
            {err && <div className="text-danger">{err.client_id}</div>}
          </div>
        </div>

        {/* ==================== */}
        <div className="row m-0 p-0 mb-3">
          <div className="col-md-12 col-12 p-0">
            <div className="font-semibold">
              {getLabel("pageType", initialLanguage)}
              <span className="ps-1">*</span>
            </div>
          </div>
          <div className="col-md-12 col-12  p-0">
            {/* <select
              className="form-select border border-2 bg-white fw-normal p-2 system-control"
              id="dropdownMenuLink"
              value={pageTypeId}
              onChange={handlePageTypeChange}
              required
            >
              <option value="" disabled selected>
                {getLabel("selectPageType",initialLanguage)}
              </option>
              {Array.isArray(showPageTypesData) &&
                showPageTypesData.map((item: any) => (
                  <option key={item.id} value={item.id}>
                    {item.name}
                  </option>
                ))}
            </select> */}
            <CustomSelect
              data={showPageTypesData}
              placeholderValue={"Select a  Page Type"}
              onChange={(value: any) => setPageTypeId(value)}
              is_search={false}
              defaultValue={value}
            />
          </div>
          <div className="col-md-12 col-12 p-0">
            {err && <div className="text-danger">{err.pageTypeId}</div>}
          </div>
        </div>
        <div className="row m-0 p-0 mb-3">
          <div className="col-md-12 col-12 p-0">
            <div className="font-semibold">{getLabel("relationship", initialLanguage)}? </div>
          </div>
          <div className="col-md-12 col-12  p-0">
            {/* <select
              className="form-select border border-2 bg-white  p-2 system-control"
              id="dropdownMenuLink"
              value={relationship}
              onChange={handleRelationshipsChange}
            >
              <option value="" disabled selected>
                {getLabel("selectRelationship",initialLanguage)}
              </option>
              {Array.isArray(showRelationshipsData) &&
                showRelationshipsData.map((option: any) => (
                  <option key={option.id} value={option.id}>
                    {option.name}
                  </option>
                ))}
            </select> */}

            <CustomSelect
              data={showRelationshipsData}
              placeholderValue={"Select a Relationship"}
              onChange={(value: any) => setRelationship(value)}
              is_search={true}
              defaultValue={value}
            />
          </div>
        </div>
        <div className="row m-0 p-0 mb-3">
          <div className="col-md-6 col-12 p-0 mb-3 mb-md-0 pe-0 pe-md-3 col-sm-6 pe-sm-3">
            <div className="row m-0 ">
              <div className="col-12 p-0 font-semibold">
                {getLabel("dateOfDeath", initialLanguage)}<span className="ps-1">*</span>
                {/* <span className="text-danger">*</span> */}
              </div>
              <div className=" col-12 p-0 sms">
                {/* <DatePicker
                  className="form-control border border-2 system-control py-3"
                  value={dod}
                  onChange={(date: string) => handleInputDateChange(date, "dateOfDeath")}
                /> */}
                <CustomDropDownDatepicker
                  onDateChange={(date: string) =>
                    handleInputDateChange(date, "dateOfDeath")
                  }
                  defaultDate={dod}
                />
                {/* <CustomDatePicker defaultDate="" onDateChange={function (selectedDate: string): void {
                  throw new Error("Function not implemented.");
                }} /> */}
                {/* <input
                  type="date"
                  className="form-control border border-2  system-control p-2"
                  value={dod}
                  onChange={(e) => handleInputChange(e, "dod")}
                  max={today}
                  min="1923-01-01"
                /> */}
              </div>
              <div className=" col-12 p-0">
                {err && <div className="text-danger">{err.dod}</div>}
              </div>
            </div>
          </div>
          <div className="col-md-6  p-0   ps-md-3 col-sm-6  ps-sm-3">
            <div className="row m-0 ">
              <div className="col-12 p-0">
                <div className="font-semibold">
                  {getLabel("dateOfBirth", initialLanguage)}
                  {/* <span className="text-danger">*</span> */}
                </div>
              </div>
              <div className=" col-12 p-0 sms">
                <CustomDropDownDatepicker
                  onDateChange={(date: string) =>
                    handleInputDateChange(date, "dateOfBirth")
                  }
                  defaultDate={dob}
                />
                {/* <DatePicker
                  className="form-control border border-2 system-control py-3"
                  value={dob}
                  onChange={(date: string) => handleInputDateChange(date, "dateOfBirth")}
                />
                {/* <CustomDatePicker defaultDate="" onDateChange={function (selectedDate: string): void {
                  throw new Error("Function not implemented.");
                }} /> */}
                {/* <input
                  type="date"
                  className="form-control border border-2  system-control p-2"
                  value={dob}
                  onChange={(e) => handleInputChange(e, "dob")}
                  max={dod}
                  min="1923-01-01"
                /> */}
              </div>
              <div className=" col-12 p-0">
                {err && <div className="text-danger">{err.dob}</div>}
              </div>
            </div>
          </div>
        </div>
        <div className="row m-0 p-0 mb-3 ">
          <div className="col-md-12 col-12 p-0">
            <div className="font-semibold">{getLabel("deathPlace", initialLanguage)}</div>
          </div>
          <div className="col-md-12 col-12  p-0">
            {/* <input
              type="text"
              className="form-control border border-2 p-2 system-control "
              placeholder="Search city of death"
              value={deathPlace}
              onChange={(e) => handleInputChange(e, "deathPlace")}
            /> */}
            <PlaceAutocomplete
              className="form-control border border-2 p-2 system-control"
              placeholder={`${getLabel("deathPlacePlaceholder", initialLanguage)}`}
              onSuggestionClick={handleSuggestionClick}
              dvalue={deathPlace}
            />
          </div>
        </div>
      </div>
    </>
  );
};
export default MemorialCreatePage;
