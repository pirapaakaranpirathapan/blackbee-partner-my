import React, { useState, useEffect, use, useContext } from "react";
import Image from "next/image";
import trash from "../../public/trash.svg";
import addMore from "../../public/add-more.svg";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import {
  eventLocationTypesReducer,
  getAllEventLocationTypes,
} from "@/redux/event-location-type";
import PlaceAutocomplete from "../Elements/PlaceAutoComplete";
import { EventTypeSelector, getAllEventsTypes } from "@/redux/event-types";
import { noticeEventSelector } from "@/redux/notices-event";
import { useApiErrorHandling } from "../Others/useApiErrorHandling";
import { noticeSelector } from "@/redux/notices";
import { getLabel } from "@/utils/lang-manager";
import CustomSelect from "../Arco/CustomSelect";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";

interface EditNewContactProps {
  actionBtnEditRef: any;
  handleCreate: (
    address: string,
    is_delivery_possible: boolean,
    instructions: string,
    virtual_event: string,
    eventLocationTypeId: string,
    eventTypeId: string,
    eventInstructions: string
  ) => void;
}

const AddNewEvent = ({
  handleCreate,
  actionBtnEditRef,
}: EditNewContactProps) => {
  const tab = "Venue";
  const [activeTab, setActiveTab] = useState(tab);
  const [address, setAddress] = useState("");
  const [is_delivery_possible, setIsDeliveryPossible] = useState(true);
  const [instructions, setInstructions] = useState("");
  const [virtual_event, setVirtualEvent] = useState("");
  const [clickstatus, setClickStatus] = useState(false);
  const [sInstructionLength, setSInstructionLength] = useState(0);
  const [esInstructionLength, seteSInstructionLength] = useState(0);
  const [eventLocationTypeId, setEventLocationTypeId] = useState("1");
  const [eventTypeId, seteventTypeId] = useState("");
  const [eventDate, setEventDate] = useState("");
  const [eventDateERR, setEventDateERR] = useState("");
  const [eventTypeIdERR, seteventTypeIdERR] = useState(false);
  const [eventInstructions, seteventInstructions] = useState("");
  const changeTab = (val: any) => {
    setActiveTab(val);
  };
  const { NoticeData } = useContext(NoticeDataContext);
  const { PageData } = useContext(PageDataContext);
  const initialLanguage = NoticeData?.initialLanguage || PageData?.initialLanguage
  const { error: apiError } = useAppSelector(noticeSelector);
  const err = useApiErrorHandling(apiError, Response);
  // console.log("L", err.event_type_id);
  useEffect(() => {
    actionBtnEditRef.current.onclick = () => {
      setClickStatus(true);
    };
    if (clickstatus) {
      handleCreate(
        address,
        is_delivery_possible,
        instructions,
        virtual_event,
        eventLocationTypeId,
        eventTypeId,
        eventInstructions
      );
      setClickStatus(false);
    }
  }, [clickstatus]);

  useEffect(() => {
    getEventLocationTypes();
    getEventType();
  }, []);
  useEffect(() => {
    eventDate && setEventDateERR("");
    eventTypeId && seteventTypeIdERR(false);
  }, [eventTypeId || eventDate]);

  // const defaultValue = '1'
  // const [timeFrames, setTimeFrames] = useState([{ startTime: '', endTime: '' }])

  // const handleAddTimeFrame = () => {
  //   setTimeFrames([...timeFrames, { startTime: '', endTime: '' }])
  // }

  // const handleRemoveTimeFrame = (index: any) => {
  //   setTimeFrames(timeFrames.filter((_, i) => i !== index))
  // }

  // const handleStartTimeChange = (index: any, event: any) => {
  //   const newTimeFrames = [...timeFrames]
  //   newTimeFrames[index].startTime = event.target.value
  //   setTimeFrames(newTimeFrames)
  // }

  // const handleEndTimeChange = (index: any, event: any) => {
  //   const newTimeFrames = [...timeFrames]
  //   newTimeFrames[index].endTime = event.target.value
  //   setTimeFrames(newTimeFrames)
  // }

  const defaultValue = "1";
  const [timeFrames, setTimeFrames] = useState([
    { id: defaultValue, startTime: "", endTime: "" },
  ]);

  const handleAddTimeFrame = () => {
    setTimeFrames([
      ...timeFrames,
      { id: Date.now().toString(), startTime: "", endTime: "" },
    ]);
  };

  const handleRemoveTimeFrame = (id: string) => {
    if (id === defaultValue) {
      return; // Prevent removing the default field
    }

    setTimeFrames(timeFrames.filter((timeFrame) => timeFrame.id !== id));
  };

  const handleStartTimeChange = (
    index: number,
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const newTimeFrames = [...timeFrames];
    newTimeFrames[index].startTime = event.target.value;
    setTimeFrames(newTimeFrames);
  };

  const handleEndTimeChange = (
    index: number,
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const newTimeFrames = [...timeFrames];
    newTimeFrames[index].endTime = event.target.value;
    setTimeFrames(newTimeFrames);
  };
  const dispatch = useAppDispatch();
  const { data: eventLocationType } = useAppSelector(eventLocationTypesReducer);
  const { data: eventTypeData } = useAppSelector(EventTypeSelector);

  const getEventLocationTypes = () => {
    dispatch(getAllEventLocationTypes());
  };
  const getEventType = () => {
    dispatch(getAllEventsTypes());
  };
  const [status, setStatus] = useState(1);
  const statusOptions = [
    { id: 1, name: "Enable" },
    { id: 2, name: "Disable" },
  ];
  const FlowerDelivery = [
    { id: 1, name: "Yes" },
    { id: 2, name: "No" },
  ];
  return (
    <>
      <div>
        <div className="row  align-items-center m-0 ">
          <div className="col-12 p-0 mb-3">
            <div className=" font-semibold">
              {getLabel("eventType", initialLanguage)}
              <span className="ps-1">*</span>
            </div>
            {/* <input
              type="text"
              className="form-control border border-2 p-2 system-control "
              placeholder="Event type"
              onChange={(e) => setVirtualEvent(e.target.value)}
            /> */}
            {/* <select
              className="form-select border border-2 bg-white font-bold p-2 system-control"
              id="dropdownMenuLink"
              // value={countryId}
              onChange={(e) => seteventTypeId(e.target.value)}
            >
              <option value="" disabled selected>
                {getLabel("selectAEvent",initialLanguage)}
              </option>
              {eventTypeData &&
                eventTypeData.map((option: any) => (
                  <option key={option.id} value={option.id}>
                    {option.name}
                  </option>
                ))}
            </select> */}
            <CustomSelect
              data={eventTypeData}
              placeholderValue={getLabel("selectAEvent", initialLanguage)}
              onChange={(value: any) => seteventTypeId(value)}
              is_search={true}
              defaultValue={""}
            />
            {/* {(err && err.event_type_id) ||
              (eventTypeIdERR && (
                <div className="text-danger">Please select Event type.</div>
              ))} */}
            {err && err.event_type_id && (
              <div className="text-danger">{err.event_type_id}</div>
            )}
          </div>
        </div>
        <div className="row m-0">
          <div className="col-md-6 col-12 p-0 mb-3 mb-md-0">
            <div className=" font-semibold mb-1">
              {getLabel("date", initialLanguage)}
              <span className="ps-1">*</span>
            </div>
            <input
              type="date"
              className="form-control border border-2 system-control p-2"
              required
              onChange={(e) => setEventDate(e.target.value)}
            />

            {eventDateERR && <div className="text-danger">{eventDateERR}</div>}
          </div>
          <div className="col-md-6 col-12 p-0 ">
            <div className="row  m-0 mb-1">
              <label className="font-90 font-semibold col-5 p-0 px-2 ">
                {getLabel("startTime", initialLanguage)}
              </label>
              <label className="font-90 font-semibold col-5 p-0 px-2">
                {getLabel("endTime", initialLanguage)}
              </label>
              <div className="col-2 p-0 pe-1 d-flex align-items-center justify-content-end">
                <Image
                  src={addMore}
                  alt="add-more"
                  onClick={handleAddTimeFrame}
                  className="pointer"
                />
              </div>
            </div>
            {timeFrames.map((timeFrame, index) => (
              <div key={index} className="row m-0 mb-2 ">
                <div className="col-5 p-0 ps-md-2">
                  <input
                    type="text"
                    value={timeFrame.startTime}
                    onChange={(event) => handleStartTimeChange(index, event)}
                    placeholder={`${getLabel("startTime", initialLanguage)}`}
                    className="form-control border system-control p-2 "
                  />
                </div>
                <div className="col-5  p-0 px-2">
                  <input
                    type="text"
                    value={timeFrame.endTime}
                    onChange={(event) => handleEndTimeChange(index, event)}
                    placeholder={`${getLabel("endTime", initialLanguage)}`}
                    className="form-control border system-control p-2"
                  />
                </div>
                {index !== 0 && (
                  <div className="col-2 p-0 pe-1 d-flex align-items-center justify-content-end">
                    <Image
                      src={trash}
                      alt="trash"
                      onClick={() => handleRemoveTimeFrame(timeFrame.id)}
                      className="pointer"
                    />
                  </div>
                )}
                {/* <div className="col-2 p-0 pe-1 d-flex align-items-center justify-content-end ">
                  <Image
                    src={trash}
                    alt="trash"
                    onClick={() => handleRemoveTimeFrame(index)}
                    className="pointer"
                  />
                </div> */}
              </div>
            ))}
            <div className="row m-0 mb-1">
              <p className="font-50 p-0 ps-md-2 col-md-10 col-12">
                {getLabel("timeDescription", initialLanguage)}
              </p>
            </div>
          </div>
        </div>

        {/* <div className="row m-0S">
          <div className="col-12 p-0  mb-3">
            <div className=" font-semibold">Event Location</div>
            <div className="row m-0" id="filter-panel">
              <div className="col-12 p-0">
                <div className="inpage-sidebar">
                  <div className="row  m-0 ">
                    <div className={
                      activeTab === 'venue'
                        ? `py-2 system-text-primary shadow-md font-semibold active font-100 system-warning-light col-4 d-flex align-items-center border border-2 border-end-0 rounded-start system-border-warning`
                        : `py-2 rounded-end-0 shadow-md font-semibold font-100 col-4 d-flex align-items-center border border-2 border-end-0 rounded-start system-border-warning`
                    } onClick={() => changeTab('venue')}>
                      Venue
                    </div>

                    <div className={
                      activeTab === 'virtual-event'
                        ? `py-2 system-text-primary shadow-md font-semibold active font-100 system-warning-light col-4 d-flex align-items-center border border-2 border-end-0 system-border-warning`
                        : `py-2 rounded-end-0 shadow-md font-semibold font-100 col-4 d-flex align-items-center border border-2 border-end-0 system-border-warning`
                    }
                      onClick={() => changeTab('virtual-event')}>
                      Virtual Event
                    </div>

                    <div className={
                      activeTab === 'family-Setting'
                        ? `py-2 system-text-primary shadow-md font-semibold active font-100 system-warning-light col-4 d-flex align-items-center border border-2 rounded-end system-border-warning`
                        : `py-2 rounded-end-0 shadow-md font-semibold font-100 col-4 d-flex align-items-center border border-2  rounded-end system-border-warning`
                    }
                      onClick={() => changeTab('family-Setting')}>
                      Family Setting
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-12 p-0 pt-3">
                <div className="">
                  {activeTab === 'venue' && (
                    <>

                      <div className="row m-0 ">
                        <div className="col-12 p-0 mb-3">
                          <div className=" font-semibold">Event Place</div>
                          <input
                            type="text"
                            className="form-control border border-2 p-2 system-control "
                            placeholder="Search the venue from our database or add new(Service providers)"
                            onChange={(e) => setAddress(e.target.value)}
                          />
                        </div>
                      </div>
                      <div className="row m-0">
                        <div className=" col-12 p-0 mb-3">
                          <div className=" font-semibold">
                            Special Instruction
                          </div>
                          <div className=" position-relative">
                            <textarea
                              onChange={(e) => {
                                setInstructions(e.target.value);
                                setSInstructionLength(e.target.value.length);
                              }}
                              className="form-control border border-2 system-control resize-none"
                              placeholder="E.g: Car parking information"
                              rows={2}
                              aria-label="With textarea"
                              disabled={sInstructionLength >= 400}

                            ></textarea>
                            <div className="text-muted position-absolute font-70  bottom-0 end-0 mx-1 mb-1">
                              <small id="message-count">{sInstructionLength}/400</small>
                            </div>
                          </div>
                        </div>
                      </div>
                    </>
                  )}
                  {activeTab === 'virtual-event' && <>Virtual Event</>}
                  {activeTab === 'family-Setting' && <>Family Setting</>}
                </div>
              </div>
            </div>

          </div>
        </div> */}

        {/* EVENT UI ======================================================================================== */}
        <div className="row m-0">
          <div className="col-12 p-0  mb-3">
            <div className=" font-semibold">{getLabel("eventLocation", initialLanguage)}</div>
            <div className="row m-0" id="filter-panel">
              <div className="col-12 p-0">
                <div className="inpage-sidebar">
                  <div className="row  m-0">
                    {/* <div
                      className={
                        activeTab === "venue"
                          ? `py-2 system-text-primary shadow-md font-semibold active font-100 system-warning-light col-4 d-flex align-items-center border border-2 border-end-0 rounded-start system-border-warning`
                          : `py-2 rounded-end-0 shadow-md font-semibold font-100 col-4 d-flex align-items-center border border-2 border-end-0 rounded-start system-border-warning`
                      }
                      onClick={() => changeTab("venue",initialLanguage)}
                    >
                      Venue
                    </div> */}

                    {eventLocationType.map((item: any) => (
                      <div
                        className={
                          activeTab === item.name
                            ? `py-2 system-text-primary shadow-md font-semibold active font-100 system-warning-light col-4 d-flex align-items-center border border-2  system-border-warning ${item.name === "Venue"
                              ? "rounded-start border-end-0"
                              : ""
                            } ${item.name === "Virtual Meeting"
                              ? "rounded-end"
                              : ""
                            } ${item.name === "Home" ? "border-end-0" : ""}`
                            : `py-2 rounded-end-0 shadow-md font-semibold font-100 col-4 d-flex align-items-center border border-2 system-border-warning ${item.name === "Venue"
                              ? "rounded-start border-end-0"
                              : ""
                            } ${item.name === "Virtual Meeting"
                              ? "rounded-end "
                              : ""
                            } ${item.name === "Home" ? "border-end-0" : ""}`
                        }
                        onClick={() => {
                          changeTab(item.name);
                          setEventLocationTypeId(item.id);
                          setSInstructionLength(0);
                          seteSInstructionLength(0);
                        }}
                      >
                        {item.name}
                      </div>
                    ))}

                    {/* <div
                      className={
                        activeTab === "family-Setting"
                          ? `py-2 system-text-primary shadow-md font-semibold active font-100 system-warning-light col-4 d-flex align-items-center border border-2 rounded-end system-border-warning`
                          : `py-2 rounded-end-0 shadow-md font-semibold font-100 col-4 d-flex align-items-center border border-2  rounded-end system-border-warning`
                      }
                      onClick={() => changeTab("family-Setting",initialLanguage)}
                    >
                      Family Setting
                    </div> */}
                  </div>
                </div>
              </div>
              <div className="col-12 p-0 pt-3">
                <div className="">
                  {/* {activeTab === "venue" && (
                    <>
                      {" "}
                      <div className="row m-0 ">
                        <div className="col-12 p-0 mb-3">
                          <div className=" font-semibold">Event Place</div>
                          <input
                            type="text"
                            className="form-control border border-2 p-2 system-control "
                            placeholder="Search the venue from our database or add new(Service providers)"
                          />
                        </div>
                      </div>
                      <div className="row m-0">
                        <div className=" col-12 p-0 mb-3">
                          <div className=" font-semibold">
                            Special Instruction
                          </div>
                          <div className=" position-relative">
                            <textarea
                              className="form-control border border-2 system-control resize-none"
                              placeholder="E.g: Car parking information"
                              rows={2}
                              aria-label="With textarea"
                            ></textarea>
                            <div className="text-muted position-absolute font-70  bottom-0 end-0 mx-1 mb-1">
                              <small id="message-count">23/400</small>
                            </div>
                          </div>
                        </div>
                      </div>
                    </>
                  )} */}
                  {activeTab === "Venue" && (
                    <>
                      {" "}
                      <div className="row m-0 ">
                        <div className="col-12 p-0 mb-3">
                          <div className=" font-semibold">
                            {getLabel("eventPlaceSearch", initialLanguage)}
                          </div>
                          <PlaceAutocomplete
                            className="form-control border border-2 p-2 system-control"
                            placeholder={"Search city of Death"}
                            onSuggestionClick={(value) => {
                              setAddress(value);
                            }}
                          // dvalue={deathPlace}
                          />
                        </div>
                      </div>
                      <div className="row m-0">
                        <div className=" col-12 p-0 mb-3">
                          <div className=" font-semibold">
                            {getLabel("specialInstruction", initialLanguage)}
                          </div>
                          <div className=" position-relative">
                            <textarea
                              onChange={(e) => {
                                setInstructions(e.target.value);
                                setSInstructionLength(e.target.value.length);
                              }}
                              className="form-control border border-2 system-control "
                              placeholder={`${getLabel("specialInstruction", initialLanguage)}`}
                              rows={2}
                              aria-label="With textarea"
                              disabled={sInstructionLength >= 400}
                            ></textarea>
                            <div className="text-muted position-absolute font-70  bottom-0 end-0 mx-1 mb-1 mb-2">
                              <small id="message-count">
                                {sInstructionLength}/400
                              </small>
                            </div>
                          </div>
                        </div>
                      </div>
                    </>
                  )}
                  {activeTab === "Virtual Meeting" && (
                    <>
                      {" "}
                      <div className="row m-0">
                        <div className=" col-12 p-0 mb-3">
                          <div className=" font-semibold">
                            {getLabel("eventInstruction", initialLanguage)}
                          </div>
                          <div className=" position-relative">
                            <textarea
                              className="form-control border border-2 system-control "
                              placeholder={`${getLabel("eventInstructionPlaceholder", initialLanguage)}`}
                              rows={2}
                              aria-label="With textarea"
                              onChange={(e) => {
                                seteventInstructions(e.target.value);
                                seteSInstructionLength(e.target.value.length);
                              }}
                              disabled={esInstructionLength >= 400}
                            ></textarea>
                            <div className="text-muted position-absolute font-70  bottom-0 end-0 mx-1 mb-2">
                              <small id="message-count">
                                {esInstructionLength}/400
                              </small>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="row m-0">
                        <div className=" col-12 p-0 mb-3">
                          <div className=" font-semibold">
                            {getLabel("specialInstruction", initialLanguage)}
                          </div>
                          <div className=" position-relative">
                            <textarea
                              onChange={(e) => {
                                setInstructions(e.target.value);
                                setSInstructionLength(e.target.value.length);
                              }}
                              className="form-control border border-2 system-control"
                              placeholder={`${getLabel("specialInstruction", initialLanguage)}`}
                              rows={2}
                              aria-label="With textarea"
                              disabled={sInstructionLength >= 400}
                            ></textarea>
                            <div className="text-muted position-absolute font-70  bottom-0 end-0 mx-1 mb-2">
                              <small id="message-count">
                                {sInstructionLength}/400
                              </small>
                            </div>
                          </div>
                        </div>
                      </div>
                    </>
                  )}
                  {activeTab === "Home" && (
                    <>
                      {" "}
                      <div className="row m-0 ">
                        <div className="col-12 p-0 mb-3">
                          <div className=" font-semibold">
                            {getLabel("eventPlaceSearch", initialLanguage)}
                          </div>

                          <PlaceAutocomplete
                            className="form-control border border-2 p-2 system-control"
                            placeholder={`${getLabel("deathPlacePlaceholder", initialLanguage)}`}
                            onSuggestionClick={(value) => {
                              setAddress(value);
                            }}
                          />
                        </div>
                      </div>
                      <div className="row m-0">
                        <div className=" col-12 p-0 mb-3">
                          <div className=" font-semibold">
                            {getLabel("specialInstruction", initialLanguage)}
                          </div>
                          <div className=" position-relative">
                            <textarea
                              onChange={(e) => {
                                setInstructions(e.target.value);
                                setSInstructionLength(e.target.value.length);
                              }}
                              className="form-control border border-2 system-control"
                              placeholder={`${getLabel("specialInstruction", initialLanguage)}`}
                              rows={2}
                              aria-label="With textarea"
                              disabled={sInstructionLength >= 400}
                            ></textarea>
                            <div className="text-muted position-absolute font-70  bottom-0 end-0 mx-1 mb-2">
                              <small id="message-count">
                                {sInstructionLength}/400
                              </small>
                            </div>
                          </div>
                        </div>
                      </div>
                    </>
                  )}
                </div>
              </div>
            </div>
            {/* tab */}
          </div>
        </div>

        {/*================================================================================================== */}

        <hr className="mb-3 mt-0 hr-width-form" />
        <div className="row m-0">
          <div className="col-md-4 col-12 p-0 mb-3">
            <div className=" font-semibold">{getLabel("status", initialLanguage)}</div>
            {/* <select
              className="form-select border border-2 bg-white p-2 system-control"
              id="dropdownMenuLink"
            >
              <option defaultValue={defaultValue}>Enable</option>
              <option value="1">Disable</option>
            </select> */}
            <CustomSelect
              data={statusOptions}
              placeholderValue={getLabel("status", initialLanguage)}
              onChange={(value: any) => setStatus(value)}
              is_search={false}
              defaultValue={status}
            />
          </div>
          <div className="col-md-8 col-12 ps-md-2 p-0 mb-3">
            <div className=" font-semibold">
              {getLabel("flowerDeliveryToThisPlace", initialLanguage)}
            </div>
            {/* <select
              onChange={(e) =>
                setIsDeliveryPossible(e.target.value === "true" ? true : false)
              }
              className="form-select border border-2 bg-white p-2 system-control"
              id="dropdownMenuLink"
            >
              <option defaultValue={defaultValue} value={"true"}>
                {getLabel("yes",initialLanguage)}
              </option>
              <option value="false">{getLabel("no",initialLanguage)}</option>
            </select> */}
            <CustomSelect
              data={FlowerDelivery}
              placeholderValue={getLabel("flowerDeliveryToThisPlace", initialLanguage)}
              onChange={(value: any) =>
                setIsDeliveryPossible(value === 1 ? true : false)
              }
              is_search={false}
              defaultValue={is_delivery_possible ? 1 : 2}
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default AddNewEvent;
