import { ACTIVE_PARTNER } from "@/config/constants";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import React, {
  useEffect,
  useState,
  forwardRef,
  useImperativeHandle,
} from "react";
import { Notification } from "@arco-design/web-react";
import { useApiErrorHandling } from "../Others/useApiErrorHandling";
import Image from "next/image";
import { getAllSalutations, identitySelector } from "@/redux/identities";
import {
  createPartnerAdmin,
  partnerAdminSelector,
} from "@/redux/partner-admins";
import { getLabel } from "@/utils/lang-manager";
import CustomSelect from "../Arco/CustomSelect";
export interface ChildModalRef {
  callModalFunction: (callback: (response: any) => void) => void;
}

export interface initialProps {
  initialLang: string;
}
export interface ApiResponse {
  msg: string;
  code: number;
  data: any;
  error: null;
  success: boolean;
  status: any;
}
const PartnerAdminModal = forwardRef<ChildModalRef, initialProps>(
  (props, ref) => {
    const initialLanguage = props.initialLang;

    const [selectedCustomer, setselectedCustomer] = useState(0);
    const dispatch = useAppDispatch();
    const { error: apiError } = useAppSelector(partnerAdminSelector);
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setpassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [status, setStatus] = useState(1);
    const [response, setResponse] = useState("");
    const [loginPswd, setLoginPswd] = useState(false);
    const [loginGoogle, setLoginGoogle] = useState(false);
    const [showPassword, setShowPassword] = useState(false);
    const [showConfirmPassword, setShowConfirmPassword] = useState(false);
    const togglePasswordVisibility = () => {
      setShowPassword(!showPassword);
    };
    const toggleConfirmPasswordVisibility = () => {
      setShowConfirmPassword(!showConfirmPassword);
    };
    const callModalFunction = (callback: any) => {
      const updatedData = {
        first_name: firstName,
        last_name: lastName,
        email: email,
        mobile: status,
        password: password,
        password_confirmation: confirmPassword,
        state_id: status,
        salutation_id: salutationId,
        login_with_password: loginPswd,
        login_with_google: loginGoogle,
      };
      dispatch(
        createPartnerAdmin({
          active_partner: ACTIVE_PARTNER,
          updatedData,
        })
      ).then((response: any) => {
        if (response.payload.success == true) {
          Notification.success({
            title: "Partner admin has been Created Successfully",
            duration: 3000,
            content: undefined,
          });
          console.log(response.payload.data.data.id);
          setselectedCustomer(response?.payload?.data?.data?.id);
          callback(response?.payload?.data?.data);
          setResponse(response);
        } else {
          response.payload.code != 422 &&
            Notification.error({
              title: "Partner admin create Failed",
              duration: 3000,
              content: undefined,
            });
        }
      });
    };
    useImperativeHandle(ref, () => ({
      callModalFunction,
      selectedCustomerId: selectedCustomer,
    }));
    useEffect(() => {}, [apiError]);
    useEffect(() => {
      loadStaticData();
    }, []);
    const loadStaticData = async () => {
      dispatch(getAllSalutations());
    };
    const { data: showSalutationsData } = useAppSelector(identitySelector);
    const [salutationId, setSalutationID] = useState(1);
    const handleSalutationChange = (
      e: React.ChangeEvent<HTMLSelectElement>
    ) => {
      const selectedValue = e.target.value;
      selectedValue;
      setSalutationID(parseInt(selectedValue, 10));
    };
    const error = useApiErrorHandling(apiError, response);
    const statusOptions = [
      { id: 1, name: "Enable" },
      { id: 2, name: "Disable" },
    ];

    return (
      <div>
        <div className="mb-3">
          <div className="row m-0">
            <div className="col-12 p-0 font-semibold">
              {getLabel("status", initialLanguage)}
              <span className="ps-1">*</span>
              {/* <span className="text-danger">*</span> */}
            </div>
            <div className="col-12 p-0">
              {/* <select
              className="form-select border border-2 p-2 system-control font-bold"
              value={status}
              onChange={(e) => setStatus(e.target.value)}
            >
              {statusOptions.map((option) => (
                <option key={option.id} value={option.id}>
                  {option.label}
                </option>
              ))}
            </select> */}
              <CustomSelect
                data={statusOptions}
                placeholderValue={getLabel("selectAStatus", initialLanguage)}
                onChange={(value: any) => setStatus(value)}
                is_search={false}
                defaultValue={status}
              />
              {error.status && (
                <div className="text-danger">{error.status}</div>
              )}
            </div>
          </div>
        </div>
        <div className="mb-3">
          <div className="row m-0">
            <div className="col-12 p-0 font-semibold">
              {getLabel("firstName", initialLanguage)}
              <span className="ps-1">*</span>
              {/* <span className="text-danger">*</span> */}
            </div>
            <div className="col-12 p-0">
              <input
                type="text"
                className="form-control border border-2 p-2 system-control "
                // placeholder="Full Name in English"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                name="text"
                autoComplete="new-text"
              />
              {error.first_name && (
                <div className="text-danger">{error.first_name}</div>
              )}
            </div>
            {/* <div className="row align-items-center m-0 col-12 p-0 ">
            <div className="col-md-3 col-3 p-0 pe-2 ">
              <select
                className="form-select border border-2 bg-white fw-normal p-2 system-control"
                id="dropdownMenuLink"
                defaultValue={salutationId}
                onChange={handleSalutationChange}
              >
                {Array.isArray(showSalutationsData) &&
                  showSalutationsData.map((option: any) => (
                    <option key={option.id} value={option.id}>
                      {option.name}
                    </option>
                  ))}
              </select>
                 <CustomSelect
                    data={showSalutationsData}
                    placeholderValue={getLabel("selectATitle",initialLanguage)}
                    onChange={(value: any) => setSalutationID(value)}
                    is_search={true}
                    defaultValue={salutationId}
                  />
            </div>
            <div className="col-md-9 col-9 p-0">
              <input
                type="text"
                className="form-control border border-2 p-2 system-control "
                placeholder="Full Name in English"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                name="text"
                autoComplete="new-text"
              />
            </div>
          </div> */}
            {/* <div className="row align-items-center m-0 col-12 p-0 ">
            <div className="col-md-3 col-3 p-0 pe-2 "></div>
            <div className="col-md-9 col-9 p-0">
              {error.first_name && (
                <div className="text-danger">{error.first_name}</div>
              )}
            </div>
          </div> */}
          </div>
        </div>
        <div className="mb-3">
          <div className="row m-0">
            <div className="col-12 p-0 font-semibold">
              {getLabel("lastName", initialLanguage)}
              {/* <span className="text-danger">*</span> */}
            </div>
            <div className="col-12 p-0">
              <input
                type="text"
                className="form-control border border-2 p-2 system-control "
                // placeholder="Enter Last Name"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                name="text"
                autoComplete="new-text"
              />
              {error.last_name && (
                <div className="text-danger">{error.last_name}</div>
              )}
            </div>
          </div>
        </div>
        <div className="mb-3">
          <div className="row m-0">
            <div className="col-12 p-0 font-semibold">
              {getLabel("email", initialLanguage)}
              <span className="ps-1">*</span>
              {/* <span className="text-danger">*</span> */}
            </div>
            <div className="col-12 p-0 ">
              <input
                type="text"
                className="form-control border border-2 p-2 system-control "
                // placeholder="Enter Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                name="email"
                autoComplete="new-email"
              />
              {error.email && <div className="text-danger">{error.email}</div>}
            </div>
          </div>
        </div>
        <div className="mb-3">
          <div className="row m-0">
            <div className="col-12 p-0 font-semibold">
              {getLabel("password", initialLanguage)}
              <span className="ps-1">*</span>
              {/* <span className="text-danger">*</span> */}
            </div>
            <div className="col-12 p-0 position-relative ">
              <input
                type={showPassword ? "text" : "password"}
                className="form-control border border-2 p-2 system-control "
                // placeholder="Enter Password"
                value={password}
                onChange={(e) => setpassword(e.target.value)}
                name="password"
                autoComplete="new-password"
              />
              <div className="">
                <Image
                  onClick={togglePasswordVisibility}
                  src={showPassword ? "/openeye.svg" : "/closeeye.svg"}
                  alt={showPassword ? "eyeIcon icon" : "closeeye icon"}
                  width={16}
                  height={16}
                  className="input-group-eye"
                />
              </div>
              {error.password && (
                <div className="text-danger">{error.password}</div>
              )}
            </div>
          </div>
        </div>
        <div className="mb-3">
          <div className="row m-0">
            <div className="col-12 p-0 font-semibold">
              {getLabel("confirmPassword", initialLanguage)}
              <span className="ps-1">*</span>
              {/* <span className="text-danger">*</span> */}
            </div>
            <div className="col-12 p-0 position-relative">
              <input
                type={showConfirmPassword ? "text" : "password"}
                className="form-control border border-2 p-2 system-control "
                // placeholder="Confirm Password"
                value={confirmPassword}
                onChange={(e) => setConfirmPassword(e.target.value)}
                name="password"
                autoComplete="new-password"
              />
              <div className="">
                <Image
                  onClick={toggleConfirmPasswordVisibility}
                  src={showConfirmPassword ? "/openeye.svg" : "/closeeye.svg"}
                  alt={showConfirmPassword ? "eyeIcon icon" : "closeeye icon"}
                  width={16}
                  height={16}
                  className="input-group-eye"
                />
              </div>
              {error.password_confirmation && (
                <div className="text-danger">{error.password_confirmation}</div>
              )}
            </div>
          </div>
        </div>

        <div className="row m-0 mb-3">
          <div className="col-12 col-md-6 d-flex align-items-center gap-3 mb-2 p-0">
            <input
              type="checkbox"
              className=" check-button2"
              // placeholder=""
              checked={loginPswd}
              onChange={() => setLoginPswd(!loginPswd)}
            />
            <div className="col-md-10 col-10 p-0 font-semibold">
              {getLabel("loginWithPassword", initialLanguage)}
            </div>
          </div>
          <div className="col-12 col-md-6 d-flex align-items-center gap-3  mb-2 p-0">
            <input
              type="checkbox"
              className=" check-button2 "
              // placeholder=""
              checked={loginGoogle}
              onChange={() => setLoginGoogle(!loginGoogle)}
            />

            <div className=" col-md-10 col-10 p-0 font-semibold ">
              {getLabel("loginWithGoogle", initialLanguage)}
            </div>
          </div>
        </div>

        <div className="mb-3">
          <div className="row m-0">
            <div className="col-12 p-0 font-bold mb-3">
              {getLabel("assignServiceProviders", initialLanguage)}
              <span className="ps-1">*</span>
            </div>
          </div>
          <div className="d-flex align-items-center gap-3 mb-2">
            <input type="checkbox" className=" check-button2" placeholder="" />
            <div className="font-semibold">Service Provider A</div>
          </div>
          <div className="d-flex align-items-center gap-3">
            <input type="checkbox" className=" check-button2" placeholder="" />
            <div className="font-semibold">Service Provider B</div>
          </div>
        </div>
      </div>
    );
  }
);
export default PartnerAdminModal;
