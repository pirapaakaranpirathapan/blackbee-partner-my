import React, {
  forwardRef,
  useContext,
  useEffect,
  useImperativeHandle,
  useRef,
  useState,
} from "react";

import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import { patchNotice } from "@/redux/notices";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import ButtonGroup from "../Elements/ButtonGroups";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";
import "bootstrap/dist/css/bootstrap.min.css";
import ToastAlert from "../Elements/ToastAlert";
import { memorialPageSelector } from "@/redux/memorial-pages";
import { DatePicker, Notification } from "@arco-design/web-react";
import Loader from "../Loader/Loader";
import PlaceAutocomplete from "../Elements/PlaceAutoComplete";
import { useApiErrorHandling } from "../Others/useApiErrorHandling";
import { getLabel } from "@/utils/lang-manager";
import CustomSelect from "../Arco/CustomSelect";
import CustomDropDownDatepicker from "../Accessories/CustomDropDownDatepicker";
import getYearFromDate from "../Utils/YearExtracter";
import convertDateFormat from "../Utils/MonthFormatter";
export interface ChildModalRef {
  callModalFunction: () => void;
}
interface EditInfoProps {
  onCloseModal: () => void;
}
export interface ApiResponse {
  msg: string;
  code: number;
  data: any;
  error: null;
  success: boolean;
  // Add any other properties if available in the actual response
}
const EditInfo = forwardRef<ChildModalRef, EditInfoProps>((props, ref) => {
  const { handleSave: noticeHandleSave, NoticeData } =
    useContext(NoticeDataContext);
  const { handleSave: pageHandleSave, PageData } = useContext(PageDataContext);
  const handleSave = pageHandleSave || noticeHandleSave;
  const Data = PageData || NoticeData;
  const initialLanguage =
    NoticeData?.initialLanguage || PageData?.initialLanguage;
  const defaultValue = "1";
  const [salutation, setSalutation] = useState(Data?.abbreviation);
  const [salutationId, setSalutationID] = useState(Data?.abbreviation_id);
  const [fullName, setFullName] = useState(Data?.["full_name"]);
  const [nickName, setNickName] = useState(Data?.["nick_name"]);
  const [dob, setDob] = useState<string>("");
  const [dod, setDod] = useState<string>("");
  const [countryId, setCountryId] = useState("131");
  const [deathLocation, setDeathLocation] = useState(Data?.["death_location"]);
  const [isModalFunctionCalled, setIsModalFunctionCalled] = useState(false);
  const [response, setResponse] = useState("");
  const overlayRef = useRef<HTMLDivElement>(null);
  const [Res, setRes] = useState(false);
  const { error: apiError } = useAppSelector(memorialPageSelector);
  const err = useApiErrorHandling(apiError, response);
  // const today = new Date().toISOString().split("T")[0];
  const today = new Date().toISOString().split("T")[0];
  const [error, setError] = useState(true);
  useEffect(() => {
    setSalutation(Data?.abbreviation);
    setSalutationID(Data?.abbreviation_id);
    setFullName(Data?.["full_name"]);
    setNickName(Data?.["nick_name"]);
    setDod(
      Data?.dod && Data?.dod !== "" && Data?.dod.split("-").reverse().join("-")
    );
    setDob(Data?.dob);
    // setCountryId(Data?.Country_id);
    setDeathLocation(Data?.["death_location"]);
  }, [Data?.["full_name"]]);
  const dispatch = useAppDispatch();
  const yearb = new Date(dob).getFullYear();
  const [day, month, year] = dod ? dod.split("-") : ["", "", ""];
  const formattedDate = `${month}-${day}-${year}`;
  const yeard = new Date(formattedDate).getFullYear();
  // console.log("dob", yearb, yeard);
  const countries = Data?.countries;
  // useEffect(() => {
  //   if (yearb > yeard || dob > today) {
  //     setDob("");
  //   }
  // }, []);
  const handleInputChange = (
    e: React.ChangeEvent<HTMLInputElement>,
    field: string
  ) => {
    const value = e.target.value;
    switch (field) {
      case "fullName":
        setFullName(value);
        break;
      case "nickName":
        setNickName(value);
        break;
      // case "dob":
      //   if (!dob || value <= today) {
      //     value <= today ? setDob(value) : setDob("");
      //     value <= dod ? setDod(dod) : setDod("");
      //   } else {
      //     setDob("");
      //   }
      //   break;
      // case "dod":
      //   if (!dod || value <= today) {
      //     value <= today ? setDod(value) : setDod("");
      //     value >= dob ? setDob(dob) : setDob("");
      //   } else {
      //     setDod("");
      //   }
      default:
        break;
    }
  };
  useEffect(() => {
    // Date of Birth validation
    if (dob) {
      if (dob > today) {
        // setDob('');
      } else if (dod && dob > dod) {
        // setDob('');
      }
    }
    // Date of Death validation
    if (dod) {
      if (dod > today) {
        // setDod('');
      } else if (dob && dod < dob) {
        // setDob('');
      }
    }
  }, [dob, dod]);

  const handleInputDateChange = (date: any, field: any) => {
    // console.log("date..", date,  getYearFromDate(date));
    if (date === undefined) {
      date = "";
    }
    switch (field) {
      case "dateOfBirth":
        if (getYearFromDate(date) > yeard) {
          setDod("");
          setDob(date);
        } else {
          setDob(date);
        }
        if (!dod || (date <= dod && date <= today)) {
          // setDob(date);
          if (dod && dob > dod) {
            // setDod('');
          }
        } else {
          // setDob('');
        }
        break;

      case "dateOfDeath":
        const [day, month, year] = date ? date.split("-") : ["", "", ""];
        const formattedDated = `${month}-${day}-${year}`;
        console.log(new Date(formattedDated).getFullYear());
        if (yearb > new Date(formattedDated).getFullYear()) {
          setDob("");
          setDod(convertDateFormat(date, "MM"));
        } else {
          setDod(convertDateFormat(date, "MM"));
        }
        // setDod(date);
        if (!dob || (date <= today && date >= dob)) {
          // setDod(date);
          if (dob >= date) {
            // setDob('');
          }
        } else {
          // setDod('');
        }
        break;

      default:
        break;
    }
  };
  const handleSalutationChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedValue = e.target.value;
    setSalutation(selectedValue);
    selectedValue;
    setSalutationID(selectedValue);
  };
  const handleCountryChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedValue = e.target.value;
    setCountryId(selectedValue);
  };
  const handlePlaceSelect = ({ selectedPlace, field }: any) => {
    console.log(`Selected ${field}:`, selectedPlace);
  };
  const handleSuggestionClick = (description: string) => {
    setDeathLocation(description);
  };
  const callModalFunction = () => {
    const updatedData = {
      name: fullName,
      nickname: nickName,
      dod: dod.split("-").reverse().join("-"),
      dob: dob,
      death_location: deathLocation,
      salutation_id: salutationId,
      country_id: countryId,
    };
    console.log("updatedData", updatedData);

    setIsModalFunctionCalled(true);
    dispatch(
      patchNotice({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        updatedData,
        active_id: Data.active_page as unknown as string,
      })
    )
      .then((response: any) => {
        setResponse(response);
        if (response?.payload?.success) {
          handleSave();
          props.onCloseModal();
          setRes(response?.payload?.success);
          Notification.success({
            title: "Updated successfully",
            duration: 3000,
            content: undefined,
          });
        } else {
          // console.log("Dispatch action failed", response.payload);
          setError(apiError.success);
          response.payload.code != 422 &&
            Notification.error({
              title: "Updated Failed",
              duration: 3000,
              content: undefined,
            });
        }
        setTimeout(() => {
          setIsModalFunctionCalled(false); // Set isModalFunctionCalled to false after a specific duration (e.g., 2 seconds)
        }, 1000);
      })
      .catch((error: any) => {
        // Handle the error response here
      });
  }; // Forward the function through the ref
  useImperativeHandle(ref, () => ({
    callModalFunction,
  }));
  return (
    <>
      <div>
        <div className="row m-0">
          <div className="col-12 font-semibold p-0">
            {getLabel("personName", initialLanguage)}
          </div>
        </div>
        <div className="row align-items-center m-0">
          <div className="col-md-4 col-3 p-0 pe-2 ">
            <CustomSelect
              data={Data?.salutations}
              placeholderValue={"Select a Salutation"}
              onChange={(value: any) => setSalutationID(value)}
              is_search={true}
              defaultData={Data?.salutations}
              defaultValue={salutationId}
            />
          </div>
          <div className="col-md-8 col-9 p-0">
            <input
              type="text"
              className="form-control border border-2 p-2 system-control "
              placeholder="Full Name in English"
              value={fullName}
              onChange={(e) => handleInputChange(e, "fullName")}
            />
          </div>{" "}
        </div>{" "}
        <div className="row align-items-center m-0">
          <div className="col-md-4 col-3 p-0 mb-3 pe-2"></div>
          <div className="col-md-8 col-9 p-0 mb-3">
            {err && <div className="text-danger">{err.name}</div>}
          </div>
        </div>
        <div className="row m-0">
          <div className=" col-12 p-0 font-semibold ">
            {getLabel("preferredName", initialLanguage)}
          </div>
          <div className="col-12 p-0 mb-3">
            <input
              type="text"
              className="form-control border border-2 p-2 system-control "
              placeholder={`${getLabel("knowAs", initialLanguage)}`}
              value={nickName}
              onChange={(e) => handleInputChange(e, "nickName")}
            />
          </div>
        </div>
        <div className="row m-0 ">
          <div className="col-md-6 col-12 p-0 pe-2 pe-md-3 mb-3 ">
            <div className="row m-0">
              <div className="col-12 p-0 font-semibold">
                {getLabel("dateOfDeath", initialLanguage)}
              </div>
              <div className="col-12 p-0 dateOfDeath">
                <DatePicker
                  className="form-control border border-2 system-control py-3"
                  value={convertDateFormat(dod, "MMM")}
                  format="DD-MMM-YYYY"
                  onChange={(date: string) =>
                    handleInputDateChange(date, "dateOfDeath")
                  }
                  onClear={() => setDod("")}
                  disabledDate={(current) => current.isAfter(today, "day")}
                  getPopupContainer={(node) => {
                    const container = node.parentElement;
                    if (container) {
                      return container;
                    }
                    return document.body;
                  }}
                />
              </div>
            </div>
            <div className="row m-0">
              {/* <div className="col-md-4 col-3 p-0 mb-3 pe-2"></div> */}
              <div className="col-12 p-0">
                {err && <div className="text-danger">{err.dod}</div>}
              </div>
            </div>
          </div>
          <div className="col-md-6 col-12 p-0  ps-md-3 mb-3">
            <div className="row m-0">
              <div className="col-12 p-0 font-semibold">
                {getLabel("dateOfBirth", initialLanguage)}
              </div>
              <div className="col-12 p-0 sms">
                <CustomDropDownDatepicker
                  onDateChange={(date: string) =>
                    handleInputDateChange(date, "dateOfBirth")
                  }
                  defaultDate={dob}
                />
              </div>
            </div>
            <div className="row m-0">
              {/* <div className="col-md-4 col-3 p-0 mb-3 pe-2 bg-danger"></div> */}
              <div className="col-md-12 col-12 p-0">
                {err && <div className="text-danger">{err.dob}</div>}
              </div>
            </div>
          </div>
        </div>
        <div className="row m-0">
          <div className=" col-12 p-0 font-semibold ">
            {getLabel("countryOfOrigin", initialLanguage)}
          </div>
          <div className="col-12 p-0 mb-3">
            <CustomSelect
              data={countries}
              placeholderValue={"Select a Country"}
              onChange={(value: any) => setCountryId(value)}
              is_search={true}
              defaultData={countries}
              defaultValue={Data.Country_id}
            />
          </div>
        </div>
        <div className="row m-0">
          <div className=" col-12 p-0 font-semibold ">
            {getLabel("deathPlace", initialLanguage)}
          </div>
          <div className="col-12 p-0 mb-3">
            <PlaceAutocomplete
              className="form-control border border-2 p-2 system-control"
              placeholder={"Search city of Death"}
              onSuggestionClick={handleSuggestionClick}
              dvalue={deathLocation}
            />
          </div>
        </div>
        <div className="row m-0">
          <div className=" col-12 p-0 font-semibold ">
            {getLabel("tags", initialLanguage)}
          </div>
          <div className="col-12 p-0 mb-3">
            <input
              type="text"
              className="form-control border border-2 p-2 system-control "
              placeholder={`${getLabel("tagsPlaceholder", initialLanguage)}`}
            />
          </div>
        </div>
      </div>
    </>
  );
});
export default EditInfo;
