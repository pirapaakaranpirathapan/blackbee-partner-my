import React, { useState, useEffect, use, useContext } from "react";
import Image from "next/image";
import trash from "../../public/trash.svg";
import addMore from "../../public/add-more.svg";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import {
  eventLocationTypesReducer,
  getAllEventLocationTypes,
} from "@/redux/event-location-type";
import PlaceAutocomplete from "../Elements/PlaceAutoComplete";
import { EventTypeSelector, getAllEventsTypes } from "@/redux/event-types";
import { useApiErrorHandling } from "../Others/useApiErrorHandling";
import { noticeSelector } from "@/redux/notices";
import { getLabel } from "@/utils/lang-manager";
import CustomSelect from "../Arco/CustomSelect";
import { DatePicker, TimePicker } from "@arco-design/web-react";
import { Radio } from "@arco-design/web-react";
import CustomTextBox from "../Elements/CustomTextBox";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";
import CustomSelect1 from "../Arco/CustomSelect1";
interface AddNewContactProps {
  actionBtnRef: any;
  handleCreate: (
    address: string,
    is_delivery_possible: boolean,
    instructions: string,
    virtual_event: string,
    eventLocationTypeId: string,
    eventTypeId: string,
    eventInstructions: string,
    status: number
  ) => void;
}
const AddNewEvent = ({ handleCreate, actionBtnRef }: AddNewContactProps) => {
  const tab = "Venue";
  const today = new Date().toISOString().split("T")[0];
  const [activeTab, setActiveTab] = useState(tab);
  const [address, setAddress] = useState("");
  const [is_delivery_possible, setIsDeliveryPossible] = useState(true);
  const [instructions, setInstructions] = useState("");
  const [virtual_event, setVirtualEvent] = useState("");
  const [clickstatus, setClickStatus] = useState(false);
  const [sInstructionLength, setSInstructionLength] = useState(0);
  const [esInstructionLength, seteSInstructionLength] = useState(0);
  const [eventLocationTypeId, setEventLocationTypeId] = useState("1");
  const [eventTypeId, seteventTypeId] = useState("");
  const [eventDate, setEventDate] = useState("");
  const [eventPlace, setEventPlace] = useState("");
  const [eventDateERR, setEventDateERR] = useState("");
  const [eventTypeIdERR, seteventTypeIdERR] = useState(false);
  const [eventInstructions, seteventInstructions] = useState("");
  const { NoticeData } = useContext(NoticeDataContext);
  const { PageData } = useContext(PageDataContext);
  const initialLanguage =
    NoticeData?.initialLanguage || PageData?.initialLanguage;
  const changeTab = (val: any) => {
    setActiveTab(val);
  };
  const { error: apiError } = useAppSelector(noticeSelector);
  const err = useApiErrorHandling(apiError, Response);
  // console.log("L", err.event_type_id);
  useEffect(() => {
    actionBtnRef.current.onclick = () => {
      setClickStatus(true);
    };
    if (clickstatus) {
      // eventDate ? setEventDateERR("") : setEventDateERR("Select Date");
      // eventTypeId ? seteventTypeIdERR(false) : seteventTypeIdERR(true);
      // eventDate ||
      handleCreate(
        address,
        is_delivery_possible,
        instructions,
        virtual_event,
        eventLocationTypeId,
        eventTypeId,
        eventInstructions,
        status
      );
      setClickStatus(false);
    }
  }, [clickstatus]);
  useEffect(() => {
    getEventLocationTypes();
    getEventType();
  }, []);
  useEffect(() => {
    eventDate && setEventDateERR("");
    eventTypeId && seteventTypeIdERR(false);
  }, [eventTypeId || eventDate]);
  const defaultValue = "1";
  const [timeFrames, setTimeFrames] = useState([
    { id: defaultValue, startTime: "", endTime: "" },
  ]);
  const [isSingleTimeFrameWithValues, setIsSingleTimeFrameWithValues] =
    useState(false);
  const checkSingleTime = () => {
    setIsSingleTimeFrameWithValues(
      timeFrames.length === 1 &&
        (timeFrames[0].startTime?.trim() !== "" ||
          timeFrames[0].endTime?.trim() !== "")
    );
  };
  useEffect(() => {
    setIsSingleTimeFrameWithValues(
      timeFrames.length === 1 &&
        (timeFrames[0].startTime || timeFrames[0].endTime ? true : false)
    );
  }, [timeFrames]);
  const handleAddTimeFrame = () => {
    setTimeFrames([
      ...timeFrames,
      { id: Date.now().toString(), startTime: "", endTime: "" },
    ]);
  };
  const handleRemoveTimeFrame = (id: string) => {
    // if (id === defaultValue) {
    //   return; // Prevent removing the default field
    // }
    setTimeFrames(timeFrames.filter((timeFrame) => timeFrame.id !== id));
  };
  // Updating startTime and endTime for the first object in timeFrames
  const updateStartTimeAndEndTime = () => {
    setTimeFrames((prevTimeFrames) => [
      {
        ...prevTimeFrames[0],
        startTime: "",
        endTime: "",
      },
      ...prevTimeFrames.slice(1),
    ]);
  };
  const setToZeroAndRemoveOthers = () => {
    setTimeFrames((prevTimeFrames) => [
      {
        ...prevTimeFrames[0],
        // startTime: "00:00 AM",
        // endTime: "00:00 AM",
        startTime: "",
        endTime: "",
      },
    ]);
  };
  const handleStartTimeChange = (index: number, value: any) => {
    const newTimeFrames = [...timeFrames];
    newTimeFrames[index].startTime = value;
    setTimeFrames(newTimeFrames);
  };

  const handleEndTimeChange = (index: number, value: any) => {
    const newTimeFrames = [...timeFrames];
    newTimeFrames[index].endTime = value;
    setTimeFrames(newTimeFrames);
  };
  const dispatch = useAppDispatch();
  const { data: eventLocationType } = useAppSelector(eventLocationTypesReducer);
  const { data: eventTypeData } = useAppSelector(EventTypeSelector);

  const getEventLocationTypes = () => {
    dispatch(getAllEventLocationTypes());
  };
  const getEventType = () => {
    dispatch(getAllEventsTypes());
  };
  const [status, setStatus] = useState(1);
  const statusOptions = [
    { id: 1, name: "Enable" },
    { id: 2, name: "Disable" },
  ];
  const FlowerDelivery = [
    { id: 1, name: "Yes" },
    { id: 2, name: "No" },
  ];
  const [disable, setDisable] = useState(false);
  const [defaultVal, setDefaultVal] = useState("default");
  const setToMidnight = () => {
    setToZeroAndRemoveOthers();
    setDisable(true);
    setDefaultVal("custom");
  };
  const setToDefault = () => {
    updateStartTimeAndEndTime();
    setDisable(false);
    setDefaultVal("default");
  };
  return (
    <>
      <div>
        <div className="row  align-items-center m-0 ">
          <div className="col-12 p-0 mb-3">
            <div className=" font-semibold">
              {getLabel("eventType", initialLanguage)}
              <span className="ps-1">*</span>
            </div>
            <CustomSelect
              data={eventTypeData}
              placeholderValue={getLabel("selectAEvent", initialLanguage)}
              onChange={(value: any) => seteventTypeId(value)}
              is_search={true}
              defaultValue={""}
            />
            {err && err.event_type_id && (
              <div className="text-danger">{err.event_type_id}</div>
            )}
          </div>
        </div>
        <div className="row m-0">
          <div className="col-md-6 col-12 p-0 mb-3 mb-md-0 pe-md-2">
            <div className=" font-semibold mb-1">
              {getLabel("date", initialLanguage)}
              <span className="ps-1">*</span>
            </div>
            {/* When Update the value use convertDateFormat(eventDate,'MM') */}
            <DatePicker
              className="form-control border border-2 system-control py-3"
              value={eventDate}
              format="DD-MMM-YYYY"
              onChange={(date: string) => setEventDate(date)}
              getPopupContainer={(node) => {
                const container = node.parentElement;
                if (container) {
                  return container;
                }
                return document.body;
              }}
              // onClear={()=>setDod("",initialLanguage)}
              // allowClear={false}
              // disabledDate={(current) => current.isAfter(today, "day")}
            />

            {eventDateERR && <div className="text-danger">{eventDateERR}</div>}
          </div>
          <div className="col-md-6 col-12 p-0 ">
            <div className="mb-md-4 "></div>
            <div className="ps-md-2 event-time-radio ">
              <Radio.Group
                mode="fill"
                type="button"
                defaultValue={defaultVal}
                className="font-semibold"
              >
                <Radio
                  value="default"
                  className={`pe-1 ${
                    defaultVal == "default" ? "b-border" : ""
                  }`}
                  onClick={defaultVal == "custom" ? setToDefault : undefined}
                >
                  Custom Hours
                </Radio>
                <span className="vertical-separator"></span>
                <Radio
                  value="custom"
                  className={`ps-1 ${defaultVal == "custom" ? "b-border" : ""}`}
                  onClick={setToMidnight}
                >
                  Full day event
                </Radio>
              </Radio.Group>
            </div>
            <>
              <div className="row  m-0 mb-1 ps-md-1">
                <label className="font-90 font-bold col-5 p-0 px-2 ">
                  {getLabel("startTime", initialLanguage)}
                </label>
                <label className="font-90 font-bold col-5 p-0 px-2">
                  {getLabel("endTime", initialLanguage)}
                </label>
              </div>
              {timeFrames.map((timeFrame, index) => (
                <div
                  key={timeFrame.id}
                  className={`row m-0 mb-2 ps-md-2   ${
                    disable ? "event-disabled" : ""
                  }`}
                >
                  <div className="col-5 p-0 pe-1 evnt ">
                    <TimePicker
                      use12Hours
                      format="h:mm A"
                      value={timeFrame.startTime}
                      placeholder={disable ? "00.00" : "Start Time"}
                      className="form-control border system-control p-2"
                      disabled={disable}
                      onChange={(value) => handleStartTimeChange(index, value)}
                      onClear={() => checkSingleTime()}
                      getPopupContainer={(node) => {
                        const container = node.parentElement;
                        if (container) {
                          return container;
                        }
                        return document.body;
                      }}
                    />
                  </div>
                  <div className="col-5  p-0 ps-1 evnt ">
                    <TimePicker
                      use12Hours
                      format="h:mm A"
                      value={timeFrame.endTime}
                      placeholder={disable ? "00.00" : " End Time"}
                      className="form-control border system-control p-2"
                      disabled={disable}
                      onChange={(value) => handleEndTimeChange(index, value)}
                      onClear={() => checkSingleTime()}
                      getPopupContainer={(node) => {
                        const container = node.parentElement;
                        if (container) {
                          return container;
                        }
                        return document.body;
                      }}
                    />
                  </div>
                  {timeFrames.length == 1 &&
                    index == 0 &&
                    !disable &&
                    !isSingleTimeFrameWithValues && (
                      <div className="col-2 p-0 pe-1 d-flex align-items-center justify-content-end">
                        <Image
                          src={addMore}
                          alt="add-more"
                          onClick={handleAddTimeFrame}
                          className="pointer"
                        />
                      </div>
                    )}
                  {(index !== 0 ||
                    (timeFrames.length > 1 && index == 0) ||
                    isSingleTimeFrameWithValues) &&
                    !disable && (
                      <div className="col-2 p-0 pe-1 d-flex align-items-center justify-content-end">
                        <Image
                          src={trash}
                          alt="trash"
                          onClick={() => {
                            if (timeFrames.length !== 1) {
                              handleRemoveTimeFrame(timeFrame.id);
                            } else {
                              updateStartTimeAndEndTime();
                            }
                          }}
                          className="pointer"
                        />
                      </div>
                    )}
                </div>
              ))}
              {(isSingleTimeFrameWithValues || timeFrames.length > 1) &&
                !disable && (
                  <div className="row  mb-1 ps-md-2 ">
                    <p className="font-semibold m-0 ">
                      <span
                        className="highlighted-text font-bold system-text-primary pointer text-decoration-underline"
                        onClick={handleAddTimeFrame}
                      >
                        Add
                      </span>
                      &nbsp;additional time
                    </p>
                  </div>
                )}
            </>
          </div>
        </div>
        {/* EVENT UI ======================================================================================== */}
        <div className="row m-0">
          <div className="col-12 p-0  mb-3">
            <div className=" font-semibold">
              {getLabel("eventLocation", initialLanguage)}
            </div>
            <div className="row m-0" id="filter-panel">
              <div className="col-12 p-0">
                <div className="inpage-sidebar">
                  <div className="row  m-0">
                    {eventLocationType.map((item: any) => (
                      <div
                        className={
                          activeTab === item.name
                            ? `py-2 system-text-primary shadow-md font-semibold active font-100 system-warning-light col-4 d-flex align-items-center border border-2  system-border-warning ${
                                item.name === "Venue"
                                  ? "rounded-start border-end-0"
                                  : ""
                              } ${
                                item.name === "Virtual Meeting"
                                  ? "rounded-end"
                                  : ""
                              } ${item.name === "Home" ? "border-end-0" : ""}`
                            : `py-2 rounded-end-0 shadow-md font-semibold font-100 col-4 d-flex align-items-center border border-2 system-border-warning ${
                                item.name === "Venue"
                                  ? "rounded-start border-end-0"
                                  : ""
                              } ${
                                item.name === "Virtual Meeting"
                                  ? "rounded-end "
                                  : ""
                              } ${item.name === "Home" ? "border-end-0" : ""}`
                        }
                        onClick={() => {
                          changeTab(item.name);
                          setEventLocationTypeId(item.id);
                          setSInstructionLength(0);
                          seteSInstructionLength(0);
                          setInstructions("");
                          setAddress("");
                          seteventInstructions("");
                        }}
                      >
                        {item.name}
                      </div>
                    ))}
                  </div>
                </div>
              </div>
              <div className="col-12 p-0 pt-3">
                <div className="">
                  {activeTab === "Venue" && (
                    <>
                      {" "}
                      <div className="row m-0 ">
                        <div className="col-12 p-0 mb-3">
                          <div className=" font-semibold">
                            {getLabel("eventPlaceSearch", initialLanguage)}
                          </div>
                          <PlaceAutocomplete
                            className="form-control border border-2 p-2 system-control"
                            placeholder={"Search city of Death"}
                            dvalue={address}
                            onSuggestionClick={(value: string) => {
                              console.log("hhhh", value);

                              setAddress(value);
                            }}
                          />
                        </div>
                      </div>
                      <div className="row m-0">
                        <div className=" col-12 p-0 mb-3">
                          <div className=" font-semibold">
                            {getLabel("specialInstruction", initialLanguage)}
                          </div>
                          <div className=" position-relative">
                            <CustomTextBox
                              value={instructions}
                              placeholder={`${getLabel(
                                "specialInstruction",
                                initialLanguage
                              )}`}
                              onChange={(e) => {
                                setInstructions(e.target.value);
                                setSInstructionLength(e.target.value.length);
                              }}
                              maxLength={400}
                            />
                          </div>
                        </div>
                      </div>
                    </>
                  )}
                  {activeTab === "Virtual Meeting" && (
                    <>
                      {" "}
                      <div className="row m-0">
                        <div className=" col-12 p-0 mb-3">
                          <div className=" font-semibold">
                            {getLabel("eventInstruction", initialLanguage)}
                          </div>
                          <div className=" position-relative">
                            <CustomTextBox
                              value={eventInstructions}
                              placeholder={`${getLabel(
                                "eventInstructionPlaceholder",
                                initialLanguage
                              )}`}
                              onChange={(e) => {
                                seteventInstructions(e.target.value);
                                seteSInstructionLength(e.target.value.length);
                              }}
                              maxLength={400}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="row m-0">
                        <div className=" col-12 p-0 mb-3">
                          <div className=" font-semibold">
                            {getLabel("specialInstruction", initialLanguage)}
                          </div>
                          <div className=" position-relative">
                            <CustomTextBox
                              value={instructions}
                              placeholder={`${getLabel(
                                "specialInstruction",
                                initialLanguage
                              )}`}
                              onChange={(e) => {
                                setInstructions(e.target.value);
                                setSInstructionLength(e.target.value.length);
                              }}
                              maxLength={400}
                            />
                          </div>
                        </div>
                      </div>
                    </>
                  )}
                  {activeTab === "Home" && (
                    <>
                      {" "}
                      <div className="row m-0 ">
                        <div className="col-12 p-0 mb-3">
                          <div className=" font-semibold">
                            {getLabel("eventPlaceSearch", initialLanguage)}
                          </div>
                          <PlaceAutocomplete
                            className="form-control border border-2 p-2 system-control"
                            placeholder={`${getLabel(
                              "deathPlacePlaceholder",
                              initialLanguage
                            )}`}
                            dvalue={address}
                            onSuggestionClick={(value: string) => {
                              setAddress(value);
                            }}
                          />
                        </div>
                      </div>
                      <div className="row m-0">
                        <div className=" col-12 p-0 mb-3">
                          <div className=" font-semibold">
                            {getLabel("specialInstruction", initialLanguage)}
                          </div>
                          <div className=" position-relative">
                            <CustomTextBox
                              value={instructions}
                              placeholder={`${getLabel(
                                "specialInstruction",
                                initialLanguage
                              )}`}
                              onChange={(e) => {
                                setInstructions(e.target.value);
                                setSInstructionLength(e.target.value.length);
                              }}
                              maxLength={400}
                            />
                          </div>
                        </div>
                      </div>
                    </>
                  )}
                </div>
              </div>
            </div>
            {/* tab */}
          </div>
        </div>

        {/*================================================================================================== */}

        <hr className="mb-3 mt-0 hr-width-form" />
        <div className="row m-0">
          <div className="col-md-4 col-12 p-0 mb-3">
            <div className="font-semibold">
              {getLabel("status", initialLanguage)}
            </div>
            <CustomSelect1
              data={statusOptions}
              placeholderValue={getLabel("status", initialLanguage)}
              onChange={(value: any) => setStatus(value)}
              is_search={false}
              defaultValue={status}
            />
          </div>
          <div className="col-md-8 col-12 ps-md-2 p-0 mb-3">
            <div className=" font-semibold">
              {getLabel("flowerDeliveryToThisPlace", initialLanguage)}
            </div>
            <CustomSelect
              data={FlowerDelivery}
              placeholderValue={getLabel(
                "flowerDeliveryToThisPlace",
                initialLanguage
              )}
              onChange={(value: any) =>
                setIsDeliveryPossible(value === 1 ? true : false)
              }
              is_search={false}
              defaultValue={is_delivery_possible ? 1 : 2}
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default AddNewEvent;
