import { getLabel } from "@/utils/lang-manager";
import React, { useContext, useEffect, useState } from "react";
import { useApiErrorHandling } from "../Others/useApiErrorHandling";
import { useAppSelector } from "@/redux/store/hooks";
import { pageManagerSelector } from "@/redux/page-manager";
import { managerTypesSelector } from "@/redux/managerTypes";
import CustomSelect from "../Arco/CustomSelect";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";

interface AddNewPageProps {
  response: any;
  actionBtnRef: any;
  HandleInviteManager: (managerTypeId: string, email: string) => void;
}

const InviteManagerAlertModal = ({
  HandleInviteManager,
  actionBtnRef,
  response,
}: AddNewPageProps) => {
  const [managerTypeId, setManagerTypeId] = useState("");
  const [email, setEmail] = useState("");
  const [clickstatus, setClickStatus] = useState(false);
  const [err, setErr] = useState({
    msg: "",
    name: "",
    first_name: "",
    last_name: "",
    mobile: "",
    email: "",
    alternative_phone: "",
    password: "",
    password_confirmation: "",
    pageTypeId: "",
    gender_id: "",
    contact_type: "",
    manager_type_id: "",
  });
  const { handleSave, PageData } = useContext(PageDataContext);
  const initialLanguage = PageData?.initialLanguage
  const { error: apiErorrMemorial } = useAppSelector(pageManagerSelector);
  const { errorNotice: apiErorrNotice } = useAppSelector(pageManagerSelector);

  const { data: ManagerTypesData } = useAppSelector(managerTypesSelector);
  // const err = useApiErrorHandling(APIError, response);
  const errorMemorial = useApiErrorHandling(apiErorrMemorial, response);
  const errorNotice = useApiErrorHandling(apiErorrNotice, response);

  useEffect(() => {
    if (errorMemorial) {
      setErr(errorMemorial);
    }
  }, [errorMemorial]);

  useEffect(() => {
    if (errorNotice) {
      setErr(errorNotice);
    }
  }, [errorNotice]);

  useEffect(() => {
    actionBtnRef.current.onclick = () => {
      setClickStatus(true);
    };
    if (clickstatus) {
      HandleInviteManager(managerTypeId, email);
      setClickStatus(false);
    }
  }, [clickstatus]);

  const handleManagerTypeChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedValue = e.target.value;
    setManagerTypeId(selectedValue);
  };
  return (
    <>
      <div className="row m-0 mb-3">
        <div className="col-12 font-semibold mb-0 p-0">{getLabel("email", initialLanguage)}</div>
        <div className="col-12 p-0">
          <input
            type="text"
            className="form-control border border-2 system-control h-46"
            placeholder={`${getLabel("addNewManagerDescription", initialLanguage)}`}
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          {err && <div className="text-danger font-normal">{err.email}</div>}
        </div>
      </div>
      <div className="row m-0 mb-4">
        <div className="col-12 font-semibold mb-0 p-0">
          {getLabel("managerType", initialLanguage)}
        </div>
        <div className="col-12 p-0">
          <CustomSelect
            data={ManagerTypesData}
            placeholderValue={"Select a Manager type"}
            onChange={(value: any) => {
              setManagerTypeId(value);
            }}
            is_search={false}
            defaultData={managerTypeId}
          />

          {err && (
            <div className="text-danger font-normal">{err.manager_type_id}</div>
          )}
        </div>
      </div>
    </>
  );
};

export default InviteManagerAlertModal;
