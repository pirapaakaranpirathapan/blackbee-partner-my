// import { options, uploader } from "@/components/ImageUploder/ImageUploder";
// import s3Client, { bucketName, objectKey } from "@/connections/s3_connection";
// import {PutObjectCommand } from "@aws-sdk/client-s3";
// import React, {useEffect, useState } from "react";
// import { Notification } from '@arco-design/web-react';
// import { UploadButton } from "react-uploader";
// interface Props {
//   name: any;
//   handleUpdate: (
//     uploadImageUrl: string
//   ) => void;
// }
// export const ImageInput = ({ name,handleUpdate }: Props) => {
//   const [uploadImageUrl, setUploadImageUrl] = useState("");
//   const [clickstatus, setClickStatus] = useState(false);
//   //url returned function
//   const handleUpload = async (files: any) => {
//     const file = files[0].originalFile.file as File;
//     const putObjectCommand = new PutObjectCommand({
//       Bucket: bucketName,
//       Key: objectKey,
//       Body: file,
//       ContentType: file.type,
//     });
//     try {
//       const data = await s3Client.send(putObjectCommand);
//       // console.log("Image uploaded successfully", data);
//       const objectUrl = `https://${bucketName}.s3.amazonaws.com/${objectKey}`;
//       setClickStatus(true);
//       setUploadImageUrl(objectUrl);
//     } catch (error) {
//       console.error("Error uploading image", error);
//       Notification.error({
//         title: 'Error uploading image',
//         duration: 3000,
//         content: undefined
//       });
//     } finally {
//       // toast("Image uploaded successfully");
//     }
//   };
//     useEffect (()=>{
//       if(clickstatus){
//         handleUpdate(uploadImageUrl);
//         setClickStatus(false);
//       }
//     },[clickstatus]);

//   return (
//     <div>
//       <UploadButton
//         uploader={uploader}
//         options={options}
//         onComplete={(files) => {
//           handleUpload(files);
//         }}
//       >
//         {({ onClick }) => (
//           <button
//             className="btn btn-sm font-bold system-outline-primary px-2 px-sm-4"
//             onClick={onClick}
//           >
//             {name}
//           </button>
//         )}
//       </UploadButton>
//     </div>
//   );
// };

import React from "react";

function imageInput() {
  return <div>imageInput</div>;
}

export default imageInput;
