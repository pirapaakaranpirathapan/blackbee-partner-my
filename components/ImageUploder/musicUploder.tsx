import React, {
  ChangeEvent,
  forwardRef,
  useContext,
  useEffect,
  useImperativeHandle,
  useRef,
  useState,
} from "react";
import { Notification } from "@arco-design/web-react";
import { PutObjectCommand } from "@aws-sdk/client-s3";
import s3Client, { bucketName } from "@/connections/s3_connection";
import Image from "next/image";
import playIcon from "public/play-black.svg";
import pauseIcon from "public/pause-black.svg";
import { getLabel } from "@/utils/lang-manager";
import MusicSkeleton from "../Skelton/MusicSkelton";
import extractFilenameFromUrl from "../TimeConverter/extractFilenameFromUrl";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
interface Props {
  name: string;
  handleUploadMusic: (uploadMusicUrl: string) => void;
  handleSelectMusic: (uploadMusicUrl: Boolean) => void;
  handleUploadApply: () => void;
  defaultOptionLink: string;
  selectedOptionId: string;
  classname: any;
}
export interface ChildModalRef {
  callModalFunction: () => void;
}
const MusicUploader = forwardRef(
  (
    {
      handleUploadMusic,
      handleUploadApply,
      defaultOptionLink,
      handleSelectMusic,
      selectedOptionId,
      classname,
    }: Props,
    ref
  ) => {
    const uniqueIdentifier = defaultOptionLink?.substring(
      defaultOptionLink?.lastIndexOf("/") + 1
    );
    const { NoticeData } = useContext(NoticeDataContext);
    const initialLanguage = NoticeData?.initialLanguage
    const [uploadMusicUrl, setUploadMusicUrl] = useState<string>("");
    const [musicUrl, setMusicUrl] = useState<string>(defaultOptionLink);
    const [musicFile, setMusicFile] = useState<File | null>(null);
    const [clickStatus, setClickStatus] = useState<boolean>(false);
    const [uploading, setUploading] = useState<boolean>(false);
    const [musicSize, setMusicSize] = useState("");
    const [musicName, setMusicName] = useState("");
    // Function to handle audio upload
    const handleUpload = async (file: File) => {
      const uuid = require("uuid");
      setUploading(true);
      const pushKey = `${uuid.v4()}`;
      const putObjectCommand = new PutObjectCommand({
        Bucket: bucketName,
        Key: musicName,
        Body: file,
        ContentType: file.type,
      });
      try {
        const data = await s3Client.send(putObjectCommand);
        const objectUrl = `https://${bucketName}.s3.amazonaws.com/${musicName}`;
        setClickStatus(true);
        setUploadMusicUrl(objectUrl);
        // objectUrl && setUploading(false);
        handleUploadMusic(objectUrl);
      } catch (error) {
        setUploading(false);
        console.error("Error uploading music", error);
        Notification.error({
          title: "Error uploading music",
          duration: 3000,
          content: undefined,
        });
      } setUploading(false);
    };
    // Function to handle music selection
    const handleMusicSelect = (e: ChangeEvent<HTMLInputElement>) => {
      setMusicSize("");
      // setMusicName("");
      if (e.target.files) {
        const file = e.target.files[0];
        //   compress(file);
        if (file && file.size <= 10 * 1024 * 1024) {
          setMusicFile(file);
          setMusicUrl(URL.createObjectURL(file));
          handleSelectMusic(true);
        } else if (file && file.size > 10 * 1024 * 1024) {
          setMusicSize("Music file must be less than 10MB");
          e.target.value = "";
        }
        if (e.target.files.length > 0) {
          const fileName = e.target.files[0].name;
          setMusicName(fileName);
        }
      }
    };
    // Function to handle music upload after clicking the submit button
    const handleUploadButtonClick = (callback: any) => {
      if (musicFile) {
        handleUpload(musicFile);
        // callback(musicFile);
      }
    };
    // Effect to handle music upload after click status changes
    useEffect(() => {
      if (clickStatus) {
        handleUploadMusic(uploadMusicUrl);
        setClickStatus(false);
      }
    }, [clickStatus, handleUploadMusic, uploadMusicUrl]);
    // useImperativeHandle(ref, () => ({
    //   callModalFunction: (callback: any) => {
    //     handleUploadButtonClick(callback);
    //   },
    // }));
    const audioRef = useRef<HTMLAudioElement | null>(null);
    const [isPlaying, setIsPlaying] = useState(false);
    const playAudio = () => {
      if (audioRef.current) {
        if (isPlaying) {
          audioRef.current.pause();
        } else {
          audioRef.current.play().catch((error) => {
            console.error("Audio playback error:", error);
          });
        }
        setIsPlaying(!isPlaying);
      }
    };
    const [currentTime, setCurrentTime] = useState(0);
    const [duration, setDuration] = useState(0);
    const onSeek = (e: any) => {
      const seekTime = (e.target.value / 100) * duration;
      setCurrentTime(seekTime);
      if (audioRef.current) {
        audioRef.current.currentTime = seekTime;
      }
    };
    return (
      <div className="">
        <div className="">
          <>
            <div className={`${classname === "upload" ? "d-none" : ""}`}>
              <div className="file-input-container mt-2">
                <label
                  htmlFor="musicFileInput"
                  className="btn btn-system-primary"
                >
                  {(musicFile || defaultOptionLink)
                    ? getLabel("changemusic", initialLanguage)
                    : getLabel("uploadmusic", initialLanguage)}
                </label>
                <div className={`${classname === "select" ? "d-none" : ""}`}>
                  <input
                    type="file"
                    id="musicFileInput"
                    accept="audio/mp3, audio/wav"
                    className="form-control"
                    onChange={handleMusicSelect}
                  />
                </div>
              </div>
              {!uploading && !musicSize && (musicFile || defaultOptionLink) && (
                <div className="">
                  <div
                    className={`d-flex justify-content-between align-items-center  w-100 mt-3 mu-bt ${"text-dark"
                      }`}
                  >
                    <div className="d-flex gap-3 align-items-center font-normal-over-flow-f">
                      <div className="d-flex align-items-center justify-content-center pointer ">
                        <Image
                          src={isPlaying ? pauseIcon : playIcon}
                          alt={""}
                          className="sound-container"
                          width={16}
                          height={16}
                          onClick={() => playAudio()}
                        />
                        <div>
                          <audio ref={(audio) => (audioRef.current = audio)} key={musicUrl}>
                            <source
                              src={musicUrl}
                              type="audio/mp3"
                            />
                            Your browser does not support the audio tag.
                          </audio>
                        </div>
                      </div>
                      <p className={` font-110   m-0 p-0 text-truncate ${(!selectedOptionId && defaultOptionLink && !musicFile)
                        ? "system-text-primary"
                        : "text-dark"
                        }`}>
                        {musicFile ? musicName : extractFilenameFromUrl(defaultOptionLink)}
                      </p>
                    </div>
                    <button
                      type="button"
                      className="btn btn-system-primary"
                      disabled={(selectedOptionId || musicFile) ? false : true}
                      onClick={() => {
                        (selectedOptionId && !musicFile) ? handleUploadApply() : handleUploadButtonClick(uploadMusicUrl);
                      }}
                    >
                      Apply
                    </button>
                  </div>
                </div>
              )}
              {uploading && (
                <div className="mt-3">
                  <MusicSkeleton loopTimes={1} />
                </div>
              )}
              {musicSize && <div className="text-danger">{musicSize}</div>}
            </div>
            <div className={`${classname === "select" ? "d-none" : ""}`}>
              <input
                type="file"
                id="musicFileInput"
                accept="audio/mp3, audio/wav"
                className="form-control"
                onChange={handleMusicSelect}
              />
            </div>
          </>
        </div>
      </div>
    );
  }
);
export default MusicUploader;
