import React, { ChangeEvent, useEffect, useState } from "react";
import { Notification, Spin } from "@arco-design/web-react";
import { Button, Modal } from "react-bootstrap";
import { PutObjectCommand } from "@aws-sdk/client-s3";
import s3Client, { bucketName } from "@/connections/s3_connection";
import Image from "next/image";
import cross from "../../public/cross.svg";
import BG from "./../../public/backVideo.svg";
import PinturaImageUploader from "../Pintura";
interface Props {
  name: string;
  handleUploadVideo: (uploadVideoUrl: string) => void;
}

export const VideoUploader = ({ name, handleUploadVideo }: Props) => {
  // State variables
  const [uploadVideoUrl, setUploadVideoUrl] = useState<string>("");
  const [videoUrl, setVideoUrl] = useState<string | null>(null);
  const [videoFile, setVideoFile] = useState<File | null>(null);
  const [btnShow, setBtnShow] = useState<boolean>(false);
  const [btnStatus, setBtnStatus] = useState(false);
  const [clickStatus, setClickStatus] = useState<boolean>(false);
  const [showModal, setShowModal] = useState<boolean>(false);
  const [progress, setProgress] = useState(false);
  const [loading, setLoading] = useState(false);
  // Function to handle video upload
  const handleUpload = async (file: File) => {
    // progress === false &&
    //   Notification.warning({
    //     id: "need_update_duration",
    //     title: "Ready to upload",
    //     content: "Will update after few seconds",
    //     duration: 3000,
    //   });
    setLoading(true);
    setBtnStatus(true);
    const uuid = require("uuid");
    const pushKey = `${uuid.v4()}`;
    const putObjectCommand = new PutObjectCommand({
      Bucket: bucketName,
      Key: pushKey,
      Body: file,
      ContentType: file.type,
    });
    try {
      // setLoading(false);
      const data = await s3Client.send(putObjectCommand).then((data) => {
        console.log("Success", data);

        if (data.$metadata.httpStatusCode === 200) {
          setProgress(true);
          setBtnShow(false);
          Notification.success({
            title: "Video has uploaded successfully",
            duration: 3000,
            content: undefined,
          });
        }
      });

      const objectUrl = `https://${bucketName}.s3.amazonaws.com/${pushKey}`;
      setClickStatus(true);
      setUploadVideoUrl(objectUrl);
      handleCloseModal();
    } catch (error) {
      setLoading(false);
      console.error("Error uploading video", error);
      setBtnStatus(false);
      Notification.error({
        title: "Error uploading video",
        duration: 3000,
        content: undefined,
      });
    }
  };

  // Function to handle video selection
  const handleVideoSelect = (e: ChangeEvent<HTMLInputElement>) => {
    setBtnShow(true);
    if (e.target.files) {
      const file = e.target.files[0];
      //   compress(file);
      setVideoFile(file);
      setVideoUrl(URL.createObjectURL(file));
    }
  };
  //   const compress = (file: File) => {
  //     return new Promise((resolve, reject) => {
  //         const reader = new FileReader();
  //         reader.readAsDataURL(file);
  //         reader.onload = () => resolve(reader.result);
  //         reader.onerror = (error) => reject(error);
  //         setVideoFile(file);
  //     });
  //     };

  // Function to handle video upload after clicking the submit button
  const handleUploadButtonClick = () => {
    if (videoFile) {
      handleUpload(videoFile);
      // handleCloseModal();
    }
  };

  // Effect to handle video upload after click status changes
  useEffect(() => {
    if (clickStatus) {
      handleUploadVideo(uploadVideoUrl);
      setClickStatus(false);
    }
  }, [clickStatus, handleUploadVideo, uploadVideoUrl]);

  // Functions to show/hide the modal
  const handleCloseModal = () => setShowModal(false);
  const handleShowModal = () => {
    setShowModal(true);
    setVideoUrl(null);
    setVideoFile(null);
    setBtnShow(false);
  };

  return (
    <div>
      <Modal show={showModal} onHide={handleCloseModal}>
        <div>
          <Modal.Header className="px-4 py-3 d-flex align-items-center">
            <Modal.Title id="contained-modal-title-vcenter ">
              <div className="">
                <h5 className="mb-0 font-150 font-bold ">
                  {loading
                    ? "Video is uploading...."
                    : "Upload your video here"}
                </h5>
              </div>
            </Modal.Title>

            <div
              // className="pointer d-flex align-items-center justify-content-center"
              className="pointer custom-close-button d-flex align-items-center justify-content-center"
              onClick={handleCloseModal}
            >
              <Image src={cross} alt="world-icon" className="" />
            </div>
            {/* Close */}
          </Modal.Header>
        </div>
        {!loading ? (
          <Modal.Body>
            <div>
              <input
                type="file"
                accept="video/mp4, video/x-m4v, video/*"
                className="form-control mb-4"
                onChange={handleVideoSelect}
              />
              {videoUrl && (
                <video controls style={{ width: "100%", marginTop: 5 }}>
                  <source src={videoUrl ?? ""} type="video/mp4" />
                  Your browser does not support the video tag.
                </video>
              )}
              {/* <PinturaImageUploader
                src={videoUrl ?? ""}
                onVoidProcess={async (res) => {
                  console.log("resPin", res);
                }}
              /> */}
            </div>
          </Modal.Body>
        ) : (
          <Modal.Body className="d-flex justify-content-center align-items-center">
            <div className="image-container">
              {videoUrl ? (
                <video controls style={{ width: "100%", marginTop: 5 }}>
                  <source src={videoUrl} type="video/mp4" />
                  Your browser does not support the video tag.
                </video>
              ) : (
                <Image
                  src={BG}
                  alt="memorialprofile"
                  className="pointer image"
                  width={264}
                  height={264}
                  style={{
                    borderRadius: "12px",
                    border: "2px",
                    minHeight: "410px",
                    minWidth: "410px",
                  }}
                />
              )}
            </div>{" "}
            <div className="overlay">
              <Spin size={40} className="spinner" />
            </div>
          </Modal.Body>
        )}
        {btnShow && (
          <Modal.Footer>
            {loading ? (
              <></>
            ) : (
              <>
                <button
                  className="btn btn-system-primary"
                  onClick={handleUploadButtonClick}
                  disabled={btnStatus}
                >
                  Submit
                </button>
              </>
            )}
          </Modal.Footer>
        )}
      </Modal>
      <button
        type="button"
        className="btn btn-sm system-outline-primary font-bold px-1 px-sm-4"
        onClick={handleShowModal}
      >
        {name}
      </button>
    </div>
  );
};
