import React from "react";
import Image from 'next/image'
import cross from '../public/cross.svg'
type CustomAlertProps = {
  type: "success" | "info" | "warning" | "error"; 
  title: string;
  content: string | React.ReactNode;
  onClose?: () => void;
};

const CustomAlert: React.FC<CustomAlertProps> = ({
  type,
  title,
  content,
  onClose,
}) => {
  return (
    <div
      className={`custom-alert custom-alert-${type} rounded active-link m-2`}
    >
        <div className="">
        <div className="custom-alert-title font-bold font-120">{title}</div>
      <div className="custom-alert-content">{content}</div>
        </div>
      
      {/* <button className="custom-alert-close btn font-bold" onClick={onClose}>
        &times;
      </button> */}
      {onClose&&(<div
              // className="pointer d-flex align-items-center justify-content-center"
              className="pointer d-flex align-items-center justify-content-center pe-2" 
              onClick={onClose}>
              <Image src={cross} alt="world-icon" className="" />
             
            </div>)}
    </div>
  );
};

export default CustomAlert;
