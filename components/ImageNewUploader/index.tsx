import React, { useState, CSSProperties, ReactElement, useEffect, useContext } from "react";
import {
  Upload,
  Button,
  Message,
  Modal,
  Grid,
  Slider,
} from "@arco-design/web-react";
import {
  IconMinus,
  IconPlus,
  IconRotateLeft,
  IconUpload,
} from "@arco-design/web-react/icon";
import EasyCropper from "react-easy-crop";
import { Modal as BooModel } from "react-bootstrap";
import { getLabel } from "@/utils/lang-manager";
import cross from "../../public/cross.svg";
import { PutObjectCommand } from "@aws-sdk/client-s3";
import s3Client, { bucketName } from "@/connections/s3_connection";
import NextImage from "next/image";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";

async function _getCroppedImg(
  url: string,
  pixelCrop: any,
  rotation: number = 0
): Promise<Blob | null> {
  const image = await new Promise<HTMLImageElement>((resolve, reject) => {
    const image = new Image();
    image.addEventListener("load", () => resolve(image));
    image.addEventListener("error", (error) => reject(error));
    image.src = url;
  });

  const canvas = document.createElement("canvas");
  const ctx = canvas.getContext("2d");

  if (!ctx || !image) {
    return null;
  }

  const imageSize =
    2 * ((Math.max(image.width, image.height) / 2) * Math.sqrt(2));
  canvas.width = imageSize;
  canvas.height = imageSize;

  if (rotation) {
    ctx.translate(imageSize / 2, imageSize / 2);
    ctx.rotate((rotation * Math.PI) / 180);
    ctx.translate(-imageSize / 2, -imageSize / 2);
  }

  ctx.drawImage(
    image,
    imageSize / 2 - image.width / 2,
    imageSize / 2 - image.height / 2
  );
  const data = ctx.getImageData(0, 0, imageSize, imageSize);

  canvas.width = pixelCrop.width;
  canvas.height = pixelCrop.height;
  ctx.putImageData(
    data,
    Math.round(0 - imageSize / 2 + image.width * 0.5 - pixelCrop.x),
    Math.round(0 - imageSize / 2 + image.height * 0.5 - pixelCrop.y)
  );
  return new Promise((resolve) => {
    canvas.toBlob((blob) => {
      resolve(blob);
    });
  });
}

interface CropperProps {
  file: File;
  onOk: (file: File) => void;
  onCancel: () => void;
}

const Cropper: React.FC<CropperProps> = ({ file, onOk, onCancel }) => {
  const [crop, setCrop] = useState<{ x: number; y: number }>({ x: 0, y: 0 });
  const [zoom, setZoom] = useState<number>(1);
  const [rotation, setRotation] = useState<number>(0);
  const [croppedAreaPixels, setCroppedAreaPixels] = useState<any>(undefined);

  const url = React.useMemo(() => {
    return URL.createObjectURL(file);
  }, [file]);

  return (
    <div>
      <div
        style={{
          width: "100%",
          height: 280,
          position: "relative",
        }}
      >
        <EasyCropper
          style={{
            containerStyle: {
              width: "100%",
              height: 280,
            },
          }}
          aspect={4 / 4}
          image={url}
          crop={crop}
          zoom={zoom}
          rotation={rotation}
          onRotationChange={setRotation}
          onCropComplete={(_, croppedAreaPixels) => {
            setCroppedAreaPixels(croppedAreaPixels);
          }}
          onCropChange={setCrop}
          onZoomChange={setZoom}
        />
      </div>
      <Grid.Row
        justify="space-between"
        style={{ marginTop: 20, marginBottom: 20 }}
      >
        <Grid.Row
          style={{
            flex: 1,
            marginLeft: 12,
            marginRight: 12,
          }}
        >
          <IconMinus
            style={{ marginRight: 10 }}
            onClick={() => {
              setZoom(Math.max(1, zoom - 0.1));
            }}
          />
          <Slider
            style={{ flex: 1 }}
            step={0.1}
            value={zoom}
            onChange={(v) => {
              setZoom(Number(v));
            }}
            min={0.8}
            max={3}
          />
          <IconPlus
            style={{ marginLeft: 10 }}
            onClick={() => {
              setZoom(Math.min(3, zoom + 0.1));
            }}
          />
        </Grid.Row>
        <IconRotateLeft
          onClick={() => {
            setRotation(rotation - 90);
          }}
        />
      </Grid.Row>

      <Grid.Row justify="end">
        <Button onClick={onCancel} style={{ marginRight: 20 }}>
          Cancel
        </Button>
        <Button
          type="primary"
          onClick={async () => {
            const blob = await _getCroppedImg(
              url || "",
              croppedAreaPixels,
              rotation
            );

            if (blob) {
              const newFile = new File([blob], file.name || "image", {
                type: file.type || "image/*",
              });
              onOk(newFile);
            }
          }}
        >
          OK
        </Button>
      </Grid.Row>
    </div>
  );
};

type Props = {
  name: string;
  textOnly?: boolean;
  edit?: boolean;
  imageUrl?: string;
  headerText: string;
  handleUploadImage: (uploadImageUrl: string) => void;
};

function ImageNewUploader({
  name,
  handleUploadImage,
  textOnly,
  edit,
  imageUrl,
  headerText,
}: Props): ReactElement {
  const { NoticeData } = useContext(NoticeDataContext);
  const { PageData } = useContext(PageDataContext);
  const initialLanguage = NoticeData?.initialLanguage || PageData?.initialLanguage

  const [loading, setLoading] = useState(false);
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const [uploadImageUrl, setUploadImageUrl] = useState("");

  const handleUpload = async (files: any) => {
    setLoading(true);
    const uuid = require("uuid");
    const pushKey = `${uuid.v4()}`;
    const file = files;
    const putObjectCommand = new PutObjectCommand({
      Bucket: bucketName,
      Key: pushKey,
      Body: file,
      ContentType: file.type,
    });
    try {
      const data = await s3Client.send(putObjectCommand);
      const objectUrl = `https://${bucketName}.s3.amazonaws.com/${pushKey}`;
      setClickStatus(true);
      setUploadImageUrl(objectUrl);
      handleClose();
      setLoading(false);
    } catch (error) {
      // setLoading(false);
      // Notification.error({
      //   title: "Error uploading image",
      //   duration: 3000,
      //   content: undefined,
      // });
    } finally {
    }
  };
  const [clickstatus, setClickStatus] = useState(false);
  useEffect(() => {
    if (clickstatus) {
      handleUploadImage(uploadImageUrl);
      setClickStatus(false);
    }
  }, [clickstatus]);
  const [crop, setCrop] = useState<{ x: number; y: number }>({ x: 0, y: 0 });
  const [zoom, setZoom] = useState<number>(1);
  const [rotation, setRotation] = useState<number>(0);
  const [croppedAreaPixels, setCroppedAreaPixels] = useState<any>(undefined);
  return (
    <div>
      {/* model */}
      {textOnly ? (
        <>
          <p
            className="pointer p-0 m-0"
            onClick={() => {
              setShow(true);
            }}
          >
            {name}
          </p>
        </>
      ) : (
        <>
          <button
            type="button"
            className="btn btn-sm system-outline-primary font-bold px-1 px-sm-4"
            onClick={() => {
              setShow(true);
            }}
          >
            {name}
          </button>
        </>
      )}

      <BooModel
        show={show}
        onHide={() => {
          handleClose();
        }}
      >
        <div>
          <BooModel.Header className="px-4 py-3 d-flex align-items-center">
            <BooModel.Title id="contained-modal-title-vcenter ">
              <div className="">
                <h5 className="mb-0 font-150 font-bold ">
                  {loading
                    ? "Photo " + `${getLabel("isUploading", initialLanguage)}`
                    : `${getLabel("", initialLanguage)} ` + headerText + ` ${getLabel("", initialLanguage)}`}
                </h5>
              </div>
            </BooModel.Title>{" "}
            <div
              className="pointer custom-close-button d-flex align-items-center justify-content-center"
              onClick={() => {
                handleClose();
              }}
            >
              <NextImage src={cross} alt="world-icon" className="" />
            </div>
          </BooModel.Header>
        </div>

        <BooModel.Body>
          {edit ? (
            <>
              <div
                style={{
                  width: "100%",
                  height: 280,
                  position: "relative",
                }}
              >
                <EasyCropper
                  style={{
                    containerStyle: {
                      width: "100%",
                      height: 280,
                    },
                  }}
                  aspect={6 / 4}
                  image={imageUrl}
                  crop={crop}
                  zoom={zoom}
                  rotation={rotation}
                  onCropComplete={(_, croppedAreaPixels) => {
                    setCroppedAreaPixels(croppedAreaPixels);
                  }}
                  onCropChange={(crop) => {
                    setCrop(crop);
                  }}
                  onZoomChange={(zoom) => {
                    setZoom(zoom);
                  }}
                />
              </div>
              <Grid.Row
                justify="space-between"
                style={{ marginTop: 20, marginBottom: 20 }}
              >
                <Grid.Row
                  style={{
                    flex: 1,
                    marginLeft: 12,
                    marginRight: 12,
                  }}
                >
                  <IconMinus
                    style={{ marginRight: 10 }}
                    onClick={() => {
                      setZoom(Math.max(1, zoom - 0.1));
                    }}
                  />
                  <Slider
                    style={{ flex: 1 }}
                    step={0.1}
                    value={zoom}
                    onChange={(v) => {
                      setZoom(Number(v));
                    }}
                    min={0.8}
                    max={3}
                  />
                  <IconPlus
                    style={{ marginLeft: 10 }}
                    onClick={() => {
                      setZoom(Math.min(3, zoom + 0.1));
                    }}
                  />
                </Grid.Row>
                <IconRotateLeft
                  onClick={() => {
                    setRotation(rotation - 90);
                  }}
                />
              </Grid.Row>

              <Grid.Row justify="end">
                <Button style={{ marginRight: 20 }}>Cancel</Button>
                <Button
                  type="primary"
                  onClick={async () => {
                    const blob = await _getCroppedImg(
                      imageUrl || "",
                      croppedAreaPixels,
                      rotation
                    );

                    if (blob) {
                      const newFile = new File([blob], "image", {
                        type: "image/*",
                      });
                      handleUpload(newFile);
                    }
                  }}
                >
                  OK
                </Button>
              </Grid.Row>
            </>
          ) : (
            <>
              <Upload
                drag
                listType="picture-card"
                action="/"
                accept="image/png, image/jpeg"
                beforeUpload={(file) => {
                  return new Promise<boolean>((resolve) => {
                    handleClose();

                    const modal = Modal.confirm({
                      title: "Photo Upload",

                      onCancel: () => {
                        Message.info("Photo Upload Cancelled");
                        resolve(false);
                        modal.close();
                      },
                      simple: false,

                      content: (
                        <Cropper
                          file={file}
                          onOk={(file: any) => {
                            resolve(file);
                            modal.close();
                            handleUpload(file);
                          }}
                          onCancel={() => {
                            resolve(false);
                            Message.info(" Photo Upload Cancelled");
                            modal.close();
                          }}
                        />
                      ),
                      footer: null,
                    });
                  });
                }}
              />
            </>
          )}
        </BooModel.Body>
      </BooModel>
    </div>
  );
}

export default ImageNewUploader;
