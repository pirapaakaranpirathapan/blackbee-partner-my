// components/LoginLayout.tsx
import React, { ReactNode } from 'react';

type Props = {
    children?: ReactNode;
};

const ContactLayout = ({ children }: Props) => {
    return (
        <div>
            {children}
        </div>
    );
};

export default ContactLayout;
