// components/LoginLayout.tsx
import { getMe } from '@/redux/me';
import { useAppDispatch } from '@/redux/store/hooks';
import React, { ReactNode, useEffect } from 'react';

type Props = {
  children?: ReactNode;
};

const LoginLayout = ({ children }: Props) => {

  return (
    <div>
      {children}
    </div>
  );
};

export default LoginLayout;
