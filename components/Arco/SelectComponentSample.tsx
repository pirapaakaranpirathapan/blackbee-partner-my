import { Select } from '@arco-design/web-react';
import { useState } from 'react';

const SelectComponent = ({ data, placeholderValue }: any) => {
    const [countryId, setCountryId] = useState("");

    const handleCountryChange = (value: any) => {
        setCountryId(value);
    };

    return (
        <Select
            placeholder={placeholderValue}
            style={{ width: 300 }}
            allowClear
            value={countryId}
            onChange={handleCountryChange}
        >
            {Array.isArray(data) &&
                data.map((option) => (
                    <Select.Option key={option.id} value={option.id}>
                        {option.name}
                    </Select.Option>
                ))}
        </Select>
    );
};

export default SelectComponent;
