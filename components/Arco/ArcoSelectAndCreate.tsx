import React, { useState, useEffect } from 'react';
import { Select, Spin } from "@arco-design/web-react";
import Image from "next/image";
import { LabeledValue, OptionInfo } from "@arco-design/web-react/es/Select/interface";

interface ArcoSelectAndCreateProps {
    data: { data: any[] };
    placeholderValue: string;
    setIsModalVisible: (isVisible: boolean) => void;
    isVisible: boolean;
    is_search: boolean;
    defaultValue: string[];
    updateAddSearchValue: (value: any) => void,
    onOptionChange: (selectedOptions: string[]) => void;
    onDeselect: (value: string | number | LabeledValue, option: OptionInfo) => void;
    onDeselectValue: (value: string | number | LabeledValue, option: OptionInfo) => void;
    onClear: (visible: boolean) => void;
}

const ArcoSelectAndCreate: React.FC<ArcoSelectAndCreateProps> = ({
    data,
    placeholderValue,
    setIsModalVisible,
    isVisible,
    is_search,
    defaultValue,
    updateAddSearchValue,
    onOptionChange,
    onDeselect,
    onDeselectValue,
    onClear,
}) => {
    const { data: OriginalData } = data;
    const [filteredData, setFilteredData] = useState<any[]>([OriginalData]);
    const [selectedOptions, setSelectedOptions] = useState<string[]>([]);
    const [addSearchValue, setAddSearchValue] = useState<string>('');
    const [selectedNames, setSelectedNames] = useState<string[]>(defaultValue);

    useEffect(() => {
        const uniqueValues = new Set([...selectedNames, ...defaultValue]);
        setSelectedNames(Array.from(uniqueValues));
    }, [defaultValue]);

    useEffect(() => {
        const dataArray = Array.isArray(OriginalData) ? OriginalData : [OriginalData];
        setFilteredData(dataArray);
    }, [OriginalData]);

    const handleChange = (option: any) => {
        if (selectedOptions.includes(option.uuid)) {
            const updatedSelectedOptions = selectedOptions.filter((uuid) => uuid !== option.uuid);
            setSelectedOptions(updatedSelectedOptions);
            onOptionChange(updatedSelectedOptions);
        } else {
            const updatedSelectedOptions = [...selectedOptions, option.uuid];
            setSelectedOptions(updatedSelectedOptions);
            onOptionChange(updatedSelectedOptions);
        }
        if (selectedNames.includes(option.name)) {
            setSelectedNames(selectedNames.filter((name) => name !== option.name));
        } else {
            setSelectedNames([...selectedNames, option.name]);
        }
    };

    const handleKeyPress = (event: string) => {
        const selectedOption = filteredData.find((option: any) => option.name === event);
        if (selectedOption) {
            handleChange(selectedOption);
        }
    };

    const addNew = () => {
        if (!filteredData.some((option: any) => option?.name === addSearchValue)) {
            setIsModalVisible(true);
        }
    };

    const handleInputChange = (value: string) => {
        setAddSearchValue(value);
        updateAddSearchValue(value.trim());
    };

    const handleClear = (visible: boolean) => {
        onClear(visible);
        setSelectedNames([]);
    };

    const handleDeselect = (value: string | number | LabeledValue, option: OptionInfo) => {
        const DeselectValue = filteredData.find((option: any) => option.name === value);
        const deselectUuid = DeselectValue?.uuid;
        onDeselect(deselectUuid, option);
        onDeselectValue(value, option);
        setSelectedNames((prevSelectedNames) => prevSelectedNames.filter((name) => name !== value));
        setSelectedOptions((prevSelectedOptions) => prevSelectedOptions.filter((uuid) => uuid !== deselectUuid));
    };

    const popupVisible = isVisible ? false : undefined;

    const renderTag = (props: any, index: any, values: any) => {
        const handleTagClose = (event: React.MouseEvent<HTMLSpanElement, MouseEvent>) => {
            event.stopPropagation();
            if (props.onClose) {
                props.onClose(event);
            }
        };

        return (

            <div className="arco-tag arco-tag-checked arco-tag-size-default arco-input-tag-tag arco-select-tag zoomIn-enter-done" title={props?.value}>
                <span className="arco-tag-content">
                    {props?.value}
                </span>
                <span className="arco-icon-hover arco-tag-icon-hover arco-tag-close-btn" role="button" aria-label="Close" onClick={handleTagClose}>
                    <svg fill="none" stroke="currentColor" stroke-width="4" viewBox="0 0 48 48" aria-hidden="true" focusable="false" className="arco-icon arco-icon-close">
                        <path d="M9.857 9.858 24 24m0 0 14.142 14.142M24 24 38.142 9.858M24 24 9.857 38.142"></path>
                    </svg>
                </span>
            </div>
        )
    };


    return (
        <Select
            mode='multiple'
            placeholder={placeholderValue}
            style={{ width: '100%' }}
            allowClear
            showSearch={is_search}
            onSearch={handleInputChange}
            value={selectedNames}
            onChange={handleKeyPress}
            onClear={handleClear}
            onDeselect={handleDeselect}
            popupVisible={popupVisible}
            renderTag={renderTag}
            getPopupContainer={(node) => {
                const container = node.parentElement;
                if (container) {
                    return container;
                }
                return document.body;
            }}
        >
            {filteredData && filteredData.length > 0 && filteredData.map((option: any) => (
                <Select.Option key={option?.id} value={option?.name} onClick={() => handleChange(option)} >
                    {option ? (
                        <>
                            <div className="d-flex gap-3 align-items-center arco-dropdown-container">
                                <div className='avatar-container bg-grey-100 rounded-circle d-flex align-items-center justify-content-center'>
                                    {option?.photo_url ?
                                        (<Image
                                            src={option?.photo_url}
                                            alt=""
                                            className="image rounded-circle"
                                            width={200}
                                            height={200}
                                        />
                                        ) : (
                                            <p className='m-0 p-0 font-bold font-150'>
                                                {option?.name?.[0]}
                                            </p>
                                        )
                                    }
                                </div>

                                <div className='text-truncate'>
                                    <div className="m-0 p-0 font-150 font-bold line-height-210 pb-1 text-truncate">
                                        {option?.salutation?.name} {option?.name}
                                    </div>
                                    <div className="m-0 p-0 font-100 font-normal line-height-150 system-icon-secondary pb-1">
                                        {option?.dod}{option?.death_location ? "," : ""} {option?.death_location}
                                    </div>
                                    <div className="m-0 p-0 font-100 line-height-150 system-icon-secondary">
                                        Manage by 3rd party
                                    </div>
                                </div>
                            </div>
                        </>
                    ) : (
                        <>
                            <div className="d-flex align-items-center justify-content-center">
                                <Spin />
                            </div>
                        </>
                    )}
                </Select.Option>
            ))}
            {addSearchValue && (
                <li role="option" aria-selected="false" className="arco-select-option-wrapper create-arco-custom" onClick={addNew}>
                    <span className="arco-select-option">
                        <div className="d-flex gap-1 align-items-center justify-content-center font-bold font-150 px-2 system-text-primary py-2">
                            <span className='font-normal'>Create</span>
                            {`"${addSearchValue.trim()}"`}
                        </div>
                    </span>
                </li>
            )}
        </Select>
    );
};

export default ArcoSelectAndCreate;
