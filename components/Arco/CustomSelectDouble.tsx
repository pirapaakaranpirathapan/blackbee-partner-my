import { Select } from '@arco-design/web-react';
import { useEffect, useState } from 'react';

const CustomSelectDouble = ({ data, placeholderValue, onChange, is_search, defaultValue,firstvalue,secondvalue }: any) => {
    const [filteredData, setFilteredData] = useState<string[]>([]);
    // const [id, setId] = useState<string>("");
    const [value, setValue] = useState<string>("");
    useEffect(() => {
        const dataArray = Array.isArray(data) ? data : [data];
        setFilteredData(dataArray);
        const defaultOption = dataArray.find(option => option.id === defaultValue);
        if (defaultOption) {
            setValue(defaultOption.birth_title);
        } else {
          setValue("");
        }
      }, [data, defaultValue]);
    const handleChange = (option: any) => {
        // setId(option.id);
        onChange(option.id)
        setValue(option.birth_title);
    };
    const handleKeyPress = (event: { key: string; }) => {
        const selectedOption = filteredData.find((option:any) => option.birth_title === event);
        if (selectedOption) {
          handleChange(selectedOption);
        }
    };
 
    
    return (
        <Select
            placeholder={placeholderValue}
            style={{ width: "100%" }}
            // allowClear
            showSearch={is_search}
            value={value !== "" ? value : undefined}
            onChange={handleKeyPress}
            getPopupContainer={(node) => {
                const container = node.parentElement;
                if (container) {
                  return container;
                }
                // Return a default element if needed
                // For example, you can return document.body as the fallback
                return document.body;
              }}

        >
            {
                Array.isArray(filteredData) &&
                filteredData.map((option: any) => (
                    <Select.Option key={option.id} value={option.birth_title} onClick={() => handleChange(option)}>
                       {`${option[firstvalue]} - ${option[secondvalue]}`}
                    </Select.Option>
                ))
            }
        </Select >
    );
};

export default CustomSelectDouble;



