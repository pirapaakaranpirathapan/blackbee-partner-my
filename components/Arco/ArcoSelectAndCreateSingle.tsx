import React, { useState, useEffect } from 'react';
import { Select, Spin, Tag } from "@arco-design/web-react";
import Image from "next/image";
import maleo from '../../public/maleo.jpg';
import { LabeledValue, OptionInfo } from "@arco-design/web-react/es/Select/interface";

interface ArcoSelectAndCreateSingleProps {
    data: any[];
    placeholderValue: string;
    setIsModalVisible: (isVisible: boolean) => void;
    isVisible: boolean;
    is_search: boolean;
    defaultValue: string | undefined;
    onOptionChange: (selectedOptions: string) => void;
    updateAddSearchValue: (value: any) => void,
    // onDeselect: (value: string | number | LabeledValue, option: OptionInfo) => void;
    // onClear: (visible: boolean) => void;
}

const ArcoSelectAndCreateSingle: React.FC<ArcoSelectAndCreateSingleProps> = ({
    data,
    placeholderValue,
    setIsModalVisible,
    isVisible,
    is_search,
    defaultValue,
    onOptionChange,
    updateAddSearchValue,
    // onDeselect,
    // onClear,
}) => {
    // const { data: OriginalData } = data;
    const [filteredData, setFilteredData] = useState<any[]>([]);
    const [addSearchValue, setAddSearchValue] = useState<string>('');
    const [selectedNames, setSelectedNames] = useState<string | undefined>(defaultValue);


    useEffect(() => {
        const dataArray = Array.isArray(data) ? data : [data];
        setFilteredData(dataArray);
    }, [data]);

    useEffect(() => {
        setSelectedNames(defaultValue);
    }, [defaultValue]);

    useEffect(() => {
        if (isVisible === true) {
            setSelectedNames(undefined);
        }
    }, [isVisible]);


    const handleChange = (option: any) => {
        // const updatedSelectedOptions = [...selectedOptions, option.uuid];
        const updatedSelectedOptions = option?.id;
        setSelectedOptions(updatedSelectedOptions);
        onOptionChange(updatedSelectedOptions);
        setSelectedNames(option?.first_name);
    };

    const handleKeyPress = (event: string) => {

        const selectedOption = filteredData.find((option: any) => option?.first_name === event);
        if (selectedOption) {
            handleChange(selectedOption);
        }
    };

    const addNew = () => {
        // if (!filteredData.some((option: any) => option?.first_name === addSearchValue)) {
        setIsModalVisible(true);
        // }
    };

    const handleInputChange = (value: string) => {
        setAddSearchValue(value.trim());
        updateAddSearchValue(value.trim());
    };

    const [selectedOptions, setSelectedOptions] = useState<string[]>([]);

    const popupVisible = isVisible ? false : undefined;

    const renderTag = (props: any) => {
        return (
            <>
                {props?.value}
            </>
        );
    };


    return (
        <Select
            placeholder={placeholderValue}
            style={{ width: '100%' }}
            showSearch={is_search}
            onSearch={handleInputChange}
            defaultValue={defaultValue}
            onChange={handleKeyPress}
            value={selectedNames}
            popupVisible={popupVisible}
            renderFormat={renderTag}
            getPopupContainer={(node) => {
                const container = node.parentElement;
                if (container) {
                    return container;
                }
                // Return a default element if needed
                // For example, you can return document.body as the fallback
                return document.body;
            }}
        >
            {filteredData.map((option: any) => (
                <Select.Option key={option?.id} value={option?.first_name} onClick={() => handleChange(option)} className={"single-select-arco-contain"}>
                    {option ? (
                        <>
                            <div className="d-flex gap-3 align-items-center arco-dropdown-container">
                                {/* <div className='avatar-container'>
                                    <Image
                                        src={option?.photo_url || maleo}
                                        alt=""
                                        className="image rounded-circle"
                                        width={200}
                                        height={200}
                                    />
                                </div> */}
                                <div className='avatar-container bg-grey-100 rounded-circle d-flex align-items-center justify-content-center'>
                                    {option?.photo_url ?
                                        (<Image
                                            src={option?.photo_url}
                                            alt=""
                                            className="image rounded-circle"
                                            width={200}
                                            height={200}
                                        />
                                        ) : (
                                            <p className='m-0 p-0 font-bold font-150'>
                                                {option?.first_name?.[0]}{option?.last_name?.[0]}
                                            </p>
                                        )
                                    }
                                </div>
                                <div className='text-truncate'>
                                    <div className="m-0 p-0 font-150 font-bold line-height-210 pb-1 text-truncate">
                                        {option?.salutation?.name} {option?.first_name} {option?.last_name}
                                    </div>
                                    <div className="m-0 p-0 font-normal font-100 line-height-150 system-icon-secondary pb-1">
                                        {option?.mobile}{option?.email ? "," : ""} {option?.email}
                                    </div>
                                    <div className="m-0 p-0 font-100 line-height-150 system-icon-secondary">
                                        {option?.country?.name}
                                    </div>
                                </div>

                            </div>
                        </>
                    ) : (
                        <>
                            <div className="d-flex align-items-center justify-content-center">
                                <Spin />
                            </div>
                        </>
                    )}
                </Select.Option>
            ))}
            {addSearchValue && (
                <li role="option" aria-selected="false" className="arco-select-option-wrapper create-arco-custom" onClick={addNew}>
                    <span className="arco-select-option">
                        <div className="d-flex gap-1 align-items-center justify-content-center font-bold font-150 px-2 system-text-primary py-2">
                            <span className='font-normal'>Create</span>
                            {`"${addSearchValue}"`}
                        </div>
                    </span>
                </li>
            )}
        </Select>
    );
};

export default ArcoSelectAndCreateSingle;