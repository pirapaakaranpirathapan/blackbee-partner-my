import { Select } from "@arco-design/web-react";
import { useEffect, useState } from "react";

const CustomSelectIndex = ({
  data,
  placeholderValue,
  is_search,
  defaultValue,
  handleSelectChange,
  index,
}: any) => {
  const [filteredData, setFilteredData] = useState<string[]>([]);
  const [value, setValue] = useState<string>("");
  const [valueId, setValueId] = useState<string>("");

  useEffect(() => {
    const dataArray = Array.isArray(data) ? data : [data];
    setFilteredData(dataArray);
    const defaultOption = dataArray.find(
      (option) => option.id === defaultValue
    );
    if (defaultOption) {
      setValue(defaultOption.name);
    } else {
      setValue("");
    }
  }, [data, defaultValue, index]);

  const handleChange = (option: any) => {
    setValueId(option.id);
    setValue(option.name);
    handleSelectChange(index, option.id);
  };
  const handleKeyPress = (event: { key: string; }) => {
    const selectedOption = filteredData.find((option: any) => option.name === event);
    if (selectedOption) {
      handleChange(selectedOption);
    }
  };

  return (
    <Select
      placeholder={placeholderValue}
      style={{ width: "100%" }}
      showSearch={is_search}
      value={value !== "" ? value : undefined}
      onChange={handleKeyPress}
    >
      {Array.isArray(filteredData) &&
        filteredData.map((option: any) => (
          <Select.Option
            key={option.id}
            value={option.name}
            onClick={() => {
              handleChange(option);
            }}
          >
            {option.name}
          </Select.Option>
        ))}
    </Select>
  );
};

export default CustomSelectIndex;


// import { Select } from "@arco-design/web-react";
// import { useEffect, useState } from "react";

// const CustomSelectIndex = ({
//   data,
//   placeholderValue,
//   is_search,
//   defaultValue,
//   handleSelectChange,
//   index,
// }: any) => {
//   const [filteredData, setFilteredData] = useState<string[]>([]);
//   const [value, setValue] = useState<string>("");
//   const [valueId, setValueId] = useState<string>("");

//   useEffect(() => {
//     const dataArray = Array.isArray(data) ? data : [data];
//     setFilteredData(dataArray);
//     const defaultOption = dataArray.find(
//       (option) => option.id === defaultValue
//     );
//     if (defaultOption) {
//       setValue(defaultOption.name);
//     } else {
//       setValue("");
//     }
//   }, [data, defaultValue, index]);

//   const handleChange = (option: any) => {
//     setValueId(option.id);
//     setValue(option.name);
//     handleSelectChange(index, option.id);
//   };

//   return (
//     <Select
//       placeholder={placeholderValue}
//       style={{ width: "100%" }}
//       showSearch={is_search}
//       value={value !== "" ? value : undefined}
//       onChange={(event) => {
//         handleSelectChange(index, valueId);
//       }}
//     >
//       {Array.isArray(filteredData) &&
//         filteredData.map((option: any) => (
//           <Select.Option
//             key={option.id}
//             value={option.name}
//             onClick={() => {
//               handleChange(option);
//             }}
//           >
//             {option.name}
//           </Select.Option>
//         ))}
//     </Select>
//   );
// };

// export default CustomSelectIndex;








// import { Select } from "@arco-design/web-react";
// import { useEffect, useState } from "react";

// const CustomSelectIndex = ({
//   data,
//   placeholderValue,
//   is_search,
//   defaultValue,
//   handleSelectChange,
//   index,
// }: any) => {
//   const [filteredData, setFilteredData] = useState<string[]>([]);
//   const [value, setValue] = useState<string>("");
//   const [valueId, setValueId] = useState<string>("");
//   console.log("indexvalueId", valueId);


//   useEffect(() => {
//     const dataArray = Array.isArray(data) ? data : [data];
//     setFilteredData(dataArray);
//     const defaultOption = dataArray.find(
//       (option) => option.id === defaultValue
//     );
//     if (defaultOption) {
//       setValue(defaultOption.name);
//     } else {
//       setValue("");
//     }
//   }, [data, defaultValue, index]);

//   const handleChange = (option: any) => {
//     setValueId(option.id)
//     setValue(option.name);
//   };

//   return (
//     <Select
//       placeholder={placeholderValue}
//       style={{ width: "100%" }}
//       showSearch={is_search}
//       value={value !== "" ? value : undefined}
//       onChange={(event) => {
//         handleSelectChange(index, event);
//       }}
//     >
//       {Array.isArray(filteredData) &&
//         filteredData.map((option: any) => (
//           <Select.Option
//             key={option.id}
//             value={option.name}
//             onClick={() => {
//               handleChange(option);
//             }}
//           >
//             {option.name}
//           </Select.Option>
//         ))}
//     </Select>
//   );
// };

// export default CustomSelectIndex;
