import React, { useState, useRef, useCallback, useEffect } from 'react';
import { Select, Spin, Avatar } from '@arco-design/web-react';
import debounce from 'lodash/debounce';
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER, BASE_ROUTE } from '@/config/constants';
import { useAppDispatch } from '@/redux/store/hooks';
import { globalSearchDeathPerson, searchDeathPerson } from '@/redux/memorial-pages';
import Image from 'next/image';

interface SearchArcoSelectProps {
    useDefaultSearch: boolean;
    placeholderValue: string;
    setIsModalVisible: (isVisible: boolean) => void;
    isVisible: boolean;
    updateCreateSearchValue: (inputValue: string, searchType: string) => void;
    onOptionChange: (selectedUuid: string) => void,
}

const determineSearchType = (inputValue: any) => {
    const searchInputValue = inputValue.trim()
    if (/^\+?\d+$/.test(searchInputValue)) {
        return "Mobile";
    }
    if (/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/.test(searchInputValue)) {
        return "Email";
    }
    if (searchInputValue.length >= 2) {
        return "Name";
    }
    return "";
};

const SearchArcoSelect: React.FC<SearchArcoSelectProps> = ({ useDefaultSearch, placeholderValue, setIsModalVisible, isVisible, updateCreateSearchValue, onOptionChange }) => {
    const [options, setOptions] = useState<any[]>([]);
    const [fetching, setFetching] = useState(false);
    const refFetchId = useRef<number | null>(null);
    const [selectedValue, setSelectedValue] = useState<string | null>(null);
    const inputValueRef = useRef<string>("");
    const popupVisible = isVisible ? false : undefined;

    const dispatch = useAppDispatch();
    const debouncedFetchUser = useCallback(
        debounce(async (inputValue: string) => {
            refFetchId.current = Date.now();
            const fetchId = refFetchId.current;
            setOptions([]);
            inputValueRef.current = inputValue.trim();
            const searchValueInitial = inputValue.trim().length >= 2;
            const searchType = determineSearchType(inputValue);
            updateCreateSearchValue(inputValue.trim(), searchType);

            if (searchValueInitial) {
                try {
                    let dispatchFunction;
                    const searchInputValue = inputValue.trim()
                    if (useDefaultSearch) {
                        if (searchType === "Email") {
                            dispatchFunction = globalSearchDeathPerson
                        } else if (searchType === "Mobile") {
                            if (searchInputValue.length > 8) {
                                dispatchFunction = globalSearchDeathPerson
                            }
                        } else if (searchType === "Name" && searchInputValue.includes("@")) {
                            dispatchFunction = null;
                        } else if (searchType === "Name" && /^\+?\d+$/.test(searchInputValue) && searchInputValue.length < 8) {
                            dispatchFunction = null;
                        } else if (searchType === "Name" && /^\d+[a-zA-Z]/.test(searchInputValue)) {
                            dispatchFunction = null;
                        } else {
                            dispatchFunction = searchDeathPerson;
                        }
                    } else {
                        if (searchType === "Email") {
                            dispatchFunction = globalSearchDeathPerson
                        } else if (searchType === "Mobile") {
                            if (searchInputValue.length > 8) {
                                dispatchFunction = globalSearchDeathPerson
                            }
                        } else if (searchType === "Name" && searchInputValue.includes("@")) {
                            dispatchFunction = null;
                        } else if (searchType === "Name" && /^\+?\d+$/.test(searchInputValue) && searchInputValue.length < 8) {
                            dispatchFunction = null;
                        } else if (searchType === "Name" && /^\d+[a-zA-Z]/.test(searchInputValue)) {
                            dispatchFunction = null;
                        } else {
                            dispatchFunction = globalSearchDeathPerson
                        }
                    }
                    if (dispatchFunction) {
                        setFetching(true);
                        const response: any = await dispatch(dispatchFunction({
                            active_partner: ACTIVE_PARTNER,
                            active_service_provider: ACTIVE_SERVICE_PROVIDER,
                            active_notice: "d82503a8-5695-415f-8c63-657e55462da2" as unknown as string,
                            keywordData: { keyword: searchInputValue },
                        }));
                        if (refFetchId.current === fetchId) {
                            const responseData = response?.payload?.data?.data;
                            const options = responseData.map((user: any) => ({
                                label: (
                                    <>
                                        <div className="d-flex gap-3 align-items-center arco-dropdown-container">
                                            <div className='avatar-container bg-grey-100 rounded-circle d-flex align-items-center justify-content-center'>
                                                {user?.photo_url ?
                                                    (<Image
                                                        src={user?.photo_url}
                                                        alt=""
                                                        className="image rounded-circle"
                                                        width={200}
                                                        height={200}
                                                    />
                                                    ) : (
                                                        <p className='m-0 p-0 font-bold font-150'>
                                                            {user?.name[0]}
                                                        </p>
                                                    )
                                                }
                                            </div>
                                            <div className='text-truncate'>
                                                <div className="m-0 p-0 font-150 font-bold line-height-210 pb-1 text-truncate">
                                                    Mr {user?.name}
                                                </div>
                                                <div className="m-0 p-0 font-100 font-normal line-height-150 system-icon-secondary pb-1">
                                                    2020-02-29, Newham, UK
                                                </div>
                                                <div className="m-0 p-0 font-100 line-height-150 system-icon-secondary">
                                                    Manage by 3rd party
                                                </div>
                                            </div>
                                        </div>
                                    </>
                                ),
                                value: user?.uuid,
                            }));
                            setOptions(options);
                            setFetching(false);
                        }
                    }
                } catch (error) {
                    console.error('Error dispatching or processing data', error);
                    setFetching(false);
                    setOptions([]);
                }
            } else {
                setFetching(false);
                setOptions([]);
            }
        }, 500),
        [useDefaultSearch]
    );

    const handleSelect = (value: any) => {
        if (value === 'createNewItem') {
        } else {
            setSelectedValue(value);
            onOptionChange(value);
        }
    };

    const handleSelectNew = () => {
        setIsModalVisible(true);
    };

    const renderTag = (props: any, index: any, values: any) => {
        if (values[index].value === 'createNewItem') {
            return null
            // return renderCreateOption();
        } else {
            const handleTagClose = (event: React.MouseEvent<HTMLSpanElement, MouseEvent>) => {
                event.stopPropagation();
                if (props.onClose) {
                    props.onClose(event);
                }
            };
            return (
                <div className="arco-tag arco-tag-checked arco-tag-size-default arco-input-tag-tag arco-select-tag zoomIn-enter-done" title={props?.value}>
                    <span className="arco-tag-content">
                        {props?.label?.props?.children.props.children[1]?.props?.children[0]?.props?.children}
                    </span>
                    <span className="arco-icon-hover arco-tag-icon-hover arco-tag-close-btn" role="button" aria-label="Close" onClick={handleTagClose}>
                        <svg fill="none" stroke="currentColor" stroke-width="4" viewBox="0 0 48 48" aria-hidden="true" focusable="false" className="arco-icon arco-icon-close">
                            <path d="M9.857 9.858 24 24m0 0 14.142 14.142M24 24 38.142 9.858M24 24 9.857 38.142"></path>
                        </svg>
                    </span>
                </div>
            );
        }
    };

    const inputValue = inputValueRef.current.trim();

    const shouldRenderDropdown = (
        inputValue.length >= 2 &&
        !(
            inputValue.length >= 2 &&
            /^\+?\d+$/.test(inputValue) &&
            inputValue.length < 9 ||
            /^\d+[a-zA-Z]/.test(inputValue) ||
            (inputValue.length >= 2 &&
                inputValue.includes("@") &&
                !/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/.test(inputValue))
        )
    );

    const renderCreateOption = () => {
        if (shouldRenderDropdown) {
            return (
                <div className="d-flex gap-1 align-items-center justify-content-center font-bold font-150 px-2 system-text-primary py-2 sticky-create-option" onClick={handleSelectNew}>
                    <span className="font-normal">Create</span>
                    {`"${inputValue}"`}
                </div>
            );
        }
        return null;
    };

    const dropDownRender = (menu: any) => {
        if (shouldRenderDropdown) {
            if (fetching) {
                return (
                    <div className="search-arco-select-dropdown pointer">
                        <div
                            style={{
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}
                        >
                            <Spin style={{ margin: 12 }} />
                        </div>
                    </div>
                );
            } else {
                return (
                    <div className="search-arco-select-dropdown pointer">
                        {options.length > 0 && menu}
                        {renderCreateOption()}
                    </div>
                );
            }
        }
        return null;
    };

    return (
        <Select
            style={{ width: "100%" }}
            showSearch
            // mode={!useDefaultSearch ? "multiple" : undefined}
            mode="multiple"
            options={options}
            placeholder={placeholderValue}
            filterOption={false}
            notFoundContent={
                shouldRenderDropdown && (
                    <div
                        style={{
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}
                    >
                        <Spin style={{ margin: 12 }} />
                    </div>
                )
            }
            onSearch={debouncedFetchUser}
            onSelect={handleSelect}
            popupVisible={popupVisible}
            renderTag={renderTag}
            // renderTag={!useDefaultSearch ? renderTag : renderSingleTag}
            dropdownRender={dropDownRender}
        />
    );
};

export default SearchArcoSelect;





// import React, { useState, useRef, useCallback } from 'react';
// import { Select, Spin } from '@arco-design/web-react';
// import debounce from 'lodash/debounce';
// import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from '@/config/constants';
// import { useAppDispatch } from '@/redux/store/hooks';
// import { globalSearchDeathPerson, searchDeathPerson } from '@/redux/memorial-pages';
// import Image from 'next/image';

// interface UserLabelProps {
//     user: any;
// }

// const UserLabel: React.FC<UserLabelProps> = ({ user }) => (
//     <div className="d-flex gap-3 align-items-center arco-dropdown-container">
//         <div className='avatar-container bg-grey-100 rounded-circle d-flex align-items-center justify-content-center'>
//             {user?.photo_url ? (
//                 <Image
//                     src={user?.photo_url}
//                     alt=""
//                     className="image rounded-circle"
//                     width={200}
//                     height={200}
//                 />
//             ) : (
//                 <p className='m-0 p-0 font-bold font-150'>
//                     {user?.name[0]}
//                 </p>
//             )}
//         </div>
//         <div className='text-truncate'>
//             <div className="m-0 p-0 font-150 font-bold line-height-210 pb-1 text-truncate">
//                 Mr {user?.name}
//             </div>
//             <div className="m-0 p-0 font-100 font-normal line-height-150 system-icon-secondary pb-1">
//                 2020-02-29, Newham, UK
//             </div>
//             <div className="m-0 p-0 font-100 line-height-150 system-icon-secondary">
//                 Manage by 3rd party
//             </div>
//         </div>
//     </div>
// );

// interface SearchArcoSelectProps {
//     useDefaultSearch: boolean;
//     placeholderValue: string;
//     setIsModalVisible: (isVisible: boolean) => void;
//     isVisible: boolean;
//     updateCreateSearchValue: (inputValue: string, searchType: string) => void;
//     onOptionChange: (selectedUuid: string) => void;
// }

// const SearchArcoSelect: React.FC<SearchArcoSelectProps> = ({
//     useDefaultSearch,
//     placeholderValue,
//     setIsModalVisible,
//     isVisible,
//     updateCreateSearchValue,
//     onOptionChange,
// }) => {
//     const [options, setOptions] = useState<any[]>([]);
//     const [fetching, setFetching] = useState(false);
//     const [selectedValue, setSelectedValue] = useState<string | null>(null);
//     const refFetchId = useRef<number | null>(null);
//     const inputValueRef = useRef<string>("");
//     const dispatch = useAppDispatch();

//     const debouncedFetchUser = useCallback(
//         debounce(async (inputValue: string) => {
//             refFetchId.current = Date.now();
//             setOptions([]);
//             inputValueRef.current = inputValue.trim();
//             const searchValueInitial = inputValue.trim().length >= 2;
//             const searchType = determineSearchType(inputValue);
//             updateCreateSearchValue(inputValue.trim(), searchType);

//             if (searchValueInitial) {
//                 try {
//                     const dispatchFunction = getDispatchFunctionForGlobalSearch(searchType, inputValue.trim());

//                     if (dispatchFunction) {
//                         setFetching(true);
//                         const response: any = await dispatch(dispatchFunction({
//                             active_partner: ACTIVE_PARTNER,
//                             active_service_provider: ACTIVE_SERVICE_PROVIDER,
//                             active_notice: "d82503a8-5695-415f-8c63-657e55462da2" as unknown as string,
//                             keywordData: { keyword: inputValue.trim() },
//                         }));

//                         if (refFetchId.current === response?.payload?.data?.fetchId) {
//                             const responseData = response?.payload?.data?.data;
//                             const updatedOptions = responseData.map((user: any) => ({
//                                 label: <UserLabel user={user} />,
//                                 value: user?.uuid,
//                             }));

//                             setOptions(updatedOptions);
//                             setFetching(false);
//                         }
//                     }
//                 } catch (error) {
//                     console.error('Error dispatching or processing data', error);
//                     setFetching(false);
//                     setOptions([]);
//                 }
//             } else {
//                 setFetching(false);
//                 setOptions([]);
//             }
//         }, 500),
//         [useDefaultSearch]
//     );

//     const determineSearchType = (inputValue: any) => {
//         const searchInputValue = inputValue.trim();
//         if (/^\+?\d+$/.test(searchInputValue)) {
//             return "Mobile";
//         }
//         if (/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/.test(searchInputValue)) {
//             return "Email";
//         }
//         if (searchInputValue.length >= 2) {
//             return "Name";
//         }
//         return "";
//     };

//     const getDispatchFunctionForGlobalSearch = (searchType: string, searchInputValue: string) => {
//         if (searchType === "Email" || (searchType === "Mobile" && searchInputValue.length > 8)) {
//             return globalSearchDeathPerson;
//         }

//         if (searchType === "Name" && !searchInputValue.includes("@") && /^\+?\d+$/.test(searchInputValue) && searchInputValue.length < 8) {
//             return null;
//         }

//         if (searchType === "Name" && /^\d+[a-zA-Z]/.test(searchInputValue)) {
//             return null;
//         }

//         return searchDeathPerson;
//     };

//     const handleSelect = (value: any) => {
//         if (value === 'createNewItem') {
//             // Handle create new item
//         } else {
//             setSelectedValue(value);
//             onOptionChange(value);
//         }
//     };

//     const handleSelectNew = () => {
//         setIsModalVisible(true);
//     };

//     const renderTag = (props: any, index: any, values: any) => {
//         if (values[index].value === 'createNewItem') {
//             return renderCreateOption();
//         } else {
//             const handleTagClose = (event: React.MouseEvent<HTMLSpanElement, MouseEvent>) => {
//                 event.stopPropagation();
//                 if (props.onClose) {
//                     props.onClose(event);
//                 }
//             };

//             return (
//                 <div className="arco-tag arco-tag-checked arco-tag-size-default arco-input-tag-tag arco-select-tag zoomIn-enter-done" title={props?.value}>
//                     <span className="arco-tag-content">
//                         {props?.label && props.label.props?.children?.props?.children[1]?.props?.children[0]?.props?.children}
//                     </span>
//                     <span className="arco-icon-hover arco-tag-icon-hover arco-tag-close-btn" role="button" aria-label="Close" onClick={handleTagClose}>
//                         <svg fill="none" stroke="currentColor" stroke-width="4" viewBox="0 0 48 48" aria-hidden="true" focusable="false" className="arco-icon arco-icon-close">
//                             <path d="M9.857 9.858 24 24m0 0 14.142 14.142M24 24 38.142 9.858M24 24 9.857 38.142"></path>
//                         </svg>
//                     </span>
//                 </div>
//             );
//         }
//     };

//     const renderCreateOption = () => {
//         const inputValue = inputValueRef.current.trim();

//         if (inputValue.length >= 2 && !(
//             inputValue.length >= 2 && /^\+?\d+$/.test(inputValue) && inputValue.length < 9 ||
//             /^\d+[a-zA-Z]/.test(inputValue) ||
//             (inputValue.length >= 2 && inputValue.includes("@") && !/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/.test(inputValue))
//         )) {
//             return (
//                 <div className="d-flex gap-1 align-items-center justify-content-center font-bold font-150 px-2 system-text-primary py-2 sticky-create-option" onClick={handleSelectNew}>
//                     <span className="font-normal">Create</span>
//                     {`"${inputValue}"`}
//                 </div>
//             );
//         }

//         return null;
//     };

//     return (
//         <Select
//             style={{ width: "100%" }}
//             showSearch
//             mode='multiple'
//             options={options}
//             placeholder={placeholderValue}
//             filterOption={false}
//             onSearch={debouncedFetchUser}
//             onSelect={handleSelect}
//             popupVisible={isVisible ? false : undefined}
//             renderTag={renderTag}
//             dropdownRender={(menu) => (
//                 <>
//                     {fetching ? (
//                         <div className="search-arco-select-dropdown pointer">
//                             <div
//                                 style={{
//                                     display: 'flex',
//                                     alignItems: 'center',
//                                     justifyContent: 'center',
//                                 }}
//                             >
//                                 <Spin style={{ margin: 12 }} />
//                             </div>
//                         </div>
//                     ) : (
//                         <div className="search-arco-select-dropdown pointer">
//                             {options.length > 0 && menu}
//                             {renderCreateOption()}
//                         </div>
//                     )}
//                 </>
//             )}
//         />
//     );
// };

// export default SearchArcoSelect;












// import React, { useState, useRef, useCallback, useEffect } from 'react';
// import { Select, Spin, Avatar } from '@arco-design/web-react';
// import debounce from 'lodash/debounce';
// import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER, BASE_ROUTE } from '@/config/constants';
// import { useAppDispatch } from '@/redux/store/hooks';
// import { globalSearchDeathPerson, searchDeathPerson } from '@/redux/memorial-pages';
// import Image from 'next/image';

// interface SearchArcoSelectProps {
//     useDefaultSearch: boolean;
//     placeholderValue: string;
//     setIsModalVisible: (isVisible: boolean) => void;
//     isVisible: boolean;
//     updateCreateSearchValue: (inputValue: string, searchType: string) => void;
//     onOptionChange: (selectedUuid: string) => void,
// }

// const determineSearchType = (inputValue: any) => {
//     const searchInputValue = inputValue.trim()
//     if (/^\+?\d+$/.test(searchInputValue)) {
//         return "Mobile";
//     }
//     if (/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/.test(searchInputValue)) {
//         return "Email";
//     }
//     if (searchInputValue.length >= 2) {
//         return "Name";
//     }
//     return "";
// };

// const SearchArcoSelect: React.FC<SearchArcoSelectProps> = ({ useDefaultSearch, placeholderValue, setIsModalVisible, isVisible, updateCreateSearchValue, onOptionChange }) => {
//     const [options, setOptions] = useState<any[]>([]);
//     const [fetching, setFetching] = useState(false);
//     const refFetchId = useRef<number | null>(null);
//     const [selectedValue, setSelectedValue] = useState<string | null>(null);
//     const [isCreateNewSelected, setIsCreateNewSelected] = useState(false);
//     const inputValueRef = useRef<string>("");
//     const popupVisible = isVisible ? false : undefined;

//     const dispatch = useAppDispatch();
//     const debouncedFetchUser = useCallback(
//         debounce(async (inputValue: string) => {
//             refFetchId.current = Date.now();
//             const fetchId = refFetchId.current;
//             setOptions([]);
//             inputValueRef.current = inputValue.trim();
//             const searchValueInitial = inputValue.trim().length >= 2;
//             const searchType = determineSearchType(inputValue);
//             updateCreateSearchValue(inputValue.trim(), searchType);

//             if (searchValueInitial) {
//                 try {
//                     let dispatchFunction;
//                     const searchInputValue = inputValue.trim()
//                     if (useDefaultSearch) {
//                         if (searchType === "Email") {
//                             dispatchFunction = globalSearchDeathPerson
//                         } else if (searchType === "Mobile") {
//                             if (searchInputValue.length > 8) {
//                                 dispatchFunction = globalSearchDeathPerson
//                             }
//                         } else if (searchType === "Name" && searchInputValue.includes("@")) {
//                             dispatchFunction = null;
//                         } else if (searchType === "Name" && /^\+?\d+$/.test(searchInputValue) && searchInputValue.length < 8) {
//                             dispatchFunction = null;
//                         } else if (searchType === "Name" && /^\d+[a-zA-Z]/.test(searchInputValue)) {
//                             dispatchFunction = null;
//                         } else {
//                             dispatchFunction = searchDeathPerson;
//                         }
//                     } else if (searchType) {
//                         if (searchType === "Email") {
//                             dispatchFunction = globalSearchDeathPerson
//                         } else if (searchType === "Mobile") {
//                             if (searchInputValue.length > 8) {
//                                 dispatchFunction = globalSearchDeathPerson
//                             }
//                         } else if (searchType === "Name" && searchInputValue.includes("@")) {
//                             dispatchFunction = null;
//                         } else if (searchType === "Name" && /^\+?\d+$/.test(searchInputValue) && searchInputValue.length < 8) {
//                             dispatchFunction = null;
//                         } else if (searchType === "Name" && /^\d+[a-zA-Z]/.test(searchInputValue)) {
//                             dispatchFunction = null;
//                         } else {
//                             dispatchFunction = globalSearchDeathPerson
//                         }
//                     }
//                     if (dispatchFunction) {
//                         setFetching(true);
//                         const response: any = await dispatch(dispatchFunction({
//                             active_partner: ACTIVE_PARTNER,
//                             active_service_provider: ACTIVE_SERVICE_PROVIDER,
//                             active_notice: "d82503a8-5695-415f-8c63-657e55462da2" as unknown as string,
//                             keywordData: { keyword: searchInputValue },
//                         }));
//                         if (refFetchId.current === fetchId) {
//                             const responseData = response?.payload?.data?.data;
//                             const options = responseData.map((user: any) => ({
//                                 label: (
//                                     <>
//                                         <div className="d-flex gap-3 align-items-center arco-dropdown-container">
//                                             <div className='avatar-container bg-grey-100 rounded-circle d-flex align-items-center justify-content-center'>
//                                                 {user?.photo_url ?
//                                                     (<Image
//                                                         src={user?.photo_url}
//                                                         alt=""
//                                                         className="image rounded-circle"
//                                                         width={200}
//                                                         height={200}
//                                                     />
//                                                     ) : (
//                                                         <p className='m-0 p-0 font-bold font-150'>
//                                                             {user?.name[0]}
//                                                         </p>
//                                                     )
//                                                 }
//                                             </div>
//                                             <div className='text-truncate'>
//                                                 <div className="m-0 p-0 font-150 font-bold line-height-210 pb-1 text-truncate">
//                                                     Mr {user?.name}
//                                                 </div>
//                                                 <div className="m-0 p-0 font-100 font-normal line-height-150 system-icon-secondary pb-1">
//                                                     2020-02-29, Newham, UK
//                                                 </div>
//                                                 <div className="m-0 p-0 font-100 line-height-150 system-icon-secondary">
//                                                     Manage by 3rd party
//                                                 </div>
//                                             </div>
//                                         </div>
//                                     </>
//                                 ),
//                                 value: user?.uuid,
//                             }));
//                             setOptions([
//                                 {
//                                     label: (
//                                         <>
//                                             <div className="d-flex gap-1 align-items-center justify-content-center font-bold font-150 px-2 system-text-primary py-2">
//                                                 <span className='font-normal'>Create</span>
//                                                 {`"${inputValueRef.current}"`}
//                                             </div>
//                                         </>
//                                         // <li role="option" aria-selected="false" className="arco-select-option-wrapper create-arco-custom" onClick={handleSelectNew}>
//                                         //     <span className="arco-select-option">
//                                         //         <div className="d-flex gap-1 align-items-center justify-content-center font-bold font-150 px-2 system-text-primary py-2">
//                                         //             <span className='font-normal'>Create</span>
//                                         //             {`"${inputValueRef.current}"`}
//                                         //         </div>
//                                         //     </span>
//                                         // </li>
//                                     ),
//                                     value: 'createNewItem',
//                                 },
//                                 ...options,
//                             ]);
//                             setFetching(false);
//                             // setOptions(options);
//                         }
//                     }
//                 } catch (error) {
//                     console.error('Error dispatching or processing data', error);
//                     setFetching(false);
//                     setOptions([]);
//                 }
//             } else {
//                 setFetching(false);
//                 setOptions([]);
//             }
//         }, 500),
//         [useDefaultSearch]
//     );

//     const handleSelect = (value: any) => {
//         if (value === 'createNewItem') {
//         } else {
//             setSelectedValue(value);
//             onOptionChange(value);
//             setIsCreateNewSelected(false);
//         }
//     };

//     const handleSelectNew = () => {
//         setIsModalVisible(true);
//     };

//     const renderTag = (props: any, index: any, values: any) => {
//         if (values[index].value === 'createNewItem') {
//             return null
//         } else {
//             const handleTagClose = (event: React.MouseEvent<HTMLSpanElement, MouseEvent>) => {
//                 event.stopPropagation();
//                 if (props.onClose) {
//                     props.onClose(event);
//                 }
//             };
//             return (
//                 <div className="arco-tag arco-tag-checked arco-tag-size-default arco-input-tag-tag arco-select-tag zoomIn-enter-done" title={props?.value}>
//                     <span className="arco-tag-content">
//                         {props?.label?.props?.children.props.children[1]?.props?.children[0]?.props?.children}
//                     </span>
//                     <span className="arco-icon-hover arco-tag-icon-hover arco-tag-close-btn" role="button" aria-label="Close" onClick={handleTagClose}>
//                         <svg fill="none" stroke="currentColor" stroke-width="4" viewBox="0 0 48 48" aria-hidden="true" focusable="false" className="arco-icon arco-icon-close">
//                             <path d="M9.857 9.858 24 24m0 0 14.142 14.142M24 24 38.142 9.858M24 24 9.857 38.142"></path>
//                         </svg>
//                     </span>
//                 </div>
//             );
//         }
//     };

//     return (
//         <Select
//             style={{ width: "100%" }}
//             showSearch
//             mode='multiple'
//             options={options}
//             placeholder={placeholderValue}
//             filterOption={false}
//             // allowClear
//             notFoundContent={
//                 fetching && (
//                     <div
//                         style={{
//                             display: 'flex',
//                             alignItems: 'center',
//                             justifyContent: 'center',
//                         }}
//                     >
//                         <Spin style={{ margin: 12 }} />
//                     </div>
//                 )
//             }
//             onSearch={debouncedFetchUser}
//             onSelect={handleSelect}
//             popupVisible={popupVisible}
//             renderTag={renderTag}
//         />
//     );
// };

// export default SearchArcoSelect;




// import React, { useState, useRef, useCallback, useEffect } from 'react';
// import { Select, Spin, Avatar } from '@arco-design/web-react';
// import debounce from 'lodash/debounce';
// import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER, BASE_ROUTE } from '@/config/constants';
// import { useAppDispatch } from '@/redux/store/hooks';
// import { globalSearchDeathPerson, searchDeathPerson } from '@/redux/memorial-pages';
// import Image from 'next/image';

// interface SearchArcoSelectProps {
//     useDefaultSearch: boolean;
//     placeholderValue: string;
//     setIsModalVisible: (isVisible: boolean) => void;
//     isVisible: boolean;
//     updateCreateSearchValue: (inputValue: string, searchType: string) => void;
//     onOptionChange: (selectedUuid: string) => void,
// }

// const determineSearchType = (inputValue: any) => {
//     const searchInputValue = inputValue.trim()
//     if (/^\+?\d+$/.test(searchInputValue)) {
//         return "Mobile";
//     }
//     if (/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/.test(searchInputValue)) {
//         return "Email";
//     }
//     if (searchInputValue.length >= 2) {
//         return "Name";
//     }
//     return "";
// };

// const SearchArcoSelect: React.FC<SearchArcoSelectProps> = ({ useDefaultSearch, placeholderValue, setIsModalVisible, isVisible, updateCreateSearchValue, onOptionChange }) => {
//     const [options, setOptions] = useState<any[]>([]);
//     const [fetching, setFetching] = useState(false);
//     const refFetchId = useRef<number | null>(null);
//     const [selectedValue, setSelectedValue] = useState<string | null>(null);
//     const inputValueRef = useRef<string>("");
//     const popupVisible = isVisible ? false : undefined;

//     const dispatch = useAppDispatch();
//     const debouncedFetchUser = useCallback(
//         debounce(async (inputValue: string) => {
//             refFetchId.current = Date.now();
//             const fetchId = refFetchId.current;
//             setOptions([]);
//             inputValueRef.current = inputValue.trim();
//             const searchValueInitial = inputValue.trim().length >= 2;
//             const searchType = determineSearchType(inputValue);
//             updateCreateSearchValue(inputValue.trim(), searchType);

//             if (searchValueInitial) {
//                 try {
//                     let dispatchFunction;
//                     const searchInputValue = inputValue.trim()
//                     if (useDefaultSearch) {
//                         if (searchType === "Email") {
//                             dispatchFunction = globalSearchDeathPerson
//                         } else if (searchType === "Mobile") {
//                             if (searchInputValue.length > 8) {
//                                 dispatchFunction = globalSearchDeathPerson
//                             }
//                         } else if (searchType === "Name" && searchInputValue.includes("@")) {
//                             dispatchFunction = null;
//                         } else if (searchType === "Name" && /^\+?\d+$/.test(searchInputValue) && searchInputValue.length < 8) {
//                             dispatchFunction = null;
//                         } else if (searchType === "Name" && /^\d+[a-zA-Z]/.test(searchInputValue)) {
//                             dispatchFunction = null;
//                         } else {
//                             dispatchFunction = searchDeathPerson;
//                         }
//                     } else if (searchType) {
//                         if (searchType === "Email") {
//                             dispatchFunction = globalSearchDeathPerson
//                         } else if (searchType === "Mobile") {
//                             if (searchInputValue.length > 8) {
//                                 dispatchFunction = globalSearchDeathPerson
//                             }
//                         } else if (searchType === "Name" && searchInputValue.includes("@")) {
//                             dispatchFunction = null;
//                         } else if (searchType === "Name" && /^\+?\d+$/.test(searchInputValue) && searchInputValue.length < 8) {
//                             dispatchFunction = null;
//                         } else if (searchType === "Name" && /^\d+[a-zA-Z]/.test(searchInputValue)) {
//                             dispatchFunction = null;
//                         } else {
//                             dispatchFunction = globalSearchDeathPerson
//                         }
//                     }
//                     if (dispatchFunction) {
//                         setFetching(true);
//                         const response: any = await dispatch(dispatchFunction({
//                             active_partner: ACTIVE_PARTNER,
//                             active_service_provider: ACTIVE_SERVICE_PROVIDER,
//                             active_notice: "d82503a8-5695-415f-8c63-657e55462da2" as unknown as string,
//                             keywordData: { keyword: searchInputValue },
//                         }));
//                         if (refFetchId.current === fetchId) {
//                             const responseData = response?.payload?.data?.data;
//                             const options = responseData.map((user: any) => ({
//                                 label: (
//                                     <>
//                                         <div className="d-flex gap-3 align-items-center arco-dropdown-container">
//                                             <div className='avatar-container bg-grey-100 rounded-circle d-flex align-items-center justify-content-center'>
//                                                 {user?.photo_url ?
//                                                     (<Image
//                                                         src={user?.photo_url}
//                                                         alt=""
//                                                         className="image rounded-circle"
//                                                         width={200}
//                                                         height={200}
//                                                     />
//                                                     ) : (
//                                                         <p className='m-0 p-0 font-bold font-150'>
//                                                             {user?.name[0]}
//                                                         </p>
//                                                     )
//                                                 }
//                                             </div>
//                                             <div className='text-truncate'>
//                                                 <div className="m-0 p-0 font-150 font-bold line-height-210 pb-1 text-truncate">
//                                                     Mr {user?.name}
//                                                 </div>
//                                                 <div className="m-0 p-0 font-100 font-normal line-height-150 system-icon-secondary pb-1">
//                                                     2020-02-29, Newham, UK
//                                                 </div>
//                                                 <div className="m-0 p-0 font-100 line-height-150 system-icon-secondary">
//                                                     Manage by 3rd party
//                                                 </div>
//                                             </div>
//                                         </div>
//                                     </>
//                                 ),
//                                 value: user?.uuid,
//                             }));
//                             // setOptions([
//                             //     {
//                             //         label: (
//                             //             <>
//                             //                 <div className="d-flex gap-1 align-items-center justify-content-center font-bold font-150 px-2 system-text-primary py-2" onClick={handleSelectNew}>
//                             //                     <span className='font-normal'>Create</span>
//                             //                     {`"${inputValueRef.current}"`}
//                             //                 </div>
//                             //             </>
//                             //             // <li role="option" aria-selected="false" className="arco-select-option-wrapper create-arco-custom" onClick={handleSelectNew}>
//                             //             //     <span className="arco-select-option">
//                             //             //         <div className="d-flex gap-1 align-items-center justify-content-center font-bold font-150 px-2 system-text-primary py-2">
//                             //             //             <span className='font-normal'>Create</span>
//                             //             //             {`"${inputValueRef.current}"`}
//                             //             //         </div>
//                             //             //     </span>
//                             //             // </li>
//                             //         ),
//                             //         value: 'createNewItem',
//                             //     },
//                             //     ...options,
//                             // ]);
//                             setOptions(options);
//                             setFetching(false);
//                         }
//                     }
//                 } catch (error) {
//                     console.error('Error dispatching or processing data', error);
//                     setFetching(false);
//                     setOptions([]);
//                 }
//             } else {
//                 setFetching(false);
//                 setOptions([]);
//             }
//         }, 500),
//         [useDefaultSearch]
//     );

//     const handleSelect = (value: any) => {
//         if (value === 'createNewItem') {
//         } else {
//             setSelectedValue(value);
//             onOptionChange(value);
//         }
//     };

//     const handleSelectNew = () => {
//         setIsModalVisible(true);
//     };

//     const renderTag = (props: any, index: any, values: any) => {
//         if (values[index].value === 'createNewItem') {
//             // return null
//             return renderCreateOption();
//         } else {
//             const handleTagClose = (event: React.MouseEvent<HTMLSpanElement, MouseEvent>) => {
//                 event.stopPropagation();
//                 if (props.onClose) {
//                     props.onClose(event);
//                 }
//             };
//             return (
//                 <div className="arco-tag arco-tag-checked arco-tag-size-default arco-input-tag-tag arco-select-tag zoomIn-enter-done" title={props?.value}>
//                     <span className="arco-tag-content">
//                         {props?.label?.props?.children.props.children[1]?.props?.children[0]?.props?.children}
//                     </span>
//                     <span className="arco-icon-hover arco-tag-icon-hover arco-tag-close-btn" role="button" aria-label="Close" onClick={handleTagClose}>
//                         <svg fill="none" stroke="currentColor" stroke-width="4" viewBox="0 0 48 48" aria-hidden="true" focusable="false" className="arco-icon arco-icon-close">
//                             <path d="M9.857 9.858 24 24m0 0 14.142 14.142M24 24 38.142 9.858M24 24 9.857 38.142"></path>
//                         </svg>
//                     </span>
//                 </div>
//             );
//         }
//     };

//     const renderCreateOption = () => (
//         <div className="d-flex gap-1 align-items-center justify-content-center font-bold font-150 px-2 system-text-primary py-2 sticky-create-option" onClick={handleSelectNew}>
//             <span className='font-normal'>Create</span>
//             {`"${inputValueRef.current}"`}
//         </div>
//     );

//     return (
//         <Select
//             style={{ width: "100%" }}
//             showSearch
//             mode='multiple'
//             options={options}
//             placeholder={placeholderValue}
//             filterOption={false}
//             notFoundContent={
//                 fetching && (
//                     <div
//                         style={{
//                             display: 'flex',
//                             alignItems: 'center',
//                             justifyContent: 'center',
//                         }}
//                     >
//                         <Spin style={{ margin: 12 }} />
//                     </div>
//                 )
//             }
//             onSearch={debouncedFetchUser}
//             onSelect={handleSelect}
//             popupVisible={popupVisible}
//             renderTag={renderTag}
//             dropdownRender={(menu) => (
//                 <div className="search-arco-select-dropdown pointer">
//                     {menu}
//                     {/* {renderCreateOption()} */}
//                     {fetching && renderCreateOption()}
//                 </div>
//             )}
//         />
//     );
// };

// export default SearchArcoSelect;