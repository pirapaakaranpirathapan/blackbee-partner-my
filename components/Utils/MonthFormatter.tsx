
const convertDateFormat = (inputDate: string, outputFormat: string) => {
  const processDateParts = (dateParts: any[], toCustomFormat: boolean) => {
    const monthNames = [
      'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
      'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
    ];

    const monthIndex = toCustomFormat ? parseInt(dateParts[1], 10) || 1 : monthNames.indexOf(dateParts[1]) + 1 || 1;
    const paddedMonth = monthIndex < 10 ? `0${monthIndex}` : monthIndex;

    return `${dateParts[0]}-${toCustomFormat ? monthNames[monthIndex - 1] : paddedMonth}-${dateParts[2]}`;
  };

  const parts = inputDate?.split('-');

  if (parts?.length === 3) {
    if (outputFormat === 'MMM' || outputFormat === 'MM') {
      return processDateParts(parts, outputFormat === 'MMM');
    } else {
      console.error('Invalid output format');
    }
  } else {
    console.error('Invalid input date format');
  }

  return inputDate;
};
  export default convertDateFormat;