import { Notification } from "@arco-design/web-react";

interface CustomToastOptions {
  title: string;
  duration?: number;
  content?: string;
  status?: number;
}

const CustomToast = (options: CustomToastOptions) => {
  const { title, duration = 3000, content, status = 200 } = options;

  const notificationConfig = {
    title,
    duration,
    content: content || undefined,
  };

  if (status === 200) {
    Notification.success(notificationConfig);
  } else if (status !== 422) {
    Notification.error(notificationConfig);
  }
};

export default CustomToast;
