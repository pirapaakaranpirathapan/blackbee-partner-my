const getYearFromDate = (dateString: any): number => {
  if (dateString) {
    if (dateString.endsWith('--')) {
      dateString = dateString.slice(0, -2);
    }

    const dateParts = dateString.split('-');
    
    // Ensure there is at least a year part
    if (dateParts.length >= 1) {
      const year = parseInt(dateParts[0], 10);
      
      // Check if the parsed year is a valid number
      if (!isNaN(year)) {
        return year;
      }
    }
  }
  return 0;
};

export default getYearFromDate;
