import Image from "next/image";
import React, { useContext, useRef, useState } from "react";
import draggable from "../../public/draggable.svg";
import FullScreenModal from "../Elements/FullScreenModal";
import ViewContact from "../Modal/ViewContact";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import {
  deleteMemorialContact,
  getAllMemorialContact,
  memorialContactSelector,
  updateMemorialContact,
} from "@/redux/momorial-contact";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import { useRouter } from "next/router";
import { Notification } from "@arco-design/web-react";
import AlertModal from "../Elements/AlertModal";
import { getLabel } from "@/utils/lang-manager";
import ViewUpdateContact from "../Memorials/MemorialAbout/ViewUpdateContact";
import { integer } from "aws-sdk/clients/cloudfront";
import { Button } from "react-bootstrap";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";
import AlertModalOne from "../Elements/AlertModalOne";
const MemorialContactCard = (props: any) => {
  const { data, handleload, handledelete } = props;
  const [response, setResponse] = useState("");
  const actionBtnRef = useRef<HTMLButtonElement>(null);
  const [showAddContact, setShowAddContact] = useState(false);
  const dispatch = useAppDispatch();
  const router = useRouter();
  const { NoticeData } = useContext(NoticeDataContext);
  const { PageData } = useContext(PageDataContext);
  const initialLanguage =
    NoticeData?.initialLanguage || PageData?.initialLanguage;
  const { id } = router.query;
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [isDisabled, setIsDisabled] = useState(false);
  const deleteContact = (uuid: any) => {
    handledelete();
    setShowAddContact(false);
    setIsDisabled(true);
    dispatch(
      deleteMemorialContact({
        activePartner: ACTIVE_PARTNER,
        activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
        activePageId: id as unknown as string,
        contactId: uuid,
      })
    ).then((res: any) => {
      if (res?.payload?.success === true) {
        setIsDisabled(false);
        Notification.success({
          title: "Contact has been deleted successfully",
          duration: 3000,
          content: undefined,
        });
        setShowDeleteModal(false);
        dispatch(
          getAllMemorialContact({
            activePartner: ACTIVE_PARTNER,
            activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
            activePageId: id as unknown as string,
            page: 1,
          })
        );
      } else {
        setIsDisabled(false);
        Notification.error({
          title: "Contact Create Failed",
          duration: 3000,
          content: undefined,
        });
      }
    });
  };

  const handleUpdate = (
    personName: string,
    countryId: string,
    relationship: number,
    visibilityId: integer,
    auto_hide: boolean,
    contactTypes: any
  ) => {
    updateContact(
      personName,
      countryId,
      relationship,
      visibilityId,
      auto_hide,
      contactTypes
    );
  };

  const updateContact = (
    personName: any,
    countryId: any,
    relationship: any,
    visibilityId: any,
    hideAfterFuneral: any,
    contactTypes: any
  ) => {
    const updatedData = {
      name: personName,
      country_id: countryId,
      relationship_id: relationship,
      contact_details: contactTypes,
      // relationship: relationship,
      visibility_id: visibilityId,
      auto_hide: hideAfterFuneral,
    };
    console.log("888***", updatedData);
    setSubmitButtonDisabled(true);

    dispatch(
      updateMemorialContact({
        activePartner: ACTIVE_PARTNER,
        activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
        activePageId: id as unknown as string,
        uuid: props?.data?.uuid,
        updatedData,
      })
    ).then((response: any) => {
      if (response.payload.success == true) {
        setResponse(response);
        handleload();
        Notification.success({
          title: "Contact has been updated successfully",
          duration: 3000,
          content: undefined,
        });
        setSubmitButtonDisabled(false);
      } else {
        response.payload.code != 422 &&
          Notification.error({
            title: "Faild to create contact",
            duration: 3000,
            content: undefined,
          });
        setSubmitButtonDisabled(false);
      }
    });
  };
  const [submitButtonDisabled, setSubmitButtonDisabled] = useState(false);
  return (
    <>
      <AlertModal
        show={showDeleteModal}
        isSmallModal={true}
        isNotiesModal={true}
        notiesId={props?.data?.uuid}
        position="centered"
        title="Confirmation"
        onClose={() => setShowDeleteModal(false)}
      >
        <div className="mb-4 d-flex conform-color font-100">
          Do you want to delete it?
          {/* <span
              className="text-truncate ps-1 font-semibold"
              style={{
                maxWidth: "100px",
              }}
            >
              {props?.data?.name}
            </span>
            ? */}
        </div>
        <div className="col-auto">
          <button
            className="btn btn-system-primary me-3"
            onClick={() => setShowDeleteModal(false)}
          >
            {getLabel("no", initialLanguage)}
          </button>
          <span
            className="yes-delete-it pointer font-120"
            onClick={() => {
              isDisabled ? null : deleteContact(props?.data?.uuid);
            }}
          >
            {getLabel("YesDeleteIt", initialLanguage)}
          </span>
        </div>
      </AlertModal>
      <FullScreenModal
        show={showAddContact}
        onClose={() => {
          setShowAddContact(false);
        }}
        title={`${getLabel("showContact", initialLanguage)}`}
        description={`${getLabel("showContactDescription", initialLanguage)}`}
        footer={
          <div style={{ display: "flex" }}>
            <Button
              ref={actionBtnRef}
              className="btn-system-primary"
              disabled={submitButtonDisabled}
            >
              Submit
            </Button>
            <a
              onClick={() => {
                setShowAddContact(false);
                setShowDeleteModal(true);
              }}
              style={{ marginLeft: "25px", marginTop: "7px" }}
            >
              <div className="text-danger pointer">
                {getLabel("DeleteTheContact", initialLanguage)}
              </div>
            </a>
          </div>
        }
      >
        {/* <ViewContact
          name={props?.data?.name}
          country={props?.data?.country?.id}
          text={"tesing"}
          hideAfterFuneral={props?.data?.auto_hids}
          visibility={props?.data?.visibility.id}
          contactFrame={props?.data?.contact_details}
          relationshipId={props?.data?.relationship?.id}
        /> */}

        <ViewUpdateContact
          actionBtnRef={actionBtnRef}
          handleUpdate={handleUpdate}
          response={response}
          name={props?.data?.name}
          country={props?.data?.country?.id}
          relationshipId={props?.data?.relationship?.id}
          visibilityid={props?.data?.visibility.id}
          hideAfterFuneralget={props?.data?.auto_hids}
          contactNameValue={props?.data?.contact_details}
        />
      </FullScreenModal>
      <div className="card border mb-3  ">
        <div className=" card-body mx-3 my-2 ps-2 p-0">
          <div className="row ">
            <div className=" col-7 m-0 p-0 ">
              <div className="d-flex align-items-start gap-2">
                <div className="">
                  <Image src={draggable} alt="memorialprofile" className="" />
                </div>

                <p className="m-0 text-muted-dark text-truncate">
                  <strong className=" font-bold ">{props?.data?.name}</strong>
                  <br />
                  <span className="font-80 font-normal text-truncate">
                    {props?.data?.country?.name}
                  </span>
                </p>
              </div>
            </div>
            <div className="col-5 d-flex justify-content-end align-items-center pe-2">
              <button
                type="button"
                className="btn btn-sm border system-view-secondary py-1 px-4 font-90 font-semibold"
                onClick={() => {
                  setShowAddContact(true);
                }}
              >
                {getLabel("view", initialLanguage)}
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default MemorialContactCard;
