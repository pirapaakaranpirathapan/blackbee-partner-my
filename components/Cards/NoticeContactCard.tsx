import Image from "next/image";
import React, { useContext, useEffect, useRef, useState } from "react";
import draggable from "../../public/draggable.svg";
import FullScreenModal from "../Elements/FullScreenModal";
import ViewContact from "../Modal/ViewContact";
import { Button } from "react-bootstrap";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import {
  deleteMemorialContact,
  memorialContactSelector,
  updateMemorialContact,
} from "@/redux/momorial-contact";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import { useRouter } from "next/router";
import { Notification } from "@arco-design/web-react";
import AlertModal from "../Elements/AlertModal";
import {
  deleteNoticeContact,
  getAllNoticesContact,
  updateNoticeContact,
} from "@/redux/notice-contact";
import ViewUpdateContact from "../Memorials/MemorialAbout/ViewUpdateContact";
import { getLabel } from "@/utils/lang-manager";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
const NoticesContactCard = (props: any) => {
  const [showAddContact, setShowAddContact] = useState(false);
  const dispatch = useAppDispatch();
  const router = useRouter();
  const { id } = router.query;
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const { NoticeData } = useContext(NoticeDataContext);
  const initialLanguage = NoticeData?.initialLanguage;
  const deleteContact = (uuid: any) => {
    setIsDisabled(true);

    setShowAddContact(false);
    dispatch(
      deleteNoticeContact({
        activePartner: ACTIVE_PARTNER,
        activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
        activePageId: id as unknown as string,
        contactId: uuid,
      })
    ).then((res: any) => {
      if (res?.payload?.success === true) {
        setIsDisabled(false);
        Notification.success({
          title: "Contact has been deleted successfully",
          duration: 3000,
          content: undefined,
        });
        handleload();
        // dispatch(
        //   getAllNoticesContact({
        //     active_partner: ACTIVE_PARTNER,
        //     active_service_provider: ACTIVE_SERVICE_PROVIDER,
        //     active_notice: id as unknown as string,
        //     page: localStorage.getItem("notices_contact_page")
        //       ? parseInt(
        //           localStorage.getItem("notices_contact_page") as string,
        //           10
        //         )
        //       : 1,
        //   })
        // );
        setShowDeleteModal(false);
      } else {
        setIsDisabled(false);
        Notification.error({
          title: "Something went wrong",
          duration: 3000,
          content: undefined,
        });
      }
    });
  };
  console.log(props);
  const { data, handleload } = props;
  const [response, setResponse] = useState("");
  const actionBtnRef = useRef<HTMLButtonElement>(null);
  const handleUpdate = (
    personName: string,
    countryId: string,
    relationship: number,
    visibilityId: number,
    auto_hide: boolean,
    contactTypes: any
  ) => {
    console.log("contactTypes", contactTypes);
    setSubmitButtonDisabled(true);

    updateContact(
      personName,
      countryId,
      relationship,
      visibilityId,
      auto_hide,
      contactTypes
    );
  };

  const updateContact = (
    personName: any,
    countryId: any,
    relationship: any,
    visibilityId: any,
    hideAfterFuneral: any,
    contactTypes: any
  ) => {
    const updatedData = {
      name: personName,
      country_id: countryId,
      relationship_id: relationship,
      contact_details: contactTypes,
      // relationship: relationship,
      visibility_id: visibilityId,
      auto_hide: hideAfterFuneral,
    };
    console.log("888***", updatedData);

    dispatch(
      updateNoticeContact({
        activePartner: ACTIVE_PARTNER,
        activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
        activeNotice: id as unknown as string,
        uuid: props?.data?.uuid,
        updatedData,
      })
    ).then((response: any) => {
      if (response.payload.success == true) {
        setResponse(response);
        handleload();
        setSubmitButtonDisabled(false);
        Notification.success({
          title: "Contact has been updated successfully",
          duration: 3000,
          content: undefined,
        });
      } else {
        setSubmitButtonDisabled(false);
        Notification.error({
          title: "Faild to create contact",
          duration: 3000,
          content: undefined,
        });
      }
    });
  };
  const [isDisabled, setIsDisabled] = useState(false);
  const [submitButtonDisabled, setSubmitButtonDisabled] = useState(false);
  return (
    useEffect(() => {
      // console.log("fdata-->", props?.data);
    }, []),
    (
      <>
        <AlertModal
          show={showDeleteModal}
          isSmallModal={true}
          position="centered"
          title="Confirmation"
          isNotiesModal={true}
          notiesId={props?.data?.uuid}
          onClose={() => {
            setShowDeleteModal(false);
          }}
        >
          <div className="mb-4 d-flex conform-color font-100">
            Do you want to delete it?
            {/* <span
              className="text-truncate ps-1 font-semibold"
              style={{
                maxWidth: "100px",
              }}
            >
              {props?.data?.name}
            </span>
            ? */}
          </div>

          {/* <p className="mb-4 text-truncate">Do you want to delete <span className="text-truncate" style={{ maxWidth: "30px" }}>{props?.data?.name}</span>?</p> */}
          <div className="col-auto">
            <button
              className="btn btn-system-primary me-3"
              onClick={() => setShowDeleteModal(false)}
            >
              No
            </button>
            <span
              className="yes-delete-it pointer font-120"
              onClick={() => {
                isDisabled ? null : deleteContact(props?.data?.uuid);
              }}
            >
              Yes, Delete it
            </span>
          </div>
        </AlertModal>
        <FullScreenModal
          show={showAddContact}
          onClose={() => {
            setShowAddContact(false);
          }}
          title="Show contact"
          description="This will be  visible in the Notices page"
          footer={
            // <a
            //   onClick={() => {
            //     setShowAddContact(false);
            //     setShowDeleteModal(true);
            //   }}
            // >
            //   <div className="text-danger pointer">Delete the contact</div>
            // </a>
            <div style={{ display: "flex" }}>
              <Button
                ref={actionBtnRef}
                className="btn-system-primary"
                disabled={submitButtonDisabled}
              >
                Submit
              </Button>
              <a
                onClick={() => {
                  setShowAddContact(false);
                  setShowDeleteModal(true);
                }}
                style={{ marginLeft: "25px", marginTop: "7px" }}
              >
                <div className="text-danger pointer">
                  {getLabel("DeleteTheContact", initialLanguage)}
                </div>
              </a>
            </div>
          }
        >
          <ViewUpdateContact
            actionBtnRef={actionBtnRef}
            handleUpdate={handleUpdate}
            response={response}
            name={props?.data?.name}
            country={props?.data?.country?.id}
            relationshipId={props?.data?.relationship?.id}
            visibilityid={props?.data?.visibility.id}
            hideAfterFuneralget={props?.data?.auto_hids}
            contactNameValue={props?.data?.contact_details}
          />
        </FullScreenModal>
        <div
          className={`card border${
            props.index === props.noData - 1 ? "" : " mb-3"
          }`}
        >
          <div className=" card-body mx-3 my-2 ps-2 p-0">
            <div className="row ">
              <div className=" col-7 m-0 p-0 ">
                <div className="d-flex align-items-start gap-2">
                  <div className="">
                    <Image src={draggable} alt="memorialprofile" className="" />
                  </div>

                  <p className="m-0 text-muted-dark text-truncate">
                    <strong className=" font-bold ">{props?.data?.name}</strong>
                    <br />
                    <span className="font-80 font-normal text-truncate">
                      {props?.data?.country?.name}
                    </span>
                  </p>
                </div>
              </div>
              <div className="col-5 d-flex justify-content-end align-items-center pe-2">
                <button
                  type="button"
                  className="btn btn-sm border system-view-secondary py-1 px-4 font-90 font-semibold"
                  onClick={() => {
                    setShowAddContact(true);
                  }}
                >
                  View
                </button>
              </div>
            </div>
          </div>
        </div>
      </>
    )
  );
};

export default NoticesContactCard;
