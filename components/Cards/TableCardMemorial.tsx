import React, { useEffect, useState } from "react";
import Image from "next/image";
import extractDate from "@/components/TimeConverter/dateExtracter";
import { MonthDate } from "../TimeConverter/MonthFormat";
import CustomImage from "../Elements/CustomImage";
import { getLabel } from "@/utils/lang-manager";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";

type Props = {
  data: any;
  singleView: (uuid: any) => void;
  initialLanguage: string
};

const TableCardMemorial = ({ data, singleView, initialLanguage }: Props) => {

  return (
    <div className="card border bg-white mb-3">
      <div className="card-body">
        <div className="row m-0">
          <div className="col-2  p-0">
            <div className=" d-flex justify-content-center">
              <div className=" table-img text-muted font-140 system-text-dark-light d-flex align-items-center justify-content-center rounded-circle">
                {data && data.photo_url ? (
                  <CustomImage
                    src={data.photo_url}
                    alt=""
                    className="image rounded-circle"
                    width={200}
                    height={200}
                    divClass="table-img"
                  />
                ) : data && data.name ? (
                  data?.name[0]
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
          <div className="col-10 ps-3">
            <div className="mb-2">

              <div className=" p-0 font-110 font-semibold ">
                <div className="d-flex">
                  <p className="text-truncate p-0 m-0">
                    {data?.name ? data?.name : ""}
                  </p>

                </div>
              </div>
              <div className=" p-0 text-dark font-90  text-truncate">
                {data?.updated_at ? extractDate(data?.updated_at) : ""}
              </div>
              {/* <div className=" p-0 text-dark font-90  text-truncate">
                {data?.email ? data?.email : ""}
              </div> */}
            </div>
            {data && (
              <div className="row m-0">
                <div className="col-6 p-0 text-secondary font-80 font-thin d-flex align-items-center  justify-content-start ">
                  <p className="p-0 m-0">{getLabel("country", initialLanguage)}</p>
                </div>
                <div className="col-6 p-0 text-dark font-90 d-flex align-items-center  justify-content-start ">
                  <p className="text-truncate p-0 m-0">
                    {data?.death_location ? data?.death_location : "Unknown"}
                  </p>
                </div>
              </div>
            )}
            {data && (
              <div className="row m-0">
                <div className="col-6 p-0 text-secondary font-80 font-thin">
                  {getLabel("deathDate", initialLanguage)}
                </div>
                <div className=" col-6 p-0 text-dark font-90 text-truncate">
                  {data?.dod ? MonthDate(data?.dod) : "Unknown"}
                </div>
              </div>
            )}
            {data && (
              <div className="row m-0 ">
                <div className="col-6 p-0 text-secondary font-80 font-thin ">
                  {getLabel("status", initialLanguage)}
                </div>
                <div className="col-6 p-0  text-dark font-90 text-truncate">
                  {data?.state.name == "Enabled" ? (
                    <span className="text-success"> {data?.state.name}</span>
                  ) : (
                    <span className="text-danger"> {data?.state.name}</span>
                  )}
                </div>
              </div>
            )}
            <div>
              <button
                onClick={() => {
                  singleView(data.uuid);
                }}
                type="button"
                className="btn btn-system-light col-12 pointer mt-2"
              >
                View
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TableCardMemorial;
