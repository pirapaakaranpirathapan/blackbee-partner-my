import React, { useEffect, useState } from "react";
import Image from "next/image";
import extractDate from "@/components/TimeConverter/dateExtracter";
import { timeagoformatter } from "../TimeConverter/timeAgoFormatter";
import { MonthDate } from "../TimeConverter/MonthFormat";
import CustomImage from "../Elements/CustomImage";
import { getLabel } from "@/utils/lang-manager";


type Props = {
  data: any;
  singleView: (uuid: any, isGroup: any) => void;
  initialLanguage: string

};

const TableCardNotice = ({ data, singleView, initialLanguage }: Props) => {
  return (
    <div className="card border bg-white mb-3">
      <div className="card-body">
        <div className="row m-0">
          <div className="col-2  p-0">
            <div className=" d-flex justify-content-center">
              <div className=" table-img text-muted font-140 system-text-dark-light d-flex align-items-center justify-content-center rounded-circle position-relative">
                {data && data?.pages[0]?.photo_url ? (
                  <CustomImage
                    src={data?.pages[0]?.photo_url}
                    alt=""
                    className="image rounded-circle object-fit-cover"
                    width={400}
                    height={400} divClass={"table-img text-muted font-140 system-text-dark-light d-flex align-items-center justify-content-center rounded-circle position-relative"} />
                ) : data && data?.pages.length > 0 ? (
                  data?.pages[0].name[0]
                ) : (
                  ""
                )}
                {Array.isArray(data?.pages) && data?.pages.length > 1 ? (
                  <div className=" position-absolute translate-middle  no-circle d-flex align-items-center justify-content-center font-80 ">
                    +{data.pages.length - 1}
                  </div>
                ) : (
                  <></>
                )}
              </div>
            </div>
          </div>

          <div className="col-10 ps-3">
            <div className="mb-2">
              <div className="  ">
                <p className="p-0 m-0 text-dark font-90 text-truncate">
                  {Array.isArray(data.pages) && data.pages.length > 1
                    ? `${getLabel("obituaryForGroupOfPeople", initialLanguage)}`
                    : ""}
                </p>
              </div>
              <div className=" p-0 font-110 font-semibold ">
                <div className="d-flex">
                  <p className="text-truncate p-0 m-0">
                    {data?.pages.length > 0 ? data?.pages[0].name : ""}
                  </p>
                  {Array.isArray(data.pages) && data.pages.length > 1 ? (
                    <>
                      <span className="px-1">&</span>
                      <span className="px-1">{data.pages.length - 1}</span>
                      {data.pages.length - 1 > 1 ? "others" : "other"}
                    </>
                  ) : (
                    ""
                  )}
                </div>
              </div>
              {data ? timeagoformatter(data?.created_at) : ""}
              {/* <div className=" p-0 text-dark font-90  text-truncate">
                {data?.email ? data?.email : ""}
              </div> */}
            </div>
            {data && (
              <div className="row m-0">
                <div className="col-6 p-0 text-secondary font-80 font-thin d-flex align-items-center  justify-content-start ">
                  <p className="p-0 m-0">{getLabel("country", initialLanguage)}</p>
                </div>
                <div className="col-6 p-0 text-dark font-90 d-flex align-items-center  justify-content-start ">
                  <p className="text-truncate p-0 m-0">
                    {data?.country ? data.country?.name : "-"}
                  </p>
                </div>
              </div>
            )}
            {data?.pages && (
              <div className="row m-0">
                <div className="col-6 p-0 text-secondary font-80 font-thin">
                  {getLabel("deathDate", initialLanguage)}
                </div>
                <div className=" col-6 p-0 text-dark font-90 text-truncate">
                  {data?.pages ? MonthDate(extractDate(data?.pages[0.]?.dod)) : ""}
                </div>
              </div>
            )}
            {data?.state && (
              <div className="row m-0 ">
                <div className="col-6 p-0 text-secondary font-80 font-thin ">
                  {getLabel("status", initialLanguage)}
                </div>
                <div className="col-6 p-0  text-dark font-90 text-truncate">
                  {data?.state.name == "Enabled" ? (
                    <span className="text-success"> {data?.state.name}</span>
                  ) : (
                    <span className="text-danger"> {data?.state.name}</span>
                  )}
                </div>
              </div>
            )}
            <div>
              <button
                onClick={() => {
                  singleView(data.uuid, data.pages.length > 1);
                }}
                type="button"
                className="btn btn-system-light col-12 pointer mt-2"
              >
                {getLabel("view", initialLanguage)}
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TableCardNotice;
