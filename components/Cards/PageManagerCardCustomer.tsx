import React, { useContext, useEffect, useRef, useState } from "react";
import Image from "next/image";
import memorialImage from "../../public/banner_profile.jpg";
import AlertModal from "../Elements/AlertModal";
import { useRouter } from "next/router";
import ArcoDropdown from "../Elements/ArcoDropdown";
import { customerPageSelector, getAllCustomerforpage } from "@/redux/customer";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import { Notification } from "@arco-design/web-react";
import { memorialPageSelector, patchMemorialClient } from "@/redux/memorial-pages";
import { getLabel } from "@/utils/lang-manager";
import ArcoSelectAndCreateSingle from "../Arco/ArcoSelectAndCreateSingle";
import CustomerDetailsModal, {
  ChildModalRef,
} from "../Modal/createCustomerModal";
import { Button } from "react-bootstrap";
import FullScreenModal from "../Elements/FullScreenModal";
import { useApiErrorHandling } from "../Others/useApiErrorHandling";
import ChangeCustomer from "./ChangeCustomer";
import CustomImage from "../Elements/CustomImage";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";

const PageManagerCardCustomer = ({ data, handleSave }: any) => {
  const [changeCustomer, setChangeCustomer] = useState(false);
  const [btnDisabled, setBtnDisabled] = useState(false);
  const [customerId, setCustomerId] = useState("");
  const [customerDatas, setCustomerDatas] = useState<string[]>([]);
  const [ShowProfileBanner, setShowProfileBanner] = useState(false);
  const [defaultCustomerNames, setDefaultCustomerNames] = useState<
    string | undefined
  >();
  const [customerErr, setCustomerErr] = useState<string>("");
  const { PageData } = useContext(PageDataContext);
  const initialLanguage = PageData?.initialLanguage

  // const [addCusSearchValue, setAddCusSearchValue] = useState('');
  const [addCusSearchValue, setAddCusSearchValue] = useState({
    inputValue: "",
    searchType: "",
  });

  const [response, setResponse] = useState("");
  const { data: allCustomerData } = useAppSelector(customerPageSelector);

  useEffect(() => {
    if (changeCustomer) {
      setCustomerId("")
      setCustomerErr("");
      setDefaultCustomerNames(undefined)
    }
  }, [changeCustomer])


  useEffect(() => {
    setCustomerDatas(allCustomerData);
  }, [allCustomerData])

  // const { error: allCustomerData } = useAppSelector(customerPageSelector);
  const { error: customerChangeError } = useAppSelector(
    memorialPageSelector
  );

  const Err = useApiErrorHandling(
    customerChangeError,
    response
  );

  const router = useRouter();
  const uuid = router?.query?.id;
  const childModalRef = useRef<ChildModalRef>(null);

  const dispatch = useAppDispatch();
  const HandleEditCustomer = (uuid: any) => {
    console.log("uuid>>", uuid);
    router.push(`/customers/${uuid}`);
  };

  // const handleSelectCustomerChange = (selectedValues: string[]) => {
  //   const selectCustomerId = selectedValues ? selectedValues[0] : "";
  //   setCustomerId(selectCustomerId);
  // };
  const handleSelectCustomerChange = (selectedValues: string) => {
    const selectCustomerId = selectedValues;
    // const selectCustomerId = selectedValues ? selectedValues[0] : "";
    setCustomerId(selectCustomerId);
  };

  const clientUuid = data?.id;
  // useEffect(() => {
  //   if (allCustomerData) {
  //     let customerData =
  //       Array.isArray(allCustomerData) &&
  //       allCustomerData.find((x: any) => x.id.toString() == clientUuid);
  //     customerData && setCustomerId(customerData.id);
  //     setCustomerDatas([customerData])
  //   }
  // }, [clientUuid, allCustomerData]);

  const handleUpdateCustomer = () => {
    setBtnDisabled(true);
    const updatedData = {
      client_id: customerId,
    };

    dispatch(
      patchMemorialClient({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_id: uuid as string,
        updatedData,
      })
    ).then((response: any) => {
      setResponse(response);
      // handleSave();

      if (response.payload.success == true) {
        setBtnDisabled(false);
        Notification.success({
          title: "Client has been updated Successfully",
          duration: 3000,
          content: undefined,
        });
        handleSave();

        setChangeCustomer(false);

      } else {
        setBtnDisabled(false);
(response.payload.code != 422) &&   Notification.error({
          title: "Client Update Failed",
          duration: 3000,
          content: undefined,
        });
        setChangeCustomer(true);

      }
    });
  };

  const onCallCustomerContinue = (value: any) => {
    console.log("value", value);
    // setCustomerId(value);
    setShowProfileBanner(false);
    // const selectedCustomer =allCustomerData?allCustomerData.find(
    //   (option: { id: number }) => option.id === parseInt(value)
    // ) :"not found";
    // console.log(selectedCustomer,allCustomerData);

    // const selectedUuid = selectedCustomer?.uuid || "";
  };
  const handleClick = async () => {
    // setsubmitButtonAddCustomerEnabled(true);
    if (childModalRef.current) {
      childModalRef.current.callModalFunction(handleChildResponse);
    }
  };

  const handleChildResponse = (response: any) => {
    const customerResponseData = response;
    setDefaultCustomerNames(response?.first_name);
    setCustomerDatas(customerResponseData);
    setCustomerId(response?.id);
    setShowProfileBanner(false);
  };
  const updateAddSearchValue = (value: any) => {
    // setAddCusSearchValue(value);
    let searchType = "Name";
    setAddCusSearchValue({ inputValue: value, searchType: searchType });
  };



  return (
    <>
      <FullScreenModal
        show={ShowProfileBanner}
        onClose={() => {
          setShowProfileBanner(false);
          setCustomerId('')
        }}
        title={`${getLabel("addNewCustomer", initialLanguage)}`}
        description={`${getLabel("addNewCustomerModalDescription", initialLanguage)}`}
        footer={
          <Button
            className="btn-system-primary pointer"
            onClick={handleClick}
          // disabled={submitButtonAddCustomerEnabled}
          >
            {getLabel("create", initialLanguage)}
          </Button>
        }
        deleteLabel={
          <a
            onClick={() => {
              setShowProfileBanner(false);
            }}
          >
            {getLabel("close", initialLanguage)}
          </a>
        }
      >
        <CustomerDetailsModal
          ref={childModalRef}
          onCallCustomerContinue={onCallCustomerContinue}
          addCusSearchValue={addCusSearchValue}
          setModalVisible={setShowProfileBanner}
        />
      </FullScreenModal>
      <AlertModal
        show={changeCustomer}
        onClose={() => {
          setChangeCustomer(false);
        }}
        title={`${getLabel("changeCustomer", initialLanguage)}`}
        description={`${getLabel("changeCustomerDescription", initialLanguage)}`}
        position="centered"
      >
        <>
          <ChangeCustomer
            allCustomerData={customerDatas}
            handleSelectCustomerChange={handleSelectCustomerChange}
            setShowProfileBanner={setShowProfileBanner}
            ShowProfileBanner={ShowProfileBanner}
            defaultCustomerNames={defaultCustomerNames}
            updateAddSearchValue={updateAddSearchValue}
            customerErr={undefined}
            response={response}
            handleUpdateCustomer={handleUpdateCustomer}
            // btnDisabled={btnDisabled}
            changeCustomer={changeCustomer}
          />
          {/* <div className="row">
            <div className="col-12 font-semibold mb-0">
              {getLabel("customer",initialLanguage)}
            </div>
            <div className="col-12 mb-3">
              <ArcoSelectAndCreateSingle
                data={allCustomerData}
                placeholderValue={"Search a person"}
                onOptionChange={handleSelectCustomerChange}
                is_search={true}
                setIsModalVisible={setShowProfileBanner}
                isVisible={ShowProfileBanner}
                // onClear={handleClear}
                // onDeselect={handleDeselect}
                defaultValue={defaultCustomerNames}
                updateAddSearchValue={updateAddSearchValue}
              />
              <div className="text-danger">
                {Err?.client_id}
              </div>
            </div>
            <div className="col-auto ">
              <button
                className="btn btn-system-primary"
                onClick={() => {
                  handleUpdateCustomer();
                }}
                disabled={btnDisabled}
              >
                {getLabel("change",initialLanguage)}
              </button>
            </div>
          </div> */}
        </>
      </AlertModal>
      <div className="mb-2">
        <div className="card border-white system-secondary-light-thin">
          <div className="card-body">
            {/* ---------------------------------------- */}
            <div className="row m-0">
              <div className="col-12  col-sm-9 p-0 d-sm-flex align-items-center justify-content-center justify-content-sm-start gap-3">
                <div className="d-flex justify-content-center justify-content-sm-start">
                  <div>
                    <div className="page-manager-container text-muted font-140 system-text-dark-light d-flex align-items-center justify-content-center rounded-circle">
                      {/* <Image
                        src={memorialImage}
                        alt="memorialprofile"
                        className="rounded-circle image"
                      /> */}

                      {data && data.photo_url ? (
                        <CustomImage
                          src={data.photo_url}
                          alt=""
                          className="image rounded-circle"
                          width={200}
                          height={200}
                          divClass="page-manager-container"
                        />
                      ) : data && data.first_name ? (
                        <div className="rounded-circle">
                          {`${data.first_name[0]}${data.last_name ? data.last_name[0] : ''}`}
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                </div>
                {/* ------------------ */}
                <div
                  className="font-90 font-bold d-flex d-sm-block justify-content-center justify-content-sm-start mt-1 mt-sm-0"
                  style={{ maxWidth: "100%" }}
                >
                  <div className="w-100" style={{ maxWidth: "100%" }}>
                    <div className="font-90 font-bold text-center text-sm-start text-truncate pe-0 pe-sm-5">
                      {data?.first_name}
                      <span className="mx-1">{data?.last_name}</span>
                    </div>
                    <div className="font-80 font-normal text-secondary text-center text-sm-start text-truncate">
                      {data?.country?.name}
                    </div>
                    <div className="d-sm-flex  font-90 fw-normal  text-truncate">
                      <div className="text-truncate text-center text-sm-start">
                        {data?.email}
                      </div>
                      <div className="d-none d-sm-flex align-items-center px-1"> <div className="dot"></div> </div>
                      <div className="text-center text-sm-start">
                        {data?.mobile}
                      </div>
                      {data?.alternative_phone ? (
                        <>
                          <div className="d-none d-sm-flex align-items-center px-1"> <div className="dot"></div> </div>
                          <div className="text-center text-sm-start">
                            {data?.alternative_phone}
                          </div>
                        </>
                      ) : ""}

                    </div>
                  </div>
                </div>
              </div>
              {/* ------------------ */}
              <div className="col-12 col-sm-3 p-0 mt-1 mt-sm-0 d-flex gap-2 justify-content-center justify-content-sm-end">
                <div className="text-secondary font-80 font-normal pointer">
                  <u onClick={() => HandleEditCustomer(data?.uuid)}>
                    {getLabel("edit", initialLanguage)}
                  </u>
                </div>
                <div
                  className="text-secondary font-80 font-normal pointer"
                  onClick={() => {
                    setChangeCustomer(true);
                  }}
                >
                  <u className="pointer">{getLabel("change", initialLanguage)}</u>
                </div>
              </div>
            </div>
            {/* ---------------------------------------- */}
          </div>
        </div>
      </div>
      {/* ))} */}
    </>
  );
};

export default PageManagerCardCustomer;

{
  /* <div className="col-12 col-sm-9 p-0 bg-danger d-sm-flex gap-3 align-items-center justify-content-center justify-content-sm-start">
                <div className="d-flex flex-column align-items-center gap-3 ">
                  <div className="page-manager-container d-flex align-items-center justify-content-center rounded-circle text-muted font-140 system-text-dark-light">
                    <Image
                      src={memorialprofile}
                      alt="memorialprofile"
                      className="rounded-circle image"
                    />
                  </div>
                  <div className="bg-info">
                    <div className="font-90 font-bold text-truncate-550">
                      David William Ronaldo
                    </div>
                    <div className="font-80 font-normal text-secondary text-truncate-100">
                      United Kingdom
                    </div>
                    <div className="d-flex gap-0  gap-sm-2 font-90 fw-normal bg-primary">
                      <div className="text-truncate-550">
                        david.william@migft.com
                      </div>
                      <div className="d-none d-sm-flex"> . </div>
                      <div className="text-truncate-550">
                        +44 7858 3322193{' '}
                      </div>
                      <div className=" d-none d-sm-flex"> . </div>
                      <div className="text-truncate-550">+44 208 1339087</div>
                    </div>
                  </div>
                </div>
              </div> */
}
{
  /* <div className="col-12 col-sm-3 p-0 d-flex justify-content-center justify-content-sm-end bg-warning">
                <div className="text-secondary font-80 font-normal">
                  <u>Edit</u>
                </div>
                <div
                  className="text-secondary font-80 font-normal ps-2"
                  onClick={() => {
                    setChangeCustomer(true);
                  }}
                >
                  <u>Change</u>
                </div>
              </div> */
}
