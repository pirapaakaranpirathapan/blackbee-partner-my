import React, { useEffect, useState } from "react";
import Image from "next/image";
import AlertModal from "../Elements/AlertModal";
import { DeletePageManagers, getAllPageManagers, pageManagerSelector } from "@/redux/page-manager";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import Loader from "../Loader/Loader";
import CustomerSkelton from "../Skelton/CustomerSkelton";
import PageManagerSkelton from "../Skelton/PageManagerSkelton";
import memorialImage from "../../public/banner_profile.jpg";
import { Notification } from "@arco-design/web-react";
import CustomImage from "../Elements/CustomImage";

const PageManagerCard = ({ data, loading, activePage }: any) => {
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [deleteItemUuid, setDeleteItemUuid] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useAppDispatch();

  const deletePageManager = (uuid: any, first_name: any, last_name: any) => {
    setDeleteItemUuid(uuid);
    setShowDeleteModal(true);
    setFirstName(first_name)
    setLastName(last_name)
  };
  const confirmDelete = () => {
    try {
      dispatch(
        DeletePageManagers({
          active_partner: ACTIVE_PARTNER,
          active_service_provider: ACTIVE_SERVICE_PROVIDER,
          active_page: activePage as unknown as string,
          active_page_manager: deleteItemUuid as unknown as string,
        })
      ).then((response: any) => {
        if (response?.payload?.success == true) {
          getAllManagers();
          Notification.success({
            title: "Removed Successfully",
            duration: 3000,
            content: undefined,
          });
          console.log("hjl", response);
        } else {
          Notification.error({
            title: "Failed",
            duration: 3000,
            content: undefined,
          });
        }
      });
      // Close the delete modal
      setShowDeleteModal(false);
      setDeleteItemUuid("");
    } catch (error) {
      // Handle error if necessary
      console.error("Error deleting:", error);
    } finally {
      setIsLoading(false);
    }
  };


  const getAllManagers = async () => {
    setIsLoading(true);
    await dispatch(
      getAllPageManagers({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_page: activePage as unknown as string,
      })
    ).then((response: any) => {
      setIsLoading(false);
    });
  };

  const [count, setCount] = useState(3);

  useEffect(() => {
    getAllManagers();
  }, []);

  const { data: pageManagerData } = useAppSelector(pageManagerSelector);

  return (
    <>
      <AlertModal
        show={showDeleteModal}
        isSmallModal={true}
        position="centered"
        title="Confirmation"
        // description="dddddddddddddd"
        onClose={() => setShowDeleteModal(false)}
      >
        <p className="mb-4 ">Are you sure you want to delete <span className="font-semibold">{firstName} {lastName}</span>?</p>
        <div className="col-auto">
          <button
            className="btn btn-system-primary me-3"
            onClick={() => setShowDeleteModal(false)}
          >
            No
          </button>
          <span className="text-danger pointer" onClick={confirmDelete}>
            Yes, Delete it
          </span>
        </div>
      </AlertModal>

      {isLoading ? (
        <PageManagerSkelton loopTimes={count} />
      ) : (
        pageManagerData &&
        Array.isArray(pageManagerData) &&
        pageManagerData?.map((item: any, index: any) => (
          <div className="mb-2" key={index}>
            <div className="card border-white system-secondary-light-thin">
              <div className="card-body">
                <div className="row m-0 w-100">
                  <div className="col-12 col-sm-9 p-0 d-sm-flex align-items-center justify-content-center justify-content-sm-start gap-3">
                    <div className="page-manager-container text-muted font-140 system-text-dark-light d-flex align-items-center justify-content-center rounded-circle">
                      {item && item.profile_image_url ? (
                        <CustomImage
                          src={item.profile_image_url}
                          alt=""
                          className="image rounded-circle"
                          width={200}
                          height={200}
                          divClass="page-manager-container"
                        />
                      ) : item && item.first_name ? (
                        <div className="rounded-circle">
                          {`${item.first_name[0]}${item.last_name ? item.last_name[0] : ''}`}
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                    <div className="font-90 font-bold d-flex d-sm-block justify-content-center justify-content-sm-start mt-1 mt-sm-0">
                      <div>
                        {item.status !== "pending" ? (
                          <div className="text-truncate">{`${item.first_name} ${item.last_name}`}</div>
                        ) : (
                          <div className="text-secondary text-center text-truncate">{`${item.first_name} ${item.last_name}`}</div>
                        )}
                        <div className="font-80 font-normal text-secondary text-center text-sm-start">
                          {item.status && item.status !== "pending"
                            ? item.user_type
                            : "Invitation sent to this email"}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-12 col-sm-3 p-0 mt-1 mt-sm-0">
                    <div
                      className="text-secondary font-80 font-normal d-flex justify-content-center justify-content-sm-end pointer"
                      onClick={() => deletePageManager(item.uuid, item.first_name, item.last_name)}
                    >
                      <u>Remove</u>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))
      )}
    </>
  );
};

export default PageManagerCard;
