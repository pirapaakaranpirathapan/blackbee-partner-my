import React, { useContext, useEffect, useState } from "react";
import { getLabel } from "@/utils/lang-manager";
import ArcoSelectAndCreateSingle from "../Arco/ArcoSelectAndCreateSingle";
import { useApiErrorHandling } from "../Others/useApiErrorHandling";
import { memorialPageSelector } from "@/redux/memorial-pages";
import { useAppSelector } from "@/redux/store/hooks";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";

interface ChangeCustomerProps {
  allCustomerData: any /* Define the type for allCustomerData */;
  handleSelectCustomerChange: any /* Define the type for handleSelectCustomerChange */;
  setShowProfileBanner: (ShowProfileBanner: boolean) => void;
  ShowProfileBanner: boolean /* Define the type for ShowProfileBanner */;
  defaultCustomerNames: any /* Define the type for defaultCustomerNames */;
  updateAddSearchValue: any /* Define the type for updateAddSearchValue */;
  customerErr: any /* Define the type for Err */;
  response: any;
  handleUpdateCustomer: any;
  // btnDisabled: any;
  changeCustomer: any;
}

const ChangeCustomer: React.FC<ChangeCustomerProps> = ({
  allCustomerData,
  handleSelectCustomerChange,
  setShowProfileBanner,
  ShowProfileBanner,
  defaultCustomerNames,
  updateAddSearchValue,
  customerErr,
  response,
  handleUpdateCustomer,
  // btnDisabled,
  changeCustomer,
}) => {
  const { NoticeData } = useContext(NoticeDataContext);
  const { PageData } = useContext(PageDataContext);
  const initialLanguage = NoticeData?.initialLanguage || PageData?.initialLanguage;
  const { error: customerChangeError } = useAppSelector(memorialPageSelector);
  const Err = useApiErrorHandling(customerChangeError, response);
  const [submitButtonDisabled, setSubmitButtonDisabled] = useState(false);

  console.log("defaultCustomerNames", defaultCustomerNames);

  return (
    <>
      <div className="row">
        <div className="col-12 font-semibold mb-0">
          {getLabel("customer", initialLanguage)}
        </div>
        <div className="col-12 mb-3">
          <ArcoSelectAndCreateSingle
            data={allCustomerData}
            placeholderValue={
              "Select from your customer database or create as new customer"
            }
            onOptionChange={handleSelectCustomerChange}
            is_search={true}
            setIsModalVisible={setShowProfileBanner}
            isVisible={ShowProfileBanner}
            // onClear={handleClear}
            // onDeselect={handleDeselect}
            defaultValue={defaultCustomerNames}
            updateAddSearchValue={updateAddSearchValue}
          />
          <div className="text-danger">
            {Err?.client_id ? Err?.client_id : customerErr}
          </div>
        </div>
        <div className="col-auto ">
          <button
            className="btn btn-system-primary"
            onClick={() => {
              handleUpdateCustomer();
              setSubmitButtonDisabled(true);
              setTimeout(() => {
                setSubmitButtonDisabled(false);
              }, 1000);
            }}
            disabled={submitButtonDisabled}
          >
            {getLabel("change", initialLanguage)}
          </button>
        </div>
      </div>
    </>
  );
};

export default ChangeCustomer;
