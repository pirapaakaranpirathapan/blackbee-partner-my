import React, { useEffect, useState } from "react";
import Image from "next/image";
import { getLabel } from "@/utils/lang-manager";
import Avatar from "../../public/avatar.jpg";
import FullScreenModal from "../Elements/FullScreenModal";
type Props = {
  data: any;
  singleView: (uuid: any) => void;
  showData: any;
  modelView: boolean;
  initialLanguage: string
};

const TableCardTwo = ({ data, singleView, showData, modelView, initialLanguage }: Props) => {
  const [showInfo, setShowInfo] = useState(false);
  <FullScreenModal
    show={modelView}
    onClose={() => {
      setShowInfo(false);
    }}
    title="Show partner admin"
  >
    <div className="row align-items-center m-0 ">
      <div className="row m-0 p-0 mb-3">
        <div className="col-md-12 col-12 p-0">
          <div className="font-semibold">Status</div>
        </div>
        <div className="col-md-12 col-12  p-0">
          <input
            type="text"
            className="form-control border border-2 p-2 system-control "
            placeholder=""
            disabled
            value={showData.status}
          />
        </div>
      </div>
      <div className="row m-0 p-0 mb-3">
        <div className="col-md-12 col-12 p-0">
          <div className="font-semibold">First Name</div>
        </div>
        <div className="col-md-12 col-12  p-0">
          <input
            type="text"
            className="form-control border border-2 p-2 system-control "
            placeholder=""
            disabled
            value={showData.first_name}
          />
        </div>
      </div>
      <div className="row m-0 p-0 mb-3">
        <div className="col-md-12 col-12 p-0">
          <div className="font-semibold">Last Name</div>
        </div>
        <div className="col-md-12 col-12  p-0">
          <input
            type="text"
            className="form-control border border-2 p-2 system-control "
            placeholder=""
            disabled
            value={showData.last_name}
          />
        </div>
      </div>
      <div className="row m-0 p-0 mb-3">
        <div className="col-md-12 col-12 p-0">
          <div className="font-semibold">Email</div>
        </div>
        <div className="col-md-12 col-12  p-0">
          <input
            type="text"
            className="form-control border border-2 p-2 system-control "
            placeholder=""
            disabled
            value={showData.email}
          />
        </div>
      </div>
    </div>
  </FullScreenModal>;
  return (
    <div className="card border bg-white mb-3">
      <div className="card-body">
        <div className="row m-0">
          <div className="col-2  p-0">
            <div className=" d-flex justify-content-center">
              <div className=" table-img text-muted font-140 system-text-dark-light d-flex align-items-center justify-content-center rounded-circle">
                {data && data.profile ? (
                  <Image src={Avatar} alt="" className="image rounded-circle" />
                ) : data && data?.["first_name"] ? (
                  data?.["first_name"][0]
                ) : data && data.name ? (
                  data?.name[0]
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
          <div className="col-10 ps-3">
            <div className="mb-2">
              <div className=" p-0 font-110 font-semibold ">
                <div className="d-flex">
                  <p className="text-truncate p-0 m-0">
                    {data?.["first_name"] && data?.["first_name"]
                      ? data?.["first_name"]
                      : ""}{" "}
                    {data?.["last_name"] && data?.["last_name"]
                      ? data?.["last_name"]
                      : "unknown"}
                  </p>
                </div>
              </div>
              {/* <div className=" p-0 text-dark font-90  text-truncate">
                {data?.publishedTime ? data?.publishedTime : 'unknown'}
              </div> */}
              <div className=" p-0 text-dark font-90  text-truncate">
                {data?.email ? data?.email : ""}
              </div>
            </div>
            {/* {data && (
              <div className="row m-0">
                <div className="col-6 p-0 text-secondary font-80 font-thin d-flex align-items-center  justify-content-start ">
                  <p className="p-0 m-0">Country</p>
                </div>
                <div className="col-6 p-0 text-dark font-90 d-flex align-items-center  justify-content-start ">
                  <p className="text-truncate p-0 m-0">
                    {data?.country.name ? data?.country.name : ""}
                  </p>
                </div>
              </div>
            )} */}
            {/* {data && (
              <div className="row m-0 ">
                <div className="col-6 p-0 text-secondary font-80 font-thin ">
                  Status
                </div>
                <div className="col-6 p-0  text-dark font-90 text-truncate">
                  {data?.status !== false ? (
                    <span className="text-success">Published</span>
                  ) : (
                    <span className="text-danger">Pending</span>
                  )}
                </div>
              </div>
            )} */}
            <div>
              <button
                onClick={() => {
                  singleView(data.uuid);
                }}
                type="button"
                className="btn btn-system-light col-12 pointer mt-2"
              >
                {getLabel("view", initialLanguage)}
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TableCardTwo;
