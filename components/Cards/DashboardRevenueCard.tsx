import React from 'react'

type Props = {
  amount?: string
  amountsymbol?: string
  title?: string
}
const DashboardRevenueCard = ({ amount, amountsymbol, title }: Props) => {
  return (
    <div className="card border revenue-card mb-3 ">
      <div className="card-body">
        <div>
          <div className="card-title font-normal text-dark text-truncate font-110">
            {title}
          </div>
        </div>

        <p className="font-bold font-210 mb-0 text-truncate">
          {amountsymbol}
          <span>{amount}</span>
        </p>
      </div>
    </div>
  )
}

export default DashboardRevenueCard
