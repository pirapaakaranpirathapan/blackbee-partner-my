import React, { useContext, useEffect, useState } from 'react';
import { useApiErrorHandling } from '../Others/useApiErrorHandling';
import { memorialPageSelector } from '@/redux/memorial-pages';
import { useAppSelector } from '@/redux/store/hooks';
import { NoticeDataContext } from '@/pages/notices/[id]/NoticeDataContext';
import ArcoSelectAndCreate from '../Arco/ArcoSelectAndCreate';
import { noticeSelector } from '@/redux/notices';

interface GroupAddPagesProps {
    memorialData: any
    handleOptionChange: any
    setIsModalVisible: (isModalVisible: boolean) => void;
    isModalVisible: boolean
    defaultNames: any
    updateAddSearchValue: any
    handleUpdatePage: any;
    handleClear: any;
    handleDeselect: any;
    handleDeselectValue: any;
    response1: any
}

const GroupAddPages: React.FC<GroupAddPagesProps> = ({
    memorialData,
    handleOptionChange,
    setIsModalVisible,
    isModalVisible,
    defaultNames,
    updateAddSearchValue,
    handleUpdatePage,
    handleClear,
    handleDeselect,
    handleDeselectValue,
    response1,
}) => {
    const { NoticeData, handleSave } = useContext(NoticeDataContext);
    const Data = NoticeData.pages;
    const data = NoticeData;
    const initialLanguage = NoticeData?.initialLanguage
    const { error: ApiError } = useAppSelector(noticeSelector);
    const GroupError = useApiErrorHandling(ApiError, response1);

    return (
        <>
            <div className="row z-front">
                <div className="col-12 font-semibold mb-0">Person Name</div>
                <div className="col-12 mb-3">
                    <ArcoSelectAndCreate
                        data={memorialData}
                        placeholderValue={"Search a person"}
                        onOptionChange={handleOptionChange}
                        is_search={true}
                        setIsModalVisible={setIsModalVisible}
                        isVisible={isModalVisible}
                        updateAddSearchValue={updateAddSearchValue}
                        onClear={handleClear}
                        onDeselect={handleDeselect}
                        onDeselectValue={handleDeselectValue}
                        defaultValue={defaultNames}
                    />
                    <div className="m-0 p-0 text-danger font-normal">
                        {GroupError?.pages}
                    </div>
                </div>
                <div className="col-auto ">
                    <button
                        className="btn btn-system-primary"
                        onClick={handleUpdatePage}
                    >
                        Add
                    </button>
                </div>
            </div>
        </>
    );
};

export default GroupAddPages;
