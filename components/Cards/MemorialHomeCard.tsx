import React from 'react';

type Props = {
  title?: string;
  amount?: number;
  time?: string;
  amountsymbol?: string;
  type?: string;
  creater?: string;
};

const MemorialHomeCard = ({
  amount,
  amountsymbol,
  type,
  title,
  creater,
  time,
}: Props) => {
  return (
    <div className="card border bg-white mb-2 ">
      <div className="card-body px-md-4 pt-md-4 ">
        <div className="row m-0  ">
          <div className="col-sm-12 col-md-6 col-lg-6 p-0">
            <div className="card-title m-0 p-0">
              {title}
            </div>
          </div>
          <div className="col-sm-12 col-md-6 col-lg-6 p-0">
            <p className="card-text system-danger d-flex justify-content-md-end font-110 font-normal">
              {type}
            </p>
          </div>
        </div>
        <div className="mb-2 mb-md-3 mt-1 font-normal">
          <p className="card-text text-black-50  ">
            {time}
            <span className="card-text text-black-50 "> ·</span>
            <span className="card-text text-black-50 "> created by</span>
            <span className="card-text ms-1">
              <a
                href=""
                className="text-decoration-none text-black-50 text-decoration-underline "
              >
                {creater}
              </a>
            </span>
          </p>
        </div>
        <div className="row align-items-center">
          <div className="col-md-6 col-9 ">
            <a
              href="#"
              className="btn btn-md border border-2 shadow-sm text-dark system-secondary-light font-110 font-normal me-2"
            >
              Edit
            </a>
            <a
              href="#"
              className="btn btn-md  border border-2 shadow-sm text-dark system-secondary-light font-110 font-normal"
            >
              Preview
            </a>
          </div>
          <div className="col-md-6 col-3 d-flex justify-content-end  ">
            <div className="font-bold font-110">
              {amountsymbol}
              <span>{amount}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MemorialHomeCard;
