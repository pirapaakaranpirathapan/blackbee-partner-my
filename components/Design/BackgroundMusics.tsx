import Image from "next/image";
import React, {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useRef,
  useState,
} from "react";
import playIcon from "public/play-black.svg";
import pauseIcon from "public/pause-black.svg";
import MusicUploader from "../ImageUploder/musicUploder";
import CustomSelect from "../Arco/CustomSelect";
import MusicSkeleton from "../Skelton/MusicSkelton";
export interface ChildModalRef {
  callModalFunction: (callback: (response: any) => void) => void;
}
interface BackgroundMusicsProps {
  Data: any;
  defaultOptionId: any;
  defaultOptionLink: any;
  classname: any;
  onOptionSelect: (
    optionId: number,
    OptionLink: string,
    selectedOptionAlt: string,
    optionUUID: string
  ) => void;
  tab: string;
  handleUploadMusic: (uploadMusicUrl: string) => void;
  handleSelectMusic: (uploadMusicUrl: Boolean) => void;
  handleUploadApply:()=>void;
}
const BackgroundMusics = forwardRef(
  (
    {
      Data,
      defaultOptionId,
      onOptionSelect,
      handleUploadMusic,
      handleSelectMusic,
      handleUploadApply,
      defaultOptionLink,
      tab,
      classname,
    }: BackgroundMusicsProps,
    ref: React.Ref<ChildModalRef>
  ) => {
    const [activeTab, setActiveTab] = useState("All");
    const [selectedOptionId, setSelectedOptionId] = useState(defaultOptionId);
    const changeTab = (val: any) => {
      const activeMusic = getNameById(uniqueMusicTypes, val);
      setActiveTab(activeMusic);
      activeMusic == "All" ? AllMusicByType() : filterMusicByType(activeMusic);
    };
    const getNameById = (
      data: { id: number; name: string }[],
      id: number
    ): string => {
      const musicItem = data.find((item) => item.id === id);
      return musicItem ? musicItem.name : "All";
    };
    const handleOptionClick = (
      optionId: number,
      thumbUrl: string,
      name: string,
      uuid: string
    ) => {
      //--------------------Not valid music file from api so now commented ---===//
      handleSelectMusic(true);
      console.log(thumbUrl,selectedOptionId);
      // handleSelectMusic(thumbUrl);
      if (selectedOptionId !== optionId) {
        if (audioRef.current) {
          audioRef.current.pause();
          setIsPlaying(false);
        }
      }

      if (selectedOptionId === optionId) {
      } else {
        setSelectedOptionId(optionId); // Select the option
        onOptionSelect(optionId, thumbUrl, name, uuid); // Notify about selection
      }
    };
    const childModalRef = useRef<ChildModalRef | null>(null);
    useImperativeHandle(ref, () => ({
      callModalFunction: (callback: (response: any) => void) => {
        if (childModalRef.current) {
          childModalRef.current.callModalFunction(callback); // Call the function in the child component
        }
      },
    }));
    const audioRef = useRef<HTMLAudioElement | null>(null);
    const [isPlaying, setIsPlaying] = useState(false);
    const [musicFile, setMusicFile] = useState(
      "https://direct-upload-s3-bucket-testing.s3.amazonaws.com/7127b00f-f4a9-4422-9eba-a30cf7bbe35a"
    );
    const [currentAudioIndex, setCurrentAudioIndex] = useState<number | null>(
      null
    );
    const audioRefs = useRef<Array<HTMLAudioElement | null>>([]);

    const playAudio = (index: number | null) => {
      if (currentAudioIndex === index || index === null) {
        if (currentAudioIndex !== null) {
          const audio = audioRefs.current[currentAudioIndex];
          if (audio) {
            if (isPlaying) {
              audio.pause();
            } else {
              audio.play().catch((error) => {
                console.error("Audio playback error:", error);
              });
            }
          }
        }
        setIsPlaying(!isPlaying);
      } else {
        if (currentAudioIndex !== null) {
          const audioToPause = audioRefs.current[currentAudioIndex];
          if (audioToPause) {
            audioToPause.pause();
            audioToPause.currentTime = 0;
          }
        }
        const audioToPlay = audioRefs.current[index];
        if (audioToPlay) {
          audioToPlay.play().catch((error) => {
            console.error("Audio playback error:", error);
          });
        }
        setCurrentAudioIndex(index);
        setIsPlaying(true);
      }
    };
    const [uniqueMusicTypes, setUniqueMusicTypes] = useState<
      { id: number; name: string }[]
    >([]);
    const [filteredMusic, setFilteredMusic] = useState(Data);

    useEffect(() => {
      const tempUniqueMusicTypes: { id: number; name: string }[] = [
        { id: 10, name: "All" },
      ];
      Array.isArray(Data) &&
        Data.forEach((music: { music_type: { id: number; name: string } }) => {
          const musicType: { id: number; name: string } = music.music_type;
          if (
            !tempUniqueMusicTypes.some(
              (type) => (type as any)?.id === (musicType as any)?.id
            )
          ) {
            tempUniqueMusicTypes.push(musicType);
          }
        });
      setUniqueMusicTypes(tempUniqueMusicTypes);
    }, [Data]);
    const [filter, setFiltering] = useState(false);
    const filterMusicByType = (value: string) => {
      setFiltering(true);
      const filteredMusicData = Data.filter(
        (music: { music_type: { name: string } }) =>
          music.music_type.name === value
      );
      setFilteredMusic(filteredMusicData);
      setFiltering(false);
    };
    const AllMusicByType = () => {  
      setFilteredMusic(Data);
    };
    return (
      <div>
        <div className="row m-0  " id="filter-panel">
          <div className="col-12 p-0  ">
            <div className="inpage-sidebar"></div>
          </div>
          <div className="col-12 p-0 ">
            <div className="">
              {
                <>
                  <MusicUploader
                    name="Upload new"
                    handleUploadMusic={handleUploadMusic}
                    handleSelectMusic={handleSelectMusic}
                    ref={childModalRef}
                    defaultOptionLink={defaultOptionLink}
                    classname={"select"}
                    selectedOptionId={selectedOptionId}
                    handleUploadApply={handleUploadApply}
                  />
                </>
              }
              <div className="music-filter d-flex mt-3 mb-4 overflowx-scroll music-dropdown">
                <CustomSelect
                  data={uniqueMusicTypes}
                  defaultValue={10}
                  onChange={(value: any) => changeTab(value)}
                />
              </div>
              {filter ? (
                <>
                  <div className="mt-3">
                    <div
                      className={
                        " d-flex flex-column text-center m-0 justify-content-center gap-3 "
                      }
                    >
                      <MusicSkeleton loopTimes={12} />{" "}
                    </div>
                  </div>
                </>
              ) : (
                <>
                  <div className="mt-3">
                    <div
                      className={
                        " d-flex flex-column text-center m-0 justify-content-center gap-3 "
                      }
                    >
                      {Array.isArray(filteredMusic) &&
                        filteredMusic.map((option: any, index: number) => (
                          <div className="" key={option.id}>
                            <div
                              className={`d-flex justify-content-between align-items-center  w-100 mu-bt ${
                                selectedOptionId === option.id
                                  ? "system-text-primary"
                                  : "text-dark"
                              }`}
                            >
                              <div className="d-flex gap-3 align-items-center font-normal-over-flow-f">
                                <div className="d-flex align-items-center justify-content-center pointer ">
                                  <Image
                                    src={
                                      isPlaying && currentAudioIndex === index
                                        ? pauseIcon
                                        : playIcon
                                    }
                                    alt={""}
                                    className="sound-container"
                                    width={16}
                                    height={16}
                                    onClick={() => playAudio(index)}
                                  />
                                  <div>
                                    <audio
                                      ref={(audio) =>
                                        (audioRefs.current[index] = audio)
                                      }
                                    >
                                      <source
                                        src={option.musicFile}
                                        type="audio/mp3"
                                      />
                                      Your browser does not support the audio
                                      tag.
                                    </audio>
                                  </div>
                                </div>
                                <p className=" font-110   m-0 p-0 text-truncate ">
                                  {option?.name || ""}
                                </p>
                              </div>
                              <button
                                type="button"
                                className="btn btn-system-primary"
                                onClick={() => {
                                  handleOptionClick(
                                    option.id || 0,
                                    option?.file || "",
                                    option?.name || "",
                                    option?.uuid || ""
                                  );
                                }}
                              >
                                Apply
                              </button>
                            </div>
                          </div>
                        ))}
                    </div>
                  </div>
                </>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
);

export default BackgroundMusics;
