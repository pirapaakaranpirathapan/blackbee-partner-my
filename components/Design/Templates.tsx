import Image from "next/image";
import React, { useContext, useState } from "react";
import { Tab, Tabs } from "react-bootstrap";
import PreviewCollections from "./PreviewCollections";
import { getLabel } from "@/utils/lang-manager";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";

const Templates = ({ Data, defaultOptionId, onOptionSelect }: any) => {
  const tab = "general";
  const [activeTab, setActiveTab] = useState(tab);
  const [selectedOptionId, setSelectedOptionId] = useState(defaultOptionId);
  const changeTab = (val: any, event: React.MouseEvent<HTMLAnchorElement>) => {
    setActiveTab(val);
    event.preventDefault();
  };
  const { NoticeData } = useContext(NoticeDataContext);
  const { PageData } = useContext(PageDataContext);
  const initialLanguage = NoticeData?.initialLanguage || PageData?.initialLanguage

  return (
    <div>
      <div className="row m-0 mt-4" id="filter-panel">
        <div className="col-12 p-0  ">
          <div className="inpage-sidebar">
            <div className="scrollmenu d-flex gap-5 align-items-center">
              <div className="">
                <a
                  href="#"
                  onClick={(e) => changeTab("general", e)}
                  className=""
                >
                  <div
                    className={
                      activeTab === "general"
                        ? `system-text-primary shadow-md sidelink font-bold active font-150`
                        : `rounded shadow-md sidelink font-bold font-150`
                    }
                  >
                    {getLabel("general", initialLanguage)}
                  </div>
                </a>
              </div>
              <div className="">
                <a href="#" onClick={(e) => changeTab("hindu", e)} className="">
                  <div
                    className={
                      activeTab === "hindu"
                        ? `system-text-primary shadow-md sidelink font-bold active font-150`
                        : `rounded shadow-md sidelink font-bold font-150`
                    }
                  >
                    {getLabel("hindu", initialLanguage)}
                  </div>
                </a>
              </div>
              <div>
                <a
                  href="#"
                  onClick={(e) => changeTab("classic", e)}
                  className=""
                >
                  <div
                    className={
                      activeTab === "classic"
                        ? `system-text-primary shadow-md sidelink  font-bold active font-150`
                        : `rounded shadow-md sidelink font-bold font-150`
                    }
                  >
                    {getLabel("classic", initialLanguage)}
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className="col-12 p-0 my-4">
          <div className="">
            {activeTab === "general" && (
              <>
                <PreviewCollections
                  Data={Data}
                  defaultOptionId={defaultOptionId}
                  onOptionSelect={onOptionSelect}
                />
              </>
            )}
            {activeTab === "hindu" && <>{getLabel("hindu", initialLanguage)}</>}
            {activeTab === "classic" && <>{getLabel("classic", initialLanguage)}</>}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Templates;
