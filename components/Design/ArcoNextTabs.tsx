import { Tabs, Typography } from "@arco-design/web-react";
import PreviewCollections from "./PreviewCollections";
const { TabPane } = Tabs;

const ArcoNextTabs = ({ Data, defaultOptionId, onOptionSelect, buttonText }: any) => {
  return (
    <>
      <Tabs tabPosition="left" className={"pt-2"}>
        <TabPane key="tab1" title="All">
          <>
            <PreviewCollections
              Data={Data}
              defaultOptionId={defaultOptionId}
              onOptionSelect={onOptionSelect}
              buttonText={buttonText}
            />
          </>
          {/* <Tabs>
          <TabPane key='tab1' title='Tab 1'>
            <Typography.Paragraph>Content of Tab Panel 1</Typography.Paragraph>
          </TabPane>
          <TabPane key='tab2' title='Tab 2'>
            <Typography.Paragraph >Content of Tab Panel 2</Typography.Paragraph>
          </TabPane>
          <TabPane key='tab3' title='Tab 3'>
            <Typography.Paragraph >Content of Tab Panel 3</Typography.Paragraph>
          </TabPane>
        </Tabs> */}
          {/* <Typography.Paragraph>Content of Tab Panel 1</Typography.Paragraph> */}
        </TabPane>
        <TabPane key="tab2" title="Hindu">
          <Typography.Paragraph>Hindu</Typography.Paragraph>
        </TabPane>
        <TabPane key="tab3" title="Classic">
          <Typography.Paragraph>Classic</Typography.Paragraph>
        </TabPane>
        {/* <TabPane key='tab4' title='Premium' >
          <Typography.Paragraph>#Premium</Typography.Paragraph>
        </TabPane>
        <TabPane key='tab5' title='Popular'>
          <Typography.Paragraph>#Popular</Typography.Paragraph>
        </TabPane> */}
      </Tabs>
      {/* <div className='mt-4'>
        <p className="m-0 p-0">#Premium</p>
        <p className="m-0 p-0">#Popular</p>
      </div> */}
    </>
  );
};

export default ArcoNextTabs;
