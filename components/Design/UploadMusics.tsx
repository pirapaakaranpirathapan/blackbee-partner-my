import Image from "next/image";
import React, {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useRef,
  useState,
} from "react";
import musicIcon from "public/musicIcon.svg";
import playIcon from "public/play-black.svg";
import pauseIcon from "public/pause-black.svg";

import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import { patchNotice } from "@/redux/notices";
import MusicUploader from "../ImageUploder/musicUploder";
export interface ChildModalRef {
  callModalFunction: (callback: (response: any) => void) => void;
}
interface BackgroundMusicsProps {
  Data: any;
  defaultOptionId: any;
  defaultOptionLink: any;
  onOptionSelect: (
    optionId: number,
    OptionLink: string,
    selectedOptionAlt: string,
    optionUUID: string
  ) => void;
  tab: string;
  handleUploadMusic: (uploadMusicUrl: string) => void;
  handleSelectMusic:(uploadMusicUrl: Boolean) => void;
}
const UploadMusics = forwardRef(
  (
    {
      Data,
      defaultOptionId,
      onOptionSelect,
      handleUploadMusic,
      handleSelectMusic,
      defaultOptionLink,
      tab,
    }: BackgroundMusicsProps,
    ref: React.Ref<ChildModalRef>
  ) => {
    // const tab = "All";
    const [activeTab, setActiveTab] = useState("All");
    const [music, setMusic] = useState("");
    const [selectedOptionId, setSelectedOptionId] = useState(defaultOptionId);
    const changeTab = (val: any) => {
      setActiveTab(val);
      val == "All" ? AllMusicByType() : filterMusicByType(val);
    };
    console.log("id", selectedOptionId);
    const handleOptionClick = (
      optionId: number,
      thumbUrl: string,
      name: string,
      uuid: string
    ) => {
      //--------------------Not valid music file from api so now commented ---===//
      handleSelectMusic(true);
      console.log(thumbUrl);
      // handleSelectMusic(thumbUrl);
      if (selectedOptionId !== optionId) {
        if (audioRef.current) {
          audioRef.current.pause();
          setIsPlaying(false);
        }
      }
      // playAudio();
      // setSelectedOptionId(optionId);
      // onOptionSelect(optionId, thumbUrl, name, uuid);
      if (selectedOptionId === optionId) {
        // setSelectedOptionId(null); // Deselect the option
        // onOptionSelect(0, thumbUrl, name, uuid); // Notify about deselection
      } else {
        setSelectedOptionId(optionId); // Select the option
        onOptionSelect(optionId, thumbUrl, name, uuid); // Notify about selection
      }
    };
    const childModalRef = useRef<ChildModalRef | null>(null);
    useImperativeHandle(ref, () => ({
      callModalFunction: (callback: (response: any) => void) => {
        if (childModalRef.current) {
          childModalRef.current.callModalFunction(callback); // Call the function in the child component
        }
      },
    }));
    const audioRef = useRef<HTMLAudioElement | null>(null);
    const [isPlaying, setIsPlaying] = useState(false);
    const [musicFile, setMusicFile] = useState(
      "https://direct-upload-s3-bucket-testing.s3.amazonaws.com/7127b00f-f4a9-4422-9eba-a30cf7bbe35a"
    );
    const [currentAudioIndex, setCurrentAudioIndex] = useState<number | null>(null);

    const audioRefs = useRef<Array<HTMLAudioElement | null>>([]);


    // const playAudio = () => {
    //   console.log("playing");
    //   if (audioRef.current) {
    //     if (isPlaying) {
    //       audioRef.current.pause();
    //     } else {
    //       audioRef.current.play().catch((error) => {
    //         console.error("Audio playback error:", error);
    //       });
    //     }
    //     setIsPlaying(!isPlaying);
    //   }
    // };

    const playAudio = (index: number | null) => {
      if (currentAudioIndex === index || index === null) {
        if (currentAudioIndex !== null) {
          const audio = audioRefs.current[currentAudioIndex];
          if (audio) {
            if (isPlaying) {
              audio.pause();
            } else {
              audio.play().catch((error) => {
                console.error("Audio playback error:", error);
              });
            }
          }
        }
        setIsPlaying(!isPlaying);
      } else {
        if (currentAudioIndex !== null) {
          const audioToPause = audioRefs.current[currentAudioIndex];
          if (audioToPause) {
            audioToPause.pause();
            audioToPause.currentTime = 0;
          }
        }
        const audioToPlay = audioRefs.current[index];
        if (audioToPlay) {
          audioToPlay.play().catch((error) => {
            console.error("Audio playback error:", error);
          });
        }
        setCurrentAudioIndex(index);
        setIsPlaying(true);
      }
    };
    

    const [uniqueMusicTypes, setUniqueMusicTypes] = useState<unknown[]>([]);
    // useEffect to compute unique music types when Data changes
    useEffect(() => {
      // Create a temporary array to hold unique music types
      const tempUniqueMusicTypes: unknown[] = [];
     Array.isArray(Data) &&Data.forEach((music: { music_type: unknown }) => {
        const musicType: unknown = music.music_type;
        // Check if the music type is not already in the tempUniqueMusicTypes array
        if (
          !tempUniqueMusicTypes.some(
            (type) => (type as any)?.id === (musicType as any)?.id
          )
        ) {
          tempUniqueMusicTypes.push(musicType);
        }
      });
      // Update the state with the computed unique music types
      setUniqueMusicTypes(tempUniqueMusicTypes);
    }, [Data]);

    // Define a state for the filtered data
    const [filteredMusic, setFilteredMusic] = useState(Data);

    // Filter and set the filtered data when needed (e.g., when a button is clicked)
    const filterMusicByType = (value: string) => {
      const filteredMusicData = Data.filter(
        (music: { music_type: { name: string } }) =>
          music.music_type.name === value
      );
      setFilteredMusic(filteredMusicData);
    };
    // Filter and set the filtered data when needed (e.g., when a button is clicked)
    const AllMusicByType = () => {
      setFilteredMusic(Data);
    };
    const handleUploadApply=()=>{};
    return (
      <div className="">
       <MusicUploader
          name="Upload new"
          handleUploadMusic={handleUploadMusic}
          handleSelectMusic={handleSelectMusic}
          handleUploadApply={handleUploadApply}
          ref={childModalRef}
          defaultOptionLink={defaultOptionLink} classname={"upload"} 
          selectedOptionId={defaultOptionId}                  />
      </div>
    );
  }
);

export default UploadMusics;
