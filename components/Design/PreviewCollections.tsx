import Image from "next/image";
import React, { useContext, useState } from "react";
import darktemplate from "../../public/darktemplate.svg";
import orangetemplate from "../../public/orangetemplate.svg";
import { ACTIVE_PARTNER } from "@/config/constants";
import {
  getAllNoticeTemplates,
  templatesSelector,
} from "@/redux/notice-templates";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import { pagetemplatesSelector } from "@/redux/page-templates";
import CustomImage from "../Elements/CustomImage";
import { getLabel } from "@/utils/lang-manager";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";

const PreviewCollections = ({
  Data,
  defaultOptionId,
  onOptionSelect,
  buttonText,
}: any) => {
  const { NoticeData } = useContext(NoticeDataContext);
  const { PageData } = useContext(PageDataContext);
  const initialLanguage =
    NoticeData?.initialLanguage || PageData?.initialLanguage;
  const dispatch = useAppDispatch();
  const [selectedOptionId, setSelectedOptionId] = useState(defaultOptionId);

  const handleOptionClick = (
    optionId: number,
    thumbUrl: string,
    name: string,
    uuid: string,
    buttonText?: string
  ) => {
    // setSelectedOptionId(optionId);
    // onOptionSelect(optionId, thumbUrl, name, uuid);
    if (selectedOptionId === optionId) {
      // setSelectedOptionId(null); // Deselect the option
      // onOptionSelect(null, null, null, uuid); // Notify about deselection
    } else {
      setSelectedOptionId(optionId); // Select the option
      onOptionSelect(optionId, thumbUrl, name, uuid); // Notify about selection
    }
  };
  return (
    <div>
      <div
        className={
          "d-flex flex-wrap justify-content-around justify-content-lg-start gap-5 text-center m-0 "
        }
        // className="grid-container-m"
      >
        {Array.isArray(Data) &&
          Data.map((option: any) => (
            <div
              // className=" d-flex  m-0 "
              className="d-flex flex-wrap justify-content-around   text-center m-0 "
              key={option.id}
              // onClick={() =>
              //   handleOptionClick(
              //     option.id || 0,
              //     option?.thumb_url || "",
              //     option?.name || "",
              //     option?.uuid || ""
              //   )
              // }
            >
              <div className={`d-flex flex-column justify-content-center`}>
                <div className={`collection-container  `}>
                  <CustomImage
                    src={option?.thumb_url || ""}
                    alt={option?.name || ""}
                    className="image"
                    width={118}
                    height={160}
                    divClass={`collection-img-container rounded d-flex align-items-center justify-content-center border position-relative system-grey-200 ${
                      selectedOptionId === option.id ? "selected-item" : ""
                    }`}
                  />
                  <button
                    type="button"
                    className={`text-white px-4 mt-4 font-110 font-bold select-template-button ${
                      selectedOptionId === option.id
                        ? "selected-item-button"
                        : ""
                    }`}
                    onClick={() =>
                      handleOptionClick(
                        option.id || 0,
                        option?.thumb_url || "",
                        option?.name || "",
                        option?.uuid || ""
                      )
                    }
                    disabled={selectedOptionId === option.id ? true : false}
                  >
                    {buttonText ? buttonText : "Select"}
                  </button>
                </div>
                <div className="text-decoration-none m-0 p-0">
                  <p className="font-normal font-90 text-dark pt-1 pointer text-truncate m-0 p-0">
                    {/* {option?.name || ""} */}
                    {getLabel("preview", initialLanguage)}
                  </p>
                </div>
              </div>
              {/* <div className={`d-flex flex-column justify-content-center`}>
                <div className={`collection-container  ${selectedOptionId === option.id ? "" : ""} `}>
                  <CustomImage
                    src={option?.thumb_url || ""}
                    alt={option?.name || ""}
                    className="image"
                    width={118}
                    height={160}
                    divClass="collection-img-container rounded d-flex align-items-center justify-content-center border position-relative system-grey-200"
                  />
                  {selectedOptionId === option.id ? (
                    <>
                      <button
                        type="button"
                        className={`text-white  px-4 mt-4 font-110 font-bold  select-template-button `}
                      >
                        Select
                      </button>
                    </>
                  ) : ""}

                </div>
                <p className="font-normal font-90 text-dark pt-1 pointer">
                  {option?.name || ""}
                </p>
              </div> */}
            </div>
          ))}
      </div>
    </div>
  );
};

export default PreviewCollections;
