import React, { useState } from 'react';

interface LoaderContextType {
  isLoading: boolean;
  showLoader: () => void;
  hideLoader: () => void;
}

const LoaderContext = React.createContext<LoaderContextType | undefined>(
  undefined
);

const LoaderProvider: React.FC = ({ children }:any) => {
  const [isLoading, setIsLoading] = useState(false);

  const showLoader = () => {
    setIsLoading(true);
  };

  const hideLoader = () => {
    setIsLoading(false);
  };

  return (
    <LoaderContext.Provider value={{ isLoading, showLoader, hideLoader }}>
      {children}
    </LoaderContext.Provider>
  );
};

const useLoader = (): LoaderContextType => {
  const context = React.useContext(LoaderContext);
  if (!context) {
    throw new Error('useLoader must be used within a LoaderProvider');
  }
  return context;
};

export { LoaderProvider, useLoader };


//-------------------------------------------apply loader state--------------------------------//
//import { useLoader } from './LoaderService';
// const YourComponent: React.FC = () => {
//     const { showLoader, hideLoader } = useLoader();
  
//     // Example usage: Show the loader for 2 seconds, then hide it
//     const simulateAsyncOperation = () => {
//       showLoader();
//       setTimeout(() => {
//         hideLoader();
//       }, 2000);
//     };