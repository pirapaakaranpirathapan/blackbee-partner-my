import React from 'react'

const Loader = () => {
  return (
    <div className="loader-container">
      <div className="loader loader-margin"></div>
    </div>
  )
}

export default Loader
