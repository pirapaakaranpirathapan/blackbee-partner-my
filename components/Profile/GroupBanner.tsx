import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
import React, { useContext } from "react";
import ShimmerEffect from "../Skelton/ShimmerEffect";
import ProfileBannerSkelton from "../Skelton/ProfileBannerSkelton";

const GroupBanner = () => {
  const { NoticeData } = useContext(NoticeDataContext);
  const Data = NoticeData.pages;
  const data = NoticeData;
  return (
    <div className="row">
      {NoticeData.loading ? (
        <div className="py-3 px-4 system-text-dark-light">
          <div className="font-170 font-bold system-text-blue d-flex gap-2">
            <div className="text-truncate"> {Array.isArray(Data) && Data.length > 0 ? Data[0].name : ""}</div>
            <div className="no-wrap">
              {Array.isArray(Data) && Data.length > 1
                ? Data.length > 2
                  ? ` & ${Data.length - 1} Others`
                  : ` & ${Data.length - 1} Other`
                : ""}
            </div>
          </div>
          <p className=" font-normal p-0 system-text-blue m-0">
            {data?.notice_title}
          </p>
        </div>
      ) : (
        <div>
          {/* <div className="py-3 px-4 system-text-dark-light">
            <h1 className="font-170 font-bold system-text-blue text-truncate">
              <ShimmerEffect width="" height="20px" />
            </h1>
            <div className=" font-normal p-0 system-text-blue m-0">
              <ShimmerEffect width="" height="20px" />
            </div>
          </div> */}
          <div className="row ps-lg-3 ps-md-3 ps-sm-2 p-1 system-text-dark-light">
            <div className="col-md-9 p-0 col-sm-12 d-md-flex align-items-center">
              <ProfileBannerSkelton />

            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default GroupBanner;
