import Image from "next/image";
import React, { useContext, useEffect, useRef, useState } from "react";
import ellipsissvg from "../../public/ellipsissvg.svg";
import { getLabel } from "@/utils/lang-manager";
import { useRouter } from "next/router";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";
import MemorialCreatePage from "../Modal/MemorialCreatePage";
import { Button } from "react-bootstrap";
import ArcoSelectAndCreate from "../Arco/ArcoSelectAndCreate";
import { integer } from "aws-sdk/clients/lightsail";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import {
  createMemorialPage,
  getAllMemorialPages,
  memorialPageSelector,
} from "@/redux/memorial-pages";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import { Notification } from "@arco-design/web-react";
import { patchNoticeGroupPage } from "@/redux/notices";
import {
  LabeledValue,
  OptionInfo,
} from "@arco-design/web-react/es/Select/interface";
import FullScreenModal from "../Elements/FullScreenModal";
import AlertModal from "../Elements/AlertModal";

const NoDataBanner = () => {
  const [showProfileMore, setShowProfileMore] = useState(false);
  const [addPages, setAddPages] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [selectedValues, setSelectedValues] = useState<string[]>([]);
  const [defaultNames, setDefaultNames] = useState<string[]>([]);
  const [response, setResponse] = useState("");
  const [groupId, setGroupId] = useState<string[]>([]);
  const [addSearchValue, setAddSearchValue] = useState("");
  const [updateError, setUpdateError] = useState("");

  const popupRef = useRef<HTMLDivElement>(null);
  const router = useRouter();
  const { query } = router;
  const path = router.pathname;
  const subtab = router.query.s;
  const allowedPaths = ["/notices/[id]"];
  const { handleSave, NoticeData } = useContext(NoticeDataContext);
  const { PageData } = useContext(PageDataContext);
  const Data = NoticeData || PageData;
  const initialLanguage = NoticeData?.initialLanguage || PageData?.initialLanguage
  const dataType = (NoticeData && "notices") || (PageData && "memorials");
  const actionBtnRef = useRef<HTMLButtonElement>(null);
  const dispatch = useAppDispatch();

  const { data: memorialData } = useAppSelector(memorialPageSelector);
  console.log("memorialDatamemorialData", memorialData);

  const handlePerson = () => {
    setAddPages(true);
  };

  // create memorial
  const handleCreate = (
    personName: string,
    salutation: integer,
    clientId: string,
    countryId: string,
    pageTypeId: string,
    relationship: string,
    dod: string,
    dob: string,
    deathPlace: string
  ) => {
    // setsubmitButtonAddDethPersonEnabled(true);
    createPage(
      personName,
      // salutationId,
      salutation,
      clientId,
      countryId,
      pageTypeId,
      relationship,
      dod,
      dob,
      deathPlace
    );
  };

  const createPage = (
    personName: any,
    // salutationId: any,
    salutation: integer,
    clientId: any,
    countryId: any,
    pageTypeId: string,
    relationship: any,
    dod: any,
    dob: any,
    deathPlace: any
  ) => {
    const CreateData = {
      name: personName,
      page_type_id: pageTypeId,
      salutation_id: salutation,
      client_id: clientId,
      relationship_id: relationship,
      // salutation: salutation,
      dod: dod,
      dob: dob,
      death_location: deathPlace,
      country_id: countryId,
    };
    dispatch(
      createMemorialPage({
        activePartner: ACTIVE_PARTNER,
        activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
        CreateData,
      })
    ).then((response: any) => {
      dispatch(
        getAllMemorialPages({
          active_partner: ACTIVE_PARTNER,
          active_service_provider: ACTIVE_SERVICE_PROVIDER,
          page: 1,
        })
      );
      setResponse(response);

      // setMemorialId(newMemorialUuid);
      const memorialResponseData = response?.payload?.data?.data;
      // setMemorialDatasCreate(memorialResponseData);
      if (response.payload.success === true) {
        const newMemorialUuid = response.payload?.data?.data?.uuid;
        const createuuid = [newMemorialUuid, ...selectedValues];
        const createUuidUniqueArray = [...createuuid, ...groupId];
        setSelectedValues(createUuidUniqueArray);
        // setMemorialId(newMemorialUuid);
        const newName = response.payload?.data?.data?.name;
        const updatedDefaultNames = [newName, ...defaultNames];

        setDefaultNames(updatedDefaultNames);

        setIsModalVisible(false);

        // setMemorialCreate(false);
        Notification.success({
          title: "Memorial has been created successfully",
          duration: 3000,
          content: undefined,
          style: {
            zIndex: 1000,
          },
        });
      } else {
        // setsubmitButtonAddDethPersonEnabled(false);
        Notification.error({
          title: "Failed to  create memorial",
          duration: 3000,
          content: undefined,
          style: {
            zIndex: 1000,
          },
        });
      }
    });
  };
  const active_id = NoticeData?.uuid;
  const handleUpdatePage = () => {
    const updateData = {
      pages: selectedValues,
    };
    dispatch(
      patchNoticeGroupPage({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_id: active_id,
        updatedData: updateData,
      })
    ).then(async (response: any) => {
      if (response.payload.success == true) {
        handleSave();
        dispatch(
          getAllMemorialPages({
            active_partner: ACTIVE_PARTNER,
            active_service_provider: ACTIVE_SERVICE_PROVIDER,
            page: 1,
          })
        );
        Notification.success({
          title: "Successfully updated",
          duration: 3000,
          content: undefined,
        });
        setAddPages(false);
      } else {
        Notification.error({
          title: "Failed",
          duration: 3000,
          content: undefined,
        });
      }
    });
  };

  const handleOptionChange = (selectedValues: string[]) => {
    const updatedSelectedValues = [...selectedValues, ...groupId];
    setSelectedValues(updatedSelectedValues);
  };
  const updateAddSearchValue = (value: React.SetStateAction<string>) => {
    setAddSearchValue(value);
  };
  const handleDeselectValue = (
    value: string | number | LabeledValue,
    option: OptionInfo
  ) => {
    const storedMemorialData = localStorage.getItem("memorialData");

    if (storedMemorialData) {
      let memorialDataArray = JSON.parse(storedMemorialData);

      // Filter out the item with the matching name property
      memorialDataArray = memorialDataArray.filter(
        (item: { name: string }) => item.name !== value
      );

      // Update the local storage with the modified data
      localStorage.setItem("memorialData", JSON.stringify(memorialDataArray));
    }

    setDefaultNames((prevNames) => prevNames.filter((name) => name !== value));
  };
  function handleClear(visible: boolean): void {
    if (visible) {
      setSelectedValues([]);
      // setProfileId([]);
    }
  }

  function handleDeselect(
    value: string | number | LabeledValue,
    option: OptionInfo
  ): void {
    setSelectedValues((prevProfileId) =>
      prevProfileId.filter((uuid) => uuid !== value)
    );
  }
  return (
    <>
      <AlertModal
        show={addPages}
        onClose={() => {
          setAddPages(false);
        }}
        title="Add memorial page with this notice"
        description="You can edit this information later as well"
        position="centered"
      >
        <>
          <div className="row z-front">
            <div className="col-12 font-semibold mb-0">Person Name</div>
            <div className="col-12 mb-3">
              <FullScreenModal
                show={isModalVisible}
                onClose={() => {
                  setIsModalVisible(false);
                }}
                title={`${getLabel("createMemorialHeading", initialLanguage)}`}
                description={`${getLabel("createMemorialHeadingDescription", initialLanguage)}`}
                footer={
                  <Button ref={actionBtnRef} className="btn-system-primary">
                    {getLabel("create", initialLanguage)}
                  </Button>
                }
              >
                <MemorialCreatePage
                  response={response}
                  actionBtnRef={actionBtnRef}
                  handleCreate={handleCreate}
                  addSearchValue={addSearchValue}
                />
              </FullScreenModal>
              <ArcoSelectAndCreate
                data={memorialData}
                placeholderValue={"Search a person"}
                onOptionChange={handleOptionChange}
                is_search={true}
                setIsModalVisible={setIsModalVisible}
                isVisible={isModalVisible}
                updateAddSearchValue={updateAddSearchValue}
                onClear={handleClear}
                onDeselect={handleDeselect}
                onDeselectValue={handleDeselectValue}
                defaultValue={defaultNames}
              />
              <div className="m-0 p-0 text-danger font-normal">
                {updateError}
              </div>
            </div>
            <div className="col-auto ">
              <button
                className="btn btn-system-primary"
                onClick={handleUpdatePage}
              >
                Add
              </button>
            </div>
          </div>
        </>
      </AlertModal>
      <div className="row system-text-dark-light no-banner">
        <div className="d-flex align-items-center justify-content-center">
          <button
            type="button"
            className="btn  btn-lg system-outline-primary bg-white font-bold "
            onClick={handlePerson}
          >
            + {getLabel("addPerson", initialLanguage)}
          </button>
        </div>
      </div>
    </>
  );
};

export default NoDataBanner;
