import React, { useState } from "react";
import Image from "next/image";
import memorialprofile from "../../public/memorialprofile.png";
import upload_camera from "../../public/upload_camera.svg";
import { patchNotice } from "@/redux/notices/actions";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";

const ProfileBanner = (props: any) => {
  const { data } = props;
  const [isEditing, setIsEditing] = useState(false);
  const [fullName, setFullName] = useState(data?.["full_name"]);
  const [nickName, setNickName] = useState(data?.["nick_name"]);
  const [dod, setDod] = useState(data?.dod);
  const [deathLocation, setDeathLocation] = useState(data?.["death_location"]);
  const dispatch = useAppDispatch();
  const handleEditClick = () => {
    if (isEditing) {
      handleUpdate(); // Call the updateInfo function when "Save" button is clicked
    }
    setIsEditing(!isEditing); // Toggle the isEditing state
  };
  const handleInputChange = ( 
    e: React.ChangeEvent<HTMLInputElement>,
    field: string
  ) => {
    const value = e.target.value;
    switch (field) {
      case "fullName":
        setFullName(value);
        break;
      case "nickName":
        setNickName(value); 
        break;
      case "dod":
        setDod(value);
        break;
      case "deathLocation":
        setDeathLocation(value);
        break;
      default:
        break;
    }
  };
  const handleUpdate = () => {
    const updatedData = {
      name: fullName,
      nickname: nickName,
      dod: dod,
    death_location: deathLocation,
     
      dob:dod,
   
    salutation_id: "5",
    birth_location: "5"
    };
    console.log("updatedData", updatedData);

    dispatch(
      patchNotice({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,

        updatedData,
        active_id: data.active_page as unknown as string,
      })
    );
  };

  return (
    <div>
      <div className="row ps-lg-3 ps-md-3 ps-sm-2 p-1 system-text-dark-light">
        <div className="col-md-9 p-0 col-sm-12 d-md-flex align-items-center">
          <div className="ps-md-3 d-flex align-items-center justify-content-center">
            <div className="main-profile-container position-relative rounded-circle border border-3 border-white">
              <Image
                src={memorialprofile}
                alt="memorialprofile"
                placeholder="blur"
                className="rounded-circle image"
              />
              <div className="bg-white rounded-circle position-absolute end-0 top-50 cam-container pointer">
                <Image src={upload_camera} alt="camera" className="image" />
              </div>
            </div>
          </div>
          <div
            style={{ maxWidth: "100%" }}
            className="px-md-3 w-100 d-flex align-items-center justify-content-md-start justify-content-center text-center text-md-start mb-3 mb-md-0"
          >
            <div className="w-100">
              <h5
                className={`m-0 system-text-blue font-170 font-bold text-truncate-mobile pe-0 me-0 pe-md-5 me-md-5 line-height-2 ${
                  isEditing ? "editable" : ""
                }`}
              >
                {isEditing ? (
                  <input
                    type="text"
                    className=" inline-input "
                    value={fullName}
                    onChange={(e) => handleInputChange(e, "fullName")}
                  />
                ) : (
                  fullName
                )}
              </h5>
              <p
                className={`m-0 font-90 system-text-blue text-truncate-mobile line-height-2 ${
                  isEditing ? "editable" : ""
                }`}
              >
                {isEditing ? (
                  <input
                    type="text"
                    className="inline-input"
                    value={nickName}
                    onChange={(e) =>
                      handleInputChange(e, `Nickname: ${nickName}`)
                    }
                  />
                ) : (
                  ` ${nickName}`
                )}
              </p>
              <p
                className={`m-0 font-90 system-text-blue line-height-2 ${
                  isEditing ? "editable" : ""
                }`}
              >
                {isEditing ? (
                  <input
                    type="text"
                    className=" inline-input"
                    value={dod}
                    onChange={(e) => handleInputChange(e, "dod")}
                  />
                ) : (
                  <>
                    {dod} -{" "}
                    <span>
                      <a
                        href=""
                        className="font-90 system-text-blue-two  ms-1 line-height-2"
                      >
                        Add death date
                      </a>
                    </span>
                  </>
                )}
              </p>
              <p
                className={`m-0 font-90 system-text-blue text-truncate line-height-2 ${
                  isEditing ? "editable" : ""
                }`}
              >
                {isEditing ? (
                  <input
                    type="text"
                    className=" inline-input "
                    value={deathLocation}
                    onChange={(e) => handleInputChange(e, "deathLocation")}
                  />
                ) : (
                  deathLocation
                )}
              </p>
            </div>
          </div>
        </div>

        <div className="col-md-3 col-sm-12 d-flex justify-content-md-end justify-content-center mt-md-2 mb-3 mb-md-0">
          <div>
            <button onClick={handleEditClick} className="btn btn-dark">
              {isEditing ? "Save" : "Edit Info"}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProfileBanner;
