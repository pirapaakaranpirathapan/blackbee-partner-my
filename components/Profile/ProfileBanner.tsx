import Image from "next/image";
import React, { useContext, useEffect, useRef, useState } from "react";
import memorialprofile from "../../public/memorialprofile.png";
import memorialImage from "../../public/cover_image.png";
import upload_camera from "../../public/upload_camera.svg";
import FullScreenModal from "../Elements/FullScreenModal";
import { Button, Modal, OverlayTrigger } from "react-bootstrap";
import AddRelationship from "../Modal/AddRelationship";
import EditInfo, { ChildModalRef } from "../Modal/EditInfo";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";
import { ImageNewUploder } from "../ImageUploder/imageNewUploder";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import { patchNotice } from "@/redux/notices";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import router, { useRouter } from "next/router";
import { Notification, Popover } from "@arco-design/web-react";
import {
  getAllPageTemplateFrames,
  pagetemplatesFrameSelector,
} from "@/redux/page-templates-frames";
import { integer } from "aws-sdk/clients/cloudfront";
import ShimmerEffect from "../Skelton/ShimmerEffect";
import { MonthDate } from "../TimeConverter/MonthFormat";
import ProfileBannerSkelton from "../Skelton/ProfileBannerSkelton";
import { getLabel } from "@/utils/lang-manager";
import CustomImage from "../Elements/CustomImage";

const ProfileBanner = () => {
  const { handleSave: noticeHandleSave, NoticeData } =
    useContext(NoticeDataContext);
  const { handleSave: pageHandleSave, PageData } = useContext(PageDataContext);

  const [ShowProfileBanner, setShowProfileBanner] = useState(false);
  const [name, setName] = useState(NoticeData?.["full_name"]);
  const Data = PageData || NoticeData;
  //
  const initialLanguage =
    NoticeData?.initialLanguage || PageData?.initialLanguage;
  // console.log("Data----->", Data);
  const dispatch = useAppDispatch();
  const momorialId = useRouter().query.id;
  const photoUrl = useRouter().query?.photo_url;
  const childModalRef = useRef<ChildModalRef>(null);
  const [btnShow, setBtnShow] = useState(false);
  const [show, setShow] = useState(false);
  const [popoverShow, setPopoverShow] = useState(false);
  const [buttonDisabled, setButtonDisabled] = useState(false);

  const imgRef = useRef<HTMLImageElement>(null);
  const aspectRatio = 1;
  const [activePage, setActivePage] = useState(1);
  const { query } = router;
  useEffect(() => {
    const img = imgRef.current;
    if (img) {
      img.style.width = "100%";
      img.style.height = "auto";
      img.style.paddingBottom = `${100 / aspectRatio}%`;
    }
  }, [query]);

  const handleActionSuccess = () => {
    setShowProfileBanner(false);
    setBtnShow(false);
    noticeHandleSave();
    pageHandleSave();
  };
  const handleClick = () => {
    setBtnShow(true);
    setButtonDisabled(true);
    if (childModalRef.current) {
      console.log("childModalRef.current", childModalRef.current);

      childModalRef.current.callModalFunction();
    }
    fewSecWait();
  };
  const route = useRouter();

  const fewSecWait = () => {
    setTimeout(() => {
      setButtonDisabled(false);
    }, 2000);
  };

  const handleUpdateImage = (url: any) => {
    const updatedData = {
      photo_url: url,
    };
    dispatch(
      patchNotice({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        updatedData,
        active_id: momorialId as unknown as string,
      })
    ).then(async (response: any) => {
      if (response.payload.success == true) {
        Notification.success({
          title: "Updated successfully",
          duration: 3000,
          content: undefined,
        });
      } else {
        Notification.error({
          title: "Update failed",
          duration: 3000,
          content: undefined,
        });
        setBtnShow(false);
      }
    });
  };
  const deletePhoto = () => {
    const updatedData = {
      photo_url: "",
    };
    dispatch(
      patchNotice({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        updatedData,
        active_id: momorialId as unknown as string,
      })
    ).then(async (response: any) => {
      if (response.payload.success == true) {
        Notification.success({
          title: "Successfully deleted",
          duration: 3000,
          content: undefined,
        });
      } else {
        Notification.error({
          title: "Failed",
          duration: 3000,
          content: undefined,
        });
      }
    });
  };
  const upload = (url: string, mediaTypeId: any) => {
    // const uploadImageData = {
    //   url: url,
    //   media_type_id: mediaTypeId,
    // };
    // dispatch(
    //   mediaUpload({
    //     active_partner: ACTIVE_PARTNER,
    //     active_service_provider: ACTIVE_SERVICE_PROVIDER,
    //     active_notice: momorialId as unknown as string,
    //     createData: uploadImageData,
    //   })
    // ).then(async (response: any) => {
    //   if (response.payload.success == true) {
    //     toast("Media uploaded successfully");
    //   } else {
    //     toast("Failed");
    //   }
    // });
    handleUpdateImage(url);
  };
  const handleUploadImage = (file: any) => {
    upload(file, 1);
  };
  const [url, setUrl] = useState("");
  const [frameId, setFrameId] = useState(1);
  const handleImageClick = (url: any) => {
    setUrl(url);
  };
  const saveFrame = () => {
    const updatedData = {
      frame_id: frameId,
    };
    dispatch(
      patchNotice({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        updatedData,
        active_id: momorialId as unknown as string,
      })
    ).then(async (response: any) => {
      if (response.payload.success == true) {
        Notification.success({
          title: "Successfully Set Frame",
          duration: 3000,
          content: undefined,
        });
      } else {
        Notification.error({
          title: "Failed",
          duration: 3000,
          content: undefined,
        });
      }
    });
  };
  // const renderContainer = (props: any) => (
  //   <div className="bg-white rounded p-2" {...props}>
  //     <ImageNewUploder
  //       name="Upload new"
  //       textOnly={true}
  //       handleUploadImage={handleUploadImage}
  //     />
  //     <p className="pointer" onClick={handleShow}>
  //       Set photo frame
  //     </p>
  //     <p className="pointer" onClick={deletePhoto}>
  //       Delete
  //     </p>
  //   </div>
  // );
  const handleClose = () => setShow(false);
  const handleShow = () => {
    setShow(true);
  };
  const { data: frameData } = useAppSelector(pagetemplatesFrameSelector);
  const getFrame = () => {
    Data.template_id && dispatch(
      getAllPageTemplateFrames({
        active_partner: ACTIVE_PARTNER,
        active_page_template: Data.template_id,
      })
    );
  };
  useEffect(() => {
    getFrame();
  }, []);
  const routerPath = useRouter();
  const routerSpath = routerPath?.query?.s;

  console.log("anodata", Data);

  return (
    <div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton className="mb-0 font-150 font-bold">
          {getLabel("updateProfilePhoto", initialLanguage)}
        </Modal.Header>
        <Modal.Body>
          <div>
            <div className="container">
              <div className="row">
                <div className="col-8">
                  <div>
                    <h6>{getLabel("updateProfilePhoto", initialLanguage)}</h6>
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "center",
                      }}
                      className="p-5"
                    >
                      <div className="position-relative">
                        <Image
                          src="https://direct-upload-s3-bucket-testing.s3.amazonaws.com/1161493c-d64b-4ede-b29f-3ebb5f932dbd"
                          alt="memorialprofile"
                          // className="img-fluid"
                          width={169}
                          height={200}
                        />
                        <div className="position-absolute top-50 start-50 translate-middle">
                          <Image
                            src={url}
                            alt="memorialprofile"
                            // className="img-fluid"
                            width={170}
                            height={200}
                          />
                        </div>
                      </div>
                      <button
                        type="button"
                        className="btn btn-sm system-outline-primary font-bold px-1 px-sm-4 m-5"
                        onClick={() => saveFrame()}
                      >
                        {getLabel("save", initialLanguage)}
                      </button>
                    </div>
                  </div>
                </div>
                <div className="col-4">
                  <div>
                    <h6>{getLabel("selectFrame", initialLanguage)}</h6>
                    <div style={{ overflowY: "scroll", maxHeight: "400px" }}>
                      <div className="d-flex flex-wrap gap-2 gap-sm-3 mb-5 justify-content-start">
                        {frameData?.map((item: any) => {
                          return (
                            <Image
                              src={item?.thumb_url}
                              alt="memorialprofile"
                              className="img-fluid pointer"
                              width={200}
                              height={100}
                              onClick={() => {
                                handleImageClick(item?.thumb_url),
                                  setFrameId(item?.id);
                              }}
                            />
                          );
                        })}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal.Body>
        {btnShow && (
          <Modal.Footer>
            <Button variant="secondary">
              {getLabel("submit", initialLanguage)}
            </Button>
          </Modal.Footer>
        )}
      </Modal>
      <div className="row ps-lg-3 ps-md-3 ps-sm-2 p-1 system-text-dark-light">
        <div className="col-md-9 p-0 col-sm-12 d-md-flex align-items-center">
          {Data?.loading ? (
            <>
              <div className="ps-md-3 d-flex align-items-center justify-content-center">
                <div className="main-profile-container position-relative  rounded-circle border border-3 border-white text-muted font-220 system-text-dark-light d-flex align-items-center justify-content-center">
                  {Data && Data.photoUrl ? (
                    <CustomImage
                      src={Data?.photoUrl}
                      alt=""
                      className="image rounded-circle"
                      ref={imgRef}
                      width={200}
                      height={200}
                      divClass="main-profile-container"
                    />
                  ) : Data && Data.full_name ? (
                    Data?.full_name[0]
                  ) : (
                    ""
                  )}

                  {/* <Image
                    src={Data.photoUrl ? Data.photoUrl : memorialImage}
                    alt="memorialprofile"
                    className="rounded-circle image"
                    width={200}
                    height={200}
                  /> */}
                  <Popover
                    trigger="click"
                    position="right"
                    className={
                      popoverShow ? "d-none" : "d-block custom-dropdown"
                    }
                    content={
                      <div className="popover-content">
                        <div onClick={() => setPopoverShow(true)}>
                          <ImageNewUploder
                            name={`${getLabel("uploadNew", initialLanguage)}`}
                            headerText="Upload your profile here"
                            textOnly={true}
                            handleUploadImage={handleUploadImage}
                          />
                        </div>

                        <li
                          className="rounded font-110 font-normal px-sm-4"
                          onClick={() => {
                            handleShow();
                            setPopoverShow(true);
                          }}
                        >
                          {getLabel("setPhotoFrame", initialLanguage)}
                        </li>
                        <li
                          className="rounded font-110 font-normal px-sm-4"
                          onClick={() => {
                            deletePhoto();
                            // setPopoverShow(true);
                          }}
                        >
                          {getLabel("delete", initialLanguage)}
                        </li>
                      </div>
                    }
                  >
                    {/* {routerSpath !== "photos-and-videos" ? (
                      <>
                        <div></div>
                      </>
                    ) : (
                      <>
                        <div className="bg-white rounded-circle position-absolute end-0 top-50 cam-container pointer">
                          <Image
                            src={upload_camera}
                            alt="camera"
                            className="image rounded-circle"
                            onClick={() => {
                              setPopoverShow(false);
                            }}
                          />
                        </div>
                      </>
                    )} */}

                    {routerSpath === "photos-and-videos" ? (
                      <div className="bg-white rounded-circle position-absolute end-0 top-50 cam-container pointer">
                        <Image
                          src={upload_camera}
                          alt="camera"
                          className="image rounded-circle"
                          onClick={() => setPopoverShow(false)}
                        />
                      </div>
                    ) : (
                      <>
                        <div></div>
                      </>
                    )}
                  </Popover>
                </div>
              </div>
              <div
                style={{ maxWidth: "100%" }}
                className="px-md-3 w-100 d-flex align-items-center justify-content-md-start justify-content-center text-center text-md-start mb-3 mb-md-0"
              >
                <div className="w-100 ">
                  <h5 className="m-0 system-text-blue font-170 font-bold text-truncate-mobile pe-0 me-0 pe-md-5 me-md-5 line-height-2">
                    {Data?.abbreviation} {Data?.["full_name"]}
                  </h5>
                  {/* <p className="m-0 font-90 system-text-blue text-truncate-mobile line-height-2">
                  {Data?.["nick_name"]}
                </p> */}
                  {Data?.nick_name ? (
                    <p className="m-0 font-90 system-text-blue text-truncate line-height-2">
                      {Data?.nick_name}
                    </p>
                  ) : (
                    <a
                      onClick={() => {
                        setShowProfileBanner(true);
                      }}
                      className="font-90 system-text-blue-two line-height-2 pointer"
                    >
                      {getLabel("addNickName", initialLanguage)}
                    </a>
                  )}
                  {/* <p className="m-0 font-90 system-text-blue line-height-2">
                  {Data?.dod} -
                  <span>
                    <a
                      onClick={() => {
                        setShowProfileBanner(true);
                      }}
                      className="font-90 system-text-blue-two  ms-1 line-height-2 pointer"
                    >
                      Add death date
                    </a>
                  </span>
                </p> */}
                  <div className="d-flex justify-content-center justify-content-md-start">
                    <p className="m-0 font-90 system-text-blue text-truncate line-height-2">
                      {MonthDate(Data?.dob)}
                    </p>
                    {Data?.dod ? (
                      <p className="p-0 m-0">
                        <span className="px-1">-</span>
                        <span className="m-0 font-90 system-text-blue text-truncate line-height-2">
                          {MonthDate(Data?.dod)}
                        </span>
                      </p>
                    ) : (
                      <a
                        onClick={() => {
                          setShowProfileBanner(true);
                        }}
                        className="font-90 system-text-blue-two line-height-2 pointer"
                      >
                        {getLabel("addDeathDate", initialLanguage)}
                      </a>
                    )}
                  </div>

                  {/* <p className="m-0 font-90 system-text-blue text-truncate line-height-2">
                  {Data?.["death_location"]}
                </p> */}
                  {Data?.death_location ? (
                    <p className="m-0 font-90 system-text-blue text-truncate line-height-2">
                      {Data?.death_location}
                    </p>
                  ) : (
                    <a
                      onClick={() => {
                        setShowProfileBanner(true);
                      }}
                      className="font-90 system-text-blue-two line-height-2 pointer"
                    >
                      {getLabel("addDeathLocation", initialLanguage)}
                    </a>
                  )}
                </div>
              </div>
            </>
          ) : (
            <ProfileBannerSkelton />
          )}
        </div>
        <FullScreenModal
          show={ShowProfileBanner}
          onClose={() => {
            setShowProfileBanner(false);
          }}
          title={`${getLabel("editPage", initialLanguage)}`}
          cusSize={"custom-size-3"}
          footer={
            <Button
              className="btn-system-primary pointer"
              onClick={handleClick}
              disabled={buttonDisabled}
            >
              {getLabel("submit", initialLanguage)}
            </Button>
          }
          deleteLabel={
            <a
              onClick={() => {
                setShowProfileBanner(false);
              }}
            >
              {getLabel("close", initialLanguage)}
            </a>
          }
        >
          <EditInfo ref={childModalRef} onCloseModal={handleActionSuccess} />
        </FullScreenModal>
        {Data?.full_name ? (
          <div className="col-md-3 col-sm-12 d-flex justify-content-md-end justify-content-center mt-md-2 mb-3 mb-md-0">
            <div
              onClick={() => {
                setShowProfileBanner(true);
              }}
            >
              <a className="btn btn-dark">
                {getLabel("editInfo", initialLanguage)}
              </a>
            </div>
          </div>
        ) : (
          <div className="col-md-3 col-sm-12 d-flex justify-content-md-end justify-content-center mt-md-2 mb-3 mb-md-0 ">
            <div>
              <button className="btn shimmer-button10 "></button>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};
export default ProfileBanner;
