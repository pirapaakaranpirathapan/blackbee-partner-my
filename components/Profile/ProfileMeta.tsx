import Link from "next/link";
import React, { useContext } from "react";
import ProfileMoreDropdown from "../Elements/ProfileMoreDropdown";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
import { useRouter } from "next/router";
import ProfileMetaSkelton from "../Skelton/ProfileMetaSkelton";
import { getLabel } from "@/utils/lang-manager";

const ProfileMeta = () => {
  const { handleSave, NoticeData } = useContext(NoticeDataContext);
  const { PageData } = useContext(PageDataContext);
  const Data = NoticeData || PageData;
  const dataType = (NoticeData && "notices") || (PageData && "memorials");
  const initialLanguage = NoticeData?.initialLanguage || PageData?.initialLanguage
  const router = useRouter();
  const { query } = router;
  const path = router.pathname;
  const allowedPaths = ['/notices/[id]'];
  const isActiveTab = allowedPaths.includes(path);

  const getHref = () => {
    if (dataType === 'memorials') {
      return `/${dataType}/${query?.id}?t=about&s=page-manager#`;
    } else if (dataType === 'notices') {
      return `/${dataType}/${query?.id}?t=home&s=Customer+%26+Manager#`;
    } else {
      return '';
    }
  };


  return (
    <div>
      {Data.loading ? (
        <div className="row py-3 m-0">
          <div className="col-md-2 col-3">
            <p className="m-0 font-90 system-text-blue-light1 text-truncate">
              {getLabel("status", initialLanguage)}
            </p>
            <p className={`m-0 font-normal font-100 ${Data?.Status ? "system-text-success" : "text-danger"}`}
            >
              {Data?.Status || "-"}
            </p>
          </div>
          {allowedPaths.includes(path) ? (
            < div className="col-md-2 col-4 ps-0">
              <p className="m-0 font-90 system-text-blue-light1 text-truncate">
                {getLabel("pageFor", initialLanguage)}
              </p>
              <p className="m-0 font-100 text-truncate system-text-blue font-normal ">
                {Data?.Page_for || "-"}
              </p>
            </div>
          ) : (
            < div className="col-md-2 col-4 ps-0">
              <p className="m-0 font-90 system-text-blue-light1 text-truncate">
                {getLabel("pageFor", initialLanguage)}
              </p>
              <p className="m-0 font-100 text-truncate system-text-blue font-normal ">
                {Data?.Page_for || "-"}
              </p>
            </div>
          )}
          <div className="col-md-2 col-3 ps-0">
            <p className="m-0 font-90 system-text-blue-light1 text-truncate">
              {getLabel("country", initialLanguage)}
            </p>
            <p className="m-0 font-100 text-truncate system-text-blue font-normal">

              {Data?.client_country || "-"}
            </p>
          </div>
          <div className="col-md-2 p-0 d-none d-md-block">
            <p className="m-0 font-90 system-text-blue-light1 text-truncate">
              {getLabel("visibility", initialLanguage)}
            </p>
            <p className="m-0 font-100 text-truncate system-text-blue font-normal">
              {Data?.visibility_name || "-"}
            </p>
          </div>
          <div className="col-md-2 d-none d-md-block">
            <p className="m-0 font-90 system-text-blue-light1 text-truncate">
              {getLabel("pageManager", initialLanguage)}
            </p>
            <a href={getHref()} className="text-decoration-none">
              <p className="m-0 font-100 text-truncate font-normal">
                {Data?.page_manager ? Data?.page_manager : "-"}
              </p>
            </a>
          </div>
          {allowedPaths.includes(path) ? (
            <div className="col-md-2 col-2 d-flex justify-content-end ">
              <ProfileMoreDropdown />
            </div>
          ) : (
            <div className="col-md-2 col-2 d-flex justify-content-end ">
              <ProfileMoreDropdown />
            </div>
          )}
          <div />
        </div>
      ) : (
        <ProfileMetaSkelton />
      )
      }
    </div >
  );
};

export default ProfileMeta;
