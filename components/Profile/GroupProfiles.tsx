import React, { useContext, useEffect, useRef, useState } from "react";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
import { useRouter } from "next/router";
import AlertModal from "../Elements/AlertModal";
import { Notification } from "@arco-design/web-react";
import {
  createMemorialPage,
  getAllMemorialPages,
  memorialPageSelector,
  patchPageDesign,
} from "@/redux/memorial-pages";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import { patchNoticeGroupPage } from "@/redux/notices";
import { getLabel } from "@/utils/lang-manager";
import FullScreenModal from "../Elements/FullScreenModal";
import {
  LabeledValue,
  OptionInfo,
} from "@arco-design/web-react/es/Select/interface";
import MemorialCreatePage from "../Modal/MemorialCreatePage";
import { integer } from "aws-sdk/clients/cloudfront";
import { Button, Dropdown, Menu } from "@arco-design/web-react";
import { IconDown } from "@arco-design/web-react/icon";
import CustomImage from "../Elements/CustomImage";
import ArcoNextTabs from "../Design/ArcoNextTabs";
import {
  getAllPageTemplateFrames,
  pagetemplatesFrameSelector,
} from "@/redux/page-templates-frames";
import GroupAddPages from "../Cards/GroupAddPages";

const GroupProfiles = () => {
  const [addPages, setAddPages] = useState(false);
  const [selectedValues, setSelectedValues] = useState<string[]>([]);
  const [groupId, setGroupId] = useState<string[]>([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [defaultNames, setDefaultNames] = useState<string[]>([]);
  const [response, setResponse] = useState("");
  const [response1, setResponse1] = useState("");
  const [addSearchValue, setAddSearchValue] = useState("");
  const [addFrame, setAddFrame] = useState(false);
  const [frameData, setFrameData] = useState<{
    frameId?: any;
    pageTemplateId?: { id: any; uuid: any };
    uuid?: any;
  }>({});
  const [isLoading, setLoading] = useState(false);
  const { NoticeData, handleSave } = useContext(NoticeDataContext);
  const Data = NoticeData.pages;
  const initialLanguage = NoticeData?.initialLanguage;
  const data = NoticeData;
  const router = useRouter();
  const dispatch = useAppDispatch();
  const actionBtnRef = useRef<HTMLButtonElement>(null);
  const active_id = NoticeData?.uuid;
  const { data: memorialData } = useAppSelector(memorialPageSelector);
  const { data: showTemplatesFramePageData } = useAppSelector(pagetemplatesFrameSelector);

  useEffect(() => {
    const uuidsArray =
      Array.isArray(Data) && Data.map((item: { uuid: any }) => item.uuid);
    if (uuidsArray) {
      setGroupId(uuidsArray);
    }
  }, [Data]);

  const loadData = () => {
    dispatch(
      getAllMemorialPages({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        page: 1,
      })
    ).then(() => { });
  };

  useEffect(() => {
    loadData();
  }, []);

  const handlePage = (uuid: any) => {
    router.push(`/memorials/${uuid}`);
  };

  const handleOptionChange = (selectedValues: string[]) => {
    if (selectedValues.length === 0) {
      setSelectedValues([]);
      return;
    }
    const updatedSelectedValues = [...selectedValues, ...groupId];
    setSelectedValues(updatedSelectedValues);
  };

  const handleUpdatePage = () => {
    const updateData = {
      pages: selectedValues,
    };
    dispatch(
      patchNoticeGroupPage({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_id: active_id,
        updatedData: updateData,
      })
    ).then(async (response: any) => {
      setResponse1(response);
      if (response.payload.success == true) {
        handleSave();
        dispatch(
          getAllMemorialPages({
            active_partner: ACTIVE_PARTNER,
            active_service_provider: ACTIVE_SERVICE_PROVIDER,
            page: 1,
          })
        );
        Notification.success({
          title: "Successfully updated",
          duration: 3000,
          content: undefined,
        });
        setAddPages(false);
      } else {
        Notification.error({
          title: "Failed",
          duration: 3000,
          content: undefined,
        });
      }
    });
  };

  const handleCreate = (
    personName: string,
    salutation: integer,
    clientId: string,
    countryId: string,
    pageTypeId: string,
    relationship: string,
    dod: string,
    dob: string,
    deathPlace: string
  ) => {
    createPage(
      personName,
      salutation,
      clientId,
      countryId,
      pageTypeId,
      relationship,
      dod,
      dob,
      deathPlace
    );
  };
  const createPage = (
    personName: any,
    salutation: integer,
    clientId: any,
    countryId: any,
    pageTypeId: string,
    relationship: any,
    dod: any,
    dob: any,
    deathPlace: any
  ) => {
    const CreateData = {
      name: personName,
      page_type_id: pageTypeId,
      salutation_id: salutation,
      client_id: clientId,
      relationship_id: relationship,
      dod: dod,
      dob: dob,
      death_location: deathPlace,
      country_id: countryId,
    };
    dispatch(
      createMemorialPage({
        activePartner: ACTIVE_PARTNER,
        activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
        CreateData,
      })
    ).then((response: any) => {
      dispatch(
        getAllMemorialPages({
          active_partner: ACTIVE_PARTNER,
          active_service_provider: ACTIVE_SERVICE_PROVIDER,
          page: 1,
        })
      );
      setResponse(response);
      if (response.payload.success === true) {
        const newMemorialUuid = response.payload?.data?.data?.uuid;
        const createuuid = [newMemorialUuid, ...selectedValues];
        const createUuidUniqueArray = [...createuuid, ...groupId];
        setSelectedValues(createUuidUniqueArray);
        const newName = response.payload?.data?.data?.name;
        const updatedDefaultNames = [newName, ...defaultNames];
        setDefaultNames(updatedDefaultNames);
        setIsModalVisible(false);
        Notification.success({
          title: "Memorial has been created successfully",
          duration: 3000,
          content: undefined,
          style: {
            zIndex: 1000,
          },
        });
      } else {
        (response.payload.code != 422) && Notification.error({
          title: "Failed to  create memorial",
          duration: 3000,
          content: undefined,
          style: {
            zIndex: 1000,
          },
        });
      }
    });
  };

  function handleClear(visible: boolean): void {
    if (visible) {
      setSelectedValues([]);
    }
  }

  function handleDeselect(
    value: string | number | LabeledValue,
    option: OptionInfo
  ): void {
    setSelectedValues((prevSelectedValues) => {
      const updatedSelectedValues = prevSelectedValues.filter(
        (uuid) => uuid !== value
      );
      const updatedWithGroupIds = [...updatedSelectedValues, ...groupId];
      const uniqueValues = Array.from(new Set(updatedWithGroupIds));
      if (uniqueValues.every((uuid) => groupId.includes(uuid))) {
        return [];
      }
      return uniqueValues;
    });
  }

  const updateAddSearchValue = (value: React.SetStateAction<string>) => {
    setAddSearchValue(value);
  };

  const handleDeselectValue = (
    value: string | number | LabeledValue,
    option: OptionInfo
  ) => {
    const storedMemorialData = localStorage.getItem("memorialData");
    if (storedMemorialData) {
      let memorialDataArray = JSON.parse(storedMemorialData);
      memorialDataArray = memorialDataArray.filter(
        (item: { name: string }) => item.name !== value
      );
      localStorage.setItem("memorialData", JSON.stringify(memorialDataArray));
    }
    setDefaultNames((prevNames) => prevNames.filter((name) => name !== value));
  };

  const frameSelection = (item: {
    frame: { id: any };
    page_template: { id: any; uuid: any };
    uuid: { id: any };
  }) => {
    setAddFrame(true);
    const loadFrameData = async () => {
      dispatch(
        getAllPageTemplateFrames({
          active_partner: ACTIVE_PARTNER,
          active_page_template: item.page_template.uuid,
        })
      );
    };
    loadFrameData();
    const filteredFrame = {
      frameId: item.frame.id,
      pageTemplateId: item.page_template.id,
      uuid: item.uuid,
    };
    setFrameData(filteredFrame);
  };

  const handleFrameUpdate = (Frameid: any) => {
    setLoading(true);
    const updatedData = {
      frame_id: Frameid,
    };
    dispatch(
      patchPageDesign({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        updatedData,
        active_page: frameData.uuid as unknown as string,
      })
    ).then((response: any) => {
      setLoading(!Data?.loading ? true : false);
      if (response?.payload?.success == true) {
        handleSave();
        Notification.success({
          title: "Frame Updated Successfully",
          duration: 3000,
          content: undefined,
        });
      } else {
        Notification.error({
          title: "Frame Updated Successfully",
          duration: 3000,
          content: undefined,
        });
      }
    });
  };

  return (
    <div>
      <FullScreenModal
        show={addFrame}
        size="xl"
        title={`${getLabel("frames", initialLanguage)}`}
        description={`${getLabel("templatesDescription", initialLanguage)}`}
        onClose={() => setAddFrame(false)}
      >
        <ArcoNextTabs
          Data={showTemplatesFramePageData}
          defaultOptionId={frameData?.frameId}
          onOptionSelect={handleFrameUpdate}
          buttonText={"Apply"}
        />
      </FullScreenModal>
      <FullScreenModal
        show={isModalVisible}
        onClose={() => {
          setIsModalVisible(false);
        }}
        cusSize={"custom-size-3"}
        title={`${getLabel("createMemorialHeading", initialLanguage)}`}
        description={`${getLabel(
          "createMemorialHeadingDescription",
          initialLanguage
        )}`}
        footer={
          <Button ref={actionBtnRef} className="btn-system-primary">
            {getLabel("create", initialLanguage)}
          </Button>
        }
      >
        <MemorialCreatePage
          response={response}
          actionBtnRef={actionBtnRef}
          handleCreate={handleCreate}
          addSearchValue={addSearchValue}
        />
      </FullScreenModal>
      <div className="my-4 row m-0 p-0">
        <AlertModal
          show={addPages}
          onClose={() => {
            setAddPages(false);
          }}
          title="Add memorial page with this notice"
          description="You can edit this information later as well"
          position="centered"
        >
          <>
            <GroupAddPages
              memorialData={memorialData}
              handleOptionChange={handleOptionChange}
              setIsModalVisible={setIsModalVisible}
              isModalVisible={isModalVisible}
              defaultNames={defaultNames}
              updateAddSearchValue={updateAddSearchValue}
              handleUpdatePage={handleUpdatePage}
              handleClear={handleClear}
              handleDeselect={handleDeselect}
              handleDeselectValue={handleDeselectValue}
              response1={response1}
            />
          </>
        </AlertModal>
        {NoticeData.loading ? (
          <>
            {Array.isArray(Data) &&
              Data.map((item: any) => (
                <div className="col-12 col-sm-6 col-md-4 col-lg-3 py-2 text-center d-flex flex-column justify-content-center align-items-center">
                  <div className="group-add-container d-flex align-items-center justify-content-center font-220 text-muted system-text-dark-light rounded-circle">
                    {item && item.photo_url ? (
                      <CustomImage
                        src={item.photo_url}
                        alt=""
                        className="image rounded-circle"
                        width={200}
                        height={200}
                        divClass="group-add-container rounded-circle"
                      />
                    ) : item && item.name ? (
                      <div className="rounded-circle">{`${item?.name[0]}`}</div>
                    ) : (
                      ""
                    )}
                  </div>
                  <div className=" mt-1 grp-select group-drop">
                    <h3 className="m-0 p-0 py-1 font-80 font-bold">
                      {item?.name}
                    </h3>
                    <p className="m-0 p-0 font-80 font-normal pb-1">
                      {item?.dob}
                    </p>
                    <Dropdown.Button
                      className="system-twoside-primary "
                      type="primary"
                      trigger="click"
                      size="mini"
                      onClick={() => {
                        handlePage(item?.uuid);
                      }}
                      getPopupContainer={(node) => {
                        const container = node.parentElement;
                        if (container) {
                          return container;
                        }
                        return document.body;
                      }}
                      droplist={
                        <Menu>
                          <Menu.Item
                            className={"arco-drop-down-font"}
                            key="1"
                            onClick={() => frameSelection(item)}
                          >
                            Change Frame
                          </Menu.Item>
                          <Menu.Item className={"arco-drop-down-font"} key="2">
                            Remove
                          </Menu.Item>
                        </Menu>
                      }
                      icon={<IconDown />}
                    >
                      Manage
                    </Dropdown.Button>
                  </div>
                </div>
              ))}
            <div className="col-12 col-sm-6 col-md-4 col-lg-3 py-2 text-center d-flex flex-column justify-content-start align-items-center">
              <div
                className="group-add-container system-danger-light-1 rounded-circle d-flex align-items-center justify-content-center pointer"
                onClick={() => {
                  setAddPages(true);
                }}
              >
                <div className="px-3 d-flex align-items-center justify-content-center fw-normal font-80 system-text-primary">
                  Add more Pages
                </div>
              </div>
            </div>
          </>
        ) : (
          // <GroupProfileSkelton loopTimes={8} />
          ""
        )}
      </div>
    </div>
  );
};

export default GroupProfiles;
