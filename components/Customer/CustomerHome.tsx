import React, { useContext, useEffect, useState } from "react";
import CustomerDetails from "./CustomerHome/CustomerDetails";
import IdVerification from "./CustomerHome/IdVerification";
import Services from "./CustomerHome/Services";
import { useRouter } from "next/router";
import { getLabel } from "@/utils/lang-manager";
import { CustomerDataContext } from "@/pages/customers/[id]/CustomerDataContext";

const CustomerHome = ({ callback }: any) => {
  const { handleSave, CustomerData } = useContext(CustomerDataContext);
  const initialLanguage = CustomerData?.initialLanguage

  const changeTab = (val: any) => {
    setActiveTab(val);
  };
  const router = useRouter();
  const { query } = router;
  //---To set tabs----------------------------------------------------------------//
  const handleTabSelect = (key: string) => {
    setActiveTab(key);
    if (key !== null) {
      router.push(
        { pathname: router.pathname, query: { ...query, t: key } },
        undefined,
        { shallow: true }
      );
    }
  };
  const tab = "Customer Details";
  const [activeTab, setActiveTab] = useState("");
  useEffect(() => {
    const handleSetActiveKey = () => {
      if (Array.isArray(query?.t)) {
        setActiveTab(query?.t[0] || "Customer Details");
      } else {
        setActiveTab(query?.t || "Customer Details");
      }
    };
    // Call the function when the component mounts or when the query changes.
    handleSetActiveKey();
  }, [query?.t]);
  return (
    <div>
      <div className="row m-0 rounded mx-md-1 my-4">
        <div className="side-width1 col-xl-3 col-lg-3 col-md-3 col-12 m-0  system-secondary-2 rounded min-ht  p-0">
          <div className="p-3 inpage-sidebar ">
            <div className="scrollmenu">
              <div className="li-hover rounded py-2 ps-2">
                <a
                  href="#"
                  onClick={() => handleTabSelect("Customer Details")}
                  className="m-0 p-0"
                >
                  <div
                    className={
                      activeTab === "Customer Details"
                        ? `active-link-side shadow-md sidelink active font-bold p-0`
                        : `rounded shadow-md sidelink font-semibold p-0`
                    }
                  >
                    <div className="">{getLabel("customerDetails", initialLanguage)}</div>
                  </div>
                </a>
              </div>

              <div className="li-hover rounded py-2 ps-2">
                <a
                  href="#"
                  onClick={() => handleTabSelect("Services")}
                  className="m-0 p-0"
                >
                  <div
                    className={
                      activeTab === "Services"
                        ? `active-link-side shadow-md sidelink active font-bold`
                        : `rounded shadow-md sidelink  font-semibold`
                    }
                  >
                    <div className="">{getLabel("services", initialLanguage)}</div>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className="col-xl-9 col-lg-9 col-md-9 col-12 bg-body pt-3">
          <div className="row ">
            <div className="col-12 m-0 p-0 px-3 mb-3">
              {activeTab === "Customer Details" && (
                <>
                  <CustomerDetails />
                </>
              )}
              {activeTab === "ID Verification" && (
                <>
                  <IdVerification />
                </>
              )}
              {activeTab === "Services" && (
                <>
                  <Services />
                </>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default CustomerHome;
