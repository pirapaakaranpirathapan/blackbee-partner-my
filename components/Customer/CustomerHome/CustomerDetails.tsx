import Loader from "@/components/Loader/Loader";
import CustomerDetailsSkelton from "@/components/Skelton/CustomerDetailsSkelton";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import { CountriesSelector, getAllCountries } from "@/redux/countries";
import {
  customerPageSelector,
  getAllCustomer,
  getOneCustomer,
  updateCustomer,
} from "@/redux/customer";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import { useRouter } from "next/router";
import React, { useContext, useEffect, useState } from "react";
import { Notification } from "@arco-design/web-react";
import { useApiErrorHandling } from "@/components/Others/useApiErrorHandling";
import { gendersSelector, getAllGenders } from "@/redux/genders";
import PhoneNumberInput from "@/components/Others/PhoneNumberInput";
import { getLabel } from "@/utils/lang-manager";
import CustomSelect from "@/components/Arco/CustomSelect";
import { CustomerDataContext } from "@/pages/customers/[id]/CustomerDataContext";

const CustomerDetails = () => {
  const { handleSave, CustomerData } = useContext(CustomerDataContext);
  const data = CustomerData;
  const initialLanguage = CustomerData?.initialLanguage
  const router = useRouter();
  const uuid = router.query?.id;
  const dispatch = useAppDispatch();
  const { error: apiError } = useAppSelector(customerPageSelector);
  const { data: showCountriresData } = useAppSelector(CountriesSelector);
  const { data: gendersData } = useAppSelector(gendersSelector);
  const [isLoading, setIsLoading] = useState(false);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [countryId, setCountryId] = useState("");
  const [email, setEmail] = useState("");
  const [mobile, setMobile] = useState("");
  const [response, setResponse] = useState("");
  const [alternativeNumber, setAlternativeNumber] = useState("");
  const [isERR, setERR] = useState(false);
  // const [genderId, setGenderId] = useState("");

  // const handleGenderChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
  //   const selectedValue = e.target.value;
  //   setGenderId(selectedValue);
  // };

  useEffect(() => {
    if (!isERR) {
      setFirstName(data?.first_name);
      setLastName(data?.last_name);
      setCountryId(data?.countryId);
      setEmail(data?.email);
    }
  }, [CustomerData]);

  useEffect(() => {
    if (!isERR) {
      const default_country_mobile = data?.mobile;
      const default_country_alternative = data?.alternative_phone;
      setMobile(default_country_mobile);
      setAlternativeNumber(default_country_alternative);
    }
  }, [CustomerData]);

  // const loadData = async () => {
  //   await dispatch(
  //     getOneCustomer({
  //       active_partner: ACTIVE_PARTNER,
  //       active_service_provider: ACTIVE_SERVICE_PROVIDER,
  //       client_id: uuid as string,
  //     })
  //   )
  // };

  // useEffect(() => {
  //   loadData();
  // }, [uuid]);

  //update customer
  const handleUpdate = () => {
    const updatedData = {
      first_name: firstName,
      last_name: lastName,
      email: email,
      mobile: mobile,
      alternative_phone: "",
      country_id: countryId,
      gender_id: "3",
    };

    if (alternativeNumber?.length > 5) {
      updatedData.alternative_phone = alternativeNumber;
    } else {
      const alternativeProtectFlag = "";
      updatedData.alternative_phone = alternativeProtectFlag;
    }

    if (uuid) {
      setERR(true);
      // dispatch(getAllCountries()).then((response: any) => {
      //   if (response?.payload?.success) {
      //     setIsLoading(false);
      //   } else {
      //     setIsLoading(false);
      //   }
      //   setTimeout(() => {
      //     setIsLoading(false);
      //   }, 1000);
      // });

      dispatch(
        updateCustomer({
          active_partner: ACTIVE_PARTNER,
          active_service_provider: ACTIVE_SERVICE_PROVIDER,
          client_id: uuid as string,
          updatedData,
        })
      ).then((response: any) => {
        if (response?.payload?.success === true) {
          handleSave();
          setERR(false);
          Notification.success({
            title: "Customer has been Updated successfully",
            duration: 3000,
            content: undefined,
          });
          setIsLoading(false);
          setResponse(response);
          // loadData()
        } else {
          (response.payload.code != 422) &&    Notification.error({
            title: "customer updated failed",
            duration: 3000,
            content: undefined,
          });
          setIsLoading(false);
        }

        setTimeout(() => {
          setIsLoading(false);
        }, 1000);
      });
      // .then(() => {
      //   dispatch(
      //     getOneCustomer({
      //       active_partner: ACTIVE_PARTNER,
      //       active_service_provider: ACTIVE_SERVICE_PROVIDER,
      //       client_id: uuid as string,
      //     })
      //   ).then((response: any) => {
      //     // console.log("Dispatch action completed", response);

      //     if (response?.payload?.success) {
      //       setIsLoading(false);
      //     } else {
      //       // console.log("Dispatch action failed", response.payload);
      //       setIsLoading(false);
      //     }
      //     setTimeout(() => {
      //       setIsLoading(false);
      //     }, 1000);
      //   });
      //   setIsLoading(true);
      // });
      setIsLoading(true);
    }
  };

  const error = useApiErrorHandling(apiError, response);

  return (
    <div>
      {isLoading || data.isLoading ? (
        <div>
          <CustomerDetailsSkelton />
        </div>
      ) : (
        <div className="px-md-5 ">
          <div className="mb-4">
            <div className="row  align-items-center m-0">
              <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0   font-bold  text-start">
                {getLabel("firstName", initialLanguage)}
                <span className="ps-1">*</span>
              </div>
              <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0">
                <input
                  type="text"
                  className="form-control border border-2 p-2 system-control"
                  placeholder={data?.first_name}
                  value={firstName}
                  onChange={(e) => setFirstName(e.target.value)}
                />
              </div>
            </div>
            {error.first_name && (
              <div className="row  align-items-center m-0">
                <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0"></div>
                <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 text-danger">
                  {error.first_name}
                </div>
              </div>
            )}
          </div>
          <div className="mb-4">
            <div className="row align-items-center m-0">
              <div className=" col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0   font-bold  text-start">
                {getLabel("lastName", initialLanguage)}
              </div>
              <div className=" col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0">
                <input
                  type="text"
                  className="form-control border border-2 p-2 system-control"
                  value={lastName}
                  onChange={(e) => setLastName(e.target.value)}
                />
              </div>
            </div>
            {error.last_name && (
              <div className="row align-items-center m-0">
                <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0"></div>
                <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 text-danger">
                  {error.last_name}
                </div>
              </div>
            )}
          </div>

          <div className="row  align-items-center m-0 mb-4">
            <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0   font-bold  text-start">
              {getLabel("country", initialLanguage)}
              <span className="ps-1">*</span>
            </div>
            <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 page-arco">
              <CustomSelect
                data={showCountriresData}
                placeholderValue={getLabel("selectACountry", initialLanguage)}
                onChange={(value: any) => setCountryId(value)}
                is_search={true}
                defaultValue={countryId}
              />
            </div>
          </div>
          <div className="mb-4">
            <div className="row align-items-center m-0">
              <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0 font-bold text-start">
                {getLabel("email", initialLanguage)}
                <span className="ps-1">*</span>
              </div>
              <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0">
                <input
                  type="text"
                  className="form-control border border-2 p-2 system-control "
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </div>
            </div>
            {error.email && (
              <div className="row  align-items-center m-0">
                <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0"></div>
                <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 text-danger">
                  {error.email}
                </div>
              </div>
            )}
          </div>
          <div className="mb-4">
            <div className="row  align-items-center m-0">
              <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0   font-bold  text-startd">
                {getLabel("mobile", initialLanguage)}
                <span className="ps-1">*</span>
              </div>
              <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0">
                <PhoneNumberInput
                  id="phone"
                  setMobile={setMobile}
                  mobile={mobile}
                  data={data}
                />
              </div>
            </div>
            {error.mobile && (
              <div className="row  align-items-center m-0 ">
                <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0"></div>
                <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 text-danger">
                  {error.mobile}
                </div>
              </div>
            )}
          </div>
          <div className="mb-4">
            <div className="row  align-items-center m-0">
              <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0   font-bold  text-start">
                {getLabel("alternativeNumber", initialLanguage)}
              </div>
              <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 ">
                <PhoneNumberInput
                  id="phoneNumber"
                  mobile={alternativeNumber}
                  setMobile={setAlternativeNumber}
                  data={data}
                />
              </div>
            </div>
            {error.alternative_phone && (
              <div className="row  align-items-center m-0">
                <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0"></div>
                <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 text-danger">
                  {error.alternative_phone}
                </div>
              </div>
            )}
          </div>
          <div className="row  align-items-center m-0 mb-3">
            <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0   font-bold  text-start"></div>
            <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0">
              <div>
                <button
                  className="btn btn-dark rounded font-110"
                  onClick={handleUpdate}
                >
                  {getLabel("update", initialLanguage)}
                </button>
              </div>
            </div>
          </div>
          <div className="row  align-items-center m-0 mb-4">
            <div className="col-xxl-2 col-xl-3 col-lg-3 col-md-3 col-12 p-0   font-bold  text-start"></div>
            <div className="col-xxl-6 col-xl-6 col-lg-9 col-md-9 col-12 p-0 ">
              <p className="text-dark font-90 fw-normal lh-sm">
                {getLabel("customerBottomDescription", initialLanguage)}
              </p>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};
export default CustomerDetails;
