import DateFormatter from "@/components/Utils/DateFormatter";
import ServiceSkelton from "@/components/Skelton/ServiceSkelton";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import { CustomerDataContext } from "@/pages/customers/[id]/CustomerDataContext";
import {
  customerServiceSelector,
  getAllCustomerServices,
} from "@/redux/customer-service";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import { getLabel } from "@/utils/lang-manager";
import { useRouter } from "next/router";
import React, { useContext, useEffect, useState } from "react";
const Services = () => {
  const { handleSave, CustomerData } = useContext(CustomerDataContext);
  const initialLanguage = CustomerData?.initialLanguage
  const router = useRouter();
  const dispatch = useAppDispatch();
  const { data: customerServiceData } = useAppSelector(customerServiceSelector);

  const uuid = router.query?.id;
  const [loading, setLoading] = useState(true);
  const loadData = (uuid: string | string[] | undefined) => {
    if (uuid) {
      // setLoading(true);
      dispatch(
        getAllCustomerServices({
          active_partner: ACTIVE_PARTNER,
          active_service_provider: ACTIVE_SERVICE_PROVIDER,
          client_id: uuid as string,
        })
      ).then(() => {
        setLoading(false);
      });
    }
  };
  const handleViewClick = (productType: any, serviceId: any) => () => {

    const isSingle = true
      ? "s"
      : "g";
    // setResponse1(response);

    // router.push(`/notices/${serviceId}`);
    if (productType === "notice") {
      router.push(`/notices/${serviceId}?g=${isSingle}&t=home&s=Notice+Details`);
    } else if (productType === "page") {
      router.push(`/memorials/${serviceId}`);
    }

  };
  useEffect(() => {
    loadData(uuid);
  }, [uuid]);

  return (
    <div className="pe-md-5">
      <div>
        <div className="page-heading  mb-2">{getLabel("services", initialLanguage)}</div>
        {loading ? (
          <ServiceSkelton loopTimes={9} />
        ) : (
          customerServiceData && (
            <table className="table">
              <tbody className="">
                {customerServiceData?.map((data: any) => {
                  return (
                    <tr className="align-middle ">
                      <td
                        className="col-4 text-start"
                        style={{ maxWidth: "100px" }}
                      >
                        <div
                          className=" font-bold text-truncate"
                          style={{ maxWidth: "100%" }}
                        >
                          {data.name ? data.name : "Unknown Name"}
                        </div>
                        <div className="font-80  system-text-success">
                          Active
                        </div>
                      </td>
                      <td>
                        <div className="text-dark font-80 text-truncate">
                          {data.product_type
                            ? data.product_type
                            : "Unknown Product"}
                        </div>
                        <div className="text-dark font-80 text-truncate">
                          {DateFormatter(data?.created_at)}
                        </div>
                      </td>
                      <td className=" text-end ">
                        <button
                          type="button"
                          className="btn btn-system-light m-1 m-sm-2 px-2 px-sm-4"
                          onClick={handleViewClick(data.product_type, data.uuid)}
                        >
                          {getLabel("view", initialLanguage)}
                        </button>
                      </td>
                    </tr>
                  );
                })}
                {/* <tr className=" align-middle">
              <td className="col-4 text-start" style={{ maxWidth: '100px' }}>
                <div
                  className="font-bold text-truncate"
                  style={{ maxWidth: '100%' }}>
                  Paul Matt
                </div>
                <div className="text-dark font-80 ">Draft</div>
              </td>
              <td>
                <div className="text-dark font-80  ">
                  Notice: Death Announcement
                </div>
                <div className="text-dark font-80 ">12 Nov 2021</div>
              </td>
              <td className=" text-end ">
                <button
                  type="button"
                  className="btn btn-system-light m-1 m-sm-2 px-2 px-sm-4">
                  View
                </button>
              </td>
            </tr>
            <tr className=" align-middle">
              <td className="col-4 text-start" style={{ maxWidth: '100px' }}>
                <div
                  className="font-bold text-truncate"
                  style={{ maxWidth: '100%' }}>
                  Paul Matt
                </div>
                <div className="text-dark font-80 ">Draft</div>
              </td>
              <td>
                <div className="text-dark font-80  ">
                  Notice: Death Announcement
                </div>
                <div className="text-dark font-80 ">12 Nov 2021</div>
              </td>
              <td className=" text-end ">
                <button
                  type="button"
                  className="btn btn-system-light m-1 m-sm-2 px-2 px-sm-4">
                  View
                </button>
              </td>
            </tr> */}
              </tbody>
            </table>
          )
        )}
      </div>
    </div>
  );
};
export default Services;
