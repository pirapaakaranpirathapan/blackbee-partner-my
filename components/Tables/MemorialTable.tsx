import Image from 'next/image';
import React from 'react';
import memorialprofile from '../../public/memorialprofile.png';

const MemorialTable = () => {
  return (
    <div>
      <table className="table table-striped mt-3 ">
        <tbody className="">
          <tr className=" align-baseline">
            <td>
              <Image
                src={memorialprofile}
                alt="memorialprofile"
                className=""
                style={{ width: '52px', height: '52px' }}
              />
            </td>
            <td>Mark</td>
            <td>Otto</td>
            <td className=" text-end ">
              <button
                type="button"
                className=" btn btn-sm border system-secondary-light-thin font-weight-normal  px-4 "
              >
                Filter
              </button>
            </td>
          </tr>
          <tr className=" align-baseline">
            <td>
              <Image
                src={memorialprofile}
                alt="memorialprofile"
                className=""
                style={{ width: '52px', height: '52px' }}
              />
            </td>
            <td>Mark</td>
            <td>Otto</td>
            <td className=" text-end ">
              <button
                type="button"
                className=" btn btn-sm border system-secondary-light-thin font-weight-normal  px-4 "
              >
                Filter
              </button>
            </td>
          </tr>
          <tr className=" align-baseline">
            <td>
              <Image
                src={memorialprofile}
                alt="memorialprofile"
                className=""
                style={{ width: '52px', height: '52px' }}
              />
            </td>
            <td>Mark</td>
            <td>Otto</td>
            <td className=" text-end ">
              <button
                type="button"
                className=" btn btn-sm border system-secondary-light-thin font-weight-normal  px-4 "
              >
                Filter
              </button>
            </td>
          </tr>
          <tr className=" align-baseline">
            <td>
              <Image
                src={memorialprofile}
                alt="memorialprofile"
                className=""
                style={{ width: '52px', height: '52px' }}
              />
            </td>
            <td>Mark</td>
            <td>Otto</td>
            <td className=" text-end ">
              <button
                type="button"
                className=" btn btn-sm border system-secondary-light-thin font-weight-normal  px-4 "
              >
                Filter
              </button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default MemorialTable;
