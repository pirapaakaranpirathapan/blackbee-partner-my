const extractFilenameFromUrl=(url:any)=> {
    const decodedUrl = decodeURIComponent(url);
    const filename = decodedUrl.substring(decodedUrl.lastIndexOf('/') + 1, decodedUrl.lastIndexOf('.'));
    return filename;
  }

  export default extractFilenameFromUrl;