const extractDate = (timestamp: string): string => {
    const parsed_time = new Date(timestamp);
  
    const year = parsed_time.getFullYear();
    const month = parsed_time.getMonth() + 1; // months are zero-based, so adding 1
    const day = parsed_time.getDate();
  
    return `${year}-${month < 10 ? '0' : ''}${month}-${day < 10 ? '0' : ''}${day}`;
  };
  
  export default extractDate;
  