export const MonthDate = (timestamp: string): string => {
  try {
    const date = new Date(timestamp);
    return new Intl.DateTimeFormat("en-GB", {
      day: "numeric",
      month: "short",
      year: "numeric",
      // hour: "numeric",
      // minute: "2-digit",
      // timeZone: "GMT",
    }).format(date);
    // + " GMT"
  } catch (error) {
    console.error("Invalid date format", error);
    return " ";
  }
  };