// import React from "react";

// export default function TimeConverter(props: { time: string }) {
//   const { time } = props;
//   const timestamp = time;
//   const dateObj = new Date(timestamp);

//   const year = dateObj.getFullYear();
//   const month = dateObj.getMonth() + 1;
//   const day = dateObj.getDate();

//   const formattedDate = `${year}-${month.toString().padStart(2, "0")}-${day
//     .toString()
//     .padStart(2, "0")}`;

//   return <div>{formattedDate}</div>;
// }

import React from "react";

interface TimeConverterProps {
  time: string;
}

const months: string[] = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];

const TimeConverter: React.FC<TimeConverterProps> = ({ time }) => {
  const dateObj = new Date(time);

  const year = dateObj.getFullYear();
  const monthIndex = dateObj.getMonth();
  const month = months[monthIndex];
  const day = dateObj.getDate().toString().padStart(2, "0");

  const formattedDate = `${year}-${month}-${day}`;

  return <div>{formattedDate}</div>;
};

export default TimeConverter;
