// export const timeagoformatter = (timestamp: string): string => {
//     const currentTimestamp: Date = new Date();
//     const providedTimestamp: Date = new Date(timestamp);
//     const differentInSeconds: number = Math.floor((currentTimestamp.getTime() - providedTimestamp.getTime()) / 1000);
  
//     const timeUnits = [
//       { unit: "year", value: Math.floor(differentInSeconds / 31536000) }, // 365 * 24 * 60 * 60 = 31536000
//       { unit: "month", value: Math.floor(differentInSeconds / 2592000) }, // 30 * 24 * 60 * 60 = 2592000
//       { unit: "day", value: Math.floor(differentInSeconds / 86400) }, // 24 * 60 * 60 = 86400
//       { unit: "hour", value: Math.floor(differentInSeconds / 3600) }, // 60 * 60 = 3600
//       { unit: "minute", value: Math.floor(differentInSeconds / 60) },
//       { unit: "second", value: differentInSeconds },
//     ];
  
//     const getTimeAgo = (value: number, unit: string): string =>
//       `${value} ${unit}${value !== 1 ? "s" : ""} ago`;
  
//     const timeAgo = timeUnits.find((unit) => unit.value >= 1);
//     let result = timeAgo ? getTimeAgo(timeAgo.value, timeAgo.unit) : "Just now";
  
//     return result;
//   };
export const timeagoformatter = (timestamp: string): string => {
  const currentTimestamp = new Date();
  const providedTimestamp = new Date(timestamp);
  const secondsAgo = Math.floor((currentTimestamp.getTime() - providedTimestamp.getTime()) / 1000);

  const timeUnits = [
    { unit: "year", divisor: 31536000, symbol: "yr" }, // 365 * 24 * 60 * 60 = 31536000 seconds
    { unit: "month", divisor: 2592000, symbol: "mo" }, // 30 * 24 * 60 * 60 = 2592000 seconds
    { unit: "day", divisor: 86400, symbol: "d" }, // 24 * 60 * 60 = 86400 seconds
    { unit: "hour", divisor: 3600, symbol: "hr" }, // 60 * 60 = 3600 seconds
    { unit: "minute", divisor: 60, symbol: "mins" }, // 60 seconds
    { unit: "second", divisor: 1, symbol: "s" }, // 1 second
  ];

  for (const unit of timeUnits) {
    const value = Math.floor(secondsAgo / unit.divisor);
    if (value >= 1) {
      return `${value} ${unit.symbol} ago`;
    }
  }

  return "Just now";
};

  