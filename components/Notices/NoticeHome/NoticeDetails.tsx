import Image from "next/image";
import React, { use, useContext, useState } from "react";
import templates from "../../../public/templates.svg";
import framDefault from "../../../public/frame.jpg";
import camera from "../../../public/camera.svg";
import drag from "../../../public/drag.svg";
import question from "../../../public/question.svg";
import closebtn from "../../../public/closebtn.svg";
import edit from "../../../public/edit.svg";
import draggable from "../../../public/draggable.svg";
import MemorialContactCard from "@/components/Cards/MemorialContactCard";
import uploadphotos from "../../../public/uploadphotos.svg";
import greendot from "../../../public/greendot.svg";
import reddot from "../../../public/reddot.svg";
import FullScreenModal from "@/components/Elements/FullScreenModal";
import AddNewContact from "../../Modal/AddNewContact";
import { Button } from "react-bootstrap";
import AddInformant from "../../Modal/AddInformant";
import AddRelationship from "../../Modal/AddRelationship";
import AddNewEvent from "../../Modal/AddNewEvent";
import { useRouter } from "next/router";
import trash from "../../../public/trash.svg";
import { supportedLanguages } from "@/config/noticeLanguges/languages";
import dayjs from "dayjs";
import {
  createNoticeContact,
  getAllNoticesContact,
  noticeContactSelector,
} from "@/redux/notice-contact";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
import {
  createEvent,
  deleteEvent,
  noticeSelector,
  patchLiveStreamVideo,
  patchNickName,
  patchNotice,
  patchNoticeDetails,
  patchNoticeProfile,
  showNotice,
  updateEvent,
} from "@/redux/notices";
import {
  DatePicker,
  Notification,
  Popover,
  TimePicker,
} from "@arco-design/web-react";
import addMore from "../../../public/add-more.svg";
import Autocomplete from "@/components/Elements/Autofill";
import Templates from "@/components/Design/Templates";
import { useEffect, useRef } from "react";
import {
  createNoticeTitle,
  noticeTitleSelector,
  searchNoticeTitle,
} from "@/redux/notice-titles";
import { templatesSelector } from "@/redux/notice-templates";
import { ImageNewUploder } from "@/components/ImageUploder/imageNewUploder";
import {
  deleteNoticeMedia,
  getMedia,
  mediaNoticeGet,
  mediaNoticeUpdate,
  mediaNoticeUpload,
  mediaSelector,
  updateCaptions,
  updateNoticeCaptions,
} from "@/redux/media";
import { PaginationComponent } from "@/components/Elements/Pagination";
import { integer } from "aws-sdk/clients/cloudfront";
import NoticesContactCard from "@/components/Cards/NoticeContactCard";
import Loader from "@/components/Loader/Loader";
import {
  eventLocationTypesReducer,
  getAllEventLocationTypes,
} from "@/redux/event-location-type";
import Link from "next/link";
import {
  getEvent,
  getOneEvent,
  noticeEventSelector,
} from "@/redux/notices-event";
import TimeConverter from "@/components/TimeConverter/timeconvert";

import NoticeDetailsSkelton from "@/components/Skelton/NoticeDetailsSkelton";
import { MonthDate } from "@/components/TimeConverter/MonthFormat";
import NoticeVidepLiveSkelton from "@/components/Skelton/NoticeVidepLiveSkelton";
import NoticeContactsSkelton from "@/components/Skelton/NoticeContactsSkelton";
import NoticeEventsSkelton from "@/components/Skelton/NoticeEventsSkelton";
import ShimmerEffectText from "@/components/Skelton/ShimmerEffectText";
import PhotosAndVideosSkelton from "@/components/Skelton/PhotosVideosSkelton";
import NoticePhotosSkelton from "@/components/Skelton/NoticePhotosSkelton";
import AlertModal from "@/components/Elements/AlertModal";
import CustomImage from "@/components/Elements/CustomImage";
import { getLabel } from "@/utils/lang-manager";
import ContactSkelton from "@/components/Skelton/ContactSkelton ";
import CustomSelect from "@/components/Arco/CustomSelect";
import MainPagination from "@/components/Paginations";
import tick from "../../../public/tick-circle.svg";
import { EventTypeSelector, getAllEventsTypes } from "@/redux/event-types";
import PlaceAutocomplete from "@/components/Elements/PlaceAutoComplete";
import { Radio } from "@arco-design/web-react";
import UpdateEvent from "@/components/Modal/UpdateEvent";
import ArcoNextTabs from "@/components/Design/ArcoNextTabs";
import { set } from "lodash";
import NoticeHeadingSkelton from "@/components/Skelton/NoticeHeadingSkelton";
import { MonthTimeFormat } from "@/components/TimeConverter/MonthTimeFormat";

interface MediaDataItem {
  id: number;
  url: string;
}
const NoticeDetails = () => {
  const [is_delivery_possible, setIsDeliveryPossible] = useState(true);
  const [status, setStatus] = useState(1);
  const [eventTypeId, seteventTypeId] = useState(1);
  const [eventDate, setEventDate] = useState("");
  const tab = "venue";
  const tabMedia = "All";
  const [activeTab, setActiveTab] = useState(tab);
  const [activeTabMedia, setActiveTabMedia] = useState(tabMedia);
  const [address, setAddress] = useState("");
  const { handleSave, NoticeData } = useContext(NoticeDataContext);
  const initialLanguage = NoticeData?.initialLanguage;
  const [eventTypeSet, seteventTypeSet] = useState(1);
  const dispatch = useAppDispatch();
  const router = useRouter();
  const uuid = router.query?.id;
  const [showAddContact, setShowAddContact] = useState(false);
  const [showAddInformant, SetShowAddInformant] = useState(false);
  const [showEvent, SetshowEvent] = useState(false);
  const [showAddRelationship, setShowAddRelationship] = useState(false);
  const [addNewEvent, setAddNewEvent] = useState(false);
  const [response, setResponse] = useState("");
  const [editResponse, seteditResponse] = useState({});
  const [popoverShow, setPopoverShow] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isvideoLoading, setIsVideoLoading] = useState(false);
  const [isEventsLoading, setIsEventsLoading] = useState(false);
  const [isCaptionLoading, setIsCaptionLoading] = useState(false);
  const [isContactssLoading, setIsContactssLoading] = useState(false);
  const [isPhotosLoading, setIsPhotosLoading] = useState(false);
  const noticeActionBtnRef = useRef<HTMLButtonElement>(null);
  const { data: allContactsData } = useAppSelector(noticeContactSelector);
  const [contactPagenationSkeleton, setcontactPagenationSkeleton] =
    useState(false);
  const [eventPagenationSkeleton, seteventPagenationSkeleton] = useState(false);
  const [submitButtonDisabled, setSubmitButtonDisabled] = useState(false);
  const { data: eventTypeData } = useAppSelector(EventTypeSelector);
  useEffect(() => {
    dispatch(getAllEventsTypes());
    dispatch(getAllEventLocationTypes());
  }, []);
  const [sInstructionLength, setSInstructionLength] = useState(0);
  const [esInstructionLength, seteSInstructionLength] = useState(0);
  const [eventLocationTypeId, setEventLocationTypeId] = useState("1");
  const { data: eventLocationType } = useAppSelector(eventLocationTypesReducer);
  const [eventInstructions, seteventInstructions] = useState("");
  const [instructions, setInstructions] = useState("");
  const [crossBtnEnable, setCrossBtnEnable] = useState(false);
  const [crossBtnEnableId, setCrossBtnEnableId] = useState("");
  const handleUpdate = (
    personName: string,
    countryId: string,
    relationship: string,
    visibilityId: integer,
    auto_hide: boolean,
    contactTypes: any
  ) => {
    createContact(
      personName,
      countryId,
      relationship,
      visibilityId,
      auto_hide,
      contactTypes
    );
  };
  const createContact = (
    personName: string,
    countryId: any,
    relationship: any,
    visibilityId: integer,
    auto_hide: boolean,
    contactTypes: any
  ) => {
    setSubmitButtonDisabled(true);
    const createData = {
      name: personName,
      country_id: countryId,
      relationship_id: relationship,
      contact_details: contactTypes,
      // additional_text: additionalText,
      status: "",
      auto_hide: auto_hide,
      visibility_id: visibilityId,
    };
    // setIsContactssLoading(true);
    console.log("ff", createData);
    dispatch(
      createNoticeContact({
        activePartner: ACTIVE_PARTNER,
        activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
        activeNotice: uuid as unknown as string,
        createData,
      })
    ).then((response: any) => {
      setIsContactssLoading(false);
      if (response.payload.success == true) {
        setResponse(response);
        setSubmitButtonDisabled(false);
        Notification.success({
          title: "Contact Create Successfully",
          duration: 3000,
          content: undefined,
        });
        getAllContacts(1);
        setShowAddContact(false);
      } else {
        setSubmitButtonDisabled(false);
        response.payload.code != 422 &&
          Notification.error({
            title: "Contact Create Failed",
            duration: 3000,
            content: undefined,
          });
      }
    });
  };
  const handleload = () => {
    getAllContacts(1);
  };
  const setAsDefault = (url: string) => {
    dispatch(
      patchNoticeProfile({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_id: uuid as unknown as string,
        updatedData: {
          profile_photo_url: url,
        },
      })
    ).then((response: any) => {
      if (response.payload.success == true) {
        Notification.success({
          title: "Profile photo updated successfully",
          duration: 3000,
          content: undefined,
        });
      } else {
        Notification.error({
          title: "Profile photo update failed",
          duration: 3000,
          content: undefined,
        });
      }
    });
  };
  const { getMediaData: mediaData } = useAppSelector(mediaSelector);
  const { deleteMedia: deleteMediaData } = useAppSelector(mediaSelector);
  const getAllContacts = (page: integer) => {
    setIsContactssLoading(true);
    dispatch(
      getAllNoticesContact({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_notice: uuid as unknown as string,
        page: page,
      })
    ).then(() => {
      setcontactPagenationSkeleton(false);
      setIsContactssLoading(false);
    });
  };
  useEffect(() => {
    getAllContacts(
      localStorage.getItem("notices_contact_page")
        ? parseInt(localStorage.getItem("notices_contact_page") as string, 10)
        : 1
    );
  }, [uuid]);
  const [inputValue, setInputValue] = useState("Add a custom information");
  const [inputValue2, setInputValue2] = useState("Add pre name information");
  const handleChange = (event: any) => {
    setInputValue(event.target.value);
    setInputValue2(event.target.value);
  };
  //--------------------------------------MainContent(JSON)**------------------------------------------------------------------------------------------------------//
  const [isEditingm, setIsEditingm] = useState(false);
  const [MainContent, setMainContent] = useState(
    "Jeyakanthan Vasanthan Joseph was born in Jaffna, Sri Lanka and lived in Amilly, France and passed away peacefully on 29th December 2022."
  );
  const [noticeTitleId, setNoticeTitleId] = useState(
    NoticeData?.["notice_title_id"]
  );
  const [postTitleId, setPostTitleId] = useState(false);
  //template
  const [addTemplate, setAddTemplate] = useState(false);
  const [clickedOptionId, setClickedOptionId] = useState(
    NoticeData?.["notice_template_id"]
  );
  const [clickedOptionLink, setClickedOptionLink] = useState(
    NoticeData?.["notice_template"]
  );
  const [clickedOptionAlt, setClickedOptionAlt] = useState(
    NoticeData?.["notice_template_alt"]
  );
  const [selectedOptionId, setSelectedOptionId] = useState(
    NoticeData?.["notice_template_id"]
  );
  const [selectedOptionLink, setSelectedOptionLink] = useState(
    NoticeData?.["notice_template"]
  );
  const [selectedOptionAlt, setSelectedOptionAlt] = useState(
    NoticeData?.["notice_template_alt"]
  );
  const [langFullName, setLangFullName] = useState(
    NoticeData?.["full_lang_name"]
  );
  const handleEditClick = () => {
    if (isEditingm) {
      handleUpdates(); // Call the updateInfo function when "Save" button is clicked
    }
    setIsEditingm(!isEditingm); // Toggle the isEditing state
  };
  const handleTextChange = (
    e: React.ChangeEvent<HTMLTextAreaElement>,
    field: string
  ) => {
    const value = e.target.value;
    switch (field) {
      case "MainContent":
        setMainContent(value);
        break;
      default:
        break;
    }
  };
  const handleUpdates = () => {};
  //---------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //------------------------------------custom--*Notice Page Language*--------To-------*Template*------------------------------------------------------//
  const [isnoticeTitlechange, setisnoticeTitlechange] = useState(true);
  const [language, setLanguage] = useState(
    parseInt(NoticeData?.["language_id"], 10)
  );
  const [selectedLanguage, setSelectedLanguage] = useState("");
  const [isLanguageSupported, setIsLanguageSupported] = useState(false);
  const [loading, setLoading] = useState(true);
  const [showRequired, setShowRequired] = useState(false);
  const [langFullNamec, setLangFullNamec] = useState("");
  // const [activePage, setActivePage] = useState(NoticeData?.["active_page"]);
  const [update, setUpdate] = useState(true);

  useEffect(() => {
    isnoticeTitlechange && setLangFullName(NoticeData?.["full_lang_name"]);
    isnoticeTitlechange &&
      setLanguage(parseInt(NoticeData?.["language_id"], 10));
    isnoticeTitlechange && setNoticeTitle(NoticeData?.["notice_title"]);
    isnoticeTitlechange && setNickName(NoticeData?.["nick_name"]);
    setSelectedOptionLink(NoticeData?.["notice_template"]);
    setSelectedOptionAlt(NoticeData?.["notice_template_alt"]);
    setNoticeTitleId(NoticeData?.["notice_title_id"]);
    setSelectedOptionId(NoticeData?.notice_template_id);
  }, [NoticeData]);

  useEffect(() => {
    const selectedLanguageObject = NoticeData?.languages.find(
      (lang: any) => lang.id === language
    );
    if (selectedLanguageObject) {
      setSelectedLanguage(selectedLanguageObject.name);
      const languageIsSupported = supportedLanguages.includes(
        selectedLanguageObject.name
      );
      setIsLanguageSupported(languageIsSupported);
      setLoading(false);
    }
  }, [language, NoticeData?.languages, supportedLanguages]);

  //template
  const handleOptionSelection = (
    optionId: number,
    OptionLink: string,
    selectedOptionAlt: string
  ) => {
    setSelectedOptionId(optionId);
    setClickedOptionId(
      optionId ? optionId : NoticeData?.["notice_template_id"]
    );
    setClickedOptionLink(
      optionId ? OptionLink : NoticeData?.["notice_template"]
    );
    setClickedOptionAlt(
      optionId ? selectedOptionAlt : NoticeData?.["notice_template_alt"]
    );
    setAddTemplate(false);
  };

  // const handleOptionSelect = () => {
  //   setSelectedOptionId(clickedOptionId);
  //   setSelectedOptionLink(clickedOptionLink);
  //   setSelectedOptionAlt(clickedOptionAlt);
  //   setAddTemplate(false);
  // };

  const actionBtnRef = useRef<HTMLButtonElement>(null);

  const handleInputChange = (
    e: React.ChangeEvent<HTMLInputElement>,
    field: string
  ) => {
    const value = e.target.value;
    switch (field) {
      case "langFullName":
        setLangFullName(value);
        // setLangFullNamec(value);
        break;
      case "nickName":
        setNickName(value);
        break;
      default:
        break;
    }
  };

  //------------------------------------------------Notice title create SEARCH -----------------------------------------------------------------------------//
  const searchnoticeTitle = async (keyword: string) => {
    await dispatch(
      searchNoticeTitle({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_notice: NoticeData.uuid as unknown as string,
        keywordData: {
          keyword: keyword,
        },
      })
    ).then((response: any) => {
      setNoticeTitledata(resultNoticeTitleresult?.data);
      if (response.payload.success == true) {
      }
    });
  };
  const postnoticeTitle = async (keyword: string) => {
    await dispatch(
      createNoticeTitle({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_notice: NoticeData.uuid as unknown as string,
        postData: {
          name: keyword,
          slug: keyword,
        },
      })
    )
      .then((response: any) => {
        setNoticeTitledata(resultNoticeTitleresult?.data);
        if (response.payload.success == true) {
          setNoticeTitleId(response.payload.data.data.id);
          callUpdateFunction(response.payload.data.data.id);
        } else {
          callUpdateFunction(noticeTitleId);
        }
      })
      .then(() => {
        // callUpdateFunction();
        // setIsLoading(!NoticeData?.loading);
      });
  };
  const { data: resultNoticeTitleresult } = useAppSelector(noticeTitleSelector);
  const [noticeTitle, setNoticeTitle] = useState(NoticeData?.["notice_title"]);
  const [noticeTitledata, setNoticeTitledata] = useState<
    { id: number; name: string }[]
  >([]);

  useEffect(() => {
    if (noticeTitledata && noticeTitledata.length > 0) {
      const inputTitle = noticeTitledata.find(
        (option: { name: string }) =>
          option.name.toLowerCase() === noticeTitle.toLowerCase()
      );
      if (!inputTitle) {
        setPostTitleId(true);
      } else {
        setNoticeTitleId(inputTitle.id);
        setPostTitleId(false);
      }
    } else {
      setPostTitleId(true);
    }
  }, [noticeTitledata]);
  // useEffect(() => {
  //   setNoticeTitleId(noticeTitle || null);
  // }, [noticeTitle]);
  useEffect(() => {
    setNoticeTitledata(resultNoticeTitleresult?.data);
  }, [resultNoticeTitleresult]);
  // useEffect(() => {
  //   setLiveUrl(NoticeData?.live_video);
  //   setVideoUrl(NoticeData?.video_url);
  //   // setNoticeTitleId(NoticeData?.notice_title_id);
  // }, [NoticeData]);
  //------------------------------------------------------------------------*Notice Page Language*--------To-------*Template*-- end----------------------------------------//

  //--------------------------------------------------------------------------Notice title EVENT and suggestion click--------------------------------------------------
  const handleNoticeTitleChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const { value } = event.target;
    setNoticeTitle(value);
    value && setisnoticeTitlechange(false);
    value && searchnoticeTitle(value);
    value && setSuggestion(true);
    if (noticeTitledata && noticeTitledata.length > 0) {
      const inputTitle = noticeTitledata.find(
        (option: { name: string }) =>
          option.name.toLowerCase() === value.toLowerCase()
      );
      if (inputTitle) {
        setPostTitleId(false);
      } else {
        setPostTitleId(true);
      }
    } else {
      // setPostTitleId(true);
    }
  };
  const [suggestion, setSuggestion] = useState(true);
  const handleOptionClick = (option: any, name: any) => {
    setNoticeTitleId((prevId: any) => (prevId === option ? prevId : option));
    setNoticeTitle(name);
    setSuggestion(false);
    setPostTitleId(false);
  };
  const createAndUpdate = () => {
    setIsLoading(true);
    if (isnoticeTitlechange) {
      callUpdateFunction(NoticeData?.["notice_title_id"]);
    } else {
      if (postTitleId) {
        postnoticeTitle(noticeTitle);
      } else {
        callUpdateFunction(noticeTitleId);
      }
    }
  };
  // const isLanguageSupported = supportedLanguages.includes(selectedLanguage);
  //---------------------------------------------------------------NOTICE DETAILS UPDATE--------------------------------------------------------------------------------
  const callUpdateFunction = (tileId: any) => {
    let updatedData = {
      language_id: language,
      lang_name: NoticeData?.is_single ? langFullName : "group name",
      notice_title_id: tileId,
      notice_template_id: selectedOptionId,
    };

    if (isLanguageSupported === false) {
      updatedData.lang_name = NoticeData?.is_single
        ? langFullName
        : "sample Name";
    }
    setisnoticeTitlechange(false);
    dispatch(
      patchNoticeDetails({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        updatedData,
        active_notice: NoticeData.uuid as unknown as string,
      })
    ).then((response: any) => {
      setIsLoading(false);
      if (response.payload.success == true) {
        // dispatch(
        //   showNotice({
        //     active_partner: ACTIVE_PARTNER,
        //     active_service_provider: ACTIVE_SERVICE_PROVIDER,
        //     active_notice: NoticeData.uuid as unknown as string,
        //   })
        // ).then(() => {
        //   setIsLoading(!NoticeData?.loading);
        // });
        setShowRequired(false);
        setSuggestion(false);
        handleSave();
        setisnoticeTitlechange(true);
        Notification.success({
          title: "Updated Successfully",
          duration: 3000,
          content: undefined,
        });
        setUpdate(true);
      } else {
        // searchnoticeTitle(noticeTitle);
        setShowRequired(true);
        // setisnoticeTitlechange(true);
        response.payload.code != 422 &&
          Notification.error({
            title: "Update Failed",
            duration: 3000,
            content: undefined,
          });
        setUpdate(true);
      }
    });
  };
  //-----------------------------------------------------------------live,video url update ----------------------------------------------------------------------------------------------------------------------//
  const [liveUrl, setLiveUrl] = useState("");
  const [videoUrl, setVideoUrl] = useState("");
  const [liveUrlErr, setLiveUrlErr] = useState("");
  const [videoUrlErr, setVideoUrlErr] = useState("");
  const [videoEdit, setVideoEdit] = useState(true);

  useEffect(() => {
    // Update the state when NoticeData changes
    !liveUrlErr &&
      videoEdit &&
      setLiveUrl(NoticeData?.live_video === null ? "" : NoticeData?.live_video);
    !videoUrlErr &&
      videoEdit &&
      setVideoUrl(NoticeData?.video_url === null ? "" : NoticeData?.video_url);
  }, [NoticeData]);
  const handleVideoInputChange = (
    e: React.ChangeEvent<HTMLInputElement>,
    field: string
  ) => {
    const value = e.target.value;
    setVideoEdit(false);
    switch (field) {
      case "liveUrl":
        setLiveUrl(value);
        break;
      case "videoUrl":
        setVideoUrl(value);
        break;
      default:
        break;
    }
  };

  function isValidURL(url: string | undefined | null): boolean {
    return (
      url === null ||
      url === undefined ||
      url === "" ||
      url === "" ||
      /^(https?|ftp):\/\/[^\s/$.?#].[^\s]*$/.test(url)
    );
  }
  const patchLiveStreamVideos = () => {
    setVideoUrlErr("");
    setLiveUrlErr("");
    setIsVideoLoading(true);
    const liveUrlValid = isValidURL(liveUrl);
    const videoUrlValid = isValidURL(videoUrl);
    const updatedData = {
      ...(liveUrlValid && { live_video_url: liveUrl }),
      ...(videoUrlValid && { background_video_url: videoUrl }),
    };
    dispatch(
      patchLiveStreamVideo({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_notice: NoticeData.uuid as unknown as string,
        updatedData,
      })
    ).then((response: any) => {
      // handleSave();
      setIsVideoLoading(false);
      if (response.payload.success == true) {
        if (liveUrlValid && videoUrlValid) {
          setLiveUrl(response?.payload?.data?.live_video_url ?? "");
          setVideoUrl(response?.payload?.data?.background_video_url ?? "");
          Notification.success({
            title:
              "Video URL and Live streaming URL have been updated successfully",
            duration: 3000,
            content: undefined,
          });
        } else if (liveUrlValid) {
          !videoUrlValid && setVideoUrlErr("Invalid Video URL");
          liveUrlValid && setLiveUrlErr("");
          setLiveUrl(response?.payload?.data?.live_video_url ?? "");
          Notification.success({
            title: "Live streaming URL has been updated Successfully",
            duration: 3000,
            content: undefined,
          });
        } else if (videoUrlValid) {
          !liveUrlValid && setLiveUrlErr("Invalid Live streaming URL");
          videoUrlValid && setVideoUrlErr("");
          setVideoUrl(response?.payload?.data?.background_video_url ?? "");
          Notification.success({
            title: "Video URL has been updated Successfully",
            duration: 3000,
            content: undefined,
          });
        } else {
          setLiveUrlErr("Invalid Live streaming URL");
          setVideoUrlErr("Invalid Video URL");
          // Notification.error({
          //   title: " Invalid Live streaming URL and Video URL ",
          //   duration: 3000,
          //   content: undefined,
          // });
        }
        setUpdate(true);
      } else {
        Notification.error({
          title: " Update Failed",
          duration: 3000,
          content: undefined,
        });
        setUpdate(true);
      }
    });
  };

  //-----------------------------------------------------------------nickname single update Notice content(replace with json) -----------------------------------------------------------------------//
  const [nickName, setNickName] = useState(NoticeData?.["nick_name"]);

  const handleNickNameInputChange = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    const value = e.target.value;
    setNickName(value);
  };

  const [isEditing, setIsEditing] = useState(false);
  const [isCaptionEditing, setIsCaptionEditing] = useState(false);
  const ref = useRef<HTMLInputElement | null>(null);

  useEffect(() => {
    if (isCaptionEditing) {
      ref.current?.focus();
    }
  }, [isCaptionEditing]);

  useEffect(() => {
    if (isEditing) {
      ref.current?.focus();
    }
  }, [isEditing]);

  const handleNickName = () => {
    setIsEditing(true);
  };
  type EditingStates = Record<string, boolean>;

  const [editingStates, setEditingStates] = useState<EditingStates>({});

  useEffect(() => {
    getEventLocationTypes;
    getEvents(1);
  }, []);

  const handleNickNameUpdate = () => {
    const updatedData = {
      nickname: nickName,
    };
    if (isEditing) {
      dispatch(
        patchNickName({
          active_partner: ACTIVE_PARTNER,
          active_service_provider: ACTIVE_SERVICE_PROVIDER,
          updatedData,
          active_notice: NoticeData?.["active_page"] as unknown as string,
        })
      ).then((response: any) => {
        if (response.payload.success == true) {
          Notification.success({
            title: "Updated Successfully",
            duration: 3000,
            content: undefined,
          });
          // handleSave();
          setIsEditing(false);
        } else {
          Notification.error({
            title: "Update Failed",
            duration: 3000,
            content: undefined,
          });
        }
      });
    }
  };

  //---------------------------------------------------------------------Event create--------------------------------------------------------------------------------//

  const [eventCreateButtonEnabled, setEventCreateButtonEnabled] =
    useState(false);

  const handleCreate = (
    address: string,
    isDeliveryPossible: boolean,
    setInstructions: string,
    virtualEvent: string,
    eventLocationTypeId: string,
    eventTypeId: string,
    eventInstructions: string,
    status: number
  ) => {
    // setIsEventsLoading(true);
    setEventCreateButtonEnabled(true);
    const createdData = {
      state_id: status,
      virtual_event: virtualEvent,
      address: address,
      is_delivery_possible: isDeliveryPossible,
      instructions: setInstructions,
      event_location_type_id: eventLocationTypeId,
      event_type_id: eventTypeId,
      event_instructions: eventInstructions,
    };
    dispatch(
      createEvent({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        notice_uuid: uuid as unknown as string,
        createdData,
      })
    ).then((response: any) => {
      if (response.payload.success == true) {
        setIsEventsLoading(true);
        setEventCreateButtonEnabled(false);
        Notification.success({
          title: "Event Create Successfully",
          duration: 3000,
          content: undefined,
        });
        setAddNewEvent(false);
        getEvents(1);
        //navigate to event page
        // window.location.href = `/notices`;
      } else {
        setEventCreateButtonEnabled(false);
        response.payload.code != 422 &&
          Notification.error({
            title: "Event Create Failed",
            duration: 3000,
            content: undefined,
          });
      }
    });
  };

  const { data: showTemplatessData } = useAppSelector(templatesSelector);
  const changeTab = (val: any) => {
    setActiveTab(val);
  };
  const changeTabMedia = (val: any) => {
    setActiveTabMedia(val);
  };

  //----------------------------------------------------------------------upload media------------------------------------------------------------------
  const handleUploadImage = (file: any) => {
    upload(file, 1);
  };
  const upload = (url: string, mediaTypeId: any) => {
    const uploadImageData = {
      url: url,
      media_type_id: mediaTypeId,
    };
    setIsPhotosLoading(true);
    dispatch(
      mediaNoticeUpload({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_notice: uuid as unknown as string,
        createData: uploadImageData,
      })
    ).then((response: any) => {
      if (response.payload.success == true) {
        setSelectedImages([]);
        setIsPhotosLoading(false);
        Notification.success({
          title: response.payload.msg,
          duration: 3000,
          content: undefined,
        });
        getNoticeMedia(1);
      } else {
        Notification.error({
          title: response.payload.error,
          duration: 3000,
          content: undefined,
        });
      }
    });
  };
  //---------------------------------------------------------------------pagination---------------------------------------------------------------
  const [mediaPagenationSkeleton, setmediaPagenationSkeleton] = useState(false);
  //--------------------------------------------------------------------------Event --------------------------------------------------------------------------------------------------
  const getEventLocationTypes = () => {
    dispatch(getAllEventLocationTypes());
  };
  const { data: getAllEventsData } = useAppSelector(noticeEventSelector);
  const getEvents = (page: integer) => {
    dispatch(
      getEvent({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        notice_uuid: uuid as unknown as string,
        page: page,
      })
    ).then(() => {
      seteventPagenationSkeleton(false);
      setIsEventsLoading(false);
    });
  };
  const { dataOne: oneEventData } = useAppSelector(noticeEventSelector);

  const OneEvent = (id: string) => {
    dispatch(
      getOneEvent({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        notice_uuid: uuid as unknown as string,
        uuid: id,
      })
    ).then((res: any) => {
      changeTab(res?.payload?.data?.event_location_type?.name);
    });
  };
  const [headerChnagePage, setheaderChnagePage] = useState(false);
  const [selectImage, setSelectImage] = useState("");
  const [selectImageUrl, setSelectImageUrl] = useState("");
  //--------------------------------------------------------------------Get Notice Media------------------------------------------------------------------------//
  const getNoticeMedia = (page: integer) => {
    dispatch(
      mediaNoticeGet({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_notice: uuid as unknown as string,
        page: page,
      })
    ).then(() => {
      setIsPhotosLoading(false);
      setmediaPagenationSkeleton(false);
      setIsCaptionLoading(false);
    });
  };
  useEffect(() => {
    getNoticeMedia(
      localStorage.getItem("notices_media_page")
        ? parseInt(localStorage.getItem("notices_media_page") as string, 10)
        : 1
    );
  }, []);
  const { getNoticeMediaData: getNoticeMediaData } =
    useAppSelector(mediaSelector);
  // delete image from notice page
  //----------------------------------------------------------------Delete media----------------------------------------------------------------------------//
  const deleteFun = (mediaUuid: String) => {
    setIsPhotosLoading(true);
    dispatch(
      deleteNoticeMedia({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_notice: uuid as unknown as string,
        uuid: mediaUuid as unknown as string,
      })
    ).then(async (response: any) => {
      // setIsPhotosLoading(false);
      if (response.payload.success == true) {
        Notification.success({
          title: response.payload.msg,
          duration: 3000,
          content: undefined,
        });
        getNoticeMedia(1);
      } else {
        Notification.error({
          title: response.payload.error,
          duration: 3000,
          content: undefined,
        });
      }
    });
  };
  //--------------------------------------------------------------------media edit and update------------------------------------------------------------------------------------------//
  const editUploadImage = (uuid: string, file: any) => {
    editImage(uuid, file);
  };
  const editImage = (imageId: any, url: string) => {
    dispatch(
      mediaNoticeUpdate({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_notice: uuid as unknown as string,
        mediaUid: imageId,
        updateData: { url: url },
      })
    ).then((response: any) => {
      if (response.payload.success == true) {
        Notification.success({
          title: response.payload.msg,
          duration: 3000,
          content: undefined,
        });
        getNoticeMedia(1);
      } else {
        Notification.error({
          title: response.payload.error,
          duration: 3000,
          content: undefined,
        });
      }
    });
  };
  const [showProfileShow, setshowProfileShow] = useState(false);
  const [showProfileUrl, setshowProfileUrl] = useState("");
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [deleteData, setDeleteData] = useState({ id: "", name: "" });
  const [selectedImages, setSelectedImages] = useState<number[]>([]);
  const handleImageClick = (item: MediaDataItem) => {
    if (selectedImages.includes(item.id)) {
      setSelectedImages(selectedImages.filter((id) => id !== item.id));
    } else {
      setSelectedImages([...selectedImages, item.id]);
    }
  };
  const [caption, setCaption] = useState("");
  const [captioncpy, setCaptioncpy] = useState("");
  const updateCaption = (imageId: any) => {
    setIsCaptionLoading(true);
    dispatch(
      updateNoticeCaptions({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_notice: uuid as unknown as string,
        uuid: imageId,
        updateData: { caption: caption },
      })
    ).then((response: any) => {
      getNoticeMedia(
        localStorage.getItem("notices_media_page")
          ? parseInt(localStorage.getItem("notices_media_page") as string, 10)
          : 1
      );
      if (response.payload.success == true) {
        setCaption("");
        setCrossBtnEnable(false);
        Notification.success({
          title: "  Caption updated successfully",
          duration: 3000,
          content: undefined,
        });
        setIsCaptionEditing(false);
      } else {
        setCrossBtnEnable(false);
        Notification.error({
          title: "Error updating caption",
          duration: 3000,
          content: undefined,
        });
      }
    });
  };
  const handleEventUpdate = (updateData: any) => {
    dispatch(
      updateEvent({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        notice_uuid: uuid as unknown as string,
        event_id: deleteData.id,
        updateData: updateData,
      })
    ).then((response: any) => {
      setIsEventsLoading(true);
      if (response.payload.success == true) {
        setEventCreateButtonEnabled(false);
        Notification.success({
          title: "Event Update Successfully",
          duration: 3000,
          content: undefined,
        });
        SetshowEvent(false);
        setAddNewEvent(false);
        getEvents(1);
        setInstructions("");
        setAddress("");
      } else {
        setEventCreateButtonEnabled(false);
        response.payload.code != 422 &&
          Notification.error({
            title: "Event Update Failed",
            duration: 3000,
            content: undefined,
          });
      }
    });
  };
  const [isDisabled, setIsDisabled] = useState(false);
  const [clickData, setClickData] = useState({
    id: "",
    name: "",
    address: "",
    is_delivery_possible: false,
    instructions: "",
    virtual_event: "",
    event_location_type_id: "",
    event_type_id: "",
    event_instructions: "",
  });
  console.log("ccc=>", clickData);

  return (
    <>
      <AlertModal
        show={showDeleteModal}
        isSmallModal={true}
        position="centered"
        title="Confirmation"
        onClose={() => {
          setShowDeleteModal(false);
        }}
      >
        <div className="mb-4 d-flex conform-color">
          Do you want to delete it?
          {/* <span
            className="text-truncate ps-1 font-semibold"
            style={{
              maxWidth: "100px",
            }}
          >
            {deleteData.name}
          </span>
          ? */}
        </div>

        <div className="col-auto">
          <button
            className="btn btn-system-primary me-3"
            onClick={() => setShowDeleteModal(false)}
          >
            No
          </button>
          <span
            className="yes-delete-it pointer font-120"
            onClick={() => {
              setIsDisabled(true);
              isDisabled
                ? null
                : dispatch(
                    deleteEvent({
                      active_partner: ACTIVE_PARTNER,
                      active_service_provider: ACTIVE_SERVICE_PROVIDER,
                      notice_uuid: uuid as unknown as string,
                      event_id: deleteData.id,
                    })
                  ).then((response: any) => {
                    if (response.payload.success == true) {
                      setIsEventsLoading(true);
                      setIsDisabled(false);
                      Notification.success({
                        title: "Deleted Successfully",
                        duration: 3000,
                        content: undefined,
                      });
                      getEvents(1);
                      setShowDeleteModal(false);
                    } else {
                      setIsDisabled(false);
                      Notification.error({
                        title: "Delete Failed",
                        duration: 3000,
                        content: undefined,
                      });
                    }
                  });
            }}
          >
            Yes, Delete it
          </span>
        </div>
      </AlertModal>

      <FullScreenModal
        show={headerChnagePage}
        size="lg"
        // title="All the information you add here will be visible in the memorial page"Featured Photos
        title="Featured Photos"
        description="This will be visible in the notice page"
        onClose={() => {
          setheaderChnagePage(false), setSelectImageUrl("");
          setSelectedImages([]);
        }}
        footer={
          selectImageUrl !== "" && (
            <Button
              className="btn-system-primary pointer px-4 py-2 mt-2"
              onClick={() => {
                upload(selectImageUrl, 1);
                setheaderChnagePage(false);
                setSelectImageUrl("");
              }}
            >
              Add
            </Button>
          )
        }
      >
        <>
          <div className="music-filter d-flex mt-2 mb-4 overflowx-scroll ">
            <button
              type="button"
              className={
                activeTabMedia === "All"
                  ? ` px-2  px-sm-3  system-filter  smaller-font me-1 system-filter2 music-tab`
                  : ` px-2  px-sm-3  system-filter  smaller-font me-1 music-tab`
              }
              onClick={() => changeTabMedia("All")}
            >
              All
            </button>
            <div>
              <button
                type="button"
                className={
                  activeTabMedia === "profile_photos"
                    ? ` px-3  px-sm-3 system-filter  smaller-font me-1 system-filter2 music-tab`
                    : ` px-3  px-sm-3 system-filter  smaller-font me-1  music-tab`
                }
                onClick={() => changeTabMedia("profile_photos")}
              >
                Profile Photos
              </button>
            </div>
            <div>
              <button
                type="button"
                className={
                  activeTabMedia === "deleted_photos"
                    ? ` px-3 px-sm-3  system-filter  smaller-font me-1 system-filter2 music-tab`
                    : ` px-3 px-sm-3  system-filter  smaller-font me-1  music-tab`
                }
                onClick={() => changeTabMedia("deleted_photos")}
              >
                Deleted Photos
              </button>
            </div>
          </div>
          <div className="">
            {activeTabMedia === "All" && (
              <>
                <div className="d-flex flex-wrap align-items-center grid-container-p">
                  {mediaData?.data?.map((item: any) => (
                    <>
                      {item.media_type.id == 1 && (
                        <div
                          key={item.id}
                          className={`  pointer  group-photo-container-p system-filter-secondary rounded-3  position-relative  ${
                            selectedImages.includes(item.id) ||
                            selectImage === item.id
                              ? "system-danger-select"
                              : ""
                          }`}
                        >
                          {selectedImages.includes(item.id) && (
                            <Image
                              src={tick}
                              alt="memorialprofile"
                              className="position-absolute bottom-0 start-0 m-1 move p-0"
                            />
                          )}
                          <Image
                            src={item.url}
                            alt=""
                            className="image rounded pointer "
                            width={133}
                            height={133}
                            onClick={() => {
                              handleImageClick(item);
                              setSelectImage(item.id);
                              setSelectImageUrl(item.url);
                            }}
                          />
                        </div>
                      )}
                    </>
                  ))}
                </div>
              </>
            )}
            {activeTabMedia === "profile_photos" && (
              <>
                <div className="d-flex flex-wrap align-items-center grid-container-p  ">
                  {mediaData?.data?.map((item: any) => (
                    <>
                      {item.media_type.id == 1 && (
                        <div
                          key={item.id}
                          className={`  pointer  group-photo-container-p system-filter-secondary rounded-3  position-relative  ${
                            selectedImages.includes(item.id) ||
                            selectImage === item.id
                              ? "system-danger-select"
                              : ""
                          }`}
                        >
                          {selectedImages.includes(item.id) && (
                            <Image
                              src={tick}
                              alt="memorialprofile"
                              className="position-absolute bottom-0 start-0 m-1 move p-0"
                            />
                          )}
                          <Image
                            src={item.url}
                            alt=""
                            className="image rounded pointer "
                            width={133}
                            height={133}
                            onClick={() => {
                              handleImageClick(item);
                              setSelectImage(item.id);
                              setSelectImageUrl(item.url);
                            }}
                          />
                        </div>
                      )}
                    </>
                  ))}
                </div>
              </>
            )}
            {activeTabMedia === "deleted_photos" && (
              <>
                <div className="d-flex flex-wrap align-items-center grid-container-p ">
                  {deleteMediaData?.data?.map((item: any) => (
                    <>
                      {item.media_type.id == 1 && (
                        <div
                          key={item.id}
                          className={`  pointer  group-photo-container-p system-filter-secondary rounded-3  position-relative  ${
                            selectedImages.includes(item.id) ||
                            selectImage === item.id
                              ? "system-danger-select"
                              : ""
                          }`}
                        >
                          {selectedImages.includes(item.id) && (
                            <Image
                              src={tick}
                              alt="memorialprofile"
                              className="position-absolute bottom-0 start-0 m-1 move p-0"
                            />
                          )}
                          <Image
                            src={item.url}
                            alt=""
                            className="image rounded pointer "
                            width={133}
                            height={133}
                            onClick={() => {
                              handleImageClick(item);
                              setSelectImage(item.id);
                              setSelectImageUrl(item.url);
                            }}
                          />
                        </div>
                      )}
                    </>
                  ))}
                </div>
              </>
            )}
          </div>
        </>
      </FullScreenModal>

      <FullScreenModal
        show={showAddContact}
        onClose={() => {
          setShowAddContact(false);
        }}
        title="Add new contact"
        description="This will be visible in the Notice page"
        footer={
          <Button
            ref={noticeActionBtnRef}
            className="btn-system-primary pointer"
            disabled={submitButtonDisabled}
          >
            {getLabel("submit", initialLanguage)}
          </Button>
        }
      >
        <AddNewContact
          response={response}
          actionBtnRef={noticeActionBtnRef}
          handleUpdate={handleUpdate}
        />
      </FullScreenModal>

      <FullScreenModal
        show={showAddInformant}
        onClose={() => {
          SetShowAddInformant(false);
        }}
        title="Add a informant"
        description="This will be visible in the notice page"
        footer={
          <Button
            className="pointer"
            onClick={() => {
              alert("Add a informant working");
            }}
          >
            {getLabel("submit", initialLanguage)}
          </Button>
        }
        deleteLabel={
          <a
            onClick={() => {
              alert("Delete is working");
            }}
          >
            {getLabel("delete", initialLanguage)}
          </a>
        }
      >
        <AddInformant />
      </FullScreenModal>

      <FullScreenModal
        show={showAddRelationship}
        onClose={() => {
          setShowAddRelationship(false);
        }}
        title="Add family and relatives"
        description="This will be visible in the notice page"
        footer={
          <Button
            className="btn-system-primary pointer"
            onClick={() => {
              // alert('Working');
            }}
          >
            {getLabel("submit", initialLanguage)}
          </Button>
        }
        deleteLabel={
          <a
            onClick={() => {
              alert("delete working");
            }}
          >
            {getLabel("delete", initialLanguage)}
          </a>
        }
      >
        <AddRelationship />
      </FullScreenModal>

      <FullScreenModal
        show={addNewEvent}
        onClose={() => {
          setAddNewEvent(false);
        }}
        title={`${getLabel("addNewEvent", initialLanguage)}`}
        description={`${getLabel("addNewEventDescription", initialLanguage)}`}
        footer={
          <Button
            className="pointer btn btn-system-primary "
            ref={actionBtnRef}
            disabled={eventCreateButtonEnabled}
          >
            {getLabel("submit", initialLanguage)}
          </Button>
        }
      >
        <AddNewEvent actionBtnRef={actionBtnRef} handleCreate={handleCreate} />
      </FullScreenModal>

      <FullScreenModal
        show={addTemplate}
        size="xl"
        title={`${getLabel("templates", initialLanguage)}`}
        description={`${getLabel("templatesDescription", initialLanguage)}`}
        onClose={() => setAddTemplate(false)}
        // footer={
        //   <Button
        //     className="btn-system-primary pointer"
        //     onClick={handleOptionSelect}
        //   >
        //     {getLabel("select",initialLanguage)}
        //   </Button>
        // }
      >
        {/* <Templates
          Data={showTemplatessData}
          defaultOptionId={selectedOptionId}
          onOptionSelect={handleOptionSelection}
        /> */}
        <ArcoNextTabs
          Data={showTemplatessData}
          defaultOptionId={selectedOptionId}
          onOptionSelect={handleOptionSelection}
        />
      </FullScreenModal>

      {/*-------------------------------------*Notice Page Language*--------To-------*Template*---------------------------------------------------------------------*/}
      <div className="" onClick={handleNickNameUpdate}>
        <div className="border  p-3 ps-4 mb-3 ovt-x bg-white">
          {!NoticeData?.loading || isLoading || loading ? (
            <>
              <NoticeDetailsSkelton />
            </>
          ) : (
            <div className="p-0 m-0 mb-4 ">
              {showRequired ? (
                <div className="mb-3 p-3 system-danger-light rounded">
                  <p className="p-0 m-0 font-90 font-bold">
                    {getLabel("fillAllRequiredAlert", initialLanguage)}
                  </p>
                  <p className="p-0 m-0 system-text-dark-80 font-90 font-normal ">
                    {getLabel(
                      "fillAllRequiredAlertDescription",
                      initialLanguage
                    )}
                  </p>
                </div>
              ) : (
                <div className="mb-3 p-3 system-light rounded d-none">
                  <p className="p-0 m-0 font-90 font-bold">
                    {getLabel("fillAllRequiredAlert", initialLanguage)}
                  </p>
                  <p className="p-0 m-0 system-text-dark-80 font-90 font-normal ">
                    {getLabel(
                      "fillAllRequiredAlertDescription",
                      initialLanguage
                    )}
                  </p>
                </div>
              )}

              <div
                className={`row  align-items-center p-0 m-0  ${
                  !showRequired ? "mt-3" : ""
                }  `}
              >
                <div className="col-md-3 col-12  font-bold p-0  pe-0 pe-md-4 d-flex">
                  <p className="m-0 p-0 ">
                    {getLabel("noticePageLanguage", initialLanguage)}
                  </p>
                </div>
                <div className="col-md-4 col-12   ps-0 ps-md-2 mt-1 mt-sm-0  notiede page-arco">
                  <CustomSelect
                    data={NoticeData?.languages}
                    placeholderValue={getLabel(
                      "selectALanguage",
                      initialLanguage
                    )}
                    // onChange={(value: any) => setLanguage(value)}
                    onChange={(value: any) => {
                      setLanguage(value);
                      setLangFullName("");
                    }}
                    is_search={true}
                    defaultValue={parseInt(NoticeData?.["language_id"], 10)}
                  />
                </div>
                <div className="row mb-3 m-0 p-0">
                  <div className="col-md-3  col-12 d-none d-sm-flex p-0 m-0 "></div>
                  <div className="col-md-9  col-12 font-80 font-normal system-muted-5  ps-0 ps-md-2 mt-1 mt-sm-0">
                    {getLabel(
                      "noticePageLanguageSelectDescription",
                      initialLanguage
                    )}
                  </div>
                </div>
              </div>
              {NoticeData?.is_single === true &&
              isLanguageSupported === true ? (
                <div className="row align-items-center p-0 m-0 mb-3">
                  <div className="col-md-3 col-12 p-0   font-bold pe-0 pe-md-4 lh-sm ">
                    {NoticeData?.["full_name"]}’s name in {selectedLanguage}
                    <div className="font-80 font-normal system-icon-secondary p-0  lh-sm">
                      {getLabel("optional", initialLanguage)}
                    </div>
                  </div>
                  <div className="col-md-9 col-12 ps-0 ps-md-2 mt-1 mt-sm-0">
                    <input
                      type="text"
                      className="form-control border border-2 notice-input system-control "
                      placeholder={`Enter name in ${selectedLanguage}`}
                      // value={langFullName || langFullNamec}
                      value={langFullName}
                      onChange={(e) => handleInputChange(e, "langFullName")}
                    />
                  </div>
                </div>
              ) : (
                <></>
              )}

              <div className="row  align-items-center p-0 m-0 ">
                <div className="col-md-3 col-12 p-0   font-bold pe-0 pe-md-4">
                  {getLabel("noticeTitle", initialLanguage)}
                  <div className="font-80 font-normal system-icon-secondary  p-0  ">
                    {getLabel("optional", initialLanguage)}
                  </div>
                </div>
                <div className="col-md-9 col-12   ps-0 ps-md-2 mt-1 mt-sm-0 ">
                  <input
                    type="text"
                    className="form-control border border-2 notice-input system-control "
                    placeholder={`${getLabel(
                      "searchOrAddNew",
                      initialLanguage
                    )}`}
                    value={noticeTitle}
                    onChange={handleNoticeTitleChange}
                  />
                </div>{" "}
                <div className="row  m-0 p-0 mb-3">
                  <div className="col-md-3 col-12 d-none d-sm-flex ps-0 "></div>
                  <div className="col-md-9  col-12 font-bold   ps-0 ps-md-2 mt-1 mt-sm-0 overflow-hidden text-truncate">
                    <span className="font-90 font-semibold text-dark ">
                      {suggestion &&
                      noticeTitle &&
                      Array.isArray(noticeTitledata) &&
                      noticeTitledata.length > 0 ? (
                        " Suggestion:"
                      ) : (
                        <>&nbsp;</>
                      )}
                    </span>
                    {suggestion &&
                      noticeTitle &&
                      Array.isArray(noticeTitledata) &&
                      noticeTitledata.length > 0 && (
                        <>
                          {noticeTitledata.map((option: any) => (
                            <span
                              className="font-90 font-normal system-text-brown pointer px-1"
                              key={option.id}
                              onClick={() =>
                                handleOptionClick(option.id, option.name)
                              }
                            >
                              {" "}
                              {option.name}
                              {"  "}
                            </span>
                          ))}
                        </>
                      )}
                  </div>
                </div>
              </div>

              <div className="row  align-items-center p-0 m-0">
                <div className="col-md-3 col-12 p-0   font-bold pe-0 pe-md-4 ">
                  {getLabel("template", initialLanguage)}
                </div>
                <div className="col-md-9 col-12 m-0  ps-0 ps-md-2 mt-1 mt-sm-0 ">
                  {selectedOptionId || NoticeData?.["notice_template_id"] ? (
                    <>
                      <div className="d-flex gap-3 align-items-center">
                        <CustomImage
                          src={selectedOptionLink}
                          alt={selectedOptionAlt}
                          className="image pointer me-3 rounded"
                          width={59}
                          height={64}
                          divClass=" position-relative template-container-small system-grey-200"
                        />{" "}
                        <span
                          className="font-normal system-text-primary pointer"
                          onClick={() => setAddTemplate(true)}
                        >
                          {getLabel("changeTemplate", initialLanguage)}
                        </span>
                      </div>
                    </>
                  ) : (
                    <button
                      type="button"
                      className="btn px-3 px-sm-4 me-3 border border-2 font-bold text-dark py-3"
                      onClick={() => setAddTemplate(true)}
                    >
                      {getLabel("selectATemplate", initialLanguage)}
                    </button>
                  )}
                </div>
              </div>
              <div className="row  m-0 ">
                <div className="col-lg-3 col-md-3 col-12 d-none d-sm-flex ps-0 "></div>
                <div className="col-lg-9 col-12 col-md-9 ps-0 ps-md-2 mt-1 mt-sm-0 ">
                  <button
                    type="button"
                    className="btn btn-system-primary mt-4 mb-1"
                    onClick={() => createAndUpdate()}
                  >
                    {getLabel("save", initialLanguage)}
                  </button>
                </div>
              </div>
            </div>
          )}

          {/*------------------------------------------------------------------*Notice contents*---------------------------------------------------------------*/}
          <hr className="mb-3  hr-width-form" />
          <div className="row m-0 p-0 align-items-center mt-4 mb-3">
            <div className="p-0 m-0 col-12 col-sm-6 d-flex justify-content-center justify-content-sm-start mb-2 mb-sm-0">
              <strong className="font-150 font-bold">
                {" "}
                {getLabel("noticeContents", initialLanguage)}
              </strong>
            </div>
            <div className="col-12 col-sm-6 d-flex justify-content-center justify-content-sm-end p-0 m-0">
              <button type="button" className="btn system-success text-white">
                {getLabel("viewSample", initialLanguage)}
              </button>
            </div>
          </div>
          <div className="system-warning-light pb-2 rounded">
            <div className="mb-4 d-flex flex-column justify-content-center ">
              <div className="m-0 p-0 py-3 font-160 font-bold system-text-brown d-flex justify-content-center ">
                {" "}
                மரண அறிவித்தல்
              </div>
              <div className="d-flex justify-content-center mb-3">
                <input
                  type="text"
                  value={inputValue}
                  onChange={handleChange}
                  className=" system-outline-primary2 p-1 bg-white px-2"
                />
              </div>
              <div className="d-flex justify-content-center align-items-center mb-3">
                <div className="single-notice-img-container position-relative system-filter-secondary rounded  ">
                  <Image
                    src={camera}
                    alt="memorialprofile"
                    className="position-absolute top-0 end-0 m-2 pointer"
                  />
                  <Image
                    src={framDefault}
                    alt="memorialprofile"
                    className=" rounded image"
                  />
                </div>
              </div>
              <div className="d-flex justify-content-center mb-3">
                {/* <button
                  type="button"
                  className="btn system-outline-primary p-1 bg-white fw-normal px-2"
                >
                  Add pre name information
                </button> */}
                <input
                  type="text"
                  value={inputValue2}
                  onChange={handleChange}
                  className=" system-outline-primary2 p-1 bg-white px-2"
                />
              </div>
              <div className="m-0 p-0 font-180 font-bold text-center ">
                திரு {NoticeData?.["full_lang_name"]}
              </div>
              <p className="m-0 p-0 text-center font-130 font-bold py-2">
                இளைப்பாறிய பொது சுகாதார பரிசோதகர்
              </p>
              {/* -----------------------------------------------------------------------------------Nickname------------------------------------------*/}
              <div
                ref={ref}
                className={`m-0 p-0 font-80 font-normal system-text-primary text-center pointer ${
                  isEditing ? "editable" : ""
                }`}
                onClick={handleNickName}
              >
                {isEditing ? (
                  <input
                    type="text"
                    value={nickName}
                    className="inline-input"
                    autoFocus
                    onChange={(e) => handleNickNameInputChange(e)}
                  />
                ) : (
                  <strong
                    className={`m-0 p-0 font-80 font-normal system-text-primary text-center pointer ${
                      isEditing ? "editable" : ""
                    }`}
                  >
                    {nickName}
                  </strong>
                )}
              </div>
            </div>
            {/* <hr className="mb-3 hr-width-form" /> */}
            <hr className="mb-4 " />
            <div className="px-md-4 pb-3">
              <div className="row m-0 p-0 align-items-center mb-2">
                <div className="p-0 m-0 col-6 ps-3 font-bold">
                  {" "}
                  {getLabel("mainContent", initialLanguage)}
                </div>
                <div
                  className="col-6 font-80 font-normal system-text-primary d-flex justify-content-end p-0 m-0 pointer"
                  onClick={handleEditClick}
                >
                  {/* Switch to simple mode */}
                  {isEditing
                    ? `${getLabel(
                        "switchToCustomParagraphMode",
                        initialLanguage
                      )}`
                    : `${getLabel("switchToSimpleMode", initialLanguage)}`}
                </div>
              </div>
              {/* <div className="p-3 font-normal lh-md">
                <p>
                  Jeyakanthan Vasanthan Joseph was born in Jaffna, Sri Lanka and
                  lived in Amilly, France and passed away peacefully on 29th
                  December 2022.
                </p>{" "}
                <p>Beloved husband of Marie.</p>{" "}
                <p>
                  Loving and devoted father of John Shana, Sonia, Leo Sanjeeve.
                  Jeyakanthan Vasanthan Joseph was born in Jaffna, Sri Lanka and
                  lived in Amilly, France and passed away peacefully on 29th
                  December 2022.
                </p>{" "}
                <p>
                  Loving and devoted father of John Shana, Sonia, Leo Sanjeeve.
                  Sri Lanka and lived in Amilly, France and passed away
                  peacefully on 29th December 2022.
                </p>
                <p className="m-0 p-0">
                  {" "}
                  Sri Lanka and lived in Amilly, France and passed away
                  peacefully on 29th December 2022.
                </p>
              </div> */}
              <div
                className={` ${
                  isEditing
                    ? "system-warning-dark p-3 font-normal lh-md"
                    : "p-3 font-normal lh-md"
                }`}
              >
                {isEditing ? (
                  <textarea
                    className="inline-input2 "
                    rows={Math.max(MainContent.split("\n").length, 1)}
                    value={MainContent}
                    onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) =>
                      handleTextChange(e, "MainContent")
                    }
                  />
                ) : (
                  <pre>{MainContent}</pre>
                )}
              </div>
              <div className="system-warning-dark p-3 font-normal lh-md">
                <p>
                  Jeyakanthan Vasanthan Joseph was born in Jaffna, Sri Lanka and
                  lived in Amilly, France and passed away peacefully on 29th
                  December 2022.
                </p>{" "}
                <p>Beloved husband of Marie.</p>{" "}
                <p>
                  Loving and devoted father of John Shana, Sonia, Leo Sanjeeve.
                  Jeyakanthan Vasanthan Joseph was born in Jaffna, Sri Lanka and
                  lived in Amilly, France and passed away peacefully on 29th
                  December 2022.
                </p>{" "}
                <p>
                  Loving and devoted father of John Shana, Sonia, Leo Sanjeeve.
                  Sri Lanka and lived in Amilly, France and passed away
                  peacefully on 29th December 2022.
                </p>
                <p className="m-0 p-0">
                  {" "}
                  Sri Lanka and lived in Amilly, France and passed away
                  peacefully on 29th December 2022.
                </p>
              </div>
            </div>
            {/* <hr className="mb-4 " /> */}
            <div className=" mx-md-2 mx-0 px-3 mb-4 pb-2">
              <div className="ms-0 ms-md-3">
                <div className="d-block d-md-flex justify-content-between align-items-center">
                  <div className="p-0 m-0 font-bold">
                    {getLabel("addFamilyMembersAndRelatives", initialLanguage)}
                  </div>
                  <div className="system-text-primary font-80 font-normal ms-0 p-0 m-0 pointer">
                    {getLabel("switchToCustomParagraphMode", initialLanguage)}
                  </div>
                </div>

                <p className="fw-normal text-muted  ">
                  {getLabel(
                    "addFamilyMembersAndRelativesDescription",
                    initialLanguage
                  )}
                </p>
                <div className="d-flex align-items-center flex-wrap mb-4 ">
                  <div className="mb-sm-0">
                    <button
                      type="button"
                      className="btn btn-system-primary p-1 bg-white font-normal px-2 me-2"
                      onClick={() => {
                        setShowAddRelationship(true);
                      }}
                    >
                      + {getLabel("add", initialLanguage)}
                    </button>
                  </div>
                  {/* <div className="system-text-primary font-80 font-normal ms-0 p-0 m-0 pointer">
                    Switch to custom paragraph mode
                  </div> */}
                </div>
                <div className="pe-3">
                  <div className="row mb-3">
                    <div className="col-12 col-md-6 ">
                      <div className="system-text-primary font-80 font-normal pb-1">
                        Parents
                      </div>
                      <div className="row gap-3 p-0 m-0 ">
                        <div className="col-auto p-0 m-0 border border-2 p-2 bg-white d-flex gap-2 rounded">
                          <div className="ps-1">
                            {" "}
                            <Image
                              src={drag}
                              alt="memorialprofile"
                              className="move"
                            />
                          </div>
                          <div
                            className="text-muted"
                            style={{ maxWidth: "90%" }}
                          >
                            <p className="p-o m-0 font-90 font-semibold text-truncate">
                              Saravanan
                            </p>
                            <p className="p-o m-0 font-80 font-normal">
                              Mother
                            </p>
                          </div>
                        </div>
                        <div className="col-auto p-0 m-0 border border-2 p-2 bg-white d-flex gap-2 rounded">
                          <div className="ps-1">
                            {" "}
                            <Image
                              src={drag}
                              alt="memorialprofile"
                              className="move"
                            />
                          </div>
                          <div
                            className="text-muted"
                            style={{ maxWidth: "90%" }}
                          >
                            <p className="p-o m-0 font-90 font-semibold text-truncate">
                              Roja
                            </p>
                            <p className="p-o m-0 font-80 font-normal">
                              Father(Late)
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-12 col-md-6">
                      <div className="system-text-primary font-80 font-normal pb-1 pt-3 pt-md-0">
                        Spouse
                      </div>
                      <div className="row gap-2 p-0 m-0">
                        <div className="col-auto p-0 m-0 border border-2  p-2 bg-white d-flex gap-2 rounded">
                          <div className="ps-1">
                            {" "}
                            <Image
                              src={drag}
                              alt="memorialprofile"
                              className="move"
                            />
                          </div>
                          <div
                            className="text-muted"
                            style={{ maxWidth: "90%" }}
                          >
                            <p className="p-o m-0 font-90 font-semibold text-truncate">
                              Rajmohan
                            </p>
                            <p className="p-o m-0 font-80 font-normal">
                              Wife, Australia, MBBS
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12  ">
                      <div className="system-text-primary pb-1 font-80 font-normal">
                        Children
                      </div>
                      <div className="row gap-3 p-0 m-0 ">
                        <div className="col-auto p-0 m-0 border border-2  p-2 bg-white d-flex gap-2 rounded">
                          <div className="ps-1">
                            {" "}
                            <Image
                              src={drag}
                              alt="memorialprofile"
                              className="move"
                            />
                          </div>
                          <div
                            className="text-muted"
                            style={{ maxWidth: "90%" }}
                          >
                            <p className="p-o m-0 font-90 font-semibold text-truncate">
                              Sujiv
                            </p>
                            <p className="p-o m-0 font-80 font-normal text-truncate">
                              Son: London
                            </p>
                          </div>
                        </div>
                        <div className="col-auto p-0 m-0 border border-2 p-2 bg-white d-flex gap-2 rounded">
                          <div className="ps-1">
                            {" "}
                            <Image
                              src={drag}
                              alt="memorialprofile"
                              className="move"
                            />
                          </div>
                          <div
                            className="text-muted"
                            style={{ maxWidth: "90%" }}
                          >
                            <p className="p-o m-0 font-90 font-semibold text-truncate">
                              Aksha
                            </p>
                            <p className="p-o m-0 font-80 font-normal text-truncate">
                              Daugther: London
                            </p>
                          </div>
                        </div>
                        <div className="col-auto p-0 m-0 border border-2 p-2 bg-white d-flex gap-2 rounded">
                          <div className="ps-1">
                            {" "}
                            <Image
                              src={drag}
                              alt="memorialprofile"
                              className=" move"
                            />
                          </div>
                          <div
                            className="text-muted"
                            style={{ maxWidth: "90%" }}
                          >
                            <p className="p-o m-0 font-semibold text-truncate">
                              Kishonmaran
                            </p>
                            <p className="p-o m-0 font-80 font-normal text-truncate">
                              Former principal of Jaffna Hindu College, Sri
                              Lanka
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <hr className="mb-4 " />
            <div className="mx-md-2 mx-0 px-3 mb-4">
              <div className="mx-0 mx-md-3">
                <div className="d-flex align-items-center ">
                  <div className="p-0 m-0 me-1  font-bold">
                    {getLabel("informantTitle", initialLanguage)}
                  </div>
                  <Image src={question} alt="memorialprofile" className="" />
                </div>
                <div className="row p-0 m-0 ">
                  <div className=" col-12 col-md-10 m-0 px-0  mt-2 ">
                    <input
                      type="text"
                      className="form-control border border-2 system-control "
                    />
                  </div>
                </div>
                <div className="row m-0 p-0 mb-3">
                  <div className="col-12 fw-normal   text-muted  px-0 ">
                    <span className="font-semibold  system-muted-5 ">
                      {getLabel("suggestion", initialLanguage)}
                    </span>
                    <span className="font-70 font-normal ms-2 me-3 system-muted-5 pointer">
                      உங்கள் நினைவில் வாழும்
                    </span>
                    <span className="font-70 font-normal ps-2 system-muted-5 pointer">
                      என்றும் உங்கள் நினைவில் வாழும்
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <hr className="mb-4 " />
            <div className="mx-md-2 mx-0 px-3 mb-4">
              <div className="d-flex align-items-center mb-2 ps-0 ps-md-3">
                <div className="p-0 m-0 me-1 font-bold">
                  {getLabel("informant", initialLanguage)}
                </div>
                <Image src={question} alt="memorialprofile" className="" />
              </div>
              <div className="row ps-0 ps-md-3">
                <div className="col-12 ">
                  <div className="row gap-2 p-0 m-0 ">
                    <div className="col-md-10 col-12 p-0 m-0 border border-2 p-2 bg-white d-flex  rounded">
                      <div className="ps-1">
                        {" "}
                        <Image src={drag} alt="drag" className="move" />
                      </div>
                      <div
                        className="text-muted ps-2"
                        style={{ maxWidth: "90%" }}
                      >
                        <p className="p-o m-0 font-semibold text-truncate">
                          Saravanan
                        </p>
                        <p className="p-o m-0 font-80 font-normal font-80">
                          Teacher
                        </p>
                      </div>
                    </div>
                    <div className="d-flex align-items-center gap-2 ps-2">
                      <div className="ps-1">
                        <Image src={drag} alt="drag" className="move" />
                      </div>
                      <div className="system-text-primary m-0 p-0 font-80 font-normal">
                        Group by Sons
                      </div>
                    </div>
                    <div className="col-md-10 col-12 p-0 m-0 border border-2 p-2 bg-white d-flex rounded">
                      <div className="ps-1">
                        {" "}
                        <Image src={drag} alt="drag" className="move" />
                      </div>
                      <div
                        className="text-muted ps-2"
                        style={{ maxWidth: "90%" }}
                      >
                        <p className="p-o m-0 font-semibold text-truncate">
                          Rajmohan
                        </p>
                        <p className="p-o m-0 font-80 font-normal">
                          Australia, MBBS
                        </p>
                      </div>
                    </div>
                    <div className="col-md-10 col-12 p-0 m-0 border border-2 p-2 bg-white d-flex  rounded">
                      <div className="ps-1">
                        {" "}
                        <Image src={drag} alt="drag" className="move" />
                      </div>
                      <div
                        className="text-muted ps-2"
                        style={{ maxWidth: "90%" }}
                      >
                        <p className="p-o m-0 font-semibold text-truncate">
                          Kishonmaran
                        </p>
                        <p className="p-o m-0 font-80 font-normal">
                          Former principal of Jaffna Hindu College, Sri Lanka
                        </p>
                      </div>
                    </div>
                    <div className="d-flex align-items-center gap-2 ps-2">
                      <div className="ps-1">
                        <Image src={drag} alt="drag" className="move" />
                      </div>
                      <div className="system-text-primary m-0 p-0 font-80 font-normal">
                        and
                      </div>
                    </div>
                    <div className="mb-2 col-md-10 col-12 p-0 m-0 border border-2 p-2 bg-white d-flex  rounded">
                      <div className="ps-1">
                        {" "}
                        <Image src={drag} alt="drag" className="move" />
                      </div>
                      <div className="text-muted ps-2">
                        <p className="p-o m-0 font-semibold">Relatives</p>
                      </div>
                    </div>
                    <div className="mb-2 mb-sm-0 m-0 p-0">
                      <button
                        onClick={() => {
                          SetShowAddInformant(true);
                        }}
                        type="button"
                        className="btn btn-sm btn-system-primary p-1 px-2 bg-white  font-normal"
                      >
                        + {getLabel("add", initialLanguage)}
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className=" mx-md-2 mx-0 ps-3 mb-4 ">
            <button
              type="button"
              className="btn btn-system-primary mt-4 ms-0 ms-md-3"
            >
              {getLabel("save", initialLanguage)}
            </button>
          </div>
        </div>
        {/* --------------events----------- */}
        {/* model open */}
        <FullScreenModal
          show={showEvent}
          onClose={() => {
            SetshowEvent(false);
          }}
          title={`${getLabel("showEvent", initialLanguage)}`}
          description={`${getLabel("showEventDescription", initialLanguage)}`}
          footer={
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <button
                ref={noticeActionBtnRef}
                className="btn btn-system-primary me-3"
              >
                Submit
              </button>
              <a
                onClick={() => {
                  SetshowEvent(false);
                  setShowDeleteModal(true);
                }}
              >
                <div className="text-danger pointer py-2">
                  {getLabel("deleteTheEvent", initialLanguage)}
                </div>
              </a>
            </div>
          }
        >
          <UpdateEvent
            actionBtnRef={noticeActionBtnRef}
            handleEventUpdate={handleEventUpdate}
            response={editResponse}
            data={clickData}
          />
        </FullScreenModal>
        {/* model end */}
        {!NoticeData.loading || isEventsLoading ? (
          <>
            <NoticeEventsSkelton />
          </>
        ) : (
          <div className="border bg-white px-4 py-3 mb-3">
            <div className="d-flex justify-content-between align-items-center">
              <h6 className="p-0 m-0 font-150 font-bold">
                {getLabel("events", initialLanguage)}
              </h6>
              <div className=" form-check form-switch">
                <input
                  className="form-check-input border border-2 system-control"
                  type="checkbox"
                  id="flexSwitchCheckDefault"
                />
              </div>
            </div>
            <p className="system-muted-5 font-90 font-normal m-0 p-0 mb-3">
              {getLabel("eventsDescription", initialLanguage)}
            </p>
            <div className="row mb-3 ">
              <div className="col-12">
                <button
                  type="button"
                  className="btn  btn-lg system-outline-primary  font-bold "
                  onClick={() => {
                    setAddNewEvent(true);
                  }}
                >
                  + {getLabel("addAnEvent", initialLanguage)}
                </button>
              </div>
            </div>
            {eventPagenationSkeleton ? (
              <>
                <div className="row gap-2 p-0 m-0 mb-4">
                  {Array.from({ length: 20 }).map((_) => (
                    <div className="col-auto p-0 m-0 border border-2 px-3 p-1 bg-white d-flex gap-2 rounded pointer">
                      <div></div>
                      <div className="text-muted py-1">
                        <div className="p-o m-0 font-semibold text-truncate mb-1">
                          <ShimmerEffectText width="100px" height="15px" />
                        </div>
                        <div className="p-o m-0 font-11 font-normal ">
                          <ShimmerEffectText width="80px" height="12px" />
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </>
            ) : (
              <>
                <div className="row gap-2 p-0 m-0 ">
                  {getAllEventsData?.data?.map((item: any) => (
                    <div
                      className="col-auto p-0 m-0 border border-2 px-3 p-1 bg-white d-flex gap-2 rounded pointer"
                      onClick={() => {
                        SetshowEvent(true);
                        OneEvent(item.uuid);
                        setClickData(item);
                        setDeleteData({
                          name: item.event_type.name,
                          id: item.uuid,
                        });
                      }}
                    >
                      <div>
                        <Image
                          src={greendot}
                          alt="memorialprofile"
                          className=""
                        />
                      </div>
                      <div className="text-muted">
                        <p className="p-o m-0 font-semibold text-truncate">
                          {item.event_type?.name}
                        </p>
                        <p className="p-o m-0 font-11 font-normal">
                          {<TimeConverter time={item.created_at} />}
                        </p>
                      </div>
                    </div>
                  ))}
                </div>
              </>
            )}
          </div>
        )}
        {/* --------------Contacts----------- */}
        {!NoticeData.loading || isContactssLoading ? (
          <>
            <NoticeContactsSkelton />
          </>
        ) : (
          <div className="border bg-white px-4 py-3 pb-4 mb-3">
            <div className="d-flex justify-content-between align-items-center">
              <h6 className="p-0 m-0 font-150 font-bold ">
                {getLabel("contacts", initialLanguage)}
              </h6>
              <div className=" form-check form-switch">
                <input
                  className="form-check-input border border-2 system-control"
                  type="checkbox"
                  id="flexSwitchCheckDefault"
                />
              </div>
            </div>
            <p className="system-muted-5 font-90 font-normal m-0 p-0">
              {getLabel("contactDescription", initialLanguage)}
            </p>
            <div className="row my-3">
              <div className="col-12">
                <button
                  type="button"
                  className="btn btn-lg system-outline-primary  font-bold"
                  onClick={() => {
                    setShowAddContact(true);
                  }}
                >
                  + {getLabel("addContact", initialLanguage)}
                </button>
              </div>
            </div>
            <div>
              {allContactsData?.data?.length >= 0 &&
                allContactsData?.data?.map((data: any, index: any) =>
                  contactPagenationSkeleton ? (
                    <>
                      {" "}
                      <ContactSkelton loopTimes={8} />
                    </>
                  ) : (
                    <>
                      <NoticesContactCard
                        data={data}
                        index={index}
                        noData={allContactsData?.data?.length}
                        handleload={handleload}
                      />
                    </>
                  )
                )}
            </div>
          </div>
        )}
        {/* --------------Photos----------- */}
        {!NoticeData?.loading || isPhotosLoading || isCaptionLoading ? (
          <NoticePhotosSkelton />
        ) : (
          <div className="border bg-white px-4 py-3  mb-3">
            <div className="d-flex justify-content-between align-items-center ">
              <h6 className="p-0 m-0 font-150 font-bold  ">
                {getLabel("photos", initialLanguage)}
              </h6>
              <div className=" form-check form-switch">
                <input
                  className="form-check-input border border-2 system-control"
                  type="checkbox"
                  id="flexSwitchCheckDefault"
                />
              </div>
            </div>
            <p className="system-muted-5 font-90 font-normal m-0 p-0">
              {getLabel("photosDescription", initialLanguage)}
            </p>
            <div className="row my-3 gap-2 gap-sm-0">
              <div className="col-auto">
                <ImageNewUploder
                  name="Upload"
                  headerText="Upload your photo here"
                  handleUploadImage={handleUploadImage}
                />
              </div>

              {NoticeData?.is_single === true && (
                <div className="col-auto">
                  <button
                    type="button"
                    className="btn btn-lg system-outline-primary font-bold pointer"
                    onClick={() => {
                      setheaderChnagePage(true);
                      setSelectImage("aaaa");
                    }}
                  >
                    {getLabel("addPhotosFromProfile", initialLanguage)}
                  </button>
                </div>
              )}
            </div>
            <FullScreenModal
              show={showProfileShow}
              onClose={() => {
                setshowProfileShow(false);
              }}
              size="lg"
              title={`${getLabel("showImage", initialLanguage)}`}
            >
              <div className="d-flex justify-content-center">
                <div className="fullmodal-image-container">
                  <Image
                    src={showProfileUrl}
                    alt="memorialprofile"
                    className="pointer image-contain"
                    width={750}
                    height={600}
                  />
                </div>
              </div>
            </FullScreenModal>
            {mediaPagenationSkeleton || isCaptionLoading ? (
              <>
                <div className="d-flex flex-wrap gap-2 gap-sm-3 mb-4 justify-content-start">
                  <PhotosAndVideosSkelton
                    loopTimes={8}
                    className={"profile-photo-container"}
                  />
                </div>
              </>
            ) : (
              <>
                <div className="d-flex flex-wrap gap-2 gap-sm-3  justify-content-start">
                  {getNoticeMediaData?.data?.map((item: any) => (
                    <div className="d-flex flex-column">
                      <div className="position-relative  d-flex justify-content-center group-photo-container system-filter-secondary mb-1 ">
                        <Image
                          src={draggable}
                          alt="noticeprofile"
                          className="position-absolute top-0 start-0 m-2 move"
                        />
                        <Image
                          src={item.url}
                          alt="noticeprofile"
                          className="pointer image"
                          width={133}
                          height={133}
                          style={{ borderRadius: "12px", border: "2px" }}
                          onClick={() => {
                            setshowProfileShow(true);
                            setshowProfileUrl(item.url);
                          }}
                        />

                        <Popover
                          className={
                            popoverShow ? "d-none" : "d-block custom-dropdown"
                          }
                          trigger="click"
                          position="right"
                          content={
                            <div className="popover-content">
                              <div
                                onClick={() => {
                                  setPopoverShow(true);
                                }}
                              >
                                <ImageNewUploder
                                  name="Edit"
                                  headerText="Edit Your Photo"
                                  textOnly={true}
                                  edit={true}
                                  handleUploadImage={(file) =>
                                    editUploadImage(item.uuid, file)
                                  }
                                  imageUrl={item.url}
                                  showPopupFun={(val) => {
                                    console.log("val", val);
                                    setPopoverShow(val);
                                  }}
                                />
                              </div>
                              <li
                                className="pointer p-0 m-0 px-1 px-sm-4"
                                onClick={() => {
                                  deleteFun(item.uuid);
                                  setPopoverShow(true);
                                }}
                              >
                                {getLabel("delete", initialLanguage)}
                              </li>
                            </div>
                          }
                        >
                          <Image
                            src={
                              crossBtnEnableId == item.uuid
                                ? crossBtnEnable
                                  ? closebtn
                                  : edit
                                : edit
                            }
                            alt="memorialprofile"
                            className="img-fluid position-absolute top-0 end-0 m-2 pointer"
                            onClick={() => {
                              crossBtnEnable
                                ? setPopoverShow(true)
                                : setPopoverShow(false);
                            }}
                          />
                        </Popover>
                      </div>

                      <div className="">
                        <input
                          type="text"
                          className="border-0 text-truncate mx-upload  customPenCursor "
                          placeholder={"Add caption"}
                          onChange={(e) => setCaption(e.target.value)}
                          onClick={() => {
                            setCaption(item.caption),
                              setCaptioncpy(item.caption);
                            setCrossBtnEnableId(item.uuid);
                            setCrossBtnEnable(true);
                          }}
                          onBlur={() => updateCaption(item.uuid)}
                          defaultValue={item.caption}
                        />
                      </div>
                    </div>
                  ))}
                </div>
              </>
            )}
          </div>
        )}
        {/* --------------Live Streaming ----------- */}
        {!NoticeData?.loading || isvideoLoading ? (
          <>
            <NoticeVidepLiveSkelton />
          </>
        ) : (
          <>
            <div className="border bg-white px-4 py-3  mb-3 ">
              <div className="d-flex justify-content-between align-items-center ">
                <h6 className="p-0 m-0 font-150 font-bold">
                  {getLabel("liveStreaming", initialLanguage)}
                </h6>
                <div className="form-check form-switch">
                  <input
                    className="form-check-input border border-2 system-control"
                    type="checkbox"
                    id="flexSwitchCheckDefault"
                    // placeholder="Paste a link to embed content from another site and press Enter"
                  />
                </div>
              </div>
              <p className="system-muted-5 font-90 font-normal m-0 p-0 mb-4 lh-sm">
                {getLabel("liveStreamingDescription", initialLanguage)}
              </p>
              <p className="font-semibold m-0 p-0 pb-1 ">
                {getLabel(
                  "liveStreamingDescriptionDescription",
                  initialLanguage
                )}
              </p>
              <div className="mb-4 px-3 py-2 system-success-light rounded d-flex justify-content-between align-items-center">
                <div className=" ">
                  <p className="p-0 m-0 font-bolder pb-1 ">
                    {getLabel("liveStreamingInformation", initialLanguage)}
                  </p>
                  <p className="p-0 m-0 system-text-dark-80 font-90 font-normal ">
                    {getLabel(
                      "liveStreamingInformationDescription",
                      initialLanguage
                    )}
                  </p>
                </div>
                <button
                  type="button"
                  className="btn btn-sm border system-secondary-thin py-1 px-2 me-2 font-70 font-semibold"
                >
                  {getLabel("view", initialLanguage)}
                </button>
              </div>
              <p className=" font-semibold m-0 p-0">
                {getLabel("liveStreamingURL", initialLanguage)}
              </p>
              <input
                type="text"
                className="form-control border border-2 system-control"
                placeholder={`${getLabel(
                  "liveStreamingURLPlaceholder",
                  initialLanguage
                )}`}
                onChange={(e) => handleVideoInputChange(e, "liveUrl")}
                // defaultValue={liveUrlErr ? liveUrl : NoticeData?.live_video}
                defaultValue={liveUrl}
              />

              {liveUrlErr && <div className="text-danger">{liveUrlErr}</div>}
              <div className="row m-0 p-0 pt-1 pb-1">
                <div className=" col-12 p-0 m-0   font-90 font-normal lh-1 system-muted-5">
                  {getLabel("liveStreamingURLDescription", initialLanguage)}
                </div>
              </div>
            </div>
            {/* --------------Videos----------- */}
            <div className="border px-4 py-3 mb-3 bg-white">
              <div className="d-flex justify-content-between align-items-center">
                <h6 className="p-0 m-0 ">
                  <strong className=" font-150 font-bold ">
                    {getLabel("videos", initialLanguage)}
                  </strong>
                </h6>
                <div className=" form-check form-switch">
                  <input
                    className="form-check-input border border-2 system-control"
                    type="checkbox"
                    id="flexSwitchCheckDefault"
                  />
                </div>
              </div>
              <p className="system-muted-5 font-90 font-normal m-0 p-0 mb-3">
                {getLabel("videosDescription", initialLanguage)}
              </p>
              <p className="m-0 p-0 mb-1 font-semibold">
                {getLabel("videoURL", initialLanguage)}
              </p>
              <input
                type="text"
                className="form-control border border-2 system-control "
                placeholder="Paste a link to embed content from another site and press Enter"
                onChange={(e) => handleVideoInputChange(e, "videoUrl")}
                // defaultValue={videoUrlErr ? videoUrl : NoticeData?.video_url}
                defaultValue={videoUrl}
              />
              {videoUrlErr && <div className="text-danger">{videoUrlErr}</div>}
              <div className="row m-0 p-0 pt-1">
                <div className=" col-12 p-0 m-0  font-90 font-normal line-height-14  system-muted-5 ">
                  {getLabel("videoDescription", initialLanguage)}
                </div>
              </div>
              <button
                type="button"
                className="btn btn-system-primary my-4 mb-4"
                onClick={() => patchLiveStreamVideos()}
              >
                {getLabel("save", initialLanguage)}
              </button>
            </div>
            {/* ------------------------- */}
            <div className="mb-4">
              <p
                className="m-0 p-0  text-end font-80 fw-normal text-truncate"
                style={{ maxWidth: "100%" }}
              >
                {" "}
                <div className="row justify-content-end">
                  <div className="col-md-9 col-11   text-end justify-content-end m-0 p-lg-0 me-1 text-truncate col-lg-7">
                    <span>{getLabel("createdBy", initialLanguage)} </span>

                    <span
                      className="system-text-primary "
                      style={{ maxWidth: "100%" }}
                    >
                      {/* JenniferJennifer */}
                      {NoticeData?.["created_by"]}
                    </span>
                    {/* <span>, </span> */}
                  </div>
                  <div className="col-md-auto  m-0 p-0 pe-3 col-lg-auto">
                    {" "}
                    <span className="d-none d-lg-inline">, </span>
                    <span> {MonthTimeFormat(NoticeData?.created_at)}</span>{" "}
                    <span>GMT</span>
                  </div>
                </div>
                {/* <span>, </span>
                <span>19:47</span> */}
              </p>
              <p
                className="m-0 p-0 text-end  font-80 fw-normal text-truncate"
                style={{ maxWidth: "100%" }}
              >
                {" "}
                <div className="row justify-content-end">
                  <div className="col-md-9 col-11   text-end justify-content-end m-0 p-lg-0 me-1 text-truncate col-lg-7">
                    <span>{getLabel("lastUpdatedBy", initialLanguage)} </span>
                    <span
                      className="system-text-primary"
                      style={{ maxWidth: "100%" }}
                    >
                      Kishor
                    </span>
                    {/* <span>, </span> */}
                  </div>
                  <div className="col-md-auto  m-0 p-0 pe-3 col-lg-auto">
                    <span className="d-none d-lg-inline">, </span>
                    <span>
                      {" "}
                      {MonthTimeFormat(NoticeData?.last_modified_at)}
                    </span>
                    {/* <span>, </span>
                 <span>19:47</span> */}
                    <span>GMT</span>
                  </div>
                </div>
              </p>
            </div>
          </>
        )}
      </div>
    </>
  );
};
export default NoticeDetails;
