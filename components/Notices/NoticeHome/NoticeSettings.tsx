import React, { useContext, useEffect, useRef, useState } from "react";
import Image from "next/image";
import Rectanglebox from "../../../public/Rectanglebox.svg";
import { getAllRelationships, relationsSelector } from "@/redux/relationships";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import {
  getAllNoticeTemplates,
  templatesSelector,
} from "@/redux/notice-templates";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import {
  getAllNoticeTemplateClipArts,
  noticetemplatesClipArtSelector,
} from "@/redux/notice-templates-cliparts";
import {
  getAllNoticeTemplateFrames,
  noticetemplatesFrameSelector,
} from "@/redux/notice-templates-frames";
import {
  getAllNoticeTemplateBorders,
  noticetemplatesBorderSelector,
} from "@/redux/notice-templates-borders";
import { Notification } from "@arco-design/web-react";
import playIcon from "public/play.svg";
import pauseIcon from "public/pause.svg";
import {
  patcNoticePageBackground,
  patcNoticeSettings,
} from "@/redux/notices";
import FullScreenModal from "@/components/Elements/FullScreenModal";
import { Button } from "react-bootstrap";
import {
  getAllNoticePageBackgrounds,
  noticePageBackgroundsSelector,
} from "@/redux/notice-page-backgrounds";
import NoticeSettingSkelton from "@/components/Skelton/NoticeSettingSkelton";
import BackgroundMusics, {
  ChildModalRef,
} from "@/components/Design/BackgroundMusics";
import { getAllMusics, musicsSelector } from "@/redux/Music";
import PlaceAutocomplete from "@/components/Elements/PlaceAutoComplete";
import CustomImage from "@/components/Elements/CustomImage";
import { getLabel } from "@/utils/lang-manager";
import CustomSelect from "@/components/Arco/CustomSelect";
import CustomSelectDouble from "@/components/Arco/CustomSelectDouble";
import UploadMusics from "@/components/Design/UploadMusics";
import ArcoNextTabs from "@/components/Design/ArcoNextTabs";
import extractFilenameFromUrl from "@/components/TimeConverter/extractFilenameFromUrl";
import { MonthTimeFormat } from "@/components/TimeConverter/MonthTimeFormat";
const NoticeSettings = () => {
  const { handleSave, NoticeData } = useContext(NoticeDataContext);
  const Data = NoticeData;
  const initialLanguage = NoticeData?.initialLanguage
  // const activePageid = NoticeData.notice_template_id;
  const { data: showRelationshipsData } = useAppSelector(relationsSelector);
  const { data: showMusicsData } = useAppSelector(musicsSelector);
  const dispatch = useAppDispatch();
  const loadData = () => {
    dispatch(getAllRelationships());
    dispatch(getAllMusics());
  };
  useEffect(() => {
    loadData();
  }, []);
  //------------------------------------------------template,border,frames,clipart---------------------------------------------
  const [addTemplate, setAddTemplate] = useState(false);
  const [addBorder, setAddBorder] = useState(false);
  const [addClipArt, setAddClipArt] = useState(false);
  const [addFrame, setAddFrame] = useState(false);
  const [pagebackground, setAddPagebackground] = useState(false);
  const [backgroundMusic, setBackgroundMusic] = useState(false);
  const { data: showNoticeTemplatessData } = useAppSelector(templatesSelector);
  // const activePageuuid = Array.isArray(showNoticeTemplatessData)
  //   ? showNoticeTemplatessData.map((item) => String(item.uuid))
  //   : "";
  // const active_notice_template = activePageuuid.length > 0 ? activePageuuid[0] : "";
  const [selectedOptionUUId, setSelectedOptionUUId] = useState(
    Data?.notice_template_uuid
  );


  const [selectedOptionId, setSelectedOptionId] = useState(
    Data?.["notice_template_id"]
  );
  const [selectedBorderId, setSelectedBorderId] = useState(Data?.["border_id"]);
  const [selectedFrameId, setSelectedFrameId] = useState(Data?.["frame_id"]);
  const [selectedClipArtId, setSelectedClipArtId] = useState(
    Data?.["clipart_id"]
  );
  const [selectedPageBackgroundId, setSelectedPageBackgroundId] = useState(
    Data?.["PageBackground_id"]
  );
  const [selectedOptionLink, setSelectedOptionLink] = useState(
    Data?.["template_url"]
  );
  const [selectedOptionAlt, setSelectedOptionAlt] = useState("");
  const [selectedBorderLink, setSelectedBorderLink] = useState(
    Data?.["border_url"]
  );
  const [selectedBorderAlt, setSelectedBorderAlt] = useState("");
  const [selectedBorderUUId, setSelectedBorderUUId] = useState("");
  const [selectedClipArtLink, setSelectedClipArtLink] = useState(
    Data?.["clipart_url"]
  );
  const [selectedClipArtAlt, setSelectedClipArtAlt] = useState("");
  const [selectedClipArtUUId, setSelectedClipArtUUId] = useState("");

  const [selectedFrameLink, setSelectedFrameLink] = useState(
    Data?.["frame_url"]
  );
  const [selectedFrameAlt, setSelectedFrameAlt] = useState("");
  const [selectedFrameUUId, setSelectedFrameUUId] = useState("");
  //useState(Data?.["PageBackground_id"]);
  const [selectedPageBackgroundLink, setSelectedPageBackgroundLink] = useState(
    Data?.["PageBackground_url"]
  );
  const [selectedPageBackgroundAlt, setSelectedPageBackgroundAlt] =
    useState("");
  const [selectedPageBackgroundUUId, setSelectedPageBackgroundUUId] =
    useState("");
  const [selectedBackgroundMusicId, setSelectedBackgroundMusicId] = useState(
    Data?.["PageBackground_id"]
  );
  const [selectedBackgroundMusicLink, setSelectedBackgroundMusicLink] =
    useState("");
  const [selectedMusicListLink, setSelectedMusicListLink] = useState("");
  const [selectedBackgroundOptionLink, setSelectedBackgroundOptionLink] =
    useState("");
  const [selectedBackgroundMusicFile, setSelectedBackgroundMusicFile] =
    useState("");
  const [selectedBackgroundMusicAlt, setSelectedBackgroundMusicAlt] =
    useState("");
  const [selectedBackgroundMusicUUId, setSelectedBackgroundMusicUUId] =
    useState("");
  const [birthDeathTitle, setBirthDeathTitles] = useState(
    Data?.birthDeathTitle_id
  );
  const [musicTab, setMusicTab] = useState("select");
  const [uploading, setUploading] = useState<boolean>(false);

  useEffect(() => {
    setSelectedOptionUUId(Data?.notice_template_uuid)
    setSelectedOptionId(Data?.["notice_template_id"]);
    setSelectedFrameId(Data?.["frame_id"]);
    setSelectedClipArtId(Data?.["clipart_id"]);
    setSelectedBorderId(Data?.["border_id"]);
    setSelectedPageBackgroundId(Data?.["PageBackground_id"]);
    setSelectedClipArtLink(Data?.["clipart_url"]);
    setSelectedOptionLink(Data?.["template_url"]);
    setSelectedFrameLink(Data?.["frame_url"]);
    setSelectedBackgroundMusicLink(Data?.["background_music_url"]);
  }, [Data]);

  useEffect(() => {
    dispatch(
      getAllNoticeTemplates({
        active_partner: ACTIVE_PARTNER,
      })
    );
  }, []);
  const active_notice_template = selectedOptionUUId;
  const loadStaticData = async () => {
    // dispatch(
    //   getAllNoticeTemplates({
    //     active_partner: ACTIVE_PARTNER,
    //   })
    // );
    dispatch(
      getAllNoticeTemplateBorders({
        active_partner: ACTIVE_PARTNER,
        active_notice_template: active_notice_template,
      })
    );
    dispatch(
      getAllNoticeTemplateClipArts({
        active_partner: ACTIVE_PARTNER,
        active_notice_template: active_notice_template,
      })
    );
    dispatch(
      getAllNoticeTemplateFrames({
        active_partner: ACTIVE_PARTNER,
        active_notice_template: active_notice_template,
      })
    );
    dispatch(
      getAllNoticePageBackgrounds({
        active_partner: ACTIVE_PARTNER,
        active_notice_template: active_notice_template,
      })
    );
  };

  useEffect(() => {
    active_notice_template && loadStaticData();
  }, [active_notice_template, selectedOptionUUId, selectedOptionId]);


  console.log("updateData active_notice_template", active_notice_template);
  console.log("updateData selectedOptionUUId", selectedOptionUUId);
  console.log("updateData selectedOptionId", selectedOptionId);
  console.log("updateData selectedFrameId", selectedFrameId);
  console.log("updateData selectedClipArtId", selectedClipArtId);
  console.log("updateData selectedBorderId", selectedBorderId);

  //template
  const handleOptionSelection = (
    optionId: number,
    OptionLink: string,
    selectedOptionAlt: string,
    optionUUID: string
  ) => {
    setSelectedOptionId(optionId);
    setSelectedFrameId("");
    setSelectedClipArtId("");
    setSelectedBorderId("");
    setSelectedOptionLink(OptionLink);
    setSelectedOptionAlt(selectedOptionAlt);
    setSelectedOptionUUId(optionUUID);
    setAddTemplate(false);
  };
  //border
  const handleBorderSelection = (
    optionId: number,
    OptionLink: string,
    selectedOptionAlt: string,
    optionUUID: string
  ) => {
    setSelectedBorderId(optionId);
    setSelectedBorderLink(OptionLink);
    setSelectedBorderAlt(selectedOptionAlt);
    setSelectedBorderUUId(optionUUID);
    setAddBorder(false);
  };
  //clipart
  const handleClipArtSelection = (
    optionId: number,
    OptionLink: string,
    selectedOptionAlt: string,
    optionUUID: string
  ) => {
    setSelectedClipArtId(optionId);
    setSelectedClipArtLink(OptionLink);
    setSelectedClipArtAlt(selectedOptionAlt);
    setSelectedClipArtUUId(optionUUID);
    setAddClipArt(false);
  };
  //frame
  const handleFrameSelection = (
    optionId: number,
    OptionLink: string,
    selectedOptionAlt: string,
    optionUUID: string
  ) => {
    setSelectedFrameId(optionId);
    setSelectedFrameLink(OptionLink);
    setSelectedFrameAlt(selectedOptionAlt);
    setSelectedFrameUUId(optionUUID);
    setAddFrame(false);
  };
  const handlePageBackgroundSelection = (
    optionId: number,
    OptionLink: string,
    selectedOptionAlt: string,
    optionUUID: string
  ) => {
    setSelectedPageBackgroundId(optionId);
    setSelectedPageBackgroundLink(OptionLink);
    setSelectedPageBackgroundAlt(selectedOptionAlt);
    setSelectedPageBackgroundUUId(optionUUID);
    setAddPagebackground(false);
  };
  const handleBackgroundMusicSelection = (
    optionId: number,
    OptionLink: string,
    selectedOptionAlt: string,
    optionUUID: string
  ) => {
    setSelectedBackgroundMusicId(optionId);
    setSelectedMusicListLink(OptionLink);
    setSelectedBackgroundMusicAlt(selectedOptionAlt);
    setSelectedBackgroundOptionLink(selectedOptionAlt);
    setSelectedBackgroundMusicUUId(optionUUID);
    setBackgroundMusic(false);
  };


  const { data: showTemplatesBorderData } = useAppSelector(
    noticetemplatesBorderSelector
  );
  const { data: showTemplatesClipArtData } = useAppSelector(
    noticetemplatesClipArtSelector
  );
  const { data: showTemplatesFrameData } = useAppSelector(
    noticetemplatesFrameSelector
  );
  const { data: showPageBackgroundData } = useAppSelector(
    noticePageBackgroundsSelector
  );
  const handlePageBackgroundUpload = (url: any) => {
    handleClick();
    const updatedData = {
      page_background_url: url,
    };
    dispatch(
      patcNoticePageBackground({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        updatedData,
        active_notice: Data.uuid as unknown as string,
      })
    ).then((response: any) => {
      handleSave();
      if (response.payload.success == true) {
        setAddPagebackground(false);
        Notification.success({
          title: "Page background Updated Successfully",
          duration: 3000,
          content: undefined,
        });
      } else {
        Notification.error({
          title: "Page background Updated Failed",
          duration: 3000,
          content: undefined,
        });
      }
    });
  };
  const backgroundMusicRef = React.createRef<ChildModalRef>();
  const handleClick = () => {
    if (backgroundMusicRef.current) {
      backgroundMusicRef.current.callModalFunction(handleChildResponse);
    }
  };
  const handleChildResponse = () => {
    setUploading(true);
  };
  const handleUploadMusic = (file: any) => {
    // console.log(file);
    setSelectedBackgroundMusicLink(file);
    setSelectedBackgroundMusicFile(file);
    file && setBackgroundMusic(false);
    file && setUploadMusic(false);
    // handleMusicSettingsUpdate(file);
    setUploading(false);
    setSelectedBackgroundMusicId("");
    setSelectedBackgroundMusicAlt("");
    setSelectedBackgroundOptionLink("");
    setSelectedBackgroundMusicUUId("");
  };
  const handleUploadApply = () => {
    setSelectedBackgroundMusicLink(Data?.["background_music_url"]);
    setSelectedBackgroundMusicFile("");
    setSelectedBackgroundOptionLink("");
    setSelectedMusicListLink("");
    setSelectedBackgroundMusicId("");
    setBackgroundMusic(false);
  };
  const [showButton, setShowButton] = useState(true);
  const handleSelectMusic = (file: any) => {
    file && setShowButton(false);
  };
  const [familyLocation, setFamilyLocation] = useState(
    Data?.["family_location"]
  );
  const handleSuggestionClick1 = (description: string) => {
    setFamilyLocation(description);
  };
  const [familyOverideLocation, setOverideFamilyLocation] = useState(
    Data?.["custom_family_location"]
  );
  const handleSuggestionClick2 = (description: string) => {
    setOverideFamilyLocation(description);
  };
  const [visibility, setvisibility] = useState(Data?.visibility);
  const [relationship, setRelationship] = useState(Data?.relationship);
  useEffect(() => {
    setFamilyLocation(Data?.["family_location"]);
    setOverideFamilyLocation(Data?.["custom_family_location"]);
  }, [Data]);
  const now = new Date();
  const formattedDate = `${now.getFullYear()}-${String(
    now.getMonth() + 1
  ).padStart(2, "0")}-${String(now.getDate()).padStart(2, "0")} ${String(
    now.getHours()
  ).padStart(2, "0")}:${String(now.getMinutes()).padStart(2, "0")}:${String(
    now.getSeconds()
  ).padStart(2, "0")}`;
  const handleNoticeSettingsUpdate = () => {
    // handleClick();
    setIsLoading(true);
    const updatedData = {
      notice_template_id: selectedOptionId,
      frame_id: selectedFrameId,
      clip_art_id: selectedClipArtId,
      border_id: selectedBorderId,
      page_background_id: selectedPageBackgroundId,
      family_address: familyLocation,
      custom_family_address: familyOverideLocation,
      birth_and_death_title_id: birthDeathTitle,
      background_music_url: selectedBackgroundMusicLink,
      visibility_id: visibility,
      auto_hide: isHided,
      hide_dob_and_dod: isHidedDob,
      hide_at: formattedDate,
    };
    dispatch(
      patcNoticeSettings({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        updatedData,
        active_notice: Data.uuid as unknown as string,
      })
    )
      .then((response: any) => {
        if (response.payload.success == true) {
          handleSave();
          setSelectedBackgroundMusicFile("");
          setSelectedBackgroundOptionLink("");
          setSelectedBackgroundMusicId("");
          setSelectedMusicListLink("");
          setIsDataLoading(true);
          setUploading(false);
          setAddPagebackground(false);
          Notification.success({
            title: "Notice Settings has been Updated Successfully",
            duration: 3000,
            content: undefined,
          });
        } else {
          (response.payload.code != 422) && Notification.error({
            title: "Notice Settings Update Failed",
            duration: 3000,
            content: undefined,
          });
        }
      })
      .finally(() => {
        setIsLoading(false);
      });
  };
  useEffect(() => {
    setIsHided(Data?.auto_hide);
    setIsHidedContacts(true);
    setIsHidedDob(Data?.hide_dob_and_dod);
  }, [NoticeData]);
  const [isLoading, setIsLoading] = useState(false);
  const [isHided, setIsHided] = useState(Data?.auto_hide);
  const [isHidedContacts, setIsHidedContacts] = useState(true);
  const [isHidedDob, setIsHidedDob] = useState(Data?.hide_dob_and_dod);
  const audioRef = useRef<HTMLAudioElement | null>(null);
  const [isPlaying, setIsPlaying] = useState(false);
  const playAudio = () => {
    if (audioRef.current) {
      if (isPlaying) {
        audioRef.current.pause();
      } else {
        audioRef.current.play().catch((error) => {
          console.error("Audio playback error:", error);
        });
      }
      setIsPlaying(!isPlaying);
    }
  };
  const [isDataLoading, setIsDataLoading] = useState(false);
  useEffect(() => {
    if (!Data?.loading) {
      setIsDataLoading(true);
    }
    const timer = setInterval(() => {
      if (!Data?.loading) {
        setIsDataLoading(true);
      } else {
        setTimeout(() => {
          setIsDataLoading(false);
        }, 100);
        clearInterval(timer);
      }
    }, 100);
    return () => {
      clearInterval(timer);
    };
  }, [Data]);
  const [uploadMusic, setUploadMusic] = useState(false);

  return (
    <>
      {isDataLoading || isLoading ? (
        <NoticeSettingSkelton />
      ) : (
        <>
          <FullScreenModal
            show={addTemplate}
            size="xl"
            title={`${getLabel("templates", initialLanguage)}`}
            description={`${getLabel("templatesDescription", initialLanguage)}`}
            onClose={() => setAddTemplate(false)}
          // footer={
          //   <Button
          //     className="btn-system-primary pointer"
          //     onClick={() =>
          //       selectedOptionId
          //         ? selectedOptionId === Data?.["notice_template_id"]
          //           ? Notification.warning({
          //             title: "Already selected Template",
          //             duration: 3000,
          //             content: undefined,
          //           })
          //           : handleTemplateUpdate()
          //         : Notification.error({
          //           title: "select Template  to Submit",
          //           duration: 3000,
          //           content: undefined,
          //         })
          //     }
          //   >
          //     {getLabel("submit",initialLanguage)}
          //   </Button>
          // }
          >
            {/* <Templates
              Data={showNoticeTemplatessData}
              defaultOptionId={Data?.["notice_template_id"]}
              onOptionSelect={handleOptionSelection}
            /> */}
            <ArcoNextTabs
              Data={showNoticeTemplatessData}
              // defaultOptionId={Data?.["template_id"]}
              defaultOptionId={selectedOptionId}
              onOptionSelect={handleOptionSelection}
            />
          </FullScreenModal>

          <FullScreenModal
            show={addBorder}
            size="xl"
            title={`${getLabel("borders", initialLanguage)}`}
            description={`${getLabel("templatesDescription", initialLanguage)}`}
            onClose={() => setAddBorder(false)}
          // footer={
          //   <Button
          //     className="btn-system-primary pointer"
          //     onClick={() =>
          //       selectedBorderId
          //         ? selectedBorderId === Data?.["border_id"]
          //           ? Notification.warning({
          //             title: "Already selected Border",
          //             duration: 3000,
          //             content: undefined,
          //           })
          //           : handleTemplateBorderUpdate()
          //         : Notification.error({
          //           title: "select Border  to Submit",
          //           duration: 3000,
          //           content: undefined,
          //         })
          //     }
          //   >
          //     {getLabel("submit",initialLanguage)}
          //   </Button>
          // }
          >
            <ArcoNextTabs
              Data={showTemplatesBorderData}
              // defaultOptionId={Data?.["template_id"]}
              defaultOptionId={selectedBorderId}
              onOptionSelect={handleBorderSelection}
            />
            {/* <Templates
              Data={showTemplatesBorderData}
              defaultOptionId={Data?.["border_id"]}
              onOptionSelect={handleBorderSelection}
            /> */}
          </FullScreenModal>

          <FullScreenModal
            show={addClipArt}
            size="xl"
            title={`${getLabel("clipArts", initialLanguage)}`}
            description={`${getLabel("templatesDescription", initialLanguage)}`}
            onClose={() => setAddClipArt(false)}
          // footer={
          //   <Button
          //     className="btn-system-primary pointer"
          //     onClick={() =>
          //       selectedClipArtId
          //         ? selectedClipArtId === Data?.["clipart_id"]
          //           ? Notification.warning({
          //             title: "Already selected Clipart",
          //             duration: 3000,
          //             content: undefined,
          //           })
          //           : handleTemplateClipArtUpdate()
          //         : Notification.error({
          //           title: "select Clipart  to Submit",
          //           duration: 3000,
          //           content: undefined,
          //         })
          //     }
          //   >
          //     {getLabel("submit",initialLanguage)}
          //   </Button>
          // }
          >
            <ArcoNextTabs
              Data={showTemplatesClipArtData}
              defaultOptionId={selectedClipArtId}
              onOptionSelect={handleClipArtSelection}
            />
            {/* <Templates
              Data={showTemplatesClipArtData}
              defaultOptionId={Data?.["clipart_id"]}
              onOptionSelect={handleClipArtSelection}
            /> */}
          </FullScreenModal>

          <FullScreenModal
            show={addFrame}
            size="xl"
            title={`${getLabel("frames", initialLanguage)}`}
            description={`${getLabel("templatesDescription", initialLanguage)}`}
            onClose={() => setAddFrame(false)}
          // footer={
          //   <Button
          //     className="btn-system-primary pointer"
          //     onClick={() =>
          //       selectedFrameId
          //         ? selectedFrameId === Data?.["frame_id"]
          //           ? Notification.warning({
          //             title: "Already selected Frame",
          //             duration: 3000,
          //             content: undefined,
          //           })
          //           : handleTemplateFrameUpdate()
          //         : Notification.error({
          //           title: "select Frame  to Submit",
          //           duration: 3000,

          //           content: undefined,
          //         })
          //     }
          //   >
          //     {getLabel("submit",initialLanguage)}
          //   </Button>
          // }
          >
            {/* <Templates
              Data={showTemplatesFrameData}
              defaultOptionId={Data?.["frame_id"]}
              onOptionSelect={handleFrameSelection}
            /> */}
            <ArcoNextTabs
              Data={showTemplatesFrameData}
              defaultOptionId={selectedFrameId}
              onOptionSelect={handleFrameSelection}
            />
          </FullScreenModal>

          <FullScreenModal
            show={pagebackground}
            size="xl"
            title={`${getLabel("pageBackgrounds", initialLanguage)}`}
            description={`${getLabel("templatesDescription", initialLanguage)}`}
            onClose={() => setAddPagebackground(false)}
          // footer={
          //   <Button
          //     className="btn-system-primary pointer"
          //     onClick={() =>
          //       selectedPageBackgroundId
          //         ? selectedPageBackgroundId === Data?.["PageBackground_id"]
          //           ? Notification.warning({
          //             title: "Already selected Background",
          //             duration: 3000,
          //             content: undefined,
          //           })
          //           : handlePageBackgroundUpdate()
          //         : Notification.error({
          //           title: "Select Background to Submit",
          //           duration: 3000,
          //           content: undefined,
          //         })
          //     }
          //   >
          //     {getLabel("submit",initialLanguage)}
          //   </Button>
          // }
          >
            {/* <Templates
              Data={showPageBackgroundData}
              defaultOptionId={Data?.["PageBackground_id"]}
              onOptionSelect={handlePageBackgroundSelection}
            /> */}
            <ArcoNextTabs
              Data={showPageBackgroundData}
              defaultOptionId={selectedPageBackgroundId}
              onOptionSelect={handlePageBackgroundSelection}
            />
          </FullScreenModal>
          <FullScreenModal
            show={backgroundMusic}
            size="lg"
            description="This will be visible in the notice page"
            title={
              !uploading
                ? `${getLabel("backgroundMusic", initialLanguage)}`
                : `${getLabel("backgroundMusicUploading", initialLanguage)}`
            }
            // description="
            // We support only mp3 and wave audio format. Maximum 10mb allowed"
            onClose={() => {
              setBackgroundMusic(false);
              setShowButton(true);
            }}
          // footer={
          //   !uploading && (
          //     <Button
          //       className="btn-system-primary pointer my-2"
          //       disabled={showButton}
          //       onClick={() => handleClick()}
          //     >
          //       {getLabel("submit",initialLanguage)}
          //     </Button>
          //   )
          // }
          >
            <BackgroundMusics
              Data={showMusicsData?.data}
              // waiting for api change------- Data?.["background_music_id"]//
              defaultOptionId={selectedBackgroundMusicId}
              defaultOptionLink={selectedBackgroundMusicLink}
              onOptionSelect={handleBackgroundMusicSelection}
              tab={musicTab}
              ref={backgroundMusicRef}
              handleUploadMusic={handleUploadMusic}
              handleSelectMusic={handleSelectMusic}
              handleUploadApply={handleUploadApply}
              classname={"select"}
            />{" "}
          </FullScreenModal>
          <FullScreenModal
            show={uploadMusic}
            description="This will be visible in the notice page"
            title={
              !uploading
                ? `${getLabel("customMusic", initialLanguage)}`
                : `${getLabel("customMusicUploading", initialLanguage)}`
            }
            // description="
            // We support only mp3 and wave audio format. Maximum 10mb allowed"
            onClose={() => {
              setUploadMusic(false);
              setShowButton(true);
            }}
            footerhide={!showButton ? false : true}
            footer={
              !uploading &&
              !showButton && (
                <Button
                  className="btn-system-primary pointer "
                  disabled={showButton}
                  onClick={() => handleClick()}
                >
                  {getLabel("submit", initialLanguage)}
                </Button>
              )
            }
          >
            <UploadMusics
              Data={showMusicsData?.data}
              // waiting for api change------- Data?.["background_music_id"]//
              defaultOptionId={Data?.["background_music_id"]}
              defaultOptionLink={Data?.["background_music_url"]}
              onOptionSelect={handleBackgroundMusicSelection}
              tab={musicTab}
              ref={backgroundMusicRef}
              handleUploadMusic={handleUploadMusic}
              handleSelectMusic={handleSelectMusic}
            />
          </FullScreenModal>

          <div className="">
            <div className="border  bg-white p-4 ps-sm-4 pe-md-5 mb-3">
              <div className="row  align-items-start  m-0 mb-4">
                <div className="col-md-3 pe-2 col-12 p-0 font-bold  pb-sm-2 pb-2 pb-md-0">
                  {getLabel("noticeDecoration", initialLanguage)}
                </div>
                <div className="col-md-9 p-0">
                  <div className="d-flex justify-content-between  text-center flex-wrap ">
                    <div
                      className="d-flex flex-column justify-content-center"
                      onClick={() => setAddTemplate(true)}
                    >
                      <div className=" ">
                        <CustomImage
                          src={selectedOptionLink}
                          alt={NoticeData?.notice_template_alt}
                          className="image"
                          width={70}
                          height={70}
                          divClass="decoration-img-container rounded d-flex align-items-center justify-content-center system-filter-secondary  position-relative"
                        />
                      </div>
                      <p className="font-normal font-90 system-text-primary pt-1 pointer">
                        {Data?.["notice_template_id"]
                          ? `${getLabel("changeTemplate", initialLanguage)}`
                          : `${getLabel("browseTemplate", initialLanguage)}`}
                      </p>
                    </div>
                    <div
                      onClick={() => setAddFrame(true)}
                      className="d-flex flex-column justify-content-center pointer"
                    >
                      <div className=" ">
                        <CustomImage
                          src={selectedFrameLink}
                          alt={NoticeData?.frame_alt}
                          className="image"
                          width={70}
                          height={70}
                          divClass="decoration-img-container rounded d-flex align-items-center justify-content-center system-filter-secondary  position-relative"
                        />
                      </div>
                      <p className="font-normal font-90 system-text-primary pt-1 pointer">
                        {Data?.["frame_id"]
                          ? " Change  Frame"
                          : " Browse Frame"}
                      </p>
                    </div>
                    <div
                      onClick={() => setAddClipArt(true)}
                      className="d-flex flex-column justify-content-center pointer"
                    >
                      <div className="">
                        <CustomImage
                          src={selectedClipArtLink}
                          alt={NoticeData?.clipart_alt}
                          className="image"
                          width={70}
                          height={70}
                          divClass="decoration-img-container rounded d-flex align-items-center justify-content-center system-filter-secondary  position-relative"
                        />
                      </div>
                      <p className="font-normal font-90 system-text-primary pt-1 pointer">
                        {Data?.["clipart_id"]
                          ? `${getLabel("changeClipArt", initialLanguage)}`
                          : `${getLabel("browseClipArt", initialLanguage)}`}
                      </p>
                    </div>
                    <div
                      onClick={() => setAddBorder(true)}
                      className="d-flex flex-column justify-content-center pointer"
                    >
                      <div className="">
                        <CustomImage
                          src={selectedBorderLink}
                          alt={NoticeData?.border_alt}
                          className="image"
                          width={70}
                          height={70}
                          divClass="decoration-img-container rounded d-flex align-items-center justify-content-center system-filter-secondary  position-relative"
                        />
                      </div>
                      <p className="font-normal font-90 system-text-primary pt-1 pointer">
                        {Data?.["border_id"]
                          ? `${getLabel("changeBorder", initialLanguage)}`
                          : `${getLabel("browseBorder", initialLanguage)}`}
                      </p>
                    </div>
                  </div>
                </div>
              </div>

              <div className="row align-items-start p-0 m-0 mb-4">
                <div className="col-md-3 pe-2 col-12 p-0 font-bold  pb-sm-2 pb-2 pb-md-0">
                  {getLabel("pageBackground", initialLanguage)}
                </div>
                <div className="col-md-9 p-0">
                  <div className="system-text-primary">
                    <div className="">
                      <CustomImage
                        src={NoticeData?.PageBackground_url || Rectanglebox}
                        alt={selectedPageBackgroundAlt}
                        className="image rounded"
                        width={70}
                        height={70}
                        divClass="page-background-container system-filter-secondary position-relative"
                      />
                    </div>
                    <div className="mt-1 d-flex gap-3">
                      <div
                        onClick={() => setAddPagebackground(true)}
                        className="font-normal font-90 system-text-primary pointer"
                      >
                        {getLabel("changePageBackground", initialLanguage)}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row align-items-center p-0 m-0 mb-4">
                <div className="col-md-3 pe-2 col-12 font-bold  p-0 text-sm-start">
                  {getLabel("birthDeathTitle", initialLanguage)}
                </div>
                <div className=" col-md-9 col-12   ps-0  mt-1 mt-sm-0 page-arco">
                  <CustomSelectDouble
                    data={Data?.birthDeathTitles}
                    placeholderValue={getLabel("selectBirthandDeathTitle", initialLanguage)}
                    onChange={(value: any) => setBirthDeathTitles(value)}
                    is_search={false}
                    defaultValue={Data?.birthDeathTitle_id}
                    firstvalue={"birth_title"}
                    secondvalue={"death_title"}
                  />
                </div>
              </div>
              <div className="row align-items-center p-0 m-0 mb-4 ">
                <div className="col-md-3 pe-2 col-12 font-bold p-0 text-sm-start ">
                  {getLabel("backgroundMusic", initialLanguage)}
                </div>

                {(selectedBackgroundMusicLink || selectedMusicListLink) && (
                  <div
                    className=" col-md-1 col-2 ps-0  d-flex align-items-center font-normal   p-0 me-2  "
                    style={{ maxWidth: "35px" }}
                  >
                    <div className=" d-flex align-items-start ">
                      <div className="music-container">
                        <Image
                          src={isPlaying ? pauseIcon : playIcon}
                          alt={""}
                          className="sound-container"
                          width={40}
                          height={40}
                          onClick={playAudio}
                        />
                        <audio
                          ref={(audio) => (audioRef.current = audio)}
                          key={
                            selectedBackgroundMusicLink || selectedMusicListLink
                          }
                        >
                          <source
                            src={
                              selectedBackgroundMusicLink ||
                              selectedMusicListLink
                            }
                            type="audio/mp3"
                          />
                          {getLabel("notSupportAudioTag", initialLanguage)}
                        </audio>
                      </div>
                    </div>
                  </div>
                )}
                {selectedBackgroundOptionLink && (
                  <div className="col-md-auto col-10 d-flex p-0 m-0 text-truncate ellipsis">
                    {" "}
                    <p
                      className=" m-0 p-0  text-truncate me-md-2 mu-name "
                      style={{ maxWidth: "100px" }}
                    >
                      {selectedBackgroundOptionLink}
                    </p>
                  </div>
                )}
                {!selectedBackgroundOptionLink &&
                  selectedBackgroundMusicLink && (
                    <div className="col-md-auto col-10 d-flex p-0 m-0 text-truncate ellipsis">
                      {" "}
                      <p
                        className=" m-0 p-0  text-truncate me-md-2 mu-name "
                        style={{ maxWidth: "100px" }}
                      >
                        {extractFilenameFromUrl(selectedBackgroundMusicLink)}
                      </p>
                    </div>
                  )}
                <div className="col-md-3 col-6 d-flex p-0 m-0">
                  <div
                    className="p-2  px-sm-3 me-2 border rounded pointer  "
                    onClick={() => {
                      setBackgroundMusic(true);
                      setMusicTab("Select");
                    }}
                  >
                    {Data?.["background_music_url"] ||
                      selectedBackgroundMusicLink ||
                      selectedBackgroundMusicId
                      ? `${getLabel("changeCustomMusic", initialLanguage)}`
                      : `${getLabel("select", initialLanguage)}`}
                  </div>
                  {(selectedBackgroundMusicFile ||
                    selectedBackgroundOptionLink) && (
                      <div
                        className="p-2  px-sm-3 me-2 border rounded pointer  "
                        onClick={() => {
                          {
                            setSelectedBackgroundMusicLink(
                              Data?.["background_music_url"]
                            );
                            setSelectedBackgroundMusicFile("");
                            setSelectedBackgroundOptionLink("");
                            setSelectedBackgroundMusicId("");
                            setSelectedMusicListLink("");
                          }
                        }}
                      >
                        Clear
                      </div>
                    )}
                </div>

                {/* <div
                    className="p-2 px-sm-3 border rounded pointer"
                    onClick={() => {
                      setUploadMusic(true);
                      setMusicTab("Uploadmusic");
                    }}
                  >
                    {Data?.["background_music_url"] ||
                    selectedBackgroundMusicLink
                      ? `${getLabel("changeCustomMusic",initialLanguage)}`
                      : `${getLabel("uploadCustomMusic",initialLanguage)}`}
                  </div> */}

                <div className="row  m-0 p-0 mt-1">
                  <div className="col-md-3 pe-2  col-12 d-none d-sm-flex p-0 m-0 "></div>
                  <div className="col-md-9  col-12 fw-normal font-80  text-muted-5  ps-0  mt-1 mt-sm-0">
                    {getLabel("backgroundMusicModalDescription", initialLanguage)}
                  </div>
                </div>
              </div>
              <hr className="mb-5 hr-width-form2" />
              <div className="row  align-items-start p-0 m-0 mb-4">
                <div className="col-md-3 pe-2 col-12 font-bold  p-0 text-sm-start"></div>
                <div className="col-md-9 col-12 ps-0  mt-1 mt-sm-0 d-flex   ">
                  <div className=" font-150 font-bold">
                    {getLabel("additionalInformation", initialLanguage)}
                  </div>
                </div>
              </div>
              <div className="row  align-items-start p-0 m-0 mb-4">
                <div className="col-md-3 pe-2 col-12 font-bold p-0 text-sm-start">
                  {getLabel("whatIsCustomerRelationship", initialLanguage)}
                </div>
                <div className="col-md-9 col-12   ps-0  mt-1 mt-sm-0 page-arco">
                  <CustomSelect
                    data={showRelationshipsData}
                    placeholderValue={getLabel("selectRelationship", initialLanguage)}
                    onChange={(value: any) => setRelationship(value)}
                    is_search={true}
                    defaultValue={relationship || ""}
                  />
                </div>
              </div>
              <div className="row  align-items-center p-0 m-0 ">
                <div className="col-md-3 pe-2 col-12 font-bold   p-0 text-sm-start">
                  {getLabel("familyAddress", initialLanguage)}
                </div>
                <div className="col-md-9 col-12   ps-0  mt-1 mt-sm-0 page-arco">
                  <PlaceAutocomplete
                    className="form-control border border-2 system-control"
                    placeholder={"Enter address (Google API)"}
                    onSuggestionClick={handleSuggestionClick1}
                    dvalue={familyLocation}
                  />
                </div>
              </div>
              <div className="row mb-3 m-0 p-0 mt-1">
                <div className="col-md-3 pe-2  col-12 d-none d-sm-flex p-0 m-0 "></div>
                <div className="col-md-9  col-12 font-80 font-normal text-muted-5  ps-0  mt-1 mt-sm-0">
                  {getLabel("familyAddressDescriptionNoticeSettings", initialLanguage)}
                </div>
              </div>
              <div className="row  align-items-start p-0 m-0 ">
                <div className="col-md-3 pe-2 col-12 font-bold p-0 text-sm-start">
                  {getLabel("overrideFamilyAddressForDisplay", initialLanguage)}
                </div>
                <div className="col-md-9 col-12   ps-0  mt-1 mt-sm-0 page-arco">
                  <PlaceAutocomplete
                    className="form-control border border-2 system-control"
                    placeholder={"Enter address"}
                    onSuggestionClick={handleSuggestionClick2}
                    dvalue={familyOverideLocation}
                  />
                </div>
              </div>
              <div className="row mb-4 m-0 p-0">
                <div className="col-md-3 pe-2  col-12 d-none d-sm-flex p-0 m-0 "></div>
                <div className="col-md-9  col-12 font-80 font-normal  text-muted-5  ps-0  mt-1 mt-sm-0">
                  {getLabel("overrideFamilyAddressForDisplayDescription", initialLanguage)}
                </div>
              </div>
              <hr className="mb-5 hr-width-form2" />
              <div className="row  align-items-start p-0 m-0 mb-4">
                <div className="col-md-3 pe-2 col-12 fw-bold  p-0 text-sm-start"></div>
                <div className="col-md-9 col-12 ps-0  mt-1 mt-sm-0 d-flex   ">
                  <div className="font-150 font-bold">
                    {getLabel("additionalSettings", initialLanguage)}
                  </div>
                </div>
              </div>
              <div className="row  align-items-center p-0 m-0 mb-4">
                <div className="col-md-3 pe-2 col-12 font-bold p-0 text-sm-start">
                  {getLabel("noticeVisibility", initialLanguage)}
                </div>
                <div className="col-md-9 col-12   ps-0  mt-1 mt-sm-0 page-arco">
                  <CustomSelect
                    data={Data?.visibilities}
                    placeholderValue={getLabel("selectRelationship", initialLanguage)}
                    onChange={(value: any) => setvisibility(value)}
                    is_search={false}
                    defaultValue={Data?.visibility_id}
                  />
                </div>
              </div>
              <div className="row  align-items-start p-0 m-0 mb-4 ">
                <div className="col-md-3 pe-2 col-12 font-bold   p-0 text-sm-start">
                  <strong> {getLabel("optionalSettings", initialLanguage)} </strong>
                </div>
                <div className="col-md-9 col-12 ps-0 p-0 ">
                  <div className="form-check form-switch  m-0 p-0 d-flex align-items-center mb-3">
                    <input
                      className="form-check-input border border-2 system-control m-0 p-0 pointer"
                      type="checkbox"
                      id="hide-contact"
                      checked={isHided}
                      onChange={() => setIsHided(!isHided)}
                    />
                    <label
                      className="form-check-label font-normal  ms-sm-3 ms-2"
                      htmlFor="hide-contact"
                    >
                      {getLabel("hide", initialLanguage)}{" "}
                      <strong>{getLabel("contactCap", initialLanguage)}</strong>{" "}
                      {getLabel("hideContactsToggleLabel", initialLanguage)}
                    </label>
                  </div>
                  <div className="form-check form-switch m-0 p-0 d-flex align-items-center mb-3">
                    <input
                      className="form-check-input border border-2 system-control m-0 p-0 pointer"
                      type="checkbox"
                      id="hide-notice"
                      checked={isHidedContacts}
                      onChange={() => setIsHidedContacts(!isHidedContacts)}
                    />
                    <label
                      className="form-check-label ms-sm-3 ms-2 font-normal "
                      htmlFor="hide-notice"
                    >
                      {getLabel("hide", initialLanguage)}{" "}
                      <strong>{getLabel("noticeCap", initialLanguage)}</strong>{" "}
                      {getLabel("hideNoticeToggleLabel", initialLanguage)}
                    </label>
                  </div>
                  <div className="form-check form-switch m-0 p-0 d-flex align-items-center">
                    <input
                      className="form-check-input border border-2 system-control m-0 p-0 pointer"
                      type="checkbox"
                      id="hide-date-of-birth"
                      checked={isHidedDob}
                      onChange={() => setIsHidedDob(!isHidedDob)}
                    />
                    <label
                      className="form-check-label ms-sm-3 ms-2 font-normal "
                      htmlFor="hide-date-of-birth"
                    >
                      {getLabel("hideDate", initialLanguage)}
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <div className="mt-4 mb-3 d-block  d-sm-flex  justify-content-sm-between ">
              <div className="mb-3 mb-sm-0 col-2 me-2">
                <button
                  type="button"
                  className="btn btn-system-primary"
                  onClick={() => {
                    handleNoticeSettingsUpdate();
                  }}
                >
                  {getLabel("update", initialLanguage)}
                </button>
              </div>

              <div className=" col-12 col-md-10 col-sm-10  text-end justify-content-end" style={{ maxWidth: "100%" }}>
                <p
                  className="m-0 p-0  text-end font-80 fw-normal text-truncate"
                  style={{ maxWidth: "100%" }}
                >
                  <div className="row justify-content-end">
                    <div className="col-md-9 col-11   text-end justify-content-end m-0 p-lg-0 me-1 text-truncate col-lg-7">
                      <span>{getLabel("createdBy", initialLanguage)} </span>

                      <span
                        className="system-text-primary "
                        style={{ maxWidth: "100%" }}
                      >
                        {/* JenniferJennifer */}
                        {NoticeData?.["created_by"]}
                      </span>
                      {/* <span>, </span> */}
                    </div>
                    <div className="col-md-auto  m-0 p-0 pe-3 col-lg-auto"><span className="d-none d-lg-inline">, </span>
                      <span> {MonthTimeFormat(NoticeData?.created_at)}</span>{" "}
                      <span>GMT</span>
                    </div>
                  </div>
                  {/* <span>, </span>
                <span>19:47</span> */}
                </p>
                <p
                  className="m-0 p-0 text-end  font-80 fw-normal text-truncate"
                  style={{ maxWidth: "100%" }}
                >
                  {" "}
                  <div className="row justify-content-end">
                    <div className="col-md-9 col-11   text-end justify-content-end m-0 p-lg-0 me-1 text-truncate col-lg-7">
                      <span>{getLabel("lastUpdatedBy", initialLanguage)} </span>
                      <span
                        className="system-text-primary"
                        style={{ maxWidth: "100%" }}
                      >
                        Kishor
                      </span>
                      {/* <span>, </span> */}
                    </div>
                    <div className="col-md-auto  m-0 p-0 pe-3 col-lg-auto"><span className="d-none d-lg-inline">, </span>
                      <span>
                        {" "}
                        {MonthTimeFormat(NoticeData?.last_modified_at)}
                      </span>
                      {/* <span>, </span>
                 <span>19:47</span> */}
                      <span>GMT</span>
                    </div>
                  </div>
                </p>
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
};
export async function getServerSideProps({ query }: any) {
  const idQuery = query.id || "";
  return {
    props: {
      idQuery,
    },
  };
}

export default NoticeSettings;
