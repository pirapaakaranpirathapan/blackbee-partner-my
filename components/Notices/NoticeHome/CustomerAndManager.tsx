import PageManagerCard from "@/components/Cards/PageManagerCard";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
import { useRouter } from "next/router";
import React, { useContext, useEffect, useRef, useState } from "react";
import CustomerManagerCard from "@/components/Cards/CustomerManagerCard";
import AlertModalOne from "@/components/Elements/AlertModalOne";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import {
  InviteManagerNotice,
  getAllCustomerManagers,
  pageManagerSelector,
} from "@/redux/page-manager";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import { managerTypesSelector } from "@/redux/managerTypes";
import { Notification } from "@arco-design/web-react";
import CustomerManagerCardInvites from "@/components/Cards/CustomerManagerCardInvites";
import { useApiErrorHandling } from "@/components/Others/useApiErrorHandling";
import { getLabel } from "@/utils/lang-manager";
import PageManagerCardCustomerSkelton from "@/components/Skelton/PageManagerClientSkelton";
import InviteManagerAlertModal from "@/components/Modal/InviteManagerAlertModal";
import { Button } from "react-bootstrap";
import ShimmerEffectText from "@/components/Skelton/ShimmerEffectText";

const CustomerAndManager = () => {
  const [invitePageManager, setInvitePageManager] = useState(false);
  const [email, setEmail] = useState("");
  const [subject, setSubject] = useState("Invitation to Become a Page Manager"); // Default subject value
  const [managerTypeId, setManagerTypeId] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [response, setResponse] = useState("");

  const { handleSave, NoticeData } = useContext(NoticeDataContext);
  const clientData = NoticeData?.client;
  const initialLanguage = NoticeData?.initialLanguage


  const dispatch = useAppDispatch();
  const { errorNotice: apiEror } = useAppSelector(pageManagerSelector);

  const err = useApiErrorHandling(apiEror, response);

  const router = useRouter();
  const uuid = router.query.id;

  const Data = NoticeData;
  const actionBtnRef = useRef<HTMLButtonElement>(null);

  const loadData = () => {
    setIsLoading(true);
    dispatch(
      getAllCustomerManagers({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_page: uuid as unknown as string,
      })
    ).then(() => {
      setIsLoading(false);
    });
  };
  const { data: pageManagerData } = useAppSelector(pageManagerSelector);
  useEffect(() => {
    loadData();
  }, [uuid]);
  const { dataCustomer: customerManagerData } =
    useAppSelector(pageManagerSelector);

  // const handleManagerTypeChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
  //   const selectedValue = e.target.value;
  //   setManagerTypeId(selectedValue);
  // };

  // const HandleInviteManager = async () => {
  //   const CreatedData = {
  //     email: email,
  //     manager_type_id: managerTypeId,
  //   };
  //   // console.log("CreatedData", CreatedData);
  //   dispatch(
  //     InviteManagerNotice({
  //       active_partner: ACTIVE_PARTNER,
  //       active_service_provider: ACTIVE_SERVICE_PROVIDER,
  //       active_id: uuid as unknown as string,
  //       CreatedData,
  //     })
  //   ).then(async (response: any) => {
  //     setEmail("");
  //     setManagerTypeId("")
  //     if (response.payload.success == true) {
  //       setInvitePageManager(false)
  //       setResponse(response)
  //       setEmail("");
  //       setManagerTypeId("")
  //       const link = response?.payload?.data?.data?.url
  //       Notification.success({
  //         title: "Invite Successfully",
  //         duration: 3000,
  //         content: undefined,
  //       });

  //       // =====send mail===========
  //       try {
  //         const text = `
  //           Dear Sir/Madem,

  //           I hope this message finds you well. We would like to extend our invitation to you to join us as a Page Manager for our platform. Your expertise and skills would be a valuable addition to our team.

  //           To accept this invitation, please click on the following link:
  //           ${link}
  //           We look forward to having you on board and working together to achieve our goals.
  //           Thank you for considering our invitation, and we hope to hear from you soon.

  //           Best regards,
  //           Blackbee Partner Portal
  //           Obituary Platform
  //         `;

  //         const response = fetch('/api/sendEmail', {
  //           method: 'POST',
  //           headers: {
  //             'Content-Type': 'application/json',
  //           },
  //           body: JSON.stringify({
  //             to: email,
  //             subject,
  //             text: text,
  //           }),
  //         });

  //         if ((await response).ok) {
  //           console.log('Email sent successfully!');
  //         } else {
  //           console.error('Error sending email:', (await response).text());
  //         }
  //       } catch (error) {
  //         console.error('Error sending email:', error);
  //       }
  //     } else {
  //       Notification.error({
  //         title: "Invite Failed",
  //         duration: 3000,
  //         content: undefined,
  //       });
  //     }
  //   });
  // };
  const HandleInviteManager = (managerTypeId: string, email: string) => {
    InviteManagerMemorial(managerTypeId, email);
  };
  const InviteManagerMemorial = (managerTypeId: string, email: string) => {
    setInviteManagerButton(true);
    const InviteData = {
      email: email,
      manager_type_id: managerTypeId,
    };
    dispatch(
      InviteManagerNotice({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_id: uuid as unknown as string,
        InviteData,
      })
    ).then(async (response: any) => {
      setResponse(response);
      setEmail("");
      setManagerTypeId("");
      if (response.payload.success == true) {
        setInvitePageManager(false);
        setResponse(response);
        setEmail("");
        setManagerTypeId("");

        const link = response?.payload?.data?.data?.url;
        Notification.success({
          title: "Invite Successfully",
          duration: 3000,
          content: undefined,
        });

        // =====send mail===========
        try {
          const text = `
              Dear Sir/Madem,
  
              I hope this message finds you well. We would like to extend our invitation to you to join us as a Page Manager for our platform. Your expertise and skills would be a valuable addition to our team.
  
              To accept this invitation, please click on the following link:
              ${link}
              We look forward to having you on board and working together to achieve our goals.
              Thank you for considering our invitation, and we hope to hear from you soon.
  
              Best regards,
              Blackbee Partner Portal
              Obituary Platform
            `;

          const response = fetch("/api/sendEmail", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              to: email,
              subject,
              text: text,
            }),
          });

          if ((await response).ok) {
            console.log("Email sent successfully!");
          } else {
            console.error("Error sending email:", (await response).text());
          }
        } catch (error) {
          console.error("Error sending email:", error);
        }
      } else {
        setInviteManagerButton(false);
        Notification.error({
          title: "Invite Failed",
          duration: 3000,
          content: undefined,
        });
      }
    });
  };
  const [inviteManagerButton, setInviteManagerButton] = useState(false);
  return (
    <>
      <AlertModalOne
        show={invitePageManager}
        onClose={() => {
          setInvitePageManager(false);
        }}
        position="centered"
        title={`${getLabel("addNewManager", initialLanguage)}`}
        description={`${getLabel("addNewManagerDescription", initialLanguage)}`}
        footer={
          <Button
            ref={actionBtnRef}
            className="btn-system-primary"
            disabled={inviteManagerButton}
          >
            {getLabel("invite", initialLanguage)}
          </Button>
        }
      >
        <InviteManagerAlertModal
          response={response}
          actionBtnRef={actionBtnRef}
          HandleInviteManager={HandleInviteManager}
        />
      </AlertModalOne>
      {/* <AlertModalOne
        show={invitePageManager}
        onClose={() => {
          setInvitePageManager(false);
        }}
        position="centered"
        title="Add new manager"
        description="Invite new user to manage this page"
      >
        <>
          <div className="row">
            <div className="col-12 font-semibold mb-0">{getLabel("email",initialLanguage)}</div>
            <div className="col-12 mb-3">
              <input
                type="text"
                className="form-control border border-2 system-control h-46"
                placeholder="Enter email address to invite"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              {err && (
                <div className="text-danger font-normal">
                  {err.email}
                </div>
              )}
            </div>
          </div>

          <div className="row">
            <div className="col-12 font-semibold mb-0">{getLabel("managerType",initialLanguage)}</div>
            <div className="col-12 mb-4">
              <select
                className="form-select border border-2 bg-white system-control p-2"
                id="dropdownMenuLink"
                value={managerTypeId}
                onChange={handleManagerTypeChange}
              >
                <option value="" disabled selected>
                  {getLabel("selectManagerType",initialLanguage)}
                </option>
                {Array.isArray(ManagerTypesData) &&
                  ManagerTypesData?.map((option: any) => (
                    <option key={option.id} value={option.id} className="text-truncate">
                      {option.name}
                    </option>
                  ))}
              </select>
              {err && (
                <div className="text-danger font-normal">{err.manager_type_id}</div>
              )}
            </div>
          </div>

          <div className="row">
            <div className="col-auto">
              <button
                className="btn btn-system-primary"
                onClick={HandleInviteManager}
              >
                {getLabel("invite",initialLanguage)}
              </button>
            </div>
          </div>
        </>
      </AlertModalOne> */}
      <div className="border  bg-white p-3 mb-3 ">
        {!NoticeData.loading ? (
          <div className="col-lg-7 col-md-6 col-6">
            <div className="mb-4 font-110 font-bold">
              <ShimmerEffectText width="130px" height="20px" />
            </div>
            <div className="font-semibold  mb-1">
              <ShimmerEffectText width="80px" height="15px" />
            </div>
            <div className="font-90 system-muted-2 mb-3">
              <ShimmerEffectText width="" height="12px" />
            </div>
          </div>
        ) : (
          <>
            <h6 className="mb-3 font-110 font-bold">
              {getLabel("customerInformation", initialLanguage)}
            </h6>
            <div className="font-semibold ">{getLabel("owner", initialLanguage)}</div>
            <div className="font-90 system-muted-2 mb-3">
              {getLabel("ownerDescription", initialLanguage)}
            </div>
          </>
        )}
        <div className="mb-4">
          {!NoticeData.loading ? (
            <PageManagerCardCustomerSkelton />
          ) : (
            <CustomerManagerCard
              data={clientData}
              handleSave={handleSave}
              id={NoticeData?.id}
            />
          )}
        </div>
        {!NoticeData.loading ? (
          <div className="col-lg-7 col-md-6 col-6">
            <div className="font-semibold mb-1">
              <ShimmerEffectText width="80px" height="15px" />
            </div>
            <div className="font-90 system-muted-2  mb-3">
              <ShimmerEffectText width="" height="12px" />
            </div>
          </div>
        ) : (
          <>
            <div className="font-semibold">Who can manage this notice?</div>
            <div className="font-90 system-muted-2  mb-3">
              {getLabel("pageMangerDescription", initialLanguage)}
            </div>
          </>
        )}
        <div>
          <CustomerManagerCardInvites
            data={customerManagerData}
            activePage={uuid}
          />
        </div>
        {NoticeData.loading ? (
          <div
            className="pointer system-text-primary font-80 font-bold text-center text-sm-start"
            onClick={() => {
              setInvitePageManager(true);
            }}
          >
            {getLabel("inviteNewManagers", initialLanguage)}
          </div>
        ) : (
          <div className="d-flex justify-content-center  d-sm-block d-md-block">
            <ShimmerEffectText width="120px" height="12px" />
          </div>
        )}
      </div>
    </>
  );
};

export default CustomerAndManager;
