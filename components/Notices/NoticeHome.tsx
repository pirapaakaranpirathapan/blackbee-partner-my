import Image from "next/image";
import React, { useContext, useEffect, useRef, useState } from "react";
import nextIcon from "../../public/nextIcon.svg";
import CustomerAndManager from "./NoticeHome/CustomerAndManager";
import NoticeDeatils from "./NoticeHome/NoticeDetails";
import NoticeSettings from "./NoticeHome/NoticeSettings";
import OrderDetails from "./NoticeHome/OrderDetails";
import ellipsissvg from "../../public/ellipsissvg.svg";
import { useRouter } from "next/router";
import { NoticeDataContext } from "@/pages/notices/[id]/NoticeDataContext";
import GroupProfiles from "../Profile/GroupProfiles";
import { getLabel } from "@/utils/lang-manager";
import ProfileBanner from "../Profile/ProfileBanner";
import FullScreenModal from "../Elements/FullScreenModal";
import { Button } from "react-bootstrap";
import CustomFullScreenModal from "../Elements/CustomFullScreenModal";
import { DatePicker } from "@arco-design/web-react";
import share from "../../public/share-link.svg";
import copy from "../../public/copy-link.svg";
import { timeagoformatter } from "../TimeConverter/timeAgoFormatter";
import PublishNow from "../Elements/PublishNow";
import NoticeTypeSkelton from "../Skelton/NoticeTypeSkelton";
import RouteSkelton from "../Skelton/RouteSkelton";

type Props = {
  tab?: any;
};
const NoticeHome = (props: any, isSingle: any) => {
  const router = useRouter();
  const { query } = router;
  const { NoticeData } = useContext(NoticeDataContext);
  const initialLanguage = NoticeData?.initialLanguage
  const Data = NoticeData;

  const tab = "Notice Details";
  const [activeTab, setActiveTab] = useState(tab);
  const changeTab = (val: any) => {
    setActiveTab(val);
    setActiveTab(val);
    if (val !== null) {
      router.push(
        { pathname: router.pathname, query: { ...query, s: val } },
        undefined,
        { shallow: true }
      );
    }
  };
  useEffect(() => {
    const handleSetActiveKey = () => {
      if (Array.isArray(query?.s)) {
        setActiveTab(query?.s[0] || tab);
      } else {
        setActiveTab(query?.s || tab);
      }
    };
    // Call the function when the component mounts or when the query changes.
    handleSetActiveKey();
  }, [query?.t]);
  const [isGroup, setIsGroup] = useState(false);

  // useEffect(() => {
  //   const handleSetActiveKey = () => {
  //     if (Array.isArray(query?.g)) {
  //       setIsGroup(query?.g[0] == "g" ? true : false);
  //     } else {
  //       setIsGroup(query?.g == "g" ? true : false);
  //     }
  //   };
  //   // Call the function when the component mounts or when the query changes.
  //   handleSetActiveKey();
  // }, [query?.g]);

  useEffect(() => {
    setIsGroup(!NoticeData?.is_single);
  }, [NoticeData]);

  const [showPublish, SetShowPublish] = useState(false);
  const [showSucess, SetShowSucess] = useState(false);
  const [isPublishNow, setIsPublishNow] = useState(false);
  const [isPublisheLater, setIsPublisheLater] = useState(true);
  const [link, setLink] = useState("https://www.bbc.co.uk/news/uk-59761477");
  const handleCopyClick = () => {
    const tempElement = document.createElement("textarea");
    tempElement.value = link;
    document.body.appendChild(tempElement);
    tempElement.select();
    document.execCommand("copy");
    document.body.removeChild(tempElement);
  };
  const handleShareClick = async () => {
    if (navigator.share) {
      try {
        await navigator.share({
          title: document.title,
          text: link,
          url: link,
        });
        console.log("Shared successfully");
      } catch (error) {
        console.error("Error sharing:", error);
      }
    } else {
      console.log("Web Share API not supported in this browser");
    }
  };
  const noticeActionBtnRef = useRef<HTMLButtonElement>(null);
  const handlePublish = (updateData: any) => {
    setIsPublishNow(updateData);
    setIsPublishNow(!updateData);
    updateData && SetShowSucess(true);
    updateData && SetShowPublish(false);
  };
  const [publishResponse, seteditResponse] = useState("");
  return (
    <>
      <CustomFullScreenModal
        show={showPublish}
        title="Publish"
        description={`Notice ID ${Data.uuid}`}
        position="center"
        customSize="custom-size"
        onClose={() => {
          SetShowPublish(false);
          setIsPublisheLater(true);
        }}
        footer={
          <>
            <button
              ref={noticeActionBtnRef}
              className="btn-system-primary btn px-1 px-sm-4 btn-md   font-110 text-dark   me-3 position-relative font-bold "
            >
              Publish
            </button>
          </>
        }
        footerText={<>Timezone: Europe/London</>}
      >
        <PublishNow
          actionBtnRef={noticeActionBtnRef}
          handlePublish={handlePublish}
          response={publishResponse}
        />
      </CustomFullScreenModal>
      <CustomFullScreenModal
        show={showSucess}
        // title="Sucess"
        position="center"
        footerCenter={true}
        customSize="custom-size-2"
        // description="Notice ID 28282822902"
        onClose={() => {
          SetShowSucess(false);
        }}
        footer={
          <>
            <button
              type="button"
              className="btn px-1 px-sm-4 btn-md border shadow-sm font-110 text-dark system-secondary-light btn-system-border me-3 position-relative"
              onClick={handleShareClick}
            >
              <div className="d-flex align-items-center">
                <Image src={share} alt="" className="me-2" /> Share
              </div>
            </button>

            <button
              type="button"
              className="btn px-1 px-sm-4 btn-md border shadow-sm font-110 text-dark system-secondary-light btn-system-border me-3"
              onClick={handleCopyClick}
            >
              Copylink
            </button>
          </>
        }
      >
        <div className="d-flex flex-column mt-3">
          <>
            <div className="d-flex flex-column align-items-center justify-content-center success-publish-msg mb-5">
              <p className="p-0 m-0">Great!</p>
              <p className="p-0 m-0">Notice successfully published</p>
            </div>
            <div className="success-publish-txt mb-4 text-center ">
              The link has been notified to the customer via SMS and registered
              Email.
            </div>

            <div className="d-flex flex-column align-items-center  success-publish-txt mb-5 ">
              <p className="p-0 m-0">View notice via this link</p>
              <span>
                <a
                  href=" https://www.bbc.co.uk/news/uk-59761477"
                  className="a-tag-black p-0 m-0 me-2"
                >
                  https://www.bbc.co.uk/news/uk-59761477
                </a>
                <Image
                  src={copy}
                  alt={""}
                  onClick={handleCopyClick}
                  className="toggle"
                />
              </span>
            </div>
          </>
        </div>
      </CustomFullScreenModal>

      <div className="bg-light px-md-4 mx-1 my-2">
        {!NoticeData.loading ? (
          <div className="route-content px-3"> <RouteSkelton /> </div>
        ) : (
          <div className="route-content">
            <p className="font-80 system-icon-secondary">
              <span>
                <a href="/dashboard" className="a-tag-disable">
                  {getLabel("home", initialLanguage)}
                </a>
                <Image src={nextIcon} alt="" className="mx-2" />
              </span>
              <span>
                <a href="/notices" className="a-tag-disable">
                  {getLabel("notices", initialLanguage)}
                </a>
                <Image src={nextIcon} alt="" className="mx-2" />
              </span>
              <span className="">{NoticeData?.uuid} </span>
            </p>
          </div>
        )}

        {NoticeData?.is_single === false ? <GroupProfiles /> : <></>}
        {/* {isGroup ? <GroupProfiles /> : <></>} */}
        {!NoticeData.loading ? (
          <NoticeTypeSkelton />
        ) : (
          <div className="bg-white border  p-2 rounded-top mb-2">
            <div className="row align-items-center m-0 p-0 ">
              <div className="col-md-7 col-12 p-0 mb-2 mb-md-0 ps-2">
                <div className="font-150 font-bold m-0 p-0 ">
                  {/* {getLabel("obituaryNotice",initialLanguage)} */}
                  {Data?.notice_type_name}
                </div>
                <div className="font-90 system-icon-secondary d-flex row m-0 p-0 ">
                  <p className="m-0 p-0 col-12 col-md-auto">
                    {NoticeData?.uuid}
                  </p>
                  <p className={`m-0 p-0 font-normal font-100 col-12 col-md-6`}>
                    <span className="px-1 d-none d-sm-none d-lg-none d-md-none d-xl-inline">
                      {" "}
                      .{" "}
                    </span>
                    {Data ? timeagoformatter(Data?.created_at) : ""}
                  </p>
                </div>
              </div>
              <div className="col-md-5 col-12 p-0 d-flex justify-content-md-end justify-content-start ps-2 ps-md-0">
                {activeTab &&
                  (activeTab === "Notice Settings" ||
                    activeTab === "Notice Details") && (
                    <div className=" d-flex gap-1 gap-sm-3">
                      {/* preview button hided with d-none, remove when use */}
                      <div className="  d-none">
                        <button
                          type="button"
                          className="btn px-1 px-sm-4 btn-md border shadow-sm font-110 text-dark system-secondary-light"
                        >
                          {getLabel("previewNotice", initialLanguage)}
                        </button>
                      </div>
                      <div className="  ">
                        <button
                          type="button"
                          className="btn px-1 px-sm-4 btn-md border shadow-sm font-110 text-dark system-secondary-light "
                          onClick={() => {
                            SetShowPublish(true), setIsPublisheLater(true);
                          }}
                        >
                          {getLabel("completeOrder", initialLanguage)}
                        </button>
                      </div>
                      <div className=" ">
                        <button
                          type="button"
                          className="btn px-2 px-sm-4 btn-md border shadow-sm font-110 text-dark system-secondary-light"
                        >
                          <Image src={ellipsissvg} alt="" />
                        </button>
                      </div>
                    </div>
                  )}
                {activeTab &&
                  (activeTab === "Order Details" ||
                    activeTab === "Customer & Manager") && (
                    <div className=" d-flex gap-2 gap-sm-3 ">
                      <div className="  ">
                        <button
                          type="button"
                          className="btn px-3 px-sm-4 btn-md border shadow-sm font-110 text-dark system-secondary-light"
                        >
                          {getLabel("goToLink", initialLanguage)}
                        </button>
                      </div>
                      <div className="  ">
                        <button
                          type="button"
                          className="btn btn-md px-3 px-sm-4 border shadow-sm font-110 system-text-success system-secondary-light "
                        >
                          {getLabel("live", initialLanguage)}
                        </button>
                      </div>
                      <div className=" ">
                        <button
                          type="button"
                          className="btn btn-md px-3 px-sm-4 border shadow-sm font-110 text-dark system-secondary-light"
                        >
                          <Image src={ellipsissvg} alt="" />
                        </button>
                      </div>
                    </div>
                  )}
              </div>
            </div>
          </div>
        )}
        {/* -----------------------notice sidebar--------------------- */}
        {/* <div className="row rounded mx-md-1 my-3 " id="filter-panel"> */}
        <div className="row rounded ms-md-1 my-3 m-0 p-0 " id="filter-panel">
          <div className="side-width1 col-xl-2  col-md-3 col-12 m-0 p-0">
            {/* <div className="side-width1 col-md-3 col-12 m-0 p-0"> */}
            <div className=" p-3  inpage-sidebar">
              <div className="scrollmenu d-flex align-items-center d-md-block">
                <div className="mb-2">
                  <div onClick={() => changeTab("Notice Details")} className="">
                    <div
                      className={
                        activeTab === "Notice Details"
                          ? `system-text-primary shadow-md sidelink font-bold active pointer`
                          : `rounded shadow-md sidelink font-bold pointer`
                      }
                    >
                      {getLabel("noticeDetails", initialLanguage)}
                    </div>
                  </div>
                </div>
                <div className="mb-2">
                  <div
                    onClick={() => changeTab("Notice Settings")}
                    className=""
                  >
                    <div
                      className={
                        activeTab === "Notice Settings"
                          ? `system-text-primary shadow-md sidelink font-bold active pointer`
                          : `rounded shadow-md sidelink font-bold pointer`
                      }
                    >
                      {getLabel("noticeSettings", initialLanguage)}
                    </div>
                  </div>
                </div>

                <div className="mb-2">
                  <div onClick={() => changeTab("Order Details")} className="">
                    <div
                      className={
                        activeTab === "Order Details"
                          ? `system-text-primary shadow-md sidelink  font-bold active pointer`
                          : `rounded shadow-md sidelink font-bold pointer`
                      }
                    >
                      {getLabel("orderDetails", initialLanguage)}
                    </div>
                  </div>
                </div>
                <div className="mb-2">
                  <div onClick={() => changeTab("Customer & Manager")}>
                    <div
                      className={
                        activeTab === "Customer & Manager"
                          ? `system-text-primary shadow-md sidelink  font-bold active pointer`
                          : `rounded shadow-md sidelink font-bold pointer `
                      }
                    >
                      {getLabel("customerManager", initialLanguage)}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-10  col-md-9 col-12 data-body">
            {/* <div className="col-md-9 col-12 data-body  "> */}
            <div className="row ">
              <div className="col-12 m-0 p-0">
                {activeTab === "Notice Details" && (
                  <>
                    <NoticeDeatils />
                  </>
                )}
                {activeTab === "Notice Settings" && (
                  <>
                    <NoticeSettings />
                  </>
                )}
                {activeTab === "Order Details" && (
                  <>
                    <OrderDetails />
                  </>
                )}
                {activeTab === "Customer & Manager" && (
                  <>
                    <CustomerAndManager />
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
        {/* -------------------------------------------- */}
      </div>
    </>
  );
};
export async function getServerSideProps(query: any) {
  const isSingle = query.g || false;
  console.log(isSingle);

  return {
    props: {
      isSingle,
    },
  };
}
export default NoticeHome;
