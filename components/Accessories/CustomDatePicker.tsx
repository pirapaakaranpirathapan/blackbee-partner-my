import React, { useState, useEffect } from 'react';

interface CustomDatePickerProps {
  defaultDate?: string;
  onDateChange: (selectedDate: string) => void;
}

const CustomDatePicker: React.FC<CustomDatePickerProps> = ({ defaultDate, onDateChange }) => {
  const [selectedDay, setSelectedDay] = useState<string>('');
  const [selectedMonth, setSelectedMonth] = useState<string>('');
  const [selectedYear, setSelectedYear] = useState<string>('');
  const [isFocused, setIsFocused] = useState<boolean>(false);
  const [openedDropdown, setOpenedDropdown] = useState<string>(''); // To track the opened dropdown.

  useEffect(() => {
    if (defaultDate) {
      const [year, month, day] = defaultDate.split('-');
      setSelectedMonth(month);
      setSelectedDay(day);
      setSelectedYear(year);
    }
  }, [defaultDate]);

  const handleDateChange = () => {
    if (selectedDay === '' || selectedMonth === '' || selectedYear === '') {
      console.error('Invalid date!');
      return;
    }

    const formattedDate = `${selectedYear}-${selectedMonth}-${selectedDay}`;
    console.log("formattedDate", formattedDate);

    onDateChange(formattedDate);
  };

  const toggleDropdown = (dropdown: string) => {
    setOpenedDropdown(openedDropdown === dropdown ? '' : dropdown);
  };

  const days = Array.from({ length: 31 }, (_, i) => (i + 1).toString());
  const months = Array.from({ length: 12 }, (_, i) => (i + 1).toString());
  const years = Array.from({ length: 10 }, (_, i) => (2023 + i).toString());

  return (
    <div className="row m-0">
      <div className="col-4 px-1 p-0">
        <div className={`arco-picker arco-picker-size-default form-control border border-2 system-control p-2 ${openedDropdown === 'days' ? 'arco-picker-focused' : ''}`}>
          <div className="arco-picker-input position-relative">
            <input
              type="text"
              placeholder="Please select day"
              value={selectedDay}
              onChange={(e) => setSelectedDay(e.target.value)}
              onBlur={handleDateChange}
              onFocus={() => setIsFocused(true)}
              onClick={() => toggleDropdown('days')}
              className='position-relative'
            />
          </div>
          {openedDropdown === 'days' && (
            <div className="days-arco-picker-dropdown">
              <div className="arco-panel-day overflow-xx">
                {days.map((day) => (
                  <div
                    key={day}
                    className={`arco-panel-day-item ${selectedDay === day ? 'arco-panel-day-item-selected' : ''}`}
                    onClick={() => {
                      setSelectedDay(day);
                      toggleDropdown('days');
                    }}
                  >
                    {day}
                  </div>
                ))}
              </div>
            </div>
          )}

          <div className="arco-picker-suffix">
            <span className="arco-picker-suffix-icon">
              <svg
                fill="none"
                stroke="currentColor"
                stroke-width="4"
                viewBox="0 0 48 48"
                aria-hidden="true"
                focusable="false"
                className="arco-icon arco-icon-calendar"
              >
                <path d="M7 22h34M14 5v8m20-8v8M8 41h32a1 1 0 0 0 1-1V10a1 1 0 0 0-1-1H8a1 1 0 0 0-1 1v30a1 1 0 0 0 1 1Z"></path>
              </svg>
            </span>
          </div>
        </div>
      </div>

      <div className="col-4 px-1 p-0">
        <div className={`arco-picker arco-picker-size-default form-control border border-2 system-control p-2 ${openedDropdown === 'months' ? 'arco-picker-focused' : ''}`}>
          <div className="arco-picker-input">
            <input
              type="text"
              placeholder="Please select month"
              value={selectedMonth}
              onChange={(e) => setSelectedMonth(e.target.value)}
              onBlur={handleDateChange}
              onFocus={() => setIsFocused(true)}
              onClick={() => toggleDropdown('months')}
            />
          </div>
          {openedDropdown === 'months' && (
            <div className=" days-arco-picker-dropdown">
              <div className="arco-panel-day overflow-xx">
                {months.map((month) => (
                  <div
                    key={month}
                    className={`arco-panel-month-item ${selectedMonth === month ? 'arco-panel-month-item-selected' : ''}`}
                    onClick={() => {
                      setSelectedMonth(month);
                      toggleDropdown('months');
                    }}
                  >
                    {month}
                  </div>
                ))}
              </div>
            </div>
          )}

          <div className="arco-picker-suffix">
            <span className="arco-picker-suffix-icon">
              <svg
                fill="none"
                stroke="currentColor"
                stroke-width="4"
                viewBox="0 0 48 48"
                aria-hidden="true"
                focusable="false"
                className="arco-icon arco-icon-calendar"
              >
                <path d="M7 22h34M14 5v8m20-8v8M8 41h32a1 1 0 0 0 1-1V10a1 1 0 0 0-1-1H8a1 1 0 0 0-1 1v30a1 1 0 0 0 1 1Z"></path>
              </svg>
            </span>
          </div>
        </div>
      </div>

      <div className="col-4 px-1 p-0">
        <div className={`arco-picker arco-picker-size-default form-control border border-2 system-control p-2 ${openedDropdown === 'years' ? 'arco-picker-focused' : ''}`}>
          <div className="arco-picker-input">
            <input
              type="text"
              placeholder="Please select year"
              value={selectedYear}
              onChange={(e) => setSelectedYear(e.target.value)}
              onBlur={handleDateChange}
              onFocus={() => setIsFocused(true)}
              onClick={() => toggleDropdown('years')}
            />
          </div>
          {openedDropdown === 'years' && (
            <div className=" days-arco-picker-dropdown">
              <div className="arco-panel-day overflow-xx">
                {years.map((year) => (
                  <div
                    key={year}
                    className={`arco-panel-year-item ${selectedYear === year ? 'arco-panel-year-item-selected' : ''}`}
                    onClick={() => {
                      setSelectedYear(year);
                      toggleDropdown('years');
                    }}
                  >
                    {year}
                  </div>
                ))}
              </div>
            </div>
          )}

          <div className="arco-picker-suffix">
            <span className="arco-picker-suffix-icon">
              <svg
                fill="none"
                stroke="currentColor"
                stroke-width="4"
                viewBox="0 0 48 48"
                aria-hidden="true"
                focusable="false"
                className="arco-icon arco-icon-calendar"
              >
                <path d="M7 22h34M14 5v8m20-8v8M8 41h32a1 1 0 0 0 1-1V10a1 1 0 0 0-1-1H8a1 1 0 0 0-1 1v30a1 1 0 0 0 1 1Z"></path>
              </svg>
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CustomDatePicker;















// ==================================================================================================================




// import React, { useState, useEffect } from 'react';

// interface CustomDatePickerProps {
//   defaultDate?: string;
//   onDateChange: (selectedDate: string) => void;
// }

// const CustomDatePicker: React.FC<CustomDatePickerProps> = ({ defaultDate, onDateChange }) => {
//   const [selectedDay, setSelectedDay] = useState<string>('');
//   const [selectedMonth, setSelectedMonth] = useState<string>('');
//   const [selectedYear, setSelectedYear] = useState<string>('');
//   const [isFocused, setIsFocused] = useState<boolean>(false);
//   const [isPickerOpen, setIsPickerOpen] = useState<boolean>(false);

//   useEffect(() => {
//     if (defaultDate) {
//       const [year, month, day] = defaultDate.split('-');
//       setSelectedMonth(month);
//       setSelectedDay(day);
//       setSelectedYear(year);
//     }
//   }, [defaultDate]);

//   const handleDateChange = () => {
//     if (selectedDay === '' || selectedMonth === '' || selectedYear === '') {
//       console.error('Invalid date!');
//       return;
//     }

//     const formattedDate = `${selectedYear}-${selectedMonth}-${selectedDay}`;
//     onDateChange(formattedDate);
//   };

//   // const togglePicker = () => {
//   //   setIsPickerOpen(!isPickerOpen);
//   // };
//   const togglePicker = () => {
//     setIsFocused(!isFocused);
//   };

//   const days = Array.from({ length: 31 }, (_, i) => (i + 1).toString());
//   const months = Array.from({ length: 12 }, (_, i) => (i + 1).toString());
//   const years = Array.from({ length: 10 }, (_, i) => (2023 + i).toString());
//   return (
//     <>
//       <div className="row m-0">
//         <div className="col-4 px-1 p-0">
//           <div className={`arco-picker arco-picker-size-default form-control border border-2 system-control p-2 ${isFocused ? 'arco-picker-focused1' : ''}`}>
//             <div className="arco-picker-input">
//               <input
//                 type="text"
//                 placeholder="Please select day"
//                 value={selectedDay}
//                 onChange={(e) => setSelectedDay(e.target.value)}
//                 onBlur={handleDateChange}
//                 onFocus={() => setIsFocused(true)}
//                 className="arco-picker-start-time"
//                 onClick={togglePicker}
//               />

//             </div>
//             {isFocused && (
//               <div className="arco-picker-dropdown">
//                 <div className="arco-picker-container">
//                   <div className="arco-panel-day">
//                     {days.map((day) => (
//                       <div
//                         key={day}
//                         className={`arco-panel-day-item ${selectedDay === day ? 'arco-panel-day-item-selected' : ''}`}
//                         onClick={() => {
//                           setSelectedDay(day);
//                           togglePicker();
//                         }}
//                       >
//                         {day}
//                       </div>
//                     ))}
//                   </div>
//                 </div>
//               </div>
//             )}

//             <div className="arco-picker-suffix">
//               <span className="arco-picker-suffix-icon">
//                 <svg
//                   fill="none"
//                   stroke="currentColor"
//                   stroke-width="4"
//                   viewBox="0 0 48 48"
//                   aria-hidden="true"
//                   focusable="false"
//                   className="arco-icon arco-icon-calendar"
//                 >
//                   <path d="M7 22h34M14 5v8m20-8v8M8 41h32a1 1 0 0 0 1-1V10a1 1 0 0 0-1-1H8a1 1 0 0 0-1 1v30a1 1 0 0 0 1 1Z"></path>
//                 </svg>
//               </span>
//             </div>
//           </div>


//         </div>

//         <div className="col-4 px-1 p-0">
//           <div className={`arco-picker arco-picker-size-default form-control border border-2 system-control p-2 ${isFocused ? 'arco-picker-focused' : ''}`}>
//             <div className="arco-picker-input">
//               <input
//                 type="text"
//                 placeholder="Please select month"
//                 value={selectedMonth}
//                 onChange={(e) => setSelectedMonth(e.target.value)}
//                 onBlur={handleDateChange}
//                 onFocus={() => setIsFocused(true)}
//                 className="arco-picker-start-time" />
//             </div>
//             {isFocused && (
//               <div className="arco-picker-dropdown">
//                 <div className="arco-picker-container">
//                   <div className="arco-panel-month">
//                     {months.map((month) => (
//                       <div
//                         key={month}
//                         className={`arco-panel-month-item ${selectedMonth === month ? 'arco-panel-month-item-selected' : ''}`}
//                         onClick={() => {
//                           setSelectedMonth(month);
//                           togglePicker();
//                         }}
//                       >
//                         {month}
//                       </div>
//                     ))}
//                   </div>
//                 </div>
//               </div>
//             )}

//             <div className="arco-picker-suffix">
//               <span className="arco-picker-suffix-icon">
//                 <svg
//                   fill="none"
//                   stroke="currentColor"
//                   stroke-width="4"
//                   viewBox="0 0 48 48"
//                   aria-hidden="true"
//                   focusable="false"
//                   className="arco-icon arco-icon-calendar"
//                 >
//                   <path d="M7 22h34M14 5v8m20-8v8M8 41h32a1 1 0 0 0 1-1V10a1 1 0 0 0-1-1H8a1 1 0 0 0-1 1v30a1 1 0 0 0 1 1Z"></path>
//                 </svg>
//               </span>
//             </div>
//           </div>
//         </div>

//         <div className="col-4 px-1 p-0">
//           <div className={`arco-picker arco-picker-size-default form-control border border-2 system-control p-2 ${isFocused ? 'arco-picker-focused' : ''}`}>
//             <div className="arco-picker-input">
//               <input
//                 type="text"
//                 placeholder="Please select year"
//                 value={selectedYear}
//                 onChange={(e) => setSelectedYear(e.target.value)}
//                 onBlur={handleDateChange}
//                 onFocus={() => setIsFocused(true)}
//                 className="arco-picker-start-time" />
//             </div>

//             {isFocused && (
//               <div className="arco-picker-dropdown">
//                 <div className="arco-picker-container">
//                   <div className="arco-panel-year">
//                     {years.map((year) => (
//                       <div
//                         key={year}
//                         className={`arco-panel-year-item ${selectedYear === year ? 'arco-panel-year-item-selected' : ''}`}
//                         onClick={() => {
//                           setSelectedYear(year);
//                           togglePicker();
//                         }}
//                       >
//                         {year}
//                       </div>
//                     ))}
//                   </div>
//                 </div>
//               </div>
//             )}
//             <div className="arco-picker-suffix">
//               <span className="arco-picker-suffix-icon">
//                 <svg
//                   fill="none"
//                   stroke="currentColor"
//                   stroke-width="4"
//                   viewBox="0 0 48 48"
//                   aria-hidden="true"
//                   focusable="false"
//                   className="arco-icon arco-icon-calendar"
//                 >
//                   <path d="M7 22h34M14 5v8m20-8v8M8 41h32a1 1 0 0 0 1-1V10a1 1 0 0 0-1-1H8a1 1 0 0 0-1 1v30a1 1 0 0 0 1 1Z"></path>
//                 </svg>
//               </span>
//             </div>
//           </div>
//         </div>

//       </div>
//     </>
//   );
// };

// export default CustomDatePicker;



// import React, { useState, useEffect } from 'react';

// interface CustomDatePickerProps {
//   defaultDate?: string;
//   onDateChange: (selectedDate: string) => void;
// }

// const generateSelectWithSuffix = (
//   value: string,
//   onChange: (e: React.ChangeEvent<HTMLSelectElement>) => void,
//   onBlur: () => void,
//   options: JSX.Element[]
// ) => (
//   <div className="custom-single-date-arco arco-picker arco-picker-size-default form-control border border-2 system-control py-3">
//     <div className="arco-picker-input">
//       <select className="arco-picker-start-time arco-select" value={value} onChange={onChange} onBlur={onBlur}>
//         {options}
//       </select>
//     </div>
// <div className="arco-picker-suffix">
//   <span className="arco-picker-suffix-icon">
//     <svg
//       fill="none"
//       stroke="currentColor"
//       stroke-width="4"
//       viewBox="0 0 48 48"
//       aria-hidden="true"
//       focusable="false"
//       className="arco-icon arco-icon-calendar"
//     >
//       <path d="M7 22h34M14 5v8m20-8v8M8 41h32a1 1 0 0 0 1-1V10a1 1 0 0 0-1-1H8a1 1 0 0 0-1 1v30a1 1 0 0 0 1 1Z"></path>
//     </svg>
//   </span>
// </div>
//   </div>
// );

// const CustomDatePicker: React.FC<CustomDatePickerProps> = ({ defaultDate, onDateChange }) => {
//   const monthNames = [
//     'January',
//     'February',
//     'March',
//     'April',
//     'May',
//     'June',
//     'July',
//     'August',
//     'September',
//     'October',
//     'November',
//     'December',
//   ];

//   const [selectedDay, setSelectedDay] = useState('');
//   const [selectedMonth, setSelectedMonth] = useState('');
//   const [selectedYear, setSelectedYear] = useState('');

//   useEffect(() => {
//     if (defaultDate) {
//       const [year, month, day] = defaultDate.split('-');
//       setSelectedMonth(month);
//       setSelectedDay(day);
//       setSelectedYear(year);
//     }
//   }, [defaultDate]);

//   const handleDateChange = () => {
//     if (selectedDay === '' || selectedMonth === '' || selectedYear === '') {
//       console.error('Invalid date!');
//       return;
//     }

//     const formattedDate = `${selectedYear}-${selectedMonth}-${selectedDay}`;
//     onDateChange(formattedDate);
//   };

//   const generateDayOptions = (numDays: number) => {
//     return Array.from({ length: numDays }, (_, i) => i + 1).map((num) => (
//       <option key={num} value={num}>
//         {num}
//       </option>
//     ));
//   };

//   const handleMonthChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
//     const selectedMonth = e.target.value;
//     setSelectedMonth(selectedMonth);
//     setSelectedDay('');
//     setSelectedYear('');
//     handleDateChange();
//   };

//   return (
//     <div className="arco-form-control">
//       <div className="row m-0 align-items-center">
//         <div className="col-4 p-0">
//           {generateSelectWithSuffix(
//             selectedDay,
//             (e) => setSelectedDay(e.target.value),
//             handleDateChange,
//             generateDayOptions(new Date(Number(selectedYear), Number(selectedMonth), 0).getDate())
//           )}
//         </div>
//         <div className="col-4 p-0">
//           {generateSelectWithSuffix(
//             selectedMonth,
//             handleMonthChange,
//             handleDateChange,
//             monthNames.map((name, index) => (
//               <option key={name} value={String(index + 1)}>
//                 {selectedMonth === String(index + 1) ? name.slice(0, 3) : name}
//               </option>
//             ))
//           )}
//         </div>
//         <div className="col-4 p-0">
//           {generateSelectWithSuffix(
//             selectedYear,
//             (e) => setSelectedYear(e.target.value),
//             handleDateChange,
//             Array.from({ length: 120 }, (_, i) => new Date().getFullYear() - i).map((num) => (
//               <option key={num} value={num}>
//                 {num}
//               </option>
//             ))
//           )}
//         </div>
//       </div>
//     </div>
//   );
// };

// export default CustomDatePicker;




// import React, { useState, useEffect } from 'react';

// interface CustomDatePickerProps {
//   defaultDate?: string;
//   onDateChange: (selectedDate: string) => void;
// }

// const CustomDatePicker: React.FC<CustomDatePickerProps> = ({ defaultDate, onDateChange }) => {
//   const monthNames = [
//     'January',
//     'February',
//     'March',
//     'April',
//     'May',
//     'June',
//     'July',
//     'August',
//     'September',
//     'October',
//     'November',
//     'December',
//   ];

//   const [selectedDay, setSelectedDay] = useState('');
//   const [selectedMonth, setSelectedMonth] = useState('');
//   const [selectedYear, setSelectedYear] = useState('');

//   useEffect(() => {
//     if (defaultDate) {
//       const [year, month, day] = defaultDate.split('-');
//       setSelectedMonth(parseInt(month, 10).toString()); // Parse as number and convert back to string
//       setSelectedDay(parseInt(month, 10).toString());
//       setSelectedYear(year);
//     }
//   }, [defaultDate]);

//   const handleDateChange = () => {
//     if (selectedDay === '' || selectedMonth === '') {
//       // Invalid date, handle the error or show an error message
//       console.error('Invalid date!');
//       return;
//     }

//     const selectedDate = new Date(`${selectedMonth}/${selectedDay}/${selectedYear}`);

//     if (isNaN(selectedDate.getTime())) {
//       // Invalid date, handle the error or show an error message
//       console.error('Invalid date!');
//       return;
//     }

//     const formattedDate = `${selectedYear}-${selectedMonth.padStart(2, '0')}-${selectedDay.padStart(2, '0')}`;
//     onDateChange(formattedDate);
//   };

//   const getDaysInMonth = (month: number, year: number) => {
//     return new Date(year, month, 0).getDate();
//   };

//   const generateDayOptions = (numDays: number) => {
//     return Array.from({ length: numDays }, (_, i) => i + 1).map((num) => (
//       <option key={num} value={num}>
//         {num}
//       </option>
//     ));
//   };

//   const handleMonthChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
//     const selectedMonth = parseInt(e.target.value, 10).toString(); // Parse as number and convert back to string
//     setSelectedMonth(selectedMonth);
//     setSelectedDay('');
//     setSelectedYear('');
//     handleDateChange(); // Check if the selected date is valid
//   };

//   return (
//     <div className="row m-0 align-items-center">
//       <div className="col-4 p-0">
//         <select
//           className="form-select border border-2 bg-white p-2 system-control"
//           value={selectedDay}
//           onChange={(e) => setSelectedDay(e.target.value)}
//           onBlur={handleDateChange}
//         >
//           <option value="">Date</option>
//           {selectedMonth &&
//             selectedYear &&
//             generateDayOptions(getDaysInMonth(Number(selectedMonth), Number(selectedYear)))}
//         </select>
//       </div>
//       <div className="col-4 px-1 p-0">
//         <select
//           className="form-select border border-2 bg-white p-2 system-control"
//           value={selectedMonth}
//           onChange={handleMonthChange}
//           onBlur={handleDateChange}
//         >
//           <option value="">Month</option>
//           {monthNames.map((name, index) => (
//             <option key={name} value={index + 1}>
//               {selectedMonth === String(index + 1) ? name.slice(0, 3) : name}
//             </option>
//           ))}
//         </select>
//       </div>
//       <div className="col-4 p-0">
//         <select
//           className="form-select border border-2 bg-white p-2 system-control"
//           value={selectedYear}
//           onChange={(e) => setSelectedYear(e.target.value)}
//           onBlur={handleDateChange}
//         >
//           <option value="">Year</option>
//           {Array.from({ length: 120 }, (_, i) => new Date().getFullYear() - i).map((num) => (
//             <option key={num} value={num}>
//               {num}
//             </option>
//           ))}
//         </select>
//       </div>
//     </div>
//   );
// };

// export default CustomDatePicker;
