import { Select } from "@arco-design/web-react";
import React, { useState, useEffect } from "react";
interface PickerDateSingleProps {
  defaultDate?: string;
  onDateChange: (selectedDate: string) => void;
  size?: string;
  shortMonth?:boolean;
}
const PickerDateSingle: React.FC<PickerDateSingleProps> = ({
  defaultDate,
  onDateChange,
  shortMonth,
  size,
}) => {
  const monthNamesFull = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  const monthNamesShort = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];  const monthNames=shortMonth?monthNamesShort:monthNamesFull;
  // const [year, month, day] = defaultDate.split("-");
  const [selectedDay, setSelectedDay] = useState<string>("");
  const [selectedMonth, setSelectedMonth] = useState<string>("");
  const [selectedYear, setSelectedYear] = useState<string>("");
  const [startDay, setStartDay] = useState<string>("");
  const [Month, setMonth] = useState<string>("");

  console.log("selected values", defaultDate);

  useEffect(() => {
    // console.log("default Date",defaultDate);
    
    if (defaultDate) {
      const [year, month, day] = defaultDate.split("-");
      setSelectedMonth(monthNames[Number(month) - 1]);
      setSelectedDay(day);
      setSelectedYear(year);
      setMonth(month);
    } else {
      setSelectedMonth("");
      setSelectedDay("");
      setSelectedYear("");
      setMonth("");
    }
  }, [defaultDate]);

  useEffect(() => {
    if (startDay !== "") {
      setSelectedDay(startDay);
    }
  }, [startDay]);
  // const handleDateChange = () => {
  //   if (selectedDay && selectedMonth && selectedYear) {
  //     const formattedDate = `${selectedYear}-${selectedMonth.padStart(
  //       2,
  //       "0"
  //     )}-${selectedDay.padStart(2, "0")}`;
  //     onDateChange(formattedDate);
  //   } else {
  //     console.error("Invalid date!");
  //   }
  // };
  const handleDateChange = () => {
    const formattedYear = selectedYear ? selectedYear : "";
    const formattedMonth = Month ? Month.padStart(2, "0") : "";
    const formattedDay = selectedDay ? selectedDay : "";
    const formattedDate = `${formattedYear}-${formattedMonth}-${formattedDay}`;
    onDateChange(formattedDate);
    console.log(formattedDate);

    // const selectedDate = new Date(
    //   `${selectedMonth}/${selectedDay}/${selectedYear}`
    // );
    // onDateChange(`${selectedMonth}/${selectedDay}/${selectedYear}`);
    // if (isNaN(selectedDate.getTime())) {
    //   // Invalid date, handle the error or show an error message
    //   console.error("Invalid date!");
    //   setSelectedDay("");
    //   const formattedYear = selectedYear ? selectedYear : "";
    //   const formattedMonth = Month
    //     ? Month.padStart(2, "0")
    //     : "";
    //   const formattedDay = selectedDay ? selectedDay.padStart(2, "0") : "";
    //   const formattedDate = `${formattedYear}-${formattedMonth}-${formattedDay}`;
    //   onDateChange(formattedDate);
    // } else {
    //   const formattedYear = selectedYear ? selectedYear : "";
    //   const formattedMonth = Month
    //     ? Month.padStart(2, "0")
    //     : "";
    //   const formattedDay = selectedDay ? selectedDay.padStart(2, "0") : "";
    //   const formattedDate = `${formattedYear}-${formattedMonth}-${formattedDay}`;
    //   onDateChange(formattedDate);
    // }
  };
  const handleMonthChange = (value: string) => {
    const selectedMonthIndex = monthNames.indexOf(value);
    if (selectedMonthIndex >= 0) {
      const formattedMonth = (selectedMonthIndex + 1)
        .toString()
        .padStart(2, "0");
      setMonth(formattedMonth);
      setSelectedMonth(value);

      // Check if the selected day is valid for the new month and year
      const daysInMonth = getDaysInMonth(
        selectedMonthIndex + 1,
        parseInt(selectedYear, 10)
      );

      if (parseInt(selectedDay, 10) > daysInMonth) {
        // If the selected day is not valid for the new month, set it to ""
        setSelectedDay("");
        // setSelectedDay("");
      }
    } else {
      console.error("Invalid month value");
      setSelectedMonth(""); // Reset the selected month
    }
  };

  const handleYearChange = (value: string) => {
    setSelectedYear(value);
    // setSelectedMonth(Month);

    // Check if the selected day is valid for the new year and month
    const daysInMonth = getDaysInMonth(
      monthNames.indexOf(selectedMonth) + 1,
      parseInt(value, 10)
    );

    if (parseInt(selectedDay, 10) > daysInMonth) {
      // If the selected day is not valid for the new year, set it to ""
      setSelectedDay("");
    }
  };
  const handleSearch = (value: string) => {
    setStartDay(value);
    setSelectedDay(value);
    if (value === "") {
      setSelectedDay("");
    } else {
      const numericValue = parseInt(value, 10);
      const daysInMonth = getDaysInMonth(
        monthNames.indexOf(selectedMonth) + 1,
        parseInt(selectedYear, 10)
      );
      if (numericValue >= 1 && numericValue <= daysInMonth) {
        setSelectedDay(numericValue.toString());
      } else {
        setSelectedDay("");
      }
    }
  };
  const getDaysInMonth = (month: number, year: number) => {
    return new Date(year, month, 0).getDate();
  };
  const generateDayOptions = () => {
    const daysInMonth =
      selectedMonth && selectedYear
        ? getDaysInMonth(
            monthNames.indexOf(selectedMonth) + 1,
            parseInt(selectedYear, 10)
          )
        : 31;
    return Array.from({ length: daysInMonth }, (_, i) => i + 1).map((num) => (
      <Select.Option
        key={num}
        value={num.toString().padStart(2, "0")}
        style={{ paddingLeft: "5px", paddingRight: "5px", fontSize: "13px" }}
      >
        {num.toString().padStart(2, "0")}
      </Select.Option>
    ));
  };
  const generateYearOptions = () => {
    const currentYear = new Date().getFullYear();
    return Array.from({ length: 120 }, (_, i) => currentYear - i).map((num) => (
      <Select.Option
        key={num}
        value={num.toString()}
        style={{ paddingLeft: "5px", paddingRight: "5px", fontSize: "13px" }}
      >
        {num}
      </Select.Option>
    ));
  };

  return (
    <div
      className={`d-flex justify-content-between m-0 align-items-center date-picker row ${size}`}
    >
      <div className="col-D p-0">
        <Select
          placeholder="Date"
          style={{ width: "100%" }}
          showSearch={true}
          value={selectedDay !== "" ? selectedDay : undefined}
          onChange={handleSearch}
          notFoundContent={null}
          onBlur={handleDateChange}
          getPopupContainer={(node) => {
            const container = node.parentElement;
            if (container) {
              return container;
            }
            return document.body;
          }}
        >
          {generateDayOptions()}
        </Select>
      </div>
      <div className="col-M  p-0">
        <Select
          placeholder="Month"
          style={{ width: "100%" }}
          value={selectedMonth !== "" ? selectedMonth : undefined}
          showSearch={true}
          onChange={handleMonthChange}
          notFoundContent={null}
          onBlur={handleDateChange}
          getPopupContainer={(node) => {
            const container = node.parentElement;
            if (container) {
              return container;
            }
            return document.body;
          }}
        >
          {monthNames.map((name, index) => (
            <Select.Option
              key={index}
              value={name}
              style={{
                paddingLeft: "5px",
                paddingRight: "5px",
                fontSize: "13px",
              }}
            >
              {/* {selectedMonth === name ? name.slice(0, 3) : name} */}
              {name}
            </Select.Option>
          ))}
        </Select>
      </div>
      <div className="col-Y p-0">
        <Select
          placeholder="Year"
          style={{ width: "100%" }}
          showSearch={true}
          value={selectedYear !== "" ? selectedYear : undefined}
          onChange={handleYearChange}
          notFoundContent={null}
          getPopupContainer={(node) => {
            const container = node.parentElement;
            if (container) {
              return container;
            }
            return document.body;
          }}
          onBlur={handleDateChange}
        >
          {generateYearOptions()}
        </Select>
      </div>
    </div>
  );
};

export default PickerDateSingle;
