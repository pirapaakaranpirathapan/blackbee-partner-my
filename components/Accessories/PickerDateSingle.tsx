

import React, { useState, useEffect } from 'react';

interface PickerDateSingleProps {
  defaultDate?: string;
  onDateChange: (selectedDate: string) => void;
}

const PickerDateSingle: React.FC<PickerDateSingleProps> = ({ defaultDate, onDateChange }) => {
  const monthNames = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];

  const [selectedDay, setSelectedDay] = useState('');
  const [selectedMonth, setSelectedMonth] = useState('');
  const [selectedYear, setSelectedYear] = useState('');

  useEffect(() => {
    if (defaultDate) {
      const [year, month, day] = defaultDate.split('-');
      setSelectedMonth(parseInt(month, 10).toString()); // Parse as number and convert back to string
      setSelectedDay(parseInt(month, 10).toString());
      setSelectedYear(year);
    }
  }, [defaultDate]);

  const handleDateChange = () => {
    if (selectedDay === '' || selectedMonth === '') {
      // Invalid date, handle the error or show an error message
      console.error('Invalid date!');
      return;
    }

    const selectedDate = new Date(`${selectedMonth}/${selectedDay}/${selectedYear}`);

    if (isNaN(selectedDate.getTime())) {
      // Invalid date, handle the error or show an error message
      console.error('Invalid date!');
      return;
    }

    const formattedDate = `${selectedYear}-${selectedMonth.padStart(2, '0')}-${selectedDay.padStart(2, '0')}`;
    onDateChange(formattedDate);
  };

  const getDaysInMonth = (month: number, year: number) => {
    return new Date(year, month, 0).getDate();
  };

  const generateDayOptions = (numDays: number) => {
    return Array.from({ length: numDays }, (_, i) => i + 1).map((num) => (
      <option key={num} value={num}>
        {num}
      </option>
    ));
  };

  const handleMonthChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedMonth = parseInt(e.target.value, 10).toString(); // Parse as number and convert back to string
    setSelectedMonth(selectedMonth);
    setSelectedDay('');
    setSelectedYear('');
    handleDateChange(); // Check if the selected date is valid
  };

  return (
    <div className="row m-0 align-items-center">
      <div className="col-4 p-0">
        <select
          className="form-select border border-2 bg-white p-2 system-control"
          value={selectedDay}
          onChange={(e) => setSelectedDay(e.target.value)}
          onBlur={handleDateChange}
        >
          <option value="">Date</option>
          {selectedMonth &&
            selectedYear &&
            generateDayOptions(getDaysInMonth(Number(selectedMonth), Number(selectedYear)))}
        </select>
      </div>
      <div className="col-4 px-1 p-0">
        <select
          className="form-select border border-2 bg-white p-2 system-control"
          value={selectedMonth}
          onChange={handleMonthChange}
          onBlur={handleDateChange}
        >
          <option value="">Month</option>
          {monthNames.map((name, index) => (
            <option key={name} value={index + 1}>
              {selectedMonth === String(index + 1) ? name.slice(0, 3) : name}
            </option>
          ))}
        </select>
      </div>
      <div className="col-4 p-0">
        <select
          className="form-select border border-2 bg-white p-2 system-control"
          value={selectedYear}
          onChange={(e) => setSelectedYear(e.target.value)}
          onBlur={handleDateChange}
        >
          <option value="">Year</option>
          {Array.from({ length: 120 }, (_, i) => new Date().getFullYear() - i).map((num) => (
            <option key={num} value={num}>
              {num}
            </option>
          ))}
        </select>
      </div>
    </div>
  );
};

export default PickerDateSingle;
