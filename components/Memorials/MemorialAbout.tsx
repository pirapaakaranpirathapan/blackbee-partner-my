import React, { useContext, useEffect, useState } from 'react'
import ContactDetail from './MemorialAbout/ContactDetail'
import Designs from './MemorialAbout/Designs'
import PageManager from './MemorialAbout/PageManager'
import PersonalInformation from './MemorialAbout/PersonalInformation'
import PhotosAndVideos from './MemorialAbout/PhotosAndVideos'
import Settings from './MemorialAbout/Settings'
import Image from 'next/image'
import eye from '../../public/eye.svg'
import { useRouter } from 'next/router'
import { getLabel } from '@/utils/lang-manager'
import { PageDataContext } from '@/pages/memorials/[id]/PageDataContext'

type Props = {
  t?: any
}
const MemorialAbout = () => {
  const { handleSave, PageData } = useContext(PageDataContext);
  const initialLanguage = PageData?.initialLanguage
  const subtab = 'personal-information'
  // const [IsMenuOpen, setIsMenuOpen] = useState<boolean>(true);
  const [activeTab, setActiveTab] = useState('');

  const changeTab = (val: any) => {
    setActiveTab(val)
  }
  const router = useRouter();
  const { query } = router;

  //---To set tabs----------------------------------------------------------------//
  const handleTabSelect = (key: string) => {
    setActiveTab(key);
    if (key !== null) {
      router.push(
        { pathname: router.pathname, query: { ...query, s: key } },
        // { pathname:`/memorials/${query.id}/${key}` },
        undefined,
        { shallow: true }
      );
    }
  };
  useEffect(() => {
    const handleSetActiveKey = () => {
      if (Array.isArray(query?.s)) {
        setActiveTab(query?.s[0] || subtab);
      } else {
        setActiveTab(query?.s || subtab);
      }
    };
    // Call the function when the component mounts or when the query changes.
    handleSetActiveKey();
  }, [query?.s]);
  return (
    <>
      <div
        className="row m-0 rounded bg-white mx-md-4 my-3 border  "
        id="filter-panel">
        <div className="side-width1 col-md-3 col-12 m-0 p-0 border-end ps-0 ps-md-1">
          <div className="p-3 inpage-sidebar">
            <h1 className=" m-0 fs-6 mb-3 system-text-blue-light">
              <strong>{getLabel("about", initialLanguage)}</strong>
            </h1>
            <div className=" scrollmenu d-flex align-items-center d-md-block">
              <div className="mb-2">
                <div
                  onClick={() => handleTabSelect('personal-information')}
                  className="font-semibold">
                  <div
                    className={
                      activeTab === 'personal-information'
                        ? `system-text-primary shadow-md sidelink active font-bold pointer`
                        : `rounded shadow-md sidelink pointer `
                    }>
                    {getLabel("personalInformation", initialLanguage)}
                  </div>
                </div>
              </div>
              <div className="2 mb-2 ">
                <div
                  onClick={() => handleTabSelect('contact-details')}
                  className=" font-semibold">
                  <div
                    className={
                      activeTab === 'contact-details'
                        ? `system-text-primary shadow-md sidelink active font-bold pointer`
                        : `sidelink pointer `
                    }>
                    {getLabel("contactDetails", initialLanguage)}
                  </div>
                </div>
              </div>
              <div className='mb-2'>
                <div
                  onClick={() => handleTabSelect('photos-and-videos')}
                  className=" font-semibold">
                  <div
                    className={
                      activeTab === 'photos-and-videos'
                        ? `system-text-primary shadow-md sidelink active font-bold r pointer`
                        : `sidelink pointer `
                    }>
                    {getLabel("photosVideos", initialLanguage)}
                  </div>
                </div>
              </div>
              <div className='mb-2'>
                <div onClick={() => handleTabSelect('designs')} className=' font-semibold'>
                  <div
                    className={
                      activeTab === 'designs'
                        ? `system-text-primary shadow-md sidelink active font-bold pointer`
                        : `sidelink pointer`
                    }>
                    {getLabel("designs", initialLanguage)}
                  </div>
                </div>
              </div>
              <div className='mb-2'>
                <div onClick={() => handleTabSelect('page-manager')} className=' font-semibold'>
                  <div
                    className={
                      activeTab === 'page-manager'
                        ? `system-text-primary shadow-md sidelink active font-bold pointer`
                        : `sidelink pointer`
                    }>
                    {getLabel("pageManager", initialLanguage)}
                  </div>
                </div>
              </div>
              <div className='mb-2'>
                <div onClick={() => handleTabSelect('settings')} className='font-semibold'>
                  <div
                    className={
                      activeTab === 'settings'
                        ? `system-text-primary shadow-md sidelink  active font-bold  mb-0 mb-md-4 pointer`
                        : ` sidelink  mb-0 mb-md-4 pointer`
                    }>
                    {getLabel("settings", initialLanguage)}
                  </div>
                </div>
              </div>
              {/* preview button hided with d-none, remove when use */}
              {/* <div className='mb-2 d-none'>
                <div onClick={() => changeTab('preview')}>
                  <div
                    className={
                      activeTab === 'preview'
                        ? `system-text-primary system-filter-secondary py-2 px-2 shadow-md sidelink active font-bold rounded pointer`
                        : `rounded shadow-md system-filter-secondary py-2 px-2 sidelink pointer`
                    }>

                    <Image src={eye} alt="eye icon" className={
                      activeTab === 'preview'
                        ? 'system-text-primary ms-1'
                        : 'system-text-secondary ms-1'
                    } />
                    <span className="ps-2 system-text-primary">{getLabel("preview",initialLanguage)}</span>
                  </div>
                </div>
              </div> */}
            </div>
          </div>
        </div>
        <div className="col-md-9 col-12 data-body pe-xxl-0">
          <div className="row ">
            <div className="col-12 m-0 p-3 ps-3 ps-md-4 pe-xl-0">
              {activeTab === 'personal-information' && (
                <>
                  <PersonalInformation />
                </>
              )}
              {activeTab === 'contact-details' && (
                <>
                  <ContactDetail />
                </>
              )}
              {activeTab === 'photos-and-videos' && (
                <>
                  <PhotosAndVideos />
                </>
              )}
              {activeTab === 'designs' && (
                <>
                  <Designs />
                </>
              )}
              {activeTab === 'page-manager' && (
                <>
                  <PageManager />
                </>
              )}
              {activeTab === 'settings' && (
                <>
                  <Settings />
                </>
              )}
              {activeTab === 'preview' && (
                <>
                  <Settings />
                </>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
export async function getServerSideProps({ query }: any) {
  const idQuery = query.t || "";
  return {
    props: {
      idQuery,
    },
  };
}
export default MemorialAbout
