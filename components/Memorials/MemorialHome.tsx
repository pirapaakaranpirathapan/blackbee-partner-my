import Link from "next/link";
import React, { Key, useContext, useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import MemorialHomeCard from "../Cards/MemorialHomeCard";
import FullScreenModal from "../Elements/FullScreenModal";
import MemorialEditInfo from "./Modal/MemorialEditInfo";
import MemorialHomeCreate from "../Modal/NoticeTypeCreate";
import FullScreenModalOne from "../Elements/FullScreenModalOne";
import MemorialNoticeTypeCreate from "../Modal/NoticeTypeCreate";
import NoticeTypeCreate from "../Modal/NoticeTypeCreate";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";
import {
  customerServiceSelector,
  getAllCustomerNotices,
} from "@/redux/customer-service";
import {
  ACTIVE_PARTNER,
  ACTIVE_SERVICE_PROVIDER,
  BASE_ROUTE,
} from "@/config/constants";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import extractDate from "@/components/TimeConverter/dateExtracter";
import { useRouter } from "next/router";
import PagePackageSkelton from "../Skelton/PagePackageSkelton";
import { getCookie } from "cookies-next";
import { getLabel } from "@/utils/lang-manager";
import { noticeTypeSelector } from "@/redux/notice-types";
import DashboardCard from "../Cards/DashboardCard";
import folder from "../../public/folder.png";
import folder2 from "../../public/folder2.png";
import folder3 from "../../public/folder3.png";
import folder4 from "../../public/folder4.png";

const MemorialHome = () => {
  const router = useRouter();
  const { handleSave, PageData } = useContext(PageDataContext);
  const initialLanguage = PageData?.initialLanguage
  const Data = PageData;
  const noticetypesDatas = Data?.noticetypes;
  const clientuuid = Data?.client?.uuid;
  const [editInfo, setEditInfo] = useState(false);
  const [memorialCreateHome, setMemorialCreateHome] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [count, setCount] = useState(3);
  const dispatch = useAppDispatch();
  const { data: noticeTypeData } = useAppSelector(noticeTypeSelector);
  const { noticesData: ClientNoticeServices } = useAppSelector(customerServiceSelector);

  let sampleData = [
    {
      image: folder4,
      amount: 80,
      amountsymbol: "£",
      title: `${getLabel("tributes", initialLanguage)}`,
      description: `${getLabel("dasTributesDes", initialLanguage)}`,
    },
    {
      image: folder,
      amount: 80,
      amountsymbol: "£",
      title: `${getLabel("deathNotice", initialLanguage)}`,
      description: `${getLabel("dasDeathNoticeDes", initialLanguage)}`,
    },
    {
      image: folder,
      amount: 80,
      amountsymbol: "£",
      title: `${getLabel("remembrance", initialLanguage)}`,
      description: `${getLabel("dasRemembranceDes", initialLanguage)}`,
    },

    {
      image: folder3,
      amount: 80,
      amountsymbol: "£",
      title: `${getLabel("birthMemorial", initialLanguage)}`,
      description: `${getLabel("dasbirthMemorialDes", initialLanguage)}`,
    },
    {
      amount: 80,
      amountsymbol: "£",
      title: `${getLabel("prayerNotice", initialLanguage)}`,
      description: `${getLabel("dasPrayerNoticeDes", initialLanguage)}`,
    },
    {
      image: folder,
      amount: 80,
      amountsymbol: "£",
      title: `${getLabel("otherServices", initialLanguage)}`,
      description: `${getLabel("dasOtherServicesDes", initialLanguage)}`,
    },
  ] as any;

  const [data, SetData] = useState(sampleData);

  const loadData = async () => {
    try {
      const response = await dispatch(
        getAllCustomerNotices({
          active_partner: ACTIVE_PARTNER,
          active_service_provider: ACTIVE_SERVICE_PROVIDER,
          client_id: clientuuid as string,
        })
      ).then(() => {
        setIsLoading(false);
      });
    } catch (error) {
      console.error("Error fetching data:", error);
      setIsLoading(false);
    }
  };

  useEffect(() => {
    clientuuid && loadData();
  }, [Data?.client?.uuid]);

  // useEffect(() => {
  //   if (data.length > 0 && ClientNoticeServices.length > 0) {
  //     setIsLoading(false);
  //   }
  // }, [data, ClientNoticeServices]);

  return (
    <>
      <FullScreenModalOne
        show={memorialCreateHome}
        onClose={() => {
          setMemorialCreateHome(false);
        }}
        title={`${getLabel("chooseNoticeType", initialLanguage)}`}
        description=""
      >
        <NoticeTypeCreate noticetypesDatas={noticeTypeData} />
      </FullScreenModalOne>
      <FullScreenModal
        show={editInfo}
        onClose={() => {
          setEditInfo(false);
        }}
        title={`${getLabel("editPage", initialLanguage)}fff`}
        // description='aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'
        footer={
          <Button className="btn-system-primary">{getLabel("save", initialLanguage)}</Button>
        }
      >
        <MemorialEditInfo />
      </FullScreenModal>
      <div className="bg-light pb-3">
        <div className="ps-md-4 py-3 ">
          {!isLoading ? (
            <button
              type="button"
              className="btn btn-system-primary border-0"
              onClick={() => {
                setMemorialCreateHome(true);
              }}
            >
              <strong>+ </strong>Create
            </button>
          ) : (
            <button
              type="button"
              className=" d-none d-sm-flex me-3 shimmer-button22 border-0 shimmer-effect"
            ></button>
          )}
        </div>
        <div className="ps-md-4 ">
          <div className="row ">
            {isLoading ? (
              <PagePackageSkelton loopTimes={count} />
            ) : ClientNoticeServices.length > 0 ? (
              ClientNoticeServices?.map((items: any) => {
                return (
                  <>
                    <div className="col-lg-7 col-md-8 col-sm-11 col-12 ">
                      <div className="card border bg-white mb-2 ">
                        <div className="card-body px-md-4 pt-3">
                          <div className="row m-0  ">
                            <div className="col-sm-12 col-md-6 col-lg-6 p-0">
                              <div className="card-title m-0 p-0 ">
                                {items?.title
                                  ? items.title
                                  : "Dummy-Memorial Page"}
                              </div>
                            </div>
                            <div className="col-sm-12 col-md-6 col-lg-6 p-0">
                              <p className="card-text system-danger d-flex justify-content-md-end font-110 font-normal ">
                                {items?.type
                                  ? items?.type
                                  : "Package not selected"}
                              </p>
                            </div>
                          </div>
                          <div className="mb-2 mb-md-3 mt-1 font-normal ">
                            <p
                              className="card-text text-black-50  text-truncate "
                              style={{ maxWidth: "100%" }}
                            >
                              {items?.created_at
                                ? extractDate(items?.created_at)
                                : ""}
                              <span className="card-text text-black-50 ">
                                {" "}
                                ·
                              </span>
                              <span className="card-text text-black-50 ">
                                {" "}
                                created by
                              </span>
                              <span className="card-text ms-1">
                                <a
                                  href=""
                                  className="text-decoration-none text-black-50 text-decoration-underline"
                                >
                                  {items?.creater
                                    ? items?.creater
                                    : "Samee Kishor"}
                                </a>
                              </span>
                            </p>
                          </div>
                          <div className="row align-items-center">
                            <div className="col-md-8 col-9 ">
                              <Link
                                href="#"
                                onClick={() => {
                                  // setEditInfo(true);
                                  router.push(`/notices/${items?.uuid}`);
                                }}
                                className="btn btn-md px-3 px-sm-4 border border-1 shadow-sm text-dark system-secondary-light me-2"
                              >
                                Edit
                              </Link>
                              <a
                                href="#"
                                className="btn btn-md px-3 px-sm-4 border shadow-sm text-dark system-secondary-light"
                              >
                                Preview
                              </a>
                            </div>
                            <div className="col-md-4 col-3 d-flex justify-content-end  ">
                              <div className="font-bold font-110">
                                {items?.currencySymbol}
                                <span>{items?.amount}</span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                );
              })
            ) : (
              <>
                <div className="row d-lg-none m-0">
                  {data.map(
                    (
                      data: any,
                      index: Key | null | undefined,
                      array: string | any[]
                    ) => {
                      return (
                        <div
                          key={index}
                          className="col-30 col-20 col-xxl-2 col-lg-3 col-md-4 col-sm-6 col-12 ps-0"
                        >
                          <DashboardCard content={data} />
                        </div>
                      );
                    }
                  )}
                </div>
                <div className="d-none d-lg-flex gap-3 flex-wrap d-style ">
                  {data.map(
                    (
                      data: any,
                      index: Key | null | undefined,
                      array: string | any[]
                    ) => {
                      return (
                        <div key={index} className="">
                          <DashboardCard content={data} />
                        </div>
                      );
                    }
                  )}
                </div>
              </>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default MemorialHome;
