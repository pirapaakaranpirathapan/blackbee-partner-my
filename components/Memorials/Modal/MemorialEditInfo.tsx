import React from "react";

const MemorialEditInfo = () => {
  const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  const defaultValue = "1";
  return (
    <>
      <div className="row align-items-center m-0 ">
        <div className="row m-0 p-0 mb-3 ">
          <div className="col-md-12 col-12 p-0 ">
            <div className="font-semibold ">
              Person Name <span className="text-danger">*</span>
            </div>
          </div>
          <div className="col-md-2 col-3 p-0 ">
            <select
              className="form-select border border-2 bg-white p-2 system-control"
              id="dropdownMenuLink"
            >
              <option defaultValue={"Mr"}>Mr</option>
              <option value="1">One</option>
              <option value="2">Two</option>
              <option value="3">Three</option>
            </select>
          </div>
          <div className="col-md-10 col-9  pe-0 ">
            <input
              type="text"
              className="form-control border border-2 p-2 system-control "
              placeholder="Full Name in English"
            />
          </div>
        </div>
        <div className="row m-0 p-0 mb-3">
          <div className="col-md-12 col-12 p-0">
            <div className="font-semibold">Preferred name (Nickname)</div>
          </div>
          <div className="col-md-12 col-12  p-0">
            <input
              type="text"
              className="form-control border border-2 p-2 system-control "
              placeholder="Know as"
            />
          </div>
        </div>
        <div className="row m-0 p-0 mb-3">
          <div className="col-md-4 pe-0 pe-md-3 col-12 p-0 mb-3 mb-md-0">
            <div className="row m-0">
              <div className="col-12 p-0 font-semibold">Date of Death</div>
              <div className=" col-12 p-0">
                <input
                  type="date"
                  className="form-control border border-2  system-control p-2"
                />
              </div>
            </div>
          </div>

          <div className="col-md-8 ps-0 ps-md-2 p-0">
            <div className="row m-0 y">
              <div className="col-12 p-0">
                <div className="font-semibold">Date of Birth</div>
              </div>
              <div className=" col-3  p-0  ">
                <select
                  className="form-select border border-2 bg-white  p-2 system-control"
                  id="dropdownMenuLink"
                >
                  <option value="">day</option>
                  {Array.from({ length: 31 }, (_, i) => i + 1).map((num) => (
                    <option key={num} value={num}>
                      {num}
                    </option>
                  ))}
                </select>
              </div>
              <div className=" col-5  p-0 px-1">
                <select
                  className="form-select border border-2 bg-white p-2 system-control"
                  id="dropdownMenuLink"
                >
                  <option value="">month</option>
                  {monthNames.map((name, index) => (
                    <option key={name} value={index + 1}>
                      {name}
                    </option>
                  ))}
                </select>
              </div>
              <div className=" col-4  p-0">
                <select
                  className="form-select border border-2 bg-white  p-2 system-control"
                  id="dropdownMenuLink"
                >
                  <option value="">year</option>
                  {Array.from(
                    { length: 120 },
                    (_, i) => new Date().getFullYear() - i
                  ).map((num) => (
                    <option key={num} value={num}>
                      {num}
                    </option>
                  ))}
                </select>
              </div>
            </div>
          </div>
        </div>

        <div className="row m-0 p-0 mb-3">
          <div className="col-md-12 col-12 p-0">
            <div className="font-semibold">Country of origin/native</div>
          </div>
          <div className="col-md-12 col-12  p-0">
            <select
              className="form-select border border-2 bg-white  p-2 system-control"
              id="dropdownMenuLink"
              defaultValue=""
            >
              <option value="" disabled selected>
                Public figure
              </option>
              <option value="">Public figure 1</option>
              <option value="">Public figure 2</option>
              <option value="">Public figure 3</option>
            </select>
          </div>
        </div>
        <div className="row m-0 p-0 mb-3 ">
          <div className="col-md-12 col-12 p-0">
            <div className="font-semibold">Death Place</div>
          </div>
          <div className="col-md-12 col-12  p-0">
            <input
              type="text"
              className="form-control border border-2 p-2 system-control "
              placeholder="Search city of death"
            />
          </div>
        </div>
        <div className="row m-0 p-0">
          <div className="col-md-12 col-12 p-0">
            <div className="font-semibold">Tags</div>
          </div>
          <div className="col-md-12 col-12  p-0">
            <input
              type="text"
              className="form-control border border-2 p-2 system-control "
              placeholder="Tag locations, topic, etc"
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default MemorialEditInfo;
