import React, { useContext, useEffect, useState } from "react";
import FullScreenModal from "@/components/Elements/FullScreenModal";
import Templates from "@/components/Design/Templates";
import { Button } from "react-bootstrap";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import {
  getAllPageTemplateFrames,
  pagetemplatesFrameSelector,
} from "@/redux/page-templates-frames";
import {
  getAllPageTemplateClipArts,
  pagetemplatesClipArtSelector,
} from "@/redux/page-templates-cliparts";
import { pagetemplatesSelector } from "@/redux/page-templates";
import { Notification } from "@arco-design/web-react";
import {
  memorialPageSelector,
  pageUpdate,
  patchPageDesign,
  patchPageTemplatesBorder,
  patchPageTemplatesClipArt,
  patchPageTemplatesframe,
  patchPageTemplatess,
  showPage,
} from "@/redux/memorial-pages";
import {
  getAllPageTemplateBorders,
  pagetemplatesBorderSelector,
} from "@/redux/page-templates-borders";
import router, { useRouter } from "next/router";
import { ImageNewUploder } from "@/components/ImageUploder/imageNewUploder";
import { mediaUpload } from "@/redux/media";
import ShimmerEffectText from "@/components/Skelton/ShimmerEffectText";
import CustomImage from "@/components/Elements/CustomImage";
import { getLabel } from "@/utils/lang-manager";
import CustomSelect from "@/components/Arco/CustomSelect";
import CustomSelectDouble from "@/components/Arco/CustomSelectDouble";
import ArcoNextTabs from "@/components/Design/ArcoNextTabs";
import { integer } from "aws-sdk/clients/storagegateway";
import DesignSkelton from "@/components/Skelton/DesignSkelton";

const Designs = () => {
  const { handleSave, PageData } = useContext(PageDataContext);
  const Data = PageData;
  const initialLanguage = PageData?.initialLanguage;
  const [birthDeathTitle, setBirthDeathTitles] = useState(
    Data?.birthDeathTitle
  );
  const [addTemplate, setAddTemplate] = useState(false);
  const [headerChnageTemplate, setheaderChnageTemplate] = useState(false);
  const [headerChnagePage, setheaderChnagePage] = useState(false);
  const [addBorder, setAddBorder] = useState(false);
  const [addClipArt, setAddClipArt] = useState(false);
  const [addFrame, setAddFrame] = useState(false);
  const [selectedOptionUUId, setSelectedOptionUUId] = useState(
    PageData.pagetemplate_uuid
  );
  const [selectedOptionId, setSelectedOptionId] = useState<integer>(
    Data?.["template_id"]
  );
  const [selectedFrameId, setSelectedFrameId] = useState(Data?.["frame_id"]);
  const [selectedClipArtId, setSelectedClipArtId] = useState(
    Data?.["clipart_id"]
  );
  const [selectedBorderId, setSelectedBorderId] = useState(Data?.["border_id"]);

  const [selectedOptionLink, setSelectedOptionLink] = useState("");
  const [selectedOptionAlt, setSelectedOptionAlt] = useState("");
  const [selectedBorderLink, setSelectedBorderLink] = useState(
    Data?.["border_url"]
  );
  const [selectedBorderAlt, setSelectedBorderAlt] = useState(
    Data?.["border_alt"]
  );
  const [selectedBorderUUId, setSelectedBorderUUId] = useState("");
  const [selectedClipArtLink, setSelectedClipArtLink] = useState(
    Data?.["clipart_url"]
  );
  const [selectedClipArtAlt, setSelectedClipArtAlt] = useState(
    Data?.["clipart_alt"]
  );
  const [selectedClipArtUUId, setSelectedClipArtUUId] = useState("");
  const [selectedFrameLink, setSelectedFrameLink] = useState(
    Data?.["frame_url"]
  );
  const [selectedFrameAlt, setSelectedFrameAlt] = useState(Data?.["frame_alt"]);
  const [selectedFrameUUId, setSelectedFrameUUId] = useState("");

  const idQuery = router.query.id;
  const momorialId = useRouter().query.id;
  const dispatch = useAppDispatch();

  //get Design Datas from selector
  const { data: showPageTemplatessData } = useAppSelector(
    pagetemplatesSelector
  );
  const { data: showTemplatesBorderData } = useAppSelector(
    pagetemplatesBorderSelector
  );
  const { data: showTemplatesClipArtData } = useAppSelector(
    pagetemplatesClipArtSelector
  );
  const { data: showTemplatesFrameData } = useAppSelector(
    pagetemplatesFrameSelector
  );
  const { dataOne: showtPageData } = useAppSelector(memorialPageSelector);

  const activePageTemplate = selectedOptionUUId;

  //fetch other designs if template update
  const loadStaticData = async () => {
    dispatch(
      getAllPageTemplateBorders({
        active_partner: ACTIVE_PARTNER,
        active_page_template: activePageTemplate,
      })
    );
    dispatch(
      getAllPageTemplateClipArts({
        active_partner: ACTIVE_PARTNER,
        active_page_template: activePageTemplate,
      })
    );
    dispatch(
      getAllPageTemplateFrames({
        active_partner: ACTIVE_PARTNER,
        active_page_template: activePageTemplate,
      })
    );
  };

  useEffect(() => {
    activePageTemplate && loadStaticData();
  }, [selectedOptionUUId, activePageTemplate, selectedOptionId]);

  useEffect(() => {
    setSelectedOptionUUId(Data.pagetemplate_uuid);
    setSelectedOptionId(Data?.["template_id"]);
    setSelectedOptionAlt(Data?.["template_alt"]);
    setSelectedFrameId(Data?.["frame_id"]);
    setSelectedClipArtId(Data?.["clipart_id"]);
    setSelectedBorderId(Data?.["border_id"]);
    setSelectedClipArtLink(Data?.["clipart_url"]);
    setSelectedClipArtAlt(Data?.["clipart_alt"]);
    setSelectedOptionLink(Data?.["template_url"]);
    setSelectedFrameLink(Data?.["frame_url"]);
    setSelectedFrameAlt(Data?.["frame_alt"]);
    setSelectedBorderAlt(Data?.["border_alt"]);
    // setLoading(!Data?.loading);
  }, [idQuery, Data]);

  //template
  const handleOptionSelection = (
    optionId: number,
    OptionLink: string,
    selectedOptionAlt: string,
    optionUUID: string
  ) => {
    setSelectedOptionId(optionId);
    // setSelectedFrameId("");
    // setSelectedClipArtId("");
    // setSelectedBorderId("");
    setSelectedOptionLink(OptionLink);
    setSelectedOptionAlt(selectedOptionAlt);
    setSelectedOptionUUId(optionUUID);
    setAddTemplate(false);
  };

  //border
  const handleBorderSelection = (
    optionId: number,
    OptionLink: string,
    selectedOptionAlt: string,
    optionUUID: string
  ) => {
    setSelectedBorderId(optionId);
    setSelectedBorderLink(OptionLink);
    setSelectedBorderAlt(selectedOptionAlt);
    setSelectedBorderUUId(optionUUID);
    setAddBorder(false);
  };

  //clipart
  const handleClipArtSelection = (
    optionId: number,
    OptionLink: string,
    selectedOptionAlt: string,
    optionUUID: string
  ) => {
    setSelectedClipArtId(optionId);
    setSelectedClipArtLink(OptionLink);
    setSelectedClipArtAlt(selectedOptionAlt);
    setSelectedClipArtUUId(optionUUID);
    setAddClipArt(false);
  };

  //frame
  const handleFrameSelection = (
    optionId: number,
    OptionLink: string,
    selectedOptionAlt: string,
    optionUUID: string
  ) => {
    setSelectedFrameId(optionId);
    setSelectedFrameLink(OptionLink);
    setSelectedFrameAlt(selectedOptionAlt);
    setSelectedFrameUUId(optionUUID);
    setAddFrame(false);
  };

  const [showHeaderImg, setShowHeaderImg] = useState("");
  const [showPageImg, setShowPageImg] = useState("");
  const [showClearButton, setShowClearButton] = useState(false);
  const handleUploadImage = (file: any) => {
    // upload(file);
    console.log("file", file);
    setShowHeaderImg(file);
    setShowClearButton(true);
  };
  const upload = (url: string) => {
    dispatch(
      pageUpdate({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_page: momorialId as unknown as string,
        updatedData: {
          header_background: {
            header_background_url: url,
          },
        },
      })
    ).then(async (response: any) => {
      if (response.payload.success == true) {
        console.log("response", response);

        Notification.success({
          title: response?.payload?.msg,
          duration: 3000,
          content: undefined,
        });
      } else {
        Notification.error({
          title: response?.payload?.error,
          duration: 3000,
          content: undefined,
        });
      }
    });
  };
  const deleteHeader = () => {
    setShowHeaderImg("");
    setShowClearButton(false);
    // dispatch(
    //   pageUpdate({
    //     active_partner: ACTIVE_PARTNER,
    //     active_service_provider: ACTIVE_SERVICE_PROVIDER,
    //     active_page: momorialId as unknown as string,
    //     updatedData: {
    //       header_background: {
    //         header_background_url: "",
    //       },
    //     },
    //   })
    // ).then(async (response: any) => {
    //   if (response.payload.success == true) {
    //     Notification.success({
    //       title: "Header Background Deleted Successfully",
    //       duration: 3000,
    //       content: undefined,
    //     });
    //   } else {
    //     Notification.error({
    //       title: "Header Background Deleted Failed",
    //       duration: 3000,
    //       content: undefined,
    //     });
    //   }
    // });
  };
  const deletePage = () => {
    dispatch(
      pageUpdate({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_page: momorialId as unknown as string,
        updatedData: {
          header_background: {
            header_background_url: "",
          },
        },
      })
    ).then(async (response: any) => {
      if (response.payload.success == true) {
        Notification.success({
          title: "Page Background Deleted Successfully",
          duration: 3000,
          content: undefined,
        });
      } else {
        Notification.error({
          title: "Page Background Deleted Failed",
          duration: 3000,
          content: undefined,
        });
      }
    });
  };
  // console.log("selectedBorderId", selectedOptionId);
  // console.log("selectedOptionId", selectedFrameId);
  // console.log("selectedClipArtId", selectedClipArtId);
  // console.log("selectedOptionIdselectedBorderId", selectedBorderId);

  //update design template,frame,clipart,border
  const handleUpdateDesign = () => {
    setLoading(true);
    const updatedData = {
      page_template_id: selectedOptionId,
      frame_id: selectedFrameId,
      clip_art_id: selectedClipArtId,
      border_id: selectedBorderId,
    };

    dispatch(
      patchPageDesign({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        updatedData,
        active_page: Data.active_page as unknown as string,
      })
    ).then((response: any) => {
      setLoading(!Data?.loading);
      if (response?.payload?.success == true) {
        // handleSave();
        Notification.success({
          title: "Updated successfully",
          duration: 3000,
          content: undefined,
        });
      } else {
        Notification.error({
          title: "Update failed",
          duration: 3000,
          content: undefined,
        });
      }
    });
  };
  const [isLoading, setLoading] = useState(false);
  return (
    <>
      {!Data?.loading || isLoading ? (
        <DesignSkelton />
      ) : (
        <>
          {/* Change template Start*/}
          <FullScreenModal
            show={addTemplate}
            size="xl"
            title={`${getLabel("templates", initialLanguage)}`}
            //  description={`${getLabel("templatesDescription",initialLanguage)}`}
            description={`${getLabel("templatesDescription", initialLanguage)}`}
            onClose={() => setAddTemplate(false)}
            // footer={
            //   <Button
            //     className="btn-system-primary pointer"
            //     onClick={() =>
            //       selectedOptionId
            //         ? selectedOptionId === Data?.["template_id"]
            //           ? Notification.warning({
            //             title: "Already selected Template",
            //             duration: 3000,
            //             content: undefined,
            //           })
            //           : handleTemplateUpdate()
            //         : Notification.error({
            //           title: "select Template  to Submit",
            //           duration: 3000,
            //           content: undefined,
            //         })
            //     }
            //   >
            //     {getLabel("submit",initialLanguage)}
            //   </Button>
            // }
          >
            {/* <Templates
              Data={showPageTemplatessData}
              defaultOptionId={Data?.["template_id"]}
              onOptionSelect={handleOptionSelection}
            /> */}
            <ArcoNextTabs
              Data={showPageTemplatessData}
              // defaultOptionId={Data?.["template_id"]}
              defaultOptionId={selectedOptionId}
              onOptionSelect={handleOptionSelection}
            />
          </FullScreenModal>

          {/* Change border Start*/}
          <FullScreenModal
            show={addBorder}
            size="xl"
            title={`${getLabel("borders", initialLanguage)}`}
            description={`${getLabel("templatesDescription", initialLanguage)}`}
            onClose={() => setAddBorder(false)}
            // footer={
            //   <Button
            //     className="btn-system-primary pointer"
            //     onClick={() =>
            //       selectedBorderId
            //         ? selectedBorderId === Data?.["border_id"]
            //           ? Notification.warning({
            //             title: "Already selected Border",
            //             duration: 3000,
            //             content: undefined,
            //           })
            //           : handleTemplateBorderUpdate()
            //         : Notification.error({
            //           title: "select Border  to Submit",
            //           duration: 3000,
            //           content: undefined,
            //         })
            //     }
            //   >
            //     {getLabel("submit",initialLanguage)}
            //   </Button>
            // }
          >
            {/* <Templates
              Data={showTemplatesBorderData}
              defaultOptionId={Data?.["border_id"]}
              onOptionSelect={handleBorderSelection}
            /> */}
            <ArcoNextTabs
              Data={showTemplatesBorderData}
              defaultOptionId={selectedBorderId}
              onOptionSelect={handleBorderSelection}
            />
          </FullScreenModal>
          {/* Change clipart Start*/}
          <FullScreenModal
            show={addClipArt}
            size="xl"
            title={`${getLabel("clipArts", initialLanguage)}`}
            description={`${getLabel("templatesDescription", initialLanguage)}`}
            onClose={() => setAddClipArt(false)}
            // footer={
            //   <Button
            //     className="btn-system-primary pointer"
            //     onClick={() =>
            //       selectedClipArtId
            //         ? selectedClipArtId === Data?.["clipart_id"]
            //           ? Notification.warning({
            //             title: "Already selected Clipart",
            //             duration: 3000,
            //             content: undefined,
            //           })
            //           : handleTemplateClipArtUpdate()
            //         : Notification.error({
            //           title: "select Clipart  to Submit",
            //           duration: 3000,
            //           content: undefined,
            //         })
            //     }
            //   >
            //     {getLabel("submit",initialLanguage)}
            //   </Button>
            // }
          >
            {/* <Templates
              Data={showTemplatesClipArtData}
              defaultOptionId={Data?.["clipart_id"]}
              onOptionSelect={handleClipArtSelection}
            /> */}
            <ArcoNextTabs
              Data={showTemplatesClipArtData}
              defaultOptionId={selectedClipArtId}
              onOptionSelect={handleClipArtSelection}
            />
          </FullScreenModal>

          {/* Change frames Start*/}
          <FullScreenModal
            show={addFrame}
            size="xl"
            title={`${getLabel("frames", initialLanguage)}`}
            description={`${getLabel("templatesDescription", initialLanguage)}`}
            onClose={() => setAddFrame(false)}
            // footer={
            //   <Button
            //     className="btn-system-primary pointer"
            //     onClick={() =>
            //       selectedFrameId
            //         ? selectedFrameId === Data?.["frame_id"]
            //           ? Notification.warning({
            //             title: "Already selected Frame",
            //             duration: 3000,
            //             content: undefined,
            //           })
            //           : handleTemplateFrameUpdate()
            //         : Notification.error({
            //           title: "select Frame  to Submit",
            //           duration: 3000,

            //           content: undefined,
            //         })
            //     }
            //   >
            //     {getLabel("submit",initialLanguage)}
            //   </Button>
            // }
          >
            {/* <Templates
              Data={showTemplatesFrameData}
              defaultOptionId={Data?.["frame_id"]}
              onOptionSelect={handleFrameSelection}
            /> */}
            <ArcoNextTabs
              Data={showTemplatesFrameData}
              defaultOptionId={selectedFrameId}
              onOptionSelect={handleFrameSelection}
            />
          </FullScreenModal>

          {/* Change header background Start*/}
          <FullScreenModal
            show={headerChnageTemplate}
            size="xl"
            title={`${getLabel("headerBackground", initialLanguage)}`}
            description={`${getLabel("templatesDescription", initialLanguage)}`}
            onClose={() => setheaderChnageTemplate(false)}
            // footer={
            //   <Button
            //     className="btn-system-primary pointer"
            //     onClick={() =>
            //       selectedOptionId
            //         ? selectedOptionId === Data?.["template_id"]
            //           ? Notification.warning({
            //             title: "Already selected Template",
            //             duration: 3000,
            //             content: undefined,
            //           })
            //           : handleTemplateUpdate()
            //         : Notification.error({
            //           title: "select Template  to Submit",
            //           duration: 3000,
            //           content: undefined,
            //         })
            //     }
            //   >
            //     {getLabel("submit",initialLanguage)}
            //   </Button>
            // }
          >
            {/* <Templates
          Data={showPageTemplatessData}
          defaultOptionId={selectedOptionId}
          onOptionSelect={handleOptionSelection}
        /> */}
          </FullScreenModal>

          {/* ************change page header start */}
          <FullScreenModal
            show={headerChnagePage}
            size="xl"
            title={`${getLabel("pageBackground", initialLanguage)}`}
            description={`${getLabel("templatesDescription", initialLanguage)}`}
            onClose={() => setheaderChnagePage(false)}
            // footer={
            //   <Button
            //     className="btn-system-primary pointer"
            //     onClick={() =>
            //       selectedOptionId
            //         ? selectedOptionId === Data?.["template_id"]
            //           ? Notification.warning({
            //             title: "Already selected Template",
            //             duration: 3000,
            //             content: undefined,
            //           })
            //           : handleTemplateUpdate()
            //         : Notification.error({
            //           title: "select Template  to Submit",
            //           duration: 3000,
            //           content: undefined,
            //         })
            //     }
            //   >
            //     {getLabel("submit",initialLanguage)}
            //   </Button>
            // }
          >
            {/* <Templates
          Data={showPageTemplatessData}
          defaultOptionId={selectedOptionId}
          onOptionSelect={handleOptionSelection}
        /> */}
          </FullScreenModal>

          <div className="pe-md-5">
            <div className="mb-4 page-heading font-bold m-0">
              {getLabel("designSettings", initialLanguage)}
            </div>
            <div className="row  align-items-center  m-0 mb-3 ">
              <div className="col-md-3 col-12  p-0 m-0 font-bold  pb-2 pb-md-0">
                {getLabel("birthAndDeathTitle", initialLanguage)}
              </div>
              <div className="col-md-9 col-12  p-0 m-0 page-arco">
                <CustomSelectDouble
                  data={Data?.birthDeathTitles}
                  placeholderValue={getLabel(
                    "selectBirthandDeathTitle",
                    initialLanguage
                  )}
                  onChange={(value: any) => setBirthDeathTitles(value)}
                  is_search={false}
                  defaultValue={Data?.birthDeathTitle_id}
                  firstvalue={"birth_title"}
                  secondvalue={"death_title"}
                />
              </div>
            </div>
            <div className="row mb-5 m-0">
              <div className="col-lg-3  col-12 d-none d-sm-flex p-0 m-0 "></div>
              <div className="col-lg-9 col-12  p-0">
                <button type="button" className="btn btn-system-primary">
                  {getLabel("save", initialLanguage)}
                </button>
                {/*------------------------------------------------------------------------------*/}
              </div>
            </div>
            <div className="row  align-items-start  m-0 mb-5">
              <div className="col-md-3 col-12 p-0 font-bold pb-sm-2 pb-2 pb-md-0">
                {getLabel("decoration", initialLanguage)}
              </div>
              <div className="col-md-9 p-0">
                <div className="d-flex gap-3 justify-content-start  text-center flex-wrap ">
                  <div className="d-flex flex-column justify-content-center">
                    <div className="">
                      <CustomImage
                        src={selectedOptionLink}
                        alt={selectedOptionAlt}
                        width={85}
                        height={92}
                        className={"image"}
                        divClass={
                          "position-relative decoration-img-container  rounded d-flex align-items-center justify-content-center system-filter-secondary "
                        }
                      />
                    </div>
                    <p
                      onClick={() => setAddTemplate(true)}
                      className="font-normal font-90 system-text-primary pt-1 pointer"
                    >
                      {selectedOptionId
                        ? `${getLabel("changeTemplate", initialLanguage)}`
                        : `${getLabel("browseTemplate", initialLanguage)}`}
                    </p>
                  </div>
                  <div className="d-flex flex-column justify-content-center">
                    <div className="  ">
                      <CustomImage
                        src={selectedFrameLink || ""}
                        alt={selectedFrameAlt}
                        className="image "
                        width={85}
                        height={92}
                        divClass={
                          "position-relative decoration-img-container rounded d-flex align-items-center justify-content-center system-filter-secondary"
                        }
                      />
                    </div>
                    <p
                      onClick={() => setAddFrame(true)}
                      className="font-normal font-90 system-text-primary pt-1 pointer"
                    >
                      {selectedFrameId ? " Change  Frame" : " Browse Frame"}
                    </p>
                  </div>
                  <div className="d-flex flex-column justify-content-center">
                    <div className="  ">
                      <CustomImage
                        src={selectedClipArtLink}
                        alt={selectedClipArtAlt}
                        className="image"
                        width={85}
                        height={92}
                        divClass={
                          "decoration-img-container rounded d-flex align-items-center justify-content-center system-filter-secondary position-relative"
                        }
                      />
                    </div>
                    <p
                      onClick={() => setAddClipArt(true)}
                      className="font-normal font-90 system-text-primary pt-1 pointer"
                    >
                      {selectedClipArtId
                        ? `${getLabel("changeClipArt", initialLanguage)}`
                        : `${getLabel("browseClipArt", initialLanguage)}`}
                    </p>
                  </div>
                  <div className="d-flex flex-column justify-content-center">
                    <div className="  ">
                      <CustomImage
                        src={selectedBorderLink || ""}
                        alt={selectedBorderAlt}
                        className="image"
                        width={85}
                        height={92}
                        divClass={
                          "decoration-img-container rounded d-flex align-items-center justify-content-center system-filter-secondary position-relative"
                        }
                      />
                    </div>
                    <p
                      onClick={() => setAddBorder(true)}
                      className="font-normal font-90 system-text-primary pt-1 pointer"
                    >
                      {selectedBorderId
                        ? `${getLabel("changeBorder", initialLanguage)}`
                        : `${getLabel("browseBorder", initialLanguage)}`}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row  align-items-start  m-0 mb-5">
              <div className="col-md-3 col-12 p-0 font-bold pb-sm-2 pb-2 pb-md-0">
                {getLabel("headerBackground", initialLanguage)}
              </div>
              <div className="col-md-9 p-0 ">
                <div className="system-text-primary">
                  <div className="">
                    {showHeaderImg == "" ? (
                      <></>
                    ) : (
                      <>
                        <CustomImage
                          src={
                            // showtPageData?.header_background?.image_url
                            //   ? showtPageData?.header_background?.image_url
                            //   : showHeaderImg
                            showHeaderImg
                          }
                          alt="memorialprofile"
                          className="image rounded "
                          width={850}
                          height={920}
                          divClass={
                            "header-background-container system-filter-secondary rounded position-relative"
                          }
                        />
                      </>
                    )}
                  </div>
                  <div className="d-flex gap-3">
                    {/* {showtPageData?.header_background?.image_url ==
                    "/header/background/image-1.png"  */}
                    <div
                      className="font-90 font-normal system-text-primary pointer"
                      onClick={() => setheaderChnageTemplate(true)}
                    >
                      {showHeaderImg == ""
                        ? "Select header background"
                        : "Change header background"}
                    </div>
                    {showHeaderImg == "" ? (
                      <>
                        <ImageNewUploder
                          name="Upload"
                          textOnly={true}
                          classLi={true}
                          handleUploadImage={handleUploadImage}
                          headerText={"Upload Header Background"}
                        />
                      </>
                    ) : (
                      <>
                        <ImageNewUploder
                          name="Upload"
                          textOnly={true}
                          classLi={true}
                          handleUploadImage={handleUploadImage}
                          headerText={"Upload Header Background"}
                        />
                      </>
                    )}
                    <div
                      className="font-90 font-normal system-text-primary pointer"
                      onClick={deleteHeader}
                    >
                      {/* {getLabel("delete",initialLanguage)} */}
                      {showClearButton ? "Clear" : ""}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row  align-items-start  m-0 mb-5">
              <div className="col-md-3 col-12 p-0 font-bold pb-sm-2 pb-2 pb-md-0">
                {getLabel("pageBackground", initialLanguage)}
              </div>
              <div className="col-md-9 p-0">
                <div className="system-text-primary">
                  <div className="">
                    {showPageImg == "" ? (
                      <></>
                    ) : (
                      <>
                        <CustomImage
                          src={showPageImg}
                          alt="memorialprofile"
                          className="image rounded"
                          width={85}
                          height={92}
                          divClass={
                            "page-background-container system-filter-secondary"
                          }
                        />
                      </>
                    )}
                  </div>
                  <div className="d-flex gap-3">
                    <div
                      className="font-90 font-normal system-text-primary pointer"
                      onClick={() => setheaderChnagePage(true)}
                    >
                      {/* {getLabel("changePageBackground", initialLanguage)} */}
                      {showPageImg == ""
                        ? "Select Page background"
                        : "Change Page background"}
                    </div>
                    {/* <div className="font-90 font-normal system-text-primary pointer">
                  Upload
                </div> */}
                    <div
                      className="font-90 font-normal system-text-primary pointer"
                      onClick={deletePage}
                    >
                      {/* {getLabel("delete",initialLanguage)} */}
                      Clear
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row mb-5 m-0">
              <div className="col-lg-3  col-12 d-none d-sm-flex p-0 m-0 "></div>
              <div className="col-lg-9 col-12  p-0">
                <button
                  type="button"
                  className="btn btn-system-primary"
                  onClick={handleUpdateDesign}
                >
                  {getLabel("save", initialLanguage)}
                </button>
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
};
export default Designs;
