import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";
import { patchGenralSettings } from "@/redux/memorial-pages";
import { useAppDispatch } from "@/redux/store/hooks";
import React, { useContext, useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Notification } from "@arco-design/web-react";
import Image from "next/image";
import Loader from "@/components/Loader/Loader";
import GenralSettingSkelton from "@/components/Skelton/GenralSettingSkelton";
import { InputGroup } from "react-bootstrap";
import { MonthDate } from "@/components/TimeConverter/MonthFormat";
import { getLabel } from "@/utils/lang-manager";
import CustomSelect from "@/components/Arco/CustomSelect";

const Settings = () => {
  const { handleSave, PageData } = useContext(PageDataContext);
  const Data = PageData;
  const initialLanguage = PageData?.initialLanguage
  //  const Data=PageData||Add any Data;
  const [visibility, setvisibility] = useState(Data?.visibility);
  const [PagePassword, setpagePassword] = useState(Data?.password);
  const [pageTypeId, setPageTypeID] = useState(Data?.pagetype_id);
  const [relationship, setRelationship] = useState(Data?.relationship);
  const [isLoading, setIsLoading] = useState(false); // Loading state
  const [err, setErr] = useState("");
  const [error, setError] = useState(true);
  const [showPassword, setShowPassword] = useState(false);
  const [isModalFunctionCalled, setIsModalFunctionCalled] = useState(false);
  const dispatch = useAppDispatch();
  const handleVisibilityChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedValue = e.target.value;
    setvisibility(selectedValue);
  };

  const handlePageTypeChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedValue = e.target.value;
    setPageTypeID(selectedValue);
  };
  const handleRelationshipsChange = (
    e: React.ChangeEvent<HTMLSelectElement>
  ) => {
    const selectedValue = e.target.value;
    setRelationship(selectedValue);
  };
  const handleUpdate = () => {
    setIsLoading(true); // Set loading state to true
    setIsModalFunctionCalled(true); // Execute handleSave function here
    const updatedData = {
      visibility_id: visibility,
      page_type_id: pageTypeId,
      relationship_id: relationship,
    };
    dispatch(
      patchGenralSettings({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        updatedData,
        active_notice: Data.active_page as unknown as string,
      })
    )
      .then((response: any) => {
        //----------------To get updated data from index -------------------------------//
        // Dispatch action completed successfully
        console.log("Dispatch action completed", Data.active_page);
        handleSave();

        if (response?.payload?.success) {
          setErr("");
          Notification.success({
            title: "Updated successfully",
            duration: 3000,
            content: undefined,
          });
        } else {
          setErr("apiError.error.errors.name");
          setError(true);
          Notification.error({
            title: "Updated Failed",
            duration: 3000,
            content: undefined,
          });
        }
        setTimeout(() => {
          setIsModalFunctionCalled(false); // Set isModalFunctionCalled to false after a specific duration (e.g., 2 seconds)
        }, 1);
        // Execute handleSave function here
      })
      .finally(() => {
        setIsLoading(false); // Set loading state back to false
        setTimeout(() => {
          setIsModalFunctionCalled(false); // Set isModalFunctionCalled to false after a specific duration (e.g., 2 seconds)
        }, 1000);
      });
  };

  useEffect(() => {
    setvisibility(Data?.visibility || 1);
    setpagePassword(Data?.password);
    setPageTypeID(Data?.pagetype_id);
    setRelationship(Data?.relationship);
  }, [Data]);

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };
  return (
    <div className="">
      {" "}
      {/* {isModalFunctionCalled && (
        <div >
          <Loader />
        </div>
      )} */}
      {isModalFunctionCalled || !Data.loading ? (
        <>
          {" "}
          <GenralSettingSkelton />
        </>
      ) : (
        <>
          <div className="mb-4 page-heading font-bold">
            {getLabel("generalSettings", initialLanguage)}
          </div>
          <div className="overlay-container">
            <div className="row m-0 my-3 align-items-center">
              <div className="col-md-3 col-12 p-0 pe-2 fw-semibold">
                {getLabel("visibility", initialLanguage)}
              </div>
              <div className="col-md-8 col-12  p-0 page-arco">
                {/* <select
                  className="form-select border border-2 bg-white fw-normal p-2 system-control"
                  id="dropdownMenuLink"
                  defaultValue={Data?.visibility}
                  onChange={handleVisibilityChange}
                >
                  {Array.isArray(Data?.visibilities) &&
                    Data.visibilities.map((option: any) => (
                      <option key={option.id} value={option.id}>
                        {option.name}
                      </option>
                    ))}
                </select> */}
                <CustomSelect
                  data={Data.visibilities}
                  placeholderValue={"Select a visibility"}
                  onChange={(value: any) => {
                    setvisibility(value);
                  }}
                  is_search={false}
                  defaultData={Data?.visibility}
                  defaultValue={Data?.visibility}
                />
              </div>
            </div>
            <div className="row m-0 align-items-center ">
              <div className="col-md-3 col-12 p-0 pe-2 fw-semibold">
                {getLabel("pagePassword", initialLanguage)}
              </div>
              <div className="col-md-8 col-12 p-0 position-relative">
                <input
                  type={showPassword ? "text" : "password"}
                  className="form-control border border-2 p-2 system-control"
                  placeholder=""
                  value={PagePassword}
                  onChange={(e) => setpagePassword(e.target.value)}
                />

                <div className="">
                  <Image
                    onClick={togglePasswordVisibility}
                    src={showPassword ? "/openeye.svg" : "/closeeye.svg"}
                    alt={showPassword ? "eyeIcon icon" : "closeeye icon"}
                    width={16}
                    height={16}
                    className="pswd-group-eye "
                  />
                </div>
              </div>
            </div>
            <div className="row m-0 my-3 align-items-center">
              <div className="col-md-3 col-12 p-0 pe-2 fw-semibold">
                {getLabel("pageType", initialLanguage)}
              </div>
              <div className="col-md-8 col-12 p-0 page-arco">
                {/* <select
                  className="form-select border border-2 bg-white fw-normal p-2 system-control"
                  id="dropdownMenuLink"
                  defaultValue={Data?.pagetype_id}
                  onChange={handlePageTypeChange}
                >
                  {Array.isArray(Data?.page_types) &&
                    Data.page_types.map((option: any) => (
                      <option key={option.id} value={option.id}>
                        {option.name}
                      </option>
                    ))}
                </select> */}
                <CustomSelect
                  data={Data.page_types}
                  placeholderValue={"Select a page type"}
                  onChange={(value: any) => {
                    setPageTypeID(value);
                  }}
                  is_search={false}
                  defaultValue={Data?.pagetype_id}
                />
              </div>
            </div>
            <div className="row m-0 my-3 align-items-center ">
              <div className="col-md-3 col-12 p-0 pe-2 fw-semibold">
                {getLabel("relationship", initialLanguage)}
              </div>
              <div className="col-md-8 col-12  p-0 page-arco">
                {/* <select
                  className="form-select border border-2 bg-white system-control p-2"
                  id="dropdownMenuLink"
                  defaultValue={Data?.relationship}
                  onChange={handleRelationshipsChange}
                >
                  {Array.isArray(Data?.relationships) &&
                    Data.relationships.map((option: any) => (
                      <option key={option.id} value={option.id}>
                        {option.name}
                      </option>
                    ))}
                </select> */}
                <CustomSelect
                  data={Data.relationships}
                  placeholderValue={"Select a relationship"}
                  onChange={(value: any) => {
                    setRelationship(value);
                  }}
                  is_search={true}
                  defaultValue={Data?.relationship}
                />
              </div>
            </div>{" "}
          </div>
          <div className="row m-0 mb-3">
            <div className="col-lg-3  col-12 d-none d-sm-flex "></div>
            <div className="col-lg-9  col-12 p-0">
              <button
                type="button"
                className="btn btn-system-primary my-1 d-flex align-items-center justify-content-center"
                onClick={handleUpdate}
              >
                {getLabel("save", initialLanguage)}
              </button>
            </div>
          </div>
          <hr className="mt-5  hr-width-form3" />
          {/* <div className="page-heading font-bold">Donation</div>

      <div className="row m-0 my-3 align-items-center">
        <div className="col-md-3 col-12 p-0 pe-2">Select Charity</div>
        <div className="col-md-8 col-12  p-0">
          <select
            className="form-select border border-2 bg-white system-control fw-normal p-2"
            id="dropdownMenuLink"
          >
            <option defaultValue={"british-redcross"}>British Redcross</option>
          </select>
        </div>
      </div>

      <div className="row m-0 mb-3">
        <div className="col-lg-3 col-12 d-none d-sm-flex"></div>
        <div className="col-lg-9 col-12 p-0">
          <button type="button" className="btn btn-system-primary my-1">
            Save
          </button>
        </div>
      </div>
      <hr className="mt-5  hr-width-form3" /> */}
          <div className="mb-4 page-heading font-bold">
            {getLabel("recordInformation", initialLanguage)}
          </div>
          <div className="row m-0 mb-3 align-items-center fw-semibold">
            <div className="col-md-3 col-12 p-0 pe-2">
              {getLabel("memorialPageID", initialLanguage)}
            </div>
            <div className="col-md-3 col-12 p-0 font-normal">
              {Data?.memorial_page_id}
            </div>
          </div>
          <div className="row m-0 mb-3 align-items-center fw-semibold">
            <div className="col-md-3 col-12 p-0 pe-2">
              {getLabel("createdAt", initialLanguage)}
            </div>
            <div className="col-md-3 col-12 font-normal p-0">
              {MonthDate(Data?.created_at)}
            </div>
          </div>
          <div className="row m-0 mb-3 align-items-center fw-semibold">
            <div className="col-md-3 col-12 p-0 pe-2">
              {getLabel("lastModifiedAt", initialLanguage)}
            </div>
            <div className="col-md-3 col-12 font-normal p-0">
              {/* 12 Nov 2022, 13:49 GMT  */}
              {MonthDate(Data?.last_modified_at)}
            </div>
          </div>
          <div className="row m-0 mb-3 align-items-center fw-semibold">
            <div className="col-md-3 col-12 p-0 pe-2">
              {getLabel("createdBy", initialLanguage)}
            </div>
            <div className="col-md-3 col-12 font-normal p-0 text-truncate">
              {Data?.created_by}
            </div>
          </div>
        </>
      )}
      {/* <div className="row m-0 mb-3 align-items-center fw-semibold">
        <div className="col-md-3 col-12  p-0 pe-2">History</div>
        <div className="col-md-3 col-12 system-text-primary font-normal p-0 pointer">
          View History
        </div>
      </div> */}
    </div>
  );
};

export default Settings;
