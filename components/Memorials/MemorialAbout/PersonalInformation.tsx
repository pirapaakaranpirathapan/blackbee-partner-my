// import DatePicker from "@/components/Accessories/Datepicker";
import Loader from "@/components/Loader/Loader";
import LocationsSkelton from "@/components/Skelton/LocationsSkelton";
import PersonalInformationSkelton from "@/components/Skelton/Personal InformationSkelton";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";
import { gendersSelector, getAllGenders } from "@/redux/genders";
import {
  personalLocationsSelector,
  updatePersonalLocations,
} from "@/redux/locations";
import { memorialPageSelector, showPage } from "@/redux/memorial-pages";
import {
  getAllNatureOfDeath,
  natureOfDeathSelector,
} from "@/redux/nature-of-death";
import {
  personalInformationSelector,
  updatePersonalInformation,
} from "@/redux/personal-information";
import {
  relatedInformationSelector,
  updateRelatedInformation,
} from "@/redux/related-information";
import { getAllReligions, religionSelector } from "@/redux/religion";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import { useRouter } from "next/router";
import React, { useContext, useEffect, useState } from "react";
import { Notification } from "@arco-design/web-react";
import PlaceAutocomplete from "@/components/Elements/PlaceAutoComplete";
import { useApiErrorHandling } from "@/components/Others/useApiErrorHandling";
import PlaceMulticomplete from "@/components/Elements/PlaceMultiComplete";
import { getLabel } from "@/utils/lang-manager";
import { CountriesSelector } from "@/redux/countries";
import CustomSelect from "@/components/Arco/CustomSelect";
import PlaceAutocomplete2 from "@/components/Elements/PlaceAutoComplete2";
import { DatePicker } from "@arco-design/web-react";
import CustomDatePicker from "@/components/Accessories/CustomDatePicker";
import CustomDropDownDatepicker from "@/components/Accessories/CustomDropDownDatepicker";
import CustomTextBox from "@/components/Elements/CustomTextBox";
import { supportedLanguages } from "@/config/noticeLanguges/languages";
import getYearFromDate from "@/components/Utils/YearExtracter";
import convertDateFormat from "@/components/Utils/MonthFormatter";
import CustomToast from "@/components/Utils/CustomToast";

const { YearPicker, MonthPicker } = DatePicker;

const PersonalInformation = () => {
  const dispatch = useAppDispatch();
  const router = useRouter();
  const uuid = router.query?.id;

  const { data: gendersData } = useAppSelector(gendersSelector);
  const { data: religionData } = useAppSelector(religionSelector);
  const { data: showCountriesData } = useAppSelector(CountriesSelector);
  const { data: natureOfDeathData } = useAppSelector(natureOfDeathSelector);
  // const { dataOne: showtPageData } = useAppSelector(memorialPageSelector);
  const { handleSave, PageData } = useContext(PageDataContext);
  const Data = PageData;
  const initialLanguage = PageData?.initialLanguage;
  const [salutation, setSalutation] = useState("");
  const [salutationId, setSalutationID] = useState("");
  const [fullName, setFullName] = useState("");
  const [language, setLanguage] = useState("");
  const [personTamilName, setPersonTamilName] = useState("");
  const [preferredName, setPreferredName] = useState("");
  const [briefAbout, setBriefAbout] = useState("");
  const [quote, setQuote] = useState("");
  const [genders, setGenders] = useState("");
  // const [dob, setDob] = useState("");
  // const [dod, setDod] = useState("");
  const [dob, setDob] = useState<string>("");
  const [dod, setDod] = useState<string>("");
  const [success, setSuccess] = useState(false);
  const [clickstatusPersonalInfo, setClickStatusPersonalInfo] = useState(true);
  const [clickstatusLocations, setClickStatusLocations] = useState(true);
  const [clickstatusRelations, setClickStatusRelations] = useState(true);
  const [response, setResponse] = useState("");
  const [profileDisplayLanguage, setProfileDisplayLanguage] = useState("Tamil");
  const [deathPlace, setDeathPlace] = useState("");
  const [livedPlaces, setLivedPlaces] = useState<string[]>([]);
  const [homeTown, setHomeTown] = useState("");
  const [countryId, setCountryId] = useState<string>("");
  const [birthPlace, setBirthPlace] = useState("");
  const [nativePlace, setNativePlace] = useState("");
  // ====================================================================
  const [religion, setReligion] = useState("");
  const [reasonDeath, setReasonDeath] = useState("");
  const [schoolAndColleges, setSchoolAndColleges] = useState("");
  //dateof birth arco design
  const [day, setDay] = useState<string>("");
  const [month, setMonth] = useState<string>("");
  const [year, setYear] = useState<string>("");
  const [finalBirthDate, setFinalBirthDate] = useState<string>("");
  const [selectedLanguage, setSelectedLanguage] = useState("");

  useEffect(() => {
    const selectedLanguageObject = Data?.languages.find(
      (lang: any) => lang.id === language
    );
    if (selectedLanguageObject) {
      setSelectedLanguage(selectedLanguageObject.name);
    }
  }, [language, Data?.languages]);
  // const today = new Date().toISOString().split("T")[0];
  const today = new Date().toISOString().split("T")[0];
  useEffect(() => {
    setDob(Data?.dob);
    setDod(
      Data?.dod && Data?.dod !== "" && Data?.dod.split("-").reverse().join("-")
    );
  }, [Data?.["full_name"]]);
  // const handleDayChange = (day: string) => {
  //   setDay(day);
  //   updateFinalDate();
  // };
  const handleDayChange = (date: string) => {
    const dateObj = new Date(date);
    const newDay = dateObj.getDate().toString();
    setDay(newDay);
    updateFinalDate(newDay, month, year);
  };

  const handleMonthChange = (date: string) => {
    const dateObj = new Date(date);
    const newMonth = (dateObj.getMonth() + 1).toString(); // Month is zero-indexed, so add 1
    setMonth(newMonth);
    updateFinalDate(day, newMonth, year);
  };

  const handleYearChange = (date: string) => {
    const dateObj = new Date(date);
    const newYear = dateObj.getFullYear().toString();
    setYear(newYear);
    updateFinalDate(day, month, newYear);
  };

  const updateFinalDate = (
    newDay: string,
    newMonth: string,
    newYear: string
  ) => {
    const BirthDate = `${newYear}-${newMonth}-${newDay}`;
    setFinalBirthDate(BirthDate);
  };

  // ================================================================================================

  // // const languages = showtPageData?.languages;
  // const loadData = async () => {
  //   setClickStatus(true);
  //   await dispatch(
  //     showPage({
  //       active_partner: ACTIVE_PARTNER,
  //       active_service_provider: ACTIVE_SERVICE_PROVIDER,
  //       active_page: uuid as unknown as string,
  //     })
  //   ).then((response) => {
  //     console.log(response);
  //     setClickStatus(false);
  //   });
  // };

  useEffect(() => {
    dispatch(getAllGenders());
    dispatch(getAllReligions());
    dispatch(getAllNatureOfDeath());
    if (dob >= dod || dob >= today) {
      // setDob("");
    }
  }, []);
  // useEffect(() => {
  //   loadData();
  //   // clearErr();
  // }, [uuid]);

  useEffect(() => {
    setSalutationID(PageData?.abbreviation_id);
    setSalutation(PageData?.abbreviation_id);
    setFullName(PageData?.full_name);
    setPersonTamilName(PageData?.full_lang_name);
    setPreferredName(PageData?.nick_name);
    setLanguage(PageData?.language_id);
    setBriefAbout(PageData?.brief_about); //----------------------------------------------------????????-----------------------------------------------
    setQuote(PageData?.quote);
    setDeathPlace(PageData?.death_location);
    setHomeTown(PageData?.hometown_location);
    setBirthPlace(PageData?.birth_location);
    setGenders(PageData?.gender_id);
    // setDob(PageData?.dob|| "-");
    // setDod(PageData?.dod);
    // setDod(PageData?.dod && PageData?.dod !== '' && PageData?.dod.split('-').reverse().join('-'));
    setReligion(PageData?.religion_id);
    setReasonDeath(PageData?.nature_of_death_id);
    setCountryId(PageData?.Country_id);
  }, [PageData]);
  useEffect(() => {
    if (PageData?.loading && PageData?.languages) {
      setClickStatusRelations(false);
      setClickStatusLocations(false);
      setClickStatusPersonalInfo(false);
    }
  }, [PageData]);
  const handleSalutationChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedValue = e.target.value;
    setSalutation(selectedValue);
    setSalutationID(selectedValue);
  };

  const handleLanguageChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedValue = e.target.value;
    setLanguage(selectedValue);
  };
  const handleGendersChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedValue = e.target.value;
    setGenders(selectedValue);
  };
  const handleReligionChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedValue = e.target.value;
    setReligion(selectedValue);
  };
  const handleReasonDeathChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedValue = e.target.value;
    setReasonDeath(selectedValue);
  };
  const handleDateChange = (selectedDate: string) => {
    setDob(selectedDate);
  };
  const handleInputChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    field: string
  ) => {
    // const value = e.target.value;
    // Update the character count dynamically
    const value = e.target.value;
    const currentLength = value.length;
    const maxLength = 600; // Your desired character limit

    switch (field) {
      case "fullName":
        setFullName(value);
        break;
      case "personTamilName":
        setPersonTamilName(value);
        break;
      case "preferredName":
        setPreferredName(value);
        break;
      case "briefAbout":
        if (currentLength <= maxLength) {
          setBriefAbout(value);
        }
        break;
      case "quote":
        if (currentLength <= maxLength) {
          setQuote(value);
        }
        break;

      // case "dateOfBirth":
      //   if (!dod || (value <= dod && value <= today)) {
      //     value <= today ? setDob(value) : setDob("");
      //   } else {
      //     setDob("");
      //   }
      //   break;
      // case "dateOfDeath":
      //   if (!dod || value <= today) {
      //     setDod(value);
      //     if (dob >= dod && dob <= today) {
      //       setDob("");
      //     } else {
      //       setDob("");
      //     }
      //   } else {
      //     setDod("");
      //   }
      //   break;

      // =================
      case "deathPlace":
        setDeathPlace(value);
        break;
      // case "livedPlace":
      //   setLivedPlace([...livedPlace, value]);
      //   break;
      // case "countryOrigin":
      //   setCountryOrigin(value);
      //   break;
      case "homeTown":
        setHomeTown(value);
        break;
      case "birthPlace":
        setBirthPlace(value);
        break;

      // =================
      // case "community":
      //   setCommunity(value);
      //   break;
      case "schoolAndColleges":
        setSchoolAndColleges(value);
        break;
      // case "hashTags":
      //   setHashTags(value);
      //   break;

      default:
        break;
    }
  };
  const maxLength = 600;
  const initialChars = 23;

  // const handleInputDateChange = (date: Date | null, field: string) => {
  //   if (date) {
  //     const formattedDate = date.toISOString().split('T')[0];
  //     switch (field) {
  //       case "dateOfBirth":
  //         setDob(formattedDate);
  //         break;
  //       case "dateOfDeath":
  //         setDod(formattedDate);
  //         break;
  //     }
  //   }
  // };
  // const handleInputDateChange = (date: string, field: string) => {
  //   if (date === undefined) {
  //     date = '';
  //   }
  //   switch (field) {
  //     case "dateOfBirth":
  //       setDob(date);
  //       break;
  //     case "dateOfDeath":
  //       setDod(date);
  //       break;
  //   }
  // };

  const yearb = new Date(dob).getFullYear();
  const [dayb, monthb, yearbb] = dod ? dod.split("-") : ["", "", ""];
  const formattedDate = `${monthb}-${dayb}-${yearbb}`;
  const yeard = new Date(formattedDate).getFullYear();
  useEffect(() => {
    // Date of Birth validation
    if (dob) {
      if (dob > today) {
        // setDob('');
      } else if (dod && dob > dod) {
        // setDob('');
      }
    }

    // Date of Death validation
    if (dod) {
      if (dod > today) {
        // setDod('');
      } else if (dob && dod < dob) {
        // setDob('');
      }
    }
  }, [dob, dod]);

  const handleInputDateChange = (date: any, field: any) => {
    if (date === undefined) {
      date = "";
    }
    switch (field) {
      case "dateOfBirth": console.log("bbb", date, "dod", dod, "dob", dob);

        if (getYearFromDate(date) > yeard) {
          getYearFromDate(date) != 0 && setDod("");
          setDob(date);
        } else {
          setDob(date);
        }
        if (!dod || (date <= dod && date <= today)) {
          // setDob(date);
          if (dod && dob > dod) {
            // setDod('');
          }
        } else {
          // setDob('');
        }
        break;

      case "dateOfDeath":
        const [day, month, year] = date ? date.split("-") : ["", "", ""];
        const formattedDated = `${month}-${day}-${year}`;
        if (yearb > new Date(formattedDated).getFullYear()) {
          getYearFromDate(date) != 0 && setDob("");
          setDod(convertDateFormat(date, "MM"));
        } else {
          setDod(convertDateFormat(date, "MM"));
        }
        if (!dob || (date <= today && date >= dob)) {
          // setDod(date);
          if (dob >= date) {
            // setDob('');
          }
        } else {
          // setDod('');
        }
        break;

      default:
        break;
    }
  };
  const handleUpdatePersonalInformation = () => {
    const updatedData = {
      salutation_id: salutation,
      name: fullName,
      language_id: language,
      lang_name: personTamilName,
      nickname: preferredName,
      briefAbout: briefAbout,
      quote: quote,
      gender_id: genders,
      dob: dob,
      dod: dod.split("-").reverse().join("-"),
    };
    if (uuid) {
      setClickStatusPersonalInfo(true);
      dispatch(
        updatePersonalInformation({
          active_partner: ACTIVE_PARTNER,
          active_service_provider: ACTIVE_SERVICE_PROVIDER,
          active_page: uuid as string,
          updatedData,
        })
      ).then((response: any) => {
        if (response?.payload?.success == true) {
          handleSave();
          setResponse(response);
          setSuccess(true);
          Notification.success({
            title: "Updated successfully",
            duration: 3000,
            content: undefined,
          });
          console.log("hjl", response);
          // loadData();
          // clearErr();
        } else {
          setClickStatusPersonalInfo(false);
          (response.payload.code != 422) &&
            Notification.error({
              title: "Update failed",
              duration: 3000,
              content: undefined,
            });
        }
        // setClickStatusPersonalInfo(false);
      });
    }
  };
  const handleUpdateLocations = () => {
    const updatedData1 = {
      death_location: deathPlace,
      // lived_locations: livedPlace,
      country_id: countryId,
      hometown_location: homeTown,
      birth_location: birthPlace,
    };
    if (uuid) {
      setClickStatusLocations(true);
      dispatch(
        updatePersonalLocations({
          active_partner: ACTIVE_PARTNER,
          active_service_provider: ACTIVE_SERVICE_PROVIDER,
          active_page: uuid as string,
          updatedData1,
        })
      ).then((response: any) => {
        // handleSave();
        if (response?.payload?.success == true) {
          setResponse(response);
          setSuccess(true);
          Notification.success({
            title: "Updated Successfully",
            duration: 3000,
            content: undefined,
          });
          // loadData();
          // clearErr();
        } else {
          (response.payload.code != 422) && Notification.error({
            title: "Update failed",
            duration: 3000,
            content: undefined,
          });
        }
        setClickStatusLocations(false);
      });
    }
  };
  const handleUpdateRelatedInformation = () => {
    const updatedData2 = {
      religion_id: religion,
      nature_of_death_id: reasonDeath,
    };
    if (uuid) {
      setClickStatusRelations(true);
      dispatch(
        updateRelatedInformation({
          active_partner: ACTIVE_PARTNER,
          active_service_provider: ACTIVE_SERVICE_PROVIDER,
          active_page: uuid as string,
          updatedData2,
        })
      ).then((response: any) => {
        if (response?.payload?.success == true) {
          setResponse(response);
          // handleSave();
          setSuccess(true);
          Notification.success({
            title: "Updated Successfully",
            duration: 3000,
            content: undefined,
          });
          console.log("hjl", response);
          // loadData();
          // clearErr();
        } else {
          (response.payload.code != 422) && Notification.error({
            title: "Update failed",
            duration: 3000,
            content: undefined,
          });
        }
        setClickStatusRelations(false);
      });
    }
  };
  const { error: relatedInformationapiError } = useAppSelector(
    relatedInformationSelector
  );

  const { error: personalLocationsapiError } = useAppSelector(
    personalLocationsSelector
  );
  const { error: personalInformationapiError } = useAppSelector(
    personalInformationSelector
  );
  // console.log("personalInredux>>>>>>", personalInformationapiError);

  const personalInformationErr = useApiErrorHandling(
    personalInformationapiError,
    response
  );
  const personalLocationsErr = useApiErrorHandling(
    personalLocationsapiError,
    response
  );
  const relatedInformationnErr = useApiErrorHandling(
    relatedInformationapiError,
    response
  );

  useEffect(() => {
    if (success) {
      // Reset the success state after a short delay
      const timer = setTimeout(() => {
        setSuccess(false);
      }, 3000);

      return () => {
        clearTimeout(timer);
      };
    }
  }, [success]);

  const handledeathPlace = (description: string) => {
    setDeathPlace(description);
  };

  const handleLivedPlaces = (description: string) => {
    // setLivedPlaces(description);
    setLivedPlaces((prevSelectedPlaces) => [
      ...prevSelectedPlaces,
      description,
    ]);
  };
  const handleCountryChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedValue = e.target.value;
    setCountryId(selectedValue);
  };
  const handleRemoveItem = (index: number) => {
    // Remove the selected place from the list of selected places
    setLivedPlaces((prevSelectedPlaces: any) => {
      const updatedSelectedPlaces = [...prevSelectedPlaces];
      updatedSelectedPlaces.splice(index, 1);
      return updatedSelectedPlaces;
    });
  };
  const handleNativePlace = (description: string) => {
    setNativePlace(description);
  };
  const handleHometown = (description: string) => {
    setHomeTown(description);
  };
  const handleBirthPlace = (description: string) => {
    setBirthPlace(description);
  };

  useEffect(() => {
    if (PageData.languages && PageData.languages.length > 0 && language) {
      const Lang = PageData.languages.find(
        (x: any) => x.id.toString() === language.toString()
      );
      if (Lang) {
        setProfileDisplayLanguage(Lang.name);
      }
    }
  }, [PageData.languages, language]);


  return (
    <>
      <div className="page-arco">
        {!PageData?.loading || clickstatusPersonalInfo ? (
          <PersonalInformationSkelton />
        ) : (
          <>
            <div className="mb-3 page-heading font-bold">
              {getLabel("personalInformation", initialLanguage)}
            </div>
            <div className="row m-0 align-items-center">
              <div className="col-md-3 col-12 p-0 pe-2 font-bold">
                {getLabel("personName", initialLanguage)}
              </div>
              <div className="col-md-2 col-4 p-0">
                <CustomSelect
                  data={PageData.salutations}
                  placeholderValue={"Select a Salutation"}
                  onChange={(value: any) => {
                    setSalutation(value);
                    setSalutationID(value);
                  }}
                  is_search={true}
                  defaultData={PageData.salutations}
                  defaultValue={salutationId}
                />
              </div>
              <div className="col-md-6 col-8 pe-0">
                <input
                  type="text"
                  className="form-control border border-2 system-control p-2 "
                  placeholder="Full Name in English"
                  value={fullName}
                  onChange={(e) => handleInputChange(e, "fullName")}
                />
              </div>
            </div>
            <div className=" page-heading font-bold"></div>
            <div className="row mb-3 m-0 align-items-center">
              <div className="col-md-3 col-12 p-0 pe-2"></div>
              <div className="col-md-2 col-4 p-0 "></div>
              <div className="col-md-6 col-8 pe-0   ">
                {personalInformationErr && (
                  <div className="text-danger font-normal">
                    {personalInformationErr.name}
                  </div>
                )}
              </div>
            </div>
            <div className="row m-0 align-items-center">
              <div className="col-md-3 col-12 p-0 pe-2 font-bold">
                {getLabel("profileDisplayLanguage", initialLanguage)}
              </div>
              <div className="col-md-8 col-12  p-0  ">
                <CustomSelect
                  data={PageData.languages}
                  placeholderValue={"Select a Language"}
                  onChange={(value: any) => {
                    setLanguage(value);
                    setPersonTamilName("")
                  }}
                  is_search={true}
                  defaultValue={language}
                />
              </div>
            </div>
            <div className="row m-0 mb-3">
              <div className="col-lg-3 col-12 d-none d-sm-flex col-md-3 "></div>
              <div className="col-lg-9 col-12 system-muted font-normal p-0 col-md-9 ">
                {getLabel("profileDisplayLanguageDescription", initialLanguage)}
              </div>
            </div>
            {supportedLanguages.includes(selectedLanguage) ? (
              <div className="row m-0 align-items-center mb-3">
                <div className="col-md-3 col-12 p-0 pe-2 font-bold">
                  <div className="">
                    {getLabel("personNameIn", initialLanguage)}{" "}
                    {profileDisplayLanguage}
                  </div>
                  <span className="system-muted font-normal ">
                    {getLabel("optional", initialLanguage)}
                  </span>
                </div>
                <div className="col-md-8 col-12  p-0">
                  <input
                    type="text"
                    className="form-control border border-2 system-control p-2"
                    placeholder={`${getLabel(
                      "fullNameIn",
                      initialLanguage
                    )} ${profileDisplayLanguage}`}
                    value={personTamilName}
                    onChange={(e) => handleInputChange(e, "personTamilName")}
                  />
                </div>
              </div>
            ) : (
              ""
            )}
            <div className="row m-0 mb-3 align-items-center">
              <div className="col-md-3 col-12 p-0 pe-2 font-bold">
                {getLabel("preferredName", initialLanguage)}
              </div>
              <div className="col-md-8 col-12 p-0">
                <input
                  type="text"
                  className="form-control border border-2 p-2 system-control "
                  value={preferredName}
                  onChange={(e) => handleInputChange(e, "preferredName")}
                />
              </div>
            </div>
            <div className="row m-0 align-items-center">
              <div className="col-md-3 col-12 p-0 pe-2 font-bold avoid-overflow-three-lines">
                {getLabel("briefAboutThe", initialLanguage)}{" "}
                {PageData?.full_name || "Person"}
              </div>
              <div className="col-md-8 col-12  p-0">
                <CustomTextBox
                  value={briefAbout}
                  onChange={(e) => handleInputChange(e, "briefAbout")}
                  maxLength={maxLength}
                  description={"briefAbout"}
                />
              </div>
            </div>
            <div className="row m-0 mb-3">
              <div className="col-lg-3 col-12 d-none d-sm-flex col-md-3"></div>
              <div className="col-lg-8 col-12 system-muted font-normal p-0 col-md-9">
                {getLabel("briefAboutDescriptionOne", initialLanguage)}{" "}
                {PageData?.full_name || "Person"}
                {getLabel("briefAboutDescriptionTwo", initialLanguage)}
              </div>
            </div>
            <div className="row m-0 mb-3 align-items-center">
              <div className="col-md-3 col-12 p-0 pe-2 font-bold">
                {getLabel("quote", initialLanguage)}
              </div>
              <div className="col-md-8 col-12  p-0">
                {" "}
                <CustomTextBox
                  value={quote}
                  onChange={(e) => handleInputChange(e, "quote")}
                  maxLength={maxLength}
                  description={"quote"}
                />
              </div>
            </div>
            <div className="row m-0 mb-3 align-items-center">
              <div className="col-md-3 col-12 p-0 pe-2 font-bold">
                {getLabel("gender", initialLanguage)}
              </div>
              <div className="col-lg-3 col-md-4 col-12  p-0">
                <CustomSelect
                  data={gendersData}
                  placeholderValue={"Select a gender"}
                  onChange={(value: any) => {
                    setGenders(value);
                  }}
                  is_search={false}
                  defaultValue={genders}
                />
              </div>
            </div>
            <div className="row m-0 align-items-center">
              <div className="col-md-3 col-12 p-0 pe-2 font-bold">
                {getLabel("dateOfDeath", initialLanguage)}
              </div>
              <div className="col-md-4 col-12  p-0 dateOfDeath">
                <DatePicker
                  className="form-control border border-2 system-control py-3"
                  value={convertDateFormat(dod, "MMM")}
                  format="DD-MMM-YYYY"
                  disabledDate={(current) => current.isAfter(today, "day")}
                  onChange={(date: string) =>
                    handleInputDateChange(date, "dateOfDeath")
                  }
                  getPopupContainer={(node) => {
                    const container = node.parentElement;
                    if (container) {
                      return container;
                    }
                    return document.body;
                  }}
                />
              </div>
            </div>
            <div className="row mb-3 m-0">
              <div className="col-md-3 col-12 d-none d-sm-flex"></div>
              <div className="col-md-9 col-12 system-muted font-normal p-0">
                {personalInformationErr && (
                  <div className="text-danger">
                    {personalInformationErr.dod}
                  </div>
                )}
              </div>
            </div>
            <div className="row m-0 align-items-center ">
              <div className="col-lg-3 col-md-3 col-sm-3 col-12 p-0 font-bold">
                {getLabel("dateOfBirth", initialLanguage)}
              </div>
              <div className="col-md-8 col-12 col-sm-12 p-0">
                <CustomDropDownDatepicker
                  onDateChange={(date: string) =>
                    handleInputDateChange(date, "dateOfBirth")
                  }
                  defaultDate={dob}
                  size="p-info"
                />
              </div>
            </div>
            <div className="row m-0">
              <div className="col-md-3 col-12 d-none d-sm-flex"></div>
              <div className="col-md-9 col-12 system-muted font-normal p-0">
                {personalInformationErr && (
                  <div className="text-danger">
                    {personalInformationErr.dob}
                  </div>
                )}
              </div>
            </div>
            <div className="row m-0 mb-3">
              <div className="col-lg-3 col-12 d-none d-sm-flex bg col-md-3"></div>
              <div className="col-lg-9 col-12 system-muted font-normal p-0 col-md-9">
                {getLabel("DOBDescription", initialLanguage)}
              </div>
            </div>
            <div className="row m-0 mb-3">
              <div className="col-lg-3  col-12 d-none d-sm-flex col-md-3"></div>
              <div className="col-lg-9  col-12 p-0 col-md-9">
                <button
                  type="button"
                  className="btn btn-system-primary my-1"
                  onClick={handleUpdatePersonalInformation}
                >
                  {getLabel("save", initialLanguage)}
                </button>
              </div>
            </div>
          </>
        )}
        <hr className="mt-5 hr-width-form3" />
        {!PageData?.loading || clickstatusLocations ? (
          <LocationsSkelton />
        ) : (
          <>
            <div className="m-0 page-heading font-bold">
              {getLabel("locations", initialLanguage)}
            </div>
            <p className="system-muted ">
              {getLabel("locationDescriptionOne", initialLanguage)}{" "}
              {PageData?.full_name || "Person"}.{" "}
              {getLabel("locationDescriptionTwo", initialLanguage)}
            </p>
            <div className="row m-0 mb-3 align-items-center ">
              <div className="col-md-3 col-12 p-0 pe-2 font-bold">
                {getLabel("deathPlace", initialLanguage)}
              </div>
              <div className="col-md-8 col-12  p-0">
                <PlaceAutocomplete
                  className="form-control border border-2 p-2 system-control"
                  placeholder={`${getLabel(
                    "deathPlacePlaceholder",
                    initialLanguage
                  )}`}
                  onSuggestionClick={handledeathPlace}
                  dvalue={deathPlace}
                />
                {/* <input
                  type="text"
                  className="form-control border border-2 p-2 system-control "
                  placeholder="Search city of death"
                  value={deathPlace}
                  onChange={(e) => handleInputChange(e, "deathPlace")}
                /> */}
              </div>
            </div>
            <div className="row m-0 mb-3 align-items-center">
              <div className="col-md-3 col-12 p-0 pe-2 font-bold">
                {getLabel("livedPlaces", initialLanguage)}
              </div>
              <div className="col-md-8 col-12 p-0 position-relative">
                <PlaceMulticomplete
                  className="form-control border border-2 p-2 system-control "
                  placeholder={`${getLabel(
                    "addMultipleCities",
                    initialLanguage
                  )}`}
                  onSuggestionClick={handleLivedPlaces}
                  dvalue={livedPlaces}
                  onRemoveItem={handleRemoveItem}
                />
                {/* <input
                  type="text"
                  className="form-control border border-2 p-2 system-control "
                  placeholder="Add multiple cities"
                /> */}
              </div>
            </div>
            <div className="row m-0 mb-3 align-items-center">
              <div className="col-md-3 col-12 p-0 pe-2 font-bold">
                {getLabel("countryOfOrigin", initialLanguage)}
              </div>
              <div className="col-md-8 col-12  p-0">
                {/* <select
                  className="form-select border border-2 bg-white fw-normal p-2 system-control"
                  id="dropdownMenuLink"
                  value={countryId}
                  onChange={handleCountryChange}
                >
                  {Array.isArray(showCountriesData) &&
                    showCountriesData.map((option: any) => (
                      <option key={option.id} value={option.id}>
                        {option.name}
                      </option>
                    ))}
                </select> */}
                {/* <input
                  type="text"
                  className="form-control border border-2 p-2 system-control "
                  placeholder="Select David’s origin country"
                /> */}

                <CustomSelect
                  data={showCountriesData}
                  placeholderValue={"Select a Country"}
                  onChange={(value: any) => {
                    setCountryId(value);
                  }}
                  is_search={true}
                  defaultValue={countryId}
                />
              </div>
            </div>
            <div className="row mb-3 m-0 align-items-center">
              <div className="col-md-3 col-12 p-0 pe-2 font-bold">
                {getLabel("hometownNativePlace", initialLanguage)}
              </div>
              <div className="col-md-8 col-12  p-0">
                <PlaceAutocomplete
                  className="form-control border border-2 p-2 system-control"
                  placeholder={`${getLabel("add", initialLanguage)} ${PageData?.full_name || "Person"
                    }${getLabel(
                      "hometownNativePlacePlaceholder",
                      initialLanguage
                    )}`}
                  onSuggestionClick={handleHometown}
                  dvalue={homeTown}
                />
                {/* <input
                  type="text"
                  className="form-control border border-2 p-2 system-control "
                  placeholder="Add David’s hometown or native place"
                  value={homeTown}
                  onChange={(e) => handleInputChange(e, "homeTown")}
                /> */}
              </div>
            </div>
            <div className="row m-0 mb-3 align-items-center">
              <div className="col-md-3 col-12 p-0 pe-2 font-bold">
                {getLabel("birthPlace", initialLanguage)}
              </div>
              <div className="col-md-8 col-12  p-0">
                <PlaceAutocomplete
                  className="form-control border border-2 p-2 system-control"
                  placeholder={`${getLabel("add", initialLanguage)} ${PageData?.full_name || "Person"
                    }${getLabel("birthPlacePlaceholder", initialLanguage)}`}
                  onSuggestionClick={handleBirthPlace}
                  dvalue={birthPlace}
                />
                {/* <input
                  type="text"
                  className="form-control border border-2 p-2 system-control "
                  placeholder="Add David’s birth place"
                  value={birthPlace}
                  onChange={(e) => handleInputChange(e, "birthPlace")}
                /> */}
              </div>
            </div>
            <div className="row m-0 mb-3">
              <div className="col-lg-3  col-12 d-none d-sm-flex col-md-3"></div>
              <div className="col-lg-8  col-12 p-0 col-md-9">
                <button
                  type="button"
                  className="btn btn-system-primary my-1"
                  onClick={handleUpdateLocations}
                >
                  {getLabel("save", initialLanguage)}
                </button>
              </div>
            </div>
          </>
        )}

        <hr className="mt-5  hr-width-form3" />
        {!PageData?.loading || clickstatusRelations ? (
          <LocationsSkelton />
        ) : (
          <>
            <div className="m-0 page-heading font-bold">
              {getLabel("relatedInformation", initialLanguage)}
            </div>
            <p className="system-muted">
              {getLabel("relatedInformationDescription", initialLanguage)}
            </p>
            <div className="row m-0 mb-3 align-items-center">
              <div className="col-md-3 col-12 p-0 pe-2 font-bold">
                {getLabel("religion", initialLanguage)}
              </div>
              <div className="col-md-8 col-12  p-0">
                {/* <select
                  className="form-select border border-2 bg-white p-2 system-control"
                  id="dropdownMenuLink"
                  value={religion}
                  onChange={handleReligionChange}
                >
                  {Array.isArray(religionData) &&
                    religionData.map((option: any) => (
                      <option key={option.id} value={option.id}>
                        {option.name}
                      </option>
                    ))}
                </select> */}
                <CustomSelect
                  data={religionData}
                  placeholderValue={`${getLabel("select", initialLanguage)} ${PageData?.full_name || "Person"
                    }${getLabel("religions", initialLanguage)}`}
                  value={schoolAndColleges}
                  onChange={(value: any) => {
                    setReligion(value);
                  }}
                  is_search={false}
                  defaultValue={religion}
                />
              </div>
            </div>

            <div className="row m-0 mb-3 align-items-center">
              <div className="col-md-3 col-12 p-0 pe-2 font-bold">
                {getLabel("reasonsForDeath", initialLanguage)}
              </div>
              <div className="col-md-8 col-12  p-0">
                {/* <select
                  className="form-select border border-2 bg-white p-2 system-control"
                  id="dropdownMenuLink"
                  value={reasonDeath}
                  onChange={handleReasonDeathChange}
                >
                  {Array.isArray(natureOfDeathData) &&
                    natureOfDeathData.map((option: any) => (
                      <option key={option.id} value={option.id}>
                        {option.name}
                      </option>
                    ))}
                </select> */}
                <CustomSelect
                  data={natureOfDeathData}
                  placeholderValue={`${getLabel("select", initialLanguage)} ${PageData?.full_name || "Person"
                    }${getLabel("selectReasonsForDeaths", initialLanguage)}`}
                  onChange={(value: any) => {
                    setReasonDeath(value);
                  }}
                  is_search={true}
                  defaultValue={reasonDeath}
                />
              </div>
            </div>
            <div className="row m-0 mb-3 align-items-center">
              <div className="col-md-3 col-12 p-0 pe-2 font-bold">
                {getLabel("schoolAndColleges", initialLanguage)}
              </div>
              <div className="col-md-8 col-12  p-0">
                <input
                  type="text"
                  className="form-control border border-2 p-2 system-control "
                  // placeholder={`${getLabel("searchAndSelect",initialLanguage)}`}
                  placeholder={`${getLabel("add", initialLanguage)} ${PageData?.full_name || "Person"
                    } ${getLabel(
                      "schoolAndCollegesPlaceholder",
                      initialLanguage
                    )}`}
                  value={schoolAndColleges}
                  onChange={(e) => handleInputChange(e, "schoolAndColleges")}
                />
              </div>
            </div>
            <div className="row m-0 mb-3 align-items-center">
              <div className="col-md-3 col-12 p-0 pe-2 font-bold">
                {getLabel("hashTag", initialLanguage)}
              </div>
              <div className="col-md-8 col-12  p-0">
                <input
                  type="text"
                  className="form-control border border-2 p-2 system-control "
                  placeholder={`${getLabel(
                    "hashTagPlaceholder",
                    initialLanguage
                  )}`}
                />
              </div>
            </div>
            <div className="row mb-3 m-0">
              <div className="col-lg-3  col-12 d-none d-sm-flex col-md-3"></div>
              <div className="col-lg-8  col-12 p-0 col-md-9">
                <button
                  type="button"
                  className="btn btn-system-primary my-1"
                  onClick={handleUpdateRelatedInformation}
                >
                  {getLabel("save", initialLanguage)}
                </button>
              </div>
            </div>
          </>
        )}
      </div>
    </>
  );
};
export async function getServerSideProps({ query }: any) {
  const idQuery = query || "";
  return {
    props: {
      idQuery,
    },
  };
}
export default PersonalInformation;
