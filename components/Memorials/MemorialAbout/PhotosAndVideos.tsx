import Image from "next/image";
import React, { useContext, useEffect, useState } from "react";

import profile from "../../../public/profile.svg";
import not from "../../../public/not.png";
import vediothumbnail from "../../../public/vediothumbnail.svg";
import closebtn from "../../../public/closebtn.svg";
import edit from "../../../public/edit.svg";
import deletedimg from "../../../public/deletedimg.svg";
import vedioicon from "../../../public/vedioicon.svg";
import draggable from "../../../public/draggable.svg";
import uploadphotos from "../../../public/uploadphotos.svg";
import noticeimage1 from "../../../public/noticeimage1.svg";
import { ImageNewUploder } from "@/components/ImageUploder/imageNewUploder";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import router from "next/router";
import { memorialPageSelector } from "@/redux/memorial-pages";
import { Notification, Popover } from "@arco-design/web-react";

import { Button, Modal, OverlayTrigger } from "react-bootstrap";
import { VideoUploader } from "@/components/ImageUploder/videoNewUploder";
import BG from "../../../public/backImage.svg";
import {
  deleteMedia,
  getDeletedMedia,
  getMedia,
  mediaSelector,
  mediaUpload,
  postRestoreMedia,
  updateCaptions,
} from "@/redux/media";
import { patchNotice } from "@/redux/notices";
import FullScreenModal from "@/components/Elements/FullScreenModal";
import { PaginationComponent } from "@/components/Elements/Pagination";
import { integer } from "aws-sdk/clients/cloudfront";
import PhotosAndVideosSkelton from "@/components/Skelton/PhotosVideosSkelton";
import ShimmerEffectText from "@/components/Skelton/ShimmerEffectText";
import { hashPermission } from "@/services/HasPermission";
import AuthPermission from "@/helpers/auth/AuthPermission";
import { getLabel } from "@/utils/lang-manager";
import MainPagination from "@/components/Paginations";
import ImageNewUploader from "@/components/ImageNewUploader";
import Pintura from "@/components/Pintura";
import PinturaImageUploader from "@/components/Pintura";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";
import MemorialPhotoSkelton from "@/components/Skelton/MemorialPhotoSkelton";
const PhotosAndVideos = () => {
  const dispatch = useAppDispatch();
  const { handleSave, PageData } = useContext(PageDataContext);
  const initialLanguage = PageData?.initialLanguage;
  const [clickstatusProfilePhotos, setClickStatusProfilePhotos] =
    useState(false);
  const [clickstatusUploads, setClickStatusUploads] = useState(false);
  const [clickstatusNoticePhotos, setClickStatusNoticePhotos] = useState(false);
  const [clickstatusDeletedPhotos, setClickStatusDeletedPhotos] =
    useState(false);
  const [getStatusMedia, setGetStatusMedia] = useState(true);
  const momorialId = router.query?.id;
  const [popoverShow, setPopoverShow] = useState(false);
  const [deleteSkeleton, setDeleteSkeleton] = useState(false);
  const [crossBtnEnable, setCrossBtnEnable] = useState(false);
  const [crossBtnEnableId, setCrossBtnEnableId] = useState("");
  console.log("ggg", popoverShow);

  const upload = (url: string, mediaTypeId: any) => {
    const uploadImageData = {
      url: url,
      media_type_id: mediaTypeId,
    };
    dispatch(
      mediaUpload({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_notice: momorialId as unknown as string,
        createData: uploadImageData,
      })
    ).then(async (response: any) => {
      if (response.payload.success == true) {
        setGetStatusMedia(true);
        if (mediaTypeId == 1) {
          Notification.success({
            title: response?.payload?.msg,
            duration: 3000,
            content: undefined,
          });
        }
        await getAllMedia(1);
        // getDeleted(1);
      } else {
        Notification.error({
          title: response?.payload?.error,
          duration: 3000,
          content: undefined,
        });
      }
    });
  };
  const deleteFun = (uuid: String) => {
    setDeleteBtnDisable(true);
    dispatch(
      deleteMedia({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_notice: momorialId as unknown as string,
        uuid: uuid as unknown as string,
      })
    ).then(async (response: any) => {
      if (response.payload.success == true) {
        setDeleteBtnDisable(false);

        Notification.success({
          title: response?.payload?.msg,
          duration: 3000,
          content: undefined,
        });
        setPopoverShow(false);
        getDeleted(1);
        setGetStatusMedia(true);
        getAllMedia(1);
      } else {
        setDeleteBtnDisable(false);
        Notification.error({
          title: response?.payload?.error,
          duration: 3000,
          content: undefined,
        });
      }
    });
  };
  const makeItDefault = (id: string) => {
    // setPopoverShow(false);
    const updatedData = {
      photo_url: id,
    };

    dispatch(
      patchNotice({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        updatedData,
        active_id: momorialId as unknown as string,
      })
    ).then(async (response: any) => {
      if (response.payload.success == true) {
        handleSave();
        setGetStatusMedia(true);
        // 5 seconds delay
        setTimeout(() => {
          setGetStatusMedia(false);
        }, 500);
        Notification.success({
          title: response?.payload?.msg,
          duration: 3000,
          content: undefined,
        });
      } else {
        Notification.error({
          title: response?.payload?.error,
          duration: 3000,
          content: undefined,
        });
      }
    });
  };

  const handleUploadImage = (file: any) => {
    console.log("file---->", file);
    upload(file, 1);
    setGetStatusMedia(true);
  };
  const handleUploadVideo = (file: any) => {
    upload(file, 2);
    setGetStatusMedia(true);
  };
  const [deleteBtnDisable, setDeleteBtnDisable] = useState(false);
  const getDeleted = (page: integer) => {
    dispatch(
      getDeletedMedia({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_notice: momorialId as unknown as string,
        page: page,
      })
    ).then((response: any) => {
      setDeleteSkeleton(false);
    });
  };
  const getAllMedia = (profileMediapage: integer) => {
    // setGetStatusMedia(true);
    dispatch(
      getMedia({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_notice: momorialId as unknown as string,
        page: profileMediapage,
      })
    ).then((response: any) => {
      setGetStatusMedia(false);
    });
    // setGetStatusMedia(false);
  };
  useEffect(() => {
    getAllMedia(
      localStorage.getItem("profile_media_pagination")
        ? parseInt(
            localStorage.getItem("profile_media_pagination") as string,
            10
          )
        : 1
    );
    getDeleted(
      localStorage.getItem("deleted_pagination")
        ? parseInt(localStorage.getItem("deleted_pagination") as string, 10)
        : 1
    );
  }, [momorialId]);
  const { deleteMedia: showDeletedData } = useAppSelector(mediaSelector);
  const { getMediaData: showMediaData } = useAppSelector(mediaSelector);
  const restore = (imageId: any) => {
    dispatch(
      postRestoreMedia({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_notice: momorialId as unknown as string,
        image_id: imageId,
      })
    ).then((response: any) => {
      if (response.payload.success == true) {
        console.log("111", response);
        Notification.success({
          title: response?.payload?.msg,
          duration: 3000,
          content: undefined,
        });
        getDeleted(1);
        getAllMedia(
          localStorage.getItem("profile_media_pagination")
            ? parseInt(
                localStorage.getItem("profile_media_pagination") as string,
                10
              )
            : 1
        );
      } else {
        Notification.error({
          title: response?.payload?.error,
          duration: 3000,
          content: undefined,
        });
      }
    });
  };
  const [caption, setCaption] = useState("");
  const updateCaption = (imageId: any) => {
    dispatch(
      updateCaptions({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_notice: momorialId as unknown as string,
        uuid: imageId,
        updateData: { caption: caption },
      })
    ).then((response: any) => {
      getDeleted(1);
      setGetStatusMedia(true);
      getAllMedia(1);
      if (response.payload.success == true) {
        setCaption("");
        setCrossBtnEnable(false);
        Notification.success({
          title: response?.payload?.msg,
          duration: 3000,
          content: undefined,
        });
      } else {
        setCrossBtnEnable(false);
        Notification.error({
          title: "The caption must be a string.",
          duration: 3000,
          content: undefined,
        });
      }
    });
  };
  const editImage = (imageId: any, url: string) => {
    dispatch(
      updateCaptions({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_notice: momorialId as unknown as string,
        uuid: imageId,
        updateData: { url: url },
      })
    ).then((response: any) => {
      if (response.payload.success == true) {
        Notification.success({
          title: response?.payload?.msg,
          duration: 3000,
          content: undefined,
        });
        setGetStatusMedia(true);
        setPopoverShow(false);
        getDeleted(1);
        getAllMedia(1);
      } else {
        Notification.error({
          title: response?.payload?.error,
          duration: 3000,
          content: undefined,
        });
      }
    });
  };
  const editUploadImage = (uuid: string, file: any) => {
    editImage(uuid, file);
  };
  const makeitUploadImage = (file: any) => {
    makeItDefault(file);
  };
  const [showProfileShow, setshowProfileShow] = useState(false);
  const [showProfileUrl, setshowProfileUrl] = useState("");

  const [showuploadVideoShow, setshowuploadVideoShow] = useState(false);
  const [showuploadVideoShowUrl, setshowuploadVideoShowUrl] = useState("");

  const [showuploadImageShow, setshowuploadImageShow] = useState(false);
  const [showuploadImageShowUrl, setshowuploadImageShowUrl] = useState("");
  // pagination start
  const [page, setPage] = useState(1);
  const [profileMediapage, setprofileMediapage] = useState(1);
  const handlePagination = (page: number) => {
    getAllMedia(page);
    setDeleteSkeleton(true);
    setPage(page);
    getDeleted(page);
    localStorage.setItem("deleted_pagination", page.toString());
  };
  const handleProfileMediaPagination = (funpage: number) => {
    setprofileMediapage(profileMediapage);
    getAllMedia(funpage);
    localStorage.setItem("profile_media_pagination", funpage.toString());
  };
  const [delay, setDelay] = useState(false);
  useEffect(() => {
    const timeout = setTimeout(() => {
      !getStatusMedia && setDelay(true);
    }, 3000);
    return () => clearTimeout(timeout);
  }, []);
  const initialShowProfileStates = Array.isArray(showMediaData?.data)
    ? showMediaData?.data.map(() => false) // Initialize an array of false values
    : [];

  const [showProfileStates, setShowProfileStates] = useState<boolean[]>(
    initialShowProfileStates
  );
  useEffect(() => {
    // Load images when showMediaData changes
    const loadImage = async (index: any) => {
      if (!showProfileStates[index]) {
        const updatedShowProfileStates = [...showProfileStates];
        updatedShowProfileStates[index] = true;
        setShowProfileStates(updatedShowProfileStates);
      }
    };

    if (Array.isArray(showMediaData?.data)) {
      showMediaData.data.forEach((_: any, index: any) => {
        loadImage(index);
      });
    }
  }, [showMediaData, showProfileStates]);
  useEffect(() => {
    // Load images when showMediaData changes
    const loadImage = async (index: any) => {
      if (!showProfileStates[index]) {
        const updatedShowProfileStates = [...showProfileStates];
        updatedShowProfileStates[index] = true;
        setShowProfileStates(updatedShowProfileStates);
      }
    };
    if (Array.isArray(showMediaData?.data)) {
      showMediaData.data.forEach((_: any, index: any) => {
        loadImage(index);
      });
    }
  }, [showProfileStates]);

  const handleImageLoad = (index: number) => {
    const updatedShowProfileStates = [...showProfileStates];
    updatedShowProfileStates[index] = true;
    setShowProfileStates(updatedShowProfileStates);
  };

  const renderShimmerRows = () => {
    const rows = [];
    for (let i = 0; i < 15; i++) {
      rows.push(
        <div className="d-flex gap-2 m-2" key={i}>
          <ShimmerEffectText width="50px" height="50px" />
          <ShimmerEffectText width="80px" height="20px" />
        </div>
      );
    }
    return rows;
  };

  return (
    <div>
      {getStatusMedia ? (
        <MemorialPhotoSkelton />
      ) : (
        <>
          <div className="mb-4 m-0 page-heading font-bold">
            {getLabel("photosVideos", initialLanguage)}
          </div>
          <div className="d-flex align-items-center  gap-2 gap-sm-3 flex-wrap mb-3">
            <div className=" font-bold m-0 ">
              {getLabel("profilePhotos", initialLanguage)}
            </div>
            {/* <AuthPermission permissions={["READ"]}> */}
            <ImageNewUploder
              name={`${getLabel("upload", initialLanguage)}`}
              headerText={`${getLabel("uploadProfile", initialLanguage)}`}
              handleUploadImage={handleUploadImage}
            />

            {/* </AuthPermission> */}

            {/* <ImageNewUploader
              name={`${getLabel("upload",initialLanguage)}`}
              headerText={`${getLabel("uploadProfile",initialLanguage)}`}
              handleUploadImage={handleUploadImage}
            /> */}
          </div>

          {/* --------------profile photos-------------- */}
          <div className="d-flex flex-wrap gap-2 gap-sm-3 mb-5 justify-content-start ">
            {showMediaData?.data?.map((item: any, index: any) => {
              return (
                <>
                  {item.media_type.id == 1 && (
                    <div
                      className={
                        showProfileStates[index]
                          ? "position-relative profile-photo-container "
                          : "position-relative profile-photo-container shimmer-effect"
                      }
                      style={{ borderRadius: "12px" }}
                    >
                      <Image
                        src={showProfileStates[index] ? item.url : BG}
                        alt="memorialprofile"
                        className={"pointer image"}
                        width={133}
                        height={133}
                        style={{ borderRadius: "12px", border: "2px" }}
                        onClick={() => {
                          setshowProfileShow(true);
                          setshowProfileUrl(item.url);
                        }}
                        onLoad={() => {
                          handleImageLoad(index);
                        }}
                      />

                      <Popover
                        className={
                          popoverShow ? "d-none " : "d-block custom-dropdown"
                        }
                        trigger="click"
                        position="right"
                        content={
                          <div className="popover-content">
                            <div
                            // onClick={() => {
                            //   setPopoverShow(true);
                            // }}
                            >
                              <ImageNewUploder
                                name="Make it default"
                                headerText="Update profile photo"
                                textOnly={true}
                                edit={true}
                                handleUploadImage={(file) => {
                                  makeitUploadImage(file);
                                }}
                                imageUrl={item.url}
                                showPopupFun={(val) => {
                                  console.log("val", val);
                                  setPopoverShow(val);
                                }}
                              />
                            </div>
                            <div>
                              <ImageNewUploder
                                name="Edit"
                                headerText="Edit Your Photo"
                                textOnly={true}
                                edit={true}
                                handleUploadImage={(file) =>
                                  editUploadImage(item.uuid, file)
                                }
                                imageUrl={item.url}
                                showPopupFun={(val) => {
                                  setPopoverShow(val);
                                }}
                              />
                            </div>
                            <li
                              className="pointer p-0 m-0 px-1 px-sm-4"
                              onClick={() => {
                                deleteBtnDisable ? null : deleteFun(item.uuid);
                                setPopoverShow(false);
                              }}
                            >
                              {getLabel("delete", initialLanguage)}
                            </li>
                          </div>
                        }
                      >
                        <Image
                          src={edit}
                          alt="memorialprofile"
                          className="img-fluid position-absolute top-0 end-0 m-2 pointer"
                          onClick={() => setPopoverShow(false)}
                        />
                      </Popover>
                    </div>
                  )}
                </>
              );
            })}
          </div>
          {/* paginations */}
          {/* <div style={{ margin: 20 }}>
            {showMediaData?.total > 15 && (
              // <PaginationComponent
              //   page={
              //     localStorage.getItem("profile_media_pagination")
              //       ? parseInt(
              //           localStorage.getItem(
              //             "profile_media_pagination"
              //           ) as string,
              //           10
              //         )
              //       : 1
              //   }
              //   totalPages={Math.ceil(showMediaData.total / 15)}
              //   handlePagination={handleProfileMediaPagination}
              // />

              <MainPagination
                page={
                  localStorage.getItem("profile_media_pagination")
                    ? parseInt(
                        localStorage.getItem(
                          "profile_media_pagination"
                        ) as string,
                        10
                      )
                    : 1
                }
                totalPages={showMediaData.total}
                handlePagination={handlePagination}
              />
            )}
          </div> */}
          {/* ***********************profile image model start*/}
          <FullScreenModal
            show={showProfileShow}
            onClose={() => {
              setshowProfileShow(false);
              setPopoverShow(false);
            }}
            size="lg"
            title=""
          >
            {/* <div className="d-flex justify-content-center bg-danger">
          <Image
            src={showProfileUrl}
            alt="memorialprofile"
            className="pointer"
            width={1500}
            height={600}
            style={{ borderRadius: "5px" }}
          />
        </div> */}
            <div className="d-flex justify-content-center">
              <div className="fullmodal-image-container">
                <Image
                  src={showProfileUrl == null ? not : showProfileUrl}
                  alt="memorialprofile"
                  className="pointer image-contain"
                  width={750}
                  height={600}
                />
              </div>
            </div>
          </FullScreenModal>
          {/* ***********************profile image model end */}
          <div className=" d-flex flex-wrap gap-2 gap-sm-3 align-items-center mb-4">
            <div className=" m-0 font-bold ">
              {getLabel("uploads", initialLanguage)}
            </div>
            <div className="d-flex gap-1 gap-sm-2  ">
              {/* <AuthPermission permissions={["READ"]}> */}
              <ImageNewUploder
                name={`+ ${getLabel("uploadPhoto", initialLanguage)}`}
                headerText={`${getLabel("uploadYourPhoto", initialLanguage)}`}
                handleUploadImage={handleUploadImage}
              />

              {/* <ImageNewUploader
                name={`${getLabel("uploadPhoto",initialLanguage)}`}
                headerText={`${getLabel("uploadYourPhoto",initialLanguage)}`}
                handleUploadImage={handleUploadImage}
              /> */}
              {/* </AuthPermission> */}
              <VideoUploader
                name={`+ ${getLabel("uploadVideo", initialLanguage)}`}
                handleUploadVideo={handleUploadVideo}
              />
            </div>
          </div>
          {/* --------------upload photos-------------- */}
          <div className="d-flex flex-wrap gap-2 gap-sm-3 mb-4 justify-content-start">
            {showMediaData?.data?.map((item: any, index: any) => {
              return (
                <>
                  {item.media_type.id === 2 ? (
                    <div className="flex-column">
                      <div
                        className="position-relative upload-image-container system-filter-secondary rounded"
                        style={{ borderRadius: "12px" }}
                      >
                        <Image
                          src={draggable}
                          alt="memorialprofile"
                          className="img-fluid position-absolute top-0 start-0  m-2 move"
                        />
                        <Popover
                          className={
                            popoverShow ? "d-none" : "d-block custom-dropdown"
                          }
                          trigger="click"
                          position="right"
                          content={
                            <div className="popover-content">
                              <li
                                className="pointer p-0 m-0 px-1 px-sm-4"
                                onClick={() => {
                                  deleteFun(item.uuid);
                                  setPopoverShow(false);
                                }}
                              >
                                {getLabel("delete", initialLanguage)}
                              </li>
                            </div>
                          }
                        >
                          <Image
                            src={
                              crossBtnEnableId == item.uuid
                                ? crossBtnEnable
                                  ? closebtn
                                  : edit
                                : edit
                            }
                            alt="memorialprofile"
                            className="img-fluid position-absolute top-0 end-0 m-1 pointer"
                            onClick={() => setPopoverShow(false)}
                          />
                        </Popover>
                        <Image
                          src={vedioicon}
                          alt="memorialprofile"
                          className="img-fluid vedio-icon-position pointer"
                          onClick={() => {
                            setshowuploadVideoShowUrl(item.url),
                              setshowuploadVideoShow(true);
                          }}
                        />
                        <object
                          data={item.url}
                          width="132px"
                          height="110px"
                          className="embed-responsive-item image rounded pointer"
                          style={{ borderRadius: "12px" }}
                        ></object>{" "}
                        <div className="">
                          <input
                            type="text"
                            className="border-0 text-truncate mx-upload customPenCursor"
                            placeholder={
                              item.caption ? item.caption : "Add caption"
                            }
                            onChange={(e) => setCaption(e.target.value)}
                            onClick={() => setCaption(item.caption)}
                            onBlur={() => updateCaption(item.uuid)}
                            defaultValue={item.caption}
                          />
                        </div>
                      </div>
                      {/* ***********************uploads Videos model start*/}
                      <FullScreenModal
                        show={showuploadVideoShow}
                        onClose={() => {
                          setshowuploadVideoShow(false);
                        }}
                        size="xl"
                        title="Show Video"
                      >
                        <div className="d-flex justify-content-center">
                          <iframe
                            className="embed-responsive embed-responsive-16by9"
                            src={showuploadVideoShowUrl}
                            width={1500}
                            height={600}
                          ></iframe>
                        </div>
                      </FullScreenModal>
                      {/* ***********************uploads Videos model end */}
                    </div>
                  ) : (
                    <div className="d-flex flex-column">
                      <div className="position-relative upload-image-container system-filter-secondary rounded mb-1">
                        <Image
                          src={draggable}
                          alt="memorialprofile"
                          className="img-fluid position-absolute top-0 start-0  m-2 move"
                        />
                        <Popover
                          trigger="click"
                          position="right"
                          className={
                            popoverShow ? "d-none" : "d-block custom-dropdown"
                          }
                          content={
                            <div className="popover-content">
                              {/* <li
                                className="pointer p-0 m-0 px-1 px-sm-4"
                                onClick={() => {
                                  makeItDefault(item.url);
                                  setPopoverShow(true);
                                }}
                                style={{ paddingRight: 20 }}
                              >
                                {getLabel("makeItDefault",initialLanguage)}
                              </li> */}
                              <div>
                                <ImageNewUploder
                                  name="Set as Profile photo"
                                  headerText="Update profile photo"
                                  textOnly={true}
                                  edit={true}
                                  handleUploadImage={(file) => {
                                    makeitUploadImage(file);
                                  }}
                                  imageUrl={item.url}
                                  showPopupFun={(val) => {
                                    setPopoverShow(val);
                                  }}
                                />
                              </div>
                              <div>
                                <ImageNewUploder
                                  name="Edit"
                                  headerText="Edit Your Photo"
                                  textOnly={true}
                                  edit={true}
                                  handleUploadImage={(file) =>
                                    editUploadImage(item.uuid, file)
                                  }
                                  imageUrl={item.url}
                                  showPopupFun={(val) => {
                                    setPopoverShow(val);
                                  }}
                                />
                              </div>
                              <li
                                className="pointer p-0 m-0 px-1 px-sm-4"
                                onClick={() => {
                                  deleteBtnDisable
                                    ? null
                                    : deleteFun(item.uuid);
                                  setPopoverShow(false);
                                }}
                              >
                                {/* {getLabel("delete", initialLanguage)} */}
                                Delete photo
                              </li>
                            </div>
                          }
                        >
                          <Image
                            src={
                              crossBtnEnableId == item.uuid
                                ? crossBtnEnable
                                  ? closebtn
                                  : edit
                                : edit
                            }
                            alt="memorialprofile"
                            className="img-fluid position-absolute top-0 end-0 m-1 pointer"
                            onClick={() => {
                              // setPopoverShow(false);
                              crossBtnEnable
                                ? setPopoverShow(true)
                                : setPopoverShow(false);
                            }}
                          />
                        </Popover>
                        {/* <Image
                          src={item.url}
                          alt="memorialprofile"
                          className="image rounded"
                          width={200}
                          height={100}
                          onClick={() => {
                            setshowuploadImageShowUrl(item.url),
                              setshowuploadImageShow(true);
                          }}
                        /> */}
                        <Image
                          src={showProfileStates[index] ? item.url : BG}
                          alt="memorialprofile"
                          className={"image rounded"}
                          width={200}
                          height={100}
                          onClick={() => {
                            setshowuploadImageShowUrl(item.url),
                              setshowuploadImageShow(true);
                          }}
                          onLoad={() => {
                            handleImageLoad(index);
                          }}
                        />
                      </div>
                      {/* <div
                        style={{
                          cursor: `url('https://img.icons8.com/material-rounded/24/pencil--v2.png'), auto`,
                        }}
                      >
                        Content here
                      </div> */}

                      <div className="">
                        <input
                          type="text"
                          className="border-0 text-truncate mx-upload customPenCursor"
                          placeholder={
                            item.caption ? item.caption : "Add caption"
                          }
                          onChange={(e) => {
                            setCaption(e.target.value);
                          }}
                          onClick={(e) => {
                            setCrossBtnEnableId(item.uuid);
                            setCrossBtnEnable(true);
                          }}
                          onBlur={() => updateCaption(item.uuid)}
                          defaultValue={item.caption}
                        />
                      </div>
                      {/* ***********************uploads Photo model start*/}

                      {/* ***********************uploads Photo model end */}
                    </div>
                  )}
                </>
              );
            })}
            {/* {showDeletedData?.data?.length &&
          showDeletedData?.data?.map((item: any) => {
            return (
              <div>
                <div className="d-flex flex-column ">
                  <div className="position-relative upload-image-container system-filter-secondary rounded">
                    <Image
                      src={draggable}
                      alt="memorialprofile"
                      className="img-fluid position-absolute top-0 start-0  m-2 move"
                    />
                    <Image
                      src={edit}
                      alt="memorialprofile"
                      className="img-fluid position-absolute top-0 end-0 m-1 pointer"
                    />
                    <Image
                      src={item.url}
                      alt="memorialprofile"
                      className="image rounded"
                      width={200}
                      height={100}
                    />
                  </div>
                  <p className="font-normal font-80 pt-1 caption text-truncate">
                    Display caption here for reference
                  </p>
                </div>
              </div>
            );
          })} */}
          </div>
          <FullScreenModal
            show={showuploadImageShow}
            onClose={() => {
              setshowuploadImageShow(false);
              setPopoverShow(false);
            }}
            size="lg"
            title=""
          >
            <div className="d-flex justify-content-center">
              <div className="fullmodal-image-container">
                <Image
                  src={showuploadImageShowUrl}
                  alt="memorialprofile"
                  className="pointer image-contain"
                  width={750}
                  height={600}
                />
              </div>
            </div>
          </FullScreenModal>
          <div className="mb-1">
            <div className="m-0 p-0 font-bold">
              {getLabel("noticePhotos", initialLanguage)}
            </div>
            <p className="system-muted1 p-0 m-0">
              {getLabel("noticePhotosDescription", initialLanguage)}
            </p>
          </div>
          <div className=" form-check form-switch mb-2 font-110">
            <input
              className="form-check-input border border-2 system-control pointer"
              type="checkbox"
              id="flexSwitchCheckDefault"
            />
            <label
              className="form-check-label font-90 font-normal ps-1"
              htmlFor="flexSwitchCheckDefault"
            >
              {getLabel("displayInTheMemorialPage", initialLanguage)}
            </label>
          </div>
          <div className="d-flex flex-wrap gap-2 gap-sm-3 mb-5 justify-content-start">
            {showMediaData?.data?.map((item: any, index: any) => {
              return (
                <>
                  {item.media_type.id == 1 && (
                    <div className="position-relative upload-image-container">
                      <Image
                        src={showProfileStates[index] ? item.url : BG}
                        alt="memorialprofile"
                        className="pointer image"
                        width={133}
                        height={110}
                        style={{ borderRadius: "12px", border: "2px" }}
                        onLoad={() => {
                          handleImageLoad(index);
                        }}
                      />
                      <Popover
                        className={
                          popoverShow ? "d-none" : "d-block custom-dropdown"
                        }
                        trigger="click"
                        position="right"
                        content={
                          <div className="popover-content">
                            <li
                              className="pointer p-0 m-0 px-1 px-sm-4"
                              onClick={() => {
                                setPopoverShow(true);
                                makeItDefault(item.url);
                              }}
                              style={{ paddingRight: 20 }}
                            >
                              {/* {getLabel("makeItDefault",initialLanguage)} */}
                              Set as Profile photo
                            </li>
                            <div>
                              <ImageNewUploder
                                name="Edit"
                                headerText="Edit Your Photo"
                                textOnly={true}
                                edit={true}
                                handleUploadImage={(file) =>
                                  editUploadImage(item.uuid, file)
                                }
                                imageUrl={item.url}
                                showPopupFun={(val) => {
                                  console.log("ff", val);

                                  setPopoverShow(val);
                                }}
                              />
                            </div>
                            <li
                              className="pointer p-0 m-0 px-1 px-sm-4"
                              onClick={() => {
                                setPopoverShow(false);
                                deleteFun(item.uuid);
                              }}
                            >
                              {/* {getLabel("delete",initialLanguage)} */}
                              Delete photo
                            </li>
                          </div>
                        }
                      >
                        <Image
                          src={edit}
                          alt="memorialprofile"
                          className="img-fluid position-absolute top-0 end-0 m-2 pointer"
                          onClick={() => setPopoverShow(false)}
                        />
                      </Popover>
                    </div>
                  )}
                </>
              );
            })}
          </div>
          {deleteSkeleton ? (
            <>
              {" "}
              <div>{renderShimmerRows()}</div>
            </>
          ) : (
            <>
              <div>
                {showDeletedData?.data?.length > 0 && (
                  <div className="mb-5">
                    <div className="mb-3">
                      <div className="m-0 p-0 font-bold">
                        {getLabel("deletedPhotos", initialLanguage)}
                      </div>
                    </div>
                    <div className="d-flex flex-column gap-2">
                      {showDeletedData?.data &&
                        Array.isArray(showDeletedData.data) &&
                        showDeletedData.data.map((data: any) => (
                          <>
                            {data.media_type.id == 1 && (
                              <div className="d-flex gap-2">
                                <div className="delete-photo-container system-filter-secondary rounded">
                                  <Image
                                    src={data?.url}
                                    alt=""
                                    className="image rounded"
                                    width={100}
                                    height={100}
                                  />
                                </div>

                                <div style={{ maxWidth: 120 }}>
                                  <div className="system-muted1 font-80 font-normal caption-photos text-truncate">
                                    {data?.caption}
                                  </div>
                                  <div
                                    className="system-text-primary font-80 font-normal pointer"
                                    onClick={() => restore(data?.uuid)}
                                  >
                                    {getLabel("restore", initialLanguage)}
                                  </div>
                                </div>
                              </div>
                            )}
                          </>
                        ))}
                      {/* {showDeletedData?.data.length >= 15 && ( */}

                      {/* )} */}
                    </div>
                  </div>
                )}
              </div>
            </>
          )}
          {/* <div>
            {showDeletedData?.total > 15 && (
              // <PaginationComponent
              //   page={
              //     localStorage.getItem("deleted_pagination")
              //       ? parseInt(
              //           localStorage.getItem("deleted_pagination") as string,
              //           10
              //         )
              //       : 1
              //   }
              //   totalPages={Math.ceil(showDeletedData.total / 15)}
              //   handlePagination={handlePagination}
              // />
              <div
                style={{
                  justifyContent: "center",
                  display: "flex",
                }}
              >
                <MainPagination
                  page={
                    localStorage.getItem("deleted_pagination")
                      ? parseInt(
                          localStorage.getItem("deleted_pagination") as string,
                          10
                        )
                      : 1
                  }
                  totalPages={showDeletedData.total}
                  handlePagination={handlePagination}
                />
              </div>
            )}
          </div> */}
        </>
      )}
    </div>
  );
};

export default PhotosAndVideos;
