import Image from "next/image";
import React, { use, useContext, useEffect, useState } from "react";
import trash from "../../../public/trash.svg";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import { CountriesSelector } from "@/redux/countries/selectors";
import { VisibilitiesSelector, getAllVisibilities } from "@/redux/visibilities";
import { memorialContactSelector } from "@/redux/momorial-contact";
import { customerPageSelector } from "@/redux/customer";
import { noticeContactSelector } from "@/redux/notice-contact";
import { integer } from "aws-sdk/clients/cloudfront";
import { contactTypeSelector, getAllContactTypes } from "@/redux/contact-type";
import { getAllRelationships, relationsSelector } from "@/redux/relationships";
import { getLabel } from "@/utils/lang-manager";
import { Select } from "@arco-design/web-react";
import { useApiErrorHandling } from "@/components/Others/useApiErrorHandling";
import CustomSelect from "@/components/Arco/CustomSelect";
import CustomSelectIndex from "@/components/Arco/CustomSelectIndex";
import CustomSelectContact from "@/components/Arco/CustomSelectContact";
import addMore from "../../../public/add-more.svg";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";
interface AddNewContactProps {
  response: any;
  actionBtnRef: any;
  handleUpdate: (
    countryId: string,
    personName: string,
    relationship: number,
    visibilityId: number,
    hideAfterFuneral: boolean,
    contactTypes: any
  ) => void;
  name?: string;
  country?: string;
  relationshipId?: number;
  visibilityid?: number;
  hideAfterFuneralget?: boolean;
  contactNameValue?: any;
}

const ViewUpdateContact = ({
  handleUpdate,
  actionBtnRef,
  response,
  name,
  country,
  relationshipId,
  contactNameValue,
  visibilityid,
  hideAfterFuneralget,
}: AddNewContactProps) => {
  const [personName, setPersonName] = useState("");
  const [countryId, setCountryId] = useState("");
  const [hideAfterFuneral, setHideAfterFuneral] = useState(false);
  const { handleSave, PageData } = useContext(PageDataContext);
  const initialLanguage = PageData?.initialLanguage;
  useEffect(() => {
    setPersonName(name || "");
  }, [name]);
  useEffect(() => {
    setCountryId(country || "");
  }, [country]);
  useEffect(() => {
    setRelationship(relationshipId || 1);
  }, [relationshipId]);
  useEffect(() => {
    setVisibilityId(visibilityid || 1);
  }, [visibilityid]);
  useEffect(() => {
    setHideAfterFuneral(hideAfterFuneralget || false);
  }, [hideAfterFuneralget]);
  useEffect(() => {}, [contactNameValue]);
  const [error, setError] = useState({
    msg: "",
    name: "",
    first_name: "",
    last_name: "",
    mobile: "",
    email: "",
    alternative_phone: "",
    password: "",
    password_confirmation: "",
    pageTypeId: "",
    gender_id: "",
    contact_type: "",
  });

  const [contactTypes, setContactTypes] = useState([
    {
      id_or_number: "",
      contact_method_id: "",
    },
  ]);

  const dispatch = useAppDispatch();
  const [clickstatus, setClickStatus] = useState(false);

  const [contactFrames, setContactFrames] = useState([
    { select: 1, value: "" },
  ]);

  useEffect(() => {
    setContactTypes(
      contactFrames.map((item: any) => {
        return {
          id_or_number: item.value,
          contact_method_id: item.select,
        };
      })
    );
  }, [contactFrames]);

  const [visibilityId, setVisibilityId] = useState(1);

  const { data: showCountriresData } = useAppSelector(CountriesSelector);
  const { data: visibility } = useAppSelector(VisibilitiesSelector);
  const { error: apiError } = useAppSelector(memorialContactSelector);
  const { error: apiError1 } = useAppSelector(noticeContactSelector);
  const { data: contactType } = useAppSelector(contactTypeSelector);
  const { data: showRelationshipsData } = useAppSelector(relationsSelector);

  const errormemorial = useApiErrorHandling(apiError, response);
  const errorcontact = useApiErrorHandling(apiError1, response);
  console.log("222", errormemorial);

  useEffect(() => {
    if (errormemorial) {
      setError(errormemorial);
    }
  }, [errormemorial]);

  useEffect(() => {
    if (errorcontact) {
      setError(errorcontact);
    }
  }, [errorcontact]);

  useEffect(() => {
    actionBtnRef.current.onclick = () => {
      setClickStatus(true);
    };
    if (clickstatus) {
      handleUpdate(
        personName,
        countryId,
        relationship,
        visibilityId,
        hideAfterFuneral,
        contactTypes
      );
      setClickStatus(false);
    }
  }, [clickstatus]);

  const handleAddContactFrame = () => {
    setContactFrames([...contactFrames, { select: 1, value: "" }]);
    setdval((prevDval) => {
      const index = contactFrames.length;
      const updatedDval = [...prevDval];
      updatedDval[index] = 1;
      return updatedDval;
    });
  };
  useEffect(() => {
    dispatch(getAllVisibilities());
    dispatch(getAllRelationships());
  }, []);

  const handleRemoveContactFrame = (index: any) => {
    const newContactFrames = contactFrames.filter((_, i) => i !== index);
    setContactFrames(newContactFrames);
    setdval((prevDval) => {
      const updatedDval = [...prevDval];
      updatedDval.splice(index, 1);
      return updatedDval;
    });
  };

  const handleSelectChange = (index: any, selectValue: any) => {
    console.log("index", index);
    console.log("indexselectValue", selectValue);

    const newContactFrames = [...contactFrames];
    newContactFrames[index].select = selectValue;
    setContactFrames(newContactFrames);
    newContactFrames[index].value = "";
    setContactFrames(newContactFrames);
  };

  const handleInputChange = (index: any, event: any) => {
    const newContactFrames = [...contactFrames];
    newContactFrames[index].value = event.target.value;
    setContactFrames(newContactFrames);
  };

  useEffect(() => {
    dispatch(getAllContactTypes());
  }, []);

  const [relationship, setRelationship] = useState(1);
  const handleRelationshipsChange = (
    e: React.ChangeEvent<HTMLSelectElement>
  ) => {
    const selectedValue = e.target.value;
    setRelationship(parseInt(selectedValue, 10));
  };
  console.log("455", contactNameValue);
  const [isSingleTimeFrameWithValues, setIsSingleTimeFrameWithValues] =
    useState(false);
  const [dval, setdval] = useState(Array(contactFrames.length).fill(1));

  useEffect(() => {
    setIsSingleTimeFrameWithValues(
      contactFrames.length === 1 && (contactFrames[0].value ? true : false)
    );
  }, [contactFrames]);

  const updateStartTimeAndEndTime = () => {
    const initialDval = Array(contactFrames.length).fill(1);
    setdval(initialDval);
    setContactFrames((prevContactFrames) => [
      {
        ...prevContactFrames[0],
        value: "",
        select: 1,
      },
      ...prevContactFrames.slice(1),
    ]);
  };
  useEffect(() => {
    setContactFrames(
      contactNameValue.map((contact: any) => ({
        select: contact.contact_method.id,
        value: contact.id_or_number,
      }))
    );
  }, [contactNameValue]);
  console.log("fff", contactFrames);

  return (
    <>
      <div>
        <div className="row  align-items-center m-0">
          <div className="col-md-12 col-12 p-0 font-semibold">
            {getLabel("country", initialLanguage)}
            <span className=" ps-1">*</span>
          </div>
          <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-12 p-0 mb-3">
            <CustomSelect
              data={showCountriresData}
              placeholderValue={"Select a country"}
              onChange={(value: any) => {
                setCountryId(value);
              }}
              is_search={true}
              defaultValue={countryId == "" ? country : countryId}
            />
          </div>
          <div className="col-md-12 col-12 p-0 mb-3">
            <div className=" font-semibold">
              {getLabel("personName", initialLanguage)}
              <span className=" ps-1">*</span>
            </div>
            <input
              type="text"
              className="form-control border border-2 p-2 system-control "
              placeholder={`${getLabel("personName", initialLanguage)}`}
              value={personName}
              onChange={(e) => setPersonName(e.target.value)}
            />
            {error.name && (
              <div className="row align-items-center m-0">
                <div className="col-xxl-12 col-xl-6 col-lg-9 col-md-9 col-12 p-0 text-danger">
                  {error.name}
                </div>
              </div>
            )}
          </div>

          <div className="col-md-12 col-12 p-0 mb-3  ">
            <div className="font-semibold">
              {getLabel("selectRelationship", initialLanguage)}
              <span className=" ps-1">*</span>
            </div>

            <CustomSelect
              data={showRelationshipsData}
              placeholderValue={getLabel("selectRelationship", initialLanguage)}
              onChange={(value: any) => {
                setRelationship(value);
              }}
              is_search={true}
              defaultValue={relationship == 1 ? relationshipId : relationship}
            />
          </div>
        </div>
        {/* -------contact---------- */}
        <div className="mb-2">
          <div className="row m-0 ">
            <div className="col-4 pe-1 p-0">
              <div className=" font-semibold">
                {getLabel("contactType", initialLanguage)}
                <span className=" ps-1">*</span>
              </div>
            </div>
            <div className="col-7 ps-1 p-0">
              <label className="col-10 p-0  font-semibold ">
                {getLabel("enterContactDetail", initialLanguage)}
                <span className=" ps-1">*</span>
              </label>
            </div>
          </div>
          {contactFrames.map((contactFrame: any, index: any) => (
            <div key={index} className="row m-0 p-0 mb-2">
              <div className="col-4 pe-1 p-0">
                <CustomSelectContact
                  key={index}
                  data={contactType}
                  placeholderValue="Select an option"
                  defaultValue={contactFrame.select}
                  is_search={false}
                  handleSelectChange={handleSelectChange}
                  index={index}
                />
              </div>

              <div className="col-7 ps-1 p-0">
                {contactFrame.select === 1 && (
                  <input
                    type="email"
                    pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"
                    value={contactFrame.value ? contactFrame.value : ""}
                    onChange={(event) => handleInputChange(index, event)}
                    className="form-control border border-2 p-2 system-control"
                    placeholder="name@domain.com"
                  />
                )}
                {contactFrame.select === 2 && (
                  <input
                    type="tel"
                    pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"
                    value={contactFrame.value}
                    onChange={(event) => handleInputChange(index, event)}
                    className="form-control border border-2 p-2 system-control"
                    placeholder="+44"
                  />
                )}
                {contactFrame.select === 3 && (
                  <input
                    type="tel"
                    pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"
                    value={contactFrame.value}
                    onChange={(event) => handleInputChange(index, event)}
                    className="form-control border border-2 p-2 system-control"
                    placeholder="+44"
                  />
                )}
                {contactFrame.select === 4 && (
                  <input
                    type="tel"
                    pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"
                    value={contactFrame.value}
                    onChange={(event) => handleInputChange(index, event)}
                    className="form-control border border-2 p-2 system-control"
                    placeholder="+44 , name@domain.com"
                  />
                )}
                {contactFrame.select === 5 && (
                  <input
                    type="tel"
                    pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"
                    value={contactFrame.value}
                    onChange={(event) => handleInputChange(index, event)}
                    className="form-control border border-2 p-2 system-control"
                    placeholder="+44"
                  />
                )}
                {contactFrame.select === 6 && (
                  <input
                    type="email"
                    value={contactFrame.value}
                    onChange={(event) => handleInputChange(index, event)}
                    className="form-control border border-2 p-2 system-control"
                    placeholder="+44"
                  />
                )}
                {contactFrame.select === 7 && (
                  <input
                    type="text"
                    value={contactFrame.value}
                    onChange={(event) => handleInputChange(index, event)}
                    className="form-control border border-2 p-2 system-control"
                    placeholder="+44"
                  />
                )}
              </div>
              <div className="col-1 p-0  d-flex align-items-center justify-content-end ">
                {contactFrames.length == 1 &&
                  index == 0 &&
                  !isSingleTimeFrameWithValues && (
                    <Image
                      src={addMore}
                      alt="add-more"
                      onClick={handleAddContactFrame}
                      className="pointer"
                    />
                  )}
                {(index !== 0 ||
                  (contactFrames.length > 1 && index == 0) ||
                  isSingleTimeFrameWithValues) && (
                  <Image
                    src={trash}
                    alt="trash"
                    onClick={() => {
                      if (contactFrames.length !== 1) {
                        handleRemoveContactFrame(index);
                      } else {
                        updateStartTimeAndEndTime();
                      }
                    }}
                    className="pointer"
                  />
                )}
              </div>
            </div>
          ))}

          {error.contact_type && (
            <div className="row align-items-center" style={{ marginLeft: 4 }}>
              <div className="col-md-9 col-12 p-0 text-danger">
                {error.contact_type}
              </div>
            </div>
          )}
          {/* {contactTypes.length > 0 && contactTypes[0].id_or_number === "" && (
            <div className="row align-items-center" style={{ marginLeft: 4 }}>
              <div className="col-md-9 col-12 p-0 text-danger">
                The contact_details.0.id_or_number field is required.
              </div>
            </div>
          )} */}
        </div>

        {/* ----------contact---------- */}
        {isSingleTimeFrameWithValues || contactFrames.length > 1 ? (
          <div className="row m-0 ">
            <div className="col-md-12 col-12 p-0 m-0  ps-0  d-flex">
              <p
                className="p-0 system-text-primary font-semibold pointer"
                onClick={handleAddContactFrame}
              >
                <u>{getLabel("add", initialLanguage)}</u>
              </p>
              <span className=" ps-1">
                {getLabel("additionalContact", initialLanguage)}{" "}
              </span>
            </div>
          </div>
        ) : (
          <div className="pb-2"></div>
        )}
        <hr className="mb-3 mt-0 hr-width-form" />
        <div className="row m-0">
          <div className="col-md-6 col-12 p-0 mb-3 pe-md-2 ">
            <div className=" font-semibold">
              {getLabel("visibility", initialLanguage)}
            </div>

            <CustomSelect
              data={visibility}
              placeholderValue={"Select a visibility"}
              onChange={(value: any) => {
                setVisibilityId(value);
              }}
              is_search={false}
              defaultValue={visibilityid}
            />
          </div>
          <div className="col-md-6 col-12 p-0 mb-3 ps-md-2 ps-0">
            <div className=" font-semibold">
              {getLabel("hideContactContent", initialLanguage)}
            </div>

            <CustomSelect
              data={[
                { id: 1, name: "Yes" },
                { id: 0, name: "No" },
              ]}
              placeholderValue={"Select a Hide After Funeral"}
              onChange={(value: any) => {
                setHideAfterFuneral(value);
              }}
              is_search={false}
              defaultValue={hideAfterFuneral ? 1 : 0}
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default ViewUpdateContact;
