import PageManagerCard from "@/components/Cards/PageManagerCard";
import React, { useContext, useEffect, useRef, useState } from "react";
import memorialprofile from "../../../public/memorialprofile.png";
import AlertModal from "@/components/Elements/AlertModal";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import {
  InviteManager,
  getAllPageManagers,
  pageManagerSelector,
} from "@/redux/page-manager";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import { useRouter } from "next/router";
import AlertModalOne from "@/components/Elements/AlertModalOne";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";
import PageManagerCardCustomerSkelton from "@/components/Skelton/PageManagerClientSkelton";
import CustomerSkelton from "@/components/Skelton/CustomerSkelton";
import PageManagerSkelton from "@/components/Skelton/PageManagerSkelton";
import PageManagerCardCustomer from "@/components/Cards/PageManagerCardCustomer";
import { Notification } from "@arco-design/web-react";
import { managerTypesSelector } from "@/redux/managerTypes";
import { useApiErrorHandling } from "@/components/Others/useApiErrorHandling";
import { getLabel } from "@/utils/lang-manager";
import ShimmerEffectText from "@/components/Skelton/ShimmerEffectText";
import InviteManagerAlertModal from "@/components/Modal/InviteManagerAlertModal";
import { Button } from "react-bootstrap";

const PageManager = () => {
  const { PageData, handleSave } = useContext(PageDataContext);
  const clientData = PageData?.client;
  const initialLanguage = PageData?.initialLanguage
  const [invitePageManager, setInvitePageManager] = useState(false);
  const [managerTypeId, setManagerTypeId] = useState("");
  const [email, setEmail] = useState("");
  const [subject, setSubject] = useState("Invitation to Become a Page Manager"); // Default subject value
  const [isLoading, setIsLoading] = useState(false);
  const [response, setResponse] = useState("");

  const router = useRouter();
  const uuid = router.query.id;

  const dispatch = useAppDispatch();
  const actionBtnRef = useRef<HTMLButtonElement>(null);

  // const { data: ManagerTypesData } = useAppSelector(managerTypesSelector);
  const loadData = () => {
    setIsLoading(true);
    dispatch(
      getAllPageManagers({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_page: uuid as unknown as string,
      })
    ).then(() => {
      setIsLoading(false);
    });
  };
  const { data: pageManagerData } = useAppSelector(pageManagerSelector);
  useEffect(() => {
    loadData();
  }, [uuid]);

  //   const selectedValue = e.target.value;
  //   setManagerTypeId(selectedValue);
  // };

  // const HandleInviteManager = async () => {
  //   const CreatedData = {
  //     email: email,
  //     manager_type_id: managerTypeId,
  //   };
  //   // console.log("CreatedData", CreatedData);
  //   dispatch(
  //     InviteManager({
  //       active_partner: ACTIVE_PARTNER,
  //       active_service_provider: ACTIVE_SERVICE_PROVIDER,
  //       active_page: uuid as unknown as string,
  //       CreatedData,
  //     })
  //   ).then(async (response: any) => {
  //     setResponse(response);
  //     setEmail("");
  //     setManagerTypeId("");
  //     if (response.payload.success == true) {
  //       setInvitePageManager(false);
  //       setResponse(response);
  //       setEmail("");
  //       setManagerTypeId("");

  //       const link = response?.payload?.data?.data?.url;
  //       Notification.success({
  //         title: "Invite Successfully",
  //         duration: 3000,
  //         content: undefined,
  //       });

  //       // =====send mail===========
  //       try {
  //         const text = `
  //           Dear Sir/Madem,

  //           I hope this message finds you well. We would like to extend our invitation to you to join us as a Page Manager for our platform. Your expertise and skills would be a valuable addition to our team.

  //           To accept this invitation, please click on the following link:
  //           ${link}
  //           We look forward to having you on board and working together to achieve our goals.
  //           Thank you for considering our invitation, and we hope to hear from you soon.

  //           Best regards,
  //           Blackbee Partner Portal
  //           Obituary Platform
  //         `;

  //         const response = fetch("/api/sendEmail", {
  //           method: "POST",
  //           headers: {
  //             "Content-Type": "application/json",
  //           },
  //           body: JSON.stringify({
  //             to: email,
  //             subject,
  //             text: text,
  //           }),
  //         });

  //         if ((await response).ok) {
  //           console.log("Email sent successfully!");
  //         } else {
  //           console.error("Error sending email:", (await response).text());
  //         }
  //       } catch (error) {
  //         console.error("Error sending email:", error);
  //       }
  //     } else {
  //       Notification.error({
  //         title: "Invite Failed",
  //         duration: 3000,
  //         content: undefined,
  //       });
  //     }
  //   });
  // };
  const [managerButtonEnable, setManagerButtonEnable] = useState(false);
  const HandleInviteManager = (managerTypeId: string, email: string) => {
    setManagerButtonEnable(true);
    InviteManagerMemorial(managerTypeId, email);
  };
  const InviteManagerMemorial = (managerTypeId: string, email: string) => {
    const InviteData = {
      email: email,
      manager_type_id: managerTypeId,
    };
    dispatch(
      InviteManager({
        active_partner: ACTIVE_PARTNER,
        active_service_provider: ACTIVE_SERVICE_PROVIDER,
        active_page: uuid as unknown as string,
        InviteData,
      })
    ).then(async (response: any) => {
      setResponse(response);
      setEmail("");
      setManagerTypeId("");
      if (response.payload.success == true) {
        setInvitePageManager(false);
        setResponse(response);
        setEmail("");
        setManagerTypeId("");

        const link = response?.payload?.data?.data?.url;
        setManagerButtonEnable(false);
        Notification.success({
          title: "Invite Successfully",
          duration: 3000,
          content: undefined,
        });

        // =====send mail===========
        try {
          const text = `
              Dear Sir/Madem,
  
              I hope this message finds you well. We would like to extend our invitation to you to join us as a Page Manager for our platform. Your expertise and skills would be a valuable addition to our team.
  
              To accept this invitation, please click on the following link:
              ${link}
              We look forward to having you on board and working together to achieve our goals.
              Thank you for considering our invitation, and we hope to hear from you soon.
  
              Best regards,
              Blackbee Partner Portal
              Obituary Platform
            `;

          const response = fetch("/api/sendEmail", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              to: email,
              subject,
              text: text,
            }),
          });

          if ((await response).ok) {
            console.log("Email sent successfully!");
          } else {
            console.error("Error sending email:", (await response).text());
          }
        } catch (error) {
          console.error("Error sending email:", error);
        }
      } else {
        setManagerButtonEnable(false);
        Notification.error({
          title: "Invite Failed",
          duration: 3000,
          content: undefined,
        });
      }
    });
  };

  return (
    <>
      <AlertModalOne
        show={invitePageManager}
        onClose={() => {
          setInvitePageManager(false);
        }}
        position="centered"
        title={`${getLabel("addNewManager", initialLanguage)}`}
        description={`${getLabel("addNewManagerDescription", initialLanguage)}`}
        footer={
          <Button
            ref={actionBtnRef}
            className="btn-system-primary"
            disabled={managerButtonEnable}
          >
            {getLabel("invite", initialLanguage)}
          </Button>
        }
      >
        <InviteManagerAlertModal
          response={response}
          actionBtnRef={actionBtnRef}
          HandleInviteManager={HandleInviteManager}
        />
      </AlertModalOne>

      {/* <AlertModalOne
        show={invitePageManager}
        onClose={() => {
          setInvitePageManager(false);
        }}
        position="centered"
        title={`${getLabel("addNewManager",initialLanguage)}`}
        description={`${getLabel("addNewManagerDescription",initialLanguage)}`}
      >
        <>
          <div className="row m-0 mb-3">
            <div className="col-12 font-semibold mb-0 p-0">
              {getLabel("email",initialLanguage)}
            </div>
            <div className="col-12 p-0">
              <input
                type="text"
                className="form-control border border-2 system-control h-46"
                placeholder={`${getLabel("addNewManagerDescription",initialLanguage)}`}
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              {err && (
                <div className="text-danger font-normal">{err.email}</div>
              )}
            </div>
          </div>
          <div className="row m-0 mb-4">
            <div className="col-12 font-semibold mb-0 p-0">
              {getLabel("managerType",initialLanguage)}
            </div>
            <div className="col-12 p-0">
              <select
                className="form-select border border-2 bg-white system-control p-2"
                id="dropdownMenuLink"
                value={managerTypeId}
                onChange={handleManagerTypeChange}
              >
                <option value="" disabled selected>
                  {getLabel("selectManagerType",initialLanguage)}
                </option>
                {Array.isArray(ManagerTypesData) &&
                  ManagerTypesData?.map((option: any) => (
                    <option
                      key={option.id}
                      value={option.id}
                      className="text-truncate"
                    >
                      {option.name}
                    </option>
                  ))}
              </select>

              {err && (
                <div className="text-danger font-normal">
                  {err.manager_type_id}
                </div>
              )}
            </div>
          </div>

          <div className="row m-0">
            <div className="col-auto p-0">
              <button
                className="btn btn-system-primary"
                onClick={HandleInviteManager}
              >
                {getLabel("invite",initialLanguage)}
              </button>
            </div>
          </div>
        </>
      </AlertModalOne> */}

      <div className="pe-md-2 ">
        {PageData.loading ? (
          <>
            <div className="mb-3 page-heading font-bold">
              {getLabel("pageManagers", initialLanguage)}{" "}
            </div>
            <div className="font-semibold">{getLabel("client", initialLanguage)}</div>
            <div className="font-90 system-muted-2 mb-2">
              {getLabel("clientDescription", initialLanguage)}
            </div>
          </>
        ) : (
          <>
            <div className="mb-3 page-heading font-bold col-lg-3 col-7 col-md-4">
              <ShimmerEffectText width="" height="20px" />{" "}
            </div>
            <div className="font-semibold mb-1 col-lg-3 col-7 col-md-4">
              <ShimmerEffectText width="" height="12px" />
            </div>
            <div className="font-90 system-muted-2 mb-1 col-lg-9 col-10 col-md-7">
              <ShimmerEffectText width="" height="12px" />
            </div>
          </>
        )}
        <div className="mb-4">
          {!PageData.loading ? (
            <PageManagerCardCustomerSkelton />
          ) : (
            <PageManagerCardCustomer
              data={clientData}
              handleSave={handleSave}
            />
          )}
        </div>
        {PageData.loading ? (
          <>
            <div className="font-semibold">{getLabel("pageManagers", initialLanguage)}</div>
            <div className="font-90 system-muted-2 mb-2">
              {getLabel("pageMangerDescription", initialLanguage)}
            </div>
          </>
        ) : (
          <>
            <div className="font-semibold  mb-1 col-lg-3 col-7 col-md-4">
              <ShimmerEffectText width="" height="12px" />
            </div>
            <div className="font-90 system-muted-2 mb-2 col-lg-9 col-10 col-md-7">
              <ShimmerEffectText width="" height="12px" />
            </div>
          </>
        )}
        <div>
          <PageManagerCard
            data={pageManagerData}
            loading={isLoading}
            activePage={uuid}
          />
        </div>
        <div
          className="pointer system-text-primary font-80 font-bold text-center text-sm-start"
          onClick={() => {
            setInvitePageManager(true);
          }}
        >
          {getLabel("inviteNewManagers", initialLanguage)}
        </div>
      </div>
    </>
  );
};

export default PageManager;
