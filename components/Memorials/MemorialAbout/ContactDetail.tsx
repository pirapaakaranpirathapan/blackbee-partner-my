import MemorialContactCard from "@/components/Cards/MemorialContactCard";
import FullScreenModal from "@/components/Elements/FullScreenModal";
import { PaginationComponent } from "@/components/Elements/Pagination";
import Loader from "@/components/Loader/Loader";
import AddNewContact from "@/components/Modal/AddNewContact";
import { ACTIVE_PARTNER, ACTIVE_SERVICE_PROVIDER } from "@/config/constants";
import { memorialPageSelector } from "@/redux/memorial-pages/selectors";
import {
  getAllMemorialContact,
  updateFamilyAddress,
  createMemorialContact,
} from "@/redux/momorial-contact/actions";
import { memorialContactSelector } from "@/redux/momorial-contact/selectors";
import { useAppDispatch, useAppSelector } from "@/redux/store/hooks";
import { integer } from "aws-sdk/clients/cloudfront";
import { useRouter } from "next/router";
import React, { useContext, useEffect, useRef, useState } from "react";
import { Button } from "react-bootstrap";
import { Notification } from "@arco-design/web-react";
import ContactSkelton from "@/components/Skelton/ContactSkelton ";
import { PageDataContext } from "@/pages/memorials/[id]/PageDataContext";
import { getLabel } from "@/utils/lang-manager";
import MemorialContactSkeleton from "@/components/Skelton/MemorialContactSkelton";
import MainPagination from "@/components/Paginations";

const ContactDetail = () => {
  const { handleSave, PageData } = useContext(PageDataContext);
  const initialLanguage = PageData?.initialLanguage;
  const { dataOne: showtPageData } = useAppSelector(memorialPageSelector);
  const [addContact, setAddContact] = useState(false);
  const [familyDisplayAddress, setfamilyDisplayAddress] = useState(
    showtPageData?.family_address
  );

  const [response, setResponse] = useState("");
  const actionBtnRef = useRef<HTMLButtonElement>(null);
  const [contactButtondisabled, setContactButtondisabled] = useState(false);
  const handleUpdate = (
    personName: string,
    countryId: string,
    relationship: string,
    visibilityId: integer,
    auto_hide: boolean,
    contactTypes: any
  ) => {
    createContact(
      personName,
      countryId,
      relationship,
      visibilityId,
      auto_hide,
      contactTypes
    );
  };

  const createContact = (
    personName: any,
    countryId: any,
    relationship: any,
    visibilityId: any,
    hideAfterFuneral: any,
    contactTypes: any
  ) => {
    setContactButtondisabled(true);
    const updatedData = {
      name: personName,
      country_id: countryId,
      relationship_id: relationship,
      contact_details: contactTypes,
      // relationship: relationship,
      visibility_id: visibilityId,
      auto_hide: hideAfterFuneral,
    };
    console.log("888***", updatedData);

    dispatch(
      createMemorialContact({
        activePartner: ACTIVE_PARTNER,
        activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
        activePageId: activePagesId as unknown as string,
        updatedData,
      })
    ).then((response: any) => {
      if (response.payload.success == true) {
        setResponse(response);
        getAllContacts(1);
        // toast(response.payload.msg);
        setContactButtondisabled(false);
        Notification.success({
          title: "Contact has been created successfully",
          duration: 3000,
          content: undefined,
        });
        // getAllContacts && getAllContacts(1);
        setAddContact(false);
        dispatch(
          getAllMemorialContact({
            activePartner: ACTIVE_PARTNER,
            activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
            activePageId: activePagesId as unknown as string,
            page: 1,
          })
        );
      } else {
        setContactButtondisabled(false);
        response.payload.code != 422 &&
          Notification.error({
            title: "Faild to create contact",
            duration: 3000,
            content: undefined,
          });
      }
    });
  };

  function deleteFunctionHere(id: any) {
    console.log(id + "Delete Function is working");
  }

  //getAllContacts
  const router = useRouter();
  const activePagesId = router.query?.id;
  const dispatch = useAppDispatch();
  const { data: allContactsData } = useAppSelector(memorialContactSelector);
  const [isLoading, setIsLoading] = useState(true);
  const getAllContacts = (page: integer) => {
    setIsLoading(true);
    dispatch(
      getAllMemorialContact({
        activePartner: ACTIVE_PARTNER,
        activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
        activePageId: activePagesId as unknown as string,
        page: page,
      })
    ).then(() => {
      setIsLoading(false);
    });
  };
  useEffect(() => {
    getAllContacts(page);
    console.log("ff", allContactsData);
  }, [activePagesId]);
  useEffect(() => {
    setfamilyDisplayAddress(showtPageData?.family_address);
  }, [showtPageData]);

  // family address update
  const familyAddressUpdateHandle = () => {
    setIsLoading(true);
    // if (familyDisplayAddress) {
    const updatedData = {
      family_address: familyDisplayAddress,
    };
    if (activePagesId != null) {
      dispatch(
        updateFamilyAddress({
          activePartner: ACTIVE_PARTNER,
          activeServiceProvider: ACTIVE_SERVICE_PROVIDER,
          activePageId: activePagesId as unknown as string,
          updatedData,
        })
      ).then((response: any) => {
        setIsLoading(PageData?.loading ? false : true);

        if (response.payload.success == true) {
          handleSave();
          Notification.success({
            title: "Family Address Save Successfully",
            duration: 3000,
            content: undefined,
          });
          // setfamilyDisplayAddress("");
        } else {
          Notification.error({
            title: "Family Address Save Failed",
            duration: 3000,
            content: undefined,
          });
          // setfamilyDisplayAddress("");
        }
      });
    }
    // } else {
    //   Notification.error({
    //     title: "Please Enter Family Address",
    //     duration: 3000,
    //     content: undefined,
    //   });
    // }
  };
  //pagination
  const [page, setPage] = useState(1);
  const handlePagination = (page: number) => {
    setPage(page);
    getAllContacts(page);
  };
  const handleload = () => {
    getAllContacts(1);
  };
  const handledelete = () => {
    setIsLoading(true);
    getAllContacts(1);
  };
  return (
    <>
      <FullScreenModal
        show={addContact}
        onClose={() => {
          setAddContact(false);
        }}
        title={`${getLabel("addNewContact", initialLanguage)}`}
        description={`${getLabel(
          "addNewContactModalHeadingDescription",
          initialLanguage
        )}`}
        footer={
          <Button
            ref={actionBtnRef}
            className="btn-system-primary"
            disabled={contactButtondisabled}
          >
            {getLabel("submit", initialLanguage)}
          </Button>
        }
      >
        <AddNewContact
          actionBtnRef={actionBtnRef}
          handleUpdate={handleUpdate}
          response={response}
        />
      </FullScreenModal>
      {isLoading ? (
        <MemorialContactSkeleton />
      ) : (
        <div>
          <div className="page-heading font-bold">
            {getLabel("contactInformation", initialLanguage)}
          </div>
          <p className="system-muted-2 font-90 mb-4">
            {getLabel("contactInformationDescription", initialLanguage)}
          </p>
          <div className="row mb-0">
            <div className="col-12">
              <button
                type="button"
                onClick={() => {
                  setAddContact(true);
                }}
                className="btn system-outline-primary mb-3 font-bold"
              >
                + {getLabel("addContact", initialLanguage)}
              </button>
            </div>
          </div>
          <div className="mb-5">
            {allContactsData && (
              <>
                {allContactsData?.data?.length >= 0 &&
                  allContactsData?.data?.map((data: any) => (
                    <MemorialContactCard
                      data={data}
                      handleload={handleload}
                      handledelete={handledelete}
                    />
                  ))}
                {/* {allContactsData?.total > 15 && (
                  // <PaginationComponent
                  //   page={page}
                  //   totalPages={Math.ceil(allContactsData.total / 15)}
                  //   handlePagination={handlePagination}
                  // />
                  <div
                    style={{
                      justifyContent: "center",
                      display: "flex",
                    }}
                  >
                    <MainPagination
                      page={page}
                      totalPages={allContactsData.total}
                      handlePagination={handlePagination}
                    />
                  </div>
                )} */}
              </>
            )}
          </div>
          <div className="">
            <div className="page-heading font-bold">
              {getLabel("familyAddress", initialLanguage)}
            </div>
            <p className="system-muted-2 font-90">
              {getLabel("familyAddressDescription", initialLanguage)}
            </p>
            <div className="row  align-items-center  m-0 mb-3">
              <div className="col-12  p-0">
                <textarea
                  className="form-control border border-2 system-control resize-none"
                  rows={2}
                  aria-label="With textarea"
                  defaultValue={familyDisplayAddress}
                  onChange={(e) => setfamilyDisplayAddress(e.target.value)}
                ></textarea>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="  col-12 ">
              <button
                type="button"
                className="btn btn-system-primary my-1"
                onClick={familyAddressUpdateHandle}
              >
                {getLabel("save", initialLanguage)}
              </button>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default ContactDetail;
